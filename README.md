Linux
=========
To check out on a machine, in the Projects directory run:
git clone git+ssh://s@192.168.1.2/~/GIT/TrainsAndThings.git

Install required tools:

    ./Tools/install.sh

Note that this will set up docker to be able to do proper builds for you.

Basically everything to build/upload is done via:

    ./Tools/build.py

Windows
===========
Tools here are provided only for debugging, windows builds are generated on Linux

Install required tools:

    ./Tools/windows_install.bat

Check out and build the engine via, go into In the Tools directory, using right click > Git Bash here:

    python build.py

Run option #1. Checkout/Update Engine from GIT followed by #2.

Mac
===========
Follow the steps here to install osxcross:
https://docs.godotengine.org/en/stable/development/compiling/compiling_for_osx.html#cross-compiling-for-macos-from-linux

build.py
===========
This script does all the heavy lifting.

Type in the platform name (eg. windows) you want to switch the boolean values. Do this also for each type of boolean displayed.
Then use the provided menu options to do a build, package etc as you need.

JIRA-Backup
================
This includes a backup of JIRA project management tool we used for trains and things.
This can be restored from inside JIRA once you have JIRA up and running under Administration (gear icon) > System > Import and export/Restore System

Release Procedure
===================
Using build.py in the git branch you want to make live, run the option "all the things" which will build, package data into Build/Demo, Full and Editor targets for Linux and Windows. It will then run unit tests and if all good, upload to steam and itch.
Once this has completed it will notify the #bitshift:matrig.org channel on failure or success (so I don't have to be baby the build).
Nothing it required for itch, the upload will be live straight away.

For Steam, login to https://partner.steamgames.com/ (I have to do this in my laptop to use my mobile hotspot to avoid the dodgey IP checking)

For the Full version, Editor and Demo, goto the App Admin page, click SteamPipe > Builds

https://partner.steamgames.com/apps/builds/878030
https://partner.steamgames.com/apps/builds/878040
https://partner.steamgames.com/apps/builds/1148800

For the top build, select "default" from the "Set build live on branch..." and press "Preview Change"


