NOTE: Install all programs to their default locations

Get VS2015 community downloading:
https://imagine.microsoft.com/en-us/Catalog/Product/101

Download and install tortise git:
https://tortoisegit.org/download/
When installing ensure you select "OpenSSH"
Download the recommended git and install it.


Make a directory called "Projects" where you want to work from
Open a terminal by right clicking and selecting "Run as administrator", navigate to the "Projects" dir and run the command:
git clone -c core.symlinks=true git+ssh://s@192.168.1.2/~/GIT/TrainsAndThings.git

Ensure there are no symlink errors in this stage - if there are, you failed to run the command prompt as admin.


Download and install python 2.7+:
https://www.python.org/downloads/

Download and install scons:
http://scons.org/pages/download.html

Install VS2015:
Type os installation: Custom
Under Programming Languages, check Visual C++ is checked


-------------------------------------------

For more information see:
http://docs.godotengine.org/en/stable/development/compiling/compiling_for_windows.html

