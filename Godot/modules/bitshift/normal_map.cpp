/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "normal_map.h"
#include "terrain.h"
#include "util.h"
#include "thread_pool.h"
#include "database.h"
//#include "image_exr.h"
#include "editor/editor_file_system.h"
#include "core/os/os.h"

#include "drivers/vulkan/rendering_device_vulkan.h"

void BNormalMap::set_texture(const Ref<Texture>& cm) {
    texture_tool.set_texture(cm);
}

Ref<Texture> BNormalMap::get_texture() const {
    return texture_tool.texture;
}


Error BNormalMap::save(const String& resource_path) {
    Error err = texture_tool.image->save_png(resource_path);
    return err;
}

Error BNormalMap::load(const String& path) {

    Ref<StreamTexture> n = ResourceLoader::load(path);
    if (n.is_null())
        return FAILED;

    set_texture(n);
    return OK; //error;
}

Error BNormalMap::generate_and_save_from_height_map(const String& path, const BHeightMap& height_map, float blur_size, float blur_sigma) {
#if 0
    uint64_t us = OS::get_singleton()->get_ticks_usec();

    Error error = generate_from_height_map(height_map);
    if (error != OK)
        return error;

    DEBUG_PRINT("BNormalMap generate_from_height_map took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
/* this d-normalises the normalmap
    // guassian blur
    if (blur_size > 0.f && blur_sigma > 0.f) {
        uint64_t us = OS::get_singleton()->get_ticks_usec();
        texture_tool.gaussian_blur(blur_size, blur_sigma);
        DEBUG_PRINT("BNormalMap gaussian_blur took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
    }
*/
    {
        Ref<ImageTexture> normal_texture;
        normal_texture.instance();
        normal_texture->create_from_image(texture_tool.image);

        ResourceSaver::save(path, normal_texture);

     #ifdef TOOLS_ENABLED
        // this is so we can load straight away in the editor
        if (EditorFileSystem::get_singleton()) {
            // cause the image to be "imported"
            Vector<String> reimports;
            reimports.push_back(path);
            EditorFileSystem::get_singleton()->reimport_files(reimports);
        }
      #endif

        //memdelete(normal_texture);
    }
#endif
    return OK;
}

/*
Ref<ImageTexture> BNormalMap::create_texture_from_image() {
    return texture_tool.create_texture_from_image();
}*/

Error BNormalMap::generate_from_height_map(const BHeightMap& height_map, float map_size) {
    Error error = OK;

    // using inv_scale as an integeensures we keep power of two textures
    // setting to 2 for example just helps with load times for development purposes
    int inv_scale = BDatabase::get_singleton()->get_value("bitshift/normal_map/downscale", 1);

    // a 512 texture on a 512 map will have a 1:1 ratio i.e 1 pixel per meter
    // a 512 texture on a 1024 sized map will have a 1:2 ratio, ie. 1 pixel per 2 metres
    // which i need to take into account when generating the normal map
    float height_map_stretch = map_size / height_map.get_height_map_tool().width;

    // check this here, because it is done inside create_texture
    // and if it fails, the image is "converted" which takes ages!
    bool is_supported = RD::get_singleton()->texture_is_format_supported_for_usage(RD::DATA_FORMAT_R16G16B16_SFLOAT, RD::TEXTURE_USAGE_SAMPLING_BIT | RD::TEXTURE_USAGE_CAN_UPDATE_BIT);
    Image::Format format = is_supported ? Image::FORMAT_RGBH : Image::FORMAT_RGBAH;

    bool ok = texture_tool.create_texture(height_map.get_height_map_tool().width / inv_scale, height_map.get_height_map_tool().height / inv_scale, false, format);
    if (!ok) {
        // something went wrong!
        return FAILED;
    }


/*
    Ref<ImageTexture> texture;
    texture.instance();
    texture->create(height_map.get_height_map_tool().width / inv_scale, height_map.get_height_map_tool().height / inv_scale, Image::FORMAT_RGBH);
    //set_texture(texture);
    Ref<Image> img = texture->get_data();
    //bool valid = img->valid();

    // create a normal map of the appropriate dimensions
    Ref<Image> normal_image;
    normal_image.instance();
    normal_image->create(height_map.get_height_map_tool().width / inv_scale, height_map.get_height_map_tool().height / inv_scale, false, Image::FORMAT_RGBH); // TODO: do we need a float verion?
    //normal_image->fill(Color(0.5f, 0.5f, 1.0f));

    texture_tool.set_image(normal_image);
*/

    // Weighted step list provides a blur/filtering/softening mechanism to iron out wrinkles
    Vector<WeightedStep> weightedStepList;
    weightedStepList.push_back(WeightedStep(1, 1.f));
    /*
    for (int i = 1; i <= 2; ++i)
    {
        float weight = (40.f - float(i)) / 40.f;
        weightedStepList.push_back(WeightedStep(i, weight));
    }*/

    uint64_t us = OS::get_singleton()->get_ticks_usec();

    bool multithreaded = BDatabase::get_singleton()->get_value("bitshift/normal_map/multithreaded", true);

    BThreadPool* thread_pool = BThreadPool::get_singleton();
    int thread_count = thread_pool->get_thread_count();

    int height = texture_tool.height;
    int rows_per_task = floor(height / thread_count);

    task_count = thread_count;
    for (int ti = 0; ti < thread_count; ++ti) {
        GenerateNormalsTaskData* task_data = memnew(GenerateNormalsTaskData);
        task_data->weighted_step_list = weightedStepList;
        task_data->height_map_stretch = height_map_stretch;
        task_data->inv_scale = inv_scale;
        task_data->height_map = &height_map;
        task_data->first_row = rows_per_task * ti;
        task_data->last_row = task_data->first_row + rows_per_task;
        if (ti == (thread_count - 1)) {
            task_data->last_row = height;
        }

        if (multithreaded) {
            Task* task = Task::Create(&BNormalMap::generate_normals_task, this, task_data);
            task->set_complete(&BNormalMap::generate_normals_task_complete, this, task_data);
            thread_pool->add_task(task);
        }
        else {
            generate_normals_task(task_data);
            memdelete(task_data);
        }
    }

    // enable this line to see how long it takes to do this job
    // but do not commit it enabled,
    // as we waste time here waiting for the jobs to be done while
    // could be loade the rest of the map
    /*
    if (multithreaded) {
        thread_pool->wait_all();
        DEBUG_PRINT("BTerrain::_load_normal_map (generate from height map) took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));   
    }*/

    if (!multithreaded) {
        texture_tool.update_texture();
    }
    
    return error;
}

void BNormalMap::generate_normals_task(GenerateNormalsTaskData* task_data) {

    int inv_scale = task_data->inv_scale;
    uint32_t wsl_size = task_data->weighted_step_list.size();
    float height_scale = task_data->height_map_stretch; //atan(task_data->height_map_stretch); // triganometry!

    uint32_t width = texture_tool.width; //task_data->height_map->get_height_map_tool().width;
    for (uint32_t y = task_data->first_row; y < task_data->last_row; ++y) {
        for (uint32_t x = 0; x < width; ++x) {
            Vector3 normal;
            for (uint32_t w = 0; w < wsl_size; ++w) {
                const WeightedStep& ws = task_data->weighted_step_list.get(w); //write[w];
                Vector3 step_normal = _fabian_get_normal_from_height_map(*task_data->height_map, x * inv_scale, y * inv_scale, float(ws.step), height_scale);
                normal += step_normal * ws.weight;
            }

            normal.normalize();
            normal = (normal + Vector3(1.f, 1.f, 1.f)) * 0.5f; // encode normal for 0-1 range of texture
            texture_tool.image->set_pixel(x, y, BUtil::to_colour(normal));
        }
    }
}

void BNormalMap::generate_normals_task_complete(GenerateNormalsTaskData* task_data) {
    memdelete(task_data);
    --task_count;

    // finished generating the normal map
    // so apply it on the texture
    if (task_count == 0) {
        texture_tool.update_texture();
    }
}

// this is a function to help us determine if the normal map we might have generated with generate_from_height_map is bung
// or if the normal from heightmap generation is bung...
Vector3 BNormalMap::get_normal_from_height_map(const BHeightMap& height_map, float map_size, const Vector2& texel_coord) const {
    // a 512 texture on a 512 map will have a 1:1 ratio i.e 1 pixel per meter
    // a 512 texture on a 1024 sized map will have a 1:2 ratio, ie. 1 pixel per 2 metres
    // which i need to take into account when generating the normal map
    float height_map_stretch = map_size / height_map.get_height_map_tool().width;

    float x = texel_coord.x;
    float y = texel_coord.y;
    height_map.get_height_map_tool().texture_space_to_pixel_space(x, y);
    Vector3 normal = _fabian_get_normal_from_height_map(height_map, x, y, 1.f, height_map_stretch);
    return Vector3(normal.x, normal.z, -normal.y); // to match godot normals
}

Vector3 BNormalMap::_fabian_get_normal_from_height_map(const BHeightMap& height_map, uint32_t x, uint32_t y, float step, float height_scale) const {
    float width = height_map.get_height_map_tool().width_m1;
    float height = height_map.get_height_map_tool().height_m1;
/*
    DEBUG_PRINT("height at (" + String::num(x) + "," + String::num(y) + ")=" + String::num(height_map.get_height_from_pixel(x, y)));
    if (x == 2048 & y == 2048) {
        int nothing = 0;
        ++nothing;
    }*/

    float up = height_map.get_height_from_pixel(x, CLAMP(y - step, 0, height));
    float down = height_map.get_height_from_pixel(x, CLAMP(y + step, 0, height));
    float left = height_map.get_height_from_pixel(CLAMP(x - step, 0, width), y);
    float right = height_map.get_height_from_pixel(CLAMP(x + step, 0, width), y);

    // https://stackoverflow.com/questions/49640250/calculate-normals-from-heightmap
    // https://www.gamedev.net/forums/topic/163625-fast-way-to-calculate-heightmap-normals/
    Vector3 normal = Vector3((left - right), (down - up), 2.0f * height_scale);
    normal.normalize();
    return normal;
}

Vector3 BNormalMap::get_normal_from_texel(const Vector2& texel_coord) const {
    return texture_tool.decompress_normal_xy(texture_tool.get_texel_as_vec3_bilinear(texel_coord));
}

bool BNormalMap::is_valid() const {
    return texture_tool.is_valid();
}

void BNormalMap::_bind_methods() {
    /*
    ClassDB::bind_method(D_METHOD("set_normal_map","texture"),&BNormalMap::set_normal_map);
    ClassDB::bind_method(D_METHOD("get_normal_map"),&BNormalMap::get_normal_map);

    ClassDB::bind_method(D_METHOD("generate_and_save_from_height_map"),&BNormalMap::generate_and_save_from_height_map);

    ADD_PROPERTYNZ( PropertyInfo(Variant::OBJECT,"normal_map/normal_map",PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_normal_map", "get_normal_map");

    //ClassDB::bind_method(D_METHOD("save"),&BNormalMap::save);*/
}

BNormalMap::BNormalMap() {
    task_count = 0;
}

BNormalMap::~BNormalMap() {
}


