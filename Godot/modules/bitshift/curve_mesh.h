/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BCURVE_MESH_H
#define BCURVE_MESH_H

#include "spline.h"
#include "arc_line_curve.h"
#include "mesh_gen_tool.h"
#include <functional>

class MeshInstance;
class MeshDataTool;
class StaticBody;
class Task;
class Spatial;

class ICurveDeformer {
public:
    virtual Transform modify_transform(const Transform& t) = 0;
};

class NoDeform : public ICurveDeformer {
    Transform modify_transform(const Transform& t) { return t; }
};

class MoveToGround : public ICurveDeformer {
public:

    float push_along_normal_distance;

    Transform modify_transform(const Transform& t);

    MoveToGround() { push_along_normal_distance = 0.f; }
};

/**
	@author Fabian Mathews <supagu@gmail.com>
*/

class BCurveMesh : public Resource {

    GDCLASS(BCurveMesh, Resource);
    
public:

    enum MeshGenerationType {
        MGT_BOUNDS,
        MGT_MESH,
    };

    struct MeshInputData {
        MeshInstance* mesh_instance;
        Transform transform;
        MeshGenerationType mesh_gen_type;

        ICurveDeformer* curve_deformer;

        // ensure that spline and arc_line_curve will NOT be modified while task is running
        // if unsure make a copy of your curve and assign it
        Ref<BSpline> spline;
        Ref<BArcLineCurve> arc_line_curve;

        MeshInputData() {
            curve_deformer = NULL;
        }

        std::function<void()> complete_method;
    };

protected:

    Mutex* mutex;

	Node* instance_template;

    /*
	Ref<BSpline> spline;
	Ref<BArcLineCurve> arc_line_curve;
    */

	MeshInstance* mesh_instance;
    MeshDataTool* mesh_data_front_cap;
    MeshDataTool* mesh_data_middle;
    MeshDataTool* mesh_data_back_cap;
	
    BMeshGenTool mesh_gen;

    Task* task; // really we want to hold a weak ref, while the thread pool holds a strong ref

	Ref<ArrayMesh> generated_mesh;	
	
	Vector3 bounds_max;
	Vector3 bounds_min;
	
	bool move_to_ground;

    NoDeform no_deform;
	
	float get_curve_length();



    MeshInputData mesh_input_data;
    bool task_running;


    void populate_mesh_data_tool(Node* p_mesh_inst, MeshDataTool* mdt);

    void add_vertex(MeshDataTool* p_mdt, int v, float z_offset, float stretch, BMeshGenTool *p_mesh_gen);
    int add_indicies(MeshDataTool* p_mdt, int index_offset, BMeshGenTool *p_mesh_gen);
	
    //static Transform move_transform_to_ground(const Transform& t);

    void generate_mesh_from_instance_template();

    // far cheaper than generate_mesh as it just returns
    // uses a cube shape to stretch along the spline
    void generate_mesh_from_bounds();

    void _generate_mesh_task();
    void _generate_mesh_task_complete();

    Transform get_transform_from_distance_along_curve(float p_offset);

protected:

	static void _bind_methods();
	
public:

    // get bounds min and max from the instance template data
    Vector3 get_bounds_max() const { return bounds_max; }
    Vector3 get_bounds_min() const { return bounds_min; }

	
    //Ref<BArcLineCurve> get_arc_line_curve() const;

    bool is_task_running();
/*
	void set_arc_line_curve(const Ref<BArcLineCurve>& p_arc_line_curve);
	void set_spline(const Ref<BSpline>& p_spline);
*/

    // supply some mesh to repeat along the curve, this caches some data
    // sets up bounds etc... trading memory to improve speed
	void set_instance_template(Node* p_instance_template);

    bool generate_mesh(const MeshInputData& p_mesh_input_data);

    // Get the set of physics objects this mesh collides with along its route
    //int intersect_shape(ShapeResult *r_results, int p_result_max, const Set<RID> &p_exclude = Set<RID>(), uint32_t p_collision_mask = 0xFFFFFFFF);

    //Array intersect_shape(int p_result_max, const Array& p_exclude_array, uint32_t p_collision_mask = 0xFFFFFFFF);

    void generate_collision(Spatial* p_parent, const Transform& p_transform, const Vector3& grow_vec);

    BCurveMesh();
    ~BCurveMesh();       
};

#endif // BCURVE_MESH_H
