/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "register_types.h"
#include "itch_io.h"
#include "util.h"
#include "internet.h"
#include "terrain.h"
#include "water.h"
#include "clutter_map.h"
#include "boundary_map.h"
#include "normal_map.h"
#include "spline.h"
#include "curve_mesh.h"
#include "curve_mesh_node.h"
#include "arc_line_curve.h"
#include "arc_line_curve_node.h"
#include "terrain_material.h"
#include "water_material.h"
#include "dialogs.h"
#include "log.h"
#include "panel.h"
#include "panel_box_container.h"
#include "surface_tool.h"
#include "master_server.h"
#include "database.h"
#include "rss.h"
#include "irc.h"
#include "lod_group.h"
#include "thread_pool.h"
#include "prop_instance.h"
#include "modio.h"
#include "yaml.h"
#include "plugin_mgr.h"
#include "image_loader_png.h"
#include "core/io/image_loader.h"
#include "polygon_area.h"
#include "spin_slider.h"
#include "core/engine.h"
#include "core/vector.h"

#ifdef TOOLS_ENABLED
#include "editor/editor_plugin.h"
#endif

static ImageLoaderPNG *image_loader_png;

uint32_t start_allocs = 0;
uint32_t end_allocs = 0;

static Vector<Variant> singletons;

template<class T>
void instance_singleton(const char* p_singleton_name = NULL) {
    Ref<T> ref;
    ref.instance();
    singletons.push_back(ref);

    if (p_singleton_name) {
        Engine::get_singleton()->add_singleton(Engine::Singleton(p_singleton_name, ref.ptr()));
    }
}

void free_singletons() {
    /*
    for (int i = singletons.size() - 1; i >= 0; --i) {
        Variant v;
        singletons[i] = v;
    }*/
    singletons.clear();
}

void register_bitshift_types() {

    start_allocs = MemoryPool::allocs_used;

#ifdef DEMO_BUILD
    print_line("www.bitshift.io - Demo Build");
    #pragma message "COMPILING WITH DEMO_BUILD ENABLED"
#else
    print_line("www.bitshift.io");
#endif

    // make user dir if it doesn't exist
    String user_dir = GLOBAL_DEF("bitshift/user_dir", "res://User");
    BUtil::make_dir(user_dir);

    // replace the default PNG image loader with ours, which supports 16-bit
    ImageFormatLoader* defaultPngLoader = ImageLoader::recognize("png");
    ImageLoader::remove_image_format_loader(defaultPngLoader);
    image_loader_png = memnew(ImageLoaderPNG);
    ImageLoader::add_image_format_loader(image_loader_png);

    ClassDB::register_class<BYAML>();
    ClassDB::register_class<BSurfaceTool>();
    ClassDB::register_class<BTerrain>();
    ClassDB::register_class<BWater>();
    ClassDB::register_class<BClutterMap>();
    ClassDB::register_class<BBoundaryMap>();
    ClassDB::register_class<BNormalMap>();
    ClassDB::register_class<BLODGroup>();
    ClassDB::register_class<BSpline>();
    ClassDB::register_class<BCurveMesh>();
    ClassDB::register_class<BCurveMeshNode>();
    ClassDB::register_class<BArcLineCurve>();
    ClassDB::register_class<BWindowDialog>();
    ClassDB::register_class<BPanel>();
    ClassDB::register_class<HPanelBoxContainer>();
    ClassDB::register_class<VPanelBoxContainer>();
    ClassDB::register_class<BStandardMaterial3D>();
    ClassDB::register_class<BTerrainMaterial>();
    ClassDB::register_class<BWaterMaterial>();
    ClassDB::register_class<BRSS>();
    ClassDB::register_class<BPolygonArea>();
    ClassDB::register_class<BPropInstance>();
    ClassDB::register_class<BSpinSlider>();
    ClassDB::register_class<BIRC>();

    SceneTree::add_idle_callback(BStandardMaterial3D::flush_changes);
    BStandardMaterial3D::init_shaders();
    BTerrainMaterial::init_shaders();

    SceneTree::add_idle_callback(BWaterMaterial::flush_changes);
    BWaterMaterial::init_shaders();

    instance_singleton<BLog>("BLog");
    instance_singleton<BUtil>("BUtil");
    instance_singleton<BDatabase>("BDatabase");
    instance_singleton<BInternet>("BInternet");
    instance_singleton<BMasterServer>("BMasterServer");
    instance_singleton<BThreadPool>("BThreadPool");
    instance_singleton<BPluginMgr>("BPluginMgr");
    instance_singleton<BModio>("BModio");

#ifdef TOOLS_ENABLED
    EditorPlugins::add_by_type<BEditorPlugin>();
#endif

    end_allocs = MemoryPool::allocs_used;
}

void unregister_bitshift_types() {
    //ERR_EXPLAINC("There are still MemoryPool allocs in use at exit!");
    uint32_t allocs = MemoryPool::allocs_used;
    uint32_t end_alloc_diff = allocs - end_allocs;
    if (end_alloc_diff != 0) {
        String err = String(itos(end_alloc_diff)) + " memory leak(s) detected from bitshift outside of register_modules";
        const char* d = err.utf8().get_data();
        DEBUG_PRINT(d);
    }

    free_singletons();

	BWaterMaterial::finish_shaders();
    BTerrainMaterial::finish_shaders();
    BStandardMaterial3D::finish_shaders();

    memdelete(image_loader_png);
    
    allocs = MemoryPool::allocs_used;
    uint32_t start_alloc_diff = allocs - start_allocs;
    start_alloc_diff = start_alloc_diff - end_alloc_diff; // take outsode of register_modeules memory leaks away to compute memory leaks inside register modules
    if (start_alloc_diff != 0) {
        String err = String(itos(start_alloc_diff)) + " memory leak(s) detected from bitshift register_modules instances";
        const char* d = err.utf8().get_data();
       DEBUG_PRINT(d);
    }
}
