 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "plugin_mgr.h"
#include "core/bind/core_bind.h"
#include "scene/resources/packed_scene.h"
#include "core/io/file_access_pack.h"
#include "util.h"
#include "globals.h"

BPluginMgr *BPluginMgr::singleton=NULL;

Node* BPluginMgr::_load_descriptor(const String& p_resource_path) {
    // look for plugin or Plugin .tscn
    String plugin_path = p_resource_path + "/plugin.tscn";
    _Directory dir;
    if (!dir.file_exists(plugin_path)) {
        plugin_path = p_resource_path + "/Plugin.tscn";
        if (!dir.file_exists(plugin_path)) {
            return NULL;
        }
    }

    Ref<PackedScene> res = ResourceLoader::load(plugin_path, "PackedScene");
    if (res.is_null()) {
        ERR_PRINT("Failed to load plugin: " + plugin_path + ", Couldn't load PackedScene");
        return NULL;
    }
    Node *descriptor = res->instance();

    // check the type is correct
    bool is_plugin = BUtil::get_singleton()->is_class_name(descriptor, "Plugin");
    if (!is_plugin) {
        ERR_PRINT("Failed to load plugin: " + plugin_path + ", not of type Plugin");
        return NULL;
    }

    return descriptor;
}

bool BPluginMgr::add_plugin(const String& p_resource_path, const String &p_plugin_path) {
    // only allow the example mod in demo builds
    Node *descriptor = _load_descriptor(p_resource_path);
    if (!descriptor) {
        return false;
    }

    PluginInfo info;
    info.resource_path = p_resource_path;
    info.plugin_path = p_plugin_path;
    info.descriptor = descriptor;
    plugins[info.resource_path] = info;
    return true;
}

bool BPluginMgr::scan_directory(const String& p_directory) {
    // convert 'res://' to full path
    // otherwise godot will look inside the pack file in a packaged version of the game
    // if we supply a full path, it will do a proper directory scan

#ifdef DEMO_BUILD
    bool demo_ok = PackedData::get_singleton()->add_pack(String(DEMO_MAP_1) + ".pck", true) == OK;
    if (demo_ok) {
        add_plugin(DEMO_MAP_1, String(DEMO_MAP_1) + ".pck");
    }

    demo_ok = PackedData::get_singleton()->add_pack(String(DEMO_MAP_2) + ".pck", true) == OK;
    if (demo_ok) {
        add_plugin(DEMO_MAP_2, String(DEMO_MAP_2) + ".pck");
    }

    demo_ok = PackedData::get_singleton()->add_pack(String(DEMO_MOD) + ".pck", true) == OK;
    if (demo_ok) {
        add_plugin(DEMO_MOD, String(DEMO_MOD) + ".pck");
    }

#else

    String abs_path = BUtil::get_singleton()->to_absolute_path(p_directory);
    _Directory dir;
    if (dir.open(abs_path) != OK) {
        ERR_PRINT("BPluginMgr::scan_directory failed to open directory: " + p_directory);
        return false;
    }
    
    dir.list_dir_begin();
    String file_name = dir.get_next();
    while (file_name != "") {
        if (dir.current_is_dir()) {
            String resource_path = p_directory + "/" + file_name;
            add_plugin(resource_path, resource_path);
        }
        else if (file_name.ends_with(".pck") || file_name.ends_with(".zip")) {
            String package_path = p_directory + "/" + file_name;
            bool ok = PackedData::get_singleton()->add_pack(package_path, true) == OK;
            if (ok) {
                String plugin_path = p_directory + "/" + file_name.get_basename();
                add_plugin(plugin_path, p_directory + "/" + file_name);
            }
        }
        
        file_name = dir.get_next();
    }
#endif

    return true;
}
    
Dictionary BPluginMgr::get_plugins() {
    Dictionary results;
    for (Map<String, PluginInfo>::Element *E = plugins.front(); E; E = E->next()) {
        results[E->key()] = E->value().descriptor;
    }

    return results;
}

bool BPluginMgr::delete_plugin(Node* p_descriptor) {
    // blast the directory or pck/zip file
    for (Map<String, PluginInfo>::Element *E = plugins.front(); E; E = E->next()) {
        if (E->value().descriptor == p_descriptor) {
            String plugin_path = E->value().plugin_path;
            if (plugin_path.ends_with(".pck") || plugin_path.ends_with(".zip")) {
                // delete a file
                _Directory dir;
                if (dir.remove(plugin_path) != OK) {
                    return false;
                }
            }
            else {
                // delete the directory? chances are - no, lets not delete a directory, only packages mods 
                // else we might be deleting a mod developers mod folder!
                return false;
            }           

            plugins.erase(E->key());
            return true;
        }
    }

    return false;
}

Node* BPluginMgr::get_descriptor(const String& p_resource_path) {
    return plugins[p_resource_path].descriptor;
}

void BPluginMgr::_bind_methods() {
    ClassDB::bind_method(D_METHOD("scan_directory"),&BPluginMgr::scan_directory);
    ClassDB::bind_method(D_METHOD("get_plugins"),&BPluginMgr::get_plugins);
    ClassDB::bind_method(D_METHOD("get_descriptor"),&BPluginMgr::get_descriptor);
    ClassDB::bind_method(D_METHOD("add_plugin"),&BPluginMgr::add_plugin);
    ClassDB::bind_method(D_METHOD("delete_plugin"),&BPluginMgr::delete_plugin);
}

BPluginMgr::BPluginMgr() {
    singleton = this;
}

BPluginMgr::~BPluginMgr() {
    for (Map<String, PluginInfo>::Element *E = plugins.front(); E; E = E->next()) {
        memdelete( E->value().descriptor );
        E->value().descriptor = NULL;
    }
}
