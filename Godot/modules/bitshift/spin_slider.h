#ifndef BEDITOR_SPIN_SLIDER_H
#define BEDITOR_SPIN_SLIDER_H

#include "editor/editor_spin_slider.h"

class BSpinSlider : public EditorSpinSlider {
    GDCLASS(BSpinSlider, EditorSpinSlider);

public:

    BSpinSlider();
    ~BSpinSlider();
};

#endif // BEDITOR_SPIN_SLIDER_H