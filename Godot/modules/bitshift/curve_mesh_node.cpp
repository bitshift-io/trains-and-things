/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "curve_mesh_node.h"
#include "curve_mesh.h"
#include "util.h"
//#include "bthread_pool.h"
#include "scene/resources/mesh.h"
#include "scene/3d/mesh_instance.h"
#include "scene/3d/physics_body.h"
#include "scene/resources/ray_shape.h"
#include "scene/resources/box_shape.h"
#include "core/core_string_names.h"
	
Ref<BArcLineCurve> BCurveMeshNode::get_local_curve() {
	return local_curve;
}

void BCurveMeshNode::set_local_curve(const Ref<BArcLineCurve>& p_curve) {
	world_curve_dirty = true;
	
	if (!p_curve.is_valid()) {
		local_curve->clear_points();
		return;
	}
		
	local_curve->copy(p_curve);
}

/*
Ref<BCurveMesh> BCurveMeshNode::get_curve_mesh() {
	return curve_mesh;
}

void BCurveMeshNode::set_curve_mesh(const Ref<BCurveMesh>& p_curve_mesh) {
	curve_mesh = p_curve_mesh; 
}*/

MeshInstance* BCurveMeshNode::get_mesh_instance() {
    return mesh_inst;
}

// get the curve in world space
Ref<BArcLineCurve> BCurveMeshNode::BCurveMeshNode::get_world_curve() {
	if (world_curve_dirty)
		_update();
	
	return world_curve;
}

void BCurveMeshNode::_update() {
	if (!world_curve_dirty)
		return;

	world_curve->copy(local_curve);
	world_curve->apply_transform_to_points(get_global_transform());	
	world_curve_dirty = false;
}

void BCurveMeshNode::_notification(int p_what) {
	
    switch (p_what) {
        case NOTIFICATION_PROCESS: {

            // is there a generate_mesh queued?
            if (generate_mesh_queued && !curve_mesh->is_task_running()) {
                generate_mesh_queued = false;
                _generate_mesh();
            }

        } break;

        case NOTIFICATION_TRANSFORM_CHANGED: {
            world_curve_dirty = true;

            if (is_visible_in_tree()) {
                /*
                if (gen_mesh_on_transform_changed) {
                    generate_mesh();
                }*/

                if (get_script_instance())
                    get_script_instance()->call("_transform_changed");
            }
        } break;
    }
	
	/*
	if (p_what == NOTIFICATION_LOCAL_TRANSFORM_CHANGED && is_visible_in_tree()) {	
		if (get_curve().is_valid()) {			
			if (gen_mesh_on_transform_changed) {
				generate_mesh();
			}
		}
		
		if (get_script_instance())
			get_script_instance()->call("_local_transform_changed");
	}*/
}

void BCurveMeshNode::set_mesh_generation_type(int p_mesh_gen_type) {
	mesh_gen_type = (MeshGenerationType)p_mesh_gen_type;
}

int BCurveMeshNode::get_mesh_generation_type() const {
	return mesh_gen_type;
}
/*
void BCurveMeshNode::_generate_mesh_task() {
    if (world_curve_dirty)
        _update();

    curve_mesh->generate_mesh();
/ *
    // convert the mesh from world space to local space
    Ref<Mesh> mesh;
    if (mesh_gen_type == MGT_MESH)
        mesh = curve_mesh->generate_mesh(get_global_transform());
    else if (mesh_gen_type == MGT_BOUNDS)
        mesh = curve_mesh->generate_mesh_from_bounds(get_global_transform());

    if (mesh.is_null())
        return;

    // the mesh that is generated is in world space
    // because we need to raycast into the terrain in world space
    //
    mesh_inst->set_transform(get_global_transform().affine_inverse());
    mesh_inst->set_mesh(mesh);* /
}

void BCurveMeshNode::_generate_mesh_task_complete() {

    if (get_script_instance())
        get_script_instance()->call("_mesh_generated", mesh_inst);
}*/

void BCurveMeshNode::generate_mesh() {
    generate_mesh_queued = true;
}

void BCurveMeshNode::_generate_mesh() {
/*
    // if generate_mesh is running, wait before starting a new task!
    if (curve_mesh->is_task_running()) {
        if (!generate_mesh_call_deferred) {
            call_deferred("_generate_mesh_deferred");
            generate_mesh_call_deferred = true;
        }

        return;
    }*/

    if (world_curve_dirty)
        _update();

    // copy world curve so the game can modify the curve while we generate the mesh!
    Ref<BArcLineCurve> world_curve_copy = Ref<BArcLineCurve>(memnew(BArcLineCurve));
    world_curve_copy->copy(world_curve);

    BCurveMesh::MeshInputData mesh_input_data;
    mesh_input_data.mesh_instance = mesh_inst;
    mesh_input_data.transform = get_global_transform();
    mesh_input_data.mesh_gen_type = (BCurveMesh::MeshGenerationType)mesh_gen_type;
    mesh_input_data.arc_line_curve = world_curve_copy;
    mesh_input_data.curve_deformer = &move_to_ground;
    mesh_input_data.complete_method = std::bind(&BCurveMeshNode::_generate_mesh_complete, this);
    curve_mesh->generate_mesh(mesh_input_data);
}

void BCurveMeshNode::_generate_mesh_complete() {
    if (get_script_instance())
        get_script_instance()->call("_mesh_generated", mesh_inst);

    emit_signal("mesh_generated", mesh_inst);
}

bool BCurveMeshNode::is_generating_mesh() {
    return curve_mesh->is_task_running();
}

/*
void BCurveMeshNode::_generate_collision_task() {

}

void BCurveMeshNode::_generate_collision_task_complete() {
    add_child(static_body);
}
*/

void BCurveMeshNode::generate_collision() {
/*
    // de-parent static_body as the thread will set this up and physics doesnt like changing parenting during
    // in a separate thread
    remove_child(static_body);

    Task* t = Task::Create(&BCurveMeshNode::_generate_collision_task, this);
    t->set_complete(&BCurveMeshNode::_generate_collision_task_complete, this);
    BThreadPool::get_singleton()->add_task(t);*/

    // generate collision

    curve_mesh->generate_collision(static_body, get_global_transform(), Vector3());

}

Array BCurveMeshNode::get_collision_nodes() {
    Array arr;
    arr.append(static_body);
    /*
    int cc = static_body->get_child_count();
    arr.resize(cc);
    for (int i = 0; i < cc; i++)
        arr[i] = static_body->get_child(i);
*/
    return arr;
}

/*
void BCurveMeshNode::set_generate_mesh_on_transform_changed(bool gen_mesh) {
	gen_mesh_on_transform_changed = gen_mesh;
}

bool BCurveMeshNode::get_generate_mesh_on_transform_changed() const {
	return gen_mesh_on_transform_changed;
}*/

Dictionary BCurveMeshNode::closest_distance_to_curve(const Vector3& p_world_pos, float p_distance_max) {
	Transform t = get_global_transform();
	Vector3 local_pos = t.xform_inv(p_world_pos); // convert from world space to local space
    Dictionary d = world_curve->closest_distance_to(local_pos, p_distance_max);
	d["point_on_curve"] = t.xform(Vector3(d["point_on_curve"])); // convert local to world to world space
	return d;
}

Transform BCurveMeshNode::get_global_transform_from_distance_along_curve(float p_offset) {
	Transform t = get_global_transform();
    Transform t_r = get_transform_from_distance_along_curve(p_offset);
	t_r = t * t_r;
	return t_r;
}

Transform BCurveMeshNode::get_transform_from_distance_along_curve(float p_offset) {
    return move_to_ground.modify_transform(world_curve->get_transform_from_distance_along_curve(p_offset));
}

void BCurveMeshNode::_validate_property(PropertyInfo &property) const {
	if (property.name == "curve" || property.name == "curve_mesh") {
		property.usage = 0;
		return;
	}
}

void BCurveMeshNode::_local_curve_changed() {
    world_curve_dirty = true;
}

void BCurveMeshNode::set_instance_template(Node* p_instance_template) {
    curve_mesh->set_instance_template(p_instance_template);
}


// TODO: make this take params?
// This casts a ray along the track to see if it collides with anything
Array BCurveMeshNode::intersect_shape(int p_result_max, const Array& p_exclude_array, uint32_t p_collision_mask) {
    Set<RID> p_exclude;
    for (int i = 0; i < p_exclude_array.size(); ++i) {
        p_exclude.insert(p_exclude_array[i]);
    }

    Vector3 bounds_max = curve_mesh->get_bounds_max();
    Vector3 bounds_min = curve_mesh->get_bounds_min();

    BTerrain* terrain = BTerrain::get_singleton();
    ERR_FAIL_COND_V(!terrain, Array());
    Ref<World> world = terrain->get_world();
    PhysicsDirectSpaceState* space_state = world->get_direct_space_state();
    ERR_FAIL_COND_V(!space_state, Array());

    float path_length = world_curve->get_baked_length();
    float z_size = bounds_max.z - bounds_min.z;
    int required_instances = Math::ceil(path_length / z_size);

    // compute how much stretching needs to occur to each instance to cover the final distance
    // and close the gap
    float stretch = (path_length / z_size) - int(path_length / z_size);// get remainder, fmod?
    stretch *= z_size;
    // at this stage stretch = the length we have to close... so spread this over the number of instances
    stretch = 1.f + stretch / required_instances;

    //float z_offset = -bounds_max.z;

    Vector3 extents = (bounds_max - bounds_min) / 2;
    BoxShape shape;
    shape.set_extents(extents);

    //RayShape ray;
    //ray.set_length(z_size * stretch);

    PhysicsShapeQueryParameters p_shape_query;
    p_shape_query.set_shape_rid(shape.get_rid());

    Array ret;

    for (int inst = 0; inst < required_instances; ++inst) {
        // populate vertex data
        float z = inst * z_size * stretch;

        // should the BCurve cache this when it bakes the points?
        // make it  bake transforms? can we interpolate matricies easy
        Transform t = get_transform_from_distance_along_curve(z);
/*

        Vector3 p_a = t.xform(Vector3(bounds_min.x, bounds_min.y, 0.f));
        Vector3 p_b = t.xform(Vector3(bounds_min.x, bounds_max.y, 0.f));
        Vector3 p_c = t.xform(Vector3(bounds_max.x, bounds_max.y, 0.f));
        Vector3 p_d = t.xform(Vector3(bounds_max.x, bounds_min.y, 0.f));
*/

        p_shape_query.set_transform(t);


        Vector<PhysicsDirectSpaceState::ShapeResult> sr;
        sr.resize(p_result_max);
        int rc = space_state->intersect_shape(p_shape_query.get_shape_rid(), p_shape_query.get_transform(), p_shape_query.get_margin(), sr.ptrw(), sr.size(), p_exclude, p_shape_query.get_collision_mask());

        for (int i = 0; i < rc; i++) {
            Dictionary d;
            d["rid"] = sr[i].rid;
            d["collider_id"] = sr[i].collider_id;
            d["collider"] = sr[i].collider;
            d["shape"] = sr[i].shape;
            ret.append(d);

            if (ret.size() >= p_result_max) {
                return ret;
            }
        }
    }

    return ret;
}

void BCurveMeshNode::_bind_methods() {
    ADD_SIGNAL(MethodInfo("mesh_generated", PropertyInfo(Variant::OBJECT, "mesh_instance")));

    ClassDB::bind_method(D_METHOD("_local_curve_changed"), &BCurveMeshNode::_local_curve_changed);

    ClassDB::bind_method(D_METHOD("is_generating_mesh"), &BCurveMeshNode::is_generating_mesh);

	ClassDB::bind_method(D_METHOD("get_world_curve"), &BCurveMeshNode::get_world_curve);
	
	ClassDB::bind_method(D_METHOD("get_local_curve"), &BCurveMeshNode::get_local_curve);
	ClassDB::bind_method(D_METHOD("set_local_curve"), &BCurveMeshNode::set_local_curve);
	
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "local_curve", PROPERTY_HINT_NONE, "BArcLineCurve", PROPERTY_USAGE_NOEDITOR), "set_local_curve", "get_local_curve");
	
    //ClassDB::bind_method(D_METHOD("get_curve_mesh"), &BCurveMeshNode::get_curve_mesh);
    //ClassDB::bind_method(D_METHOD("set_curve_mesh"), &BCurveMeshNode::set_curve_mesh);
        
    ClassDB::bind_method(D_METHOD("get_mesh_instance"), &BCurveMeshNode::get_mesh_instance);
	
    //ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "curve_mesh", PROPERTY_HINT_NONE, "BCurveMesh", PROPERTY_USAGE_NOEDITOR), "set_curve_mesh", "get_curve_mesh");
	
	ClassDB::bind_method(D_METHOD("get_mesh_generation_type"), &BCurveMeshNode::get_mesh_generation_type);
	ClassDB::bind_method(D_METHOD("set_mesh_generation_type"), &BCurveMeshNode::set_mesh_generation_type);
	
	BIND_CONSTANT(MGT_BOUNDS);
	BIND_CONSTANT(MGT_MESH);
	
	ClassDB::bind_method(D_METHOD("generate_mesh"), &BCurveMeshNode::generate_mesh);
    ClassDB::bind_method(D_METHOD("generate_collision"), &BCurveMeshNode::generate_collision);
	
    //ClassDB::bind_method(D_METHOD("get_generate_mesh_on_transform_changed"), &BCurveMeshNode::get_generate_mesh_on_transform_changed);
    //ClassDB::bind_method(D_METHOD("set_generate_mesh_on_transform_changed"), &BCurveMeshNode::set_generate_mesh_on_transform_changed);
	
	ClassDB::bind_method(D_METHOD("closest_distance_to_curve"), &BCurveMeshNode::closest_distance_to_curve);
	ClassDB::bind_method(D_METHOD("get_global_transform_from_distance_along_curve"), &BCurveMeshNode::get_global_transform_from_distance_along_curve);

    ClassDB::bind_method(D_METHOD("get_collision_nodes"), &BCurveMeshNode::get_collision_nodes);

    ClassDB::bind_method(D_METHOD("set_instance_template", "instance_template"), &BCurveMeshNode::set_instance_template);
    ClassDB::bind_method(D_METHOD("get_transform_from_distance_along_curve", "offset"),&BCurveMeshNode::get_transform_from_distance_along_curve);
    ClassDB::bind_method(D_METHOD("intersect_shape"), &BCurveMeshNode::intersect_shape, DEFVAL(Array()), DEFVAL(0x7FFFFFFF));
}

BCurveMeshNode::BCurveMeshNode() {
	local_curve = Ref<BArcLineCurve>(memnew(BArcLineCurve)); 
	local_curve->local = true; // for debugging!
    local_curve->connect(CoreStringNames::get_singleton()->changed, this, "_local_curve_changed");
	
	world_curve = Ref<BArcLineCurve>(memnew(BArcLineCurve)); 
    curve_mesh = Ref<BCurveMesh>(memnew(BCurveMesh));
	mesh_gen_type = MGT_MESH;
	world_curve_dirty = true;
	
	mesh_inst = memnew(MeshInstance); // should we inherit from mesh inst?
	add_child(mesh_inst);

    static_body = memnew(StaticBody);
    add_child(static_body);
	
	set_notify_local_transform(true);
	set_notify_transform(true);
    //set_generate_mesh_on_transform_changed(true);

    //mutex = Mutex::create();

    move_to_ground.push_along_normal_distance = 1.f; // TODO: expose to editor
    generate_mesh_queued = false;

    set_process(true);
}

BCurveMeshNode::~BCurveMeshNode() {
    //memdelete(mutex);
}
