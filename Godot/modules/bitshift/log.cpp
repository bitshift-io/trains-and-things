 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "log.h"
#include "util.h"
#include "core/io/logger.h"

#define private public
#define protected public
#include "core/os/os.h"
#undef private
#undef protected

BLog *BLog::singleton = NULL;

#define MAX_DEBUG_STRING_LENGTH 32768

class BLogger : public Logger {
public:

    bool in_logv_fn;

    BLogger(): in_logv_fn(false) {}
    virtual ~BLogger() {}

    virtual void logv(const char *p_format, va_list p_list, bool p_err) {
        if (!should_log(p_err)) {
            return;
        }

        // stop terrible recursion
        if (in_logv_fn) {
            return;
        }

        in_logv_fn = true;

        char buffer[MAX_DEBUG_STRING_LENGTH] = {0};
        vsnprintf(buffer, MAX_DEBUG_STRING_LENGTH, p_format, p_list);

        String str(buffer);

        if (BLog::get_singleton()) {
            BLog::get_singleton()->_print(str, p_err ? BLog::ERROR : BLog::PRINT);
        }

        in_logv_fn = false;
    }
};

/*
void BLog::_print_handler(void *p_this, const String &p_string, bool p_error) {
    BLog *log = (BLog *)p_this;
    log->_print(p_string, p_error ? BLog::ERROR : BLog::PRINT);
}*/

void BLog::_print(const String &p_string, Type p_type) {
    BUtil::get_singleton()->log_editor_message(p_string);
    emit_signal("print", p_string, p_type);
}

void BLog::debug(const String &p_string) {
#ifdef DEBUG_ENABLED
    _print(p_string, DEBUG);
#endif
}

void BLog::print(const String &p_string) {
    _print(p_string, PRINT);
}

void BLog::warn(const String &p_string) {
    _print(p_string, WARN);
}

void BLog::error(const String &p_string) {
     _print(p_string, ERROR);
}

void BLog::_bind_methods() {
    ClassDB::bind_method(D_METHOD("print"), &BLog::print);
    ClassDB::bind_method(D_METHOD("debug"), &BLog::debug);
    ClassDB::bind_method(D_METHOD("error"), &BLog::error);
    ClassDB::bind_method(D_METHOD("warn"), &BLog::warn);

    ADD_SIGNAL(MethodInfo("print", PropertyInfo(Variant::STRING, "text"), PropertyInfo(Variant::INT, "type")));

    BIND_ENUM_CONSTANT(DEBUG);
    BIND_ENUM_CONSTANT(PRINT);
    BIND_ENUM_CONSTANT(WARN);
    BIND_ENUM_CONSTANT(ERROR);
}

BLog::BLog() {
    singleton = this;
    ///OS::get_singleton()->add_logger(memnew(BLogger())); // TODO: VULKAN: disabled to see if this is the cause of vulkan crashes - seems to be the problem

    /*
    print_handler.printfunc = _print_handler;
    print_handler.userdata = this;
    add_print_handler(&print_handler);
    */
}

BLog::~BLog() {
    singleton = NULL;
    //remove_print_handler(&print_handler);
}
