
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "modio.h"
#include "util.h"
#include "plugin_mgr.h"
#include "process_node.h"
#include "database.h"
#include "scene/main/http_request.h"
#include "core/io/json.h"
#include "core/io/zip_io.h"
#include "core/method_bind_ext.gen.inc"

BModio *BModio::singleton = NULL;

void BModio::process() {
}

Dictionary BModio::get_metadata_kvp(const Dictionary &p_mod) {
  Dictionary dict;
  if (p_mod.has("metadata_kvp")) {
    Array arr = p_mod["metadata_kvp"];
    for (int i = 0; i < arr.size(); ++i) {
      Dictionary kvp = arr[i];
      dict[kvp["metakey"]] = kvp["metavalue"];
    }
  }

  return dict;
}

// https://docs.mod.io/#pagination
void BModio::request_mods(Dictionary p_params) {
  String url = String("https://api.mod.io/v1/games/") + String::num(game_id) + String("/mods?api_key=") + api_key;

  if (p_params.has("page")) {
    int page = p_params["page"];
    url += "&_limit=" + String::num(page_size);
    url += "&_offset=" + String::num(page * page_size);
  }

  if (p_params.has("query")) {
    String query = p_params["query"];
    url += "&_q=" + query;
  }

  make_http_request(url, this, "_http_mods_completed");
}

void BModio::_http_mods_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request) {
  switch (p_status) {

      case HTTPRequest::RESULT_CANT_RESOLVE: {
        //template_list_state->set_text(TTR("Can't resolve."));
      } break;
      case HTTPRequest::RESULT_BODY_SIZE_LIMIT_EXCEEDED:
      case HTTPRequest::RESULT_CONNECTION_ERROR:
      case HTTPRequest::RESULT_CHUNKED_BODY_SIZE_MISMATCH:
      case HTTPRequest::RESULT_SSL_HANDSHAKE_ERROR:
      case HTTPRequest::RESULT_CANT_CONNECT: {
        //template_list_state->set_text(TTR("Can't connect."));
      } break;
      case HTTPRequest::RESULT_NO_RESPONSE: {
        //template_list_state->set_text(TTR("No response."));
      } break;
      case HTTPRequest::RESULT_REQUEST_FAILED: {
        //template_list_state->set_text(TTR("Request Failed."));
      } break;
      case HTTPRequest::RESULT_REDIRECT_LIMIT_REACHED: {
        //template_list_state->set_text(TTR("Redirect Loop."));
      } break;
      default: {
        if (p_code != 200) {
          //template_list_state->set_text(TTR("Failed:") + " " + itos(p_code));
        } else {

          Variant js;
          String errs;
          int errl;
          String str_data = BUtil::to_string(p_data);
          Error err = JSON::parse(str_data, js, errs, errl);
          if (err != OK) {
            print_line(str_data);
            ERR_PRINT(errs);
          }

          // https://docs.mod.io/#mods
          mods = js;

          // some extra settings to help simplify pagination
          mods["result_page"] = int(mods["result_offset"]) / page_size;
          mods["page_size"] = page_size;
          mods["page_count"] = int(Math::ceil(float(mods["result_total"]) / float(page_size)));
          emit_signal("on_request_mods", mods);
        }
      } break;
  }

  delete_complete_http_requests();
}

void BModio::_http_download_1_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request) {

  switch (p_status) {

    case HTTPRequest::RESULT_CANT_RESOLVE: {
      //template_list_state->set_text(TTR("Can't resolve."));
    } break;
    case HTTPRequest::RESULT_BODY_SIZE_LIMIT_EXCEEDED:
    case HTTPRequest::RESULT_CONNECTION_ERROR:
    case HTTPRequest::RESULT_CHUNKED_BODY_SIZE_MISMATCH:
    case HTTPRequest::RESULT_SSL_HANDSHAKE_ERROR:
    case HTTPRequest::RESULT_CANT_CONNECT: {
      //template_list_state->set_text(TTR("Can't connect."));
    } break;
    case HTTPRequest::RESULT_NO_RESPONSE: {
      //template_list_state->set_text(TTR("No response."));
    } break;
    case HTTPRequest::RESULT_REQUEST_FAILED: {
      //template_list_state->set_text(TTR("Request Failed."));
    } break;
    case HTTPRequest::RESULT_REDIRECT_LIMIT_REACHED: {
      //template_list_state->set_text(TTR("Redirect Loop."));
    } break;
    default: {
      if (p_code != 200) {
        //template_list_state->set_text(TTR("Failed:") + " " + itos(p_code));
      } else {

        Variant js;
        String errs;
        int errl;
        String str_data = BUtil::to_string(p_data);
        Error err = JSON::parse(str_data, js, errs, errl);
        if (err != OK) {
          print_line(str_data);
          ERR_PRINT(errs);
        }

        //print_line(str_data);

        // https://docs.mod.io/#mods
        Dictionary mod = js;

        mod["download_progress"] = 0;
        int mod_id = mod["id"];
        mod_downloads[mod_id] = mod;

        Dictionary modfile = mod["modfile"];
        Dictionary download = modfile["download"];

        // now we have the mod data stored in mod_downloads, lets now start downloading the file
        String download_url = download["binary_url"];
        String filename = modfile["filename"];
        Vector<Variant> binds;
        binds.push_back(mod);
        HTTPRequest *http_request = make_http_request(download_url, this, "_http_download_2_completed", binds);
        if (http_request) {
          String download_file = cache_dir.plus_file(filename);
          http_request->set_download_file(download_file);
        }
      }
    } break;
  }

  delete_complete_http_requests();
}

void BModio::_http_download_2_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request, Dictionary p_mod) {

  switch (p_status) {

    case HTTPRequest::RESULT_CANT_RESOLVE: {
      //template_list_state->set_text(TTR("Can't resolve."));
    } break;
    case HTTPRequest::RESULT_BODY_SIZE_LIMIT_EXCEEDED:
    case HTTPRequest::RESULT_CONNECTION_ERROR:
    case HTTPRequest::RESULT_CHUNKED_BODY_SIZE_MISMATCH:
    case HTTPRequest::RESULT_SSL_HANDSHAKE_ERROR:
    case HTTPRequest::RESULT_CANT_CONNECT: {
      //template_list_state->set_text(TTR("Can't connect."));
    } break;
    case HTTPRequest::RESULT_NO_RESPONSE: {
      //template_list_state->set_text(TTR("No response."));
    } break;
    case HTTPRequest::RESULT_REQUEST_FAILED: {
      //template_list_state->set_text(TTR("Request Failed."));
    } break;
    case HTTPRequest::RESULT_REDIRECT_LIMIT_REACHED: {
      //template_list_state->set_text(TTR("Redirect Loop."));
    } break;
    default: {
      if (p_code != 200) {
        //template_list_state->set_text(TTR("Failed:") + " " + itos(p_code));
      } else {

        String install_dir = "Mods";
        if (p_mod.has("metadata_kvp")) {
          Dictionary metadata = p_mod["metadata_kvp"];
          if (metadata.has("install_dir")) {
            install_dir = metadata["install_dir"];
          }
        }

        HTTPRequest *request = Object::cast_to<HTTPRequest>(http_request);
        String path = request->get_download_file();
				bool ret = _install_from_file(path, install_dir, false);
        if (ret) {
					// Clean up downloaded file.
					DirAccessRef da = DirAccess::create(DirAccess::ACCESS_FILESYSTEM);
					Error err = da->remove(path);
					if (err != OK) {
						//EditorNode::get_singleton()->add_io_error(TTR("Cannot remove temporary file:") + "\n" + path + "\n");
            ERR_PRINT("Cannot remove temporary file:" + path + "\n");
					}

          emit_signal("on_download_mod", p_mod); // mod successfully installed!
				} else {
					//EditorNode::get_singleton()->add_io_error(vformat(TTR("Templates installation failed.\nThe problematic templates archives can be found at '%s'."), path));
          ERR_PRINT(vformat("Mod installation failed. The problematic archive can be found at '%s'.", path));
				}
      }
    } break;
  }

  delete_complete_http_requests();
}

// ganked from export_template_manager.cpp
bool BModio::_install_from_file(const String &p_file, const String &p_install_dir, bool p_use_progress) {

	// unzClose() will take care of closing the file stored in the unzFile,
	// so we don't need to `memdelete(fa)` in this method.
	FileAccess *fa = NULL;
	zlib_filefunc_def io = zipio_create_io_from_file(&fa);

	unzFile pkg = unzOpen2(p_file.utf8().get_data(), &io);
	if (!pkg) {
    ERR_PRINT("Can't open export templates zip.");
		//EditorNode::get_singleton()->show_warning(TTR("Can't open export templates zip."));
		return false;
	}
	int ret = unzGoToFirstFile(pkg);

	int fc = 0; //count them and find version
  /*
	String version;
	String contents_dir;

	while (ret == UNZ_OK) {

		unz_file_info info;
		char fname[16384];
		ret = unzGetCurrentFileInfo(pkg, &info, fname, 16384, NULL, 0, NULL, 0);

		String file = fname;

		if (file.ends_with("version.txt")) {

			Vector<uint8_t> data;
			data.resize(info.uncompressed_size);

			//read
			unzOpenCurrentFile(pkg);
			ret = unzReadCurrentFile(pkg, data.ptrw(), data.size());
			unzCloseCurrentFile(pkg);

			String data_str;
			data_str.parse_utf8((const char *)data.ptr(), data.size());
			data_str = data_str.strip_edges();

			// Version number should be of the form major.minor[.patch].status[.module_config]
			// so it can in theory have 3 or more slices.
			if (data_str.get_slice_count(".") < 3) {
        ERR_PRINT(vformat(TTR("Invalid version.txt format inside templates: %s."), data_str));
				//EditorNode::get_singleton()->show_warning(vformat(TTR("Invalid version.txt format inside templates: %s."), data_str));
				unzClose(pkg);
				return false;
			}

			version = data_str;
			contents_dir = file.get_base_dir().trim_suffix("/").trim_suffix("\\");
		}

		if (file.get_file().size() != 0) {
			fc++;
		}

		ret = unzGoToNextFile(pkg);
	}

	if (version == String()) {
    ERR_PRINT(TTR("No version.txt found inside templates."));
		//EditorNode::get_singleton()->show_warning(TTR("No version.txt found inside templates."));
		unzClose(pkg);
		return false;
	}

  // TODO: move this to project, also should change the plugin manager to just read a list of dirs from project.godot also!
	String template_path = "res://Mods"; //EditorSettings::get_singleton()->get_templates_dir().plus_file(version);
  /*
	DirAccessRef d = DirAccess::create(DirAccess::ACCESS_FILESYSTEM);
	Error err = d->make_dir_recursive(template_path);
	if (err != OK) {
    ERR_PRINT(TTR("Error creating path for templates:") + "\n" + template_path);
		//EditorNode::get_singleton()->show_warning(TTR("Error creating path for templates:") + "\n" + template_path);
		unzClose(pkg);
		return false;
	}
  * /

	ret = unzGoToFirstFile(pkg);

  /*
	EditorProgress *p = NULL;
	if (p_use_progress) {
		p = memnew(EditorProgress("ltask", TTR("Extracting Export Templates"), fc));
	}
  

	fc = 0;
*/
  // TODO: move this to project, also should change the plugin manager to just read a list of dirs from project.godot also!
	String template_path = "res://" + p_install_dir;

	while (ret == UNZ_OK) {

		//get filename
		unz_file_info info;
		char fname[16384];
		unzGetCurrentFileInfo(pkg, &info, fname, 16384, NULL, 0, NULL, 0);

		String file_path(String(fname).simplify_path());

		String file = file_path.get_file();

		if (file.size() == 0) {
			ret = unzGoToNextFile(pkg);
			continue;
		}

		Vector<uint8_t> data;
		data.resize(info.uncompressed_size);

		//read
		unzOpenCurrentFile(pkg);
		unzReadCurrentFile(pkg, data.ptrw(), data.size());
		unzCloseCurrentFile(pkg);

		String base_dir = file_path.get_base_dir().trim_suffix("/");
/*
		if (base_dir != contents_dir && base_dir.begins_with(contents_dir)) {
			base_dir = base_dir.substr(contents_dir.length(), file_path.length()).trim_prefix("/");
			file = base_dir.plus_file(file);

			DirAccessRef da = DirAccess::create(DirAccess::ACCESS_FILESYSTEM);
			ERR_CONTINUE(!da);

			String output_dir = template_path.plus_file(base_dir);

			if (!DirAccess::exists(output_dir)) {
				Error mkdir_err = da->make_dir_recursive(output_dir);
				ERR_CONTINUE(mkdir_err != OK);
			}
		}
    */

    /*
		if (p) {
			p->step(TTR("Importing:") + " " + file, fc);
		}*/

		String to_write = template_path.plus_file(file);
		FileAccessRef f = FileAccess::open(to_write, FileAccess::WRITE);

		if (!f) {
			ret = unzGoToNextFile(pkg);
			fc++;
			ERR_CONTINUE_MSG(true, "Can't open file from path '" + String(to_write) + "'.");
		}

		f->store_buffer(data.ptr(), data.size());

#ifndef WINDOWS_ENABLED
		FileAccess::set_unix_permissions(to_write, (info.external_fa >> 16) & 0x01FF);
#endif

    /* just call BPLuginMgr::scan_directory(const String& p_directory); in gdscript 
    // register the mod with plugin_mgr
    if (to_write.ends_with(".pck")) {
      String resource_path = to_write.replace(".pck", "");
      if (!BPluginMgr::get_singleton()->add_plugin(resource_path))) {
        ERR_PRINT("Failed to load plugin: " + resource_path);
      }
    }
    */

		ret = unzGoToNextFile(pkg);
		fc++;
	}

  /*
	if (p) {
		memdelete(p);
	}*/

	unzClose(pkg);

	//_update_template_list();
	return true;
}

HTTPRequest *BModio::make_http_request(const String &p_url, Object * p_object, const StringName &p_to_method, const Vector<Variant> &p_binds) {
  HTTPRequest *http_request = memnew(HTTPRequest);
  Object *obj = http_request;
  Vector<Variant> variants;
  variants.push_back(obj);
  for (int i = 0; i < p_binds.size(); ++i) {
    variants.push_back(p_binds[i]);
  }

	http_request->connect("request_completed", p_object, p_to_method, variants);
	http_request->set_use_threads(true);
  process_node->add_child(http_request);

	Error err = http_request->request(p_url);
	if (err != OK) {
    http_request->queue_delete();
		return nullptr;
	}

  http_requests.push_back(http_request);
  return http_request;
}

void BModio::delete_complete_http_requests() {
  for (List<HTTPRequest *>::Element *E = http_requests.front(); E; E = E->next()) {
    HTTPRequest *http_request = E->get();
    auto status = http_request->get_http_client_status();
    if (status == HTTPClient::STATUS_DISCONNECTED) {
      http_requests.erase(E); // is this safe in the loop?
      http_request->queue_delete();
    }
  }
}

// install/update the mod
void BModio::download_mod(int p_id) {
  // request the info for this mod from mod io
  String url = String("https://api.mod.io/v1/games/") + String::num(game_id) + String("/mods/") + String::num(p_id) + String("?api_key=") + api_key;
  make_http_request(url, this, "_http_download_1_completed");
}

void BModio::set_process(bool p_processing) {
  process_node->set_process(p_processing);
}

void BModio::_bind_methods() {
  ClassDB::bind_method("_http_mods_completed", &BModio::_http_mods_completed);
  ClassDB::bind_method("_http_download_1_completed", &BModio::_http_download_1_completed);
  ClassDB::bind_method("_http_download_2_completed", &BModio::_http_download_2_completed);

  ClassDB::bind_method(D_METHOD("set_process"), &BModio::set_process);
  ClassDB::bind_method(D_METHOD("request_mods"), &BModio::request_mods);
  ClassDB::bind_method(D_METHOD("download_mod"), &BModio::download_mod);
  ClassDB::bind_method(D_METHOD("get_metadata_kvp"), &BModio::get_metadata_kvp);

  ADD_SIGNAL(MethodInfo("on_request_mods"));
  ADD_SIGNAL(MethodInfo("on_download_mod"));
}

BModio::BModio() {
  singleton = this;

  api_key = String(BDatabase::get_singleton()->get_value("bitshift/modio/api_key", ""));
  game_id = BDatabase::get_singleton()->get_value("bitshift/modio/game_id", "");
  page_size = BDatabase::get_singleton()->get_value("bitshift/modio/page_size", 50);

  cache_dir = GLOBAL_DEF("bitshift/cache_dir", "res://.cache");
  BUtil::make_dir(cache_dir);

  process_node = memnew(ProcessNode(&BModio::process, this));
}

BModio::~BModio() {
}
