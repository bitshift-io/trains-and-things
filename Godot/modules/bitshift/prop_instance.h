/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BPROP_INSTANCE_H
#define BPROP_INSTANCE_H

#include "scene/3d/collision_polygon.h"
#include "scene/resources/packed_scene.h"
#include "lod_group.h"

class BPolygonArea;
class Spatial;
class MultiMeshInstance;

/**
    Scatter props over terrain inside a BPolygonArea (which must be its parent!)
*/

class BPropInstance : public BLODGroup {

    GDCLASS(BPropInstance, BLODGroup)

protected:

    bool enabled_in_game;

    bool use_multi_mesh_instance;

    Ref<PackedScene> scene;

    BLODGroup* spatial_node;

    int instance_count;
    int random_seed;

    uint64_t _seed;

    float rotation_y_min;
    float rotation_y_max;

    float scale_min;
    float scale_max;

    Color color_min;
    Color color_max;

    bool generating;
    bool dirty;

    void make_dirty();

    //Vector<Transform> instance_transforms;
    Vector<MultiMeshInstance*> mmi_nodes;
    Vector<Spatial*> prop_instances;
    Spatial* orig_instance;

    static void _bind_methods();
    void _notification( int p_what);

    void generate_task();
    void generate_task_complete();
    void generate();

    float get_rand_range(float p_from, float p_to);
    bool point_in_poly(const Vector<Point2>& p_poly_points, const Point2& p_point);

    void _warning_changed(Node* p_node);

    BPolygonArea* get_polygon_area();

    virtual void add_child_notify(Node *p_child);

public:

    void set_color_min(const Color& p_value);
    Color get_color_min();

    void set_color_max(const Color& p_value);
    Color get_color_max();

    void set_use_multi_mesh_instance(bool b_enabled);
    bool get_use_multi_mesh_instance();

    void set_enabled_in_game(bool b_enabled);
    bool get_enabled_in_game();

    void set_random_seed(int p_seed);
    int get_random_seed();

    void set_instance_count(int p_count);
    int get_instance_count();

    void set_rotation_y_min(float p_value);
    float get_rotation_y_min();

    void set_rotation_y_max(float p_value);
    float get_rotation_y_max();

    void set_scale_min(float p_value);
    float get_scale_min();

    void set_scale_max(float p_value);
    float get_scale_max();

    void set_scene(const Ref<PackedScene>& p_scene);
    Ref<PackedScene> get_scene();


    BPropInstance();
    ~BPropInstance();
};

#endif // BPROP_INSTANCE_H
