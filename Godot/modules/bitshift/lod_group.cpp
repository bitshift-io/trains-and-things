/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "lod_group.h"
#include "util.h"
#include "database.h"
#include "core/core_string_names.h"
#include "scene/3d/camera.h"
#include "core/engine.h"
#include "scene/3d/mesh_instance.h"

void BLODGroup::set_enabled_in_editor(bool b_enabled) {
    enabled_in_editor = b_enabled;
    _change_notify();
}

bool BLODGroup::get_enabled_in_editor() const {
    return enabled_in_editor;
}

void BLODGroup::set_lod_range_name(int p_group, String p_name) {
    ERR_FAIL_INDEX(p_group, lod_ranges.size());
    lod_ranges.write[p_group].name = p_name;
    _change_notify();
}

String BLODGroup::get_lod_range_name(int p_group) const {
    ERR_FAIL_INDEX_V(p_group, lod_ranges.size(), String());
    return lod_ranges[p_group].name;
}

void BLODGroup::set_lod_range_near(int p_group, float p_range) {
    ERR_FAIL_INDEX(p_group, lod_ranges.size());

    // change change the first nearest!
    if (p_group == 0) {
        p_range = 0;
    }

    lod_ranges.write[p_group].near = p_range;

    // if we change the near, we are also changing the far of the previous (if it exists!)
    if (p_group > 0) {
        lod_ranges.write[p_group - 1].far = p_range;
    }
    _change_notify();
}

float BLODGroup::get_lod_range_near(int p_group) const {
    ERR_FAIL_INDEX_V(p_group, lod_ranges.size(), 0);
    return lod_ranges[p_group].near;
}

void BLODGroup::set_lod_range_far(int p_group, float p_range) {
    ERR_FAIL_INDEX(p_group, lod_ranges.size());
    lod_ranges.write[p_group].far = p_range;

    // if we change the far, we are also changing the near of the next
    if (p_group < (lod_ranges.size() - 1)) {
        lod_ranges.write[p_group + 1].near = p_range;
    }
    _change_notify();
}

float BLODGroup::get_lod_range_far(int p_group) const {
    ERR_FAIL_INDEX_V(p_group, lod_ranges.size(), 0);
    return lod_ranges[p_group].far;
}

void BLODGroup::set_distance(float p_d) {
    // cant change distance!
}

float BLODGroup::get_distance() {
    return distance;
}

void BLODGroup::set_current_lod_idx(int p_d) {
    // no!
}

int BLODGroup::get_current_lod_idx() {
    return current_lod_idx;
}

float lod_graph(float x) {
    return x;
}

void BLODGroup::set_lod_range_size(int p_count) {
    // cant have zero lod groups
    if (p_count < 1)
        p_count = 1;

    lod_ranges.resize(p_count);

    // auto setup some suitable values
    // lets use a suitable lod curve that brings the lod switching cloer to the camera
    float far_dist = 1000.0f; // 1km spread lods out evenly over that distance
    float r = 1.0 / p_count;
    for (int i = 0; i < lod_ranges.size(); ++i) {
        lod_ranges.write[i].name = "*LOD" + itos(i);
        lod_ranges.write[i].near = lod_graph(r * i) * far_dist;
        lod_ranges.write[i].far = lod_graph(r * (i + 1)) * far_dist;
        //lod_ranges.write[i].child_node = nullptr;
    }
    lod_ranges.write[lod_ranges.size() - 1].far = -1; // infinity!

    _change_notify();
}

int BLODGroup::get_lod_range_size() const {
    return lod_ranges.size();
}

void BLODGroup::set_aabb(const AABB &p_aabb) {
    aabb = p_aabb;

    // debugging
    if (debug) {
        if (debug_aabb_mi) {
            debug_aabb_mi->queue_delete();
        }
        debug_aabb_mi = BUtil::get_singleton()->aabb_to_mesh(aabb);
        add_child(debug_aabb_mi);
    }
}

AABB BLODGroup::get_aabb() {
    return aabb;
}

/*
void BLODGroup::set_aabb_offset(const Vector3& p_pos) {
    aabb.position = p_pos;
}

Vector3 BLODGroup::get_aabb_offset() {
    return aabb.position;
}

void BLODGroup::set_aabb_radius(const float& p_radius) {
    aabb.size = Vector3(p_radius, p_radius, p_radius);
}

float BLODGroup::get_aabb_radius() {
    aabb.size.x;
}*/

void BLODGroup::_validate_property(PropertyInfo &property) const {
    if (property.name.begins_with("lod_range_") && !property.name.begins_with("lod_range_size")) {
        int slice_count = property.name.get_slice_count("_");
        int index = property.name.get_slicec('_', slice_count - 1).to_int() - 1;
        if (index >= lod_ranges.size()) {
            property.usage = 0;
            return;
        }
    }
}

void BLODGroup::add_child_notify(Node *p_child) {
    /*
    for (int i = 0; i < lod_ranges.size(); ++i) {
        const LODRange& lod_range = lod_ranges[i];
        if (lod_range.child_node_name == p_child->get_name()) {
            lod_ranges.write[i].child_node = Object::cast_to<Spatial>(p_child);
        }
    }*/
}

void BLODGroup::remove_child_notify(Node *p_child) {
    /*
    for (int i = 0; i < lod_ranges.size(); ++i) {
        const LODRange& lod_range = lod_ranges[i];
        if (lod_range.child_node == p_child) {
            lod_ranges.write[i].child_node = nullptr;
        }
    }*/
}

void BLODGroup::move_child_notify(Node *p_child) {

}

void BLODGroup::_update() {
    if (Engine::get_singleton()->is_editor_hint() && !enabled_in_editor) {
        return;
    }

    // disabled lod means always use LOD0
    bool lod_enabled = BDatabase::get_singleton()->get_value("bitshift/lod/enabled", true);
    if (!lod_enabled) {
        apply_lod(0);
        return;
    }

    Camera *c = nullptr;
    if (Engine::get_singleton()->is_editor_hint()) {
        c = BUtil::get_singleton()->get_editor_camera(this);
    } else {
        c = get_viewport()->get_camera(); //BUtil::get_singleton()->get_top_viewport_with_camera(this);
    }
    if (!c) {
        return;
    }

    float oldDistance = distance;

    //
    // we convert the AABB to a sphere
    //
    float radius = aabb.get_longest_axis_size();
    Transform cam_xform = c->get_global_transform();
    Vector3 origin = get_global_transform().origin + aabb.position;
    Vector3 camPos = cam_xform.origin;
    distance = MAX(0.f, (origin - camPos).length() - radius); // TODO: optimise by using length_squared
    
    // dont bother doing anything if the camera hasn't moved
    // TODO: make a epsilon value here so it waits for camera to move x units before testing again?
    if (Math::is_equal_approx(oldDistance, distance)) {
        return;
    }

    if (Engine::get_singleton()->is_editor_hint()) {
        _change_notify();
    }

    //float far = c->get_zfar();
    //float near = c->get_znear();

    // global lod adjustment, +ve numbers will push the lods back better for higher graphics
    // -ve numbers will brings the lods forward for potato computers
    int distance_offset = BDatabase::get_singleton()->get_value("bitshift/lod/distance_offset", 0);
    float lod_distance = MAX(0, distance - distance_offset);

    // TODO: hysteresis support: https://en.wikipedia.org/wiki/Hysteresis
    // search godot code for hysteresis_decision
    // keep a history and have a threshold - should be pretty easy!

    // TODO: ability to stop processing of hidden children, such as particle systems?

    String name = get_name(); // to help debug

    bool lod_applied = false;
    for (int i = 0; i < lod_ranges.size(); ++i) {
        const LODRange& lod_range = lod_ranges[i];
        if (lod_distance >= lod_range.near && lod_distance < lod_range.far) {
            apply_lod(i);
            lod_applied = true;
            return;
        }
    }

    // check for the infinite lod
    if (!lod_applied && lod_ranges.size() && lod_ranges[lod_ranges.size() - 1].far < 0) {
        apply_lod(lod_ranges.size() - 1);
    }
    else {
        apply_lod(-1); // everything is hidden!
    }
}

void BLODGroup::apply_lod(int p_lod_range_idx) {
    // no change to LOD level from previous call, so bail
    if (current_lod_idx == p_lod_range_idx) {
        return;
    }

    current_lod_idx = p_lod_range_idx;

    if (Engine::get_singleton()->is_editor_hint()) {
        _change_notify();
    }

    for (int i = 0; i < lod_ranges.size(); ++i) {
        const LODRange& lod_range = lod_ranges[i];
        Array lodNodes = BUtil::get_singleton()->find_children(this, lod_range.name, true);
        bool visible = (i == p_lod_range_idx);
        for (int c = 0; c < lodNodes.size(); ++c) {
            Node* child = lodNodes[c];
            Spatial *s = Object::cast_to<Spatial>(child);
            if (s)
                s->set_visible(visible);
        }
    }

    emit_signal("apply_lod", p_lod_range_idx);
}

// reset lod settings to force LOD to be reapplied next update
void BLODGroup::reapply_lod() {
    current_lod_idx = -2;
    distance = 0;
}

void BLODGroup::_notification(int p_what) {

    switch (p_what) {
        case NOTIFICATION_READY: {

            lod_scale = BDatabase::get_singleton()->get_value("bitshift/lod/scale", 1.0);
            lod_bias = BDatabase::get_singleton()->get_value("bitshift/lod/bias", 0.0);

        /*
            // find children
            for (int i = 0; i < lod_ranges.size(); ++i) {
                const LODRange& lod_range = lod_ranges[i];

                for (int c = 0; c < get_child_count(); ++c) {
                    Node* child = get_child(c);
                    if (lod_range.child_node_name == child->get_name()) {
                        lod_ranges.write[i].child_node = Object::cast_to<Spatial>(child);
                    }
                }
            }*/

        } break;

        case NOTIFICATION_PROCESS: {
            _update();
        } break;
    }
}

void BLODGroup::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_distance", "distance"), &BLODGroup::set_distance);
    ClassDB::bind_method(D_METHOD("get_distance"), &BLODGroup::get_distance);
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "distance", PROPERTY_HINT_NONE, "float"), "set_distance", "get_distance");

    ClassDB::bind_method(D_METHOD("set_current_lod_idx", "idx"), &BLODGroup::set_current_lod_idx);
    ClassDB::bind_method(D_METHOD("get_current_lod_idx"), &BLODGroup::get_current_lod_idx);
    ADD_PROPERTY(PropertyInfo(Variant::INT, "current_lod_idx", PROPERTY_HINT_NONE, "int"), "set_current_lod_idx", "get_current_lod_idx");

    ClassDB::bind_method(D_METHOD("set_enabled_in_editor", "enabled"), &BLODGroup::set_enabled_in_editor);
    ClassDB::bind_method(D_METHOD("get_enabled_in_editor"), &BLODGroup::get_enabled_in_editor);
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled_in_editor"), "set_enabled_in_editor", "get_enabled_in_editor");

    ClassDB::bind_method(D_METHOD("set_lod_range_size", "size"), &BLODGroup::set_lod_range_size);
    ClassDB::bind_method(D_METHOD("get_lod_range_size"), &BLODGroup::get_lod_range_size);

    ClassDB::bind_method(D_METHOD("set_lod_range_near", "group", "range"), &BLODGroup::set_lod_range_near);
    ClassDB::bind_method(D_METHOD("get_lod_range_near", "group"), &BLODGroup::get_lod_range_near);

    ClassDB::bind_method(D_METHOD("set_lod_range_far", "group", "range"), &BLODGroup::set_lod_range_far);
    ClassDB::bind_method(D_METHOD("get_lod_range_far", "group"), &BLODGroup::get_lod_range_far);

    ClassDB::bind_method(D_METHOD("set_lod_range_name", "group", "node_name"), &BLODGroup::set_lod_range_name);
    ClassDB::bind_method(D_METHOD("get_lod_range_name", "group"), &BLODGroup::get_lod_range_name);

    ClassDB::bind_method(D_METHOD("set_aabb", "aabb"), &BLODGroup::set_aabb);
    ClassDB::bind_method(D_METHOD("get_aabb"), &BLODGroup::get_aabb);
    ADD_PROPERTY(PropertyInfo(Variant::AABB, "aabb"), "set_aabb", "get_aabb");

    /*
    ClassDB::bind_method(D_METHOD("set_aabb_offset", "position"), &BLODGroup::set_aabb_offset);
    ClassDB::bind_method(D_METHOD("get_aabb_offset"), &BLODGroup::get_aabb_offset);
    ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "aabb_offset"), "set_aabb_offset", "get_aabb_offset");

    ClassDB::bind_method(D_METHOD("set_aabb_radius", "radius"), &BLODGroup::set_aabb_radius);
    ClassDB::bind_method(D_METHOD("get_aabb_radius"), &BLODGroup::get_aabb_radius);
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "aabb_radius"), "set_aabb_radius", "get_aabb_radius");
    */

    ADD_GROUP("LOD Range", "lod_range_");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "lod_range_size", PROPERTY_HINT_RANGE, "0," + itos(MAX_LOD_RANGES) + ",1"), "set_lod_range_size", "get_lod_range_size");
    for (int i = 0; i < MAX_LOD_RANGES; i++) {
         ADD_PROPERTYI(PropertyInfo(Variant::REAL, "lod_range_near_" + itos(i + 1)), "set_lod_range_near", "get_lod_range_near", i);
         ADD_PROPERTYI(PropertyInfo(Variant::REAL, "lod_range_far_" + itos(i + 1)), "set_lod_range_far", "get_lod_range_far", i);
         ADD_PROPERTYI(PropertyInfo(Variant::STRING, "lod_range_name_" + itos(i + 1)), "set_lod_range_name", "get_lod_range_name", i);
    }

    BIND_CONSTANT(MAX_LOD_RANGES);

    ADD_SIGNAL(MethodInfo("apply_lod"));
}

BLODGroup::BLODGroup() {
    debug = false; // false;
    debug_aabb_mi = nullptr;
    enabled_in_editor = false;
    reapply_lod();
    aabb = AABB();
    set_lod_range_size(1);
    set_process(true);
}

BLODGroup::~BLODGroup() {
}
