/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BUTIL_H
#define BUTIL_H

//#include "core/image.h"
//#include "reference.h"
//#include "self_list.h"
//#include "func_ref.h"
//#include "itch_io.h"
//#include "com_err.h/func_ref.h"
#include "scene/main/node.h"
#include "core/os/dir_access.h"

class FuncRef;
class BTerrain;
class BWater;
class ArrayMesh;
class CanvasItem;
class Spatial;
class TextureButton;
class BBoundaryMap;
class Timer;
class SceneTree;
class RichTextLabel;
class Camera;
class MeshInstance;

/**
	@author Fabian Mathews <supagu@gmail.com>
*/

class BUtil : public Reference {

	GDCLASS(BUtil,Reference)

	//ItchIo itch;

	static BUtil *singleton;

    Timer* demo_timer;

    Camera* editor_camera;

    String resource_path;
        
protected:

	static void _bind_methods();
        
    bool _is_class_name(Node* node, const String& class_name, const String& scriptClassName);
    void _find_children_by_class_name(Node* node, const String& class_name, bool recursive, const String& scriptClassName, Array& results);

    Node *_find_child(Node *p_node, const String &p_mask, bool p_recursive, Node* p_parent, int p_depth) const;
    void _find_children(Node *p_node, const String &p_mask, bool p_recursive, Node* p_parent, int p_depth, Array& results) const;

    void _on_demo_expire();
    void _add_demo_timer();

    void _visit_nodes(Node* p_node, FuncRef& p_func);
    void _visit_children(Node* p_node, FuncRef& p_func);

    void _get_branch_as_array(Node *root, Array &result);
        
public:

    int get_rich_text_label_height(Control* p_rich_text);

    AABB get_aabb_from_points_2d(const Array& p_point_list);
    AABB get_aabb_from_vec_points_2d(const Vector<Point2>& p_point_list);
    MeshInstance* aabb_to_mesh(const AABB& p_aabb);

	String get_resource_dir() const;
	//String get_username();

    bool is_child_of(Node* possible_child, Node* possible_parent);
    bool is_class_name(Node* node, String class_name);
    bool is_same_class(Node *p_node_a, Node *p_node_b);
    
	Node* find_parent_by_class_name(Node* node, String class_name);
	Array find_children_by_class_name(Node* node, String class_name, bool recursive = true);
	Node* find_first_child_by_class_name(Node* node, String class_name, bool recursive = true);
	void delete_children(Node* node);
    void queue_delete_children(Node* node);
    void set_children_visible(Node* node, bool visible);

    Node *find_child(Node *p_node, const String &p_mask, bool p_recursive = true) const;

    Array find_children(Node *p_node, const String &p_mask, bool p_recursive = true) const;

    Ref<ArrayMesh> create_tessellated_quad(int resolution);

    BTerrain *get_terrain();
    BWater *get_water();
    BBoundaryMap *get_boundary_map();

    bool set_click_mask_from_normal_alpha(Control* p_texture_button);
    void remove_from_parent(Node* node);

    void log_editor_message(const String& msg);

    SceneTree* get_scene_tree();

    String url_encode(const String& str);

    String camelcase_to_underscore(const String& str);

    Camera* get_editor_camera(Node* node);
    //Camera *get_top_viewport_with_camera(Node *p_node);

    BUtil();
    ~BUtil();

    static BUtil *get_singleton() { return singleton; }

    static inline Vector3 to_vector3(const Color& colour) {
            return Vector3(colour.r, colour.g, colour.b);
    }

    static inline Color to_colour(const Vector3& vector) {
        return Color(vector.x, vector.y, vector.z);
    }
	
    static inline bool is_equal_approx(const Vector3& a, const Vector3& b) {
            return Math::is_equal_approx(a.x, b.x) && Math::is_equal_approx(a.y, b.y) && Math::is_equal_approx(a.z, b.z);
    }

    static inline bool is_equal_approx(const Color& a, const Color& b) {
        return Math::is_equal_approx(a.r, b.r) && Math::is_equal_approx(a.g, b.g) && Math::is_equal_approx(a.b, b.b) && Math::is_equal_approx(a.a, b.a);
    }

    static inline bool is_equal_approx(const Quat& a, const Quat& b) {
        return Math::is_equal_approx(a.x, b.x) && Math::is_equal_approx(a.y, b.y) && Math::is_equal_approx(a.z, b.z) && Math::is_equal_approx(a.w, b.w);
    }

    // PoolByteArray to String
    static inline String to_string(const PoolByteArray &p_pba) {
        String str;
        PoolByteArray::Read r = p_pba.read();
        str.parse_utf8((const char *)r.ptr(), p_pba.size());
        return str;
    }

    static inline bool make_dir(const String &p_path) {
        DirAccess *da = DirAccess::create_for_path(p_path);
        if (!da->dir_exists(p_path)) {
            if (da->make_dir(p_path) != OK) {
                ERR_PRINT("Failed to make dir: " + p_path);
                return false;
            }
        }
        memdelete(da);
        return true;
    }

    Dictionary merge_dict(const Dictionary &p_target, const Dictionary &p_patch);
    Array join_array(const Array &p_target, const Array &p_patch);
    Array get_branches_as_array(const Array &selection);
    Array get_children(Node *p_node) const;

    /**
     * Calls ResourceLoader::import_remap which converts a path to the .import path
     * which is required when we are packaging up the maps and mods
     */
    String import_remap(const String &p_path);

    /**
     * Convert from res://blah to /users/fabian/project/blah
     * does what FileAccess::fix_path does but exposes it so we can use it!
     */
    String to_absolute_path(const String& p_path);

    /**
     * depth first visitor pattern
     */
    void visit_nodes(Node* p_node, Object* p_object, String p_func_name);
    //void visit_children(Node* p_node, Reference& p_object, String& p_func_name);

    

    void reparent(Node* p_node, Node* p_new_parent);
};

// https://www.cprogramming.com/c++11/c++11-lambda-closures.html
template<typename Func>
void visit_nodes(Node* p_node, Func func) {
    for (int i = 0; i < p_node->get_child_count(); ++i) {
       Node* child = p_node->get_child(i);
       visit_nodes(child, func);
    }

    func(p_node);
}


template<typename Func>
void for_each(const Array& array, Func func) {
    for (int i = 0; i < array.size(); ++i) {
       func(array[i], i);
    }
}



/**
 * Look for first token and split the string into two parts
 */
bool split_first(const String &p_key, String &p_before, String &p_after, const String &p_token = "/");

template <class T>
T array_to_poolarray(const Array &p_array) {
    T poolArray;
    // TODO: 
    return poolArray;
}

template<class T>
class RAII {
public:
    T* ptr;
    const T& value_on_return;

    RAII(T* p_ptr, const T& p_value, const T& p_value_on_return) :
        value_on_return(p_value_on_return)
    {
        ptr = p_ptr;
        *ptr = p_value;
    }

    ~RAII() {
        *ptr = value_on_return;
    }
};

/**
 * Generate a random hex string (GUID/UUID for example) 
 */
String generate_hex(const unsigned int length);
String generate_uuid();

#endif // BUTIL_H
