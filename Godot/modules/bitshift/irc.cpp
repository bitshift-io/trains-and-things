#include "irc.h"
#ifdef IRC_ENABLED
    #pragma message "COMPILING WITH IRC_ENABLED"
    #ifdef _WIN32
        #define WIN32
        #pragma message "COMPILING WITH IRC_ENABLED and WIN32"
    #endif
    #include "libircclient.h"
    #include "libirc_rfcnumeric.h"
#endif
#include "core/os/thread.h"
#include "util.h"
#include <string>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

void addlog (const char * fmt, ...)
{
	//FILE * fp;
	char buf[1024];
	va_list va_alist;

	va_start (va_alist, fmt);
#if defined (WIN32)
	_vsnprintf (buf, sizeof(buf), fmt, va_alist);
#else
	vsnprintf (buf, sizeof(buf), fmt, va_alist);
#endif
	va_end (va_alist);

	printf ("%s\n", buf);
/*
	if ( (fp = fopen ("irctest.log", "ab")) != 0 )
	{
		fprintf (fp, "%s\n", buf);
		fclose (fp);
	}
    */
}

void dump_event (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count)
{
	char buf[512];
	buf[0] = '\0';

    unsigned int cnt;
	for (cnt = 0; cnt < count; cnt++ )
	{
		if ( cnt )
			strcat (buf, "|");

		strcat (buf, params[cnt]);
	}


	addlog ("Event \"%s\", origin: \"%s\", params: %d [%s]", event, origin ? origin : "NULL", cnt, buf);
}

void event_notice(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_notice(event, origin, params, count);
#endif
}

void BIRC::event_notice(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);
#endif
}

void event_invite (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_invite(event, origin, params, count);
#endif
}

void BIRC::event_invite(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);
#endif
}

void event_part(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_quit(event, origin, params, count);
#endif
}

void event_quit(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_quit(event, origin, params, count);
#endif
}

void BIRC::event_quit(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);

    char nickbuf[128];
	if ( !origin || count != 1 )
		return;

	irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));
    String nick = nickbuf;

    // someone quit, so remove them from user lists
    for (Map<String, Channel>::Element *E = channels.front(); E; E = E->next()) {
        auto& channel = E->get();
        if (channel.users.has(nick)) {
            channel.users.erase(nick);
            emit_signal("channel_user_quit", channel.name, nick);
        }
    }
#endif
}

void event_nick (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_nick(event, origin, params, count);
#endif
}

void BIRC::event_nick(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);

	char nickbuf[128];
	if ( !origin || count != 1 )
		return;

	irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));

    String old_nick = nickbuf;
    String new_nick = params[0];

    printf("old nick %s\n", nickbuf);
    printf("new nick %s\n", params[0]);

    // someone changing their nick, so update the user list in the channels
    for (Map<String, Channel>::Element *E = channels.front(); E; E = E->next()) {
        auto& channel = E->get();
        if (channel.users.has(old_nick)) {
            User user = { new_nick };
            channel.users[new_nick] = user;
            channel.users.erase(old_nick);
            emit_signal("channel_user_nick_change", channel.name, old_nick, new_nick);
        }
    }
#endif
}

void event_numeric (irc_session_t * session, unsigned int event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_numeric(event, origin, params, count);
#endif
}

void BIRC::event_numeric (unsigned int event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    switch (event) {
        case LIBIRC_RFC_RPL_TOPIC: {
            int nothing = 0;
            ++nothing;
        } break;

        case LIBIRC_RFC_ERR_YOUREBANNEDCREEP: {
            int nothing = 0;
            ++nothing;
        } break;

        case LIBIRC_RFC_RPL_NAMREPLY: {
            String my_nick = params[0];
            String unknown = params[1];
            String channel_name = params[2];
            String names = params[3];
            Vector<String> user_names = names.split(" ");

            auto &channel = channels[channel_name];
            for (int i = 0; i < user_names.size(); ++i) {
                auto user_name = user_names[i];

                // chop of any admin chars
                if (user_name.begins_with("@") || user_name.begins_with("~") || user_name.begins_with("&")) {
                    user_name = user_name.substr(1);
                }

                User user = { user_name };
                channel.users[user_name] = user;
            }
        } break;

        case LIBIRC_RFC_RPL_ENDOFNAMES: {
            String my_nick = params[0];
            String channel_name = params[1];
            auto &channel = channels[channel_name];
            emit_signal("channel_users_received", channel.name, channel.users.size());
        } break;

        case LIBIRC_RFC_ERR_NONICKNAMEGIVEN:
        case LIBIRC_RFC_ERR_ERRONEUSNICKNAME:
        case LIBIRC_RFC_ERR_NICKNAMEINUSE:
        case LIBIRC_RFC_ERR_NICKCOLLISION:
            // generate a new nick which has the desired name + uuid
            set_nick(me.desired, true);
            break;

        default:
            break;
    }

	if ( event > 400 )
	{
		std::string fulltext;
		for ( unsigned int i = 0; i < count; i++ )
		{
			if ( i > 0 )
				fulltext += " ";

			fulltext += params[i];
		}

		printf ("ERROR %d: %s: %s\n", event, origin ? origin : "?", fulltext.c_str());
        emit_signal("error", fulltext.c_str());
	}
#endif
}

void event_connect (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_connect(event, origin, params, count);
#endif
}

void BIRC::event_connect(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);

    String nick = params[0];
    me.nick = nick;

    connection_status = CS_CONNECTED;
    emit_signal("server_connected", server.host);

    // tell existing channels to connect
    for (Map<String, Channel>::Element *E = channels.front(); E; E = E->next()) {
        irc_cmd_join(session, E->get().name.ascii(), 0);
        E->get().connection_status = CS_CONNECTING;
    }
#endif
}

void event_channel (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_channel(event, origin, params, count);
#endif
}

void BIRC::event_channel(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);

	char nickbuf[128];

	if ( count != 2 )
		return;

    const char *nick = origin;// ? origin : "unknown";
    const char *channel = params[0];
    const char *message = params[1];

    irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));
    
    emit_signal("channel_event", channel, nick, message);

	printf ("'%s' said in channel %s: %s\n", 
		origin ? origin : "someone",
		params[0], params[1] );

	if ( !origin )
		return;

	irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));

	if ( !strcmp (params[1], "quit") )
		irc_cmd_quit (session, "of course, Master!");

	if ( !strcmp (params[1], "help") )
	{
		irc_cmd_msg (session, params[0], "quit, help, dcc chat, dcc send, ctcp");
	}

	if ( !strcmp (params[1], "ctcp") )
	{
		irc_cmd_ctcp_request (session, nickbuf, "PING 223");
		irc_cmd_ctcp_request (session, nickbuf, "FINGER");
		irc_cmd_ctcp_request (session, nickbuf, "VERSION");
		irc_cmd_ctcp_request (session, nickbuf, "TIME");
	}
/*
	if ( !strcmp (params[1], "dcc chat") )
	{
		irc_dcc_t dccid;
		irc_dcc_chat (session, 0, nickbuf, dcc_recv_callback, &dccid);
		printf ("DCC chat ID: %d\n", dccid);
	}

	if ( !strcmp (params[1], "dcc send") )
	{
		irc_dcc_t dccid;
		irc_dcc_sendfile (session, 0, nickbuf, "irctest.c", dcc_file_recv_callback, &dccid);
		printf ("DCC send ID: %d\n", dccid);
	}
*/
	if ( !strcmp (params[1], "topic") )
		irc_cmd_topic (session, params[0], 0);
	else if ( strstr (params[1], "topic ") == params[1] )
		irc_cmd_topic (session, params[0], params[1] + 6);

	if ( strstr (params[1], "mode ") == params[1] )
		irc_cmd_channel_mode (session, params[0], params[1] + 5);

	if ( strstr (params[1], "nick ") == params[1] )
		irc_cmd_nick (session, params[1] + 5);

	if ( strstr (params[1], "whois ") == params[1] )
		irc_cmd_whois (session, params[1] + 5);
#endif
}


void event_join (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_join(event, origin, params, count);
#endif
}

void BIRC::event_join(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event(session, event, origin, params, count);

    auto channel_name = params[0];

    char nickbuf[128];
	if ( !origin || count != 1 )
		return;

	irc_target_get_nick (origin, nickbuf, sizeof(nickbuf));
    String nick = nickbuf;

    auto &channel = channels[channel_name];
    if (channel.connection_status != CS_CONNECTED) {
        channel.connection_status = CS_CONNECTED;

        // this is called when we first join a channel
        String full_nick = origin;
        if (full_nick.begins_with(me.nick)) {
            me.full = full_nick;
            me.nick = nick;
        }

        // I joined a channel
        emit_signal("channel_connected", channel.name, nick);
    }
    else {
        // a new user, who is not us
        User user = { nick };
        channel.users[nick] = user;

        // some other user joined the channel
        emit_signal("channel_user_join", channel.name, nick);
    }
#endif
}

void event_privmsg (irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    BIRC * irc = (BIRC *) irc_get_ctx (session);
    irc->event_privmsg(event, origin, params, count);
#endif
}

void BIRC::event_privmsg(const char * event, const char * origin, const char ** params, unsigned int count) {
#ifdef IRC_ENABLED
    dump_event (session, event, origin, params, count);

    String channel_name = params[0];

    char nickbuf[128];
	if (!origin)
		return;

	irc_target_get_nick(origin, nickbuf, sizeof(nickbuf));
    String nick = nickbuf;

    emit_signal("channel_event", channel_name, nick, params[1]);

	printf ("'%s' said me (%s): %s\n", 
		origin ? origin : "someone",
		params[0], params[1] );
#endif
}

void connect_thread_function(void *self) {
    BIRC *irc = (BIRC *)self;
    irc->connect_thread();
}

void BIRC::connect_thread() {
#ifdef IRC_ENABLED
    // The IRC callbacks structure
    irc_callbacks_t callbacks;

    // Init it
    memset ( &callbacks, 0, sizeof(callbacks) );

    // Set up the mandatory events
    callbacks.event_connect = ::event_connect;
    callbacks.event_channel = ::event_channel;
    callbacks.event_join = ::event_join;
    callbacks.event_numeric = ::event_numeric;
    callbacks.event_privmsg = ::event_privmsg;

	callbacks.event_nick = ::event_nick;
	callbacks.event_quit = ::event_quit;
	callbacks.event_part = ::event_part;
	callbacks.event_mode = dump_event;
	callbacks.event_topic = dump_event;
	callbacks.event_kick = dump_event;
	callbacks.event_notice = ::event_notice;
	callbacks.event_invite = ::event_invite;
	callbacks.event_umode = dump_event;
	callbacks.event_ctcp_rep = dump_event;
	callbacks.event_ctcp_action = dump_event;
	callbacks.event_unknown = dump_event;
/*
	callbacks.event_dcc_chat_req = irc_event_dcc_chat;
	callbacks.event_dcc_send_req = irc_event_dcc_send;
    */

    // Set up the rest of events

    // ensure we have a nick to send to the server
    if (me.desired.size() <= 0) {
        // generate a random nick
        // TODO: pull default name from config
        set_nick("guest", true);
    }

    // Now create the session
    session = irc_create_session( &callbacks );
    if ( !session ) {
        printf ("Could not create session\n");
        return;
    }

    // debug on for testing
    irc_option_set( session, LIBIRC_OPTION_DEBUG );

    connection_status = CS_CONNECTING;
    irc_set_ctx(session, this);

    irc_option_set( session, LIBIRC_OPTION_SSL_NO_VERIFY );

    const char *host_str = server.host.ascii();
    const char *nick_str = me.nick.ascii();
    if (irc_connect(session, host_str, server.port, 0, nick_str, 0, 0)) {
        printf("Could not connect: %s\n", irc_strerror (irc_errno(session)));
        connection_status = CS_DISCONNECTED;
        return;
    }

	// and run into forever loop, generating events
    // need to put in our own while loop
    // as for some reason it just bails with no error
    while ( session && irc_is_connected(session) ) {
        if ( irc_run (session) ) {
            int error_no = irc_errno(session);
            if (error_no != 0) {
                printf ("Could not connect or I/O error: %s\n", irc_strerror (irc_errno(session)));
                connection_status = CS_DISCONNECTED;
                return;
            }
        }
    }

    connection_status = CS_DISCONNECTED;
#endif
}

void BIRC::server_connect(String p_host, int p_port, bool p_useSSL) {
#ifdef IRC_ENABLED
    // If the port number is specified in the server string, use the port 0 so it gets parsed
	if (p_host.find(":") >= 0) {
		p_port = 0;
    }

    server = { p_host, p_port, p_useSSL };
    thread = Thread::create(connect_thread_function, this);
#endif
}

void BIRC::server_disconnect() {
#ifdef IRC_ENABLED
    irc_cmd_quit(session, "");
#endif
}

bool BIRC::is_server_connected() {
#ifdef IRC_ENABLED
    return session && !!irc_is_connected(session);
#else
    return false;
#endif
}

BIRC::ConnectionStatus BIRC::get_connection_status() {
    return connection_status;
}

void BIRC::join_channel(String p_channel) {
#ifdef IRC_ENABLED
    // see if there exists the channel already
    if (channels.has(p_channel)) {
        return;
    }

    Channel chan = { p_channel, "", connection_status == CS_CONNECTED ? CS_CONNECTING : CS_DISCONNECTED, Map<String, User>() };
    channels[p_channel] = chan;

    // if already connected to IRC, attempt to join the channel straight away (TODO: thread this?)
    if (connection_status == CS_CONNECTED) {
        irc_cmd_join(session, p_channel.ascii(), 0);
    }
#endif
}

/*
Vector<String> BIRC::get_channel_users(String p_channel) {
    if (channels.has(p_channel)) {
        return channels[p_channel].users;
    }

    return Vector<String>();
}*/

void BIRC::set_nick(String p_nick, bool p_append_uuid) {
    // no nick name change, so abort
    if (p_nick == me.desired && !p_append_uuid) {
        return;
    }

    me.desired = p_nick;

    String nick = p_nick;
    if (p_append_uuid) {
        String uuid = generate_hex(16);
        nick += "_" + uuid;
    }

#ifdef IRC_ENABLED
    if (is_server_connected()) {
        const char *nick_str = nick.ascii();
        printf("irc_cmd_nick with nick: %s", nick_str);
        irc_cmd_nick(session, nick_str);
    }
#endif
}

String BIRC::get_nick() {
    return me.nick;
}

BIRC::Me *BIRC::get_me() {
    return &me;
}

BIRC::Server *BIRC::get_server() {
    return &server;
}

int BIRC::get_user_count(String p_channel) {
    if (!channels.has(p_channel)) {
        return -1;
    }

    return channels[p_channel].users.size();
}

void BIRC::send_message(String p_nick_or_channel, String p_message) {
#ifdef IRC_ENABLED
    const char *nch_str = p_nick_or_channel.ascii();
    const char *text_str = p_message.utf8();
    irc_cmd_msg(session, nch_str, text_str);
#endif
}

String BIRC::get_server_name() {
    return get_server()->host;
}

void BIRC::_bind_methods() {
    ClassDB::bind_method(D_METHOD("server_connect"), &BIRC::server_connect);
    ClassDB::bind_method(D_METHOD("server_disconnect"), &BIRC::server_disconnect);
    ClassDB::bind_method(D_METHOD("join_channel"), &BIRC::join_channel);
    ClassDB::bind_method(D_METHOD("is_server_connected"), &BIRC::is_server_connected);
    ClassDB::bind_method(D_METHOD("get_user_count"), &BIRC::get_user_count);
    ClassDB::bind_method(D_METHOD("set_nick"), &BIRC::set_nick);
    ClassDB::bind_method(D_METHOD("get_nick"), &BIRC::get_nick);
    ClassDB::bind_method(D_METHOD("get_server_name"), &BIRC::get_server_name);
    ClassDB::bind_method(D_METHOD("get_connection_status"), &BIRC::get_connection_status);

    ADD_SIGNAL(MethodInfo("server_connected"));
    ADD_SIGNAL(MethodInfo("channel_event"));
    ADD_SIGNAL(MethodInfo("channel_connected"));
    ADD_SIGNAL(MethodInfo("channel_user_join"));
    ADD_SIGNAL(MethodInfo("channel_users_received"));
    ADD_SIGNAL(MethodInfo("channel_user_nick_change"));
    ADD_SIGNAL(MethodInfo("channel_user_quit"));
    ADD_SIGNAL(MethodInfo("error"));

    BIND_ENUM_CONSTANT(CS_DISCONNECTED);
    BIND_ENUM_CONSTANT(CS_CONNECTING);
    BIND_ENUM_CONSTANT(CS_CONNECTED);
}

BIRC::BIRC() {
    session = nullptr;
    connection_status = CS_DISCONNECTED;
}

BIRC::~BIRC() {
#ifdef IRC_ENABLED
    // disconnect/cleanup
    connection_status = CS_DISCONNECTED;
    if (thread) {
        irc_disconnect(session);
        Thread::wait_to_finish(thread);
        memdelete(thread);
        thread = NULL;
    }
#endif
}