/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "prop_instance.h"
#include "polygon_area.h"
#include "terrain.h"
#include "thread_pool.h"
#include "database.h"
#include "core/math/random_number_generator.h"
#include "core/engine.h"
#include "core/math/math_funcs.h"
#include "scene/3d/mesh_instance.h"
#include "scene/3d/multimesh_instance.h"
#include "core/os/os.h"

BPolygonArea* BPropInstance::get_polygon_area() {
    Node* parent = get_parent();
    String path = parent->get_path();

    BPolygonArea* poly_parent = Object::cast_to<BPolygonArea>(parent);
    return poly_parent;
}

void BPropInstance::add_child_notify(Node *p_child) {
    if (generating || orig_instance) {
        return;
    }

    orig_instance = Object::cast_to<Spatial>(p_child);
    ERR_FAIL_COND(!orig_instance);
    orig_instance->set_visible(false);
}

void BPropInstance::generate() {
    // if currently generating, wait another frame
    if (generating) {
        //call_deferred("generate");
        return;
    }

    generating = true;

    // help us see if problems are caused by prop instances
    bool enabled = BDatabase::get_singleton()->get_value("bitshift/prop_instance/enabled", true);
    if (!enabled) {
        generating = false;
        return;
    }

    // ensure parent is a BPolygonArea
    BPolygonArea* polygon_area = get_polygon_area();
    ERR_FAIL_COND(!polygon_area);

    // kill children as cleanly as possible (but not the orig_instance!)
    int idx = 0;
    while (idx < get_child_count()) {
        Node *child = get_child(idx);
        if (child == orig_instance) {
            ++idx;
            continue;
        }
        remove_child(child);
        memdelete(child);
    }

    /*
    // delete old multimesh instance nodes
    for (int m = 0; m < mmi_nodes.size(); ++m) {
        remove_child(mmi_nodes[m]);
        memdelete(mmi_nodes[m]);
    }
    */
    mmi_nodes.clear();
    prop_instances.clear();

    // validate we can generate some instances

    if (!is_visible_in_tree()) {
        generating = false;
        return;
    }

    if (Engine::get_singleton()->is_editor_hint() && !enabled_in_editor) {
        generating = false;
        return;
    }

    if (!Engine::get_singleton()->is_editor_hint() && !enabled_in_game) {
        generating = false;
        return;
    }

    Vector<Point2> poly_points = polygon_area->get_polygon();
    if (poly_points.size() <= 0) {
        generating = false;
        return;
    }

    // spawn instances in main thread for physics purposes
    //  if a scene is supplied, used that, else use the first child as the instance
    // 
    for (int i = 0; i < instance_count; ++i) {

        Spatial* inst = NULL;
        if (scene.is_valid()) {
            inst = Object::cast_to<Spatial>(scene->instance());
        }
        else if (orig_instance) {
            inst = Object::cast_to<Spatial>(orig_instance->duplicate());
            inst->set_visible(true);
        }

        prop_instances.push_back(inst);
    }


    // this is causing issues - probably due to creating instances on the thread!
    // TODO: make this an option: multithread
/*
    Task* t = Task::Create(&BPropInstance::generate_task, this);
    t->set_complete(&BPropInstance::generate_task_complete, this);
    BThreadPool::get_singleton()->add_task(t);
*/

    // for now just do it on the main thread
    generate_task();
    generate_task_complete();
}

void BPropInstance::generate_task() {

    //uint64_t us = OS::get_singleton()->get_ticks_usec();

    // if use_multi_mesh_instance is disabled in project.godot, then disallow multi mesh usage
    bool can_use_multi_mesh_instance = BDatabase::get_singleton()->get_value("bitshift/prop_instance/use_multi_mesh_instance", true);
    if (can_use_multi_mesh_instance) {
        can_use_multi_mesh_instance = use_multi_mesh_instance;
    }

    BPolygonArea* poly_area = get_polygon_area();
    //Vector<Point2> poly_points = poly_area->get_polygon();
    AABB aabb = poly_area->get_aabb(); //BUtil::get_singleton()->get_aabb_from_vec_points_2d(poly_points);
    Transform area_xform = poly_area->get_global_transform();

    _seed = random_seed;

    List<Node *> nodes;
    get_tree()->get_nodes_in_group("terrain", &nodes);
    BTerrain* terrain = nodes.size() ? Object::cast_to<BTerrain>(nodes[0]) : NULL; //get_tree()->get_nodes_in_group("terrain"); //BUtil::get_singleton()->get_terrain();

    // TODO: this doesnt seem quite right...
    // set lod position in the centre of the polygon
    Vector3 lod_world_origin = Vector3(aabb.position.x, aabb.position.y, 0.f);
    lod_world_origin = area_xform.xform(lod_world_origin);
    if (terrain) {
        Dictionary col = terrain->raycast_down(lod_world_origin);
        if (col.has("position")) {
            lod_world_origin = col["position"];
        }
    }
    //AABB aabb;
    //aabb.position = lod_world_origin;

    // convert AABB from local space to world space for the LODGroup
    AABB worldAabb = area_xform.xform(aabb);
    set_aabb(worldAabb);


    // the areas AABB and polygon are in local space
    // I find the point inside the poly, then convert it to world space
    // ebfore casting onto terrain
    int max_attempts = 100;
    Vector<Vector3> pt_list;
    while (pt_list.size() < instance_count) {
        float x = get_rand_range(aabb.position.x - aabb.size.x, aabb.position.x + aabb.size.x);
        float y = get_rand_range(aabb.position.y - aabb.size.y, aabb.position.y + aabb.size.y);

        Point2 pt = Point2(x, y);
        if (poly_area->is_point_inside(pt)) {
            // convert from local to world space
            Vector3 worldPt3 = Vector3(pt.x, pt.y, 0.f);
            worldPt3 = area_xform.xform(worldPt3);

            if (terrain) {
                Dictionary col = terrain->raycast_down(worldPt3);
                if (!col.has("position")) {
                    // fell off the map!
                    // if we fall off the map enough, assume the terrain is borked
                    // and ignore it!
                    --max_attempts;
                    if (max_attempts <= 0) {
                        print_line("Error sampling terrain. Is BPropInstance over terrain? " + get_path());
                        terrain = NULL;
                    }
                    continue;
                }

                worldPt3 = col["position"];
            }

            pt_list.push_back(worldPt3);
        }
    }

    //DEBUG_PRINT("BPropInstance::generate_task generate points took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
    //uint64_t us2 = OS::get_singleton()->get_ticks_usec();

    ERR_FAIL_COND(pt_list.size() != instance_count);
    ERR_FAIL_COND(prop_instances.size() != instance_count);


    // this will help us track instance indexes
    // and speed up lookups

    // go through the first instance and work out how many unique meshes there are
    // we will need a MultiMeshInstance for each unique mesh
    Map<MultiMeshInstance*, int> mmi_count;
    Map<String, MultiMeshInstance*> mesh_name_to_mmi;
    Map<String, Ref<Mesh> > mesh_name_to_mesh;
    Map<String, int> mesh_name_count;
    Map<String, String> mesh_to_mesh_inst_name;

    if (can_use_multi_mesh_instance) {

        Spatial* inst = prop_instances[0];
        Array mesh_instances = BUtil::get_singleton()->find_children_by_class_name(inst, "MeshInstance");
        for (int m = 0; m < mesh_instances.size(); ++m) {
            Variant v = mesh_instances[m];
            Node* min = v;
            MeshInstance* mi = Object::cast_to<MeshInstance>(min);
            String mi_name = mi->get_name();
            Ref<Mesh> mesh = mi->get_mesh();
            String mesh_path = mesh->get_path();

            mesh_name_to_mesh.insert(mesh_path, mesh);
            if (mesh_name_count.has(mesh_path)) {
                int count = mesh_name_count[mesh_path];
                mesh_name_count[mesh_path] = (count + 1);
            }
            else {
                mesh_name_count[mesh_path] = 1;
                mesh_to_mesh_inst_name[mesh_path] = mi_name;
            }
        }

        // iterate over mesh dictionaries setting up mesh instances
        for (const Map<String, int>::Element *E = mesh_name_count.front(); E; E = E->next()) {

            String key = E->key();
            int count = E->value();

            Ref<Mesh> mesh = mesh_name_to_mesh[key];

            Ref<MultiMesh> multiMesh;
            multiMesh.instance();
            multiMesh->set_mesh(mesh);
            multiMesh->set_transform_format(MultiMesh::TransformFormat::TRANSFORM_3D);
            multiMesh->set_use_colors(true); // multiMesh->set_color_format(MultiMesh::ColorFormat::COLOR_FLOAT); // FM: CHECK THIS
            multiMesh->set_instance_count(count * instance_count);

            MultiMeshInstance* mmi_node = memnew(MultiMeshInstance);
            //mmi_node->set_rotation_degrees(Vector3(90, 0, 0));
            mmi_node->set_multimesh(multiMesh);

            String mi_name = mesh_to_mesh_inst_name[key];
            mmi_node->set_name(mi_name);

            mmi_nodes.push_back(mmi_node);
            add_child(mmi_node);

            mmi_count[mmi_node] = 0;
            mesh_name_to_mmi[key] = mmi_node;
        }
    }

    //DEBUG_PRINT("BPropInstance::generate_task instance mesh instances took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us2) / 1000000.0));
    //uint64_t us3 = OS::get_singleton()->get_ticks_usec();

    // position ourself at the origin
    set_global_transform(Transform());

/*
    set_translation(Vector3(0, 0, 0));
    set_scale(Vector3(1, 1, 1));
    set_rotation_degrees(Vector3(0, 0, 0));
*/

    // Transform spatial_transform_inverse = get_global_transform().affine_inverse();

    // use generated points
    for (int i = 0; i < pt_list.size(); ++i) {
        Vector3 worldPt3 = pt_list[i];
        Spatial* inst = prop_instances[i];
        ERR_FAIL_COND(!inst);

        inst->set_translation(worldPt3);

        Vector3 rand_scale = Vector3(1, 1, 1) * float(Math::random(scale_min, scale_max));
        inst->set_scale(rand_scale);

        Vector3 rand_rot = Vector3(0, Math::random(rotation_y_min, rotation_y_max), 0);
        inst->set_rotation_degrees(rand_rot);

    /*
        // test where props should be
        AABB aabb;
        aabb.position = worldPt3;
        aabb = aabb.grow(1);
        MeshInstance* testMesh = BUtil::get_singleton()->aabb_to_mesh(aabb);
        add_child(testMesh);
*/

        // iterate over the children extracting MeshInstances
        // will need a mesh instance for each type of Mesh?
        // hide the MeshInstance in the instance as it might have collision or other things
        if (can_use_multi_mesh_instance) {
            Array mesh_instances = BUtil::get_singleton()->find_children_by_class_name(inst, "MeshInstance");
            for (int m = 0; m < mesh_instances.size(); ++m) {
                Variant v = mesh_instances[m];
                Node* min = v;
                MeshInstance* mi = Object::cast_to<MeshInstance>(min);
                Ref<Mesh> mesh = mi->get_mesh();
                String mesh_name = mesh->get_path();

                MultiMeshInstance* mmi_node = mesh_name_to_mmi[mesh_name];
                int instance_index = mmi_count[mmi_node];

                // compute the nodes transform relative to top level parent
                Transform xform = mi->get_transform();
                Spatial* parent = Object::cast_to<Spatial>(mi->get_parent());
                while (parent) {
                    Transform parent_xform = parent->get_transform();
                    xform = parent_xform * xform;
                    parent = Object::cast_to<Spatial>(parent->get_parent());
                }
                // remove the parent transform (our global transform) from this xform
                // xform = spatial_transform_inverse * xform;

                Ref<MultiMesh> multiMesh = mmi_node->get_multimesh();
                ERR_FAIL_COND(!multiMesh.is_valid());
                multiMesh->set_instance_transform(instance_index, xform); // or global transform? or transform with spatial_node inversed out?
                mmi_count[mmi_node] = instance_index + 1;

                // colour variation
                Color color = color_min.linear_interpolate(color_max, Math::random(0.f, 1.f));
                multiMesh->set_instance_color(instance_index, color);

                mi->queue_delete(); // release the mesh instance as we don't need it anymore
            }
        }

        add_child(inst);
    }

    // re-apply and LOD level now we have setup the meshes
    reapply_lod();

    //DEBUG_PRINT("BPropInstance::generate_task setup mesh instances took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us3) / 1000000.0));
    //DEBUG_PRINT("BPropInstance::generate_task took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
}

void BPropInstance::generate_task_complete() {
    generating = false;
}

// generate a random number in a range which is always based on the seed number
float BPropInstance::get_rand_range(float p_from, float p_to) {
    float randf = Math::rand_from_seed(&_seed) / (float)Math::RANDOM_MAX; // number from 0 to 1
    float delta = p_to - p_from;
    float result = p_from + (delta * randf);
    return result;
}

// https://gis.stackexchange.com/questions/6412/generate-points-that-lie-inside-polygon
bool BPropInstance::point_in_poly(const Vector<Point2>& p_poly_points, const Point2& p_point) {

    int i;
    int j = p_poly_points.size() - 1;
    bool output = false;

    for (i = 0; i < p_poly_points.size(); ++i) {
        const Point2& poly_pt_i = p_poly_points[i];
        const Point2& poly_pt_j = p_poly_points[j];

        if ((poly_pt_i.x < p_point.x && poly_pt_j.x >= p_point.x) || (poly_pt_j.x < p_point.x && poly_pt_i.x >= p_point.x)) {

            float val = poly_pt_i.y + (p_point.x - poly_pt_i.x) / (poly_pt_j.x - poly_pt_i.x) * (poly_pt_j.y - poly_pt_i.y);
            if (val < p_point.y) {
                output = !output;
            }
        }

        j = i;
    }

    return output;
}

void BPropInstance::set_random_seed(int p_seed) {
    random_seed = p_seed;
    make_dirty();
}

int BPropInstance::get_random_seed() {
    return random_seed;
}

void BPropInstance::set_scene(const Ref<PackedScene>& p_scene) {
    scene = p_scene;
    orig_instance = NULL;
    make_dirty();
}

Ref<PackedScene> BPropInstance::get_scene() {
    return scene;
}

void BPropInstance::set_color_min(const Color& p_value) {
    color_min = p_value;
    make_dirty();
}

Color BPropInstance::get_color_min() {
    return color_min;
}

void BPropInstance::set_color_max(const Color& p_value) {
    color_max = p_value;
    make_dirty();
}

Color BPropInstance::get_color_max() {
    return color_max;
}

void BPropInstance::set_use_multi_mesh_instance(bool b_enabled) {
    if (use_multi_mesh_instance == b_enabled) {
        return;
    }

    use_multi_mesh_instance = b_enabled;
    make_dirty();
}

bool BPropInstance::get_use_multi_mesh_instance() {
    return use_multi_mesh_instance;
}

void BPropInstance::set_enabled_in_game(bool p_enabled) {
    enabled_in_game = p_enabled;
    make_dirty();
}

bool BPropInstance::get_enabled_in_game() {
    return enabled_in_game;
}

void BPropInstance::set_instance_count(int p_count) {
    instance_count = p_count;
    make_dirty();
}

int BPropInstance::get_instance_count() {
    return instance_count;
}

void BPropInstance::_warning_changed(Node* p_node) {
    // make_dirty();
}

void BPropInstance::set_rotation_y_min(float p_value) {
    rotation_y_min = p_value;
    make_dirty();
}

float BPropInstance::get_rotation_y_min() {
    return rotation_y_min;
}

void BPropInstance::set_rotation_y_max(float p_value) {
    rotation_y_max = p_value;
    make_dirty();
}

float BPropInstance::get_rotation_y_max() {
    return rotation_y_max;
}

void BPropInstance::set_scale_min(float p_value) {
    scale_min = p_value;
    make_dirty();
}

float BPropInstance::get_scale_min() {
    return scale_min;
}

void BPropInstance::set_scale_max(float p_value) {
    scale_max = p_value;
    make_dirty();
}

float BPropInstance::get_scale_max() {
    return scale_max;
}

void BPropInstance::_notification( int p_what) {

    switch (p_what) {
        case NOTIFICATION_READY: {
            if (Engine::get_singleton()->is_editor_hint()) {
                get_tree()->connect("node_configuration_warning_changed", this, "_warning_changed");
            }
        } break;

        case NOTIFICATION_PROCESS: {
            if (dirty && !generating) {
                dirty = false;
                generate();
            }
        } break;
    }
}

void BPropInstance::_bind_methods() {
    ClassDB::bind_method(D_METHOD("generate"), &BPropInstance::generate);
    ClassDB::bind_method(D_METHOD("_warning_changed"), &BPropInstance::_warning_changed);

    ClassDB::bind_method(D_METHOD("set_color_min"), &BPropInstance::set_color_min);
    ClassDB::bind_method(D_METHOD("get_color_min"), &BPropInstance::get_color_min);

    ClassDB::bind_method(D_METHOD("set_color_max"), &BPropInstance::set_color_max);
    ClassDB::bind_method(D_METHOD("get_color_max"), &BPropInstance::get_color_max);

    ClassDB::bind_method(D_METHOD("set_use_multi_mesh_instance"), &BPropInstance::set_use_multi_mesh_instance);
    ClassDB::bind_method(D_METHOD("get_use_multi_mesh_instance"), &BPropInstance::get_use_multi_mesh_instance);

    ClassDB::bind_method(D_METHOD("set_enabled_in_game"), &BPropInstance::set_enabled_in_game);
    ClassDB::bind_method(D_METHOD("get_enabled_in_game"), &BPropInstance::get_enabled_in_game);

    ClassDB::bind_method(D_METHOD("set_random_seed"), &BPropInstance::set_random_seed);
    ClassDB::bind_method(D_METHOD("get_random_seed"), &BPropInstance::get_random_seed);

    ClassDB::bind_method(D_METHOD("set_scene"), &BPropInstance::set_scene);
    ClassDB::bind_method(D_METHOD("get_scene"), &BPropInstance::get_scene);

    ClassDB::bind_method(D_METHOD("set_instance_count"), &BPropInstance::set_instance_count);
    ClassDB::bind_method(D_METHOD("get_instance_count"), &BPropInstance::get_instance_count);

    ClassDB::bind_method(D_METHOD("set_rotation_y_min"), &BPropInstance::set_rotation_y_min);
    ClassDB::bind_method(D_METHOD("get_rotation_y_min"), &BPropInstance::get_rotation_y_min);

    ClassDB::bind_method(D_METHOD("set_rotation_y_max"), &BPropInstance::set_rotation_y_max);
    ClassDB::bind_method(D_METHOD("get_rotation_y_max"), &BPropInstance::get_rotation_y_max);

    ClassDB::bind_method(D_METHOD("set_scale_min"), &BPropInstance::set_scale_min);
    ClassDB::bind_method(D_METHOD("get_scale_min"), &BPropInstance::get_scale_min);

    ClassDB::bind_method(D_METHOD("set_scale_max"), &BPropInstance::set_scale_max);
    ClassDB::bind_method(D_METHOD("get_scale_max"), &BPropInstance::get_scale_max);

    ADD_PROPERTY(PropertyInfo(Variant::BOOL,"enabled_in_game"), "set_enabled_in_game", "get_enabled_in_game");

    ADD_PROPERTY(PropertyInfo(Variant::BOOL,"use_multi_mesh_instance"), "set_use_multi_mesh_instance", "get_use_multi_mesh_instance");

    ADD_PROPERTY(PropertyInfo(Variant::INT,"instance_count"), "set_instance_count", "get_instance_count");
    ADD_PROPERTY(PropertyInfo(Variant::INT,"random_seed"), "set_random_seed", "get_random_seed");

    ADD_PROPERTY(PropertyInfo(Variant::REAL,"rotation_y_min"), "set_rotation_y_min", "get_rotation_y_min");
    ADD_PROPERTY(PropertyInfo(Variant::REAL,"rotation_y_max"), "set_rotation_y_max", "get_rotation_y_max");

    ADD_PROPERTY(PropertyInfo(Variant::REAL,"scale_min"), "set_scale_min", "get_scale_min");
    ADD_PROPERTY(PropertyInfo(Variant::REAL,"scale_max"), "set_scale_max", "get_scale_max");

    ADD_PROPERTY(PropertyInfo(Variant::COLOR,"color_min"), "set_color_min", "get_color_min");
    ADD_PROPERTY(PropertyInfo(Variant::COLOR,"color_max"), "set_color_max", "get_color_max");

    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "scene", PROPERTY_HINT_RESOURCE_TYPE, "PackedScene"), "set_scene", "get_scene");
}

void BPropInstance::make_dirty() {
    dirty = true;
    set_process(true);
}

BPropInstance::BPropInstance() {

    RandomNumberGenerator rng;
    rng.randomize();
    random_seed = rng.randi();

    enabled_in_editor = true;
    enabled_in_game = true;

    use_multi_mesh_instance = true;

    instance_count = 10;

    rotation_y_min = 0;
    rotation_y_max = 360;

    scale_min = 1;
    scale_max = 1.5;

    color_min = Color(1, 1, 1);
    color_max = Color(1, 1, 1);

    // this counters the rotation required in BPolygonArea!
    //set_rotation_degrees(Vector3(90, 0, 0));

    generating = false;
    dirty = false;

    orig_instance = NULL;
}

BPropInstance::~BPropInstance() {

}


