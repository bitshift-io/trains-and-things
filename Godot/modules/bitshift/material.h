/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BMATERIAL_H
#define BMATERIAL_H

#include "scene/resources/material.h"

class BStandardMaterial3D : public StandardMaterial3D {

    GDCLASS(BStandardMaterial3D, StandardMaterial3D);

public:
    BStandardMaterial3D();
    ~BStandardMaterial3D();

    void set_vertex_sway_enabled(bool p_value);
    bool get_vertex_sway_enabled();

    void set_vertex_sway_amplitude(Vector3 p_value);
    Vector3 get_vertex_sway_amplitude();

   void set_vertex_sway_frequency(Vector3 p_value);
    Vector3 get_vertex_sway_frequency();    

    void set_vertex_sway_speed(float p_value);
    float get_vertex_sway_speed();    

    static void init_shaders();
    static void finish_shaders();

protected:
    struct ShaderNames {
        StringName vertex_sway_speed;
        StringName vertex_sway_amplitude;
        StringName vertex_sway_frequency;
    };

    static ShaderNames *shader_names;

    bool vertex_sway_enabled;
    float vertex_sway_speed;
    Vector3 vertex_sway_amplitude;
    Vector3 vertex_sway_frequency;

    virtual void _modify_shader_parameters_string(String& code);
    virtual void _modify_shader_vertex_string(String& code);
    virtual void _modify_shader_fragment_string(String& code);
    virtual void _modify_albedo_texture(String& code);
    virtual void _modify_normal_texture(String& code);

    static void _bind_methods();
};

#endif // BMATERIAL_H
