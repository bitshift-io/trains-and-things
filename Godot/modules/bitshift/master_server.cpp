
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "master_server.h"
#include "database.h"
#include "util.h"
#include "scene/main/viewport.h"
#include "core/io/json.h"
#include "core/os/os.h"
#include <climits>
#include <iostream>
#include <sstream>

#define REFRESH_TIMEOUT 5000 // todo: move to project config? timeout the determines how often we can response to refresh servers query

BMasterServer *BMasterServer::singleton=NULL;

void BMasterServer::connect_to_master_server() {
    String server = BDatabase::get_singleton()->get_value("bitshift/matrix_master_server/server", "chat.freenode.net");
    chat_channel = BDatabase::get_singleton()->get_value("bitshift/matrix_master_server/chat_channel", "");

#ifdef DEMO_BUILD
    server_channel = BDatabase::get_singleton()->get_value("bitshift/matrix_master_server/demo_server_channel", "");
#else
    server_channel = BDatabase::get_singleton()->get_value("bitshift/matrix_master_server/server_channel", "");
#endif

    String player_name = BDatabase::get_singleton()->get_value("game/player_name", "guest");

    connect("channel_event", this, "on_channel_event");
    connect("channel_user_join", this, "on_channel_user_join");
    connect("channel_connected", this, "on_channel_connected");

    set_nick(player_name);
    join_channel(chat_channel);
    join_channel(server_channel);

    server_connect(server);
}

Dictionary BMasterServer::get_server_data() {
    return server_data;
}

void BMasterServer::update_server(const Dictionary &p_data) {
    server_data = p_data;
    send_server_data(false);
}

void BMasterServer::delete_server() {
    server_data = Dictionary();
    send_server_data(false);
}

Array BMasterServer::get_servers() {
    Array server_list;
    for (Map<String, Dictionary>::Element *E = servers.front(); E; E = E->next()) {
        server_list.append(E->value());
    }
    return server_list;
}

void BMasterServer::refresh_servers() {
    // TODO: put a timer on when we last saw anyone post a "query_servers" message
    // and don't allow it to post another within about 10-30 seconds to stop
    // the channel hammering everyones internet
    send_message(server_channel, "query_servers");
}

Error BMasterServer::send_text_message(String text) {
    send_message(chat_channel, text);
    return OK;
}

String BMasterServer::get_chat_channel() {
    return chat_channel;
}

String BMasterServer::get_server_channel() {
    return server_channel;
}

void BMasterServer::send_server_data(bool p_if_hosting) {
    if (p_if_hosting && server_data.empty()) {
        return;
    }

    // broadcast our server info
    String text = JSON::print(server_data);
    send_message(server_channel, text);
}

void BMasterServer::on_channel_user_join(String p_channel, String p_nick) {
    if (p_channel == server_channel) {
        // when someone joins the channel,
        // max out the timer so query_servers can be seen by them straight away
        query_servers_last = UINT32_MAX;
    }
}

void BMasterServer::on_channel_connected(String p_channel, String p_nick) {
    if (p_channel == server_channel) {
        // I joined the server channel
        // so send my server data if I am hosting
        // this resolves the case where I start hosting a server before I connect to the master server
        send_server_data(true);
    }
}

void BMasterServer::on_channel_event(String p_channel, String p_nick, String p_message) {
    if (p_channel == chat_channel) {
        emit_signal("message_received", p_nick, p_message);
    }
    else {
        if (p_message == "query_servers") {
            // should we resond to this query_servers call?
            uint32_t elapsed = OS::get_singleton()->get_ticks_msec() - query_servers_last;
            if (query_servers_last != UINT32_MAX && elapsed <= REFRESH_TIMEOUT) {
                return;
            }

            send_server_data(true);

            // start the timer so stop spamming the server channel
            query_servers_last = OS::get_singleton()->get_ticks_msec();
            return;
        }
        else {
            // we are receiving some server info!
            String err_txt;
	        int err_line;
            Variant v;
            Error err = JSON::parse(p_message, v, err_txt, err_line);
            if (err == OK) {
                Dictionary d = v;
                String action = "";

                if (d.empty()) {
                    servers.erase(p_nick);
                    action = "d"; // delete
                }
                else {
                    if (servers.has(p_nick)) {
                        action = "u"; // update
                    }
                    else {
                        action = "c"; // create
                    }

                    // inject nick to save having the game to inject who its from!
                    d["nick"] = p_nick;
                    servers[p_nick] = d;
                }

                // TODO: should this be on a 10 second timeout to stop it spamming events?
                emit_signal("server_updated", action, p_nick, d);
            }
        }
    }
}

void BMasterServer::_bind_methods() {
    ClassDB::bind_method(D_METHOD("on_channel_event"),&BMasterServer::on_channel_event);
    ClassDB::bind_method(D_METHOD("on_channel_user_join"),&BMasterServer::on_channel_user_join);
    ClassDB::bind_method(D_METHOD("on_channel_connected"),&BMasterServer::on_channel_connected);

    ClassDB::bind_method(D_METHOD("get_chat_channel"),&BMasterServer::get_chat_channel);
    ClassDB::bind_method(D_METHOD("get_server_channel"),&BMasterServer::get_server_channel);

    ClassDB::bind_method(D_METHOD("connect_to_master_server"),&BMasterServer::connect_to_master_server);

    ClassDB::bind_method(D_METHOD("get_server_data"),&BMasterServer::get_server_data);
    ClassDB::bind_method(D_METHOD("update_server"),&BMasterServer::update_server);
    ClassDB::bind_method(D_METHOD("delete_server"),&BMasterServer::delete_server);

    ClassDB::bind_method(D_METHOD("get_servers"),&BMasterServer::get_servers);
    ClassDB::bind_method(D_METHOD("refresh_servers"),&BMasterServer::refresh_servers);

    ClassDB::bind_method(D_METHOD("send_text_message"),&BMasterServer::send_text_message);

    ADD_SIGNAL(MethodInfo("message_received"));
    ADD_SIGNAL(MethodInfo("server_updated"));
}

BMasterServer::BMasterServer() {
    singleton = this;
    query_servers_last = UINT32_MAX;
}

BMasterServer::~BMasterServer() {
}
