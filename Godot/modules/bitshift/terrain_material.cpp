/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "terrain_material.h"
// #include "drivers/gles3/rasterizer_storage_gles3.h"
#include "database.h"

BTerrainMaterial::ShaderNames *BTerrainMaterial::shader_names = NULL;

void BTerrainMaterial::init_shaders() {

	shader_names = memnew(ShaderNames);

	shader_names->extra_texture_names[EXTRA_TEXTURE_TERRAIN] = "texture_terrain";
	shader_names->extra_texture_names[EXTRA_TEXTURE_ALBEDO_R] = "texture_albedo_r";
	shader_names->extra_texture_names[EXTRA_TEXTURE_ALBEDO_G] = "texture_albedo_g";
	shader_names->extra_texture_names[EXTRA_TEXTURE_ALBEDO_B] = "texture_albedo_b";
	shader_names->extra_texture_names[EXTRA_TEXTURE_ALBEDO_A] = "texture_albedo_a";
	shader_names->extra_texture_names[EXTRA_TEXTURE_ALBEDO_BASE] = "texture_albedo_base";

	shader_names->extra_texture_names[EXTRA_TEXTURE_NORMAL_R] = "texture_normal_r";
	shader_names->extra_texture_names[EXTRA_TEXTURE_NORMAL_G] = "texture_normal_g";
	shader_names->extra_texture_names[EXTRA_TEXTURE_NORMAL_B] = "texture_normal_b";
	shader_names->extra_texture_names[EXTRA_TEXTURE_NORMAL_A] = "texture_normal_a";
	shader_names->extra_texture_names[EXTRA_TEXTURE_NORMAL_BASE] = "texture_normal_base";	
	
	shader_names->terrain_uv_scale = "terrain_uv_scale";
}

void BTerrainMaterial::finish_shaders() {

	memdelete(shader_names);
}

void BTerrainMaterial::_update_shader() {
    BStandardMaterial3D::_update_shader();
}

void BTerrainMaterial::set_feature(Feature p_feature, bool p_enabled) {

	// if ambient occlusion is disabled for this map - then turn it off
	// else if enabled, while game details are on low, ensure it is off
	if (p_feature == FEATURE_AMBIENT_OCCLUSION) {
		bool ambient_occlusion = BDatabase::get_singleton()->get_value("bitshift/terrain/ambient_occlusion", true);
		if (p_enabled && !ambient_occlusion) {
			p_enabled = false;
		}
	}

	BStandardMaterial3D::set_feature(p_feature, p_enabled);
}

void BTerrainMaterial::set_extra_feature(ExtraFeature p_feature, bool p_enabled) {

    ERR_FAIL_INDEX(p_feature, EXTRA_FEATURE_MAX);

	// detail normal is set purely by game visual quality
	if (p_feature == EXTRA_FEATURE_DETAIL_NORMAL) {
		bool detail_normal_mapping = BDatabase::get_singleton()->get_value("bitshift/terrain/detail_normal_mapping", true);
		p_enabled = detail_normal_mapping;
	}

	
    if (extra_features[p_feature] == p_enabled) {
        return;
	}

	/*
	// special case for potato
	if (p_feature == EXTRA_FEATURE_DETAIL_NORMAL && p_enabled) {
		int max_texture_image_units;
		glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &max_texture_image_units);
		/*
		RasterizerGLES3 *gles3_raster = dynamic_cast<RasterizerGLES3>(VisualServer::get_singleton());
		RasterizerStorage *storage = gles3_raster->get_storage();
		RasterizerStorageGLES3* gles3_storage = dynamic_cast<RasterizerStorageGLES3>(storage);
		if (gles3_storage->config.max_texture_image_units < 8) {
		* /
		if (max_texture_image_units <= 16) {
			WARN_PRINT("Potato detected, disabling detail normals on terrain");
			return;
		}
	}*/

    extra_features[p_feature] = p_enabled;
    _change_notify();
    _queue_shader_change();
}

bool BTerrainMaterial::get_extra_feature(ExtraFeature p_feature) const {

    ERR_FAIL_INDEX_V(p_feature, EXTRA_FEATURE_MAX, false);
    return extra_features[p_feature];
}

void BTerrainMaterial::set_extra_texture(ExtraTextureParam p_param, const Ref<Texture> &p_texture) {

	ERR_FAIL_INDEX(p_param, EXTRA_TEXTURE_MAX);
	
	extra_textures[p_param] = p_texture;
	RID rid = p_texture.is_valid() ? p_texture->get_rid() : RID();
	VS::get_singleton()->material_set_param(_get_material(), shader_names->extra_texture_names[p_param], rid);
	
	_queue_shader_change();
	_change_notify();
}

Ref<Texture> BTerrainMaterial::get_extra_texture(ExtraTextureParam p_param) const {

	ERR_FAIL_INDEX_V(p_param, EXTRA_TEXTURE_MAX, Ref<Texture>());
	return extra_textures[p_param];
}

void BTerrainMaterial::set_terrain_uv_scale(const Vector2 &p_scale) {

	terrain_uv_scale = p_scale;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->terrain_uv_scale, p_scale);
}

Vector2 BTerrainMaterial::get_terrain_uv_scale() const {

	return terrain_uv_scale;
}

void BTerrainMaterial::_modify_albedo_texture(String& code) {
	code += "\tvec4 terrain_tex = texture(texture_terrain,UV);\n";
	code += "\tvec2 terrain_UV = UV*terrain_uv_scale;\n";
	
	// load diffuse/albedo textures
	code += "\tvec3 albedo_a_tex = texture(texture_albedo_a,terrain_UV).rgb;\n";
	code += "\tvec3 albedo_r_tex = texture(texture_albedo_r,terrain_UV).rgb;\n";
	code += "\tvec3 albedo_g_tex = texture(texture_albedo_g,terrain_UV).rgb;\n";
	code += "\tvec3 albedo_b_tex = texture(texture_albedo_b,terrain_UV).rgb;\n";
	code += "\tvec3 albedo_base_tex = texture(texture_albedo_base,terrain_UV).rgb;\n";

	// blender the layers
	code += "\tvec3 layer_0 = albedo_base_tex;\n";
	code += "\tvec3 layer_1 = mix(layer_0, albedo_b_tex, terrain_tex.b);\n";
	code += "\tvec3 layer_2 = mix(layer_1, albedo_g_tex, terrain_tex.g);\n";
	code += "\tvec3 layer_3 = mix(layer_2, albedo_r_tex, terrain_tex.r);\n";
	code += "\tvec3 layer_4 = mix(layer_3, albedo_a_tex, 1.0 - terrain_tex.a);\n";

	code += "\talbedo_tex.rgb *= layer_4;\n";
}


void BTerrainMaterial::_modify_normal_texture(String& code) {
	if (extra_features[EXTRA_FEATURE_DETAIL_NORMAL]) {
		// terrain tex and terrain UV are from above
		code += "\tvec3 normal_a_tex = texture(texture_normal_a,terrain_UV).rgb;\n";
		code += "\tvec3 normal_r_tex = texture(texture_normal_r,terrain_UV).rgb;\n";
		code += "\tvec3 normal_g_tex = texture(texture_normal_g,terrain_UV).rgb;\n";
		code += "\tvec3 normal_b_tex = texture(texture_normal_b,terrain_UV).rgb;\n";
		code += "\tvec3 normal_base_tex = texture(texture_normal_base,terrain_UV).rgb;\n";

		code += "\tvec3 n_layer_0 = normal_base_tex;\n";
		code += "\tvec3 n_layer_1 = mix(n_layer_0, normal_b_tex, terrain_tex.b);\n";
		code += "\tvec3 n_layer_2 = mix(n_layer_1, normal_g_tex, terrain_tex.g);\n";
		code += "\tvec3 n_layer_3 = mix(n_layer_2, normal_r_tex, terrain_tex.r);\n";
		code += "\tvec3 n_layer_4 = mix(n_layer_3, normal_a_tex, 1.0 - terrain_tex.a);\n";

		// BM blend
		code += "\tNORMALMAP = NORMALMAP * n_layer_4 / vec3(0.5,0.5,1.0);\n";
	}
	// these dont work right for some reason?
	// UDN blend
	//code += "\tNORMALMAP = vec3(NORMALMAP.xy + n_layer_4.xy , NORMALMAP.z);\n";

	// RNM
	//code += "\tvec3 n1 = NORMALMAP * vec3( 2,  2, 2) + vec3(-1, -1,  0);\n"; // basemap
    //code += "\tvec3 n2 = n_layer_4 * vec3(-2, -2, 2) + vec3( 1,  1, -1);\n"; // detail
    //code += "\tNORMALMAP = n1*dot(n1, n2)/n1.z - n2;\n";
	//code += "\tNORMALMAP = n_layer_4;\n";

}
   
    
void BTerrainMaterial::_modify_shader_parameters_string(String& code) {
	// need the hint so godot knows this is NOT srgb
	code += "uniform sampler2D texture_terrain : hint_black;\n";
	code += "uniform sampler2D texture_albedo_a : hint_albedo;\n";
	code += "uniform sampler2D texture_albedo_r : hint_albedo;\n";
	code += "uniform sampler2D texture_albedo_g : hint_albedo;\n";
	code += "uniform sampler2D texture_albedo_b : hint_albedo;\n";
	code += "uniform sampler2D texture_albedo_base : hint_albedo;\n";
	code += "uniform vec2 terrain_uv_scale;\n";

	if (extra_features[EXTRA_FEATURE_DETAIL_NORMAL]) {
		code += "uniform sampler2D texture_normal_a : hint_normal;\n";
		code += "uniform sampler2D texture_normal_r : hint_normal;\n";
		code += "uniform sampler2D texture_normal_g : hint_normal;\n";
		code += "uniform sampler2D texture_normal_b : hint_normal;\n";
		code += "uniform sampler2D texture_normal_base : hint_normal;\n";	
	}
}

void BTerrainMaterial::_modify_shader_vertex_string(String& code) {
}

void BTerrainMaterial::_modify_shader_fragment_string(String& code) {
	// debug normals
	//code += "\tALBEDO = NORMALMAP;\n";
}

void BTerrainMaterial::_bind_methods() {
    //BStandardMaterial3D::_bind_methods();
	
	ClassDB::bind_method(D_METHOD("set_extra_texture", "texture"), &BTerrainMaterial::set_extra_texture);
	ClassDB::bind_method(D_METHOD("get_extra_texture"), &BTerrainMaterial::get_extra_texture);
	
	ClassDB::bind_method(D_METHOD("set_terrain_uv_scale", "scale"), &BTerrainMaterial::set_terrain_uv_scale);
	ClassDB::bind_method(D_METHOD("get_terrain_uv_scale"), &BTerrainMaterial::get_terrain_uv_scale);
	
	ADD_GROUP("TerrainUV", "terrain_uv_");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "terrain_uv_scale"), "set_terrain_uv_scale", "get_terrain_uv_scale");
	
	ADD_GROUP("Terrain", "terrain_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_TERRAIN);

	ADD_GROUP("Terrain Layer (Alpha)", "terrain_layer_a_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_a_albedo", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_ALBEDO_A);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_a_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_NORMAL_A);
			
	ADD_GROUP("Terrain Layer (Red)", "terrain_layer_r_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_r_albedo", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_ALBEDO_R);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_r_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_NORMAL_R);
	
	ADD_GROUP("Terrain Layer (Green)", "terrain_layer_g_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_g_albedo", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_ALBEDO_G);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_g_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_NORMAL_G);
	
	ADD_GROUP("Terrain Layer (Blue)", "terrain_layer_b_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_b_albedo", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_ALBEDO_B);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_b_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_NORMAL_B);
	
	ADD_GROUP("Terrain Layer (Base)", "terrain_layer_base_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_base_albedo", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_ALBEDO_BASE);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "terrain_layer_base_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_NORMAL_BASE);
	
	BIND_CONSTANT(EXTRA_TEXTURE_TERRAIN);
	BIND_CONSTANT(EXTRA_TEXTURE_ALBEDO_R);
	BIND_CONSTANT(EXTRA_TEXTURE_ALBEDO_G);
	BIND_CONSTANT(EXTRA_TEXTURE_ALBEDO_B);
	BIND_CONSTANT(EXTRA_TEXTURE_ALBEDO_A);
	BIND_CONSTANT(EXTRA_TEXTURE_ALBEDO_BASE);
	BIND_CONSTANT(EXTRA_TEXTURE_NORMAL_R);
	BIND_CONSTANT(EXTRA_TEXTURE_NORMAL_G);
	BIND_CONSTANT(EXTRA_TEXTURE_NORMAL_B);
	BIND_CONSTANT(EXTRA_TEXTURE_NORMAL_A);
	BIND_CONSTANT(EXTRA_TEXTURE_NORMAL_BASE);	
}

BTerrainMaterial::BTerrainMaterial() {
	for (int i = 0; i < EXTRA_FEATURE_MAX; i++) {
        extra_features[i] = false;
    }

	set_terrain_uv_scale(Vector2(10, 10));
	set_feature(FEATURE_NORMAL_MAPPING, true);
	set_extra_feature(EXTRA_FEATURE_DETAIL_NORMAL, true);
	set_feature(FEATURE_AMBIENT_OCCLUSION, true);
}

BTerrainMaterial::~BTerrainMaterial() {
	
}
