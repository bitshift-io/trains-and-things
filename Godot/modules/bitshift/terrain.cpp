 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "terrain.h"
#include "terrain_generator.h"
#include "thread_pool.h"
#include "util.h"
#include "database.h"
#include "mesh_gen_tool.h"
#include "lod_group.h"
#include "image_exr.h"
#include "core/bind/core_bind.h"
#include "core/ustring.h"
#include "scene/main/node.h"
#include "scene/main/timer.h"
#include "scene/2d/canvas_item.h"
#include "scene/3d/spatial.h"
#include "scene/3d/mesh_instance.h"
#include "scene/3d/physics_body.h"
#include "scene/resources/texture.h"
#include "core/core_string_names.h"

BTerrain *BTerrain::singleton=NULL;

AABB BTerrain::compute_aabb(float size, float height) {
    float half_size = size / 2;
    AABB aabb;
    aabb.expand_to(Vector3(half_size, height, half_size));
    aabb.expand_to(Vector3(-half_size, 0.f, -half_size));
    aabb.grow_by(1.0f);
    return aabb;
}

void BTerrain::set_regenerate(bool regen) {
   _dirty(F_TERRAIN_MATERIAL | F_HEIGHT_MAP | F_HEIGHT);
}

bool BTerrain::get_regenerate() const {
    return false;
}

void BTerrain::set_height_map_path(const String& path) {
    if (height_map_path == path)
        return;

    height_map_path = path;
    _dirty(F_HEIGHT_MAP);
}

String BTerrain::get_height_map_path() const {
    return height_map_path;
}

void BTerrain::set_normal_map_path(const String& path) {
    if (normal_map_path == path)
        return;

    normal_map_path = path;
    _dirty(F_NORMAL_MAP);
}

String BTerrain::get_normal_map_path() const {
    return normal_map_path;
}

void BTerrain::_changed_callback(Object *p_changed, const char *p_prop) {
	// material changed, so update the mesh
    //_dirty();
    int nothing = 0;
    ++nothing;
}

void BTerrain::_terrain_material_changed() {
    _dirty(F_TERRAIN_MATERIAL);
}

void BTerrain::set_terrain_material(const Ref<Material>& p_material) {
    if (terrain_material == p_material) {
        return;
    }

	if (terrain_material.is_valid()) {
		terrain_material->remove_change_receptor(this);
	}
	
    terrain_material = p_material;
    _dirty(F_TERRAIN_MATERIAL);
	
	if (terrain_material.is_valid()) {
		terrain_material->add_change_receptor(this);
        terrain_material->connect(CoreStringNames::get_singleton()->changed, this, "_terrain_material_changed");
	}
}

Ref<Material> BTerrain::get_terrain_material() const {
    return terrain_material;
}

void BTerrain::set_skirt_material(const Ref<Material>& p_material) {
    if (p_material == skirt_material)
        return;

    skirt_material = p_material;
    _dirty(F_SKIRT_MATERIAL);
}

Ref<Material> BTerrain::get_skirt_material() const {
    return skirt_material;
}

void BTerrain::_notification(int p_what)
{
    switch (p_what)
    {
        // this gets called straight after properties have been set!
        // perfect time to start getting terrain loading!
        case NOTIFICATION_PARENTED: {
            /*
            // the problem is the editor calls this when saving
            // which causes a crashhhhhh, its really only an optimisation for the game
            // load time
            if (!Engine::get_singleton()->is_editor_hint()) {
                first_set_flags = -1; // any changes after a ready means we should apply them!
                dirty_flags = F_SIZE; // flags to determine what we need to generate on ready - need to generate mesh
                _update();
            }*/
        } break;

        case NOTIFICATION_READY: {
            const bool TEXTURE_DEBUG = false;

            /*
            if (Engine::get_singleton()->is_editor_hint()) {
            */
                first_set_flags = -1; // any changes after a ready means we should apply them!
                dirty_flags = F_SIZE; // flags to determine what we need to generate on ready - need to generate mesh
                _update();
            /*
            } else {
                // the game is ready, so block here till we are done! TODO: really should be done in WorldMgr.gd
                if (TEXTURE_DEBUG) {
                    BThreadPool::get_singleton()->wait_all();
                }
            }*/
            BThreadPool::get_singleton()->wait_all();

            // for testing the normal map
            if (TEXTURE_DEBUG)
            {
                BThreadPool::get_singleton()->wait_all();
                normal_map.save("res://User/normal.png");
                height_map.save("res://User/height.png");
            }
        } break;

        case NOTIFICATION_ENTER_TREE: {
            inTree = true;
            //_dirty();
        } break;
        
        case NOTIFICATION_EXIT_TREE: {
            inTree = false;
        } break;
        
        case NOTIFICATION_ENTER_WORLD: {
            //_dirty();
        } break;
        
        case NOTIFICATION_EXIT_WORLD: {
            //BUtil::get_singleton()->delete_children(this);
        } break;
			
        default: {}
    }

    if (p_what == Spatial::NOTIFICATION_ENTER_WORLD || p_what == Spatial::NOTIFICATION_ENTER_TREE) {
		add_to_group("terrain");
	} else if (p_what == Spatial::NOTIFICATION_EXIT_WORLD || p_what == Spatial::NOTIFICATION_EXIT_TREE) {
		remove_from_group("terrain");
	}
	
    //Spatial::_notification(p_what);
}

void BTerrain::_dirty(int flags) {
    bool first_changed = !(first_set_flags & flags);
    first_set_flags |= flags;

    // do not call _update on first set of values
    // this is handled by the NOTIFICATION_READY
    if (first_changed)
        return;

    dirty_flags |= flags;
    call_deferred("_update");
}

void BTerrain::_update() {

    if (in_update)
        return;

    in_update = true;

    bool load_height_map_ok =_load_height_map();
    if (!load_height_map_ok || !height_map.get_height_map_tool().is_valid()) {
        BUtil::get_singleton()->log_editor_message("ERROR: Setup a proper heightmap to see terrain. Failed to load height map:" + height_map_path);
        in_update = false;
        return;
    }

    bool load_normal_map_ok = _generate_normal_map();
    if (!load_normal_map_ok) {
        BUtil::get_singleton()->log_editor_message("ERROR: Failed to generate terrin normal map");
    }

    // we can get away with setting the normal map on the terrain material here
    // as the texture has been created.
    // the threads will, when complete update the contents of the texture
    // so all good, shouldn't need to wait, but i'll leave the line here
    // in case we need to debug in future:
    // BThreadPool::get_singleton()->wait_all();
    _set_normal_map_on_terrain_material();

    if ((dirty_flags & F_HEIGHT) != 0 || (dirty_flags & F_CHUNKS) != 0 || (dirty_flags & F_SIZE) != 0 || (dirty_flags & F_RESOLUTION) != 0) {
        for (int i = 0; i < lodNodes.size(); ++i) {
            remove_child(lodNodes[i]);
            memdelete(lodNodes[i]);
        }
        lodNodes.clear();
        /*
        if (lod_group) {
            remove_child(lod_group);
            memdelete(lod_group);
        }*/

        //aabb = AABB();
        collisionNodes.clear();

        //

        // compute resolution

        float max_quads_per_metre = BDatabase::get_singleton()->get_value("bitshift/terrain/max_quads_per_metre", 1.f);
        float min_quads_per_metre = BDatabase::get_singleton()->get_value("bitshift/terrain/min_quads_per_metre", 0.1f);

        //float collision_quads_per_metre = min_quads_per_metre;
        float terrain_quality = BDatabase::get_singleton()->get_value("bitshift/terrain/model_quality", 1.f); // percentage

        bool skirt_enabled = BDatabase::get_singleton()->get_value("bitshift/terrain/skirt_enabled", true);
        bool shadows_enabled = BDatabase::get_singleton()->get_value("bitshift/terrain/shadows_enabled", true);

        // we want fast loading in the editor, not so caring about quality of the terrain
        if (Engine::get_singleton()->is_editor_hint()) {
            terrain_quality = BDatabase::get_singleton()->get_value("bitshift/terrain/editor_model_quality", 0.f);
            skirt_enabled = features[FEATURE_SKIRT];
            shadows_enabled = features[FEATURE_CAST_SHADOWS];
        }      

        float mesh_quads_per_metre = min_quads_per_metre + (max_quads_per_metre - min_quads_per_metre) * terrain_quality;
        float quads_per_chunk = 100000;

        // at 1 quad per metre, should be about 90,000 quads per chunk
        float root_quads_per_chunk = Math::sqrt(quads_per_chunk);
        float chunk_size = root_quads_per_chunk * (1.f / mesh_quads_per_metre); // 100 x 100 so there will be about 10000 per chunk

        Spatial* lod = memnew(Spatial());
        lod->set_name("Terrain");
        add_child(lod);
        lodNodes.push_back(lod);
        
        BTerrainGenerator::MeshInputData mid(lod, NULL /*&normal_map*/, &height_map, size);
        mid.terrain_material = terrain_material_clone;
        mid.skirt_material = skirt_material;
        mid.skirt_bottom = skirt_bottom;
        //mid.quads_per_chunk = quads_per_chunk;
        mid.flags |= shadows_enabled ? BTerrainGenerator::F_CAST_SHADOWS : 0;
        mid.flags |= skirt_enabled ? BTerrainGenerator::F_SKIRT : 0;
        mid.chunk_size = chunk_size;
        
        // setup LOD's
        Array terrain_lods = BDatabase::get_singleton()->get_value("bitshift/terrain/lods", Array());
        for (int l = 0; l < terrain_lods.size(); ++l) {
            Dictionary lod_dict = terrain_lods[l];
            float near = lod_dict["near"];
            float far = lod_dict["far"];
            float quads_per_metre_multiplier = lod_dict["quads_per_metre_multiplier"];
            String lod_name = "LOD" + String::num(l);

            mid.lods.push_back(BTerrainGenerator::MeshInputData::LOD(mesh_quads_per_metre * quads_per_metre_multiplier, near, far, lod_name));
        }

        BTerrainGenerator *terrain_generator = memnew(BTerrainGenerator);
        //aabb = terrain_generator->compute_aabb(size, height_map.get_height());
        terrain_generator->create_lod(mid, BTerrainGenerator::F_DELETE_ON_COMPLETE);

        /* TODO: get physics working
        // physics
        // start this task before the LOD's
        // as the LOD's thread better
        Spatial* physics = memnew(Spatial());
        physics->set_name("Terrain Collision");
        add_child(physics);
        collisionNodes.push_back(physics);

        BTerrainGenerator::PhysicsInputData pid;
        pid.quads_per_metre = collision_quads_per_metre;
        pid.size = size;
        pid.parent = physics;
        pid.height_map = &height_map;
        pid.normal_map = &normal_map;
        terrain_generator->create_collision(pid);
        */
    }

    dirty_flags = 0;

    _change_notify();
    emit_signal(CoreStringNames::get_singleton()->changed);

    in_update = false;
}

bool BTerrain::_load_height_map() {
    uint64_t us = OS::get_singleton()->get_ticks_usec();

    height_map.load(height_map_path);
    DEBUG_PRINT("BTerrain::_load_height_map took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
    return true;
}

bool BTerrain::_generate_normal_map() {
/*
    // testing loading from file
    {
        uint64_t us = OS::get_singleton()->get_ticks_usec();

        Error error = normal_map.load(normal_map_path);
        if (error != OK) {
            return false;
        }

        DEBUG_PRINT("BTerrain::_load_normal_map (load from file) took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
    }
*/
    // testing generating at load time
    {   
        Error error = normal_map.generate_from_height_map(height_map, size);
        if (error != OK) {
            return false;
        }
    }

    return true;
}

void BTerrain::_set_normal_map_on_terrain_material() {
    if (!terrain_material.is_valid()) {
        BUtil::get_singleton()->log_editor_message("ERROR: Terrain has no valid material but expects a BStandardMaterial3D of some sort, ideally BTerrainMaterial");
        return;
    }

    // clone the terrain material so we dont end up saving our normal map into the material!
    terrain_material_clone = BTerrainGenerator::duplicate_material(terrain_material, Transform());
    StandardMaterial3D* spat_mat = Object::cast_to<StandardMaterial3D>(*terrain_material_clone);

    if (spat_mat) {
        Ref<ImageTexture> normal_texture = normal_map.get_texture();
        spat_mat->set_texture(StandardMaterial3D::TEXTURE_NORMAL, normal_texture);
        spat_mat->set_feature(StandardMaterial3D::FEATURE_NORMAL_MAPPING, true);
    }
    else {
        BUtil::get_singleton()->log_editor_message("ERROR: Terrain expects a BStandardMaterial3D of some sort, ideally BTerrainMaterial");
    }
}

Array BTerrain::get_collision_nodes() {
    Array results;
    for (int i = 0; i < collisionNodes.size(); ++i) {
        results.append(collisionNodes[i]);
    }

    return results;
}

Transform BTerrain::move_transform_to_ground(const Transform& p_transform, float p_push_along_normal_distance) const {
    Transform t_r = p_transform;

    // bring world space position into local space
    Transform xform = get_global_transform();
    Vector3 localPos = xform.xform_inv(t_r.origin);
	
    float halfSize = float(size) / 2.0;
    
    float px = (localPos.x + halfSize) / float(size); // * (heightmap.width_m1 / float(size));
    float py = (localPos.z + halfSize) / float(size); // * (heightmap.height_m1 / float(size));
    Vector2 texel(px, py);

    if (!height_map.is_texel_valid(texel)) //!heightmap.is_bilinear_texel_valid(px, py))
        return p_transform;
	
    //ERR_FAIL_COND_V(heightmap.height != normal_map.height);
    //ERR_FAIL_COND_V(heightmap.width != normal_map.width);
    Vector3 out_normal = get_normal_from_texel(texel);
    
    // dodgey normal maps! - shouldnt be possible, but you never know!
    // if a dodgey normal gets used, the game can crash!
    if (Math::is_nan(out_normal.x) || Math::is_nan(out_normal.y) || Math::is_nan(out_normal.z)) {
        return p_transform;
    }

    //Vector3 krita_col = out_normal * 255.f;
    //DEBUG_PRINT("pixel: (" + itos(px) + ", " + itos(py) + ")");
    //DEBUG_PRINT("krita col: (" + itos(krita_col.x) + ", " + itos(krita_col.y) + ", "+ itos(krita_col.z) + ")");
    
    float height = height_map.get_height_from_texel(texel);
    //Vector3 out_pos = heightmap.get_texel_as_vec3_bilinear(px, py);
        
    localPos.y = height; //out_pos.x * height * height_normalise;
    t_r.origin = xform.xform(localPos);
    
    Vector3 binormal =  p_transform.get_basis().get_axis(0).cross(out_normal);
    Vector3 tangent = out_normal.cross(binormal); //p_transform.get_basis().get_axis(2));
    t_r.basis.set_axis(0, tangent);
    t_r.basis.set_axis(1, out_normal);
    t_r.basis.set_axis(2, binormal);

    // push along normal if requested
    if (p_push_along_normal_distance != 0.0f) {
        t_r.origin += out_normal * p_push_along_normal_distance;
    }
    
    return t_r;
}

Vector3 BTerrain::move_vector_to_ground(const Vector3& p_world_pos) const {
	// bring world space position into local space
    Transform xform = get_global_transform();
    Vector3 localPos = xform.xform_inv(p_world_pos);
	
    float halfSize = float(size) / 2.0;
    
    float px = (localPos.x + halfSize) / float(size);
    float py = (localPos.z + halfSize) / float(size);
    Vector2 texel(px, py);

    if (!height_map.is_texel_valid(texel)) //if (!heightmap.is_pixel_valid(px, py))
        return p_world_pos;
	
    float height = height_map.get_height_from_texel(texel);
    //Vector3 c = heightmap.get_texel_as_vec3_bilinear(px, py);
    //Color c = image.get_pixel(px, py);   
    
    localPos.y = height; //c.x * height * height_normalise;
    
    Vector3 worldPos = xform.xform(localPos);
	return worldPos;
}

Vector2 BTerrain::world_to_texture_space(const Transform& xform, const Vector3& position) const {
    // bring world space position into local space
    Vector3 localPos = xform.xform_inv(position);

    float halfSize = float(size) / 2.0;

    float px = (localPos.x + halfSize) / size;
    float py = (localPos.z + halfSize) / size;

    return Vector2(px, py);
}

Vector2 BTerrain::world_to_texture_space(const Vector3& position) const {
    return world_to_texture_space(get_global_transform(), position);
}

Dictionary BTerrain::raycast_down(const Vector3& position) const {
    // bring world space position into local space
    Transform xform = get_global_transform();
    Vector3 localPos = xform.xform_inv(position);
    
    float halfSize = float(size) / 2.0;
    
    float px = (localPos.x + halfSize) / float(size);
    float py = (localPos.z + halfSize) / float(size);
    Vector2 texel(px, py);

	Dictionary d;
    if (!height_map.is_texel_valid(texel)) //if (heightmap.image.is_null() || !heightmap.is_pixel_valid(px, py))
		return d;
	
    float height = height_map.get_height_from_texel(texel);
    //Color c = image.get_pixel(px, py);   
    
    localPos.y = height; //c.x * height * height_normalise;
    
    Vector3 worldPos = xform.xform(localPos);
    
    d["position"] = worldPos;
    d["normal"] = get_normal_from_texel(texel);
    d["collider"]=this;        
    return d;
}

Dictionary BTerrain::raycast(const Vector3& from, const Vector3& to) {
    Dictionary d;
    
    if (!normal_map.is_valid())
        return d;

    Transform xform = get_global_transform();
    AABB laabb = xform.xform(aabb);

    // here i cast the ray through the AABB for the terrain
    // and reduce the length of the ray to start at the collision point
    // and exit when it gets to the other side of the AABB collision point
    // to make it more accurate!
    Vector3 clipFrom;
    Vector3 clipTo;
    
    if (!laabb.intersects_segment(from, to, &clipFrom)) {
        // no collision
        return d;
    }
    
    if (!laabb.intersects_segment(to, from, &clipTo)) {
        ERR_FAIL_COND_V(!false, d); // what????
    }
    
    // ERR_FAIL_COND_V(!height_map.get_height_map_tool().is_square(), d);
    float w_div_size = 1/*height_map.get_height_map_tool().width_m1*/ / size;
    float halfSize = size / 2.0;
    
    Vector3 fromLocal = xform.xform_inv(clipFrom);
    Vector3 toLocal = xform.xform_inv(clipTo);
    
    // convert from local space to texel space
    fromLocal = Vector3((fromLocal.x + halfSize) * w_div_size, fromLocal.y, (fromLocal.z + halfSize) * w_div_size);
    toLocal = Vector3((toLocal.x + halfSize) * w_div_size, toLocal.y, (toLocal.z + halfSize) * w_div_size);
    
    
    // OPTIMISATIONS:
    // wait for the distance sign to flip?
    // then do some sort of binary search
    const int step_count = 1000.0;
    Vector2 texelStep = (Vector2(toLocal.x, toLocal.z) - Vector2(fromLocal.x, fromLocal.z)) / (float)step_count;
    float heightStep = (toLocal.y - fromLocal.y) / (float)step_count;
    
    //float closestDist = 10000.0;
    bool collision = false;
    //Vector3 localPos = fromLocal;
    
    float prevDist = 1.0;
    
    Vector2 texelPos = Vector2(fromLocal.x, fromLocal.z); // the current ray position ray in texel space
    float texelHeight = fromLocal.y; // the height at the current ray

    for (int s = 0; s < step_count; ++s) {
        if (height_map.is_texel_valid(texelPos)) { //.is_pixel_valid(localPos.x, localPos.z)) {
            //Vector3 c =
            float height = height_map.get_height_from_texel(texelPos); //.get_height_from_pixel(localPos.x, localPos.z);
            //Color c = image.get_pixel(localPos.x, localPos.z);
            //float pixelHeight = c.x * height * height_normalise;
            float dist = texelHeight - height; //pixelHeight;

            // wait for a change in sign
            if ((prevDist > 0.0 && dist < 0.0) || (prevDist < 0.0 && dist > 0.0)) {
                // close enough! - collision
                //DEBUG_PRINT("we have a collision!");
                collision = true;
                break;
            }

            // or some tiny distance
            if (ABS(dist) < 0.01) {
                // close enough! - collision
                collision = true;
                break;
            }

            prevDist = dist;
        }

        //localPos += step;
        texelPos += texelStep;
        texelHeight += heightStep;
    }
     
    if (collision) {
        Vector3 worldNormal = get_normal_from_texel(texelPos);
                
        // convert from texel space to local space
        Vector3 localPos = Vector3((texelPos.x / w_div_size) - halfSize, texelHeight, (texelPos.y / w_div_size) - halfSize); //Vector3((localPos.x / w_div_size) - halfSize, localPos.y, (localPos.z / w_div_size) - halfSize);

        // local to world
        Vector3 worldPos = xform.xform(localPos);

        //DEBUG_PRINT("position: " + worldPos);

        d["position"]=worldPos;
        d["normal"]=worldNormal;
        d["collider"]=this;   
    }
    else {
        //DEBUG_PRINT("no collision with terrain!");
    }
    return d;
}

void BTerrain::set_size(float p_size) {
    if (p_size == size)
        return;

    size = p_size;
    aabb = compute_aabb(size, height_map.get_height());
    _dirty(F_SIZE);
}

float BTerrain::get_size() const {
    return size;
}

void BTerrain::set_height(float p_height) {
    if (p_height == height_map.get_height())
        return;

    height_map.set_height(p_height);
    aabb = compute_aabb(size, height_map.get_height());
    _dirty(F_HEIGHT);
}

float BTerrain::get_height() const {
    return height_map.get_height();
}

AABB BTerrain::get_aabb() const {
    return aabb;
}

void BTerrain::set_feature(Feature p_feature, bool p_enabled) {

    ERR_FAIL_INDEX(p_feature, FEATURE_MAX);
    if (features[p_feature] == p_enabled)
        return;

    if (!Engine::get_singleton()->is_editor_hint()) {
        return;
    }

    features[p_feature] = p_enabled;
    _dirty(F_FEATURE);
    _change_notify();
}

bool BTerrain::get_feature(Feature p_feature) const {

    ERR_FAIL_INDEX_V(p_feature, FEATURE_MAX, false);
    return features[p_feature];
}

void BTerrain::_bind_methods() {
    ClassDB::bind_method(D_METHOD("set_feature", "feature", "enable"), &BTerrain::set_feature);
    ClassDB::bind_method(D_METHOD("get_feature", "feature"), &BTerrain::get_feature);

    ClassDB::bind_method(D_METHOD("set_regenerate"),&BTerrain::set_regenerate);
    ClassDB::bind_method(D_METHOD("get_regenerate"),&BTerrain::get_regenerate);
    ADD_PROPERTY( PropertyInfo(Variant::BOOL,"terrain/regenerate"), "set_regenerate", "get_regenerate");

    ClassDB::bind_method(D_METHOD("_terrain_material_changed"),&BTerrain::_terrain_material_changed);
    ClassDB::bind_method(D_METHOD("_update"),&BTerrain::_update);
    
    ClassDB::bind_method(D_METHOD("raycast_down"),&BTerrain::raycast_down);
    ClassDB::bind_method(D_METHOD("raycast"),&BTerrain::raycast);

    ClassDB::bind_method(D_METHOD("set_size"),&BTerrain::set_size);
    ClassDB::bind_method(D_METHOD("get_size"),&BTerrain::get_size);
    ADD_PROPERTY( PropertyInfo(Variant::REAL,"terrain/size"), "set_size", "get_size");

    ClassDB::bind_method(D_METHOD("set_height"),&BTerrain::set_height);
    ClassDB::bind_method(D_METHOD("get_height"),&BTerrain::get_height);
    ADD_PROPERTY( PropertyInfo(Variant::REAL,"terrain/height"), "set_height", "get_height");

    ClassDB::bind_method(D_METHOD("set_height_map_path"),&BTerrain::set_height_map_path);
    ClassDB::bind_method(D_METHOD("get_height_map_path"),&BTerrain::get_height_map_path);
    ADD_PROPERTY( PropertyInfo(Variant::STRING,"terrain/height_map_path", PROPERTY_HINT_FILE, "*.tif,*.png,*.exr"), "set_height_map_path", "get_height_map_path");

    //ClassDB::bind_method(D_METHOD("set_normal_map_path"),&BTerrain::set_normal_map_path);
    //ClassDB::bind_method(D_METHOD("get_normal_map_path"),&BTerrain::get_normal_map_path);
    //ADD_PROPERTY( PropertyInfo(Variant::STRING,"terrain/normal_map_path", PROPERTY_HINT_FILE, "*.png"), "set_normal_map_path", "get_normal_map_path");

    ClassDB::bind_method(D_METHOD("set_terrain_material","material"),&BTerrain::set_terrain_material);
    ClassDB::bind_method(D_METHOD("get_terrain_material"),&BTerrain::get_terrain_material);
    ADD_PROPERTY( PropertyInfo(Variant::OBJECT,"terrain/terrain_material",PROPERTY_HINT_RESOURCE_TYPE, "Material"), "set_terrain_material", "get_terrain_material");
    
    ClassDB::bind_method(D_METHOD("set_skirt_material","material"),&BTerrain::set_skirt_material);
    ClassDB::bind_method(D_METHOD("get_skirt_material"),&BTerrain::get_skirt_material);
    ADD_PROPERTY( PropertyInfo(Variant::OBJECT,"terrain/skirt_material",PROPERTY_HINT_RESOURCE_TYPE, "Material"), "set_skirt_material", "get_skirt_material");
	
    ClassDB::bind_method(D_METHOD("move_transform_to_ground"), &BTerrain::move_transform_to_ground, DEFVAL(0.f));
	ClassDB::bind_method(D_METHOD("move_vector_to_ground"), &BTerrain::move_vector_to_ground);

    ClassDB::bind_method(D_METHOD("get_collision_nodes"), &BTerrain::get_collision_nodes);

    ClassDB::bind_method(D_METHOD("get_aabb"),&BTerrain::get_aabb);

    ADD_GROUP("Editor Features", "feature_");
    ADD_PROPERTYI(PropertyInfo(Variant::BOOL, "features_cast_shadows"), "set_feature", "get_feature", FEATURE_CAST_SHADOWS);
    ADD_PROPERTYI(PropertyInfo(Variant::BOOL, "features_skirt"), "set_feature", "get_feature", FEATURE_SKIRT);

    BIND_ENUM_CONSTANT(FEATURE_CAST_SHADOWS);
    BIND_ENUM_CONSTANT(FEATURE_SKIRT);
    BIND_ENUM_CONSTANT(FEATURE_MAX);

    ADD_SIGNAL(MethodInfo(CoreStringNames::get_singleton()->changed));
}

BTerrain::BTerrain() {
    singleton = this;   

    size = 1000;
    //height = 0;
    skirt_bottom = -20.0;
    resolution = 100;
    chunks = 5;
    castShadows = false;
    generateCollision = false;
    generateLOD = false;
    inTree = false;
    dirty_flags = 0;
    first_set_flags = 0;
    in_update = false;

    for (int i = 0; i < FEATURE_MAX; i++) {
        features[i] = true;
    }
}

BTerrain::~BTerrain() {
    set_terrain_material(Ref<Material>());
	
    if (singleton == this) {
        singleton = NULL;
    }
}


