/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "polygon_area.h"
#include "terrain.h"
#include "thread_pool.h"
#include "core/math/random_number_generator.h"
#include "core/engine.h"
#include "core/math/math_funcs.h"
#include "scene/3d/mesh_instance.h"
#include "scene/3d/multimesh_instance.h"
#include "core/os/os.h"

AABB BPolygonArea::get_aabb() {
    return aabb;
}

void BPolygonArea::set_depth(float p_depth) {
	depth = p_depth;
	update_gizmo();
}

float BPolygonArea::get_depth() const {
	return depth;
}

void BPolygonArea::set_polygon(const Vector<Point2> &p_polygon) {
	polygon = p_polygon;
    aabb = BUtil::get_singleton()->get_aabb_from_vec_points_2d(polygon);
	update_configuration_warning();
	update_gizmo();
}

Vector<Point2> BPolygonArea::get_polygon() const {

	return polygon;
}

// https://gis.stackexchange.com/questions/6412/generate-points-that-lie-inside-polygon
bool BPolygonArea::is_point_inside(const Point2& p_point) {

    const Vector<Point2>& p_poly_points = get_polygon();

    int i;
    int j = p_poly_points.size() - 1;
    bool output = false;

    for (i = 0; i < p_poly_points.size(); ++i) {
        const Point2& poly_pt_i = p_poly_points[i];
        const Point2& poly_pt_j = p_poly_points[j];

        if ((poly_pt_i.x < p_point.x && poly_pt_j.x >= p_point.x) || (poly_pt_j.x < p_point.x && poly_pt_i.x >= p_point.x)) {

            float val = poly_pt_i.y + (p_point.x - poly_pt_i.x) / (poly_pt_j.x - poly_pt_i.x) * (poly_pt_j.y - poly_pt_i.y);
            if (val < p_point.y) {
                output = !output;
            }
        }

        j = i;
    }

    return output;
}

bool BPolygonArea::_is_editable_3d_polygon() const {
	return true;
}

void BPolygonArea::_bind_methods() {
    ClassDB::bind_method(D_METHOD("_is_editable_3d_polygon"), &BPolygonArea::_is_editable_3d_polygon);

    ClassDB::bind_method(D_METHOD("set_polygon", "polygon"), &BPolygonArea::set_polygon);
	ClassDB::bind_method(D_METHOD("get_polygon"), &BPolygonArea::get_polygon);

	ClassDB::bind_method(D_METHOD("set_depth", "depth"), &BPolygonArea::set_depth);
	ClassDB::bind_method(D_METHOD("get_depth"), &BPolygonArea::get_depth);

    ClassDB::bind_method(D_METHOD("get_aabb"), &BPolygonArea::get_aabb);
    ClassDB::bind_method(D_METHOD("is_point_inside"), &BPolygonArea::is_point_inside);

    ADD_PROPERTY(PropertyInfo(Variant::REAL, "depth"), "set_depth", "get_depth");
	ADD_PROPERTY(PropertyInfo(Variant::POOL_VECTOR2_ARRAY, "polygon"), "set_polygon", "get_polygon");
}

BPolygonArea::BPolygonArea() {
    spatial_node = NULL;

    aabb = AABB(Vector3(-1, -1, -1), Vector3(2, 2, 2));
	depth = -10.0;
	set_notify_local_transform(true);

    
    // we need to rotate 90 degrees so that the polygon creation in the editor is in the X-Z plane
    // the polygon editor does a raycast into the plane formed by the origin, pushed by z-axis & z-axis of this objects
    // transform 
    set_rotation_degrees(Vector3(90, 0, 0));
    /*
    set_depth(100.f); // set depth to 100 to get edit points on top of the shape

    // set up a child to fix the orientation
    spatial_node = memnew(Spatial);
    add_child(spatial_node);
    spatial_node->set_rotation_degrees(Vector3(90, 0, 0));

    set_process(false);*/
}

BPolygonArea::~BPolygonArea() {

}



