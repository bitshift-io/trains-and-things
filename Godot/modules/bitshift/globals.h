/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BGLOBALS_H
#define BGLOBALS_H

#include "core/math/quat.h"
//#define DEMO_BUILD

#define DEMO_TIME 35*60 // an extra 5 minutes to give time to show the quit demo screen
#define DEMO_MAP_1 "res://Maps/Tutorial"
#define DEMO_MAP_2 "res://Maps/Hawaii"
#define DEMO_MOD "res://Mods/Example"


#ifdef DEBUG_ENABLED
    #define DEBUG_PRINT(_x_) print_line( _x_ );
#else
    #define DEBUG_PRINT(_x_)
#endif

// #define EDITOR_NODE_ENABLED

typedef Quat Vector4;

#define VULKAN 1

#endif // BGLOBALS_H
