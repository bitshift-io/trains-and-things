/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BIRC_H
#define BIRC_H

#include "core/reference.h"
#include "core/map.h"

class Thread;
typedef struct irc_session_s	irc_session_t;

class BIRC : public Reference {
    GDCLASS(BIRC, Reference)

public:

    enum ConnectionStatus {
        CS_DISCONNECTED,
        CS_CONNECTING,
        CS_CONNECTED,
    };

    struct Me {
        String desired;
        String nick;
        String full;
    };

    struct Server {
        String host;
        int port;
        bool useSSL;
    };

    struct User {
        String nick;
    };

    struct Channel {
        String name;
        String topic;
        ConnectionStatus connection_status;
        Map<String, User> users;
    };

protected:

    Thread *thread;
    irc_session_t *session;
    ConnectionStatus connection_status;
    Map<String, Channel> channels;
    Server server;
    Me me;

    static void _bind_methods();

public:

    // private events!
    void connect_thread();
    void event_connect(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_numeric(unsigned int event, const char * origin, const char ** params, unsigned int count);
    void event_notice(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_invite(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_nick(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_channel(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_join(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_privmsg(const char * event, const char * origin, const char ** params, unsigned int count);
    void event_quit(const char * event, const char * origin, const char ** params, unsigned int count);
    // end privates

    void server_connect(String p_host, int p_port = 6667, bool p_useSSL = false);
    void server_disconnect();
    bool is_server_connected();
    ConnectionStatus get_connection_status();

    void set_nick(String p_nick, bool p_append_uuid = false);
    String get_nick();
    Me *get_me();

    String get_server_name();
    Server *get_server();

    void send_message(String p_nick_or_channel, String p_message);

    void join_channel(String p_channel);
    //Vector<String> get_channel_users(String p_channel);
    int get_user_count(String p_channel);

    BIRC();
    ~BIRC();
};

VARIANT_ENUM_CAST(BIRC::ConnectionStatus);

#endif // BIRC_H
