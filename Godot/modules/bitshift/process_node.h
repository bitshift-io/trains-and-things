
#ifndef BPROCESSNODE_H
#define BPROCESSNODE_H

#include "scene/main/node.h"
#include "util.h"

#include <functional>

/**
 * Helper class to give singletons process/update functionality
 */
class ProcessNode : public Node {

    GDCLASS(ProcessNode, Node);

    void _notification(int p_what);

    std::function<void()> execute_method;

    static void _bind_methods();

public:

    void add_to_scene_tree();

    template< class R, class F, class... Args >
    ProcessNode(R r, F f, Args... a) {
        execute_method = std::bind(r, f, a...);
        set_process(true);
        call_deferred("add_to_scene_tree");
    }

    ~ProcessNode() {}
};

#endif // BPROCESSNODE_H