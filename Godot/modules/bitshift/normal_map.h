/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BNORMAL_MAP_H
#define BNORMAL_MAP_H

#include "scene/3d/spatial.h"
#include "texture_io_tool.h"
#include "height_map.h"

/**
    @author Fabian Mathews <supagu@gmail.com>

    Generate a normalmap from a heightmap
*/

class BNormalMap : public Spatial {

    GDCLASS(BNormalMap, Spatial)

    struct WeightedStep {
        int step;
        float weight;

        WeightedStep() {
        }

        WeightedStep(int p_step, float p_weight) {
            step = p_step;
            weight = p_weight;
        }
    };

    struct GenerateNormalsTaskData {
        uint32_t first_row;
        uint32_t last_row;

        int inv_scale;
        float height_map_stretch;

        Vector<WeightedStep> weighted_step_list;
        const BHeightMap* height_map;
    };

    BTextureIOTool texture_tool;

    uint32_t task_count;

    Vector3 _fabian_get_normal_from_height_map(const BHeightMap& height_map, uint32_t x, uint32_t y, float step, float height_scale) const;

    void generate_normals_task(GenerateNormalsTaskData* task_data);
    void generate_normals_task_complete(GenerateNormalsTaskData* task_data);

protected:

    static void _bind_methods();
           
public:

    bool is_valid() const;

    void set_texture(const Ref<Texture>& normal_map);
    Ref<Texture> get_texture() const;

    //Ref<ImageTexture> create_texture_from_image();

    Error generate_and_save_from_height_map(const String& path, const BHeightMap& height_map, float blur_size, float blur_sigma);

    Error generate_from_height_map(const BHeightMap& height_map, float map_size);

    Vector3 get_normal_from_height_map(const BHeightMap& height_map, float map_size, const Vector2& texel_coord) const;

    // save as png or exr extension
    Error save(const String& resource_path);
    Error load(const String& path);

    Vector3 get_normal_from_texel(const Vector2& texel_coord) const;
    //Vector3 get_normal_from_pixel(int x, int y) const;

    BNormalMap();
    ~BNormalMap();
};

#endif // BNORMAL_MAP_H
