/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BDatabase_H
#define BDatabase_H

#include "core/reference.h"
#include "core/io/config_file.h"

/**
        @author Fabian Mathews <supagu@gmail.com>

        Config system for storing and retriving settings
*/

class BDatabase : public Reference {

    GDCLASS(BDatabase,Reference)

    static BDatabase *singleton;

    struct Node;
    
    struct NodeResolver {
        
        struct NodeResolution {
            Node* node;
            String key;
            Variant value;
        };
        Vector<NodeResolution> node_resolutions;

        void add(Node *p_node, const String &p_key, const Variant &p_value);
        void resolve(Node* root_node);
    };

    struct Node {
        Node(const Variant &p_value = Variant()) :
            value(p_value) {
        }

        void insert(const String& p_key, const Variant &p_value, NodeResolver& p_resolver_ref);
        void insert(const Dictionary &p_dict, NodeResolver& p_resolver_ref);
        Node* find_child(const String& p_key);
        Node* find_child_recursive(const String& p_key);

        bool find_value(const String& p_key, Variant& p_value_ref);

        void print_tree(const String& p_path = "");
        void clear();

        void compile_dictionary_tree(Dictionary& p_dict_ref);
        Node *clone();

        //String key;
        Variant value;
        //vector<Node *> children;
        HashMap<String, Node *> children;
    };

    Node *root_node;

    bool dirty;

    String settings_file_path;
    Ref<ConfigFile> settings;

    Ref<ConfigFile> project_settings;

    //Dictionary _opts; // map options to value

    //Dictionary resolve_dict(const Dictionary &p_root_dict, const Dictionary &p_dict, int level);

protected:

    static void _bind_methods();

    void unit_test();

public:

    void print_tree();
    void clear(); // only clears the tree - not settings files

    // config file accessors
    void save();

    // load a YAML file and push it into the database
    // overriding values
    Error load(const String& p_path);

    void insert(const Dictionary &p_dict);

    void set_setting(String p_key, Variant p_value);

    /*
    Variant get_setting(String p_key, Variant p_default_value);
    bool has_setting(String p_key);
    */
   
    // database accessors
    bool has(const String &p_key) const;
    Variant get_value(const String &p_key, const Variant &p_default = Variant()) const;
    void set_value(const String &p_key, const Variant &p_value);

    // some fixed database settings
    bool is_build_type(String type);

    // project files accessors
    Ref<ConfigFile> get_project_settings();

    BDatabase();
    ~BDatabase();

    static BDatabase *get_singleton() { return singleton; }
};

#endif // BDatabase_H
