/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BMASTER_SERVER_H
#define BMASTER_SERVER_H

#include "core/reference.h"
#include "irc.h"

class BMasterServer : public BIRC {

    GDCLASS(BMasterServer, BIRC)

public:

protected:

    static BMasterServer *singleton;

    Dictionary server_data;
    uint32_t query_servers_last;

    Map<String, Dictionary> servers;

    String chat_channel;
    String server_channel;

    static void _bind_methods();

    void on_channel_connected(String p_channel, String p_nick);
    void on_channel_event(String p_channel, String p_nick, String p_message);
    void on_channel_user_join(String p_channel, String p_nick);
    void send_server_data(bool p_if_hosting);

public:

    void connect_to_master_server();

    String get_chat_channel();
    String get_server_channel();
    
    // client hosting server information
    Dictionary get_server_data();
    void update_server(const Dictionary &p_data);
    void delete_server();

    // get hosting information for available hosts
    Array get_servers();
    void refresh_servers();

    // room methods
    Error send_text_message(String text);

    BMasterServer();
    ~BMasterServer();

    static BMasterServer *get_singleton() { return singleton; }
};

#endif // BMASTER_SERVER_H
