/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BTERRAIN_GENERATOR_H
#define BTERRAIN_GENERATOR_H

#include "scene/3d/spatial.h"
#include "core/self_list.h"
#include "core/func_ref.h"
#include "normal_map.h"
#include "height_map.h"
#include "mesh_gen_tool.h"
#include <vector>

class CanvasItemMaterial;
class Spatial;
class Mesh;
class Node;
class BMeshGenTool;
class StaticBody;
class BLODGroup;
class MeshInstance;
class BNormalMap;
class BHeightMap;

class BTerrainGenerator {

public:

    enum TerrainGeneratorFlags {
        F_DELETE_ON_COMPLETE = 1 << 0,
    };

    enum Flags {
        F_CAST_SHADOWS         = 1 << 0,
        F_SKIRT             = 1 << 1,

        F_DEFAULT = 0,
    };

    enum Edge {
        E_BOTTOM = 1 << 0,
        E_LEFT   = 1 << 1,
        E_RIGHT  = 1 << 2,
        E_TOP    = 1 << 3
    };

    struct MeshInputData {

        int flags;

        Node* parent;

        Ref<Material> terrain_material;
        Ref<Material> skirt_material;

        BNormalMap *normal_map;
        BHeightMap *height_map;

        float size; // size in metres of the whole map
        float chunk_size; // size of a single chunk
        float skirt_bottom;

        struct LOD {
            float quads_per_metre;
            float near;
            float far;
            String name;

            LOD(float p_quads_per_metre, float p_near, float p_far, const String &p_name) {
                quads_per_metre = p_quads_per_metre;
                near = p_near;
                far = p_far;
                name = p_name;
            }
        };

        std::vector<LOD> lods;

        MeshInputData() {
            parent = NULL;
            normal_map = NULL;
            height_map = NULL;
            size = 100.f;
            chunk_size = 100.f; 
            flags = F_DEFAULT;
            skirt_bottom = -20.f;
        }

        MeshInputData(Node* p_parent, BNormalMap *p_normal_map, BHeightMap* p_height_map, float p_size) {
            parent = p_parent;
            normal_map = p_normal_map;
            height_map = p_height_map;
            size = p_size;
            flags = F_DEFAULT;
            skirt_bottom = -20.f;
        }
    };

    struct PhysicsInputData {
        Node* parent;
        float size; // size in metres
        float quads_per_metre;

        BNormalMap *normal_map;
        BHeightMap *height_map;
    };

    struct MeshOutputData {
        AABB aabb;
    };

    struct Chunk {
        BMeshGenTool mesh_gen;
        int x;
        int y;
        float uv_step;
        int resolution;
        int chunks;
        float chunk_step;
        Vector3 origin;
        Transform uvXform;
        MeshInstance* meshInst;
    };

    struct Skirt {
        BMeshGenTool mesh_gen;
        int resolution;
        int chunks;
        Transform uvXform;
        //Transform xform;
        MeshInstance* meshInst;
        int edges;
        int edgeCount;
    };

    BTerrainGenerator();
    ~BTerrainGenerator();

    void create_lod(MeshInputData& meshInputData, int flags);

    void create_collision(PhysicsInputData& physicsInputData);

    static Ref<Material> duplicate_material(Ref<Material>& material, const Transform& uvTransform);

protected:

    void _create_chunk(BMeshGenTool& mesh_gen, int resolution, int x, int y, const Transform& uvXform);

    void add_skirt_verts_for_point(BMeshGenTool& mesh_gen, int x, int y, int resolution, const Transform& uvXform);

    void _set_normal_map_on_terrain_material();

    void create_chunk(Chunk* chunk);
    void create_chunk_complete(Chunk* chunk);

    void create_skirt(Skirt* skirt);
    void create_skirt_complete(Skirt* skirt);

    void on_complete_task();

    void _create_lod();

    MeshInputData mesh_input_data;
    PhysicsInputData physics_input_data;

    std::vector<BLODGroup*> lod_groups;

    int task_count;
    uint64_t start_time;
    int flags;

    bool multithreaded;
};

#endif // BTERRAIN_GENERATOR_H
