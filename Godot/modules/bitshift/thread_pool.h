/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BTHREAD_POOL_H
#define BTHREAD_POOL_H

#include "core/reference.h"
#include "core/func_ref.h"
#include "core/os/thread.h"
#include "core/object.h"
#include "scene/main/node.h"

#include <functional>
#include <condition_variable>
#include <mutex>
#include <atomic>

/**
        @author Fabian Mathews <supagu@gmail.com>
*/


// Resource acquisition is initialization
class MutexRAII {
public:
    Mutex* mutex;
    Error error;

    enum LockType {
        Lock,
        TryLock,
    };

    MutexRAII(Mutex* p_mutex, LockType lt = Lock) :
        mutex(p_mutex),
        error(Error::OK) {
        if (lt == Lock)
            mutex->lock();
        else
            error = mutex->try_lock();
    }

    Error result() {
        return error;
    }

    ~MutexRAII() {
        // try lock failed in this case, no need to unlock!
        if (error != OK) {
            return;
        }

        mutex->unlock();
    }
};

/* - use this if we want a gdscript add_task variant
Error MessageQueue::push_call(ObjectID p_id, const StringName &p_method, VARIANT_ARG_DECLARE) {

    VARIANT_ARGPTRS;

    int argc = 0;

    for (int i = 0; i < VARIANT_ARG_MAX; i++) {
        if (argptr[i]->get_type() == Variant::NIL)
            break;
        argc++;
    }

    return push_call(p_id, p_method, argptr, argc, false);
}
*/


// https://stackoverflow.com/questions/27280629/variadic-template-class-to-make-a-deferred-call-to-a-variadic-template-function
// https://stackoverflow.com/questions/15752659/thread-pooling-in-c11

class ITask {
    friend class BThreadPool;

public:
    virtual void execute() = 0;

    // callback on main thread to indicate completion
    // what if I want to pass something returned from execute to complete?
    virtual void complete() = 0;

    // if this task has yet to run something, tell it not to
    virtual void halt() = 0;

protected:

    virtual void destroy() = 0;
};

class Task : public ITask {

public:
    std::function<void()> execute_method;
    std::function<void()> complete_method;

    virtual void execute() {
        if (execute_method)
            execute_method();
    }

    virtual void complete() {
        if (complete_method)
            complete_method();
    }

    virtual void destroy() {
        memdelete(this);
    }

    Task() {}

    template< class R, class F, class... Args >
    Task(R r, F f, Args... a) {
        execute_method = std::bind(r, f, a...);
    }

    template< class R, class F, class... Args >
    void set_execute(R r, F f, Args... a) {
        execute_method = std::bind(r, f, a...);
    }

    template< class R, class F, class... Args >
    void set_complete(R r, F f, Args... a) {
        complete_method = std::bind(r, f, a...);
    }

    void set_complete(std::function<void()>& bind) {
        complete_method = bind;
    }

    template< class R, class F, class... Args >
    static Task* Create(R r, F f, Args... a) {
        return memnew(Task(r, f, a...));
    }

    virtual void halt() {
        execute_method = NULL;
        complete_method = NULL;
    }
};

class BThreadPool : public Reference {

    GDCLASS(BThreadPool, Reference)

    static BThreadPool *singleton;

    enum Priority {
        High,
        Normal,
        Low,
    };

    Vector<Thread*> threads;
    List<ITask*> tasks;
    List<ITask*> complete_tasks;

    // how many threads are performing a task?
    std::atomic<uint32_t> working_threads;

    std::condition_variable condition;

    std::mutex tasks_mutex;
    std::mutex complete_tasks_mutex;

    Node* process_node;

    bool running;

protected:

    static void _bind_methods();

public:

    void _thread_function();

    static BThreadPool *get_singleton() { return singleton; }

    void add_task(ITask* p_task, Priority p_priority = High);
    void process();
    void wait_all();
    bool has_tasks();

    int get_thread_count() const;

    BThreadPool();
    ~BThreadPool();

};

#endif // BTHREAD_POOL_H
