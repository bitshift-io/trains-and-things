/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "material.h"

BStandardMaterial3D::ShaderNames *BStandardMaterial3D::shader_names = NULL;

void BStandardMaterial3D::init_shaders() {
    shader_names = memnew(ShaderNames);

    // vertex sway
    shader_names->vertex_sway_speed = "vertex_sway_speed";
    shader_names->vertex_sway_amplitude = "vertex_sway_amplitude";
    shader_names->vertex_sway_frequency = "vertex_sway_frequency";
}

void BStandardMaterial3D::finish_shaders() {

    memdelete(shader_names);
}

BStandardMaterial3D::BStandardMaterial3D() {
    vertex_sway_enabled = false;
    vertex_sway_speed = 1.0;
    vertex_sway_amplitude = Vector3(1.0,1.0,1.0);
    vertex_sway_frequency = Vector3(1.0,1.0,1.0);
    set_vertex_sway_amplitude(vertex_sway_amplitude);
    set_vertex_sway_frequency(vertex_sway_frequency);
    set_vertex_sway_speed(vertex_sway_speed);
}

BStandardMaterial3D::~BStandardMaterial3D() {
}

void BStandardMaterial3D::_modify_albedo_texture(String& code) {
}

void BStandardMaterial3D::_modify_normal_texture(String& code) {
}

void BStandardMaterial3D::_modify_shader_parameters_string(String& code) {
    if(vertex_sway_enabled){
        code += "uniform float vertex_sway_speed;\n";
        code += "uniform vec3 vertex_sway_amplitude;\n";
        code += "uniform vec3 vertex_sway_frequency;\n";
    }
}

void BStandardMaterial3D::_modify_shader_vertex_string(String& code) {
    if(vertex_sway_enabled){
        /* OLD method
        code += "vec3 world_pos=(WORLD_MATRIX*vec4(VERTEX,1)).xyz;\n";
        code += "float frac_time = TIME * vertex_sway_speed;\n";
        code += "vec3 sway_scale = vertex_sway_amplitude * (1.0-UV2.r);\n";
        code += "float x_sin=sin(world_pos.x+frac_time)-0.5;\n";
        code += "float z_sin=sin(world_pos.z+frac_time)-0.5;\n";
        code += "float vertex_sway_start_height=1.0;\n";
        code += "float y_height_scale=sway_scale.y*max(0.0, min(1.0, VERTEX.y - vertex_sway_start_height));\n";
        code += "float z_offset=z_sin*y_height_scale;\n";
        code += "float x_offset=x_sin*y_height_scale;\n";
        code += "vec3 sway_offset=vec3(x_offset,0.0,z_offset);\n";
        code += "VERTEX+=sway_offset;\n";
        */
  
        code += "float frac_time = TIME * vertex_sway_speed;\n";
        code += "float sway_scale = (1.0-UV2.r);\n";
        code += "float x_offset = vertex_sway_amplitude.x * (sin((VERTEX.y+frac_time)*vertex_sway_frequency.x)-0.5);\n";
        code += "float y_offset = vertex_sway_amplitude.y * (sin((VERTEX.y+frac_time)*vertex_sway_frequency.y)-0.5);\n";
        code += "float z_offset = vertex_sway_amplitude.z * (sin((VERTEX.y+frac_time)*vertex_sway_frequency.z)-0.5);\n";
        code += "vec3 sway_offset = sway_scale * vec3(x_offset, y_offset ,z_offset);\n";
        code += "VERTEX+=sway_offset;\n";
    }
}

void BStandardMaterial3D::_modify_shader_fragment_string(String& code) {

}


void BStandardMaterial3D::set_vertex_sway_enabled(bool p_value){
    vertex_sway_enabled = p_value;
    _queue_shader_change();
    _change_notify();
}

bool BStandardMaterial3D::get_vertex_sway_enabled(){
    return vertex_sway_enabled;
}

void BStandardMaterial3D::set_vertex_sway_amplitude(Vector3 p_value){
    vertex_sway_amplitude = p_value;
    VS::get_singleton()->material_set_param(_get_material(), shader_names->vertex_sway_amplitude, p_value);
    _queue_shader_change();
    _change_notify();
}

Vector3 BStandardMaterial3D::get_vertex_sway_amplitude(){
    return vertex_sway_amplitude;
}

void BStandardMaterial3D::set_vertex_sway_frequency(Vector3 p_value){
    vertex_sway_frequency = p_value;
    VS::get_singleton()->material_set_param(_get_material(), shader_names->vertex_sway_frequency, p_value);
    _queue_shader_change();
    _change_notify();
}

Vector3 BStandardMaterial3D::get_vertex_sway_frequency(){
    return vertex_sway_frequency;
}

void BStandardMaterial3D::set_vertex_sway_speed(float p_value){
    vertex_sway_speed = p_value;
    VS::get_singleton()->material_set_param(_get_material(), shader_names->vertex_sway_speed, p_value);
    _queue_shader_change();
    _change_notify();
}

float BStandardMaterial3D::get_vertex_sway_speed(){
    return vertex_sway_speed;
}


void BStandardMaterial3D::_bind_methods() {
    // vertex sway
    ClassDB::bind_method(D_METHOD("set_vertex_sway_enabled", "value"), &BStandardMaterial3D::set_vertex_sway_enabled);
    ClassDB::bind_method(D_METHOD("get_vertex_sway_enabled"), &BStandardMaterial3D::get_vertex_sway_enabled);

    ClassDB::bind_method(D_METHOD("set_vertex_sway_speed", "value"), &BStandardMaterial3D::set_vertex_sway_speed);
    ClassDB::bind_method(D_METHOD("get_vertex_sway_speed"), &BStandardMaterial3D::get_vertex_sway_speed);   

    ClassDB::bind_method(D_METHOD("set_vertex_sway_amplitude", "value"), &BStandardMaterial3D::set_vertex_sway_amplitude);
    ClassDB::bind_method(D_METHOD("get_vertex_sway_amplitude"), &BStandardMaterial3D::get_vertex_sway_amplitude);

    ClassDB::bind_method(D_METHOD("set_vertex_sway_frequency", "value"), &BStandardMaterial3D::set_vertex_sway_frequency);
    ClassDB::bind_method(D_METHOD("get_vertex_sway_frequency"), &BStandardMaterial3D::get_vertex_sway_frequency);    

    ADD_GROUP("Vertex Sway", "vertex_sway_");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "vertex_sway_enabled"), "set_vertex_sway_enabled", "get_vertex_sway_enabled");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "vertex_sway_speed", PROPERTY_HINT_RANGE, "-100,100,0.1"), "set_vertex_sway_speed", "get_vertex_sway_speed");
    ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "vertex_sway_amplitude"), "set_vertex_sway_amplitude", "get_vertex_sway_amplitude");
    ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "vertex_sway_frequency"), "set_vertex_sway_frequency", "get_vertex_sway_frequency");
}
