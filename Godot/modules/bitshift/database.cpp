
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "database.h"
#include "yaml.h"
#include "util.h"
#include "core/project_settings.h"
#include "core/os/dir_access.h"

BDatabase *BDatabase::singleton=NULL;

void BDatabase::NodeResolver::add(Node *p_node, const String &p_key, const Variant &p_value) {
    // queue up stuff to resolve after stuff is added to the database
    NodeResolution nr;
    nr.node = p_node;
    nr.key = p_key;
    nr.value = p_value;
    node_resolutions.push_back(nr);
}

void BDatabase::NodeResolver::resolve(Node* root_node) {
    // here we resolve what needs resolving
    for (int i = 0; i < node_resolutions.size(); ++i) {
        NodeResolution nr = node_resolutions[i];
        Node *node = nr.node;
        String key = nr.key;
        Variant value = nr.value;

        // in future we could make a LINK to the add_node instead of cloning it
        // this might be $add_link function or something?
        // this might cause issues trying to clean up the tree

        // TODO: what about inheriting A from B, which B inherits from C
        // how do we guarantee B is resolved first!!!

        if (key == "$add") {
            Array arr;
            if (value.get_type() == Variant::ARRAY) {
                arr = value;
            }
            else if (value.get_type() == Variant::STRING) {
                String str_val = value;
                arr.push_back(str_val);
            }
            else {
                ERR_PRINT("BDatabase expects $add to be an array or a string. Used by " + key);
            }
            
            for (int ai = 0; ai < arr.size(); ++ai) {
                const String arr_value = arr[ai];
                Node *add_node = root_node->find_child_recursive(arr_value);
                if (!add_node) {
                    ERR_PRINT("BDatabase could not resolve $add for " + arr_value + ", used by " + key);
                    continue;
                }

                const String *k = NULL;
                while ( (k = add_node->children.next(k)) ) {
                    Node* child = add_node->children.get(*k);
                    Node *clone = child->clone();
                    node->children[*k] = clone;
                }
            }
        }
    }
}

/*
// TODO: can we move this to util to be a generic thing?
// TODO: support for #includes/_import ?
Dictionary BDatabase::resolve_dict(const Dictionary &p_root_dict, const Dictionary &p_dict, int level) {
    // resolve any special keys words
    Dictionary results;
    for (int i = 0; i < p_dict.size(); ++i) {
        String key = p_dict.get_key_at_index(i);
        Variant value = p_dict[key];

        if (key == "_add") {
            if (value.get_type() == Variant::ARRAY) {
                Array array = value;
                for (int a = 0; a < array.size(); ++a) {
                    String arr_key = array[a];
                    Dictionary arr_value = p_root_dict[arr_key];

                    // resolve what we are about to patch in, as we don't know if it has been processed
                    arr_value = resolve_dict(p_root_dict, arr_value, level + 1);
                    BUtil::merge_dict(results, arr_value);
                }
            }
        }

        if (value.get_type() == Variant::DICTIONARY) {
            Dictionary temp = value;
            value = resolve_dict(p_root_dict, temp, level + 1);
        }
        if (value.get_type() == Variant::ARRAY) {
            Array array = value;
            for (int a = 0; a < array.size(); ++a) {
                Variant arr_value = array[a];
                if (arr_value.get_type() == Variant::DICTIONARY) {
                    array[a] = resolve_dict(p_root_dict, arr_value, level + 1);
                }
            }
            value = array;
        }

        results[key] = value;
    }

    return results;
}*/

BDatabase::Node *BDatabase::Node::clone() {
    Node *clone = memnew(Node);
    clone->value = value;

    const String *k = NULL;
    while ( (k = children.next(k)) ) {
        Node* child = children.get(*k);
        clone->children[*k] = child->clone();
    }

    return clone;
}

void BDatabase::Node::clear() {
    const String *k = NULL;
    while ( (k = children.next(k)) ) {
        Node* child = children.get(*k);
        child->clear();
        memdelete(child);
    }
    children.clear();
}

void BDatabase::Node::print_tree(const String& p_path) {
    // print this tree's path
    if (value.get_type() != Variant::NIL) {
        String value_str = value;
        print_line(p_path + " [" + value_str + "]");
    }
    else {
        print_line(p_path);
    }
    
    // print children
    const String *k = NULL;
    while ( (k = children.next(k)) ) {
        String child_path = p_path.size() ? p_path + "/" + *k : *k;
        Node* child = children.get(*k);
        child->print_tree(child_path);
    }
}

void BDatabase::Node::compile_dictionary_tree(Dictionary& p_dict_ref) {

    // add children children
    const String *k = NULL;
    while ( (k = children.next(k)) ) {
        String child_path = *k;
        Node* child = children.get(*k);

        // child is itself a dictionary!
        if (child->children.size()) {
            Dictionary dict;
            child->compile_dictionary_tree(dict);
            p_dict_ref[child_path] = dict;
        }
        // just a plain old value...
        else {
            p_dict_ref[child_path] = child->value;
        }
    }
}

bool BDatabase::Node::find_value(const String& p_key, Variant& p_value_ref) {
    String primary_key;
    String sub_key;
    if (split_first(p_key, primary_key, sub_key)) {
        Node* child = find_child(primary_key);
        if (child) {
            return child->find_value(sub_key, p_value_ref);
        }

        // dead end!
        return false;
    }

    Node* child = find_child(p_key);
    if (child) {
        // right, time to compile a dictionary from the child!
        if (child->value.get_type() == Variant::NIL) {
            Dictionary dict;
            child->compile_dictionary_tree(dict);
            p_value_ref = dict;
            return true;
        }

        p_value_ref = child->value;
        return true;
    }

    // dead end!
    return false;
}

// only finds a direct child
BDatabase::Node* BDatabase::Node::find_child(const String& p_key) {
    if (!children.has(p_key)) {
        return NULL;
    }
    return children[p_key];
}

// will search by breaking down the key
BDatabase::Node *BDatabase::Node::find_child_recursive(const String& p_key) {
    String primary_key;
    String sub_key;
    if (split_first(p_key, primary_key, sub_key)) {
        Node *child = find_child(primary_key);
        if (child) {
            return child->find_child_recursive(sub_key);
        }
    }

    return find_child(p_key);
}

void BDatabase::Node::insert(const String& p_key, const Variant &p_value, NodeResolver& p_resolver_ref) {
    String key = p_key;

    // if key has '/' split it
    String primary_key;
    String sub_key;
    if (split_first(p_key, primary_key, sub_key)) {    
        Node* child = find_child(primary_key);
        if (!child) {
            child = memnew(Node);
            children.set(primary_key, child);
        }

        child->insert(sub_key, p_value, p_resolver_ref);
        return;
    }

    // handle inheritance/composition
    if (p_key == "$add") {
        p_resolver_ref.add(this, p_key, p_value);
        return;
    }

    Node* child = find_child(p_key);
    if (!child) {
        child = memnew(Node);
        children.set(p_key, child);
    }

    // if a value is a dictionary, break it down into
    // this tree
    if (p_value.get_type() == Variant::DICTIONARY) {
        child->insert(p_value, p_resolver_ref);
    }
    else {
        child->value = p_value;
    }
}

void BDatabase::Node::insert(const Dictionary &p_dict, NodeResolver& p_resolver_ref) {
    for (int i = 0; i < p_dict.size(); ++i) {
        String key = p_dict.get_key_at_index(i);
        Variant value = p_dict.get_valid(key);
        insert(key, value, p_resolver_ref);
    }

    // resolve stage
    // resolve special symbols like $add etc...
    const String *k = NULL;
    while ( (k = children.next(k)) ) {
        String child_path = *k;
        Node* child = children.get(*k);
    }
}

void BDatabase::insert(const Dictionary &p_dict) {

    NodeResolver node_resolver;
    root_node->insert(p_dict, node_resolver);
    node_resolver.resolve(root_node);
    /*
    Dictionary resolved_dict = resolve_dict(p_dict, p_dict, 0);
    BUtil::merge_dict(_opts, resolved_dict);*/
}

Error BDatabase::load(const String& p_path) {
    // doesnt accept empty path
    if (p_path.size() <= 0) {
        return OK;
    }

    DirAccess *da = DirAccess::open(p_path);
   
    List<String> files;

    // if directory, load all files from the dir
    // else just load the file
    if (da) {
        List<String> dirs;
	    
        da->list_dir_begin();
        String n = da->get_next();
        while (n != String()) {

            if (n != "." && n != "..") {

                if (da->current_is_dir())
                    dirs.push_back(n);
                else
                    files.push_back(p_path + "/" + n);
            }

            n = da->get_next();
        }

        da->list_dir_end();

        memdelete(da);
    }
    else {
        files.push_back(p_path);
    }

    // load files
    for (List<String>::Element *E = files.front(); E; E = E->next()) {
/*
        Error err;
		f = FileAccess::open(E->get(), FileAccess::READ, &err);
		if (err != OK) {
			ERR_FAIL_COND_V(err != OK, ERR_FILE_CANT_OPEN);
		}
        
        String contents = f->get_as_utf8_string();
*/
        Variant result;
        BYAML yaml;
        String err_str;
        int err_line;
        String filename = E->get();
        Error err = yaml.parse_file(filename, result, err_str, err_line);
        if (err != OK) {
            ERR_PRINT("Error parsing file: " + filename + ", BDatabase::load called with: " + p_path);
			ERR_FAIL_COND_V(err != OK, err);
		}

        insert(result);
        
        //memdelete(f);
	}

/*
        Error err;
		f = FileAccess::open(p_path, FileAccess::READ, &err);
		if (err != OK) {
			ERR_FAIL_COND_V(err != OK, ERR_FILE_CANT_OPEN);
		}

    memdelete(da);

    Variant result;
    BYAML yaml;
    String err_str;
    int err_line;
    Error err = yaml.parse_file(p_path, result, err_str, err_line);
    insert(result);
    return err;*/
    return OK;
}

Variant BDatabase::get_value(const String &p_key, const Variant &p_default) const {
    // 1. check for local override (in future: commandline and environment vars)
    Variant value;
    if (root_node->find_value(p_key, value)) {
        return value;
    }
    /*
    if (_opts.has(p_key)) {
        return _opts[p_key];
    }*/

    // 2. check settings file
    int first_slash = p_key.find("/");
    String section = first_slash != -1 ? p_key.substr(0, first_slash) : "";

    String key = first_slash != -1 ? p_key.substr(first_slash + 1, p_key.length()) : p_key;
    if (settings->has_section_key(section, key))
        return settings->get_value(section, key);

    // 3. check globals
    if (ProjectSettings::get_singleton()->has_setting(p_key))
        return ProjectSettings::get_singleton()->get(p_key);

    // failure on every level!
    return p_default;
}

void BDatabase::set_value(const String &p_key, const Variant &p_value) {
    NodeResolver node_resolver;
    root_node->insert(p_key, p_value, node_resolver);
    node_resolver.resolve(root_node);
    /*
    _opts[p_key] = p_default;
    */
    emit_signal("value_changed", p_key, p_value);
}

/*
Ref<ConfigFile> BDatabase::get_project_settings() {
    return project_settings;
}
*/

void BDatabase::save() {
    if (dirty) {
        settings->save(settings_file_path);
    }

    dirty = false;
}

/*
Variant BDatabase::get_setting(String p_key, Variant p_default_value) {
    // 1. check for local override (in future: commandline and environment vars)
    if (_opts.has(p_key)) {
        return _opts[p_key];
    }

    // 2. check settings file
    int first_slash = p_key.find("/");
    String section = first_slash != -1 ? p_key.substr(0, first_slash) : "";

    String key = first_slash != -1 ? p_key.substr(first_slash + 1, p_key.length()) : p_key;
    if (settings->has_section_key(section, key))
        return settings->get_value(section, key);

    // 3. check globals
    if (ProjectSettings::get_singleton()->has_setting(p_key))
        return ProjectSettings::get_singleton()->get(p_key);

    // failure on every level!
    return p_default_value;
}*/

void BDatabase::set_setting(String p_key, Variant p_value) {
    int first_slash = p_key.find("/");
    String section = first_slash != -1 ? p_key.substr(0, first_slash) : "";
    String key = first_slash != -1 ? p_key.substr(first_slash + 1, p_key.length()) : p_key;
    settings->set_value(section, key, p_value);
    dirty = true;
}

bool BDatabase::has(const String &p_key) const {
    
    // 1. check for local override (in future: commandline and environment vars)
    Variant value;
    if (root_node->find_value(p_key, value)) {
        return true;
    }

    /*
    if (_opts.has(p_key))
        return true;
        */

    // 2. check settings file
    int first_slash = p_key.find("/");
    String section = first_slash != -1 ? p_key.substr(0, first_slash) : "";

    String key = first_slash != -1 ? p_key.substr(first_slash + 1, p_key.length()) : p_key;
    if (settings->has_section_key(section, key))
        return true;

    // 3. check globals
    if (ProjectSettings::get_singleton()->has_setting(p_key))
        return true;

    // failure on every level!
    return false;
}


bool BDatabase::is_build_type(String type) {
    /*
    const BT_DEMO = "demo";
    const BT_FULL = "full";
    const BT_DEVELOPER = "developer";
    */

#ifdef DEMO_BUILD
    return "demo" == type;
#else
    return get_value("application/build_type", "demo") == type;
#endif
}

void BDatabase::print_tree() {
    root_node->print_tree();
}

void BDatabase::clear() {
    root_node->clear();
}

void BDatabase::_bind_methods() {
    ClassDB::bind_method(D_METHOD("save"), &BDatabase::save);
    ClassDB::bind_method(D_METHOD("set_setting"), &BDatabase::set_setting);
    ClassDB::bind_method(D_METHOD("get_value", "key", "default_value"), &BDatabase::get_value, DEFVAL(Variant()));
    ClassDB::bind_method(D_METHOD("set_value", "key", "value"), &BDatabase::set_value);
    ClassDB::bind_method(D_METHOD("has"), &BDatabase::has);
    ClassDB::bind_method(D_METHOD("is_build_type"), &BDatabase::is_build_type);
    //ClassDB::bind_method(D_METHOD("get_project_settings"), &BDatabase::get_project_settings);
    ClassDB::bind_method(D_METHOD("load"), &BDatabase::load);
    ClassDB::bind_method(D_METHOD("insert"), &BDatabase::insert);
    ClassDB::bind_method(D_METHOD("print_tree"), &BDatabase::print_tree);
    ClassDB::bind_method(D_METHOD("clear"), &BDatabase::clear);
	ClassDB::bind_method(D_METHOD("unit_test"), &BDatabase::unit_test);
}

void BDatabase::unit_test() {
//#ifdef DEBUG_ENABLED

    Dictionary a;
    a["a/b"] = 1;
    a["a/b/d"] = 3;
    a["a/c"] = 2;
    insert(a);

    int ival = get_value("a/c");
    ERR_FAIL_COND(!(ival == 2));

    Dictionary b;
    b["a/c"] = "c";
    insert(b);

    String sval = get_value("a/c");
    ERR_FAIL_COND(!(sval == "c"));

    Variant doesntExist = get_value("a/b/x");
    ERR_FAIL_COND(!(doesntExist.get_type() == Variant::NIL));

    // testing of inserting dictionary with dictionaries
    Dictionary c;
    Dictionary d;
    d["d1"] = 1;
    d["d2"] = 2;
    d["d3/f"] = "f";
    c["a/b/e"] = d;
    insert(c);

    ival = get_value("a/b/e/d1");
    ERR_FAIL_COND(!(ival == 1));


    // lets test getting something that is not a leaf node
    // and make sure it returns a dictionary tree!
    Dictionary dvalue = get_value("a/b/e");
    ERR_FAIL_COND(!(dvalue.size() == 3));
    ERR_FAIL_COND(!(dvalue.has("d1")));
    ERR_FAIL_COND(!(dvalue.has("d2")));
    ERR_FAIL_COND(!(dvalue.has("d3")));

    Dictionary dvalue2 = dvalue["d3"];
    ERR_FAIL_COND(!(dvalue2.size() == 1));
    ERR_FAIL_COND(!(dvalue2.has("f")));

    // test special database functions: $add
    {
        Dictionary dadd;
        Array arr;
        arr.push_back("a/b/e");
        dadd["add_fn/$add"] = arr;
        insert(dadd);
        Dictionary dvalue3 = get_value("add_fn");
        ERR_FAIL_COND(!(dvalue.size() == 3));
        ERR_FAIL_COND(!(dvalue.has("d1")));
        ERR_FAIL_COND(!(dvalue.has("d2")));
        ERR_FAIL_COND(!(dvalue.has("d3")));
    }

    //print_tree();
    clear();
    //print_tree();

//#endif // DEBUG_ENABLED
}

BDatabase::BDatabase() {
    singleton = this;
    dirty = false;

    root_node = memnew(Node);

    settings_file_path = GLOBAL_DEF("bitshift/settings_file_path", "res://override.cfg");
    Ref<ConfigFile> cf = memnew(ConfigFile);
    settings = cf;

    if (FileAccess::exists(settings_file_path)) {
        Error err = settings->load(settings_file_path);
        if (err != OK) {
            ERR_PRINT("Error loading settings: " + settings_file_path);
        }
    }

    /*
    String project_settings_file_path = "res://project.godot";
    Ref<ConfigFile> cf2 = memnew(ConfigFile);
    project_settings = cf2;
    err = project_settings->load(project_settings_file_path);
    if (err != OK) {
        err = project_settings->load("res://project.binary");
        if (err != OK) {
            ERR_PRINT("Error loading settings: res://project.godot or res://project.binary");
        }
    }
    */
   
    // unit_test();
}

BDatabase::~BDatabase() {
    //save(); - bad! keeps corrupting my filez!
}
