/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BPOLYGON_AREA_H
#define BPOLYGON_AREA_H

#include "scene/3d/collision_polygon.h"
#include "scene/resources/packed_scene.h"
#ifdef TOOLS_ENABLED
#include "editor/spatial_editor_gizmos.h"
#endif

class Spatial;
class MultiMeshInstance;

/**
    Just a Area + CollisionPolygon rotated in its side by default
    so the polygon is in the XZ plane just to make godot a little
    less tedious to use!
*/

class BPolygonArea : public Spatial {

    GDCLASS(BPolygonArea, Spatial)

protected:

    float depth;
    Vector<Vector2> polygon;

    Spatial* spatial_node;
    AABB aabb;

    bool _is_editable_3d_polygon() const;

    static void _bind_methods();
    
public:

    void set_depth(float p_depth);
    float get_depth() const;

    void set_polygon(const Vector<Vector2> &p_polygon);
	Vector<Vector2> get_polygon() const;

    bool is_point_inside(const Point2& p_point);
    AABB get_aabb();

    BPolygonArea();
    ~BPolygonArea();
};

#endif // BPOLYGON_AREA_H
