 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "mesh_gen_tool.h"
#include "scene/resources/mesh.h"

void BMeshGenTool::resize_verts(int vertCount) {
    ERR_FAIL_COND(vertCount < 0);

    pointsw = PoolVector<Vector3>::Write();
    normalsw = PoolVector<Vector3>::Write();
    tangentsw = PoolVector<float>::Write();
    uvsw = PoolVector<Vector2>::Write();
    uvsw2 = PoolVector<Vector2>::Write();
    coloursw = PoolVector<Color>::Write();

    Error err = points.resize(vertCount);
    if (err != OK) {
        int nothing = 0;
        ++nothing;
    }
    normals.resize(vertCount);
    tangents.resize(vertCount*4);
    uvs.resize(vertCount);
    uvs2.resize(vertCount);
    colours.resize(vertCount);
    
    pointsw = points.write();
    normalsw = normals.write();
    tangentsw = tangents.write();
    uvsw = uvs.write();
    uvsw2 = uvs2.write();
    coloursw = colours.write();
    
    point_index = 0;
    uv_index = 0;
    uv2_index = 0;
    normal_index = 0;
    tangent_index = 0;
    colour_index = 0;
}

void BMeshGenTool::resize_indicies(int indexCount) {
    indicesw = PoolVector<int>::Write();

    indices.resize(indexCount);

    indicesw = indices.write();
    indices_index = 0;
}

bool BMeshGenTool::validate() {
    if (uv_index > 0) {
        ERR_FAIL_NULL_V((uv_index == uvs.size()), false);
    }

    if (uv2_index > 0) {
        ERR_FAIL_NULL_V((uv2_index == uvs2.size()), false);
    }

    if (point_index > 0) {
        int points_size = points.size();
        if (point_index != points_size) {
            int nothing = 0;
            ++nothing;
        }
        ERR_FAIL_NULL_V((point_index == points.size()), false);
    }

    if (normal_index > 0) {
        ERR_FAIL_NULL_V((normal_index == normals.size()), false);
    }

    if (tangent_index > 0) {
        ERR_FAIL_NULL_V((tangent_index == tangents.size()), false);
    }

    if (colour_index > 0) {
        ERR_FAIL_NULL_V((colour_index == colours.size()), false);
    }

    if (indices_index > 0) {
        ERR_FAIL_NULL_V((indices_index == indices.size()), false);
    }

    return true;
}

Ref<ArrayMesh> BMeshGenTool::generate_mesh() {
    
    ERR_FAIL_NULL_V(validate(), Ref<ArrayMesh>());

    Array arr;
    arr.resize(VS::ARRAY_MAX);
    arr[VS::ARRAY_VERTEX] = points;
    arr[VS::ARRAY_INDEX] = indices;
	
    if (uv_index > 0) {
        arr[VS::ARRAY_TEX_UV] = uvs;
    }

    if (uv2_index > 0) {
        arr[VS::ARRAY_TEX_UV2] = uvs2;
    }

    if (normal_index > 0) {
        arr[VS::ARRAY_NORMAL] = normals;
    }
	
    if (tangent_index > 0) {
        arr[VS::ARRAY_TANGENT] = tangents;
    }
	
    if (colour_index > 0) {
        arr[VS::ARRAY_COLOR] = colours;
    }

    Ref<ArrayMesh> mesh = memnew( ArrayMesh() );
    if (points.size() && indices.size()) {
        mesh->add_surface_from_arrays(Mesh::PRIMITIVE_TRIANGLES, arr);

        // ensure aabb is good!
        //aabb.grow_by(0.01f);
        //mesh->surface_set_custom_aabb(0, aabb);
    }

    return mesh;
}

void BMeshGenTool::set_next_point(const Vector3& point) {
    if (point_index >= points.size()) {
        int nothing = 0;
        ++nothing;
    }
    ERR_FAIL_COND(point_index >= points.size());
    //if (point_index == 0)
    //    aabb.position = point;
    //else
    //    aabb.expand_to(point);

    pointsw[point_index] = point;
    ++point_index;
}

void BMeshGenTool::set_next_uv(const Vector2& uv) {
    ERR_FAIL_COND(uv_index >= uvs.size());
    uvsw[uv_index] = uv;
    ++uv_index;
}

void BMeshGenTool::set_next_uv2(const Vector2& uv) {
    ERR_FAIL_COND(uv2_index >= uvs2.size());
    uvsw2[uv2_index] = uv;
    ++uv2_index;
}
    
void BMeshGenTool::set_point(int index, const Vector3& point) {
    ERR_FAIL_COND(!(index >= 0 && index < points.size()));
    pointsw[index] = point;
}

void BMeshGenTool::set_uv(int index, const Vector2& uv) {
    ERR_FAIL_COND(!(index >= 0 && index < uvs.size()));
    uvsw[index] = uv;
}

void BMeshGenTool::set_next_normal(const Vector3& normal) {
    ERR_FAIL_COND(!(normal_index < normals.size()));
    normalsw[normal_index] = normal;
    ++normal_index;
}

void BMeshGenTool::set_next_tangent(const Plane& tangent) {
    ERR_FAIL_COND(!(tangent_index < tangents.size()));
    tangentsw[tangent_index + 0] = tangent.normal.x;
	tangentsw[tangent_index + 1] = tangent.normal.y;
	tangentsw[tangent_index + 2] = tangent.normal.z;
	tangentsw[tangent_index + 3] = tangent.d;
    tangent_index += 4;
}

void BMeshGenTool::set_next_colour(const Color& colour) {
    ERR_FAIL_COND(!(colour_index < colours.size()));
    coloursw[colour_index] = colour;
    ++colour_index;
}

void BMeshGenTool::set_next_triangle_indicies(int a, int b, int c) {
    ERR_FAIL_COND(!((indices_index + 2) < indices.size()));
    ERR_FAIL_COND(!(a >= 0 && a < points.size()));
    ERR_FAIL_COND(!(b >= 0 && b < points.size()));
    ERR_FAIL_COND(!(c >= 0 && c < points.size()));
    
    indicesw[indices_index] = a;
    indicesw[indices_index + 1] = b;
    indicesw[indices_index + 2] = c;  
    
    indices_index += 3;
}

void BMeshGenTool::set_triangle_indicies(int tri_index, int a, int b, int c) {
    int idx = tri_index * 3;
    ERR_FAIL_COND(!(idx >= 0 && (idx + 2) < indices.size()));
    ERR_FAIL_COND(!(a >= 0 && a < points.size()));
    ERR_FAIL_COND(!(b >= 0 && b < points.size()));
    ERR_FAIL_COND(!(c >= 0 && c < points.size()));
    
    indicesw[idx] = a;
    indicesw[idx + 1] = b;
    indicesw[idx + 2] = c;    
}

void BMeshGenTool::populate_all_normals_and_tangents(const Vector3& normal, const Vector3& tangent, const Vector3& binormal) {    
    Vector3 bn = normal.cross(tangent);
    float d = binormal.dot(bn);
    
    for (int i = 0; i < points.size(); ++i) {
        normalsw[i] = normal;

        // taken from SurfaceTool::commit case Mesh::ARRAY_FORMAT_TANGENT
        // this generates the tangent plane
        float ti = i * 4;
        tangentsw[ti+0]=tangent.x;
        tangentsw[ti+1]=tangent.y;
        tangentsw[ti+2]=tangent.z;
        tangentsw[ti+3]=d<0 ? -1 : 1;
    }
	
	normal_index = points.size();
	tangent_index = points.size() * 4;
}

Vector3 BMeshGenTool::get_point(int index) {
    ERR_FAIL_COND_V(!(index >= 0 && index < points.size()), Vector3());
    return pointsw[index];
}

void BMeshGenTool::clear() {
    resize_verts(0);
    resize_indicies(0);
}

BMeshGenTool::BMeshGenTool() {
    clear();
}

BMeshGenTool::~BMeshGenTool() {

}


