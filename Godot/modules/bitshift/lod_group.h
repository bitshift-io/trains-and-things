 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BLOD_GROUP_H
#define BLOD_GROUP_H

#include "scene/3d/spatial.h"
#include "core/vector.h"

class BCurveMesh;
class BArcLineCurve;
class MeshInstance;
class StaticBody;

/**
    @author Fabian Mathews <supagu@gmail.com>

    Node that switches child nodes based on distance from camera
*/

class BLODGroup : public Spatial  {

    GDCLASS(BLODGroup, Spatial);

public:

    enum {
        MAX_LOD_RANGES = 100
    };

    struct LODRange {
        float far;
        float near;
        String name;
        //Spatial* child_node;
    };

    Vector<LODRange> lod_ranges;
    float distance; // distance to camera

    int current_lod_idx;

    bool enabled_in_editor;

    float lod_scale;
    float lod_bias;

    // this is a local space offset to compute the origin
    //Vector3 lod_origin;
    AABB aabb;

    MeshInstance* debug_aabb_mi;
    bool debug;

protected:

    static void _bind_methods();

    void _notification(int p_what);
    void _update();
    void _validate_property(PropertyInfo &property) const;

    virtual void add_child_notify(Node *p_child);
    virtual void remove_child_notify(Node *p_child);
    virtual void move_child_notify(Node *p_child);

    void apply_lod(int p_lod_range_idx);

    void reapply_lod();

public:

    void set_lod_range_name(int p_group, String p_name);
    String get_lod_range_name(int p_group) const;

    void set_enabled_in_editor(bool b_enabled);
    bool get_enabled_in_editor() const;

    void set_lod_range_near(int p_group, float p_range);
    float get_lod_range_near(int p_group) const;

    void set_lod_range_far(int p_group, float p_range);
    float get_lod_range_far(int p_group) const;

    void set_distance(float p_d);
    float get_distance();

    void set_current_lod_idx(int p_d);
    int get_current_lod_idx();

    void set_lod_range_size(int p_count);
    int get_lod_range_size() const;

    /*
    void set_aabb_offset(const Vector3& p_pos);
    Vector3 get_aabb_offset();

    void set_aabb_radius(const float& p_radius);
    float get_aabb_radius();
    */
   
    void set_aabb(const AABB &p_aabb);
    AABB get_aabb();

    BLODGroup();
    virtual ~BLODGroup();
};

#endif // BLOD_GROUP_H
