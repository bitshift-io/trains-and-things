#include "editor_tool.h"
#include "editor_plugin.h"
#include "scene/resources/packed_scene.h"
#include "scene/3d/spatial.h"
#include "../globals.h"
#include "editor/editor_data.h"
#include <string.h>
#include "modules/bitshift/polygon_area.h"
#include "core/math/vector2.h"
#include "core/vector.h"
#include "scene/3d/mesh_instance.h"
#include "../util.h"

void BEditorTool::init(BEditorPlugin *p_parent) {
    parent = p_parent;
}

/** POLYGON TOOL **/
void PolygonTool::init(BEditorPlugin *p_parent) {
    BEditorTool::init(p_parent);
    p_parent->register_tool("Polygon", this);
}

bool PolygonTool::process_input(Camera *p_camera, const Ref<InputEvent> &p_event) {
    const InputEventMouseButton *mb = Object::cast_to<InputEventMouseButton>(*p_event);
    const InputEventMouseMotion *mm = Object::cast_to<InputEventMouseMotion>(*p_event);
    bool capture_event = false;

    // left mouse pressed
    if (mb && !tool_active && mb->get_button_index() == BUTTON_LEFT && mb->is_pressed()) {
        if (!start()) return false; // return if fails
        tool_active = true;
        capture_event = true;
    }

    // mouse move drag
    if (tool_active && (mb || mm)) {
        // cast ray
        Dictionary ray = parent->get_ray(p_camera, p_event);
        Transform xform;
        bool result = parent->cast_ray(ray["start"], ray["end"], xform);
        if (!result) {
            cursor_over_terrain = false;
        }

        // we moved back on the terrain
        if (result && !cursor_over_terrain) {
            // so we add the extra points
            cursor_over_terrain = true;
            add_point(last_terrain_point);
            add_point(xform.get_origin());
        } 
        
        // already over terrain
        if (result && cursor_over_terrain) {
            last_terrain_point = xform.get_origin();
            if (check_distance(last_terrain_point)) {
                add_point(last_terrain_point);
            }
        }

        capture_event = true;
    }

    // mouse released
    if (tool_active && mb && !mb->is_pressed()) {
        end();
        tool_active = false;
        capture_event = true;
    }

    return capture_event;
}

bool PolygonTool::start() {
    // get first selected
    Array sel = parent->editor_selection->get_selected_nodes();
    if (sel.empty()) {
        DEBUG_PRINT("no selection");
        return false;
    }
    selected = Object::cast_to<Node>(sel[0]);

    poly_array.clear();
    cursor_over_terrain = true;
    node = memnew(BPolygonArea);
    selected->add_child(node);
    selected->move_child(node, 0); // move to top
    parent->set_owner_to_editor(node);
    return true;
}

bool PolygonTool::check_distance(const Vector3 &p_point) {
    float distance = last_point.distance_to(p_point);
    if (distance > min_distance) {
        return true;
    }
        
    return false;
}

void PolygonTool::add_point(const Vector3 &p_point) {
    poly_array.append(p_point);
    last_point = p_point;
    return;
}



AABB PolygonTool::get_aabb() {
    AABB aabb = AABB(poly_array[0], Vector3(0,0,0));

    for (int i=0; i<poly_array.size(); ++i) {
        Vector3 vec = poly_array[i];
        aabb = aabb.expand(vec);
    }
        
    return aabb;
}

void PolygonTool::end() {
    if (node == NULL) {
        return;
    }

    // cleanup
    if (poly_array.empty()) {
        return;
    }

    AABB aabb = get_aabb();
    Vector3 aabb_size = aabb.get_size();
    Vector3 aabb_min = aabb.get_position();
	Vector3 aabb_max = aabb_min + aabb_size;
    
    // debug aabb mesh
    //MeshInstance *mesh = BUtil::get_singleton()->aabb_to_mesh(aabb);
    //selected->add_child(mesh);
    //parent->set_owner_to_editor(mesh);

    
    // move to the calculated origin
    Vector3 origin = aabb_min + aabb_size / 2.0;
    origin.y += additional_depth;
    
    Vector3 local = node->to_local(origin);
    node->translate(local);
    
    // rotate the node 90
    node->set_rotation_degrees(Vector3(90,0,0));
    
    // set the depth
    float depth = aabb_max.y - aabb_min.y + (additional_depth * 2.0);
    node->set_depth(-depth);
            
    // convert to vec2 array
    // dont need y values
    // and transform to local space
    Vector<Point2> point2_array;
	point2_array.resize(poly_array.size());

    for (int i = 0; i < poly_array.size(); ++i) {
        Vector3 vec = poly_array[i];
        vec -= origin;
        point2_array.set(i, Point2(vec.x, vec.z));
    }
 
    // set the points
    node->set_polygon(point2_array);
    return;
}


/** BRUSH TOOL **/
void BrushTool::init(BEditorPlugin *p_parent) {
    BEditorTool::init(p_parent);
    p_parent->register_tool("Brush", this);
}


bool BrushTool::process_input(Camera *p_camera, const Ref<InputEvent> &p_event) {
    return false;
}

/** RESOURCE TOOL **/
void ResourceTool::init(BEditorPlugin *p_parent) {
    BEditorTool::init(p_parent);
    p_parent->register_tool("Resource", this);
}

/**
 * release the node
 **/
void ResourceTool::end() {
    node = NULL;
}

/**
 * create the node
 **/
bool ResourceTool::start() {
    // get first selected
    Array sel = parent->editor_selection->get_selected_nodes();
    if (sel.empty()) {
        DEBUG_PRINT("no selection");
        return false;
    }
    Node *first = Object::cast_to<Node>(sel[0]);

    // add node to editor
    node = add_node_to_editor();
    if (node == NULL) {
        DEBUG_PRINT("node is null");
        return false;
    };
    first->add_child(node);
    parent->set_owner_to_editor(node);
    return true;
}

bool ResourceTool::process_input(Camera *p_camera, const Ref<InputEvent> &p_event) {
    const InputEventMouseButton *mb = Object::cast_to<InputEventMouseButton>(*p_event);
    const InputEventMouseMotion *mm = Object::cast_to<InputEventMouseMotion>(*p_event);
    bool capture_event = false;

    // left mouse pressed
    if (mb && !tool_active && mb->get_button_index() == BUTTON_LEFT && mb->is_pressed()) {
        if (!start()) return false; // return if fails
        tool_active = true;
        capture_event = true;
    }

    // mouse move drag
    if (tool_active && (mb || mm)) {
        // cast ray
        Dictionary ray = parent->get_ray(p_camera, p_event);
        Transform xform;
        bool result = parent->cast_ray(ray["start"], ray["end"], xform);
        if (result) {
            // modify transform to be identity + pos
            // convert global to local space
            Vector3 origin = xform.get_origin();
            Vector3 local = node->to_local(origin);
            node->translate(local);
        }

        capture_event = true;
    }

    // mouse released
    if (tool_active && mb && !mb->is_pressed()) {
        end();
        tool_active = false;
        capture_event = true;
    }

    return capture_event;
}

Spatial* ResourceTool::add_node_to_editor() {
	String resource_name = parent->ui_opt_res_node->get_item_text(parent->ui_opt_res_node->get_selected());
    Spatial *node;
    if (resource_name == "Spatial") {
        node = memnew(Spatial);
        return node;
    }

    resource_name = parent->ui_opt_res_node->get_selected_metadata();
    if (resource_name != "") {
        Ref<PackedScene> packed_scene = ResourceLoader::load(resource_name);
        node = Object::cast_to<Spatial>(packed_scene->instance());
    }
    return node;
}
		