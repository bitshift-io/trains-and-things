#include "editor_plugin.h"
#include "../polygon_area.h"
#include "scene/gui/control.h"
#include "../util.h"
#include "scene/gui/file_dialog.h"
#include "../globals.h"
#include "../terrain.h"
#include "core/os/input_event.h"
#include "editor_tool.h"
#include "scene/3d/camera.h"
#include "editor/editor_data.h"
//#include "editor/plugins/spatial_editor_plugin.h"
#include "editor/spatial_editor_gizmos.h"
#include "core/bind/core_bind.h"
#include "editor_generate_height_map.h"
#include "editor_generate_normal_map.h"

//#ifdef TOOLS_ENABLED

class PolygonAreaSpatialGizmoPlugin : public CollisionShapeSpatialGizmoPlugin {
	GDCLASS(PolygonAreaSpatialGizmoPlugin, CollisionShapeSpatialGizmoPlugin);

public:
	bool has_gizmo(Spatial *p_spatial);
	String get_name() const;
	//int get_priority() const;
	void redraw(EditorSpatialGizmo *p_gizmo);
	//PolygonAreaSpatialGizmoPlugin();
};

bool PolygonAreaSpatialGizmoPlugin::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BPolygonArea>(p_spatial) != NULL;
}

String PolygonAreaSpatialGizmoPlugin::get_name() const {
	return "PolygonArea";
}

void PolygonAreaSpatialGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

	BPolygonArea *polygon = Object::cast_to<BPolygonArea>(p_gizmo->get_spatial_node());

	p_gizmo->clear();

	Vector<Vector2> points = polygon->get_polygon();
	float depth = polygon->get_depth() * 0.5;

	Vector<Vector3> lines;
	for (int i = 0; i < points.size(); i++) {

		int n = (i + 1) % points.size();
		lines.push_back(Vector3(points[i].x, points[i].y, depth));
		lines.push_back(Vector3(points[n].x, points[n].y, depth));
		lines.push_back(Vector3(points[i].x, points[i].y, -depth));
		lines.push_back(Vector3(points[n].x, points[n].y, -depth));
		lines.push_back(Vector3(points[i].x, points[i].y, depth));
		lines.push_back(Vector3(points[i].x, points[i].y, -depth));
	}

	Ref<Material> material = get_material("shape_material", p_gizmo);

	p_gizmo->add_lines(lines, material);
	p_gizmo->add_collision_segments(lines);
}

//#endif // TOOLS_ENABLED


BEditorPlugin::BEditorPlugin(EditorNode *p_editor) {
    active_tool = NULL;

    // the polgon plugin
    SpatialEditor::get_singleton()->add_gizmo_plugin(Ref<PolygonAreaSpatialGizmoPlugin>(memnew(PolygonAreaSpatialGizmoPlugin)));
}

/**
 * enable forward spatial gui input
 */
bool BEditorPlugin::handles(Object *p_object) const {
    return true;
}

/**
 * capture input from the editor 3d viewport
 */
bool BEditorPlugin::forward_spatial_gui_input(Camera *p_camera, const Ref<InputEvent> &p_event) {
    //ignore if plugin disabled
	if ( active_tool == nullptr ) {
		return EditorPlugin::forward_spatial_gui_input(p_camera, p_event);
    }

	// process input here
	bool captured_event = false;
    //const InputEventMouseButton *mb = Object::cast_to<InputEventMouseButton>(*p_event);
    const InputEventMouseMotion *mm = Object::cast_to<InputEventMouseMotion>(*p_event);

    // pass to tool
    captured_event = active_tool->process_input(p_camera, p_event);

    // TODO 3d cursor/overlay
    if (mm) {

    }

	return captured_event;
}

void BEditorPlugin::_notification(int p_what) {
    switch (p_what) {
        case NOTIFICATION_ENTER_TREE: {
			// load editor dock
            init_editor_dock();
		} break;
		case NOTIFICATION_EXIT_TREE: {
			// exit editor dock
            free_editor_dock();
		} break;
    }
}

void BEditorPlugin::free_editor_dock() {
    // cleanup editor dock
    remove_control_from_docks(control);
}

/**
 * tools register themselves
 */
void BEditorPlugin::register_tool(const String &p_name, BEditorTool *p_tool) {
    ui_opt_tool_mode->add_item(p_name);
    int idx = ui_opt_tool_mode->get_item_count() - 1;
    tool_list[idx] = p_tool;
}

void BEditorPlugin::init_editor_dock() {

    _Directory dir;
    if (!dir.file_exists("res://Editor/dock.tscn")) {
        return;
    }

    // start editor dock
    Ref<PackedScene> packed_scene = ResourceLoader::load("res://Editor/dock.tscn"); // TODO: read path from project.godot
    control = Object::cast_to<Control>(packed_scene->instance());
    add_control_to_dock(DockSlot::DOCK_SLOT_RIGHT_UL, control);

    // get editor stuff
    editor_interface = get_editor_interface();
    editor_selection = editor_interface->get_selection();

    // connect to editor
    editor_selection->connect("selection_changed", this, "editor_selection_changed");

    // interface
    ui_opt_tool_mode = Object::cast_to<OptionButton>(BUtil::get_singleton()->find_child(control, "opt_tool_mode"));
    ui_opt_brush_mode = Object::cast_to<OptionButton>(BUtil::get_singleton()->find_child(control, "opt_brush_mode"));
    ui_opt_res_node = Object::cast_to<OptionButton>(BUtil::get_singleton()->find_child(control, "opt_res_node"));
    ui_label_active_layer = Object::cast_to<Label>(BUtil::get_singleton()->find_child(control, "label_active_layer")); // global
    ui_file_dialog = Object::cast_to<FileDialog>(BUtil::get_singleton()->find_child(control, "file_dialog"));

    Button *ui_btn_add_res = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_add_res"));
    Button *ui_btn_delete_res = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_delete_res"));
    Button *ui_btn_clear_res = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_clear_res"));
    Button *ui_btn_replace_sel = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_replace_sel"));
    Button *ui_btn_swap_sel = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_swap_sel"));
    Button *ui_btn_shuffle_sel = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_shuffle_sel"));
    Button *ui_btn_ground_sel = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_ground_sel"));
    Button *ui_btn_sel_children = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_sel_children"));
    Button *ui_btn_resize_map = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_resize_map"));
    Button *ui_btn_fold_tree = Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_fold_tree"));
    Button *ui_btn_sort_tree= Object::cast_to<Button>(BUtil::get_singleton()->find_child(control, "btn_sort_tree"));

    // buttons
    // make sure these are also in the bind methods function
    ui_opt_res_node->add_item("Spatial");
        
    ui_btn_add_res->connect("pressed", this, "show_file_dialog");
    ui_btn_clear_res->connect("pressed", this, "clear_res");
    ui_btn_delete_res->connect("pressed", this, "delete_res");
    ui_btn_replace_sel->connect("pressed", this, "replace_sel");
    ui_btn_shuffle_sel->connect("pressed", this, "shuffle_sel");
    ui_btn_swap_sel->connect("pressed", this, "swap_sel");
    ui_btn_ground_sel->connect("pressed", this, "ground_sel");
    ui_btn_sel_children->connect("pressed", this, "sel_children");
    ui_btn_resize_map->connect("pressed", this, "resize_map");
    ui_btn_sort_tree->connect("pressed", this, "sort_tree");
    ui_btn_fold_tree->connect("pressed", this, "fold_tree");
    
    ui_file_dialog->connect("files_selected", this, "set_files_selected");

    // add register tools
    register_tool("Off", NULL);
    resource_tool.instance();
    resource_tool->init(this);
    polygon_tool.instance();
    polygon_tool->init(this);
    brush_tool.instance();
    brush_tool->init(this);

    ui_opt_tool_mode->connect("item_selected", this, "set_active_tool");

    // brush
    ui_opt_brush_mode->add_item("Scatter");
    ui_opt_brush_mode->add_item("Rotate");
    ui_opt_brush_mode->add_item("Add Scale");
    ui_opt_brush_mode->add_item("Subtract Scale");
    ui_opt_brush_mode->add_item("Erase");


    // the menu
    bitshift_menu = memnew(MenuButton);
    bitshift_menu->set_flat(false);
    bitshift_menu->set_text(TTR("Bitshift"));
    add_control_to_container(CONTAINER_TOOLBAR, bitshift_menu);

    PopupMenu *p = bitshift_menu->get_popup();
    /*
    p->add_item(TTR("Generate Height Map (legacy)"), BITSHIFT_GENERATE_HEIGHT_MAP);
    p->add_item(TTR("Generate Normal Map (legacy)"), BITSHIFT_GENERATE_NORMAL_MAP);
    p->add_separator();
    */
    p->add_item(TTR("Docs"), BITSHIFT_DOCS);
    p->connect("id_pressed", this, "_menu_option");

    /*
    generate_height_map = memnew(BEditorGenerateHeightMap);
    add_child(generate_height_map);

    generate_normal_map = memnew(BEditorGenerateNormalMap);
    add_child(generate_normal_map);
    */
    return;
}

void BEditorPlugin::_menu_option(int p_option) {

        //_menu_option_confirm(p_option, false);

    switch (p_option) {
        /*
    case BITSHIFT_GENERATE_HEIGHT_MAP:
        generate_height_map->popup();
        break;

    case BITSHIFT_GENERATE_NORMAL_MAP:
        generate_normal_map->popup();
        break;
*/
    case BITSHIFT_DOCS: {
        DirAccess *dir = DirAccess::create(DirAccess::ACCESS_FILESYSTEM);
		String cwd = dir->get_current_dir();
        memdelete(dir);

        String docs_url = "file://" + cwd + "/Docs/index.html";
        OS::get_singleton()->shell_open(docs_url);
    } break;

    }
}

/**
 * show file dialog for adding resources
 */
void BEditorPlugin::show_file_dialog() {
    ui_file_dialog->popup_centered();
}

/**
 * apply resource files from dialog
 */
void BEditorPlugin::set_files_selected(PoolStringArray p_paths) {
    for (int i=0; i<p_paths.size(); ++i) {
        String res = p_paths[i];
        String clean_name = res.get_file();
        String ext = "." + res.get_extension();
        clean_name = clean_name.replace(ext, "");
        ui_opt_res_node->add_item(clean_name);
        int idx = ui_opt_res_node->get_item_count() - 1;
        ui_opt_res_node->set_item_metadata(idx, res);
    }
    return;
}
 
void BEditorPlugin::editor_selection_changed() {
    // editor selection changed
    if (!ui_label_active_layer) return;

    Array sel = editor_selection->get_selected_nodes();
    //parent.set_selected_nodes(sel);

    ui_label_active_layer->set_text("");
    if (sel.size() == 1) {
        ui_label_active_layer->set_text(Object::cast_to<Node>(sel[0])->get_name());}
    if (sel.size() > 1) {
        ui_label_active_layer->set_text(String::num(sel.size()) + " nodes");}
    return;
}

/**
 * editor dock tool activated
 */
void BEditorPlugin::set_active_tool(int p_id) {
    if (p_id == 0) { // off
        active_tool = NULL;
        return;
    }
    DEBUG_PRINT(ui_opt_tool_mode->get_item_text(p_id));
    Variant something = tool_list[p_id];
    active_tool = Object::cast_to<BEditorTool>(something);
}

/**
 * clear the reource list
 */
void BEditorPlugin::clear_res() {
    ui_opt_res_node->clear();
    ui_opt_res_node->add_item("Spatial");
}

/**
 * delete item from resource list
 */
void BEditorPlugin::delete_res() {
    ui_opt_res_node->remove_item( ui_opt_res_node->get_selected() );
    ui_opt_res_node->select(0);
}

/**
 * replace selected objects with item from resource list
 */
void BEditorPlugin::replace_sel() {
	Array sel = editor_selection->get_selected_nodes();
	if (sel.empty()) return;
	
	Spatial *new_node;
	Ref<PackedScene> packed_scene = ResourceLoader::load(ui_opt_res_node->get_selected_metadata());

    // loop each selected item
	for ( int i=0; i<sel.size(); ++i ) { // Spatial *old_node : sel
        Spatial *old_node = Object::cast_to<Spatial>(sel[i]);
		if ( packed_scene != NULL ) { new_node = Object::cast_to<Spatial>(packed_scene->instance()); }
		else { new_node = memnew(Spatial); }
			
		Node *parent = old_node->get_parent();
		parent->add_child(new_node);
		set_owner_to_editor(new_node);
		
		auto xform = old_node->get_global_transform();
		new_node->set_global_transform(xform);
		old_node->queue_delete();
    }
	
}

/**
 * set editor as owner
 */
void BEditorPlugin::set_owner_to_editor(Node *p_node) {
	Node *editor_root = get_tree()->get_edited_scene_root();
	p_node->set_owner(editor_root);
	return;
}

/**
 * shuffle selected objects
 */
void BEditorPlugin::shuffle_sel() {
	Array sel = editor_selection->get_selected_nodes();
	if (sel.size() < 2) return;
	
	// get transform list
	Array xform_list;
	for (int i = 0; i < sel.size(); ++i) {
        Spatial *node = Object::cast_to<Spatial>(sel[i]);
		xform_list.append(node->get_global_transform());
    }
	
	// shuffle
	Math::randomize();
	xform_list.shuffle();
	
	// switch positions
	for (int i = 0; i < sel.size(); ++i) {
        Spatial *node = Object::cast_to<Spatial>(sel[i]);
		node->set_global_transform(xform_list[i]);
	}
}

/**
 * swap selected objects
 */
void BEditorPlugin::swap_sel() {
	Array sel = editor_selection->get_selected_nodes();
	if (sel.size() != 2) {
        DEBUG_PRINT("select only 2 nodes to swap");
        return;
    }

	// get transform list
	Array xform_list;
	for (int i = 0; i < sel.size(); ++i) {
        Spatial *node = Object::cast_to<Spatial>(sel[i]);
		xform_list.append(node->get_global_transform());
    }
        
    // switch positions
    Object::cast_to<Spatial>(sel[0])->set_global_transform(xform_list[1]);
    Object::cast_to<Spatial>(sel[1])->set_global_transform(xform_list[0]);
}

/**
 * move to ground selected objects
 */
void BEditorPlugin::ground_sel() {
 	Array sel = editor_selection->get_selected_nodes();
	if (sel.empty()) return;

	for (int i = 0; i < sel.size(); ++i) {
        Spatial *node = Object::cast_to<Spatial>(sel[i]);
		Transform xform = node->get_global_transform();
		Vector3 start = xform.origin + Vector3(0, 10000, 0);
		Vector3 end = xform.origin - Vector3(0, 10000, 0);
		
		// only want origin
	    // discard rotation/normal
		cast_ray(start, end, xform);
		Vector3 origin = xform.origin;
		Vector3 local = node->to_local(origin);
		node->translate(local);
    }
}

/**
 * cast ray to terrain
 */
bool BEditorPlugin::cast_ray(Vector3 const &p_start, Vector3 const &p_end, Transform &p_xform) {
	// get terrain
    List<Node*> terrain_group;
	get_tree()->get_nodes_in_group("terrain", &terrain_group);
	if (terrain_group.empty()) {
		DEBUG_PRINT("no terrain found");
		return false;
    }
		
	BTerrain *terrain = Object::cast_to<BTerrain>(terrain_group[0]);
	Dictionary col = terrain->raycast(p_start, p_end);
	if (col.empty()) return NULL;
		
	Transform xform = Transform();
	xform.origin = col["position"];
	//p_xform = terrain->move_transform_to_ground(xform);
    p_xform = xform;
    return true;
}

/**
 * select children nodes
 */
void BEditorPlugin::sel_children() {
	Array sel = editor_selection->get_selected_nodes();
	if (sel.empty()) return;
	
    Array new_sel;
	for (int i = 0; i < sel.size(); ++i) {
        Node *node = Object::cast_to<Node>(sel[i]);
        Array children = BUtil::get_singleton()->get_children(node);
        new_sel = BUtil::get_singleton()->join_array(new_sel, children);
    }

    set_editor_selection(new_sel);
}

/**
 * select nodes in editor
 * todo: This might be worth asking the godot guys to add?
 */
void BEditorPlugin::set_editor_selection(const Array &p_array) {
    editor_selection->clear();
    int size = p_array.size();
	for ( int i=0; i<size; ++i ) {
        Node *node = Object::cast_to<Node>(p_array[i]);
        editor_selection->add_node(node);
    }
}

/**
 * sort tree a-z
 */
void BEditorPlugin::sort_tree() {
    Array sel = editor_selection->get_selected_nodes();
    if (sel.empty()) {
        DEBUG_PRINT("no selection");
        return;
    }

    // TODO: Fab help

    /*
    // get children of each branch
    // sort all children in each branch
    Array children = BUtil::get_singleton()->get_branches_as_array(sel);

    children.sort_custom(obj, string)
    */
}

/**
 * fold tree
 */
void BEditorPlugin::fold_tree() {
    Array sel = editor_selection->get_selected_nodes();
    if (sel.empty()) {
        DEBUG_PRINT("no selection");
        return;
    }


    Array editor_nodes;
    // https://docs.microsoft.com/en-us/cpp/cpp/lambda-expressions-in-cpp?view=vs-2019
    auto collect_editor_nodes = [&editor_nodes](Node *p_node) {
        if (p_node->get_owner() != nullptr) {
            editor_nodes.append(p_node);
        }
    };
    for (int i = 0; i < sel.size(); ++i) {
        visit_nodes(sel[i], collect_editor_nodes);
    }

    /* javascript style!
    for_each(sel, [&](Node *p_node, int i) {
        visit_nodes(p_node, collect_editor_nodes);
    });
    */

    // modify selection array
    //sel = BUtil::get_singleton()->get_branches_as_array(sel);

    // fold
    int size = editor_nodes.size();
    for (int i=0; i<size; ++i) {
        Node *child = Object::cast_to<Node>(editor_nodes[i]);
        child->set_display_folded(true);
    }
}

/**
 * resize map
 */
void BEditorPlugin::resize_map() {
    /*
    var resize_scale = BUtil.find_child(dock, "spin_resize_scale").value;
	
	# get terrain
	var terrain_group = get_tree().get_nodes_in_group("terrain");
	if (terrain_group.empty()):
		print("no terrain found");
		return;
	
	print(get_tree().name)
	print(get_tree().get_root().name);
	# scale terrain
	for terrain in terrain_group:
		terrain.size = terrain.size * resize_scale;
	
	# get all other nodes in scene
	print(get_tree().get_root().name);
	
	# scale/move
	
	
	return;
    */
}

/**
 * get ray from camera + event
 */
Dictionary BEditorPlugin::get_ray(Camera *p_camera, const Ref<InputEvent> &p_event) {
    Vector2 mouse_pos = Object::cast_to<InputEventMouse>(*p_event)->get_position();
	Vector3 start = p_camera->project_ray_origin(mouse_pos);
    Vector3 end = start + p_camera->project_ray_normal(mouse_pos) * 10000;
    Dictionary result;
    result["start"] = start;
    result["end"] = end;
    return result;
}

void BEditorPlugin::_bind_methods() {
    // link functions to strings
    ClassDB::bind_method("editor_selection_changed", &BEditorPlugin::editor_selection_changed);
    ClassDB::bind_method("show_file_dialog", &BEditorPlugin::show_file_dialog);
    ClassDB::bind_method("set_files_selected", &BEditorPlugin::set_files_selected);
    ClassDB::bind_method("set_active_tool", &BEditorPlugin::set_active_tool);
    ClassDB::bind_method("clear_res", &BEditorPlugin::clear_res);
    ClassDB::bind_method("delete_res", &BEditorPlugin::delete_res);
    ClassDB::bind_method("replace_sel", &BEditorPlugin::replace_sel);
    ClassDB::bind_method("shuffle_sel", &BEditorPlugin::shuffle_sel);
    ClassDB::bind_method("swap_sel", &BEditorPlugin::swap_sel);
    ClassDB::bind_method("ground_sel", &BEditorPlugin::ground_sel);
    ClassDB::bind_method("sel_children", &BEditorPlugin::sel_children);
    ClassDB::bind_method("resize_map", &BEditorPlugin::resize_map);
    ClassDB::bind_method("fold_tree", &BEditorPlugin::fold_tree);
    ClassDB::bind_method("sort_tree", &BEditorPlugin::sort_tree);
    ClassDB::bind_method("_menu_option", &BEditorPlugin::_menu_option);
}