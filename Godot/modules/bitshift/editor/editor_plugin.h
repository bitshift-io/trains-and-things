
#ifndef BEDITOR_PLUGIN_H
#define BEDITOR_PLUGIN_H

#include <editor/editor_plugin.h>

class FileDialog;
class BEditorTool;
class ResourceTool;
class PolygonTool;
class BrushTool;
class Dictionary;
class EditorInterface;
class EditorSelection;
class MenuButton;
class BEditorGenerateHeightMap;
class BEditorGenerateNormalMap;

/**
 * 	the interface between the gui and the plugin
 *  we do the mini functions in here
 *  tools have their own class for each tool
 */
class BEditorPlugin : public EditorPlugin {
	GDCLASS(BEditorPlugin, EditorPlugin)

public:

	enum MenuOptions {
        BITSHIFT_GENERATE_HEIGHT_MAP,
        BITSHIFT_GENERATE_NORMAL_MAP,
        BITSHIFT_DOCS,
    };

	Control *control; // control root of dock.tcsn
	EditorInterface *editor_interface; // editor interface
	EditorSelection *editor_selection; // editor interface->editor selection

	// interface globals
	Label *ui_label_active_layer;
	FileDialog *ui_file_dialog;
	OptionButton *ui_opt_tool_mode;
	OptionButton *ui_opt_brush_mode;
	OptionButton *ui_opt_res_node;
	BEditorTool *active_tool;
	bool mouse_pressed = false;
	Ref<ResourceTool> resource_tool;
	Ref<PolygonTool> polygon_tool;
	Ref<BrushTool> brush_tool;
	Dictionary tool_list;

	MenuButton* bitshift_menu;
	/*
    BEditorGenerateHeightMap *generate_height_map;
    BEditorGenerateNormalMap *generate_normal_map;
	*/

	BEditorPlugin(EditorNode *p_editor);

	virtual bool forward_spatial_gui_input(Camera *p_camera, const Ref<InputEvent> &p_event);
	bool handles(Object *p_object) const;

	// editor dock stuff bellow here
	void init_editor_dock();
	void free_editor_dock();
	void editor_selection_changed();
	void set_active_tool(int p_id);
	void show_file_dialog();
	void set_files_selected(PoolStringArray p_paths);
	void clear_res();
	void delete_res();
	void replace_sel();
	void shuffle_sel();
	void swap_sel();
	void ground_sel();
	void sel_children();
	void resize_map();
	void fold_tree();
	void sort_tree();
	void set_owner_to_editor(Node *p_node);
	void set_editor_selection(const Array &p_array);
	bool cast_ray(Vector3 const &p_start, Vector3 const &p_end, Transform &p_xform);
	void register_tool(const String &p_name, BEditorTool *p_tool);
	Dictionary get_ray(Camera *p_camera, const Ref<InputEvent> &p_event);
	void _menu_option(int p_option);

protected:
	void _notification(int p_what);
	static void _bind_methods();
};

#endif // BEDITOR_PLUGIN_H