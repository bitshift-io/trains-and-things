#ifndef BEDITOR_TOOL_H
#define BEDITOR_TOOL_H

#include "core/reference.h"

class InputEvent;
class Camera;
class BEditorPlugin;
class Spatial;
class BPolygonArea;

/**
 * 	editor dock tools
 */
class BEditorTool : public Reference {
    GDCLASS(BEditorTool, Reference)

    public:
        BEditorPlugin *parent;
        virtual void init(BEditorPlugin *p_parent) = 0;
        virtual bool process_input(Camera *p_camera, const Ref<InputEvent> &p_event) = 0;

};

/**
 * resource tool for placing resources
 */
class ResourceTool: public BEditorTool {
    GDCLASS(ResourceTool, BEditorTool)
    public:
        virtual void init(BEditorPlugin *p_parent);
        bool start();
        void end();
        virtual bool process_input(Camera *p_camera, const Ref<InputEvent> &p_event);
        Spatial* add_node_to_editor();
    private:
        bool tool_active;
        Spatial *node;
};

/**
 * resource tool for placing resources
 */
class PolygonTool: public BEditorTool {
    GDCLASS(PolygonTool, BEditorTool)
    public:
        virtual void init(BEditorPlugin *p_parent);
        virtual bool process_input(Camera *p_camera, const Ref<InputEvent> &p_event);
        void add_point(const Vector3 &p_point);
        bool check_distance(const Vector3 &p_point);
        bool start();
        AABB get_aabb();
        void end(); 
    private:
    	BPolygonArea *node;
        Array poly_array;
        Vector3 last_point;
        Node *selected;
        int min_distance = 30; // min distance for spacing points
        int additional_depth = 20;
        Vector3 last_terrain_point;
        bool cursor_over_terrain;
        bool tool_active;
};

/**
 * resource tool for placing resources
 */
class BrushTool: public BEditorTool {
    GDCLASS(BrushTool, BEditorTool)
    public:
        virtual void init(BEditorPlugin *p_parent);
        virtual bool process_input(Camera *p_camera, const Ref<InputEvent> &p_event);
};

#endif // BEDITOR_TOOL_H