#include "process_node.h"
#include "scene/main/viewport.h"

void ProcessNode::_notification(int p_what) {
    switch (p_what) {
        case NOTIFICATION_PROCESS: {
            if (execute_method) {
                execute_method();
            }
        } break;
    }
}

void ProcessNode::add_to_scene_tree() {
    BUtil::get_singleton()->get_scene_tree()->get_root()->add_child(this);
}

void ProcessNode::_bind_methods() {
    ClassDB::bind_method(D_METHOD("add_to_scene_tree"), &ProcessNode::add_to_scene_tree);
}