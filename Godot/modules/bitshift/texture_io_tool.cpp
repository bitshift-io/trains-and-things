  
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "texture_io_tool.h"
#include <vector>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"

#ifndef M_PI
#define M_PI 3.141592653
#endif

/**
 * Create a texture and image from scratch and set it up to be drawn into 
 */
bool BTextureIOTool::create_texture(int p_width, int p_height, bool p_use_mipmaps, Image::Format p_format) {
    Ref<ImageTexture> texture_inst;
    texture_inst.instance();

    Ref<Image> image_inst;
    image_inst.instance();
    image_inst->create(p_width, p_height, p_use_mipmaps, p_format);

    texture_inst->create_from_image(image_inst);

    return set_texture(texture_inst);
}

void BTextureIOTool::update_texture() {
    image->unlock();

    Ref<ImageTexture> texture_inst = texture;
    texture_inst->update(image);
    
    image->lock();
}

/*
Ref<ImageTexture> BTextureIOTool::create_texture_from_image() {
    image->unlock();

    Ref<ImageTexture> texture;
    texture.instance();
    texture->create_from_image(image);
    
    image->lock();

    return texture;
}*/


bool BTextureIOTool::set_image(const Ref<Image>& p_image) {
    // unlock old image
    if (image.is_valid() && !image.is_null()) {
        image->unlock();
    }

    image = p_image;
    if (image->is_compressed()) {
        print_line("Texture '" + texture->get_name() + "' is compressed, making it unusable in BTextureIOTool, attempting to decompress");
        image->decompress();
    }

    image->lock();

    //pixel_size = Image::get_format_pixel_size(image->get_format());
    width = image->get_width();
    height = image->get_height();

    width_m1 = width - 1;
    height_m1 = height - 1;

    //r = image->get_data().read();
    return true;
}

bool BTextureIOTool::set_texture(const Ref<Texture2D>& texture) {
    this->texture = texture;
    if (texture.is_null()) {
        image = Ref<Image>();
        width = 0;
        height = 0;
        width_m1 = 0;
        height_m1 = 0;
        return true;
    }

    image = texture->get_data();
    if (image.is_null()) {
        auto name = texture->get_name();
        print_line("Image is null in BTextureIOTool::set_texture: " + name);
        return false;
    }
    if (image->is_compressed()) {
        print_line("Image is compressed and must not be! Check godot texture settings is lossless or lossy. In BTextureIOTool::set_texture: " + texture->get_name());
        return false;
    }

    return set_image(image);
}

void BTextureIOTool::compute_min_max(float& min, float& max) {
    min = FLT_MAX;
    max = -FLT_MAX;

    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            float v = get_pixel(x, y).r;
            min = MIN(min, v);
            max = MAX(max, v);
        }
    }
}

typedef std::vector<float> FloatArray;
typedef std::vector<FloatArray> Matrix;

Matrix getGaussian(int height, int width, double sigma)
{
    Matrix kernel(height, FloatArray(width));
    double sum=0.0;
    int i,j;

    for (i=0 ; i<height ; i++) {
        for (j=0 ; j<width ; j++) {
            kernel[i][j] = exp(-(i*i+j*j)/(2*sigma*sigma))/(2*M_PI*sigma*sigma);
            sum += kernel[i][j];
        }
    }

    for (i=0 ; i<height ; i++) {
        for (j=0 ; j<width ; j++) {
            kernel[i][j] /= sum;
        }
    }

    return kernel;
}

// https://gist.github.com/OmarAflak/aca9d0dc8d583ff5a5dc16ca5cdda86a
void BTextureIOTool::gaussian_blur(int size, float sigma) {
    Matrix filter = getGaussian(size, size, sigma);

    Ref<Image> newImage;
    newImage.instance();
    newImage->create(width, height, false, image->get_format());
    newImage->lock();

    for (uint32_t i=0 ; i<height ; i++) {
        for (uint32_t j=0 ; j<width ; j++) {
            for (uint32_t h=i ; h<i+size ; h++) {
                for (uint32_t w=j ; w<j+size ; w++) {

                    // out of bounds
                    if (w < 0 || h < 0 || w >= width || h >= height) {
                        continue;
                    }

                    Color p = newImage->get_pixel(j, i);


                    Color d = get_pixel(w, h);

                    p += d * filter[h-i][w-j];

                    newImage->set_pixel(j, i, p);
                    //newImage[d][i][j] += filter[h-i][w-j]*image[d][h][w];
                }
            }
        }
    }

    newImage->unlock();
    image = newImage;
}

void BTextureIOTool::render_circle_texel(Vector2& uv, float radius, const Color& color) {
    float cx = uv.x;
    float cy = uv.y;
    float r = radius * width_m1; // assume the image is square
    texture_space_to_pixel_space(cx, cy);

    // https://answers.unity.com/questions/590469/drawing-a-solid-circle-onto-texture.html
    int x, y, px, nx, py, ny, d;
    for (x = 0; x <= r; x++)
    {
        d = (int)Math::ceil(Math::sqrt(r * r - x * x));
        for (y = 0; y <= d; y++)
        {
            px = cx + x;
            nx = cx - x;
            py = cy + y;
            ny = cy - y;

            set_pixel_safe(px, py, color);
            set_pixel_safe(nx, py, color);

            set_pixel_safe(px, ny, color);
            set_pixel_safe(nx, ny, color);
        }
    }  
}

BTextureIOTool::BTextureIOTool() {
    width = 0;
    height = 0;
    width_m1 = 0;
    height_m1 = 0;
}

BTextureIOTool::~BTextureIOTool() {
    // unlock old image
    if (image.is_valid() && !image.is_null()) {
        image->unlock();
    }
}

#pragma GCC diagnostic pop
