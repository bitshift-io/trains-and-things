/*************************************************************************/
/*  particles_material.cpp                                               */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2019 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2019 Godot Engine contributors (cf. AUTHORS.md)    */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "water_material.h"


Mutex *BWaterMaterial::material_mutex = NULL;
SelfList<BWaterMaterial>::List *BWaterMaterial::dirty_materials = NULL;
Map<BWaterMaterial::MaterialKey, BWaterMaterial::ShaderData> BWaterMaterial::shader_map;
BWaterMaterial::ShaderNames *BWaterMaterial::shader_names = NULL;

void BWaterMaterial::init_shaders() {

#ifndef NO_THREADS
	material_mutex = Mutex::create();
#endif

	dirty_materials = memnew(SelfList<BWaterMaterial>::List);

	shader_names = memnew(ShaderNames);

	// wave 1 normal texture
	// wave 2 normal texture
	// uv_texture
	// foam texture
	// caustic_texture
	// reflection texture
	shader_names->extra_texture_names[EXTRA_TEXTURE_WAVE_1_NORMAL] = "texture_wave_1_normal";
	shader_names->extra_texture_names[EXTRA_TEXTURE_WAVE_2_NORMAL] = "texture_wave_2_normal";
	shader_names->extra_texture_names[EXTRA_TEXTURE_UV] = "texture_uv_sampler";
	shader_names->extra_texture_names[EXTRA_TEXTURE_FOAM] = "texture_foam";
	//shader_names->extra_texture_names[EXTRA_TEXTURE_CAUSTIC] = "texture_caustic";
	shader_names->extra_texture_names[EXTRA_TEXTURE_REFLECTION] = "texture_reflection";

	shader_names->texture_caustic = "texture_caustic";
	shader_names->caustic_sampler_scale = "caustic_sampler_scale";

	shader_names->wave_speed = "wave_speed";
	shader_names->wave_a = "wave_a";
	shader_names->wave_b = "wave_b";
	shader_names->wave_c = "wave_c";
	shader_names->normal_sampler_scale = "normal_sampler_scale";
	shader_names->normal_sampler_direction = "normal_sampler_direction";
	shader_names->uv_sampler_scale = "uv_sampler_scale";
	shader_names->uv_sampler_strength = "uv_sampler_strength";
	shader_names->foam_level = "foam_level";
	shader_names->foam_sampler_scale = "foam_sampler_scale";
	shader_names->color_refraction = "color_refraction";
	shader_names->color_deep = "color_deep";
	shader_names->color_shallow = "color_shallow";
	shader_names->normal_depth = "normal_depth";
	shader_names->fresnel_scale = "fresnel_scale";
	shader_names->fresnel_power = "fresnel_power";
	shader_names->reflection_brightness = "reflection_brightness";
}

void BWaterMaterial::finish_shaders() {

#ifndef NO_THREADS
	memdelete(material_mutex);
#endif

	memdelete(dirty_materials);
	dirty_materials = NULL;

	memdelete(shader_names);
}

void BWaterMaterial::_update_shader() {

	dirty_materials->remove(&element);

	MaterialKey mk = _compute_key();
	if (mk.key == current_key.key)
		return; //no update required in the end

	if (shader_map.has(current_key)) {
		shader_map[current_key].users--;
		if (shader_map[current_key].users == 0) {
			//deallocate shader, as it's no longer in use
			VS::get_singleton()->free(shader_map[current_key].shader);
			shader_map.erase(current_key);
		}
	}

	current_key = mk;

	if (shader_map.has(mk)) {

		VS::get_singleton()->material_set_shader(_get_material(), shader_map[mk].shader);
		shader_map[mk].users++;
		return;
	}

	// SHADER HERE
	

	// I've temporarily gimped that shader by 
	// commenting out VARYING_HEIGHT_INTERP lines
	// this seems to cause issues compiling,
	// I work around it by leaving it out
	// refraction might be a bit broken as a result
	// when fixed search for the comment "TODO: ENABLE THIS WHEN VARYING FIXED"
	// and put those lines back in (or just do a diff log to see what the code was like prior to this commit)

	String code = R"<godot_shader>(
		// water_material
		shader_type spatial;
		render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;


		// Wave settings:
		uniform float	wave_speed		 = 0.5; // Speed scale for the waves
		uniform vec4	wave_a			 = vec4(-1.0, 1.0, 0.28, 100.0); 	// xy = Direction, z = Steepness, w = Length
		uniform	vec4	wave_b			 = vec4(0.7, 0.6, 0.3, 80.0);	// xy = Direction, z = Steepness, w = Length
		uniform	vec4	wave_c			 = vec4(-0.8, 1.3, 0.4, 30.0); 	// xy = Direction, z = Steepness, w = Length

		// Surface settings:
		uniform vec2 	normal_sampler_scale 	 = vec2(0.02, 0.02); 			// Scale for the sampler
		uniform vec2	normal_sampler_direction= vec2(0.05, 0.04); 			// Direction and speed for the sampler offset

		uniform sampler2D texture_uv : hint_aniso; 						// UV motion sampler for shifting the normalmap
		uniform vec2 	uv_sampler_scale = vec2(0.25, 0.25); 			// UV sampler scale
		uniform float 	uv_sampler_strength = 0.02; 					// UV shifting strength

		uniform sampler2D texture_wave_1_normal : hint_normal;			// Normalmap sampler A
		uniform sampler2D texture_wave_2_normal : hint_normal;			// Normalmap sampler B

		uniform sampler2D texture_foam : hint_black;					// Foam sampler
		uniform float 	foam_level 		 = 0.5;							// Foam level -> distance from the object (0.0 - 0.5)
		uniform vec2 	foam_sampler_scale = vec2(0.25, 0.25); 			// foam sampler scale

		// Volume settings:
		uniform float 	color_refraction 		 = 0.075;						// Refraction of the water

		uniform vec4 	color_deep : hint_color;						// Color for deep places in the water, medium to dark blue
		uniform vec4 	color_shallow : hint_color;						// Color for lower places in the water, bright blue - green
		uniform float 	beers_law		 = 0.1;							// Beers law value, regulates the blending size to the deep water level
		uniform float 	depth_offset	 = 0.0;						// Offset for the blending
		uniform vec2 invAtan = vec2(0.1591, 0.3183); // for conversion to polar coords

		// Projector for the water caustics:
		uniform mat4	projector;										// Projector matrix, mostly the matric of the sun / directlight
		uniform sampler2DArray texture_caustic : hint_black;			// Caustic sampler, (Texture array with 16 Textures for the animation)
		uniform vec2 	caustic_sampler_scale = vec2(0.25, 0.25); 			// foam sampler scale

		uniform float normal_depth = 1.0;

		// reflection settings
		uniform float fresnel_scale = 1.0;
		uniform float fresnel_power = 1.0;
		uniform float reflection_brightness = 1.0;
		uniform sampler2D texture_reflection : hint_black; // Reflection spheremap

		//varying vec3	VERTEX_PROJECTION; // new height of water		
		//varying vec3 	vertex_normal;									// Vertex normal -> Needed for refraction calculation
		//varying vec3 	vertex_binormal;								// Vertex binormal -> Needed for refraction calculation
		//varying vec3 	vertex_tangent;									// Vertex tangent -> Needed for refraction calculation
		
		//varying float VERTEX_HEIGHT_INTERP;									// Height of the water surface - TODO: ENABLE THIS WHEN VARYING FIXED:
		varying mat4 INVERSE_MODEL_VIEW_PROJECTION; // Inverse ModelViewProjection matrix -> Needed for caustic projection
		/*
		varying vec3 CAMERA_DIRECTION; // direction normal of camera
		varying vec3 CAMERA_POSITION; 
		varying vec3 WORLD_VIEW;
		*/

		vec4 wave(vec4 parameter, vec2 position, float time, inout vec3 tangent, inout vec3 binormal, float amount)
		{
			float	wave_steepness = parameter.z;
			float	wave_length = parameter.w;

			float	k				 = 2.0 * 3.14159265359 / wave_length;
			float 	c 				 = sqrt(9.8 / k);
			vec2	d				 = normalize(parameter.xy);
			float 	f 				 = k * (dot(d, position) - c * time);
			float 	a				 = wave_steepness / k * amount; // brons added amount
			
			tangent.xyz += normalize(vec3(1.0-d.x * d.x * (wave_steepness * sin(f)), d.x * (wave_steepness * cos(f)), -d.x * d.y * (wave_steepness * sin(f))));
			binormal += normalize(vec3(-d.x * d.y * (wave_steepness * sin(f)), d.y * (wave_steepness * cos(f)), 1.0-d.y * d.y * (wave_steepness * sin(f))));
			return vec4(d.x * (a * cos(f)), a * sin(f) * 0.25, d.y * (a * cos(f)), 0.0);
		}

		void vertex() {
			float time = TIME * wave_speed;
			vec4 vertex = vec4(VERTEX, 1.0);
			vec3 vertex_position = (WORLD_MATRIX * vertex).xyz; // in world space

			// calc wave
			vec3 vertex_tangent = vec3(0.0, 0.0, 0.0);
			vec3 vertex_binormal = vec3(0.0, 0.0, 0.0);
			float vertex_color = COLOR.r;
			
			vertex += wave(wave_a, vertex_position.xz, time, vertex_tangent, vertex_binormal, vertex_color);
			vertex += wave(wave_b, vertex_position.xz, time, vertex_tangent, vertex_binormal, vertex_color);
			vertex += wave(wave_c, vertex_position.xz, time, vertex_tangent, vertex_binormal, vertex_color);
			
			vertex_position = vertex.xyz;
			mat4 projection_model_matrix = PROJECTION_MATRIX * MODELVIEW_MATRIX;
			//VERTEX_HEIGHT_INTERP = (projection_model_matrix * vertex).z; // TODO: ENABLE THIS WHEN VARYING FIXED
			

			// output (these are not in world space)
			TANGENT = vertex_tangent;
			BINORMAL = vertex_binormal;
			vec3 vertex_normal = normalize(cross(vertex_binormal, vertex_tangent));
			NORMAL = vertex_normal;
			//UV = vertex.xz * normal_sampler_scale; // replaced uv's
			UV = UV * normal_sampler_scale;

			VERTEX = vertex.xyz;
			
			// calc world view for fragment shader
			//CAMERA_POSITION = CAMERA_MATRIX[3].xyz;
			//WORLD_VIEW = normalize(CAMERA_POSITION - VERTEX); // camera position - abs world position // for reflection etc
			//CAMERA_DIRECTION =  vec3(0.0, 0.0, -1.0) * mat3(CAMERA_MATRIX); // camera looks down neg z
			//VERTEX_PROJECTION = mat3(projection_model_matrix) * VERTEX; // for projection, i think this is projection vertex, not world??
			INVERSE_MODEL_VIEW_PROJECTION = inverse(projection_model_matrix); // for projection caustic effect
		}

		vec2 motion_panner(vec2 p_coordinates, float p_time, vec2 p_speed_direction) {
			return p_coordinates + p_speed_direction * p_time;
		}


		vec2 sample_sphere_map(vec3 direction) {
			vec2 uv = vec2(atan(direction.z, direction.x), asin(direction.y)); // cartesian to polar coordinates
			uv *= invAtan; // move to -0.5 to 0.5 uv space
			uv += 0.5; // move to 0 to 1 uv space
			return uv;
		}


		// Fragment shader:
		void fragment()
		{
	
				// init variables
				vec3 color = vec3(0.0, 0.0, 0.0);
				

				//vec3 vertex_world_normal = NORMAL * mat3(INV_CAMERA_MATRIX); // DEBUG
				//color = vec3(VERTEX_HEIGHT_INTERP);

				// Calculation of the UV with the UV motion sampler
				vec2 uv_offset = normal_sampler_direction * TIME;
				vec2 uv_sampler_uv = UV * uv_sampler_scale + uv_offset;
				vec2 uv_sampler_uv_offset = uv_sampler_strength * texture(texture_uv, uv_sampler_uv).rg * 2.0 - 1.0;
				vec2 uv = UV + uv_sampler_uv_offset;
				
				
				// Normalmap
				// this is in local tangent/texture space
				vec3 normalmap = texture(texture_wave_1_normal, uv - uv_offset*2.0).rgb * 0.75;		// 75 % sampler A
				normalmap += texture(texture_wave_2_normal, uv + uv_offset).rgb * 0.25; // 25 % sampler B
				
				//normalmap = texture(texture_wave_1_normal, UV).rgb; // for DEBUG
/*
				// from Godot
				// which godot does in the lighting, so dont pass this as the normalmap
				// convert the normals into uv space
				
				vec3 uv_normal = normalmap;
				uv_normal.xy = uv_normal.xy * 2.0 - 1.0;
				uv_normal.z = sqrt(max(0.0, 1.0 - dot(uv_normal.xy, uv_normal.xy))); //always ignore Z, as it can be RG packed, Z may be pos/neg, etc.
				uv_normal = normalize(mix(NORMAL, TANGENT * uv_normal.x + BINORMAL * uv_normal.y + NORMAL * uv_normal.z, normal_depth)); // Brons added amount
				

				// Refraction UV:
				// TODO: refraction of 1 and above has artifact line!
				vec3 ref_normal = uv_normal;
				//ref_normal = normalize( mix(NORMAL,TANGENT * NORMALMAP.x + BINORMAL * NORMALMAP.y + NORMAL * NORMALMAP.z, NORMALMAP_DEPTH) );
				//vec3 ref_normalmap = normalmap * 2.0 - 1.0;
				//vec3 ref_normalmap2 = normalize(TANGENT*ref_normalmap.x + BINORMAL*ref_normalmap.y + NORMAL*ref_normalmap.z);
				vec2 ref_uv = SCREEN_UV + (ref_normal.xy * color_refraction); // TODO: ENABLE THIS WHEN VARYING FIXED: / VERTEX_HEIGHT_INTERP; // height of vertex in world space (VERTEX_PROJECTION.z?)
				
				// Ground depth:
				float depth_raw = texture(DEPTH_TEXTURE, ref_uv).r * 2.0 - 1.0;
				float depth = PROJECTION_MATRIX[3][2] / (depth_raw + PROJECTION_MATRIX[2][2]);
				
				float depth_blend = exp((depth+VERTEX.z + depth_offset) * -beers_law);
				depth_blend = clamp(1.0-depth_blend, 0.0, 1.0);
				float depth_blend_pow = clamp(pow(depth_blend, 2.5), 0.0, 1.0);

				depth_blend_pow = 1.0; // TODO: REMOVE THIS LINE - ADDED TO TEMPORARILY HACK FIX WATER

				// Ground color:
				vec3 screen_color = textureLod(SCREEN_TEXTURE, ref_uv, depth_blend_pow * 2.5).rgb;
				vec3 color_deep_screen_mix = mix(screen_color, screen_color * color_deep.rgb, color_deep.a);
				vec3 color_shallow_screen_mix = mix(screen_color, screen_color * color_shallow.rgb, color_shallow.a);
				vec3 dye_color = mix(color_shallow.rgb, color_deep.rgb, depth_blend_pow);
				//color = mix(screen_color * dye_color.rgb, dye_color.rgb * 0.25, depth_blend_pow * 0.5); // old method with no alpha
				color = mix(color_shallow_screen_mix, color_deep_screen_mix, depth_blend_pow);
				//color = vec3(depth_blend_pow);
				//color = screen_color * dye_color.rgb;

				// Caustic screen projection
				vec4 caustic_screenPos = vec4(ref_uv*2.0-1.0, depth_raw, 1.0);
				vec4 caustic_localPos = INVERSE_MODEL_VIEW_PROJECTION * caustic_screenPos;
				caustic_localPos = vec4(caustic_localPos.xyz/caustic_localPos.w, caustic_localPos.w);
				vec2 caustic_uv = (caustic_localPos.xz / vec2(1024.0) + 0.5) * caustic_sampler_scale;
				vec4 caustic_color = texture(texture_caustic, vec3(caustic_uv, mod(TIME*14.0, 16.0)));
				color *= 1.0 + pow(caustic_color.r, 1.50) * (1.0-depth_blend) * 6.0;
				
				// proximity/edge fade
				float proximity_fade_distance = 2.0;
				float depth_tex = textureLod(DEPTH_TEXTURE, SCREEN_UV, 0.0).r * 2.0 - 1.0;
				vec4 world_pos = INV_PROJECTION_MATRIX * vec4(SCREEN_UV * 2.0 - 1.0, depth_tex, 1.0);
				world_pos.xyz /= world_pos.w;
				float proximity_fade = clamp(1.0-smoothstep(world_pos.z+proximity_fade_distance, world_pos.z, VERTEX.z), 0.0, 1.0);
				ALPHA *= proximity_fade;

				// foam
				//float foam_distance = 4.0;
				//foam_sampler_scale
				float foam_mix = clamp(1.0-smoothstep(world_pos.z+foam_level, world_pos.z, VERTEX.z), 0.0, 1.0);
				vec3 foam_tex = texture(texture_foam, (UV*foam_sampler_scale) - uv_offset).rgb;
				color = mix(foam_tex, color, foam_mix);
				
				
				// reflection spherical map
				// vertex and normal are in screen space
				// hence vertex is the eye vector
				// ref_eye_normal is the reflected eye vector in screen space
				// hence world_reflect is converting screen space to world space
				vec3 ref_eye_normal = normalize(reflect(VIEW, uv_normal));
				vec3 world_reflect = normalize( mat3(CAMERA_MATRIX) * ref_eye_normal );
				vec2 sphere_uv = sample_sphere_map(world_reflect);
				vec3 sphere_map = texture(texture_reflection, sphere_uv).rgb;
				
				// reflection note, if we get funky colors in the reflection, its due to compression of textures
				// change the compression settings till it looks right!
				
				// fresnel to reflection
				vec3 world_normal = normalize(uv_normal * mat3(INV_CAMERA_MATRIX));
				float fresnel = fresnel_scale * pow(1.0 - dot(VIEW, uv_normal), fresnel_power); // dot(WORLD_VIEW, world_normal)
				fresnel = clamp(fresnel, 0.0, 1.0);
				color += sphere_map * fresnel * reflection_brightness;
				
				// DEBUG
				//color = world_normal;
				//color = sphere_map;
				//color = vec3(fresnel);
				//color = uv_normal;
				//color = VIEW;
				//color = -CAMERA_DIRECTION;
				//color = WORLD_VIEW;
				//color = camera_dir_world;
				//color = world_normal;
				//color = vec3(depth_blend_pow); // depth data
				//color = vec3(proximity_fade); // edge fade data
				//color = vec3(foam_mix); // edge fade data
				
				// Set all values:
				
				METALLIC = 0.2;
				ROUGHNESS = 0.2;
				SPECULAR = 0.3 + depth_blend_pow * 0.4;
				NORMALMAP = normalmap;
				NORMALMAP_DEPTH = normal_depth;


				ALBEDO = color;

			*/

			// TODO: TEMPORARILY HACK FIX WATER		
			METALLIC = 0.2;
			ROUGHNESS = 0.2;
			ALPHA = 1.0;
			ALBEDO = color_deep.rgb;
			SPECULAR = 0.3;
			NORMALMAP = normalmap; //vec3(0, 1, 0);
			NORMALMAP_DEPTH = 1.0;	
		}

	)<godot_shader>";
	// SHADER END


	ShaderData shader_data;
	shader_data.shader = VS::get_singleton()->shader_create();
	shader_data.users = 1;

	VS::get_singleton()->shader_set_code(shader_data.shader, code);

	shader_map[mk] = shader_data;

	VS::get_singleton()->material_set_shader(_get_material(), shader_data.shader);
}

void BWaterMaterial::flush_changes() {

	if (material_mutex)
		material_mutex->lock();

	while (dirty_materials->first()) {

		dirty_materials->first()->self()->_update_shader();
	}

	if (material_mutex)
		material_mutex->unlock();
}

void BWaterMaterial::_queue_shader_change() {

	if (material_mutex)
		material_mutex->lock();

	if (!element.in_list()) {
		dirty_materials->add(&element);
	}

	if (material_mutex)
		material_mutex->unlock();
}

bool BWaterMaterial::_is_shader_dirty() const {

	bool dirty = false;

	if (material_mutex)
		material_mutex->lock();

	dirty = element.in_list();

	if (material_mutex)
		material_mutex->unlock();

	return dirty;
}

// start shader item functions
// these are assigned into the shader variables using get/set

void BWaterMaterial::set_wave_speed(const float &p_val) {
	wave_speed = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->wave_speed, p_val);
}

float BWaterMaterial::get_wave_speed() const {
	return wave_speed;
}

void BWaterMaterial::set_wave_a(const Vector4 &p_val) {
	wave_a = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->wave_a, p_val);
}

Vector4 BWaterMaterial::get_wave_a() const {
	return wave_a;
}

void BWaterMaterial::set_wave_b(const Vector4 &p_val) {
	wave_b = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->wave_b, p_val);
}

Vector4 BWaterMaterial::get_wave_b() const {
	return wave_b;
}

void BWaterMaterial::set_wave_c(const Vector4 &p_val) {
	wave_c = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->wave_c, p_val);
}

Vector4 BWaterMaterial::get_wave_c() const {
	return wave_c;
}

void BWaterMaterial::set_normal_sampler_scale(const Vector2 &p_val) {
	normal_sampler_scale = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->normal_sampler_scale, p_val);
}

Vector2 BWaterMaterial::get_normal_sampler_scale() const {
	return normal_sampler_scale;
}

void BWaterMaterial::set_normal_sampler_direction(const Vector2 &p_val) {
	normal_sampler_direction = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->normal_sampler_direction, p_val);
}

Vector2 BWaterMaterial::get_normal_sampler_direction() const {
	return normal_sampler_direction;
}

void BWaterMaterial::set_uv_sampler_scale(const Vector2 &p_val) {
	uv_sampler_scale = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->uv_sampler_scale, p_val);
}

Vector2 BWaterMaterial::get_uv_sampler_scale() const {
	return uv_sampler_scale;
}

void BWaterMaterial::set_uv_sampler_strength(const float &p_val) {
	uv_sampler_strength = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->uv_sampler_strength, p_val);
}

float BWaterMaterial::get_uv_sampler_strength() const {
	return uv_sampler_strength;
}


void BWaterMaterial::set_foam_level(const float &p_val) {
	foam_level = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->foam_level, p_val);
}

float BWaterMaterial::get_foam_level() const {
	return foam_level;
}

void BWaterMaterial::set_foam_sampler_scale(const Vector2 &p_val) {
	foam_sampler_scale = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->foam_sampler_scale, p_val);
}

Vector2 BWaterMaterial::get_foam_sampler_scale() const {
	return foam_sampler_scale;
}

void BWaterMaterial::set_color_refraction(const float &p_val) {
	color_refraction = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->color_refraction, p_val);
}

float BWaterMaterial::get_color_refraction() const {
	return color_refraction;
}


void BWaterMaterial::set_color_deep(const Color &p_val) {
	color_deep = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->color_deep, p_val);
}

Color BWaterMaterial::get_color_deep() const {
	return color_deep;
}

void BWaterMaterial::set_color_shallow(const Color &p_val) {
	color_shallow = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->color_shallow, p_val);
}

Color BWaterMaterial::get_color_shallow() const {
	return color_shallow;
}

void BWaterMaterial::set_normal_depth(const float &p_val) {
	normal_depth = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->normal_depth, p_val);
}

float BWaterMaterial::get_normal_depth() const {
	return normal_depth;
}

void BWaterMaterial::set_fresnel_scale(const float &p_val) {
	fresnel_scale = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->fresnel_scale, p_val);
}

float BWaterMaterial::get_fresnel_scale() const {
	return fresnel_scale;
}

void BWaterMaterial::set_fresnel_power(const float &p_val) {
	fresnel_power = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->fresnel_power, p_val);
}

float BWaterMaterial::get_fresnel_power() const {
	return fresnel_power;
}

void BWaterMaterial::set_reflection_brightness(const float &p_val) {
	reflection_brightness = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->reflection_brightness, p_val);
}

float BWaterMaterial::get_reflection_brightness() const {
	return reflection_brightness;
}

void BWaterMaterial::set_caustic_sampler_scale(const Vector2 &p_val) {
	caustic_sampler_scale = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->caustic_sampler_scale, p_val);
}

Vector2 BWaterMaterial::get_caustic_sampler_scale() const {
	return caustic_sampler_scale;
}

void BWaterMaterial::set_texture_caustic(const Ref<Texture> &p_val) {
	texture_caustic = p_val;
	VS::get_singleton()->material_set_param(_get_material(), shader_names->texture_caustic, p_val);
}

Ref<Texture> BWaterMaterial::get_texture_caustic() const {
	return texture_caustic;
}

void BWaterMaterial::set_extra_texture(ExtraTextureParam p_param, const Ref<Texture> &p_texture) {

	ERR_FAIL_INDEX(p_param, EXTRA_TEXTURE_MAX);
	
	extra_textures[p_param] = p_texture;
	RID rid = p_texture.is_valid() ? p_texture->get_rid() : RID();
	VS::get_singleton()->material_set_param(_get_material(), shader_names->extra_texture_names[p_param], rid);
	
	_queue_shader_change();
	_change_notify();
}

Ref<Texture> BWaterMaterial::get_extra_texture(ExtraTextureParam p_param) const {

	ERR_FAIL_INDEX_V(p_param, EXTRA_TEXTURE_MAX, Ref<Texture>());
	return extra_textures[p_param];
}
// end shader item functions


RID BWaterMaterial::get_shader_rid() const {

	ERR_FAIL_COND_V(!shader_map.has(current_key), RID());
	return shader_map[current_key].shader;
}

Shader::Mode BWaterMaterial::get_shader_mode() const {

	return Shader::MODE_SPATIAL;
}

void BWaterMaterial::_bind_methods() {
	// bind shader names to function names
	// this is for use in the editor

	// extra textures
	ClassDB::bind_method(D_METHOD("set_extra_texture", "texture"), &BWaterMaterial::set_extra_texture);
	ClassDB::bind_method(D_METHOD("get_extra_texture"), &BWaterMaterial::get_extra_texture);


	ClassDB::bind_method(D_METHOD("set_texture_caustic", "texture_caustic"), &BWaterMaterial::set_texture_caustic);
	ClassDB::bind_method(D_METHOD("get_texture_caustic"), &BWaterMaterial::get_texture_caustic);
		
	ClassDB::bind_method(D_METHOD("set_caustic_sampler_scale", "caustic_sampler_scale"), &BWaterMaterial::set_caustic_sampler_scale);
	ClassDB::bind_method(D_METHOD("get_caustic_sampler_scale"), &BWaterMaterial::get_caustic_sampler_scale);

	// the global speed the wave moves
	ClassDB::bind_method(D_METHOD("set_wave_speed", "wave_speed"), &BWaterMaterial::set_wave_speed);
	ClassDB::bind_method(D_METHOD("get_wave_speed"), &BWaterMaterial::get_wave_speed);

	// wave A
	// xy = Direction, z = Steepness, w = Length
	ClassDB::bind_method(D_METHOD("set_wave_a", "wave_a"), &BWaterMaterial::set_wave_a);
	ClassDB::bind_method(D_METHOD("get_wave_a"), &BWaterMaterial::get_wave_a);
	
	// wave B
	// xy = Direction, z = Steepness, w = Length
	ClassDB::bind_method(D_METHOD("set_wave_b", "wave_b"), &BWaterMaterial::set_wave_b);
	ClassDB::bind_method(D_METHOD("get_wave_b"), &BWaterMaterial::get_wave_b);
	
	// wave C
	// xy = Direction, z = Steepness, w = Length
	ClassDB::bind_method(D_METHOD("set_wave_c", "wave_c"), &BWaterMaterial::set_wave_c);
	ClassDB::bind_method(D_METHOD("get_wave_c"), &BWaterMaterial::get_wave_c);

	ClassDB::bind_method(D_METHOD("set_normal_depth", "normal_depth"), &BWaterMaterial::set_normal_depth);
	ClassDB::bind_method(D_METHOD("get_normal_depth"), &BWaterMaterial::get_normal_depth);
	
	ClassDB::bind_method(D_METHOD("set_foam_level", "foam_level"), &BWaterMaterial::set_foam_level);
	ClassDB::bind_method(D_METHOD("get_foam_level"), &BWaterMaterial::get_foam_level);

	ClassDB::bind_method(D_METHOD("set_foam_sampler_scale", "foam_sampler_scale"), &BWaterMaterial::set_foam_sampler_scale);
	ClassDB::bind_method(D_METHOD("get_foam_sampler_scale"), &BWaterMaterial::get_foam_sampler_scale);
	
	ClassDB::bind_method(D_METHOD("set_color_refraction", "refraction"), &BWaterMaterial::set_color_refraction);
	ClassDB::bind_method(D_METHOD("get_color_refraction"), &BWaterMaterial::get_color_refraction);
	
	ClassDB::bind_method(D_METHOD("set_color_deep", "color_deep"), &BWaterMaterial::set_color_deep);
	ClassDB::bind_method(D_METHOD("get_color_deep"), &BWaterMaterial::get_color_deep);
	
	ClassDB::bind_method(D_METHOD("set_color_shallow", "color_shallow"), &BWaterMaterial::set_color_shallow);
	ClassDB::bind_method(D_METHOD("get_color_shallow"), &BWaterMaterial::get_color_shallow);
	
	ClassDB::bind_method(D_METHOD("set_fresnel_scale", "fresnel_scale"), &BWaterMaterial::set_fresnel_scale);
	ClassDB::bind_method(D_METHOD("get_fresnel_scale"), &BWaterMaterial::get_fresnel_scale);
	
	ClassDB::bind_method(D_METHOD("set_fresnel_power", "fresnel_power"), &BWaterMaterial::set_fresnel_power);
	ClassDB::bind_method(D_METHOD("get_fresnel_power"), &BWaterMaterial::get_fresnel_power);
	
	ClassDB::bind_method(D_METHOD("set_reflection_brightness", "reflection_brightness"), &BWaterMaterial::set_reflection_brightness);
	ClassDB::bind_method(D_METHOD("get_reflection_brightness"), &BWaterMaterial::get_reflection_brightness);
	
	ClassDB::bind_method(D_METHOD("set_uv_sampler_strength", "uv_sampler_strength"), &BWaterMaterial::set_uv_sampler_strength);
	ClassDB::bind_method(D_METHOD("get_uv_sampler_strength"), &BWaterMaterial::get_uv_sampler_strength);
	
	ClassDB::bind_method(D_METHOD("set_uv_sampler_scale", "uv_sampler_scale"), &BWaterMaterial::set_uv_sampler_scale);
	ClassDB::bind_method(D_METHOD("get_uv_sampler_scale"), &BWaterMaterial::get_uv_sampler_scale);

	ClassDB::bind_method(D_METHOD("set_normal_sampler_scale", "normal_sampler_scale"), &BWaterMaterial::set_normal_sampler_scale);
	ClassDB::bind_method(D_METHOD("get_normal_sampler_scale"), &BWaterMaterial::get_normal_sampler_scale);

	ClassDB::bind_method(D_METHOD("set_normal_sampler_direction", "normal_sampler_scale"), &BWaterMaterial::set_normal_sampler_direction);
	ClassDB::bind_method(D_METHOD("get_normal_sampler_direction"), &BWaterMaterial::get_normal_sampler_direction);

	// editor gui
	ADD_GROUP("Wave", "wave_");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "wave_wave_speed"), "set_wave_speed", "get_wave_speed");
	ADD_PROPERTY(PropertyInfo(Variant::QUAT, "wave_wave_a"), "set_wave_a", "get_wave_a");
	ADD_PROPERTY(PropertyInfo(Variant::QUAT, "wave_wave_b"), "set_wave_b", "get_wave_b");
	ADD_PROPERTY(PropertyInfo(Variant::QUAT, "wave_wave_c"), "set_wave_c", "get_wave_c");
	

	ADD_GROUP("Normal", "norm_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "norm_wave_1_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_WAVE_1_NORMAL);
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "norm_wave_2_normal", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_WAVE_2_NORMAL);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "norm_normal_depth"), "set_normal_depth", "get_normal_depth");	
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "norm_normal_sampler_scale"), "set_normal_sampler_scale", "get_normal_sampler_scale");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "norm_normal_sampler_direction"), "set_normal_sampler_direction", "get_normal_sampler_direction");
	

	ADD_GROUP("UV Sampler", "uv_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "uv_uv_texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_UV);
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "uv_uv_sampler_scale"), "set_uv_sampler_scale", "get_uv_sampler_scale");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "uv_uv_sampler_strength"), "set_uv_sampler_strength", "get_uv_sampler_strength");

	ADD_GROUP("Color", "color_");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "color_refraction"), "set_color_refraction", "get_color_refraction");
	//ADD_PROPERTY(PropertyInfo(Variant::REAL, "color_beers_law"), "set_beers_law", "get_beers_law");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "color_deep"), "set_color_deep", "get_color_deep");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "color_shallow"), "set_color_shallow", "get_color_shallow");

	ADD_GROUP("Foam", "foam_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "foam_foam_texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_FOAM);
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "foam_sampler_scale"), "set_foam_sampler_scale", "get_foam_sampler_scale");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "foam_level"), "set_foam_level", "get_foam_level");

	/*
	pi.type = Variant::OBJECT;
				pi.hint = PROPERTY_HINT_RESOURCE_TYPE;
				pi.hint_string = "TextureArray";
				*/
	ADD_GROUP("Caustic", "caustic_");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "caustic_texture_caustic", PROPERTY_HINT_RESOURCE_TYPE, "TextureArray"), "set_texture_caustic", "get_texture_caustic");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR2, "caustic_sampler_scale"), "set_caustic_sampler_scale", "get_caustic_sampler_scale");
	// projector

	ADD_GROUP("Surface", "surface_");
	ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "surface_reflection_texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_extra_texture", "get_extra_texture", EXTRA_TEXTURE_REFLECTION);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "surface_fresnel_scale"), "set_fresnel_scale", "get_fresnel_scale");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "surface_fresnel_power"), "set_fresnel_power", "get_fresnel_power");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "surface_reflection_brightness"), "set_reflection_brightness", "get_reflection_brightness");

	// inv_tan  <- whats this?

	// textures 
	BIND_CONSTANT(EXTRA_TEXTURE_WAVE_1_NORMAL);
	BIND_CONSTANT(EXTRA_TEXTURE_WAVE_2_NORMAL);
	BIND_CONSTANT(EXTRA_TEXTURE_UV);
	BIND_CONSTANT(EXTRA_TEXTURE_FOAM);
	//BIND_CONSTANT(EXTRA_TEXTURE_CAUSTIC);
	BIND_CONSTANT(EXTRA_TEXTURE_REFLECTION);
}

BWaterMaterial::BWaterMaterial() :
		element(this) {

	// set shader defaults
	set_wave_speed(0.5);
	set_wave_a(Vector4(-1,1,0.2,200));
	set_wave_b(Vector4(0.7,0.6,0.25,135));
	set_wave_c(Vector4(-0.8,1.3,0.3,91));

	set_normal_sampler_scale(Vector2(30,30));
	set_normal_sampler_direction(Vector2(0.05,0.04));

	set_uv_sampler_scale(Vector2(0.25,0.25));
	set_uv_sampler_strength(0.02);

	set_foam_level(4.0);
	set_foam_sampler_scale(Vector2(5,5));
	set_caustic_sampler_scale(Vector2(30,30));
	set_color_refraction(0.7);

	set_color_deep(Color(0.082, 0.18, 0.295, 0.98)); // Color(21, 46, 75, 252)
	set_color_shallow(Color(0.07, 0.353, 0.4, 0.9)); // Color(18, 90, 103, 231)

	set_normal_depth(2.0);
	set_fresnel_scale(1.0);
	set_fresnel_power(20.0);
	set_reflection_brightness(1000.0);

	current_key.key = 0;
	current_key.invalid_key = 1;

	_queue_shader_change();
}

BWaterMaterial::~BWaterMaterial() {

	if (material_mutex)
		material_mutex->lock();

	if (shader_map.has(current_key)) {
		shader_map[current_key].users--;
		if (shader_map[current_key].users == 0) {
			//deallocate shader, as it's no longer in use
			VS::get_singleton()->free(shader_map[current_key].shader);
			shader_map.erase(current_key);
		}

		VS::get_singleton()->material_set_shader(_get_material(), RID());
	}

	if (material_mutex)
		material_mutex->unlock();
}
