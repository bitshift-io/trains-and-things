
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "thread_pool.h"
#include "util.h"
#include "process_node.h"
#include "database.h"
#include "core/os/os.h"

BThreadPool *BThreadPool::singleton=NULL;

void _thread_function(void *self) {
    BThreadPool* pool = (BThreadPool*)self;
    pool->_thread_function();
}

void BThreadPool::_thread_function() {
    while(1) {
        ITask* task = NULL;

        // get from the queue
        // https://stackoverflow.com/questions/16350473/why-do-i-need-stdcondition-variable
        {
            //MutexRAII lock(tasks_mutex);

            std::unique_lock<std::mutex> l(tasks_mutex);
            while (tasks.size() <= 0) {
                condition.wait(l);

                // check for termination
                if (!running) {
                    return;
                }
            }

            if (tasks.size() > 0) {
                task = tasks[0];
                tasks.pop_front();
                ++working_threads;
            }
        }

        if (task) {
            task->execute();

            // push to complete list
            {
                //MutexRAII lock(complete_tasks_mutex);
                std::lock_guard<std::mutex> l(complete_tasks_mutex);
                complete_tasks.push_back(task);
            }

            --working_threads;
        }
    }
}

int BThreadPool::get_thread_count() const {
    return threads.size();
}

void BThreadPool::add_task(ITask* p_task, Priority p_priority) {
    // TODO: p_priority determines where in the tasks list we push this task:
    // high priority at the end of the high priority tasks - higher than all normal tasks etc...
    //MutexRAII lock(tasks_mutex);
    std::unique_lock<std::mutex> l(tasks_mutex);
    tasks.push_back(p_task);
    condition.notify_one();
}

void BThreadPool::process() {
    if (!running)
        return;

    if (complete_tasks_mutex.try_lock() == false)
        return;

    while (complete_tasks.front()) {
        List<ITask*>::Element *E = complete_tasks.front();
        ITask* task = E->get();
        complete_tasks.erase(E);

        task->complete();
        task->destroy();
    }
  
    complete_tasks_mutex.unlock();
}

bool BThreadPool::has_tasks() {
    std::unique_lock<std::mutex> l(tasks_mutex);
    std::unique_lock<std::mutex> c(complete_tasks_mutex);
    bool _has_tasks = tasks.size() || complete_tasks.size() || working_threads;
    return _has_tasks;
}

void BThreadPool::wait_all() {
    if (!running)
        return;

    do {
        process();        
    } while (has_tasks());
}

void BThreadPool::_bind_methods() {
    ClassDB::bind_method(D_METHOD("wait_all"), &BThreadPool::wait_all);
    ClassDB::bind_method(D_METHOD("has_tasks"), &BThreadPool::has_tasks);
}

BThreadPool::BThreadPool() {
    singleton = this;
    running = true;
    working_threads = 0;

    // create threads
    int threadCount = OS::get_singleton()->get_processor_count();
    for (int i = 0; i < threadCount; ++i) {
        Thread* thread = Thread::create(::_thread_function, this);
        threads.push_back(thread);
    }

    process_node = memnew(ProcessNode(&BThreadPool::process, this));
}

BThreadPool::~BThreadPool() {
    running = false;
    //memdelete(update_node); // does this get cleaned up by the scene tree?

    condition.notify_all();

    // clean up threads
    for (int i = 0; i < threads.size(); ++i) {
        Thread::wait_to_finish(threads[i]);
        memdelete(threads[i]);
    }

    // TODO:
    // what about non-finish tasks?! if tasks exist in complete_tasks here we have a memory leak!
}

