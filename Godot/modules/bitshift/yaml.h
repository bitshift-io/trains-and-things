#ifndef BYAML_H
#define BYAML_H

#include "core/reference.h"

class BYAML : public Reference {

    GDCLASS(BYAML, Reference)

public:
	static void _bind_methods();

	BYAML();
	~BYAML();

	String print(Variant p_value);
	Error parse(const String &p_text, Variant &r_ret, String &r_err_str, int &r_err_line);
	Error parse_file(const String& p_path, Variant &r_ret, String &r_err_str, int &r_err_line);

	//static String print(const Variant &p_var, const String &p_indent = "", bool p_sort_keys = true);
	//static Error parse(const String &p_json, Variant &r_ret, String &r_err_str, int &r_err_line);
};


#endif