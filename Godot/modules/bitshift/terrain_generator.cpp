 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "terrain_generator.h"
#include "util.h"
#include "database.h"
#include "lod_group.h"
#include "thread_pool.h"
#include "core/bind/core_bind.h"
#include "core/ustring.h"
#include "scene/main/node.h"
#include "scene/main/timer.h"
#include "scene/2d/canvas_item.h"
#include "scene/3d/spatial.h"
#include "scene/3d/mesh_instance.h"
#include "scene/3d/physics_body.h"
#include "scene/3d/collision_shape.h"
#include "scene/resources/texture.h"
#include "core/core_string_names.h"
#include "scene/resources/height_map_shape.h"

// same as in BWater! if you fix something in here, you might need to fix in there!
void BTerrainGenerator::create_lod(MeshInputData& meshInputData, int p_flags) {
    flags = p_flags;
    mesh_input_data = meshInputData;
     _create_lod();
}

void BTerrainGenerator::_create_lod() {
    start_time = OS::get_singleton()->get_ticks_usec();

    int chunks = MAX(1, Math::ceil(mesh_input_data.size / mesh_input_data.chunk_size));
    int total_chunks = chunks * chunks;

    float chunk_step = mesh_input_data.size / chunks;
    float uv_step = 1.0 / chunks;
    Vector3 origin(-(mesh_input_data.size - chunk_step)/2.0, 0.0, -(mesh_input_data.size - chunk_step)/2.0); // minus one chunk
    
    // create LOD groups
    for (int tc = 0; tc < total_chunks; ++tc) {
        BLODGroup* lod_group = memnew(BLODGroup);
        lod_group->set_enabled_in_editor(true);
        mesh_input_data.parent->add_child(lod_group);

        String name = "Terrain_LODGroup_" + itos(tc);
        lod_group->set_name(name);

        lod_group->set_lod_range_size(mesh_input_data.lods.size());
        for (unsigned int l = 0; l < mesh_input_data.lods.size(); ++l) {
            auto lod = mesh_input_data.lods[l];
            lod_group->set_lod_range_near(l, lod.near);
            lod_group->set_lod_range_far(l, lod.far);
            lod_group->set_lod_range_name(l, lod.name);
        }

        lod_groups.push_back(lod_group);
    }

    // for each LOD
    // start by generating the low quality LOD's first so we can render something quickly!
    for (int l = mesh_input_data.lods.size() - 1; l >= 0; --l) {
        auto lod = mesh_input_data.lods[l];

        int resolution = mesh_input_data.size * lod.quads_per_metre;

        // for each chunk
        int chunk_idx = 0;
        for (int y = 0; y < chunks; ++y)
        {
            for (int x = 0; x < chunks; ++x)
            {
                task_count++;

                BLODGroup* lod_group = lod_groups[chunk_idx];

                Chunk* chunk = memnew(Chunk);
                chunk->x = x;
                chunk->y = y;
                chunk->uv_step = uv_step;
                chunk->chunk_step = chunk_step;
                chunk->origin = origin;
                chunk->resolution = resolution;
                chunk->chunks = chunks;

                // compute UV transform
                Transform uvXform;
                uvXform = uvXform.scaled(Vector3(uv_step, uv_step, 1.0));
                uvXform.origin = Vector3(chunk->x * uv_step, y * uv_step, 0.0);   // position 
                chunk->uvXform = uvXform;

                // create transform - this moves the chunk so the centre of the chunk is at the origin
                Transform xform;
                xform = xform.scaled(Vector3(chunk_step, 1.0, chunk_step));
                xform.origin = Vector3(-chunk_step * 0.5, 0, -chunk_step * 0.5);

                // setup the mesh instance for the chunk
                MeshInstance* meshInst = memnew(MeshInstance());
                chunk->meshInst = meshInst;
                String chunkName = lod.name;
                meshInst->set_name(chunkName);
                
                if (lod_group) {
                    lod_group->add_child(meshInst);
                }
                meshInst->set_transform(xform);

                bool visible = (l == (mesh_input_data.lods.size() - 1));
                meshInst->set_visible(visible);

                // move the LOD group into the centre of the chunk
                Transform lod_group_xform;
                lod_group_xform.origin = Vector3((x + 0.5) * chunk_step, 0, (y + 0.5) * chunk_step) + origin;
                lod_group->set_transform(lod_group_xform);

                if (multithreaded) {
                    Task* task = Task::Create(&BTerrainGenerator::create_chunk, this, chunk);
                    task->set_complete(&BTerrainGenerator::create_chunk_complete, this, chunk);
                    BThreadPool::get_singleton()->add_task(task);
                }
                else {
                    create_chunk(chunk);
                    create_chunk_complete(chunk);
                }

                // create skirt
                if (!!(mesh_input_data.flags & F_SKIRT)) {
                    if (x == 0 || x == (chunks - 1) || y == 0 || y == (chunks - 1)) {
                        task_count++;

                        Skirt* skirt = memnew(Skirt);
                        skirt->resolution = resolution;
                        skirt->chunks = chunks;
                        skirt->uvXform = uvXform;

                        skirt->edges = 0;
                        skirt->edgeCount = 0;
                        if (x == 0) {
                            skirt->edges |= E_LEFT;
                            skirt->edgeCount++;
                        }
                        if (x == (chunks - 1)) {
                            skirt->edges |= E_RIGHT;
                            skirt->edgeCount++;
                        }
                        if (y == 0) {
                            skirt->edges |= E_BOTTOM;
                            skirt->edgeCount++;
                        }
                        if (y == (chunks - 1)) {
                            skirt->edges |= E_TOP;
                            skirt->edgeCount++;
                        }

                        // setup the mesh instance for the chunk
                        // attach it as a child of the terrain chunk it belongs too
                        MeshInstance* meshInst2 = memnew(MeshInstance());
                        skirt->meshInst = meshInst2;
                        meshInst2->set_name("skirt");
                        if (meshInst) {
                            meshInst->add_child(meshInst2);
                        }

                        if (multithreaded) {
                            Task* task = Task::Create(&BTerrainGenerator::create_skirt, this, skirt);
                            task->set_complete(&BTerrainGenerator::create_skirt_complete, this, skirt);
                            BThreadPool::get_singleton()->add_task(task);
                        }
                        else {
                            create_skirt(skirt);
                            create_skirt_complete(skirt);
                        }
                    }
                }

                ++chunk_idx;
            }
        }
    }

    if (!multithreaded) {
        DEBUG_PRINT("BTerrainGenerator::create_lod took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - start_time) / 1000000.0));

        if (flags & F_DELETE_ON_COMPLETE) {
            memdelete(this);
        }
    }
}

void BTerrainGenerator::create_chunk(Chunk* chunk) {
    _create_chunk(chunk->mesh_gen, MAX(1, chunk->resolution / chunk->chunks), chunk->x, chunk->y, chunk->uvXform);
}

void BTerrainGenerator::create_chunk_complete(Chunk* chunk) {
    Ref<Mesh> mesh = chunk->mesh_gen.generate_mesh();
    chunk->meshInst->set_mesh(mesh);

    Ref<Material> mat = duplicate_material(mesh_input_data.terrain_material, chunk->uvXform);
    chunk->meshInst->set_material_override(mat); 

    chunk->meshInst->set_cast_shadows_setting(!!(mesh_input_data.flags & F_CAST_SHADOWS) ? GeometryInstance::SHADOW_CASTING_SETTING_ON : GeometryInstance::SHADOW_CASTING_SETTING_OFF);
   
    AABB chunk_aabb = chunk->meshInst->get_aabb();
    // check for AABB validity
    #ifdef DEBUG_ENABLED
        ERR_FAIL_COND(chunk_aabb.position.x > 1e15 || chunk_aabb.position.x < -1e15);
        ERR_FAIL_COND(chunk_aabb.position.y > 1e15 || chunk_aabb.position.y < -1e15);
        ERR_FAIL_COND(chunk_aabb.position.z > 1e15 || chunk_aabb.position.z < -1e15);
        ERR_FAIL_COND(chunk_aabb.size.x > 1e15 || chunk_aabb.size.x < 0.0);
        ERR_FAIL_COND(chunk_aabb.size.y > 1e15 || chunk_aabb.size.y < 0.0);
        ERR_FAIL_COND(chunk_aabb.size.z > 1e15 || chunk_aabb.size.z < 0.0);
        ERR_FAIL_COND(Math::is_nan(chunk_aabb.size.x));
        ERR_FAIL_COND(Math::is_nan(chunk_aabb.size.y));
        ERR_FAIL_COND(Math::is_nan(chunk_aabb.size.z));
    #endif

    memdelete(chunk);
    on_complete_task();
}

void BTerrainGenerator::create_skirt_complete(Skirt* skirt) {
    Ref<Mesh> skirt_mesh = skirt->mesh_gen.generate_mesh();  

    skirt->meshInst->set_mesh(skirt_mesh);
    skirt->meshInst->set_cast_shadows_setting(!!(mesh_input_data.flags & F_CAST_SHADOWS) ? GeometryInstance::SHADOW_CASTING_SETTING_ON : GeometryInstance::SHADOW_CASTING_SETTING_OFF);

    Ref<Material> skirtmat = duplicate_material(mesh_input_data.skirt_material, Transform());
    skirt->meshInst->set_material_override(skirtmat);

    memdelete(skirt);
    on_complete_task();
}

void BTerrainGenerator::on_complete_task() {
    --task_count;
    if (task_count == 0 && multithreaded) {
        DEBUG_PRINT("BTerrainGenerator::create_lod took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - start_time) / 1000000.0));

        if (flags & F_DELETE_ON_COMPLETE) {
            memdelete(this);
        }
    }
}

void BTerrainGenerator::create_collision(PhysicsInputData& pid) {
    uint64_t us = OS::get_singleton()->get_ticks_usec();
    physics_input_data = pid;

    StaticBody *static_body = memnew(StaticBody);
    HeightMapShape *shape = memnew(HeightMapShape);
    
    // width = height - square
    int width = physics_input_data.quads_per_metre * physics_input_data.size;
    float texel_step = 1.0f / float(width);

    int i = 0;
    PoolRealArray heights;
    heights.resize(width * width);
    PoolRealArray::Write w = heights.write();

    for (int y = 0; y < width; ++y) {
        for (int x = 0; x < width; ++x) {
            float height = physics_input_data.height_map->get_height_from_texel(Vector2(float(x) * texel_step, float(y) * texel_step));
            w[i] = height;
            ++i;
        }
    }

    shape->set_map_width(width);
    shape->set_map_depth(width);
    shape->set_map_data(heights);
    PhysicsServer::get_singleton()->body_add_shape(static_body->get_rid(), shape->get_rid());

    float scale = 100.f;
    Transform xform;
    xform.scale(Vector3(scale, scale, scale));
    static_body->set_transform(xform);
    physics_input_data.parent->add_child(static_body);

    PhysicsServer::get_singleton()->body_set_space(static_body->get_rid(), static_body->get_world()->get_space());


    DEBUG_PRINT("BTerrainGenerator::create_collision took (seconds): " + rtos((OS::get_singleton()->get_ticks_usec() - us) / 1000000.0));
}

Ref<Material> BTerrainGenerator::duplicate_material(Ref<Material>& material, const Transform& uvTransform) {
    // setup the material
    // ready to be used on the terrain
    ERR_FAIL_COND_V(!material.is_valid(), Ref<Material>());

    Ref<ShaderMaterial> mat = SAFE_CAST<ShaderMaterial *>(material.ptr());
    if (mat.is_valid()) {
        mat = material->duplicate();
        mat->set_shader_param("uv_transform", uvTransform);
        //mat->set_shader_param("height_range", height);
        //ERR_FAIL_COND_V(!heightmap.is_valid());
        //ERR_FAIL_COND_V(!heightmap.is_square()); // expect a square heightmap
        //mat->set_shader_param("heightmap_pixel_step", Vector3(-1.0 / heightmap.width, 0, 1.0 / heightmap.height));
        return mat; 
    }
    
    Ref<StandardMaterial3D> bmat = SAFE_CAST<StandardMaterial3D *>(material.ptr());
    if (bmat.is_valid()) {
        bmat = material->duplicate();
        Vector3 scale = uvTransform.get_basis().get_scale();
        bmat->set_uv1_scale(Vector3(scale.x, scale.y, 1.f));
        bmat->set_uv1_offset(Vector3(uvTransform.origin.x, uvTransform.origin.y, 1.f));
        return bmat;
    }

    return nullptr;
}

// Given an x and y location, convert that to world space position and add verts for the top of the skirt at terrain height
// and some bottom point specified by skirt_bottom
void BTerrainGenerator::add_skirt_verts_for_point(BMeshGenTool& mesh_gen, int x, int y, int resolution, const Transform& uvXform) {
    float size = 1.0;
    Vector3 origin(-size/2.0, 0.0, -size/2.0);
    float resolution_step = size / resolution;
    float uv_step = 1.0 / resolution;
    
    Vector3 xformedUV = uvXform.xform(Vector3(x * uv_step, y * uv_step, 0.0)); 
    float height = mesh_input_data.height_map->get_height_from_texel(Vector2(xformedUV.x, xformedUV.y));
    //Vector3 c = heightmap.get_pixel_as_vec3(xformedUV.x * heightmap.width_m1, xformedUV.y * heightmap.height_m1);
    //float pixelHeight = c.x * height * height_normalise;

    mesh_gen.set_next_point(Vector3(x * resolution_step, height, y * resolution_step) + origin);
    mesh_gen.set_next_uv(Vector2(x * uv_step, y * uv_step));

    // add the bottom of the skirt
    mesh_gen.set_next_point(Vector3(x * resolution_step, mesh_input_data.skirt_bottom, y * resolution_step) + origin);
    mesh_gen.set_next_uv(Vector2(x * uv_step, y * uv_step));
}

void BTerrainGenerator::create_skirt(Skirt* skirt) {
    BMeshGenTool& mesh_gen = skirt->mesh_gen;
    int resolution = MAX(1, skirt->resolution / skirt->chunks);
    const Transform& uvXform = skirt->uvXform;

    ERR_FAIL_COND(!(mesh_input_data.height_map->get_height_map_tool().is_valid()));

    int edgeCount = skirt->edgeCount;
    int pointsWidth = (resolution + 1);
    int pointCount = ((pointsWidth * edgeCount) - (edgeCount - 1));
    // closed loop, so -1
    if (edgeCount == 4) {
        pointCount -=1;
    }
    pointCount *= 2;
    int quadCount = resolution * edgeCount;
    int indexCount = quadCount * 2 * 3;
   
    mesh_gen.resize_verts(pointCount);
    mesh_gen.resize_indicies(indexCount);
    
    // make all normals face directly upwards
    mesh_gen.populate_all_normals_and_tangents();
    
    /*
    //             < edge 3
    //              -------
    //             |       |
    //  \/ edge 4  |       | edge2 /\
    //             |_______|
    //              edge1 >
    */

    bool bottomEdge = (skirt->edges & E_BOTTOM) != 0;
    bool rightEdge = (skirt->edges & E_RIGHT) != 0;
    bool topEdge = (skirt->edges & E_TOP) != 0;
    bool leftEdge = (skirt->edges & E_LEFT) != 0;

    int bottom_c = 0;
    int right_c = 0;
    int top_c = 0;
    int left_C = 0;

    // special case, because we need left edge before bottom edge
    // else we end up making a triangle!
    bool leftEdgeDone = false;
    if (bottomEdge && leftEdge) {

        // edge 4 (duplicate of below!)
        int x = 0;
        int startIdx = topEdge ? (pointsWidth - 2) : (pointsWidth - 1);
        for (int yi = startIdx; yi >= 0; --yi)
        {
            add_skirt_verts_for_point(mesh_gen, x, yi, resolution, uvXform);
            ++left_C;
        }

        leftEdgeDone = true;
    }

    // edge 1
    if (bottomEdge) {
        int y = 0;
        int startIdx = leftEdge ? 1 : 0;
        for (int xi = startIdx; xi < pointsWidth; ++xi)
        {
            add_skirt_verts_for_point(mesh_gen, xi, y, resolution, uvXform);
            ++bottom_c;
        }
    }
    
    // edge 2
    if (rightEdge) {
        int x = resolution;
        int startIdx = bottomEdge ? 1 : 0;
        for (int yi = startIdx; yi < pointsWidth; ++yi)
        {
            add_skirt_verts_for_point(mesh_gen, x, yi, resolution, uvXform);
            ++right_c;
        }
    }

    // edge 3
    if (topEdge) {
        int y = resolution;
        int startIdx = rightEdge ? (pointsWidth - 2) : (pointsWidth - 1);
        for (int xi = startIdx; xi >= 0; --xi)
        {
            add_skirt_verts_for_point(mesh_gen, xi, y, resolution, uvXform);
            ++top_c;
        }
    }
    
    // edge 4
    if (leftEdge && !leftEdgeDone) {
        int x = 0;
        int startIdx = topEdge ? (pointsWidth - 2) : (pointsWidth - 1);
        for (int yi = startIdx; yi >= 0; --yi)
        {
            add_skirt_verts_for_point(mesh_gen, x, yi, resolution, uvXform);
            ++left_C;
        }
    }
    
    // generate indicies
    int qi = 0; // quad index
    int lastQuad = (skirt->edgeCount == 4) ? (quadCount - 1) : quadCount;
    for (int q = 0; q < lastQuad; ++q)
    {
        mesh_gen.set_next_triangle_indicies(qi, qi + 1, qi + 3);
        mesh_gen.set_next_triangle_indicies(qi, qi + 3, qi + 2);
        qi+=2;
    }
    
    // add the last quad which wraps around
    // if the edge goes all the way around!
    if (skirt->edgeCount == 4) {
        mesh_gen.set_next_triangle_indicies(qi, qi + 1, 1);
        mesh_gen.set_next_triangle_indicies(qi, 1, 0);
    }

    bool ok = mesh_gen.validate();
    ERR_FAIL_COND(!ok);
}

void BTerrainGenerator::_create_chunk(BMeshGenTool& mesh_gen, int resolution, int x, int y, const Transform& uvXform) {

    ERR_FAIL_COND(!mesh_input_data.height_map->get_height_map_tool().is_valid());
    //ERR_FAIL_COND(!mesh_input_data.height_map->get_height_map_tool().is_square());
    
    float size = 1.0;
    Vector3 origin(-size/2.0, 0.0, -size/2.0);
    float resolution_step = size / resolution;
    float uv_step = 1.0 / resolution;
    
    int pointsWidth = (resolution + 1);
    int pointCount = (resolution + 1) * (resolution + 1);
    int triCount = resolution * resolution * 2;
    int indexCount = triCount * 3;
    
    mesh_gen.resize_verts(pointCount);
    mesh_gen.resize_indicies(indexCount);
    
    // make all normals face directly upwards
    mesh_gen.populate_all_normals_and_tangents();
        
    // generate verts
    for (int yi = 0; yi < pointsWidth; ++yi)
    {
        for (int xi = 0; xi < pointsWidth; ++xi)
        {
            mesh_gen.set_next_uv(Vector2(xi * uv_step, yi * uv_step));
            
            Vector3 xformedUV = uvXform.xform(Vector3(xi * uv_step, yi * uv_step, 0.0));
            float height = mesh_input_data.height_map->get_height_from_texel(Vector2(xformedUV.x, xformedUV.y));
            //Vector3 c = heightmap.get_pixel_as_vec3(xformedUV.x * heightmap.width_m1, xformedUV.y * heightmap.height_m1);
            //float pixelHeight = c.x * height * height_normalise;
            mesh_gen.set_next_point(Vector3(xi * resolution_step, height, yi * resolution_step) + origin);
        }
    }
    
    // generate indices
    int q = 0; // what quad are we generating?
    int qi = 0; // quad index
    for (int i = 0; i < indexCount; i += 6)
    {
        // reference: https://github.com/Zylann/godot_terrain_plugin/blob/master/addons/zylann.terrain/terrain.gd
        int p00 = qi;
        int p01 = qi + 1;
        int p11 = qi + pointsWidth + 1;
        int p10 = qi + pointsWidth;
        
        // quad adaption
        bool reverse_quad = ABS(mesh_gen.get_point(p00).y - mesh_gen.get_point(p11).y) > ABS(mesh_gen.get_point(p10).y - mesh_gen.get_point(p01).y);
        
        // 00---01
	//  |  /|
	//  | / |
	//  |/  |
        // 10---11
        if (reverse_quad) {
            mesh_gen.set_next_triangle_indicies(p00, p01, p10);
            mesh_gen.set_next_triangle_indicies(p01, p11, p10);
        }
        // 00---01
	//  |\  |
	//  | \ |
	//  |  \|
        // 10---11
        else {
            mesh_gen.set_next_triangle_indicies(p00, p01, p11);
            mesh_gen.set_next_triangle_indicies(p00, p11, p10);
        }
        
        ++qi;
        ++q;
        
        // skip the extra vert at the end of the row as we jump back to the start
        if (q != 0 && (q % resolution) == 0) {
            ++qi;
        }
    }
}

BTerrainGenerator::BTerrainGenerator() {
    task_count = 0;
    multithreaded = BDatabase::get_singleton()->get_value("bitshift/terrain/multithreaded", true);
}

BTerrainGenerator::~BTerrainGenerator() {

}


