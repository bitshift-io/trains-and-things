 
/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#include "globals.h"
#include "util.h"
#include "terrain.h"
#include "water.h"
#include "boundary_map.h"
#include "core/io/http_client.h"
#include "core/bind/core_bind.h"
#include "core/ustring.h"
#include "scene/2d/canvas_item.h"
#include "scene/main/node.h"
#include "scene/main/timer.h"
#include "scene/main/viewport.h"
#include "scene/resources/surface_tool.h"
#include "scene/3d/collision_polygon.h"
#include "scene/gui/texture_button.h"
#include "../../editor/editor_node.h"
#include "scene/3d/camera.h"
#include "scene/3d/mesh_instance.h"
#include "scene/resources/primitive_meshes.h"
#include "scene/gui/rich_text_label.h"
#include "core/math/random_number_generator.h"

#ifdef TOOLS_ENABLED
    #include "editor/editor_node.h"
    #include "editor/editor_log.h"
#endif

BUtil *BUtil::singleton=NULL;

/**
 * Look for first token and split the string into two parts
 */
bool split_first(const String &p_key, String &p_before, String &p_after, const String &p_token) {
    int split_idx = p_key.find(p_token);
    if (split_idx == -1) {
        return false;
    }

    // break the key up
    p_before = p_key.substr(0, split_idx);
    p_after = p_key.substr(split_idx + 1, p_key.length() - split_idx);
    return true;
}

void BUtil::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_aabb_from_points_2d"),&BUtil::get_aabb_from_points_2d);
    ClassDB::bind_method(D_METHOD("is_child_of"),&BUtil::is_child_of);
    ClassDB::bind_method(D_METHOD("is_class_name"),&BUtil::is_class_name);
    ClassDB::bind_method(D_METHOD("is_same_class"),&BUtil::is_same_class);
    ClassDB::bind_method(D_METHOD("find_parent_by_class_name"),&BUtil::find_parent_by_class_name);
    ClassDB::bind_method(D_METHOD("find_children_by_class_name"),&BUtil::find_children_by_class_name, DEFVAL(true));
    ClassDB::bind_method(D_METHOD("find_first_child_by_class_name"),&BUtil::find_first_child_by_class_name, DEFVAL(true));
    ClassDB::bind_method(D_METHOD("queue_delete_children"),&BUtil::queue_delete_children);
    ClassDB::bind_method(D_METHOD("delete_children"),&BUtil::delete_children);
    ClassDB::bind_method(D_METHOD("set_children_visible"),&BUtil::set_children_visible);
    ClassDB::bind_method(D_METHOD("find_child"),&BUtil::find_child, DEFVAL(true));
    //ClassDB::bind_method(D_METHOD("get_username"),&BUtil::get_username);
    ClassDB::bind_method(D_METHOD("get_terrain"),&BUtil::get_terrain);
    ClassDB::bind_method(D_METHOD("get_water"),&BUtil::get_water);
    ClassDB::bind_method(D_METHOD("get_boundary_map"),&BUtil::get_boundary_map);
    ClassDB::bind_method(D_METHOD("create_tessellated_quad"),&BUtil::create_tessellated_quad);
    ClassDB::bind_method(D_METHOD("get_resource_dir"),&BUtil::get_resource_dir);
    ClassDB::bind_method(D_METHOD("set_click_mask_from_normal_alpha"),&BUtil::set_click_mask_from_normal_alpha);
    ClassDB::bind_method(D_METHOD("remove_from_parent"),&BUtil::remove_from_parent);
    ClassDB::bind_method(D_METHOD("log_editor_message"),&BUtil::log_editor_message);
    ClassDB::bind_method(D_METHOD("get_rich_text_label_height"),&BUtil::get_rich_text_label_height);
    ClassDB::bind_method(D_METHOD("_add_demo_timer"),&BUtil::_add_demo_timer);
    ClassDB::bind_method(D_METHOD("_on_demo_expire"),&BUtil::_on_demo_expire);
    ClassDB::bind_method(D_METHOD("get_scene_tree"),&BUtil::get_scene_tree);
    ClassDB::bind_method(D_METHOD("camelcase_to_underscore"),&BUtil::camelcase_to_underscore);
    ClassDB::bind_method(D_METHOD("merge_dict"),&BUtil::merge_dict);
    ClassDB::bind_method(D_METHOD("join_array"),&BUtil::join_array);
    ClassDB::bind_method(D_METHOD("import_remap"),&BUtil::import_remap);  
    ClassDB::bind_method(D_METHOD("to_absolute_path"),&BUtil::to_absolute_path);

    ClassDB::bind_method(D_METHOD("visit_nodes"), &BUtil::visit_nodes);
    //ClassDB::bind_method(D_METHOD("visit_children"), &BUtil::visit_children);
    
    ClassDB::bind_method(D_METHOD("reparent"), &BUtil::reparent);
    ClassDB::bind_method(D_METHOD("get_branches_as_array"), &BUtil::get_branches_as_array);   
}

String BUtil::to_absolute_path(const String& p_path) {
    String r_path = p_path.replace("\\", "/");

    if (ProjectSettings::get_singleton()) {
        if (r_path.begins_with("res://")) {
            if (resource_path != "") {
                return r_path.replace("res:/", resource_path);
            }

            return r_path.replace("res://", "");
        }
    }

    return r_path;
}

String BUtil::import_remap(const String &p_path) {
    return ResourceLoader::import_remap(p_path);
}

Dictionary BUtil::merge_dict(const Dictionary &p_target, const Dictionary &p_patch) {
    Dictionary result = p_target;
    for (int i = 0; i < p_patch.size(); ++i) {
        Variant key = p_patch.get_key_at_index(i);
        result[key] = p_patch.get_valid(key);
    }
    return result;
}

/**
 * Join/combine/merge array to first
 */
Array BUtil::join_array(const Array &p_target, const Array &p_patch) {
    int new_size = p_target.size() + p_patch.size();
    Array result;
    result.resize(new_size);
    for (int i = 0; i < p_target.size(); ++i) {
        result[i] = p_target[i];
    }
    for (int i = p_target.size(); i < new_size; ++i) {
        result[i] = p_patch[i];
    }
    return result;
}


/**
 * hidden recursive loop for above
 */
void BUtil::_get_branch_as_array(Node *node, Array &result) {
    // recursive check children
    Array children = get_children(node);
    int size = children.size();
    for (int i = 0; i < size; ++i) {
        _get_branch_as_array(children[i], result);
    }

    // finally add parent
    result.append(Variant(node));
}


/**
 * get children recursive, including original selection
 */
Array BUtil::get_branches_as_array(const Array &selection) {
    Array result;
    // loop each parent node
    int size = selection.size();
    for (int i = 0; i < size; ++i) {
        _get_branch_as_array(selection[i], result);
    }
    return result;
}



/**
 * Get children
 */
Array BUtil::get_children(Node *p_node) const {
	Array arr;
	int cc = p_node->get_child_count();
	arr.resize(cc);
	for (int i = 0; i < cc; i++)
		arr[i] = p_node->get_child(i);

	return arr;
}

int BUtil::get_rich_text_label_height(Control* p_rich_text) {
    RichTextLabel* rich_text = Object::cast_to<RichTextLabel>(p_rich_text);
    //message_label.set_custom_minimum_size(Vector2(message_label.rect_min_size.x, 10));
    //message_label.set_size(Vector2(message_label.rect_min_size.x, 10));
    Size2 size = rich_text->get_size();
    rich_text->set_size(Size2(size.width, 0));
    rich_text->scroll_to_line(0); // force vscroll to be updated
    int height = rich_text->get_v_scroll()->get_max();
    return height;
}

void BUtil::log_editor_message(const String& msg) {
#ifdef TOOLS_ENABLED
    if (EditorNode::get_singleton() && EditorNode::get_log())
        EditorNode::get_log()->add_message(msg);
#endif
}

void BUtil::remove_from_parent(Node* node) {
    if (node && node->get_parent())
        node->get_parent()->remove_child(node);
}

bool BUtil::set_click_mask_from_normal_alpha(Control* p_texture_button) {
    TextureButton* texture_button = Object::cast_to<TextureButton>(p_texture_button);
    Ref<Texture2D> texture = texture_button->get_normal_texture();
    String res_path = texture->get_path();

    Ref<Image> image = texture->get_data();
    Ref<BitMap> mask;
    mask.instance();
    mask->create_from_image_alpha(image);
    texture_button->set_click_mask(mask);
    return true;
}

AABB BUtil::get_aabb_from_points_2d(const Array& p_point_list) {
    AABB aabb;
    if (p_point_list.size() <= 0)
        return aabb;

    const Vector2& pt = p_point_list[0];
    aabb.position = Vector3(pt.x, pt.y, 0.f);
    for (int i = 1; i < p_point_list.size(); ++i) {
        const Vector2& pti = p_point_list[i];
        aabb.expand_to(Vector3(pti.x, pti.y, 0.f));
    }
    return aabb;
}

AABB BUtil::get_aabb_from_vec_points_2d(const Vector<Point2>& p_point_list) {
    AABB aabb;
    if (p_point_list.size() <= 0)
        return aabb;

    const Vector2& pt = p_point_list[0];
    aabb.position = Vector3(pt.x, pt.y, 0.f);
    for (int i = 1; i < p_point_list.size(); ++i) {
        const Vector2& pti = p_point_list[i];
        aabb.expand_to(Vector3(pti.x, pti.y, 0.f));
    }
    return aabb;
}

/**
 * convert aabb to mesh
 * mainly for debuging
 */
MeshInstance* BUtil::aabb_to_mesh(const AABB& p_aabb) {
	MeshInstance* node = memnew(MeshInstance);
	CubeMesh* mesh = memnew(CubeMesh);
    Vector3 aabb_size = p_aabb.get_size();
	mesh->set_size(aabb_size);
	node->set_mesh(mesh);
	Vector3 aabb_min = p_aabb.get_position();
	Vector3 aabb_max = aabb_min + aabb_size;
	Vector3 origin = aabb_min + (aabb_size / 2.0);
	node->translate(origin);
	return node;
}

String BUtil::get_resource_dir() const {
	return OS::get_singleton()->get_resource_dir();
}

/*
String BUtil::get_username()
{
    return itch.get_username();
}*/

bool BUtil::is_child_of(Node* possible_child, Node* possible_parent) {
    Node* node = possible_child;
    while (node) {
        if (node == possible_parent)
            return true;

        node = node->get_parent();
    }

    return false;
}

bool BUtil::_is_class_name(Node* node, const String& class_name, const String& scriptClassName) {
    if (!node)
        return false;

    Ref<Script> script = node->get_script();
    while (script.is_valid()) {
        String scriptName = script->get_path().get_file();
        if (scriptName == scriptClassName)
            return true;

        script = script->get_base_script();
    }

    if (ClassDB::is_parent_class(node->get_class(), class_name))
        return true;

    return false;
}

bool BUtil::is_class_name(Node* node, String class_name) {
    return _is_class_name(node, class_name, class_name + ".gd");
}

bool BUtil::is_same_class(Node *p_node_a, Node *p_node_b) {
    if (!p_node_a || !p_node_b) {
        return false;
    }

    Ref<Script> script_a = p_node_a->get_script();
    Ref<Script> script_b = p_node_b->get_script();
    if (script_a.is_valid() && script_b.is_valid()) {
        return script_a->get_path().get_file() == script_b->get_path().get_file();
    }

    if (script_a.is_valid() || script_b.is_valid()) {
        return false;
    }

    // no scripts, so check native classes
    return p_node_a->get_class() == p_node_b->get_class();
}

Node* BUtil::find_parent_by_class_name(Node* node, String class_name) {
    if (!node)
        return NULL;

    Node* n = node;
    String scriptClassName = class_name + ".gd";
    while (n && !(n->get_name() == (const char*)"root")) {
        if (_is_class_name(n, class_name, scriptClassName)) {
            return n;
        }

        n = n->get_parent();
    }

    return NULL;
}

Array BUtil::find_children_by_class_name(Node* node, String class_name, bool recursive) {
    if (!node)
        return Array();

    Node* n = node;
    Array results;
    for (int i = 0; i < n->get_child_count(); ++i)
    {
        _find_children_by_class_name(n->get_child(i), class_name, recursive, class_name + ".gd", results);
    }
    return results;
}

Node* BUtil::find_first_child_by_class_name(Node* node, String class_name, bool recursive) {
	Array children = find_children_by_class_name(node, class_name, recursive);
	if (children.size()) 
		return children[0];
	
	return NULL;
}

void BUtil::_find_children_by_class_name(Node* node, const String& class_name, bool recursive, const String& scriptClassName, Array& results) {
    if (!node)
        return;

    if (_is_class_name(node, class_name, scriptClassName)) {
        results.append(Variant(node));
    }

    if (!recursive)
        return;
    
    for (int i = 0; i < node->get_child_count(); ++i)
    {
        _find_children_by_class_name(node->get_child(i), class_name, recursive, scriptClassName, results);
    }
}

BTerrain *BUtil::get_terrain() {
    return BTerrain::get_singleton();
}

BWater *BUtil::get_water() {
    return BWater::get_singleton();
}

BBoundaryMap *BUtil::get_boundary_map() {
    return BBoundaryMap::get_singleton();
}

void BUtil::queue_delete_children(Node* node) {
    if (!node) {
        return;
    }

    for (int i = node->get_child_count() - 1; i >= 0; --i) {
        Node *child = node->get_child(i);
        child->queue_delete();
    }
}

void BUtil::delete_children(Node* node) {
    if (!node) {
        return;
    }

    // kill children as cleanly as possible
    while (node->get_child_count()) {
        Node *child = node->get_child(0);
        node->remove_child(child);
        memdelete(child);
    }
}

void BUtil::set_children_visible(Node* node, bool visible) {
    for (int i = 0; i < node->get_child_count(); ++i) {
        Node* n = node->get_child(i);
        CanvasItem *child = Object::cast_to<CanvasItem>(n); //n->cast_to<CanvasItem>();
        ERR_FAIL_COND(!child);
        if (child)
            child->set_visible(visible);
    }
}

void BUtil::_find_children(Node *p_node, const String &p_mask, bool p_recursive, Node* p_parent, int p_depth, Array& results) const {
    Node *n = p_node;
    String node_path_to_parent = n->get_name();
    n = p_node->get_parent();
    do {
        node_path_to_parent = n->get_name().operator String() + "/" + node_path_to_parent;
        n = p_node->get_parent();
    } while (n && n != p_parent);

    if (node_path_to_parent.match(p_mask))
        results.append(p_node);

    if (p_recursive == false)
        return;

    for (int i = 0; i < p_node->get_child_count(); ++i) {
        Node* cn = p_node->get_child(i);
        _find_children(cn, p_mask, p_recursive, p_node, p_depth + 1, results);
    }
}

Node *BUtil::_find_child(Node *p_node, const String &p_mask, bool p_recursive, Node* p_parent, int p_depth) const {
    Node *n = p_node;
    String node_path_to_parent = n->get_name();
    n = p_node->get_parent();
    do {
        node_path_to_parent = n->get_name().operator String() + "/" + node_path_to_parent;
        n = p_node->get_parent();
    } while (n && n != p_parent);

    if (node_path_to_parent.match(p_mask))
        return p_node;

    if (p_recursive == false)
        return NULL;

    for (int i = 0; i < p_node->get_child_count(); ++i) {
        Node* cn = p_node->get_child(i);
        Node *r = _find_child(cn, p_mask, p_recursive, p_node, p_depth + 1);
        if (r)
            return r;
    }

    return NULL;
}

// The problem with Node::find_node is:
// 1. sometimes it works, sometimes it doesn't
// 2. it cant take a string like: NodeName/*/AnotherNodeName where * can be some arbitary number of nodes in between
Node *BUtil::find_child(Node *p_node, const String &p_mask, bool p_recursive) const {
    if (!p_node)
        return NULL;

    String modified_mask;

    // user is doing some manualling it if they have a /
    if (p_mask.find("/") == -1)
        modified_mask = "*/" + p_mask;
    else
        modified_mask = p_mask;

    for (int i = 0; i < p_node->get_child_count(); ++i) {
        Node* n = p_node->get_child(i);
        Node *r = _find_child(n, modified_mask, p_recursive, p_node, 0);
        if (r)
            return r;
    }

    return NULL;
}

Array BUtil::find_children(Node *p_node, const String &p_mask, bool p_recursive) const {
    Array results;
    if (!p_node)
        return results;

    String modified_mask;

    // user is doing some manualling it if they have a /
    if (p_mask.find("/") == -1)
        modified_mask = "*/" + p_mask;
    else
        modified_mask = p_mask;


    for (int i = 0; i < p_node->get_child_count(); ++i) {
        Node* n = p_node->get_child(i);
        _find_children(n, modified_mask, p_recursive, p_node, 0, results);
    }

    return results;
}

Ref<ArrayMesh> BUtil::create_tessellated_quad(int resolution) {       
    float size = 1.0;
    Vector3 origin(-size/2.0, 0.0, -size/2.0);
    float resolution_step = size / resolution;
    float uv_step = 1.0 / resolution;
    
    int pointsWidth = (resolution + 1);
    int pointCount = (resolution + 1) * (resolution + 1);
    int triCount = resolution * resolution * 2;
    int indexCount = triCount * 3;
    
   
    PoolVector<Vector3> points;
    points.resize(pointCount);
    PoolVector<Vector3>::Write pointsw = points.write();
        
    PoolVector<Vector3> normals;
    normals.resize(pointCount);
    PoolVector<Vector3>::Write normalsw = normals.write();
        
    PoolVector<Vector2> uvs;
    uvs.resize(pointCount);
    PoolVector<Vector2>::Write uvsw = uvs.write();

    PoolVector<int> indices;
    indices.resize(indexCount);
    PoolVector<int>::Write indicesw = indices.write();
    
    // generate verts
    for (int y = 0; y < pointsWidth; ++y)
    {
        for (int x = 0; x < pointsWidth; ++x)
        {
            int pi = (y * pointsWidth) + x;
            
            pointsw[pi] = Vector3(x * resolution_step, 0.0, y * resolution_step) + origin;
            normalsw[pi] = Vector3(0, 1, 0);
            uvsw[pi] = Vector2(x * uv_step, y * uv_step);
        }
    }
    
    // generate indicies
    int q = 0; // what quad are we generating?
    int qi = 0; // quad index
    for (int i = 0; i < indexCount; i += 6)
    {
        indicesw[i + 0] = qi;
        indicesw[i + 1] = qi + 1;
        indicesw[i + 2] = qi + pointsWidth + 1;
        
        indicesw[i + 3] = qi;
        indicesw[i + 4] = qi + pointsWidth + 1;
        indicesw[i + 5] = qi + pointsWidth;
        
        ++qi;
        ++q;
        
        // skip the extra vert at the end of the row as we jump back to the start
        if (q != 0 && (q % resolution) == 0) {
            ++qi;
        }
    }
        
    Array arr;
    arr.resize(VS::ARRAY_MAX);
    arr[VS::ARRAY_VERTEX]=points;
    arr[VS::ARRAY_NORMAL]=normals;
    arr[VS::ARRAY_TEX_UV]=uvs;
    arr[VS::ARRAY_INDEX]=indices;
            
    Ref<ArrayMesh> mesh = memnew( ArrayMesh() );
    mesh->add_surface_from_arrays(Mesh::PRIMITIVE_TRIANGLES, arr);
    return mesh;
}

void BUtil::_add_demo_timer() {
    // add demo timer
    demo_timer = memnew(Timer);
    demo_timer->set_wait_time(DEMO_TIME);
    demo_timer->start();
    demo_timer->connect("timeout", this, "_on_demo_expire");

    get_scene_tree()->get_root()->add_child(demo_timer);
}

void BUtil::_on_demo_expire() {
    get_scene_tree()->quit();
}

SceneTree* BUtil::get_scene_tree() {
    MainLoop *ml = OS::get_singleton()->get_main_loop();
    SceneTree *sml = Object::cast_to<SceneTree>(ml);
    return sml;
}

String BUtil::camelcase_to_underscore(const String& str) {
    // camel case to snake case
    // Joe Smith -> joe_smith
    // joe smith -> joe_smith
    // Joe-Smith -> joe_smith
    // joe-smith DONT DO THIS! -> joesmith
    return str.camelcase_to_underscore().replace(" ", "").replace("-", "");
}

String BUtil::url_encode(const String& str) {
    String r = str;
    r = r.replace("%", "%25");
    r = r.replace(" ", "%20");
    r = r.replace("&", "%26");
    r = r.replace(";", "%3B");
    r = r.replace("\"", "%22");
    r = r.replace("'", "%27");
    return r;
}

Camera* BUtil::get_editor_camera(Node* node) {
    if (!Engine::get_singleton()->is_editor_hint()) {
        return NULL;
    }

    // what if its freed?!
    if (editor_camera && editor_camera->is_current()) {
        return editor_camera;
    }

    Camera* c = NULL;
    Node* editor_node = node->get_node(NodePath("/root/EditorNode"));
    if (editor_node) {
        Array viewports = BUtil::get_singleton()->find_children_by_class_name(editor_node, "Viewport");
        for (int v = 0; v < viewports.size(); ++v) {
            Viewport* editor_vp = Object::cast_to<Viewport>(viewports[v]);
            if (editor_vp) {
                //DEBUG_PRINT(editor_vp->get_path());
                c = editor_vp->get_camera();

                // we want a current camera!
                if (c && !c->is_current()) {
                    c = NULL;
                }
            }

            // we found a camera!
            if (c) {
                break;
            }
        }
    }

    editor_camera = c;
    return editor_camera;
}

/**
 * Climb the viewport stack looking for a current camera
 * we need this as we have Viewports in Viewports in our games,
 * godot get_viewport()->get_camera() simply returns the closest viewport camera,
 * while often we just want the camera that is current
 * /
Camera *BUtil::get_top_viewport_with_camera(Node *p_node) {
    if (!p_node) {
        return nullptr;
    }

    Viewport *v = p_node->get_viewport();
    String vt = v->get_path();
    Camera *c = nullptr;

    while (v) {
        c = v->get_camera();

        Viewport* pv = v && v->get_parent() ? v->get_parent()->get_viewport() : nullptr;
        String pvt = pv->get_path();
        v = pv && pv->get_camera() ? pv : nullptr;
    }

    return c;
}
*/

BUtil::BUtil() {
    singleton = this;
    demo_timer = NULL;
    //itch.auth();

    editor_camera = NULL;

#ifdef DEMO_BUILD
    call_deferred("_add_demo_timer");
#else
#endif

    // emulate project_settings::_setup
    // which when shipping a packaged game, doesn't run anymore...
    // hence me cloning it here
    DirAccess *d = DirAccess::create(DirAccess::ACCESS_FILESYSTEM);
	ERR_FAIL_COND(!d);
	resource_path = d->get_current_dir();
	resource_path = resource_path.replace("\\", "/"); // windows path to unix path just in case
}


BUtil::~BUtil() {
    singleton = NULL;
}


void BUtil::visit_nodes(Node* p_node, Object* p_object, String p_func_name) {
    FuncRef fr;
    fr.set_instance(p_object);
    fr.set_function(p_func_name);
    _visit_nodes(p_node, fr);
}

void BUtil::_visit_children(Node* p_node, FuncRef& p_func) {
    for (int i = 0; i < p_node->get_child_count(); ++i) {
       Node* child = p_node->get_child(i);
       _visit_nodes(child, p_func);
   }
}

void BUtil::_visit_nodes(Node* p_node, FuncRef& p_func) {
    if (!p_node) {
        return;
    }

    _visit_children(p_node, p_func);

    Variant::CallError err;
    Variant vnode(p_node);
    const Variant *args[1] = { &vnode };
    p_func.call_func(args, 1, err);
}

void BUtil::reparent(Node* p_node, Node* p_new_parent) {
    if (!p_node) {
        return;
    }

    if (p_node->get_parent()) {
        p_node->get_parent()->remove_child(p_node);
    }

    if (p_new_parent) {
	    p_new_parent->add_child(p_node);
    }
}

String generate_hex(const unsigned int length) {
    String chars = "0123456789abcdef";
    String str;
    str.resize(length);
    RandomNumberGenerator rng;
    rng.randomize();

    for (unsigned int i = 0; i < length; i++) {
        int rc = rng.randi() % chars.size();
        str[i] = chars[rc];
    }

    return str;
}

String generate_uuid() {
    // https://en.wikipedia.org/wiki/Universally_unique_identifier
    String id = generate_hex(8) + "-" + generate_hex(4) + "-" + generate_hex(4) + "-" + generate_hex(4) + "-" + generate_hex(12);
    return id;
}