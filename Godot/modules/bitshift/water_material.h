/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BWATER_MATERIAL_H
#define BWATER_MATERIAL_H

#include "scene/resources/material.h"
#include "globals.h"

class BWaterMaterial : public Material {

	GDCLASS(BWaterMaterial, Material);

public:

	enum ExtraTextureParam {
		EXTRA_TEXTURE_WAVE_1_NORMAL,
		EXTRA_TEXTURE_WAVE_2_NORMAL,
		EXTRA_TEXTURE_UV,
		EXTRA_TEXTURE_FOAM,
		EXTRA_TEXTURE_CAUSTIC,
		EXTRA_TEXTURE_REFLECTION,

		EXTRA_TEXTURE_MAX
	};

	enum ExtraFeature {
        EXTRA_FEATURE_DETAIL_NORMAL,
        EXTRA_FEATURE_MAX
    };

private:

	union MaterialKey {

		struct {
			uint32_t texture_mask : 16;
			uint32_t flags : 4;
			uint32_t invalid_key : 1;
		};

		uint32_t key;

		bool operator<(const MaterialKey &p_key) const {
			return key < p_key.key;
		}
	};

	struct ShaderData {
		RID shader;
		int users;
	};

	static Map<MaterialKey, ShaderData> shader_map;

	MaterialKey current_key;

	_FORCE_INLINE_ MaterialKey _compute_key() const {

		MaterialKey mk;
		mk.key = 0;
		for (int i = 0; i < EXTRA_TEXTURE_MAX; i++) {
			if (extra_textures[i].is_valid()) {
				mk.texture_mask |= (1 << i);
			}
		}
		for (int i = 0; i < EXTRA_FEATURE_MAX; i++) {
			if (extra_features[i]) {
				mk.flags |= (1 << i);
			}
		}

		return mk;
	}
	
	struct ShaderNames {
		StringName wave_speed;
		StringName wave_a;
		StringName wave_b;
		StringName wave_c;
		StringName normal_sampler_scale;
		StringName normal_sampler_direction;
		StringName uv_sampler_scale;
		StringName uv_sampler_strength;
		StringName foam_level;
		StringName foam_sampler_scale;
		StringName color_refraction;
		StringName color_deep;
		StringName color_shallow;
		StringName normal_depth;
		StringName fresnel_scale;
		StringName fresnel_power;
		StringName reflection_brightness;
		StringName texture_caustic;
		StringName caustic_sampler_scale;

		StringName extra_texture_names[EXTRA_TEXTURE_MAX];
	};

	static Mutex *material_mutex;
	static SelfList<BWaterMaterial>::List *dirty_materials;
	static ShaderNames *shader_names;

	SelfList<BWaterMaterial> element;

	void _update_shader();
	_FORCE_INLINE_ void _queue_shader_change();
	_FORCE_INLINE_ bool _is_shader_dirty() const;

	// new stuff
	float wave_speed;
	Vector4 wave_a;
	Vector4 wave_b;
	Vector4 wave_c;
	Vector2 normal_sampler_scale;
	Vector2 normal_sampler_direction;
	Vector2 uv_sampler_scale;
	float uv_sampler_strength;
	float foam_level;
	Vector2 foam_sampler_scale;
	float color_refraction;
	Color color_deep;
	Color color_shallow;
	float normal_depth;
	float fresnel_scale;
	float fresnel_power;
	float reflection_brightness;

	Vector2 caustic_sampler_scale;
	Ref<Texture2DArray> texture_caustic;

	bool extra_features[EXTRA_FEATURE_MAX];
	Ref<Texture> extra_textures[EXTRA_TEXTURE_MAX];

protected:
	static void _bind_methods();
	virtual bool _can_do_next_pass() const { return true; }

public:

	void set_wave_speed(const float &p_val);
	float get_wave_speed() const;

	void set_wave_a(const Vector4 &p_val);
	Vector4 get_wave_a() const;

	void set_wave_b(const Vector4 &p_val);
	Vector4 get_wave_b() const;

	void set_wave_c(const Vector4 &p_val);
	Vector4 get_wave_c() const;

	void set_normal_sampler_scale(const Vector2 &p_val);
	Vector2 get_normal_sampler_scale() const;

	void set_normal_sampler_direction(const Vector2 &p_val);
	Vector2 get_normal_sampler_direction() const;

	void set_uv_sampler_scale(const Vector2 &p_val);
	Vector2 get_uv_sampler_scale() const;

	void set_uv_sampler_strength(const float &p_val);
	float get_uv_sampler_strength() const;

	void set_foam_level(const float &p_val);
	float get_foam_level() const;

	void set_foam_sampler_scale(const Vector2 &p_val);
	Vector2 get_foam_sampler_scale() const;

	void set_color_refraction(const float &p_val);
	float get_color_refraction() const;

	void set_color_deep(const Color &p_val);
	Color get_color_deep() const;

	void set_color_shallow(const Color &p_val);
	Color get_color_shallow() const;

	void set_normal_depth(const float &p_val);
	float get_normal_depth() const;

	void set_fresnel_scale(const float &p_val);
	float get_fresnel_scale() const;

	void set_fresnel_power(const float &p_val);
	float get_fresnel_power() const;

	void set_reflection_brightness(const float &p_val);
	float get_reflection_brightness() const;

	void set_caustic_sampler_scale(const Vector2 &p_val);
	Vector2 get_caustic_sampler_scale() const;

	void set_texture_caustic(const Ref<Texture> &p_val);
	Ref<Texture> get_texture_caustic() const;

	void set_extra_texture(ExtraTextureParam p_param, const Ref<Texture> &p_texture);
	Ref<Texture> get_extra_texture(ExtraTextureParam p_param) const;
	//
	
	static void init_shaders();
	static void finish_shaders();
	static void flush_changes();

	RID get_shader_rid() const;

	virtual Shader::Mode get_shader_mode() const;

	BWaterMaterial();
	virtual ~BWaterMaterial();
};

VARIANT_ENUM_CAST(BWaterMaterial::ExtraTextureParam)
VARIANT_ENUM_CAST(BWaterMaterial::ExtraFeature)

#endif // BWATER_MATERIAL_H
