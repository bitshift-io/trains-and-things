/*************************************************************************/
/*                    This file is part of:                              */
/*                    BITSHIFT GODOT PLUGIN                              */
/*                    http://bit-shift.io                                */
/*************************************************************************/
/* Copyright (c) 2017   Fabian Mathews.                                  */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/
#ifndef BMODIO_H
#define BMODIO_H

#include "core/reference.h"
#include "core/list.h"

class HTTPRequest;

class BModio : public Reference {

    GDCLASS(BModio, Reference)

public:

protected:

    static BModio *singleton;

    static void _bind_methods();

    String cache_dir;
    Node* process_node;
    String api_key;
    int game_id;
    int page_size;

    void process();

    Dictionary mods;
    Map<int, Dictionary> mod_downloads;
    List<HTTPRequest *> http_requests;

    void _http_mods_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request);

    void _http_download_1_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request);
    void _http_download_2_completed(int p_status, int p_code, const PoolStringArray &p_headers, const PoolByteArray &p_data, Object *http_request, Dictionary p_mod);

    // these 2 methods could be moved to a separate HTTPRequestMgr class
    HTTPRequest *make_http_request(const String &p_url, Object * p_object, const StringName &p_to_method, const Vector<Variant> &p_binds = Vector<Variant>());
    void delete_complete_http_requests();

    bool _install_from_file(const String &p_file, const String &p_install_dir, bool p_use_progress);

public:

    BModio();
    ~BModio();

    void set_process(bool p_processing);

    void request_mods(Dictionary p_params);

    void download_mod(int p_id);

    // get metadata_kvp as a dictionary
    Dictionary get_metadata_kvp(const Dictionary &p_mod);

    static BModio *get_singleton() { return singleton; }
};

#endif // BMODIO_H
