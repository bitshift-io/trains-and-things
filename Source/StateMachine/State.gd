#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

# Handle any transitions into this state. Subclasses should first chain to this method.
func state_enter(previousState):
	pass

# Handle input events.
func state_input(event):
	return;

# Input events that arent handled - events that get past GUI for example
func state_unhandled_input(event):
	return;

# Update physics processing.
func state_physics_process(delta):
	return;

# Handle exit events.
func state_exit(nextState):
	return;

func get_state_machine():
	return get_parent();
	
func is_current_state():
	return (get_parent().get_current_state() == self);
	
func push_state():
	get_state_machine().push_state(self);
	
func pop_state():
	get_state_machine().pop_state(self);
	
# push/pop using a bool
func toggle_state(push):
	if (push):
		get_state_machine().push_state(self);
	else:
		get_state_machine().pop_state(self);
		
