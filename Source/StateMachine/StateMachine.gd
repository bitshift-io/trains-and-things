#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

export(String) var startState

var currentStateStack = []

signal state_changed(from_state, to_state);

func _ready():
	if (startState):
		push_state_by_name(startState)
	else:
		push_state_by_name(get_children()[0])

func get_state_by_name(name):
	var states = get_children()
	for state in states:
		if (state.get_name() == name):
			return state
			
	return null
		
func get_current_state():
	if (currentStateStack.size() <= 0):
		return null
		
	return currentStateStack[currentStateStack.size() - 1]
							
func push_state_by_name(name):
	var nextState = get_state_by_name(name)
	push_state(nextState);
	
func push_state(nextState):
	Util.assert(nextState != null, "Can't push null state")
	var currentState = get_current_state()
	if (currentState):
		currentState.state_exit(nextState)
		
	currentStateStack.push_back(nextState)
	
	nextState.state_enter(currentState)
	emit_signal("state_changed", currentState, nextState);
	return;
	
# name supplied is just for assertion purposes
func pop_state_by_name(name = null):
	var currentState = get_current_state()
	if (name):
		Util.assert(currentState.get_name() == name, "Error popping state")
		
	pop_state(currentState);
	return;
	
func pop_state(currentState):
	Util.assert(currentState, "Can't pop null state");
	Util.assert(currentState == get_current_state(), "Can't pop current state");
		
	currentStateStack.pop_back()
	var nextState = currentStateStack[currentStateStack.size() - 1]
	currentState.state_exit(nextState)
	
	if (nextState):
		nextState.state_enter(currentState);
		
	emit_signal("state_changed", currentState, nextState);
	return;
	
	
