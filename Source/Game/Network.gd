#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var clientIdMap = {} # map of id and a unique number


# pass in a standard name componenet and it wil generate a name thats unique across the network
func next_unique_name(clientId, name):
	Util.assert(get_tree().is_network_server(), "next_unique_name expected ton only run on the server"); # ONLY to be run on the server
	if (!clientIdMap.has(clientId)):
		clientIdMap[clientId] = -1
		
	var number = clientIdMap[clientId] + 1
	clientIdMap[clientId] = number
	return "%s_%d_%d" % [name, clientId, number]

#
# godot will try to generate a unique name for a node
# which stuffs over multiplayer
# here I detect it is a generated name, I strip out the funky characters
# to make it work properly in multiplayer
#func generate_unique_name(node):
#	var name = node.get_name();
#	if (name.begins_with("@")):
#		var last_at = name.rfind("@");
#		name = name.substr(1, last_at - 1);
#		name = next_unique_name(world.master_player.networkId, name);
#		node.set_name(name);
#		#assert(node.get_name() == name);
#		
#	return;
	


func _init():
	pass
	
	
func _ready():
	return;
	
