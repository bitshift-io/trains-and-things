#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

func _ready():
	# make self visible, but disable effects
	self.set_enabled(false);
	self.set_visible(true);
	pass
	
func set_enabled(enable):
	for child in get_children():
		child.set_emitting(enable);
	return;
