#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node 

onready var input_actions = Util.load_script_into_node("res://Game/InputActions.gd");

var script_path;

func _init():
	return;
			
func _ready():
	# https://github.com/godotengine/godot/issues/8721
	var cmdline = OS.get_cmdline_args();
	if (cmdline.size() > 0):
		# roll our own -s support as using godots -s
		# doesnt work with autoloads
		# specify command line like so:
		# -s=UnitTest/MapsLoad.gd
		var splits = cmdline[0].split("=");
		
		if ((splits[0] == '-s' || splits[0] == '--script') && splits.size() == 2):
			script_path = splits[1];
			var obj = Util.load_script_into_node(script_path);
			# to allow other systems to boot up
			call_deferred("add_child", obj);
			
	add_child(input_actions);
	input_actions.add_action("screenshot", input_actions.HandledMode.HM_HANDLED).connect("is_pressed", self, "screenshot_pressed");

	
func screenshot_pressed(p_event):
	screenshot();
	
	
func screenshot(p_override_name = null):
	# from EditorNode::_save_screenshot
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured
	#yield(get_tree(), "idle_frame")
	#yield(get_tree(), "idle_frame")

	# Retrieve the captured image
	var img = get_viewport().get_texture().get_data();
	
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ALWAYS);
	
	# TODO: put this in thread so it doesn't lag the game
	var path = p_override_name;
	if (!p_override_name):
		var file = File.new();
		var idx = 0;
		path = "res://User/Screenshot.png";
		var exists = file.file_exists(path);
		while (exists):
			idx += 1;
			path = "res://User/Screenshot_" + str(idx) + ".png";
			exists = file.file_exists(path);
		
	BLog.debug("Screenshot saved to: " + path);
	img.save_png(path);
	
	
