#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

signal finances_changed(finances);

# class handles all financial tracking and reporting

export(float) var purchase_cost = 1000;
export(float) var maintenance_cost = 100; # cost per year

var refund_percent = 0.8; # how much to refund as % of origional purchase price

var life_time_revenues = 0;
var life_time_expenses = 0;

var time_of_last_maintenance;

func _ready():
	set_process(true);
	add_to_group("persist");
	
func _process(delta):
	# TODO: use DateTime to determine when a year ticks over
	pass

func get_asset_value():
	return get_purchase_cost();
	
func get_refund_amount():
	return get_purchase_cost() * refund_percent;

func get_purchase_cost():
	return purchase_cost;
	
func get_maintenance_cost():
	return maintenance_cost;
	
func get_year_to_date_profit():
	return life_time_revenues - life_time_expenses; # TODO: YTD VERSION
	
func get_year_to_date_revenues():
	return life_time_revenues; # TODO: YTD VERSION
	
func get_year_to_date_expenses():
	return life_time_expenses; # TODO: YTD VERSION
	
func get_life_time_profit():
	return life_time_revenues - life_time_expenses;
	
func get_life_time_revenues():
	return life_time_revenues;
	
func get_life_time_expenses():
	return life_time_expenses;
	
func add_expense(expense, network_sync):
	life_time_expenses += expense;
	emit_signal("finances_changed", self);
	if (network_sync):
		rpc_unreliable("_set_life_time_expenses_rpc", life_time_expenses);
	
remote func _set_life_time_expenses_rpc(value):
	life_time_expenses = value;
	emit_signal("finances_changed", self);
	
func add_revenue(revenue, network_sync):
	life_time_revenues += revenue;
	emit_signal("finances_changed", self);
	if (network_sync):
		rpc_unreliable("_set_life_time_revenues_rpc", life_time_revenues);
	
remote func _set_life_time_revenues_rpc(value):
	life_time_revenues = value;
	emit_signal("finances_changed", self);
	
# compile a tally from the array of given finances
func compile(finances_list):
	purchase_cost = 0;
	maintenance_cost = 0;
	life_time_revenues = 0;
	life_time_expenses = 0;
	
	for f in finances_list:
		purchase_cost += f.purchase_cost;
		maintenance_cost += f.maintenance_cost;
		add_expense(f.get_life_time_expenses(), false);
		add_revenue(f.get_life_time_revenues(), false);
		
func save_to(savegame):
	savegame.append_builtins({
		life_time_revenues = life_time_revenues,
		life_time_expenses = life_time_expenses,
	});
	return;
	
func load_from(savegame):
	#savegame.add_child_to_parent(); # setup parenting
	savegame.load_builtins();
	return;
	
