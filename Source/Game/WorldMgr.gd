#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Handles loading, unloading and tracking of the World
# First we load the "World" which includes game specific
# containers, as well as viewports that the map needs to be parented under
#
# Basically, the World is an instance of the game map and all that is connected
# to that instance of the... world!
# so when we finish we can just dispose of the world and everything is cleaned 
# up nicely!
#

extends Node
const ResLoader = preload("res://Game/ResLoader.gd")

signal world_instanced();
signal world_loaded();
signal error();

export(PackedScene) var world_scene; # should be set to: "res://Game//World.tscn";

var worlds = []; # this could be a list if our game supports multiple world

var is_loading = false;

func _init():
	return;
	
func _ready():
	var cmdline = OS.get_cmdline_args();
	
	var map = null;
	var loaded_scene = BDatabase.get_value("application/run/main_scene");
	var using_default_scene = true;
	if (cmdline.size()):
		var game_path = cmdline[0];
		
		# perhaps this hsould be handled by Main.gd?
		if (!game_path.begins_with("res://")):
			return;
			
		using_default_scene = (game_path == loaded_scene);
		loaded_scene = game_path;
		
	# lets find the loaded scene and rename it to "World"
	var children = get_node("/root").get_children();
	for child in children:
		var fname = child.get_filename();
		if (fname == loaded_scene):
			# the default scene is an 'empty' scene we have to specify as godot
			# forces us too, so free it here
			if (using_default_scene):
				child.queue_free();
				return;
				
			child.name = "World";
			map = child;
		
	is_loading = true;
	BLog.debug("Loaded: " + loaded_scene);	
	#_load_world(map);
	# deferr this so map._ready gets called first
	call_deferred("_load_world", map);
	return;
	

func load_world(p_map_name):
	is_loading = true;
	
	var res_loader = ResLoader.new();
	res_loader.path = p_map_name;
	res_loader.connect("error", self, "_res_loader_error");
	res_loader.connect("loaded", self, "_res_loader_loaded");
	add_child(res_loader);


func _res_loader_error(p_loader, p_error):
	print("world picked up resloader error:" + str(p_error));
	emit_signal("error", "Failed to load resource " + p_loader.path)
	return;
	
func _res_loader_loaded(p_loader):
	var map = p_loader.instances[0];
	_load_world(map);
	
func _load_world(map):
	if (worlds):
		unload_worlds();
		
	# TODO: thread the loading of this using ResLoader also?
	if (!world_scene):
		emit_signal("error", "Failed to load world " + world_scene)
		return;
		
	var world = world_scene.instance();
	
	# add to worlds list
	worlds.append(world);
	
	# call preready to allow the world to some stuff
	world._pre_ready();
	world.add_map(map);
	
	# give others an opportunity to modify world before
	# we go calling _ready on everything
	emit_signal("world_instanced", world);
	
	# this will call _ready on everything!
	get_tree().get_root().add_child(world);
	
	# now wait for tasks on the thread pool to complete
	# this will be terrain generation, normal map generation
	# what ever else
	_wait_for_thread_pool_to_complete(world);
	return;
	
	
func _wait_for_thread_pool_to_complete(world):
	#if (BThreadPool.has_tasks()):
	#	call_deferred("_wait_for_thread_pool_to_complete", world);
	#	return;
	
	# above code doesn't work, so for now just block load
	BThreadPool.wait_all();

	BUtil.visit_nodes(world, self, "_call_post_ready");
	
	BLog.debug("Loaded: " + world.map.get_filename());
	is_loading = false;
	emit_signal("world_loaded", world);
	return world;
	
func _call_post_ready(p_node):
	if (p_node.has_method("_post_ready")):
		p_node._post_ready();
	
# unload all worlds
func unload_worlds():
	for world in worlds:
		world.queue_free();
		
	worlds = [];
	return;

# given a node, find the world it is contained within
func get_world(node):
	return BUtil.find_parent_by_class_name(node, "World");
	
	
