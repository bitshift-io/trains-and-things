#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# A class to load in a resource over time (aka stream it in a thread)
# then it emits signals once loaded or some sort of error
#
# used for background menu streaming in and WorldMgr
#
extends Node

signal error(loader, error);
signal loaded(loader);

export(String, FILE) var path;
export(int) var max_time_per_frame_ms = 30; # allow the frame rate to be around 30fps
export(bool) var free_on_load = true;
export(int) var instance_count = 1; # how many instances to spawn - for resource pools?

enum OnLoadOperation { NONE, ADD_TO_PARENT };

export(OnLoadOperation) var on_load_operation = OnLoadOperation.NONE;

var resource;
var thread;
var instances = [];

var start_load_time_us;

# Called when the node enters the scene tree for the first time.
func _ready():
	if (path):
		load_resource(path);
	
	return;


func load_resource(p_path):
	path = p_path;
	start_load_time_us = OS.get_ticks_usec();
	thread = Thread.new();
	thread.start(self, "_thread_load")
	
	
func _done():	
	BLog.debug(path + " loaded in " + str((OS.get_ticks_usec() - start_load_time_us) / 1000000.0) + " seconds");
	emit_signal("loaded", self);
	
	match on_load_operation:
		OnLoadOperation.ADD_TO_PARENT:
			for instance in instances:
				get_parent().add_child(instance);

	_finish();
	return;
	
	
func _error(p_error):
	print(p_error);
	emit_signal("error", self, p_error);
	_finish();
	return;
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _thread_load(user_data):
	print("start _thread_load")
	var loader = ResourceLoader.load_interactive(path);
	if (!loader):
		print("ldr error!");
		call_deferred("_error", "load_interactive failed");
		return;
		
	var result = OK;
	while (result == OK):
		var err = loader.poll();
		if (err == ERR_FILE_EOF):
			resource = loader.get_resource();
			print("loaded, now instancing");
			
			for i in range(0, instance_count):
				instances.append(resource.instance());
				
			print("instanced");
			call_deferred("_done");
			return;
		elif (err != OK):
			print("error");
			call_deferred("_error", err);
			return;
			
	call_deferred("_error", "Unhandled error");
	return;

	
func _finish():
	if (free_on_load):
		queue_free();
	return;


# Thread must be disposed (or "joined"), for portability.
func _exit_tree():
	thread.wait_to_finish()
	return;
