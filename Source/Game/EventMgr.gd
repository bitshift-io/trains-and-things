#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

enum Id {
	BUTTON_PRESSED,
	
	PLAYER_STATE_CHANGED,
	PLAYER_JOINED_COMPANY,
	
	COMPANY_NAME_CHANGED,
	
	BUILDING_CONSTRUCTED,
	BUILDING_LOCATION_SELECTED,
	BUILDING_DELETED,
	
	VEHICLE_CONFIGURATION_DIALOG_DISPLAYED,
	VEHICLE_CONSTRUCTED,
	VEHICLE_EDITED,
	VEHICLE_DELETED,
	
	TRACK_CONSTRUCTION_STARTED,
	TRACK_CONSTRUCTION_FINISHED,
	TRACK_DELETED,
	
	RESOURCE_NODE_LEVEL_UP,
	RESOURCE_NODE_LEVEL_DOWN,
		
	EVENT_MAX
};

#const EVENT_BUTTON_PRESSED = 1001
#const EVENT_PLAYER_STATE_CHANGED = 1002
#
#const EVENT_BUILDING_CONSTRUCTED = 1000
#const EVENT_BUILDING_LOCATION_SELECTED = 1003
#const EVENT_BUILDING_DELETED = 10011
#
#const EVENT_VEHICLE_CONFIGURATION_DIALOG_DISPLAYED = 1005
#const EVENT_VEHICLE_CONSTRUCTED = 1006
#const EVENT_VEHICLE_EDITED = 1009
#const EVENT_VEHICLE_DELETED = 1010
#
#const EVENT_TRACK_CONSTRUCTION_STARTED = 1007
#const EVENT_TRACK_CONSTRUCTION_FINISHED = 1008
#const EVENT_TRACK_DELETED = 1012

signal notify_event(p_params);

#signal notify_event(event_id, sender, args);
#
#
## new style, just using signals. BUT the tutorial intercepts all signals and re-emits them (hence broadcast)
## can we do that with this tecnique? or do we need to keep both?
#signal vehicle_constructed(p_sender, p_vehicle);
#signal vehicle_deleted(p_vehicle);
#signal vehicle_edited(p_sender, p_vehicle);
#
#signal building_deleted(p_building);
#
#signal track_deleted(p_track);


class Event:
	signal emit(p_params);
	
	var id: int;
	
	func _init(p_id):
		id = p_id;
		
	
var events: Dictionary;
	
	
func _ready():
	for i in range(0, Id.EVENT_MAX):
		register_event(i);
		
	return;
	
	
func register_event(p_event_id):
	var event = Event.new(p_event_id);
	events[p_event_id] = event
	return event;
		
	
func get_event(p_event_id) -> Event:
	Util.assert(events.has(p_event_id), "Event not found: " + str(p_event_id));
	return events[p_event_id];
	
	
func trigger(p_event_id, p_params: Dictionary):
	p_params.id = p_event_id; # inject the event
	emit_signal("notify_event", p_params);
	get_event(p_event_id).emit_signal("emit", p_params);
	
	
func listen(p_event_id_list: Array, p_target: Object, p_method: String):
	for event_id in p_event_id_list:
		get_event(event_id).connect("emit", p_target, p_method);
		
	
	
#
#func broadcast_event(event_id, sender, args):
#	emit_signal("notify_event", event_id, sender, args);
