#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Helper class for generating a bigger curve from many smaller curves
# each time the user clicks the mouse you bake the point and a new point is started
# and so on until you are finished
#

extends Node

const BOUNDARY_MAP_INCREMENT = 1.0;

var working_snap_track_list = [];
var snap_to_track_list = [];

var curve_mesh_node_list = [];
onready var curve_mesh_node;

var active = false

var radius;
var blueprint;
var _is_good = true; # used to work out if we can build with this curve or not

var instance_template = null;

var ignore_collisions_list = []; # what colliders are we allowed to collide with?

func _init():
	pass
		
func spawn(p_parent, p_instance_template, p_blueprint):
	p_parent.add_child(self);
	blueprint = p_blueprint;
	instance_template = p_instance_template;

func _ready():
	pass;
	
func set_ignore_collisions_list(ignore_list):
	ignore_collisions_list = ignore_list;
	
func get_length():
	var length = 0
	for c in curve_mesh_node_list:
		length += c.get_world_curve().get_baked_length();
		
	return length
		
func clear_all():
	for c in curve_mesh_node_list:
		c.free();
		
	curve_mesh_node_list.clear();

	working_snap_track_list = []
	snap_to_track_list = []
	
func start(p_radius, pos, dir, snap_to_track_result):   
	# TODO: also do collision check here! 
	if (BUtil.get_boundary_map()):
		var in_bounds = BUtil.get_boundary_map().is_in_bounds(pos, 0);
		if (!in_bounds):
			return;
	
	active = true
	radius = p_radius;

	working_snap_track_list.clear();
	working_snap_track_list.append(snap_to_track_result);

	curve_mesh_node = create_curve_mesh_node();
	curve_mesh_node.get_local_curve().add_point(pos, Vector3(0, 0, 0), dir);
	curve_mesh_node.get_local_curve().add_point(pos);
	return;
	
func create_curve_mesh_node():
	var node = BCurveMeshNode.new();
	node.local_curve.radius = radius;
	node.set_mesh_generation_type(node.MGT_BOUNDS);
	node.set_instance_template(instance_template);
	node.set_name("BCurveMeshNode");
	node.connect("mesh_generated", self, "_mesh_generated");
	add_child(node);
	return node;
	
func end():
	active = false;
	if (curve_mesh_node):
		curve_mesh_node.free();
		
	curve_mesh_node = null;

func undo_last():
	# remove the last spline, and start the spline from the beginning of this last spline
	if (curve_mesh_node_list.size() <= 0):
		end();
		return false;
		
	var last_curve_mesh_node = curve_mesh_node_list.pop_back();
	var last_curve = last_curve_mesh_node.get_world_curve();
	
	# need to pop 2 of the snap to track list
	var last_snap_to_track = snap_to_track_list.pop_back();
	last_snap_to_track = snap_to_track_list.pop_back();
	
	# there should be a snap for each end of each curve
	var snap_size = snap_to_track_list.size();
	Util.assert((curve_mesh_node_list.size() * 2) == snap_size, "Snap size error");

	end();
	start(radius, last_curve.get_point_pos(0), last_curve.get_point_out(0), last_snap_to_track);
	last_curve_mesh_node.free();
	return true;
	
func is_active():
	return active

func update_end_point(pos, dir):
	if (!is_active()):
		return
		
	# wait for the last mesh generation task to finish to stop flickering
	if (curve_mesh_node.is_generating_mesh()):
		return;
		
	var curve = curve_mesh_node.get_local_curve();
	
	# remove all points except start point
	# but if there is no change in the position/direction, then bail
	while (curve.get_point_count() > 1):
		var lastPointIdx = curve.get_point_count() - 1
		var ppos = curve.get_point_pos(lastPointIdx);
		var pin = curve.get_point_in(lastPointIdx);
		var pout = curve.get_point_out(lastPointIdx);
		
		ppos.y = pos.y; # the curve negates the Y value, so add it back in for the comparison
		if (pos == ppos && pin == -dir && pout == dir):
			return;
			
		curve.remove_point(lastPointIdx)
	
	var start_pos = curve.get_point_pos(0);
	var dist = (start_pos - pos).length();
	#if (dist >= 1000):
	#	Util.assert("Stretching CurveEditor to far, start_pos:" + start_pos + ", end_pos:" + pos);
	#	return;
	
	curve.add_point(pos, -dir, dir);
	
	var world_curve = curve_mesh_node.get_local_curve();
	for i in range(0, curve.get_point_count()):
		Util.assert(world_curve.get_point_pos(i) == curve.get_point_pos(i), "Problem with curve editor!");
	
	curve_mesh_node.generate_collision();
	
	if (BUtil.get_boundary_map()):
		var in_bounds = BUtil.get_boundary_map().is_in_bounds(pos, 0);
		if (!in_bounds):
			_set_good(false, "Out of bounds");
			return;
		
	_check_for_goodness();
	
	curve_mesh_node.generate_mesh();	
	# _mesh_generated will be called when the mesh is generated
	return;
	
func _mesh_generated(mesh_inst):
	blueprint.set_mesh_instance_material(mesh_inst, blueprint.get_material_from_bool(_is_good));
	return;
	
# check along the shape to see if it is good or bad - ie. what does it collide with?
func _check_for_goodness():
	# 1. check for boundary offenses!
	var world_curve = curve_mesh_node.get_world_curve();
	var length = world_curve.get_baked_length();
	for i in range(0, length, BOUNDARY_MAP_INCREMENT):
		var xform = world_curve.get_transform_from_distance_along_curve(i);
		if (BUtil.get_boundary_map()):
			var in_bounds = BUtil.get_boundary_map().is_in_bounds(xform.origin, 0);
			if (!in_bounds):
				_set_good(false, "Out of bounds");
				return;
			
	# 2. check for collisions (except vehicle collisions)
	var collision_nodes_to_ignore = curve_mesh_node.get_collision_nodes() + BUtil.get_terrain().get_collision_nodes();
	var results = curve_mesh_node.intersect_shape(32, collision_nodes_to_ignore);  # ignore terrain
	for result in results:
		var collider = result["collider"];
		#BLog.debug("colliding with: " + collider.get_path());
		
		# don't collide with outself!
		#if (curve_mesh_node.is_a_parent_of(collider)):
		#	continue;
		
		if (!collider.is_visible_in_tree()):
			continue;
		
		# ignore a user specificed set of stuff - used for when we snap to track
		var ignore_col = false;
		for ignore in ignore_collisions_list:
			if (collider == ignore || BUtil.is_child_of(collider, ignore)):
				ignore_col = true;
				break;
				
		if (ignore_col):
			continue;
			
		# get the previous snap point, ignore any tracks connected to that junction (if there is a junction)
		# else just check the node being snapped against
		if (working_snap_track_list.size() && working_snap_track_list.back()):
			var snap_track = working_snap_track_list.back();
			var node = snap_track["node"];
			var junction = snap_track["snap_to_junction"];
			if (junction):
				var track_list = junction.getTracks();
				for track in track_list:
					if (collider == track || BUtil.is_child_of(collider, track)):
						ignore_col = true;
						break;
			else: #if (node):
				if (collider == node || BUtil.is_child_of(collider, node)):
						continue;
						
		if (ignore_col):
			continue;
			
		# ignore our previous track we laid
		#for ignore in curve_mesh_node_list:
		if (curve_mesh_node_list.size()):
			var ignore = curve_mesh_node_list.back();
			if (collider == ignore || BUtil.is_child_of(collider, ignore)):
				continue;
			
		#BLog.debug("collider path:" + collider.get_path());
		var parent = BUtil.find_parent_by_class_name(collider, "Vehicle");
		if (!parent):
			_set_good(false, "Collided with collider:" + collider.get_path());
			return;
			
	# whew, we made it, time to ooze goodness
	_set_good(true, "Good");
	return;
	
func _set_good(p_good, p_reason):
	_is_good = p_good;
	
	#if (!p_good):
	#	BLog.debug("CurveEditor: " + p_reason);
	
	blueprint.set_mesh_instance_material(curve_mesh_node.get_mesh_instance(), blueprint.get_material_from_bool(p_good));
	return;
	
func is_good():
	return _is_good;

# bakes this point and it now becomes the new start point for the next segment of the curve
func bake_end_point(pos, dir, snap_to_track_result):
	if (!is_active()):
		return;
		
	if (BUtil.get_boundary_map()):
		var in_bounds = BUtil.get_boundary_map().is_in_bounds(pos, 0);
		if (!in_bounds):
			return;
	
	var curve = curve_mesh_node.get_local_curve();
	
	# if some direction is supplied, use that, else
	# use the existing curve direction
	if (dir.length_squared() <= 0.0):
		var t = curve.get_transform_from_distance_along_curve(curve.get_baked_length());
		dir = -t.basis[2];
		pos = t.origin;
	
	curve_mesh_node_list.append(curve_mesh_node);
	curve_mesh_node = null;
	
	for snap in working_snap_track_list:
		snap_to_track_list.append(snap);
		
	snap_to_track_list.append(snap_to_track_result);
	
	# start next segment of the curve starting at the current snap point
	start(radius, pos, dir, snap_to_track_result);
	return;


func get_curve_list():
	var curve_list = [];
	for c in curve_mesh_node_list:
		curve_list.append(c.get_world_curve());
		
	return curve_list;


#
#func updateDebugDrawing():
#	# draw the path
#	var pointList = curve.tesselate(5, 4);
#
#	im.set_material_override(m)
#	im.clear()
#	im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
#	for x in pointList:
#		im.add_vertex(x)
#	im.end()
	
# just get the top as a flat mesh
#func createCheap():
#	var newMesh = b_curve_mesh.generate_mesh_from_bounds(Transform());
#	if (newMesh == null):
#		return;
#
#	# check to see if this collides with any shapes (ignore vehicles)
#	# for now any collision will be bad and stop construction
#	isGood = true;
#	var results = b_curve_mesh.intersect_shape();
#	for result in results:
#		var collider = result["collider"];
#		#BLog.debug("collider path:" + collider.get_path());
#		var parent = BUtil.find_parent_by_class_name(collider, "Vehicle");
#		if (!parent):
#			isGood = false;
#			break;
#
#	if (isGood):
#		newMesh.surface_set_material(0, blueprint.get_material(blueprint.TYPE_GOOD));
#	else:
#		newMesh.surface_set_material(0, blueprint.get_material(blueprint.TYPE_BAD));
#
#	var meshInst = MeshInstance.new();
#	meshInst.set_mesh(newMesh);
#	add_child(meshInst);
#	return;

	
