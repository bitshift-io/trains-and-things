#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

onready var button_icon = BUtil.find_child(self,"ButtonIcon");
export(bool) var target_camera = false;

var active = false;

onready var world = WorldMgr.get_world(self); 

# Called when the node enters the scene tree for the first time.
func _ready():
	if (!world):
		return;
		
	add_to_group("vantage_points");
	button_icon.set_visible(false);
	set_process_input(false);
	set_process_unhandled_input(false);
	return;
	
	
func _post_ready():
	var pc = world.master_player;
	if (pc && pc.hud):
		pc.hud.add_context_ui(button_icon);
	return;


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# keep up up, remove rotation around x and z axis
	if (active):
		var xform = get_global_transform();
		var basis = xform.basis;
		var up = Vector3(0, 1, 0);
		var new_x = up.cross(basis.z);
		var new_z = new_x.cross(up);
		xform.basis.x = new_x;
		xform.basis.y = up;
		xform.basis.z = new_z;
		set_global_transform(xform);
		
	return;
	
	
func _input(event):
	# any press on any device disables the vantage point
	# by restoring the previous camera
	if ((event is InputEventKey && event.get_scancode() == KEY_ESCAPE)):
		get_tree().set_input_as_handled();
		set_process_input(false);
		active = false;
		var pc = world.master_player;
		if (pc):
			var cam = pc.get_current_camera();
			pc.add_camera(cam); # reparent camera back to PC
			pc.pop_camera();
			pc.get_cursor().set_visible(true);
			pc.hud.set_visible(true);


func _on_ButtonIcon_pressed():
	if (!world):
		return;
		
	var pc = world.master_player;
	if (!pc):
		return;
		
	# parent cam to self so the camera moves with the vantage point
	var cam = pc.find_camera("FreeCamera");
	BUtil.reparent(cam, $Spatial);
	cam.translation = Vector3(0, 0, 0);
	pc.push_camera(cam);
	pc.get_cursor().set_visible(false);
	
	_activate();
	
	pc.hud.set_visible(false);
	return;
	
	
func _activate():
	set_process_input(true);
	active = true;
	var pc = world.master_player;
	if (pc):
		pc.toggle_vantage_points(null);
		
	set_process(true);


func set_mouse_over(controller, mouseOver):
	button_icon.set_visible(mouseOver);
	set_process(mouseOver);

	if (mouseOver):
		emit_signal("on_mouse_over"); # can we connect it via the editor?
	else:
		emit_signal("on_mouse_leave");
	return;
