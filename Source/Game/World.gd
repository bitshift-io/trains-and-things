#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# The world is everything related to a game play session
# map, map wide globals etc that should be cleaned up at the end of a
# game 
#
# So lets expose some globals the game components can access...
#

extends Node

# some world globals
var network;
var date_time;
var game_mode;
var astar;
var market;

var companies;
var tracks;
var junctions;
var trains;
var bridges_and_tunnels;
var airplanes;
var trucks;
var roads;
var players;
var master_player; # the master (network master ie. local) player

var map;

var world_container = null;
var world_viewport = null;
var ui_viewport = null;
var lights = [];
var environments = [];

func _init():
	return;
	
# a time to setup references to globals
func _pre_ready():
	network = BUtil.find_child(self, "Network");
	date_time = BUtil.find_child(self, "DateTime");
	game_mode = BUtil.find_child(self, "GameMode");
	astar = BUtil.find_child(self, "AStar");
	market = BUtil.find_child(self, "Market");
	
	companies = BUtil.find_child(self, "Companies");
	tracks = BUtil.find_child(self, "Tracks");
	junctions = BUtil.find_child(self, "Junctions");
	trains = BUtil.find_child(self, "Trains");
	bridges_and_tunnels = BUtil.find_child(self, "BridgesAndTunnels");
	airplanes = BUtil.find_child(self, "Airplanes");
	trucks = BUtil.find_child(self, "Trucks");
	roads = BUtil.find_child(self, "Roads");
	players = BUtil.find_child(self, "Players");
	
	world_viewport = BUtil.find_child(self, "WorldViewport");
	world_container = BUtil.find_child(self, "WorldContainer");
	ui_viewport = BUtil.find_child(self, "UIViewport");
	
	#set_world_in_viewport(false);
	return;


func _ready():
	Database.connect("settings_changed", self, "settings_changed");
	
	# find list of world env's
	environments = BUtil.find_children_by_class_name(self, 'WorldEnvironment');
	
	
	# find list of lights
	# we assume the directional lights are the shadow casting ones only
	lights = BUtil.find_children_by_class_name(self, 'DirectionalLight');
#	for l in arr:
#		if l.shadow_enabled == true:
#			lights.append(l);
	
	settings_changed();
#
#	# Register console commands
#	Console.register_command("world.set_map_in_viewport", {
#		method = funcref(self, "set_map_in_viewport"),
#		owner = self,
#		description = "Push map into or out of the world viewport",
#		args = ["bool"]
#	});
#
	# allow the environment to be tweaked by the user
	# ATM this is done like so in the console:
	# 	/c world.set_map_in_viewport = false
	Console.register_variable('world', {
		variable = self,
		owner = self,
		description = "world",
	});
	
	return;


func update_environments():
	var shader_quality = Database.get_value(Database.SHADER_QUALITY, 0);
	
	for env in environments:
		# glow on at lvl 1
		if (shader_quality == 0):
			env.environment.glow_enabled = false;
		else:
			env.environment.glow_enabled = true;
			
		# reflections on at lvl 3
		if (shader_quality >= 3):
			env.environment.ss_reflections_enabled = true;
		else:
			env.environment.ss_reflections_enabled = false;
	
		# ssao on at lvl 2
		if (shader_quality >= 2):
			env.environment.ssao_enabled = true;
			#env.environment.ssao_quality = shader_quality - 1;
		else:
			env.environment.ssao_enabled = false;
	return;


func update_lights():
	var shadow_quality = Database.get_value(Database.SHADOW_QUALITY, 0);
	
	for light in lights:
		#light.shadow_bias = 0; # disabled this as it is causing jagged shadows! terrible!
		light.shadow_reverse_cull_face = true;
		
		var shadow_atlas_size_setting = 0;
		if (shadow_quality == 0):
			light.shadow_enabled = false;
		else:
			light.shadow_enabled = true;
			
#		if (shadow_quality == 1):
#			 light.shadow_atlas_size_setting = 4;
##		if (shadow_quality == 2):
##			 light.shadow_atlas_size_setting = 8;
#		if (shadow_quality == 3):
#			 light.shadow_atlas_size_setting = 16;
#		if (shadow_quality == 4):
#			 light.shadow_atlas_size_setting = 32;

	return;


func settings_changed():
	update_environments();
	update_lights();
	return;


func add_map(p_map):
	map = p_map;
	# rename root node to "World" 
	map.set_name("Map");
	BUtil.reparent(map, world_container);
	return;

# testing to see if we get any extra FPS by removing the WorldViewport container
func set_world_in_viewport(p_in):
	if (p_in):
		world_viewport.get_parent().visible = true;
		BUtil.reparent(world_container, world_viewport);
	else:
		world_viewport.get_parent().visible = false;
		BUtil.reparent(world_container, self);
		move_child(world_container, 0);
		
	return;
