#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

#onready var Company = preload("res://Game/Company.gd").new();
onready var world = WorldMgr.get_world(self); 
var game_time = 0;
var previous_sorted_companies = [];

func _ready():
	if (!world):
		return;
		
	if (!get_tree().is_network_server()):
		return;

	Console.register_command("endgame", {
		method = funcref(self, "console_end_game"),
		owner = self,
		description = "end the game",
		args = []
	})
	
	world.date_time.connect("month_changed", self, "month_changed")
	return;
	
	
func month_changed(month):
	# go for next frame incase some interest or 
	# fee the player must pay gets deducted!
	call_deferred("check_end_game");
		
		
func check_end_game():
	# check for company win condition
	var companies = get_tree().get_nodes_in_group("companies");
	var winners = [];
	var game_over = false;
	
	for c in companies:
		if (meets_company_merger_condition(c) || (meets_bank_loan_amount_condition(c) && meets_company_net_worth_condition(c))):
			winners.append(c);
			game_over = true;
			
	# check for timeout
	var time_limit = BDatabase.get_value("game_settings/win_time_limit", -1);
	if (time_limit && time_limit >= 0):
		var elapsed_minutes = world.date_time.seconds / 60;
		if (elapsed_minutes >= time_limit):
			game_over = true;
			
			
	companies.sort_custom(Company, "sort_net_worth");

	if (!game_over):
		# check if some company has moved up in the net worth race, and if so fire an event for all players to see to keep them aware of the ranking
		for ci in range(0, companies.size() - 1):
			var c = companies[ci];
			
			# has c ranked up since last time we checked the win condition? do this be searching through previous_sorted_companies
			# to see if it has moved up from where it was
			var c_ranked_up = false;
			for pci in range(ci + 1, previous_sorted_companies.size()):
				var pc = previous_sorted_companies[pci];
				if (pc == c):
					c_ranked_up = true;
					
			if (!c_ranked_up):
				continue;
				
			var cn = companies[ci + 1];
			
			# c has moved up ahead of cn
			var players = get_tree().get_nodes_in_group("players");
			for p in players:
				p.show_event({ "id": "COMPANY_RANK_UP", "display_name": c.get_display_name(), "display_name_other": cn.get_display_name(), "rank": (ci + 1) });
		
		previous_sorted_companies = companies;
		return false
		
	# sort by net worth
	winners.sort_custom(Company, "sort_net_worth");
	end_game(companies, winners);
				
			
func meets_company_merger_condition(company):
	# this condition doesn't apply in single player
	if (get_tree().get_nodes_in_group("players").size() <= 1):
		return false;
		
	if (get_tree().get_nodes_in_group("companies").size() <= 1):
		return true;
	
	# TODO: need to know if a merger has occured. Company might need to tracl
	# how many mergers it has done	
	#var merger = BDatabase.get_value("game_settings/win_company_merger", 'disabled');
		
	return false;
	
	
func meets_bank_loan_amount_condition(company):
	var max_amount = BDatabase.get_value("game_settings/win_company_loan_amount", -1);
	if (!max_amount || max_amount == -1):
		return true;
		
	if company.loan <= max_amount:
		return true;
		
	return false;
		
		
func meets_company_net_worth_condition(company):
	var net_worth = BDatabase.get_value("game_settings/win_company_net_worth", -1);
	if (!net_worth || net_worth == -1):
		return false;
	
	if company.get_net_worth() >= net_worth:
		return true;
		
	return false;
	

func end_game(companies, winners):
	assert(get_tree().is_network_server());
	
	var players = get_tree().get_nodes_in_group("players");
	for p in players:
		p.show_end_game_panel(companies, winners);
		
	world.date_time.disconnect("month_changed", self, "month_changed")
	return
	
	
func console_end_game():
	var companies = get_tree().get_nodes_in_group("companies");
	companies.sort_custom(Company, "sort_net_worth");
	end_game(companies, []);
	return;
	
