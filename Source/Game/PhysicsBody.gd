#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Add enable/disable support to physics bodies
#
extends PhysicsBody

var old_layer_mask = 0
var old_collision_mask = 0


func _ready():
	old_layer_mask = get_collision_layer();
	old_collision_mask = get_collision_mask();
	

func set_enabled(p_enabled):
	if (p_enabled):
		set_collision_layer(old_layer_mask);
		set_collision_mask(old_collision_mask);
	else:
		old_layer_mask = get_collision_layer();
		old_collision_mask = get_collision_mask();
		set_collision_layer(0);
		set_collision_mask(0);
