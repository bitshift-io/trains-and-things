#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#


extends Node

var file;
var node_stack = [];
var path_stack = [];

var node_map = {};
var yet_to_load_map = {};

const HEADER = "Header";

func free_peristance_objects():
	# TODO: really this should be a wekref to a company
	# remove players from their companies
	for p in get_tree().get_nodes_in_group("players"):
		p.join_company(null);
		
	# We need to revert the game state so we're not cloning objects during loading.  This will vary wildly depending on the needs of a project, so take care with this step.
	# For our example, we will accomplish this by deleting savable objects.
	
	# the problem is, not all nodes that are in the persist group are all spawned from a prefab.
	# for this reason I check for get_filename() to indicate which nodes are from a prefab
	# if this is still problematic in future, move the handling of this to each node to allow it to 
	# work out what to do here
	var savenodes = get_tree().get_nodes_in_group("persist")
	for node in savenodes:
		if (node.get_filename() != null):
			node.set_name(node.get_name() + "_deleted"); # change name so we can continue to load while old stuff is still around
			node.queue_free();

# save some globals to file
func save_header():
	path_stack = [HEADER];
	
	var app_version = BDatabase.get_value("application/version", "");
	var app_name = BDatabase.get_value("application/name", "");
	
	append_dict({
		app_version = app_version,
		app_name = app_name,
		map_name = "Hawaii" # TODO:
	});
	
	path_stack = [];
	return;
	
# load globals and verify if this is a legitimate save
func load_header():
	path_stack = [HEADER];
	var c = get_current_node_dict();
	
	var app_version = BDatabase.get_value("application/version", "");
	var app_name = BDatabase.get_value("application/name", "");
	
	if (c["app_name"] != app_name):
		return "Incorrect game";
		
	if (c["map_name"] != "Hawaii"):
		return "Incorrect map";
	
	path_stack = []; 
	return null;
	
func load_from_file(filename):
	var f = File.new();
	if !f.file_exists(filename):
		return;

	free_peristance_objects();
	
	# First pass instances the objects
	# the second pass populates the instances
	# such that objects can be resolved
#	
	file = ConfigFile.new();
	file.load(filename);
	
	var err = load_header();
	if (err):
		return false;
	
	for k in file.get_sections():
		if (k == HEADER):
			continue;
			
		var node_path = NodePath(k);
		var node_filename = file.get_value(k, "filename");
		var node;
		if (node_filename != null):
			node = load(node_filename).instance();
			
			# set the object name
			var paths = split_path(k);
			node.set_name(str(paths[1]));

		node_map[node_path] = node;
		yet_to_load_map[node_path] = node;

		
#	# if no node here, then this node was not loaded from a file,
#	# so try to load the parent first
#	if (!node):
#		var paths = split_path(node_path);
#		try_load_node(paths[0]);
#
#		# now parent is loaded, resolve this node
#		# and load it
#		node = get_node(node_path);
#		node_map[node_path] = node;
		
		
	# now add any nodes that were not instanced from file directly (that is, their parent was loaded from file)
#	for node_path in non_load_nodes:
#		var k = String(node_path);
#		var node = get_node(node_path);
#
#		node_map[node_path] = node;
#		yet_to_load_map[node_path] = node;
#
#		#path_stack = [node_path];
#		#node_stack = [node];
#
#		# set the object name
#		var paths = split_path(k);
#		node.set_name(str(paths[1]));
#
#	path_stack = [];
#	node_stack = [];
	
	while (yet_to_load_map.size()):
		var k = yet_to_load_map.keys().back();
		try_load_node(k);
		
	return true;
		
# This will try to load the node given a path
# if it has not been loaded yet
# if it has already been loaded, it doesnt need to be loaded again
func try_load_node(node_path):
	if (!yet_to_load_map.has(node_path)):
		return;
		
	# pop from yet_to_load_map
	yet_to_load_map.erase(node_path);

	var node = node_map[node_path];
	
	# if no node here, then this node was not loaded from a file,
	# so try to load the parent first
	if (!node):
		var paths = split_path(node_path);
		try_load_node(paths[0]);

		# now parent is loaded, resolve this node
		# and load it
		node = get_node(node_path);
		node_map[node_path] = node;
		Util.assert(node, "Could not resolve: " + node_path);
		
	path_stack.push_back(node_path);
	node_stack.push_back(node);
	
	node.load_from(self);
	
	# pop back
	path_stack.pop_back();
	node_stack.pop_back();
	return;
	
func split_path(path):
	var path_str = str(path);
	var last_slash_idx = path_str.find_last("/");
	if (last_slash_idx <= 0):
		return [path];
		
	var left = path_str.left(last_slash_idx);
	var right = path_str.right(last_slash_idx + 1);
	return [NodePath(left), NodePath(right)];
	
func resolve_parent():
	var paths = split_path(get_current_node_path());
	var parent_path = paths[0];
	var parent = get_node(parent_path);
	
	# parent not found, hrmm we are probably loading it
	# so lets resolve the parent
	if (!parent):
		parent = resolve_node(parent_path);
		
	Util.assert(parent, "Invalid parent");
	return parent;
	
#func add_child_to_parent():
#	var paths = split_path(get_current_node_path());
#	var parent_path = paths[0];
#	var parent = get_node(parent_path);
#
#	# parent not found, hrmm we are probably loading it
#	# so lets resolve the parent
#	if (!parent):
#		parent = resolve_node(parent_path);
#
#	Util.assert(parent, "Invalid parent");
#	parent.add_child(get_current_node());
	
# used to resolve a path to an object
# if an object is to be resolved, it means it should be loaded
# before the object trying to use it, so this code will do that
func resolve_node(path):	
	if (node_map.has(path)):
		try_load_node(path);
		return node_map[path];
	
	# if a user is trying to resolve and its not found
	# we need to go back and try to resovle a subsection of the path
	# then if found, look forward for its children	
	var paths = split_path(str(path));
	while (paths.size() == 2):
		var parent_node_path = paths[0];
		if (node_map.has(parent_node_path)):
			var node = node_map[parent_node_path];
			try_load_node(parent_node_path);
			var right = str(path).right(str(parent_node_path).length() + 1);
			var child = node.get_node(right);
			return child;
			
		paths = split_path(parent_node_path);
		
	Util.assert(false, "Should never reach here"); # couldn't resolve something we need resolved!
	return null;
	
# apply builtsin to the current node
func load_builtins():
	var builtins = file.get_value(str(get_current_node_path()), "builtins", {});
	for key in builtins.keys():
		get_current_node().set(key, builtins[key]);
		
func get_current_node_path():
	return path_stack.back();
	
func get_current_node():
	return node_stack.back();
	
func save_to_file(filename):
	file = ConfigFile.new();
	
	save_header();
	
	var savenodes = get_tree().get_nodes_in_group("persist");
	for i in savenodes:
		node_stack = [i];
		
		# some data we need to save for each object
		path_stack = [get_current_node().get_path()];
		var ok = get_current_node().save_to(self);
		if (ok):
			append_dict({
				filename = get_current_node().get_filename()
			});
	
	file.save(filename);
	
func append_dict(dict):
	var section_str = str(get_current_node_path());
	for key in dict.keys():
		var value = dict[key];
		file.set_value(section_str, key, value);
	
func append_builtins(dict):
	var builtins = file.get_value(str(get_current_node_path()), "builtins", {});
	
	for key in dict.keys():
		builtins[key] = dict[key];
		
	file.set_value(str(get_current_node_path()), "builtins", builtins);
	
func get_current_node_dict():
	var d = {};
	for key in file.get_section_keys(str(get_current_node_path())):
		d[key] = file.get_value(str(get_current_node_path()), key);
	return d;

	
