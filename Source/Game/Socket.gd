#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var socket = StreamPeerTCP.new()
var accum = 0  # delta accumulator
var _active = false   # socket intended for use
var _connected = false  # socket is connected

const ERR = 1

export(String) var host = "localhost"
export(int) var port = 7777

signal textReceived(msg)

var data = ""

var updateRate = 0.5

func _ready():
	set_process(true)
	
func _process(delta):
	_readloop(delta)	

func write(string):
	if(_connected):
		#socket.put_utf8_string(string)
		socket.put_partial_data(_string_to_raw_array(string))
		#BLog.debug("wrinting to irc: %s" % string)

func start(host, port):
	self.host = host
	self.port = port
	_active = true
	var err = socket.connect_to_host(host, port)
	_connected = socket.is_connected_to_host()
	return err

func end():
	_active = false
	_connected = false
	socket.disconnect()

func _readloop(delta):
	if(not _active):
		pass
		
	# TODO, emit errors and data
	accum += delta
	
	if (accum > updateRate):
		accum = 0
		var connected = socket.is_connected_to_host()
		
		if(not connected):
			_respond("Lost Connection", ERR)
		else:
			var output = socket.get_partial_data(1024)
			var errCode = output[0]
			var outputData = output[1]
			
			if(errCode != 0):
				_respond( "ErrCode:" + str(errCode), ERR)
			else:
				var outStr = data + outputData.get_string_from_utf8()
				data = ""
				if(outStr != ""):
					var lineArray = outStr.split("\r\n")						
					for l in range(0, lineArray.size() -1):					
						_respond(lineArray[l], 0)
						
					data = lineArray[lineArray.size() - 1]
						
					

func _respond(msg, errCode):
	if(errCode == 0):
		_respondOK(msg)
	elif(errCode == ERR):
		_respondErr(msg)

func _respondOK(msg):
	emit_signal("textReceived", msg)
	pass

func _respondErr(msg):
	# TODO emit?
	pass
func _string_to_raw_array(string):
	var le = string.length()
	var raw = Array()
	var i=0
	
	while(i<le):
		raw.push_back( string.ord_at(i) )
		i=i+1
	
	return raw
