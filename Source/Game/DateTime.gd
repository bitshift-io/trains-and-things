#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

# some way to configure the game speed, how long is a month and year etc?
# signal to indicate when a month changes
# signal when the year changes

var MONTH_TIME = 30.0 # seconds

var monthTimer = Timer.new()
var secondTimer = Timer.new()

const start_year = 1900;

var seconds = 0;
var months = 0;

var year = null
var month = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
var monthIndex = null

signal year_changed(year)
signal month_changed(monthIndex)
signal second()

func _init():
	pass
	
func _ready():
	monthTimer.set_one_shot(false)
	monthTimer.set_wait_time(MONTH_TIME)
	monthTimer.connect("timeout", self, "monthTimeout")
	add_child(monthTimer) 
	
	secondTimer.set_one_shot(false)
	secondTimer.set_wait_time(1.0)
	secondTimer.connect("timeout", self, "secondTimeout")
	add_child(secondTimer) 
	
	Game.connect("game_started", self, "game_started")
	
func game_started():
	seconds = 0;
	months = -1;
	year = start_year;
	monthIndex = -1
	secondTimeout()
	monthTimeout()
	monthTimer.start()
	secondTimer.start()

func secondTimeout():
	emit_signal("second")
	seconds += 1

	
func monthTimeout():
	months += 1
	monthIndex += 1
	if (monthIndex >= 12):
		monthIndex = 0
		year += 1		
		emit_signal("year_changed", year)
		
	emit_signal("month_changed", monthIndex)
	
func getDateString():
	return "%s %d" % [month[monthIndex].substr(0, 3), year]
	

# given a months value, work out the date in the form "Q1 1990"
func months_to_quarter_date_str(p_months: int) -> String:
	var yr = start_year + int(float(p_months) / 12.0);
	var monthIdx = p_months % 12;
	var quarter = (monthIdx / 3) + 1;
	return "Q%d %d" % [quarter, yr];
	
	
# convert a real world time into game time
func monthDelta(delta: float) -> float:
	return delta / MONTH_TIME
	
# convert a yearly value to a monthly value
func yearlyToMonthly(yearValue: float) -> float:
	return yearValue / 12.0
	
# given a number (XXX occurs 'time' times per month)
# return a value
func monthFrequency(time):
	if (time == 0):
		return 0;
		
	return float(MONTH_TIME) / float(time);
	
func yearFrequency(time):
	if (time == 0):
		return 0;
		
	return float(MONTH_TIME * 12) / float(time);
