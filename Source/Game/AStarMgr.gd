#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This class manages the AStar networks
#
extends Node



const RAIL = "rail"
const ROAD = "road"
const AIR  = "air"

var astarMap = {};

class AStarInt extends AStar:
	var AStarPath = preload("AStarPath.gd");
	var idObjMap = {};
	
	func _init():
		return;
	
	#func _estimate_cost(from, to):
	#	return get_point_pos(from).distance_squared_to(get_point_pos(to));

	func _compute_cost(from, to):
		return idObjMap[from].get_astar_weight() + idObjMap[to].get_astar_weight();
		
	func add_point_object(object):
		var id = get_available_point_id();
		.add_point(id, Vector3(0, 0, 0), 1.0);
		idObjMap[id] = object;
		object.astarId = id;
		return id;
		
	func remove_point_object(object):
		var id = object.astarId;
		if (id != null):
			.remove_point(id);
		
	func get_astar_path(fromJunction, toJunction):
		var path = get_path(fromJunction, toJunction);
		if (path.size() <= 0):
			return null;
			
		var astar_path = AStarPath.new();
		astar_path.set_path(path);
		return astar_path;
		
	func get_path(fromJunction, toJunction):
		if (fromJunction == null || toJunction == null):
			return [];

		var idPath = get_id_path(fromJunction.astarId, toJunction.astarId);
		var objPathList = [];
		for id in idPath:
			var obj = idObjMap[id];
			objPathList.append(obj);
			
		return objPathList;
		
	func get_path_weight(obj_path_list):
		var weight = 0.0;
		for obj in obj_path_list:
			weight += obj.get_path_weight();
			
		return weight;
		
	func connect_point_objects(from, to):
		connect_points(from.astarId, to.astarId, false);
		
	func disconnect_point_objects(from, to):
		disconnect_points(from.astarId, to.astarId);

# end class AStarInt

func _ready():
	return;

func get(type):
	if (astarMap.has(type)):
		return astarMap[type];
	
	var astar = AStarInt.new();
	astarMap[type] = astar;
	return astar;
	
	
