#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Holds a AStar pathing result as a list of game objects
#

extends Node

var path = []; # TODO: this might need to be an arrya of weak refs!

func set_path(p_path):
	path = p_path;
	return;
	
func _ready():
	pass
	
func size():
	return path.size();
	
# get weight of the whole path
func get_weight():
	var weight = 0.0
	for obj in path:
		weight += obj.get_astar_weight();
		
	return weight;
	
# get weight up till there is a blockage in the path
func get_weight_to_first_blockage():
	var weight = 0.0
	for obj in path:
		if (BUtil.is_class_name(obj, "Track")):
			var occupant = obj.get_occupant();
			if (occupant):
				break;
				
		weight += obj.get_astar_weight();
		
	return weight;
	
func find_first_track():
	for obj in path:
		if (BUtil.is_class_name(obj, "Track")):
			return obj;
			
	return null;
	
func find_last_track():
	for i in range(path.size() - 1, -1, -1):
		if (BUtil.is_class_name(path[i], "Track")):
			return path[i];
			
	return null;
	
func find_next_track(p_current_track):
	var i = 0;
	for i in range(0, path.size()):
		if (path[i] == p_current_track):
			break;
			
	for i in range(i + 1, path.size()):
		if (BUtil.is_class_name(path[i], "Track")):
			return path[i];
			
	return null;
	
# is there a loop in the path?
func has_loop():
	var dict = {};
	for obj in path:
		if (dict.has(obj)):
			return true;
			
		dict[obj] = true;
		
	return false;
	