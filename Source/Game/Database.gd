#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#


extends Node
signal settings_changed();

# kbitshift keys
const TERRRAIN_AMBIENT_OCCLUSION = 'bitshift/terrain/ambient_occlusion';
const TERRRAIN_DETAIL_NORMAL_MAPPING = 'bitshift/terrain/detail_normal_mapping';
const TERRAIN_MODEL_QUALITY = 'bitshift/terrain/model_quality';
const TERRAIN_SKIRT_ENABLED = 'bitshift/terrain/skirt_enabled';
const TERRAIN_SHADOWS_ENABLED = 'bitshift/terrain/shadows_enabled';
const LOD_DISTANCE_OFFSET = 'bitshift/lod/distance_offset';

# engine keys
const ANISOTROPIC_FILTER_LEVEL = 'rendering/quality/filters/anisotropic_filter_level';
const SHADOW_FILTER_MODE = 'rendering/quality/shadows/filter_mode';
const DIRECTIONAL_SHADOW_SIZE  = 'rendering/quality/directional_shadow/size';
const SHADOW_ATLAS_SIZE = 'rendering/quality/shadow_atlas/size';

# keys for override.cfg
const MONITOR = 'video/monitor';
const PLAYER_NAME = 'game/player_name';
const HIDE_CITY_NAMES = 'game/hide_city_names';
const CAMERA_MOVE_SPEED = "game/camera_move_speed";
const CAMERA_ZOOM_SPEED = "game/camera_zoom_speed";
const JOIN_ADDRESS = 'game/join_address';
const STRETCH_SHRINK = 'video/stretch_shrink';
const FRAME_RATE = 'video/frame_rate';
const MSAA = 'video/msaa';
const WINDOW_FULL_SCREEN = 'video/window_fullscreen';
const MODEL_QUALITY = 'video/model_quality';
const SHADOW_QUALITY =  'video/shadow_quality';
const SHADER_QUALITY = 'video/shader_quality';
const MASTER_VOLUME =  'audio/master';
const EFFECT_VOLUME = 'audio/effect';
const MUSIC_VOLUME = 'audio/music';
const INTERFACE_VOLUME = 'audio/interface';

func _ready():
	reload();
	return;


func value_changed(name, value):
	return;


func get_value(name, default):
	return BDatabase.get_value(name, default);


func set_setting(name, value):
	BDatabase.set_setting(name, value);


func save():
	BDatabase.save();
	emit_signal("settings_changed");
	return;
	
	
func reload():
	BDatabase.clear();
	BDatabase.load("res://Database");
	
	
func insert(p_dict):
	BDatabase.insert(p_dict);
	
	
func get_player_name():
	var player_name = get_value(PLAYER_NAME, "Recruit");
	return player_name;


	
