#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Object specific instance to handle outline selection fx
# 
extends Node

# TODO: move this material to data
onready var outline_material = load("res://Materials/M-OutlineObject.tres");

var spatial_node;

onready var world = WorldMgr.get_world(self); 

# takes the outline node, detaches it from the parent
# and moves it into the effect mgr scene
func prepare(mesh_instance, global_transform):
	# free existing stuff
	release();
	
	var mesh_instance_clone = mesh_instance.duplicate();

	spatial_node = Spatial.new();
	spatial_node.global_transform = global_transform;
	spatial_node.add_child(mesh_instance_clone);
	Util.set_override_material_on_children(spatial_node, outline_material);

	world.master_player.get_selection_effect_mgr().add(spatial_node);
	spatial_node.set_visible(false);
	return;
	
	
func release():
	# free existing stuff
	if (spatial_node):
		if (world.master_player):
			world.master_player.get_selection_effect_mgr().remove(spatial_node);
			
		spatial_node.queue_free();
		
	return;


func set_global_transform(xform):
	spatial_node.global_transform = xform;
	return;
	
	
func apply_to(node, enabled):
	if (!spatial_node):
		return;
		
	spatial_node.set_visible(enabled);
	return;
