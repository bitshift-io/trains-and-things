#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This class is attached to the player controller
# it is passed available to any states where the player can build something
# so this class setup the render states (material) for rendering of the models
# when in "blueprint" mode, that is the player is planning where to build.
#
# So far this is used in the BuildState.gd and Track building state (CurveMesh.gd)
#

extends Node

export(Material) var blueprint_material = preload("res://Materials/M-Blueprint.tres");

var good_material;
var bad_material;

export(Color) var good_colour = Color(0.29, 0.36, 0.75, 0.8);
export(Color) var bad_colour = Color(0.75, 0.29, 0.31, 0.8);

enum Type {
	TYPE_GOOD,
	TYPE_BAD
	};

func _ready():
	good_material = blueprint_material.duplicate();
	good_material.albedo_color = good_colour;
	
	bad_material = blueprint_material.duplicate();
	bad_material.albedo_color = bad_colour;
	return;
	
func bool_to_type(boolean):
	return Type.TYPE_GOOD if boolean else Type.TYPE_BAD;

func get_material_from_bool(boolean):
	return get_material(bool_to_type(boolean));
	
func get_material(type):
	if (type == Type.TYPE_GOOD):
		return good_material;
	else:
		return bad_material;
		
func set_material_override(node, material):
	var mesh_instances = BUtil.find_children_by_class_name(node, "MeshInstance");
	for mesh_instance in mesh_instances:
		set_mesh_instance_material(mesh_instance, material);

	return;
	
func set_mesh_instance_material(mesh_instance, material):
	var mesh = mesh_instance.get_mesh();
	if (mesh):
		for surf_idx in range(0, mesh.get_surface_count()):
			mesh_instance.set_surface_material(surf_idx, material);
			
	return;
