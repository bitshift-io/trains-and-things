#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# A helper class to help us not need to use _input or _unhandled_input
# in our code
extends Node

# A note on godot _input and _unhandled_input
# first godot calls _input to see if input is handled by a specific class
# if nothing consumes the event, it gets passed to _unhandled_input
# when a text field has focus for example, the event is still passed to _input
# to allow intercepting it before the text field, so most often
# if you dont want to steal focus, use _unhandled_input by supplying HM_UNHANDLED
#
# It seems mouse events ignore this mechanic and require HM_HANDLED
enum HandledMode {
	HM_HANDLED,
	HM_UNHANDLED,
	HM_ALL
};


class Action:
	
	# basically just pressed or just released
	signal is_pressed(p_event);
	signal is_released(p_event);
	
	var name;
	var handled_mode = HandledMode.HM_ALL;
	
	func _init():
		return;
		
		
	func input(event):
		# disabled while trying to get viewports working
		if (handled_mode == HandledMode.HM_ALL || handled_mode == HandledMode.HM_HANDLED):
			_process_input(event);
			
		return;
		
		
	func _process_input(event):
		if (event.is_action_pressed(name)):
			emit_signal("is_pressed", event);
			
		if (event.is_action_released(name)):
			emit_signal("is_released", event);
			
		return;
		
		
	func unhandled_input(event):
		# disabled while trying to get viewports working
		if (handled_mode == HandledMode.HM_ALL || handled_mode == HandledMode.HM_UNHANDLED):
			_process_input(event);
			
		return;


	# key is down
	func is_pressed():
		return Input.is_action_pressed(name);


	# key is up
	func is_released():
		return !Input.is_action_pressed(name);


	# key was just pressed
	func is_just_pressed():
		return Input.is_action_just_pressed(name);


	# key was just releeased
	func is_just_released():
		return Input.is_action_just_released(name);

		
# end class Action
	
var actions = {};

func _ready():
	set_process(false);
	set_process_input(true);
	set_process_unhandled_input(true);
	return;

	
func _unhandled_input(event):
	for action_name in actions:
		if (event.is_action(action_name)):
			actions[action_name].unhandled_input(event);
			
	return;
	
	
func _input(event):
	for action_name in actions:
		if (event.is_action(action_name)):
			actions[action_name].input(event);
			
	return;
	
	
func add_action(action_name, handled_mode = HandledMode.HM_ALL):
	var action = Action.new();
	action.name = action_name;
	action.handled_mode = handled_mode;
	actions[action_name] = action;
	return action;
	
	
func add_actions(p_actions, handled_mode = HandledMode.HM_ALL):
	var results = [];
	for action_name in p_actions:
		results.push_back(add_action(action_name, handled_mode));
		
	return results;
		
		
func is_pressed(p_action_name):
	var action = actions[p_action_name];
	return action.is_pressed();
	
	
func is_release(p_action_name):
	var action = actions[p_action_name];
	return action.is_release();
	
	
func is_just_pressed(p_action_name):
	var action = actions[p_action_name];
	return action.is_just_pressed();


func is_just_released(p_action_name):
	var action = actions[p_action_name];
	return action.is_just_released();
	
	
func is_enabled():
	return is_processing_input();
	
	
func set_enabled(enable):
	set_process_input(enable);
	set_process_unhandled_input(enable);
