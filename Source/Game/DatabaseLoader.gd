#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Takes a list of paths (yml files) and loads them into the database
# Maps should use this to load thier resource etc....
#
extends Node

export(Array, String, FILE, "*.yml") var database_paths setget _set_database_paths; # path to yaml file to inject into the game database

func _set_database_paths(value):
	database_paths = value;
	# do it here is the first opportunity and it works!
	_load_database();
	return;
	
func _init():
	# cant do it here as the variables arent yet setup!
	return;

func _ready():
	# cant setup db here, its too late!
	return;
	
func _load_database():
	for path in database_paths:
		BDatabase.load(path);
		
	return;
