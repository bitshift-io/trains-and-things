#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends Node

export(float) var size = 1000.0 # how big we have the map
export(int) var originalSize = 601 # the original texture size

export(String) var filename
export(bool) var saveInScene = false setget set_saveInScene
	
func set_saveInScene(value):
	saveInScene = value
	parseXml()
	
func parseXml():
	remove_children()
	if (!saveInScene):
		return 
		
	print("!!trying to parse xml: " + filename)
	var xml = XMLParser.new()
	var result = xml.open(filename);
	if (result != 0):
		print("Failed to open: " + filename + ", error code: " + str(result))
		return
		
	var result = 0
	while (result == 0):
		result = xml.read()
		print("nn: " + xml.get_node_name())
		if (xml.get_node_type() == xml.NODE_ELEMENT && xml.get_node_name() == "RRTCityTypes"):
			xml.skip_section()
		if (xml.get_node_type() == xml.NODE_ELEMENT && xml.get_node_name() == "RRTModels"):
			xml.skip_section()
		elif (xml.get_node_type() == xml.NODE_ELEMENT && xml.get_node_name() == "City"):
			print("city found")
			parseCity(xml)
			#return
		
		
	print("DONE: " + str(result))
	

func remove_children():
	for c in get_children():
		c.free()
			
func parseCity(xml):
	var szName
	var startX
	var startY
	var rotation
	var result = 0
	while (result == 0):
		result = xml.read()
		if (xml.get_node_type() == xml.NODE_ELEMENT_END && xml.get_node_name() == "City"):
			print("end of city found, returning")
			createCity({
				szName = szName,
				pos = convertCoords(startX, startY),
				rotation = rotation
			})
			return
			
		elif (xml.get_node_name() == "szName"):
			xml.read()
			szName = xml.get_node_data()
			print("szName found: " + szName)
			xml.read()
		elif (xml.get_node_name() == "Rotation"):
			xml.read()
			print("Rotation found: " + str(xml.get_node_type()))
			rotation = int(xml.get_node_data())
			xml.read()
		elif (xml.get_node_name() == "StartX"):
			xml.read()
			print("startX found")
			startX = int(xml.get_node_data())
			xml.read()
		elif (xml.get_node_name() == "StartY"):
			xml.read()
			print("startY found")
			startY = int(xml.get_node_data())
			xml.read()
		elif (xml.get_node_name() == "CitySongs"):
			xml.skip_section()
	

# save in scene 
func setOwnerAsScene(node):
	if get_tree() != null and get_tree().is_editor_hint() and saveInScene:
		node.set_owner(get_tree().get_edited_scene_root())

				
func createCity(data):
	var city = load("res://ResourceNodes/City.tscn").instance()
	city.set_name(data["szName"])
	city.name = data["szName"]
	
	var xform = city.get_transform()
	xform.basis = xform.basis.rotated(Vector3(0, 1, 0), data["rotation"])
	xform.origin = data["pos"]
	city.set_transform(xform)

#	# remove, add, owner - in this order only!
	remove_child(city) # remove child first!
	add_child(city) 
	setOwnerAsScene(city) # crashy if doing stuff before this step
	pass
	
func convertCoords(x, y):
	
	var rrMapSizeMap = {601 : 100, 661 : 121, 721 : 144, 781 : 169, 841 : 196, 901 : 225}
	var rrHalfMapSize = rrMapSizeMap[originalSize] * 100
	var rrOrigin = Vector3(-7680, 0, -7680)
	var rrOriginPct = rrOrigin / rrHalfMapSize
	var rrOrginInTex = rrOriginPct * (originalSize / 2)
	
	var rrCoord = Vector3(x, 0, y) / (originalSize / 2)
	var rrCoordPct = rrCoord + rrOriginPct
	
	var coord = rrCoordPct * (size / 2)
	#coord.z = -coord.z # flip z-coordinate (which is y in railroads)
	
	# now for my magic hacks....
	coord += Vector3(150, 0, 150)
	coord *= 0.9
	
	if (x == -42):
		print("rrOriginPct:" + str(rrOriginPct) + " rrOrginInTex:" + str(rrOrginInTex) + " - rrCoord:" + str(rrCoord) + " rrCoordPct:" + str(rrCoordPct) + " coord:" + str(coord))
	return coord
	
