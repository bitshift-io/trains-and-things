#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Experience system handles experience and levelling
#

extends Node
#tool

signal level_changed(xp);
signal xp_changed(xp);

#export(int) var level_count = 0

# https://github.com/godotengine/godot/issues/3586
export(Array, int) var levels;

export(int) var min_experience = 0;
export(int) var max_experience = 0; # zero = no xp limit

#var levels = [];

var xp : int = 0;
var level : int = 1;
var prev_level : int = 1;

#func _resize_levels():
#	# resize levels to match level count
#	while (levels.size() < level_count):
#		print("expanding levels")
#		levels.append([0, 0]);
#
#	while (levels.size() > level_count):
#		print("shrinking levels")
#		levels.pop_back();
#
#	return;
#
#func _set(property, value):
#	print("_set: ", property, " val: ", value)
#
#	_resize_levels();
#
#	var splits = property.split(" ");
#	var level_name = splits[0].strip_edges();
#	var level_number = int(level_name.right(5)); #"Level";
#	var index = int(0.0 if property.ends_with("Start Xp") else 1.0);
#	print("levels: level_number:", level_number, " idx: ", index);
#	var arr = levels[level_number];
#	arr[index] = value;
#
#	# ensure the previous/next levels match
#	if (index == 0):
#		levels[level_number - 1][1] = value;
#	else:
#		levels[level_number + 1][0] = value;
#
#	#._set(property, value)
#
#func _get(property):
#	print("_get:", property)
#
#	if (property.begins_with("Level")):
#		_resize_levels();
#
#		var splits = property.split(" ");
#		var level_name = splits[0].strip_edges();
#		var level_number = int(level_name.right(5)); #"Level";
#		var index = 0 if property.ends_with("Start Xp") else 1;
#		print("levels: level_number:", level_number, " idx: ", index);
#		print("value: ", levels[level_number][index]);
#		return levels[level_number][index];
#
#	#return _get(property)
#
#func _get_property_list():
#	var list = [];
#	var property_info;
#
#	for i in range(0, level_count):
#		property_info = {
#		    "name": "Level" + str(i + 1) + " - Start Xp",
#		    "type": TYPE_INT
#		}
#		list.append(property_info);
#
#		property_info = {
#		    "name": "Level" + str(i + 1) + " - End Xp",
#		    "type": TYPE_INT
#		}
#		list.append(property_info);
#
#	return list;

func _ready():
	return;

# add experience
func add_xp(p_amount : int):
	Util.assert(get_tree().is_network_server(), "add_xp only to be called on the server");
	
	var old_xp = xp;

	xp += p_amount;

	# clamp the xp value
	if (max_experience > 0):
		xp = int(min(xp, max_experience));

	xp = int(max(xp, min_experience));

	if (old_xp != xp):
		rpc("_set_xp_rpc", xp);

	# update the level
	for i in range(get_min_level(), get_max_level() + 1):
		var xp_range = get_level_xp_range(i);
		if (xp >= xp_range[0] && xp < xp_range[1]):
			_set_level(i);
			break;

	return;
	
remotesync func _set_xp_rpc(p_xp):
	xp = p_xp;
	emit_signal("xp_changed", self);
	
func _set_level(p_level):
	if (p_level == level):
		return;

	rpc("_set_level_rpc", p_level);

remotesync func _set_level_rpc(p_level):
	prev_level = level;
	level = p_level;
	emit_signal("level_changed", self);

func get_level():
	return level;

func get_previous_level():
	return prev_level;

func get_min_level():
	return 1;

func get_max_level():
	return levels.size() + 1;

func get_xp():
	return xp;

# given a level return the XP range of that level
func get_level_xp_range(p_level):
	var start = min_experience;
	if (p_level > 1): 
		start = levels[p_level - 2];

	var end = max_experience;
	if ((p_level - 1) < levels.size()):
		end = levels[p_level - 1];

	return [start, end];
