#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Game handles basically the core spine of the game
# it handles:
# - joining clients
# - transition between game and menu
# - starting and ending games
# - storing/instancing of clients/peers -> player
#

extends Node

enum NetworkState {
	NONE,
	SERVER,
	CLIENT
};

var network_state = NetworkState.NONE;

enum GameState {
	NONE,
	LOADING,
	INGAME,
};

var game_state = GameState.NONE;

#var cl_chatting = false

# Default game port
const DEFAULT_PORT = 34197 # 10567 hijack the factorio port for now

const CLIENT_CONNECT_TIMEOUT = 10;

# Max number of players
const MAX_PEERS = 64

# Names for remote players in id:name format
# this should just be a dictionary for each peer
var clients = {};
var clients_ready = []

var server_id = null;
var password;

# Signals to let lobby GUI know what's going on
signal register_client(p_client)
signal unregister_client(p_client)
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)
signal game_loading();
signal game_started()
signal player_created(player);


func set_password(p_password):
	password = p_password;
	

# Callback from SceneTree
func _peer_connected(id):
	var host = get_tree().get_network_peer();
	host.set_peer_timeout(id, 32000, 50000, 3000000);
	if (get_tree().is_network_server()):
		Util.create_timer(self, "_check_if_client_connected", CLIENT_CONNECT_TIMEOUT, true, true, [id]);
		
	return;


# Callback from SceneTree
func _peer_disconnected(id):
	if (get_tree().is_network_server()):
		if (WorldMgr.worlds.size() > 0): # Game is in progress
			BLog.debug("Client " + clients[id] + " disconnected");
		else: # Game is not in progress
			# If we are the server, send to the new dude all the already registered players
			unregister_client(id)
			for p_id in clients:
				# Erase in the server
				rpc_id(p_id, "unregister_client", id)


# here we check if a client successfully called register_client
# if not, boot them!
func _check_if_client_connected(p_id):
	if (!clients.has(p_id)):
		rpc_id(p_id, "disconnect_by_server", "Failed to register with the server");
		get_tree().get_network_peer().disconnect_peer(p_id);
		
	return;
	
	
func _get_registration_dict():
	return {
		"id": get_tree().get_network_unique_id(), 
		"name": Database.get_player_name(), 
		"app_name": BDatabase.get_value("application/config/name", ""), 
		"app_version": BDatabase.get_value("application/version_major", "") + BDatabase.get_value("application/build_number", ""),
		"password": password
	};
	
	
# Callback from SceneTree, only for clients (not server)
func _connected_ok():
	server_id = null;
	var network_peers = get_tree().get_network_connected_peers();
	for npeer in network_peers:
		if (npeer != get_tree().get_network_unique_id()):
			server_id = npeer;
	
	var peer = get_tree().get_network_peer();
	peer.set_peer_timeout(server_id, 32000, 50000, 3000000);
	
	# Registration of a client beings here, tell everyone that we are here
	rpc_id(server_id, "register_client_with_server", _get_registration_dict());
	
	emit_signal("connection_succeeded")


# Callback from SceneTree, only for clients (not server)
func _server_disconnected():
	network_state = NetworkState.NONE;
	emit_signal("game_error", "Server disconnected")
	end_game()


# Callback from SceneTree, only for clients (not server)
func _connected_fail():
	network_state = NetworkState.NONE;
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")


remote func disconnect_by_server(reason):
	network_state = NetworkState.NONE;
	emit_signal("game_error", reason);
	end_game();
	
# Lobby management functions

remote func register_client_with_server(p_dict):
	assert(get_tree().is_network_server());
	
	# ops, client has a different version, kick them!
	var server_dict = _get_registration_dict();
	if (!p_dict || typeof(p_dict) != TYPE_DICTIONARY || !p_dict.has("app_name") || p_dict.app_name != server_dict.app_name || p_dict.app_version != server_dict.app_version):
		rpc_id(p_dict.id, "disconnect_by_server", "Trying to join a game of " + server_dict.app_name + " but you are playing " + p_dict.app_name);
		get_tree().get_network_peer().disconnect_peer(p_dict.id);
		return;
		
	# check for correct password - if we have a password
	if (password && password.length() > 0):
		var client_password = p_dict.password if p_dict.has("password") else "";
		if (client_password != password):
			rpc_id(p_dict.id, "disconnect_by_server", "Invalid password");
			get_tree().get_network_peer().disconnect_peer(p_dict.id);
			return;
	
	# tell the connecting player about the other players
	for id in clients:
		rpc_id(p_dict.id, "register_client", clients[id])
		
	# add the new player
	register_client(p_dict);
	
	# let everyone know about the new player
	for p_id in clients: # Then, for each remote player
		rpc_id(p_id, "register_client", p_dict)

	return;
	
	
# append/update info about connected peers/clients
remote func register_client(p_dict):
	var id = p_dict.id;
	var client_name = p_dict.name if p_dict.has("name") else "Rookie";
	clients[id] = p_dict;
	BLog.debug("Client " + client_name + " registered");
	emit_signal("register_client", p_dict);
	

remote func unregister_client(id):
	var dict = clients[id] if clients.has(id) else "???";
	clients.erase(id)
	emit_signal("unregister_client", dict);
	

remote func pre_start_game(p_game_state):
	WorldMgr.connect("world_instanced", self, "_on_world_instanced");
	WorldMgr.connect("world_loaded", self, "_on_world_loaded");
	
	BDatabase.insert(p_game_state); # insert all game state into the database
	
	
	game_state = GameState.LOADING;
	emit_signal("game_loading");
	
	WorldMgr.load_world(p_game_state.map);
	# wait for _on_world_loaded to be called...
	return;
	
	
func _on_world_instanced(world):
	var playersNode = world.players;
	
	PluginMgr.instance_mods();
	
	var networkId = get_tree().get_network_unique_id()		
	var idx = 0
	
	# setup default player if none supplied
	if (clients.size() <= 0):
		register_client(_get_registration_dict()); # register ourself as a player
		
	# create master player first so that world.master_player works
	for idx in range(0, clients.size()):
		# TODO: just pass in the client dictionary into the player controller (which we can now rename to just player)
		var netId = clients.keys()[idx];
		var name = clients.values()[idx].name;
		var is_master = (netId == networkId);
		if (is_master):
			world.master_player = create_player(netId, name, playersNode);
		
	# create non-master players	
	for idx in range(0, clients.size()):
		# TODO: just pass in the client dictionary into the player controller (which we can now rename to just player)
		var netId = clients.keys()[idx]
		var name = clients.values()[idx].name;
		var is_master = (netId == networkId);
		if (!is_master):
			create_player(netId, name, playersNode);
	
	
func _on_world_loaded(world):
	WorldMgr.disconnect("world_instanced", self, "_on_world_instanced");
	WorldMgr.disconnect("world_loaded", self, "_on_world_loaded");
	
	if (not get_tree().is_network_server()):
		# Tell server we are ready to start
		rpc_id(1, "ready_to_start", get_tree().get_network_unique_id())
	elif clients.size() == 1:
		post_start_game()
		
	# game startup commands to help development process
	var game_started_commands = BDatabase.get_value("dev/game_started_cmd", "");
	Console.handle_command(game_started_commands);
	
	game_state = GameState.INGAME;
	emit_signal("game_started")
	
	
func create_player(netId, name, playersNode):
	# load the player data from the database to allow mods to modify this!
	var player_data = BDatabase.get_value("player");
	var player = load(player_data.scene).instance();
	
	player.set_name("Player_%d" % netId)
	player.set_network_master(netId);
	
	var networkId = get_tree().get_network_unique_id()
	var is_master = (netId == networkId);

	player._pre_ready(name, netId, is_master);
	playersNode.add_child(player)
	
	emit_signal("player_created", player);
	return player;


remote func post_start_game():
	get_tree().set_pause(false) # Unpause and unleash the game!


remote func ready_to_start(id):
	Util.assert(get_tree().is_network_server(), "ready_to_start should only be called on the server");

	if (not id in clients_ready):
		clients_ready.append(id)

	if (clients_ready.size() == clients.size()):
		for p in clients:
			rpc_id(p, "post_start_game")
		post_start_game()


func set_host_server(p_host):	
	Util.break_on_assert = !p_host; # disable asserts breaking in MP games
	
	if (p_host):
		if (network_state != NetworkState.NONE):
			return;
		
		network_state = NetworkState.SERVER;
		var host = NetworkedMultiplayerENet.new();
		var err = host.create_server(DEFAULT_PORT, MAX_PEERS);
		if (err != OK):
			emit_signal("game_error", "Failed to host")
			return false;
			
		get_tree().set_network_peer(host);
		register_client(_get_registration_dict()); # register ourself as a client
	else:
		network_state = NetworkState.NONE;
		clients.clear();
		get_tree().set_network_peer(null) # End networking

	return true;


func join_game(ip):
	if (network_state != NetworkState.NONE):
		return;
		
	network_state = NetworkState.CLIENT;
	Util.break_on_assert = false; # disable asserts breaking in MP games
	
	var host = NetworkedMultiplayerENet.new();
	host.create_client(ip, DEFAULT_PORT);
	get_tree().set_network_peer(host);


func leave_game():
	network_state = NetworkState.NONE;
	clients.clear();
	get_tree().set_network_peer(null) # End networking
	

func get_clients():
	return clients.values()


func begin_game(p_game_state):
	Util.assert(p_game_state.map, "Can't load map null");
	
	# Call to pre-start game on all clients except server
	for p in clients:
		if (p != 1):
			rpc_id(p, "pre_start_game", p_game_state)

	pre_start_game(p_game_state)
	

func end_game():
	game_state = GameState.NONE;
	emit_signal("game_ended");
	
	PluginMgr.destroy_mods();
	
	# clean up players
	for world in WorldMgr.worlds:
		for p in world.players.get_children():
			p.end_game();
		
	# destroy worlds
	WorldMgr.unload_worlds();

	#controllers.clear();
	set_host_server(false); # TODO: we just go back to the lobby?!
	
	# reload the game database (ready for next game)
	Database.reload();
	return;


func _ready():
	get_tree().connect("network_peer_connected", self, "_peer_connected")
	get_tree().connect("network_peer_disconnected", self,"_peer_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	call_deferred("_post_ready");
	return;
	
	
func _post_ready():
	# set menu visible if there is no world loaded
	var loading_world = WorldMgr.is_loading;
	Menu.set_visible(!loading_world);

