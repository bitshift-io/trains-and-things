#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

# This script if ised on spawned effects that need to self kill after a time

export(float) var life_time;

var timer;

func _ready():
	timer = Util.create_timer(self, "_timeout", life_time, true)
	return;
	
func _timeout():
	queue_free();
