#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

# This script if ised on spawned effects that need to self kill after a time

export(float) var life_time;
var timer;
var node; # animation player node

func _ready():
	node = BUtil.find_first_child_by_class_name(self, "AnimationPlayer");
	if (node == null):
		return;
		
	node.connect("animation_finished", self, "_animation_finished");
	
	if node.has_animation("End"):
		life_time -= node.get_animation("End").length * node.playback_speed;
		
	if node.has_animation("Start"):
		node.play("Start");
		
	if node.has_animation("Idle"):
		node.queue("Idle");
		
	timer = Util.create_timer(self, "_timeout", life_time, true)		
	return;


func _animation_finished(p_value):
	if p_value == "End":
		_destroy();
	return;


func _timeout():
	if node.has_animation("End"):
		node.play("End", 0.2);
	else:
		_destroy();
	return;


func _destroy():
	queue_free();
	return;