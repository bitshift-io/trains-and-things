#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node
tool

export(PackedScene) var meshScene setget setMeshScene # the scene/mesh of our tree
export(bool) var enabledInEditor = true setget setEditorEnable
export(bool) var enabledInGame = true

var rotationVariation = 8 # degrees
var scaleVariation = 0.2 # multiplier 
var sway = 2
var nodeName = "" # name of node

func _set(property, value):
	if (property == "node_name"):
		freeNodes()
		nodeName = value
		update()
		
	#._set(property, value)

func _get(property):
	if (property == "node_name"):
		return nodeName
	
	#return _get(property)

func _get_property_list():
	
	var listStr = ""
	if (get_parent()):
		var parentChildren = get_parent().get_children()
		for pc in parentChildren:
			if (pc.get_children().size() && pc.get_class() == "Spatial"):
				listStr += "%s," % pc.get_name()

	var property_info = {
		"name": "node_name",
		"type": TYPE_STRING,
		"hint": PROPERTY_HINT_ENUM,
		"hint_string": listStr
	}
	return [property_info]


func _ready():
	update()

func update():
	if (!is_inside_tree()):
		return
		
	# if in editor
	if (Engine.is_editor_hint() == true):
		if enabledInEditor == true:
			instanceNodes()
		else:
			freeNodes()
	# else in game
	else:
		if enabledInGame == true:
			instanceNodes()
		else:
			freeNodes()


func setEditorEnable(value):
	freeNodes()
	enabledInEditor = value
	update()

func setMeshScene(value):
	# free old first
	freeNodes()
		
	# set
	meshScene = value
	
	update()
#
func instanceNodes():
	if (!nodeName):
		return
		
	var meshNode = get_parent().get_node(nodeName)		
	for n in meshNode.get_children():
		var Obj = meshScene.instance()
		n.add_child(Obj)
		var randScale = Vector3 (1,1,1) * float (rand_range(1-scaleVariation, 1+scaleVariation))
		n.set_scale(randScale)
		var randRot = Vector3 (rand_range(-rotationVariation, rotationVariation), rand_range(0, 360), rand_range(-rotationVariation, rotationVariation))
		n.set_rotation_deg (randRot)

func freeNodes():
	if (!nodeName):
		return
		
	var meshNode = get_parent().get_node(nodeName)		
	for n in meshNode.get_children():
		for s in n.get_children():
			s.free()


