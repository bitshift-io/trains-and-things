#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Particles

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Center our particles on our cameras position
	var viewport = get_viewport();
	var camera = viewport.get_camera();
	
	var pos = camera.global_transform.origin;
	pos.y = 0.0;
	global_transform.origin = pos;
	
	
	# ground collision
	var col = BUtil.get_terrain().raycast_down(pos);
		
	if (!col.empty()):
		var pos2 = col["position"];
		global_transform.origin = pos2;

	return;
