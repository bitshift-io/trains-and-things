#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends Path

export(String, FILE, "*.json") var curve_file = null;
export(bool) var import = false setget set_import;


func _set(property, value):
	print("_set: ", property, " val: ", value);
	return;


func set_import(value):
	if !Engine.is_editor_hint():
		return;
		
	import = false;
	create_curve();
	return;


func _ready():
	create_curve();
	return;


func create_curve():
	if (curve_file == null):
		print("no curve file selected");
		return;
		
	# read the file
	var data_file = File.new();
	if data_file.open(curve_file, File.READ) != OK:
		print("file not found");
		return;
	
	# create new curve
	var c = Curve3D.new();
	curve = c;
	curve.clear_points();
	
	# parse the curve json data
	var data_text = data_file.get_as_text();
	data_file.close();
	var data_parse = JSON.parse(data_text);
	if data_parse.error != OK:
		print(data_parse.error);
		print(data_parse.error_line);
		print(data_parse.error_string);
		return;
		
	var data = data_parse.result;
	
	# debug
	#print(data);
	
	# assign the data to godot
	var curve_array = data['curve'];
	
	var is_cyclic = data['cyclic'];
	if is_cyclic:
		curve_array.append(curve_array[0]);
	
	# swap y > z coord
	# mirror y coord
	for curve_data in curve_array:
		var left = Vector3(curve_data[0][0], curve_data[0][2], -curve_data[0][1]);
		var position = Vector3(curve_data[1][0], curve_data[1][2], -curve_data[1][1]);
		var right = Vector3(curve_data[2][0], curve_data[2][2], -curve_data[2][1]);
		curve.add_point(position, left, right);
	return;
