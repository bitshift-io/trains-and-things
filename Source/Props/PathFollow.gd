#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends PathFollow

export(float) var speed = 10

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	set_process(true);

func _process(delta):
	set_offset(get_offset() + delta * speed);
	return;
