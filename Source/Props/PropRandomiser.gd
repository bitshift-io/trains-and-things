#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

export(bool) var enabled = false;
export(int) var random_seed = 3455677;

func _ready():
	if !enabled:
		return;
		
	# store list of children
	var children = self.get_children();
	
	# get transform list
	var xform_list = [];
	for child in children:
		xform_list.append(child.get_global_transform())
	
	# shuffle
	seed(random_seed) # for default generator (not get)
	xform_list.shuffle();
	
	# switch positions
	var i = 0;
	for child in children:
		child.set_global_transform(xform_list[i]);
		i += 1;
	
	return;