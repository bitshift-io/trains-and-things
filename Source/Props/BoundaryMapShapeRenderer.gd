#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

export(float) var radius = 10;

func _ready():
	# if any parent is hidden, then we are hidden
	var vis = is_visible();
	var p = get_parent();
	while (p):
		if (p.has_method("is_visible") && !p.is_visible()):
			vis = false;
			break;
			
		p = p.get_parent();
		
	if (!vis):
		return;
		
	var boundary_maps = get_tree().get_nodes_in_group("boundary_maps");
	var boundary_map = boundary_maps[0];
	var xform = get_global_transform();
	boundary_map.render_circle(xform.origin, radius, BBoundaryMap.OUT_OF_BOUNDS);
	return;
