#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

export(bool) var enabled = true
export(bool) var recursive = true
export(float) var heightOffset = 0.0


func _ready():
	if (enabled):
		set_physics_process(true);
	
	
func _physics_process(delta):
	set_physics_process(false) # disable, only need to do that once for physics

	var spaceState = get_parent().get_world().get_direct_space_state()
	var exclusions = BUtil.find_children_by_class_name(get_parent(), "CollisionShape");
	
	# ray cast and move
	var node = get_parent()
	moveNode(node, spaceState, exclusions)
	moveChildren(node, spaceState, exclusions)
	
	
func moveChildren(node, spaceState, exclusions):
	for c in node.get_children():
		moveNode(c, spaceState, exclusions)

		if recursive:
			moveChildren(c, spaceState, exclusions)


func moveNode(c, spaceState, exclusions):
	if !ClassDB.is_parent_class(c.get_class(), "Spatial"):
		return;
		
	if (!BUtil.get_terrain()):
		return;
	
	if (c.has_method("get_transformed_aabb")):
		cast_aabb(c, spaceState, exclusions);
	else:
		cast_centre(c, spaceState, exclusions);
		
		
# a bit more elaborate, measure the 4 AABB ground points and use the average
func cast_aabb(c, spaceState, exclusions):
	var aabb = c.get_transformed_aabb();
	var gxform = c.get_global_transform();
	var xform = c.get_transform()
	
	var point_indexes = [0, 1, 4, 5];
	var dist_moved = 0;
	var col_count = 0;
	for pi in point_indexes:
		var pt: Vector3 = aabb.get_endpoint(pi);
		var col = BUtil.get_terrain().raycast_down(pt);
		if (!col.empty()):
			var pos = col["position"];
			var dist = pos.y - pt.y;
			dist_moved += dist;
			col_count += 1;
			
	var average_dist_moved = (dist_moved / col_count);
	xform.origin += Vector3(0.0, average_dist_moved, 0.0);
	xform.origin += Vector3(0.0, heightOffset, 0.0);
	c.set_transform(xform);
	
	
# just a simple cast the centre point
func cast_centre(c, spaceState, exclusions):
	var gxform = c.get_global_transform()
	var col = BUtil.get_terrain().raycast_down(gxform.origin);

	if (!col.empty()):
		var pos = col["position"]
		var distMoved = pos - gxform.origin
		var xform = c.get_transform()
		xform.origin += distMoved + Vector3(0.0, heightOffset, 0.0)
		c.set_transform(xform)
