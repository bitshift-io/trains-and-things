#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends AnimationPlayer

export(bool) var autplay_idle = true;
export(bool) var random_animation_offset = true;
export(bool) var play_random_animation = false;

export(NodePath) var destination_node;
export(bool) var apply_destination_node = false setget _apply_func;


func _apply_func(value):
	var anim_player : AnimationPlayer = self;
	var anim_list = anim_player.get_animation_list();
	for i in range(0, anim_list.size()):
		var animation = anim_player.get_animation(anim_list[i]);
		_modify_animation(animation);
		
	return;



# transfer animation to target/destination
func _modify_animation(animation : Animation):
	var root_node = self.get_root();
	var root = get_node(root_node);

	for i in range(0, animation.get_track_count()):
		var path : NodePath = animation.track_get_path(i);
		var colon_split = str(path).split(":");
		var path_from_root = colon_split[0];
		colon_split.remove(0);
		var property = colon_split.join(":");
		var new_path = str(destination_node) + ":" + property;
		new_path = new_path.replace(str(root_node) + "/", "");
		animation.track_set_path(i, new_path);
		var new_node = get_node(destination_node);
		print(path, " changed to ", new_path);
	return;


func _ready():
	if autplay_idle && self.has_animation("Idle"):
		var anim = self.get_animation("Idle");
		anim.set_loop(true);
		self.play("Idle");
		#self.set_autoplay("Idle");
		
	if random_animation_offset:
		var rand = rand_range(0,1);
		self.advance(rand);
		self.seek(rand, true);		
	
func _process(delta):
	
	return;	

