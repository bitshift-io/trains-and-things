#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends Spatial

# height map stuff
export(ImageTexture) var heightmap setget set_heightmap
export(Material) var material setget set_material
export(int, 1, 2000) var size = 1000 setget set_size
export(float, 0.1, 100, 0.1) var height = 50 setget set_height
export(int, 1, 1000) var resolution = 60 setget set_resolution
export(int, 1, 100) var chunks = 5 setget set_chunks
export(bool) var castShadows = false setget set_castShadows
export(bool) var generateCollision = true setget set_collision
export(bool) var generateLOD = false setget set_lod
export(bool) var saveInScene = false setget set_saveInScene

# need to export some kind of dictionary, so a color value, with a mesh(scene) as a pair


# clutter map stuffs
export(ImageTexture) var cluttermap setget set_cluttermap

var mesh_builder

func _init():
	update_heightmap()
	update_cluttermap()

	
func remove_children():
	for c in get_children():
		c.free()
	
func update_heightmap():
	remove_children()

	if heightmap == null:
		return

	# generate a mesh, LOD(mesh instance) then collision
	create_LOD("LOD0", 0, 500, resolution)

	# LOD
	if generateLOD:
		create_LOD("LOD1", 500, 1000, resolution/2.0)
		create_LOD("LOD2", 1000, 2000, resolution/4.0)
	
	update_heightmap_properties()


# updates things such as materials, lod ranges that dont effect the mesh
func update_heightmap_properties(node=self):
	for c in node.get_children():
		update_heightmap_properties(c)
		if c.get_type() == "MeshInstance":
			c.set_material_override(material)
			c.set_cast_shadows_setting(int(castShadows))



# save in scene 
func setOwnerAsScene(node):
	if get_tree() != null and get_tree().is_editor_hint() and saveInScene:
		node.set_owner(get_tree().get_edited_scene_root())


# create LOD
func create_LOD(name, rangeBegin, rangeEnd, res):
	# add LOD group to root scene
	var newSpatial = Spatial.new()
	newSpatial.set_name(name)
	
	# remove, add, owner - in this order only!
	#remove_child(newSpatial) # remove child first!
	add_child(newSpatial) 
	setOwnerAsScene(newSpatial) # crashy if doing stuff before this step
	
	# generate chunks & collisions for chunks
	for x in range(1):
		for y in range(1):
			
			# add chunk
			var newInstance = MeshInstance.new() 
			newInstance.set_name("chunk")
			
			# setup parenting
			newSpatial.add_child(newInstance)
			setOwnerAsScene(newInstance) # crashy if doing stuff before this step
			
			# old moved, to update properties
			#newInstance.set_material_override(material)
			#newInstance.set_cast_shadows_setting(int(castShadows))
			
			# add mesh to chunk
			var newMesh = create_mesh(res, x, y)
			newInstance.set_mesh(newMesh)
			
			if generateLOD:
				newInstance.set_draw_range_begin(rangeBegin)
				newInstance.set_draw_range_end(rangeEnd)
		
			if name == "LOD0":
				create_Collision(newInstance)
				
	return newSpatial


func create_mesh(res, x, y):
	var origin = Vector3(-size/2.0, 0, -size/2.0)
	var res_size = (size/res)
	
	var image
	var w
	var h
	var get_h = false
	
	if heightmap:
		image = heightmap.get_data()
		if image.empty():
			get_h = false
		else:
			get_h = true
			w = image.get_width() - 1 # starts a zero
			h = image.get_height() - 1
			
	var surf = SurfaceTool.new()
	surf.begin(VS.PRIMITIVE_TRIANGLES)
	surf.add_smooth_group(true)
	
	# loop over x,y axis step size by resolution
	for i in range(res): 
		for j in range(res):
			var vertex_height = [0,0,0,0]
			
			if get_h:
				vertex_height[0] = image.get_pixel(w * float(i)/res, h * float(j)/res).r * height
				vertex_height[1] = image.get_pixel(w * float(i+1)/res, h * float(j)/res).r * height
				vertex_height[2] = image.get_pixel(w * float(i+1)/res, h * float(j+1)/res).r * height
				vertex_height[3] = image.get_pixel(w * float(i)/res, h * float(j+1)/res).r * height
				
			# add triangle for first half of quad
			surf.add_uv(Vector2(0 + i, 0 + j)/res)
			surf.add_vertex(Vector3(i * res_size, vertex_height[0], j * res_size) + origin)
			surf.add_uv(Vector2(1 + i, 0 + j)/res)
			surf.add_vertex(Vector3((i+1) * res_size, vertex_height[1], j * res_size) + origin)
			surf.add_uv(Vector2(1 + i, 1 + j)/res)
			surf.add_vertex(Vector3((i+1) * res_size, vertex_height[2], (j+1) * res_size) + origin)
			
			# add triangle for second half of quad
			surf.add_uv(Vector2(0 + i, 0 + j)/res)
			surf.add_vertex(Vector3(i * res_size, vertex_height[0], j * res_size) + origin)
			surf.add_uv(Vector2(1 + i, 1 + j)/res)
			surf.add_vertex(Vector3((i+1) * res_size, vertex_height[2], (j+1) * res_size) + origin)
			surf.add_uv(Vector2(0 + i, 1 + j)/res)
			surf.add_vertex(Vector3(i * res_size, vertex_height[3], (j+1) * res_size) + origin)
			
	surf.generate_normals()
	surf.index()
	var mesh = surf.commit()
	surf.clear()
		
	return mesh


# create Collision
func create_Collision(meshInstance):
	# generate and move the collision so we can toggle LOD terrain on/off
	if generateCollision:
		meshInstance.create_trimesh_collision()
		var newCollision = meshInstance.get_child(0)
#		meshInstance.remove_child(newCollision)
#		add_child(newCollision)
#		if saveInScene:
#			newCollision.set_owner(get_tree().get_edited_scene_root())
#			newCollision.get_child(0).set_owner(get_tree().get_edited_scene_root())
		return newCollision

# update clutter
func update_cluttermap():
	if !heightmap or !cluttermap:
		return
		
	var clutter_img = cluttermap.get_data()
	var height_img = heightmap.get_data()
	
	if clutter_img.empty() or height_img.empty():
		return
	
	# cluttermap to worldspace conversion
	var pixel_w_size = float(size) / (clutter_img.get_width() - 1)
	var pixel_h_size = float(size) / (clutter_img.get_height() - 1)
	
	# origin offset account for half pixel width as we spawn at the center of the pixel
	var origin = Vector3(-size/2.0 + pixel_w_size/2.0, 0, -size/2.0 + pixel_h_size/2.0)
	
	# cluttermap to heightmap conversion
	var height_w_size = height_img.get_width() / clutter_img.get_width()
	var height_h_size = height_img.get_height()  / clutter_img.get_height()
	
	# test dictionary
	var testScene = load("res://Mesh/Flora-Tree-A.scn")
	var testColor = Color(0,(250.0/255.0),0)
	
	for x in range(clutter_img.get_width() - 1):
		for y in range(clutter_img.get_height() - 1):
			var clutter_pixel = clutter_img.get_pixel(x, y)

			if clutter_pixel == testColor:
				# get height
				var height_pixel = height_img.get_pixel(x * height_w_size, y * height_h_size)
				
				# spawn item at the coords
				var new_scene = testScene.instance()
				add_child(new_scene)
				new_scene.set_translation(Vector3((x * pixel_w_size) + origin.x, height_pixel.r * height, (y * pixel_h_size) + origin.z))


#Setter functions
func set_heightmap(value):
	heightmap = value
	update_heightmap()
	
func set_cluttermap(value):
	cluttermap = value
	update_cluttermap()
	
func set_material(value):
	material = value
	update_heightmap_properties()
	
func set_castShadows(value):
	castShadows = value
	update_heightmap_properties()
	
func set_height(value):
	height = value
	update_heightmap()
	
func set_resolution(value):
	resolution = value
	update_heightmap()
	
func set_size(value):
	size = float(value)
	update_heightmap()

func set_chunks(value):
	chunks = value
	update_heightmap()
	
func set_collision(value):
	generateCollision = value
	update_heightmap()
	
func set_lod(value):
	generateLOD = value
	update_heightmap()
	
func set_saveInScene(value):
	saveInScene = value
	update_heightmap()
