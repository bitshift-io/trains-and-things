#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This supersceds AmazingToolExample
#
tool
extends Spatial

var enabled_value = false;

var x_translation_value = null; 
var y_translation_value = null; 
var z_translation_value = null; 

var x_rotation_value = null; 
var y_rotation_value = null; 
var z_rotation_value = null; 

var x_scale_value = null; 
var y_scale_value = null;
var z_scale_value = null;

export(float) var animation_length = 10.0;

var original_transform;

func _create_curve_property(p_name):
	return {
		"name": p_name,
		"type": TYPE_OBJECT,
		"hint": PROPERTY_HINT_RESOURCE_TYPE,
		"hint_string": "CurveTexture"
	};
	
func _create_curve(min_value, max_value, default_value):
	var tex = CurveTexture.new();
	tex.curve = Curve.new();
	tex.curve.min_value = min_value;
	tex.curve.max_value = max_value;
	tex.curve.add_point(Vector2(0, default_value));
	tex.curve.add_point(Vector2(1, default_value));
	return tex;
	
func _set(property, value):
	#print("_set: ", property, " val: ", value)
	
	if property == "enabled":
		if value == false && original_transform != null:
			self.transform = original_transform;
		else:
			original_transform = self.transform;
		enabled_value = value;
			
	match property:
		"x_scale": x_scale_value = value;
		"y_scale": y_scale_value = value;
		"z_scale": z_scale_value = value;
		"x_translation": x_translation_value = value;
		"y_translation": y_translation_value = value;
		"z_translation": z_translation_value = value;
		"x_rotation": x_rotation_value = value;
		"y_rotation": y_rotation_value = value;
		"z_rotation": z_rotation_value = value;
		
	return;

	
func _get(property):
	#print("_get:", property)
		
	match property:
		"x_scale":
			if x_scale_value == null:
				x_scale_value = _create_curve(0, 2, 1);
			return x_scale_value;
		"y_scale":
			if y_scale_value == null:
				y_scale_value = _create_curve(0, 2, 1);
			return y_scale_value;
		"z_scale":
			if z_scale_value == null:
				z_scale_value = _create_curve(0, 2, 1);
			return z_scale_value;
		"x_translation":
			if x_translation_value == null:
				x_translation_value = _create_curve(-4, 4, 0);
			return x_translation_value;
		"y_translation":
			if y_translation_value == null:
				y_translation_value = _create_curve(-4, 4, 0);
			return y_translation_value;
		"z_translation":
			if z_translation_value == null:
				z_translation_value = _create_curve(-4, 4, 0);
			return z_translation_value;
		"x_rotation":
			if x_rotation_value == null:
				x_rotation_value = _create_curve(-180, 180, 0);
			return x_rotation_value;
		"y_rotation":
			if y_rotation_value == null:
				y_rotation_value = _create_curve(-180, 180, 0);
			return y_rotation_value;
		"z_rotation":
			if z_rotation_value == null:
				z_rotation_value = _create_curve(-180, 180, 0);
			return z_rotation_value;
		"enabled":
			return enabled_value;
			
	return null;
	
# add extra properties
func _get_property_list():	
	var property_list = [
		{
			"name": "enabled",
			"type": TYPE_BOOL
			#"hint": PROPERTY_HINT_BOOL ,
			#"hint_string": "CurveTexture"
		},	
		_create_curve_property("x_translation"),
		_create_curve_property("y_translation"),
		_create_curve_property("z_translation"),
		_create_curve_property("x_rotation"),
		_create_curve_property("y_rotation"),
		_create_curve_property("z_rotation"),
		_create_curve_property("x_scale"),
		_create_curve_property("y_scale"),
		_create_curve_property("z_scale")
	]
	return property_list

# Called when the node enters the scene tree for the first time.
func _ready():
	original_transform = self.transform;
	pass; 


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Engine.is_editor_hint() && !enabled_value:
		return;
		
	var ticks = float(OS.get_ticks_msec())  / 1000.0;
	var time = ticks / animation_length;
	var int_speed = int(time);
	var frac_time = time - float(int_speed);
	
	# get transform at start of frame
	var new_transform = original_transform;
	
	# translation
	var x_translation_curve = x_translation_value.get_curve();
	var y_translation_curve = y_translation_value.get_curve();
	var z_translation_curve = z_translation_value.get_curve();
	
	var new_x_translation = x_translation_curve.interpolate_baked(frac_time);
	var new_y_translation = y_translation_curve.interpolate_baked(frac_time);
	var new_z_translation = z_translation_curve.interpolate_baked(frac_time);
	
	var new_translate = Vector3(new_x_translation, new_y_translation, new_z_translation);
	new_transform = new_transform.translated(new_translate);
	
	# rotation
	var x_rotation_curve = x_rotation_value.get_curve();
	var y_rotation_curve = y_rotation_value.get_curve();
	var z_rotation_curve = z_rotation_value.get_curve();
	
	var new_x_rotation = x_rotation_curve.interpolate_baked(frac_time);
	var new_y_rotation = y_rotation_curve.interpolate_baked(frac_time);
	var new_z_rotation = z_rotation_curve.interpolate_baked(frac_time);
	
	new_transform.basis = new_transform.basis.rotated(Vector3(1, 0, 0), deg2rad(new_x_rotation));
	new_transform.basis = new_transform.basis.rotated(Vector3(0, 1, 0), deg2rad(new_y_rotation));
	new_transform.basis = new_transform.basis.rotated(Vector3(0, 0, 1), deg2rad(new_z_rotation));
	
	# scale
	var x_scale_curve = x_scale_value.get_curve();
	var y_scale_curve = y_scale_value.get_curve();
	var z_scale_curve = z_scale_value.get_curve();
	
	var new_x_scale = x_scale_curve.interpolate_baked(frac_time);
	var new_y_scale = y_scale_curve.interpolate_baked(frac_time);
	var new_z_scale = z_scale_curve.interpolate_baked(frac_time);
	
	var new_scale = Vector3(new_x_scale, new_y_scale, new_z_scale);
	new_transform = new_transform.scaled(new_scale);
	
	# set transform
	self.transform = new_transform;
	return;
