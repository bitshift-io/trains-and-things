#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends Spatial

export(Texture) var heightmap_texture setget set_heightmap_texture 
export(bool) var generate_normalmap = false setget set_generate_normalmap

func set_generate_normalmap(value):
	if value == true:
		update_heightmap();
	
func set_heightmap_texture(value):
	heightmap_texture = value
	#update_heightmap()
	
func update_heightmap():
	if heightmap_texture == null:
		return
		
		
	var normal_map = BNormalMap.new();
	normal_map.generate_from_height_map(heightmap_texture, get_height());
	normal_map.save("res://test.png");
#
#	# https://stackoverflow.com/questions/5281261/generating-a-normal-map-from-a-height-map
#	# terrain height
#	var scale = 1; 
#
#	# get image
#	var image = heightmap_texture.get_data();
#	image.lock();
#
#	# create blank normal image
#	var normal_image = Image.new();
#	normal_image.create(image.get_width(),image.get_height(),false,Image.FORMAT_RGB8);
#	normal_image.fill(Color(0.5,0.5,1));
#	normal_image.lock();
#
#	# loop and filter
#	for x in range(1, image.get_width()-1):
#		for y in range(1, image.get_height()-1):
#			normal_image.set_pixel( x, y, vector3_to_color(get_normal_from_pixel(image,x,y,scale)) );
#
#	image.unlock();
#	normal_image.unlock();
#	normal_image.save_png("res://test.png");
#
#
#
#func get_normal_from_pixel(image, x, y, scale):
#	# [6][7][8]
#	# [3][4][5]
#	# [0][1][2]
#	var s = [];
#	s.resize(9);
#	s[0] = image.get_pixel(x-1,y+1).r;
#	s[1] = image.get_pixel(x  ,y+1).r;
#	s[2] = image.get_pixel(x+1,y+1).r;
#	s[3] = image.get_pixel(x-1,y  ).r;
#	#s[4] = image.get_pixel(x  ,y  );
#	s[5] = image.get_pixel(x+1,y  ).r;
#	s[6] = image.get_pixel(x-1,y-1).r;
#	s[7] = image.get_pixel(x  ,y-1).r;
#	s[8] = image.get_pixel(x+1,y-1).r;
#
#	# normal
#	var n = Vector3();
#	n.x = (-(s[2]-s[0]+2.0*(s[5]-s[3])+s[8]-s[6]) * scale) * 0.5 + 0.5;
#	n.y = (-(s[6]-s[0]+2.0*(s[7]-s[1])+s[8]-s[2]) * scale) * 0.5 + 0.5;
#	n.z = 1.0;
#	n = n.normalized();
#	return n;
#
#func color_to_vector3(col):
#	var v = Vector3(col[0],col[1],col[2]);
#	return v;
#
#func vector3_to_color(vec):
#	var c = Color(vec.x,vec.y,vec.z);
#	return c;
#
