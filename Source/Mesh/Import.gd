#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool
extends EditorScenePostImport

# ensure the Materials "Location" is set to "Mesh"
# and that "Storage" is set to "Built-In"

var missing_material_name = "res://Materials/M-Default.tres";
var material_dir = "res://Materials/";


func post_import(scene):
	print("\nImport: %s from %s" % [get_source_file(), get_source_folder()] );
	iterate_scene(scene);
	return scene; # scene contains the imported scene starting from the root node


func iterate_scene(node):
	if (node == null):
		return;
	
	# import animation
	if (node is AnimationPlayer):
		print("AnimationPlayer found:" + node.get_name());
		if node.has_animation("Idle"):
			var anim = node.get_animation("Idle");
			anim.set_loop(true);
			node.set_autoplay("Idle");
			#node.play();
		
	# import material
	if (node is MeshInstance):
		print("Mesh: " + node.get_name());
		var mesh = node.get_mesh();
		var surf_count = mesh.get_surface_count();
		
		for i in range(0, surf_count):
			var mat = mesh.surface_get_material(i);
			# no material assigned
			if (mat == null):
				print("Material is null, assigning default");
				mat = load(missing_material_name);
				mesh.surface_set_material(i, mat);
			else:
				# search for material
				
				# first look in default location
				var mat_path = material_dir + mat.get_name() + ".tres";

				# then try the same folder as the asset
				if (file_exists(mat_path) == false):
					mat_path = get_source_folder() + '/' + mat.get_name() + ".tres";

				if (file_exists(mat_path) == false):
					print("Missing: " + mat.get_name());
				else:
					mat = load(mat_path);
					print("Material: " + mat_path);
					mesh.surface_set_material(i, mat);
					#print("Material:" + mat.resource_name);
			
	for c in node.get_children():
		iterate_scene(c);

	return;


func file_exists(file):
	return File.new().file_exists(file);
