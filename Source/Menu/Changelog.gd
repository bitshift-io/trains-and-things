extends Control

var controller; # this is the main root menu

onready var ui_tab_container = BUtil.find_child(self, "TabContainer");
onready var ui_change_template = BUtil.find_child(self, "ChangeTemplate");


func apply_to_controller(p_controller):
	controller = p_controller;
	return;
	
	
func _ready():
	ui_change_template.get_parent().remove_child(ui_change_template); # remove from parent, its a template

	var changelog = BDatabase.get_value("changelog", null);
	var first = true;
	var i = 0;
	for change in changelog:
		var version_major = change.version_major;
		
		var template = ui_change_template.duplicate();
		var text = BUtil.find_child(template, "RichTextLabel");
		
		text.set_bbcode(get_text_for_change(change));
		
		ui_tab_container.add_child(template);		
		ui_tab_container.set_tab_title(i, str(version_major));
		
		i += 1;


	call_deferred("_post_ready");
	return;
	
	
func _post_ready():
	# emulate button click on this panel to show it
	controller._button_toggled(true, self);
	

func get_alt_color(alt_idx):
	if (alt_idx % 2 == 0):
		return "[color=white]";
	
	return "[color=#d6d6d6]";
	
	
func set_visible(vis):
	.set_visible(vis);

	
func get_text_for_change(change):
	var version_major = change.version_major;
	var version_name = change.version_name;
	var summary = change.summary;
	var alt_idx = 0;
	
	var issues_str = "";
	if (change.has('issues')):
		issues_str = "\n[color=yellow]Known Issues & Workarounds[/color]\n\n";
		for issue in change.issues:
			issues_str += get_alt_color(alt_idx) + "[color=red]+[/color] " + issue + "[/color]\n";
			alt_idx += 1;
			
	var features_str = "";
	if (change.has('features')):
		features_str = "\n[color=yellow]Features added[/color]\n\n";
		for feature in change.features:
			features_str += get_alt_color(alt_idx) + "[color=red]+[/color] " + feature + "[/color]\n";
			alt_idx += 1;
			
	var bugs_str = "";
	if (change.has('fixes')):
		bugs_str = "\n[color=yellow]Bugs fixed[/color]\n\n";
		for bug in change.fixes:
			bugs_str += get_alt_color(alt_idx) + "[color=red]+[/color] " + bug + "[/color]\n";
			alt_idx += 1;

	var change_str = "[color=yellow]" + str(version_major) + " - " + version_name + "[/color]\n\n" + summary + "\n\n" + issues_str + features_str + bugs_str;
	return change_str;
