extends Control

var controller; # this is the main root menu

onready var ui_thumbnail = BUtil.find_child(self, "TextureRect");
onready var ui_text = BUtil.find_child(self, "RichTextLabel");


func apply_to_controller(p_controller):
	controller = p_controller;
	
	
func start_loading():
	var tips = BDatabase.get_value("loading/tips");
	var tip_idx = int(rand_range(0, tips.size() - 1));
	var tip = tips[tip_idx];
	
	var map = BDatabase.get_value("map");
	var map_descriptor = PluginMgr.get_map_descriptor_from_scene_path(map);
	
	var map_description = map_descriptor.get_description();
	ui_text.set_bbcode("[color=yellow]" + tip + "[/color]\n\n\n" + map_description);
	ui_thumbnail.texture = map_descriptor.thumbnail;
	
