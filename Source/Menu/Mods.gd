#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
extends Control

var controller; # this is the main root menu

onready var ui_tab_container = BUtil.find_child(self, "TabContainer");
onready var ui_local_list = BUtil.find_child(self, "LocalList");
onready var ui_modio_list = BUtil.find_child(self, "ModioList");
onready var ui_mod_io_list = BUtil.find_child(self, "ModIoList");
onready var ui_pagination_lbl = BUtil.find_child(self, "PaginationLbl");
onready var ui_search_query = BUtil.find_child(self, "SearchQuery");

onready var search_timer = Util.create_timer(self, "request_mods", 0.2, false, true);

var mod_io_total = 0;
var page_count = 0;
var request: Dictionary = { "page": 0, "query": "" };


func apply_to_controller(p_controller):
	controller = p_controller;
	BModio.connect("on_request_mods", self, "_on_request_mods");
	BModio.connect("on_download_mod", self, "_on_download_mod", [], CONNECT_DEFERRED);
	return;
	
	
func _ready():
	ui_tab_container.set_current_tab(0);
	
	ui_search_query.set_right_icon(get_icon("Search", "EditorIcons"));

	var mod_descriptors = PluginMgr.get_mod_descriptors();
	for mod_descriptor in mod_descriptors:
		# call _ready on the mod
		mod_descriptor.set_menu_controller(controller);
		var ui_item = ui_local_list.create_item();
		ui_item.set_meta("mod_descriptor", mod_descriptor);
		var item_components = get_local_components(ui_item);
		
		item_components.ui_title.set_text(get_title(mod_descriptor));
		item_components.ui_description_lbl.set_bbcode(get_description(mod_descriptor));
		item_components.ui_website_btn.visible = mod_descriptor.website.length();
		item_components.ui_enabled_chk.set_pressed(mod_descriptor.is_enabled());
		
		item_components.ui_delete_btn.set_button_icon(item_components.ui_delete_btn.get_icon("Remove", "EditorIcons"));
		item_components.ui_website_btn.set_button_icon(get_icon("Instance", "EditorIcons"));
		
		item_components.ui_website_btn.connect("pressed", self, "_on_local_website_pressed", [ui_item, mod_descriptor]);
		item_components.ui_delete_btn.connect("pressed", self, "_on_local_delete_pressed", [ui_item, mod_descriptor]);
		item_components.ui_update_btn.connect("pressed", self, "_on_local_update_pressed", [ui_item, mod_descriptor]);
		item_components.ui_enabled_chk.connect("toggled", self, "_on_local_enable_toggled", [ui_item, mod_descriptor]);
		
		item_components.ui_texture_rect.set_texture(mod_descriptor.thumbnail);
	
	return;
	
	
func get_local_components(p_ui_item):
	return {
		"mod_descriptor": p_ui_item.get_meta("mod_descriptor"),
		"ui_title": BUtil.find_child(p_ui_item, "Title"),
		"ui_description_lbl": BUtil.find_child(p_ui_item, "DescriptionLbl"),
		"ui_website_btn": BUtil.find_child(p_ui_item, "WebsiteBtn"),
		"ui_delete_btn": BUtil.find_child(p_ui_item, "DeleteBtn"),
		"ui_update_btn": BUtil.find_child(p_ui_item, "UpdateBtn"),
		"ui_enabled_chk": BUtil.find_child(p_ui_item, "EnabledChk"),
		"ui_texture_rect": BUtil.find_child(p_ui_item, "TextureRect"),
	};
	
	
func get_modio_components(p_ui_item):
	return {
		"mod_descriptor": p_ui_item.get_meta("mod_descriptor"),
		"ui_title": BUtil.find_child(p_ui_item, "Title"),
		"ui_description_lbl": BUtil.find_child(p_ui_item, "DescriptionLbl"),
		"ui_website_btn": BUtil.find_child(p_ui_item, "WebsiteBtn"),
		"ui_install_btn": BUtil.find_child(p_ui_item, "InstallBtn"),
		"ui_texture_rect": BUtil.find_child(p_ui_item, "TextureRect"),
	};
	
	
func get_title(mod_descriptor):
	return mod_descriptor.get_display_name() + " - " + mod_descriptor.version;
	
	
# description to show in the mod menu
func get_description(mod_descriptor):
	var desc = mod_descriptor.description;
	if (mod_descriptor.dependencies.length()):
		desc += "\n[color=aqua]Dependencies:[/color] " + mod_descriptor.dependencies.replace("\n", ", ");
		
	return desc;
	
	
func _on_local_website_pressed(p_ui_item, p_mod_descriptor):
	OS.shell_open(p_mod_descriptor.website);
	
	
func _on_local_delete_pressed(p_ui_item, p_mod_descriptor):
	var plugin = PluginMgr.get_plugin_from_descriptor(p_mod_descriptor);
	if (PluginMgr.delete_plugin(plugin)):
		p_ui_item.queue_free();
	
	
func _on_local_update_pressed(p_ui_item, p_mod_descriptor):
	var item_components = get_local_components(p_ui_item);
	item_components.ui_update_btn.text = "Updating...";
	item_components.ui_update_btn.disabled = true;
	var plugin = PluginMgr.get_plugin_from_descriptor(p_mod_descriptor);
	BModio.download_mod(plugin.mod_id);
	return;
	
	
func _on_local_enable_toggled(p_pressed, p_ui_item, p_mod_descriptor):
	p_mod_descriptor.set_enabled(p_pressed);
	return;

	
func set_visible(vis):
	.set_visible(vis);
	
	# update mod listing by requesting the first page
	if (vis):
		request_mods();
		
		
func request_mods():
	BModio.request_mods(request);
		
		

	
	
func _on_request_mods(p_mods):
	mod_io_total = p_mods.result_total;
	page_count = p_mods.page_count;
	ui_pagination_lbl.set_text(str(request.page + 1) + " of " + str(max(1, page_count)));
	
	ui_modio_list.clear();
	
	var mods = p_mods.data;
	for mod_descriptor in mods:
		var ui_item = ui_modio_list.create_item();
		ui_item.set_meta("mod_descriptor", mod_descriptor);
		var item_components = get_modio_components(ui_item);
		
		item_components.ui_title.set_text(mod_descriptor.name);
		item_components.ui_description_lbl.set_bbcode(mod_descriptor.summary);
		item_components.ui_website_btn.visible = mod_descriptor.homepage_url && mod_descriptor.homepage_url.length();
	
		item_components.ui_website_btn.connect("pressed", self, "_on_modio_website_pressed", [ui_item, mod_descriptor]);
		item_components.ui_install_btn.connect("pressed", self, "_on_modio_install_pressed", [ui_item, mod_descriptor]);
		
		item_components.ui_website_btn.set_button_icon(get_icon("Instance", "EditorIcons"));
		
		# is this mod installed?
		var plugin = PluginMgr.find_plugin_by_mod_id(mod_descriptor.id) if mod_descriptor.has("id") else null;
		if (plugin):
			item_components.ui_install_btn.text = "Installed";
			item_components.ui_install_btn.disabled = true;
		
		item_components.ui_texture_rect.load_from_url(mod_descriptor.logo.thumb_320x180, mod_descriptor.logo.filename);

	return;


func _on_modio_website_pressed(p_ui_item, p_mod_descriptor):
	OS.shell_open(p_mod_descriptor.homepage_url);
	
	
func _on_modio_install_pressed(p_ui_item, p_mod_descriptor):
	var item_components = get_modio_components(p_ui_item);
	item_components.ui_install_btn.text = "Installing...";
	item_components.ui_install_btn.disabled = true;
	BModio.download_mod(p_mod_descriptor.id);
	
	
func _on_download_mod(p_mod):
	BLog.debug("Mod downloaded: " + String(p_mod.id));
	
	var children = ui_modio_list.get_children();
	for c in children:
		var item_components = get_modio_components(c);
		if (item_components.mod_descriptor.id == p_mod.id):
			item_components.ui_install_btn.text = "Installed";
			item_components.ui_install_btn.disabled = true;
			
	children = ui_local_list.get_children();
	for c in children:
		var item_components = get_local_components(c);
		var plugin = PluginMgr.get_plugin_from_descriptor(item_components.mod_descriptor);
		if (plugin && plugin.mod_id == p_mod.id):
			item_components.ui_update_btn.text = "Updated";
			item_components.ui_update_btn.disabled = true;
			
	return;
	

func _on_PreviousPageBtn_pressed():
	request.page = max(request.page - 1, 0);
	request_mods();


func _on_NextPageBtn_pressed():
	request.page = min(request.page + 1, mod_io_total);
	request_mods();


func _on_SearchQuery_text_changed(p_text):
	search_timer.stop();
	request.query = ui_search_query.text;
	search_timer.start();
