#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var ui_resolution = BUtil.find_child(self,"Video_Resolution");
onready var ui_frame_rate = BUtil.find_child(self,"Video_FrameRate");
onready var ui_monitor = BUtil.find_child(self,"Video_Monitor");
onready var ui_full_screen = BUtil.find_child(self,"Video_Fullscreen");
var full_screen_array = ["Windowed","Borderless Window","Fullscreen"];
onready var ui_model_quality = BUtil.find_child(self,"Video_ModelQuality");
onready var ui_shadow_quality = BUtil.find_child(self,"Video_ShadowQuality");
onready var ui_shader_quality= BUtil.find_child(self, "Video_ShaderQuality");

onready var ui_master = BUtil.find_child(self, "Audio_MasterVolume");
onready var ui_music = BUtil.find_child(self,"Audio_MusicVolume");

onready var ui_player_name = BUtil.find_child(self,"Game_PlayerName");
onready var ui_hide_city_names = BUtil.find_child(self,"Game_HideCityNames");
onready var ui_camera_move_speed = BUtil.find_child(self,"Game_CameraMoveSpeed");
onready var ui_camera_zoom_speed = BUtil.find_child(self,"Game_CameraZoomSpeed");

var controller;
var _dirty = true;


func apply_to_controller(p_controller):
	controller = p_controller;
	return;


func _ready():	
	Database.connect('settings_changed', self, 'settings_changed');
	apply(true);
	update();
	return;


func settings_changed():
	update();
	return;



func update():
	# graphics
	Util.populate_option_button(ui_monitor, ["0", "1"]);
	ui_monitor.select(int(Database.get_value(Database.MONITOR, 0)));
	
	Util.populate_option_button(ui_full_screen, full_screen_array);
	ui_full_screen.select(int(Database.get_value(Database.WINDOW_FULL_SCREEN, 2)));

	ui_resolution.set_value(Database.get_value(Database.STRETCH_SHRINK, 4));
	ui_frame_rate.set_value(Database.get_value(Database.FRAME_RATE, 4));
	ui_model_quality.set_value(Database.get_value(Database.MODEL_QUALITY, 2));
	ui_shadow_quality.set_value(Database.get_value(Database.SHADOW_QUALITY, 2));
	ui_shader_quality.set_value(Database.get_value(Database.SHADER_QUALITY, 2));

	# game settings
	ui_player_name.set_text(Database.get_value(Database.PLAYER_NAME, "Recruit"));
	ui_hide_city_names.pressed = Database.get_value(Database.HIDE_CITY_NAMES, false);
	ui_camera_move_speed.value = Database.get_value(Database.CAMERA_MOVE_SPEED, 600.0);
	ui_camera_zoom_speed.value = Database.get_value(Database.CAMERA_ZOOM_SPEED, 2.0);
	
	# audio
	ui_master.set_value(Database.get_value(Database.MASTER_VOLUME, 100));
	ui_music.set_value(Database.get_value(Database.MUSIC_VOLUME, 30));
	return;


func save():
	# move UI values into config, save and apply
	# audio
	Database.set_setting(Database.MASTER_VOLUME, int(ui_master.value));
	Database.set_setting(Database.MUSIC_VOLUME, int(ui_music.value));
	
	# game
	Database.set_setting(Database.PLAYER_NAME, ui_player_name.text);
	Database.set_setting(Database.HIDE_CITY_NAMES, ui_hide_city_names.pressed);
	Database.set_setting(Database.CAMERA_MOVE_SPEED, ui_camera_move_speed.value);
	Database.set_setting(Database.CAMERA_ZOOM_SPEED, ui_camera_zoom_speed.value);
	
	# graphics
	Database.set_setting(Database.MONITOR, int(ui_monitor.get_selected()));
	Database.set_setting(Database.WINDOW_FULL_SCREEN, int(ui_full_screen.get_selected()));
	
	Database.set_setting(Database.SHADOW_QUALITY, int(ui_shadow_quality.value));
	Database.set_setting(Database.SHADER_QUALITY, int(ui_shader_quality.value));
	Database.set_setting(Database.STRETCH_SHRINK, int(ui_resolution.value));
	Database.set_setting(Database.FRAME_RATE, int(ui_frame_rate.value));
	Database.set_setting(Database.MODEL_QUALITY, int(ui_model_quality.value));
	
	Database.save();
	return;


func _on_ApplyButton_pressed():
	save();
	apply();
	return;


# apply current options
func apply(first_load_vulkan_hack = false):
	_dirty = false;
	
	# monitor
	var dst_screen = int(Database.get_value(Database.MONITOR, 0));
	if (OS.current_screen != dst_screen):
		OS.set_window_fullscreen(false);
		OS.set_current_screen(dst_screen);

	var frame_rate_value = Database.get_value(Database.FRAME_RATE, 0);
	if (frame_rate_value == 0):
		Engine.set_target_fps(20);
	if (frame_rate_value == 1):
		Engine.set_target_fps(30);
	if (frame_rate_value == 2):
		Engine.set_target_fps(40);
	if (frame_rate_value == 3):
		Engine.set_target_fps(60);
	if (frame_rate_value == 4):
		Engine.set_target_fps(0);

#	# window mode
	var window_full_screen_value = Database.get_value(Database.WINDOW_FULL_SCREEN, 2);
	if (window_full_screen_value == 0):
		OS.set_window_fullscreen(false);
		if (!first_load_vulkan_hack):
			OS.set_borderless_window(false);
	if (window_full_screen_value == 1):
		OS.set_window_fullscreen(false);
		if (!first_load_vulkan_hack):
			OS.set_borderless_window(true);
	if (window_full_screen_value == 2):
		OS.set_window_fullscreen(true);
		if (!first_load_vulkan_hack):
			OS.set_borderless_window(false);
		
	apply_audio();


	# shadows
	var shadow_value = Database.get_value(Database.SHADOW_QUALITY, 0);
	if (shadow_value == 0):
		Database.set_setting(Database.DIRECTIONAL_SHADOW_SIZE, 16); # not ShadowAtlasSize, as we arent using shadow atlas!
		Database.set_setting(Database.SHADOW_FILTER_MODE, 0);
	if (shadow_value == 1):
		Database.set_setting(Database.DIRECTIONAL_SHADOW_SIZE, 2048);
		Database.set_setting(Database.SHADOW_FILTER_MODE, 1);
	if (shadow_value == 2):
		Database.set_setting(Database.DIRECTIONAL_SHADOW_SIZE, 4096);
		Database.set_setting(Database.SHADOW_FILTER_MODE, 1);
	if (shadow_value == 3):
		Database.set_setting(Database.DIRECTIONAL_SHADOW_SIZE, 8192);
		Database.set_setting(Database.SHADOW_FILTER_MODE, 2);
	if (shadow_value == 4):
		Database.set_setting(Database.DIRECTIONAL_SHADOW_SIZE, 16384);
		Database.set_setting(Database.SHADOW_FILTER_MODE, 2);

	# AA
	var shader_quality = Database.get_value(Database.SHADER_QUALITY, 0);
	if (shader_quality == 0):
		Database.set_setting(Database.ANISOTROPIC_FILTER_LEVEL, 1);
	if (shader_quality == 1):
		Database.set_setting(Database.ANISOTROPIC_FILTER_LEVEL, 4);
	if (shader_quality == 2):
		Database.set_setting(Database.ANISOTROPIC_FILTER_LEVEL, 8);
	if (shader_quality == 3):
		Database.set_setting(Database.ANISOTROPIC_FILTER_LEVEL, 12);
	if (shader_quality == 4):
		Database.set_setting(Database.ANISOTROPIC_FILTER_LEVEL, 16);

	# model quality
	var model_quality = Database.get_value(Database.MODEL_QUALITY, 0);
	if (model_quality == 0): # potato
		Database.set_setting(Database.TERRAIN_MODEL_QUALITY, 0.0);
		Database.set_setting(Database.TERRAIN_SKIRT_ENABLED, 0);
		Database.set_setting(Database.TERRAIN_SHADOWS_ENABLED, 0);
		Database.set_setting(Database.TERRRAIN_DETAIL_NORMAL_MAPPING, 0);
		Database.set_setting(Database.TERRRAIN_AMBIENT_OCCLUSION, 0);
		Database.set_setting(Database.LOD_DISTANCE_OFFSET, -10000);
	if (model_quality == 1): # low
		Database.set_setting(Database.TERRAIN_MODEL_QUALITY, 0.25);
		Database.set_setting(Database.TERRAIN_SKIRT_ENABLED, 0);
		Database.set_setting(Database.TERRAIN_SHADOWS_ENABLED, 0);
		Database.set_setting(Database.TERRRAIN_DETAIL_NORMAL_MAPPING, 1);
		Database.set_setting(Database.TERRRAIN_AMBIENT_OCCLUSION, 0);
		Database.set_setting(Database.LOD_DISTANCE_OFFSET, -1000);
	if (model_quality == 2): # normal
		Database.set_setting(Database.TERRAIN_MODEL_QUALITY, 0.5);
		Database.set_setting(Database.TERRAIN_SKIRT_ENABLED, 1);
		Database.set_setting(Database.TERRAIN_SHADOWS_ENABLED, 1);
		Database.set_setting(Database.TERRRAIN_DETAIL_NORMAL_MAPPING, 1);
		Database.set_setting(Database.TERRRAIN_AMBIENT_OCCLUSION, 1);
		Database.set_setting(Database.LOD_DISTANCE_OFFSET, 0);
	if (model_quality == 3): # high
		Database.set_setting(Database.TERRAIN_MODEL_QUALITY, 0.8);
		Database.set_setting(Database.TERRAIN_SKIRT_ENABLED, 1);
		Database.set_setting(Database.TERRAIN_SHADOWS_ENABLED, 1);
		Database.set_setting(Database.TERRRAIN_DETAIL_NORMAL_MAPPING, 1);
		Database.set_setting(Database.TERRRAIN_AMBIENT_OCCLUSION, 1);
		Database.set_setting(Database.LOD_DISTANCE_OFFSET, 10000);
	if (model_quality == 4): # ultra
		Database.set_setting(Database.TERRAIN_MODEL_QUALITY, 1.0);
		Database.set_setting(Database.TERRAIN_SKIRT_ENABLED, 1);
		Database.set_setting(Database.TERRAIN_SHADOWS_ENABLED, 1);
		Database.set_setting(Database.TERRRAIN_DETAIL_NORMAL_MAPPING, 1);
		Database.set_setting(Database.TERRRAIN_AMBIENT_OCCLUSION, 1);
		Database.set_setting(Database.LOD_DISTANCE_OFFSET, 20000);

	return;


func apply_audio():
	var master_volume = BDatabase.get_value(Database.MASTER_VOLUME, 100);
	var master_idx = AudioServer.get_bus_index('Master');
	AudioServer.set_bus_volume_db(master_idx, volume_percent_to_db(master_volume));
	var music_volume = BDatabase.get_value(Database.MUSIC_VOLUME, 25);
	var music_idx = AudioServer.get_bus_index('Music');
	AudioServer.set_bus_volume_db(music_idx, volume_percent_to_db(music_volume));
	return;


func volume_percent_to_db(percent):
	var db = 10 * log(float(percent) / 100.0)
	db = clamp(db, -80, 0);
	return db;


func apply_game():
	var name = Database.get_value(Database.settings.player_name, "Rookie");
	Game.set_player_name(name);
	return;

