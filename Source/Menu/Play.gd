#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

var controller; # this is the main root menu

onready var ui_title_lbl = BUtil.find_child(self, "TitleLbl");
onready var ui_map_list = BUtil.find_child(self, "MapList");
onready var ui_map_description_lbl = BUtil.find_child(self, "MapDescriptionLbl");
onready var ui_thumbnail = BUtil.find_child(self, "Thumbnail");
onready var ui_players = BUtil.find_child(self, "Players");
onready var ui_player_list = BUtil.find_child(self, "PlayerList");
onready var ui_private = BUtil.find_child(self, "Private");
onready var ui_game_password = BUtil.find_child(self, "Game_Password");
onready var ui_game_settings_control = BUtil.find_child(self, "GameSettingsControl");
onready var ui_start_btn = BUtil.find_child(self, "StartBtn");

var is_selecting_map = false;


func apply_to_controller(p_controller):
	controller = p_controller;
	# assign self as play
	if controller.play == null:
		controller.play = self;
	return;


func _init():
	return;


func _ready():
	# connect pmultiplay stuff
	Game.connect("connection_succeeded", self, "on_connection_succeeded") # for client
	Game.connect("register_client", self, "on_register_client"); # for server
	Game.connect("unregister_client", self, "on_unregister_client"); # for server
	
	ui_game_settings_control.connect("changed", self, "on_game_settings_control_changed");
	
	ui_players.set_visible(!is_private());

	var player_name = Database.get_value(Database.PLAYER_NAME, "Recruit");
	
	# display your IP for easy joining - TODO: not very reliable this method, need some regex checking here
	var ip = Util.get_best_ip4_lan_address();
	BUtil.find_child(self, "LocalIpLbl").set_text("IP: %s" % ip)
	setup_map_list();
	
	call_deferred("check_load_straight_into_scene");
	return;
	
	
func check_load_straight_into_scene():
	var map = BDatabase.get_value("dev/map", "");
	if (!map || map.empty()):
		return;
		
	Game.set_host_server(true);
	var game_state = get_state();
	game_state.map = PluginMgr.get_map_resource_path(map);
	controller.begin_game(game_state);
	
	
# get lobby state
func get_state():
	var game_settings = ui_game_settings_control.get_state();
	var selected_map = ui_map_list.get_selected().get_text(0);
	
	var state = {};
	state.game_settings = game_settings;
	state.map = selected_map;
	return state;
	
	
master func on_lobby_change():
	# only server can change the server settings
	if (!get_tree().has_network_peer() || !get_tree().is_network_server()):
		return;
		
	var lobby_state = get_state();
	rpc("sync_lobby_rpc", lobby_state);
	return;
	
	
remotesync func sync_lobby_rpc(lobby_state):
	# server doesnt need to update the UI as they are the authority
	if (get_tree().is_network_server()):
		return;
		
	# apply the game settings
	ui_game_settings_control.set_state(lobby_state.game_settings);

		
	# select the current map
	set_map_selectable(true);
	var map_items = Util.getTreeItemChildren(ui_map_list.get_root());
	for map_item in map_items:
		if (map_item.get_text(0) == lobby_state.map):
			map_item.select(0);
			break;
			
	var is_client = !get_tree().is_network_server();
	set_map_selectable(!is_client);
			
			
func set_map_selectable(p_selectable):
	var map_items = Util.getTreeItemChildren(ui_map_list.get_root());
	for map_item in map_items:
		map_item.set_selectable(0, p_selectable);
		map_item.set_selectable(1, p_selectable);
		map_item.set_selectable(2, p_selectable);
	
	
func on_game_settings_control_changed(p_state):
	on_lobby_change();


# when a client joints, send them the lobby state
func on_register_client(p_client_dict):
	var player_id = p_client_dict.id;
	var lobby_state = get_state();
	rpc_id(player_id, "sync_lobby_rpc", lobby_state);
	update_multiplay_lobby();
	
	
func on_unregister_client(p_client_dict):
	update_multiplay_lobby();
	
	
func on_connection_succeeded():
	update_multiplay_lobby();
	
	
func update_multiplay_lobby():
	# the client and server code both use this
	# ensure the gui is setup
	ui_private.pressed = false;
	
	# update player list
	var client_list = Game.get_clients();

	ui_player_list.clear();
	ui_player_list.set_hide_root(true);
	ui_player_list.create_item(); # create the root item
	ui_player_list.set_columns(3);
	ui_player_list.set_column_title(0, "Name");
	ui_player_list.set_column_title(1, "Difficulty");
	ui_player_list.set_column_title(2, "Team");
	ui_player_list.set_column_min_width(0, 8);
	ui_player_list.set_column_min_width(1, 4);
	ui_player_list.set_column_min_width(2, 3);
	ui_player_list.set_column_titles_visible(true);
	ui_player_list.set_select_mode(ui_player_list.SELECT_ROW);
	
	# populate
	# Names for remote players in id:name format
	for client in client_list:
		var tree_item = ui_player_list.create_item(ui_player_list.get_root());
		tree_item.set_text(0, client.name);
		tree_item.set_collapsed(false);
		
	return;


func is_private():
	return ui_private.pressed;


func _on_StartBtn_pressed():
	if (!get_tree().is_network_server()):
		return;
		
	var game_state = get_state();
	# for now do this here instead of in Singleplayer.gd or Multiplayer.gd as they overwrite each other
	BDatabase.set_setting("main_menu/selected_map", game_state.map);
	BDatabase.save();
	game_state.map = PluginMgr.get_map_resource_path(game_state.map);
	controller.begin_game(game_state);
	return


func update_hosting_status():
	if (controller.master_server):
		var hosting = !is_private();
		var password = ui_game_password.get_text();
		Game.set_password(password);
		controller.master_server.update_hosting_status(hosting, password.length() > 0);
	return;


func get_selected():
	return ui_map_list.get_selected().get_text(0);


func setup_map_list():
	var ui_map_tree = ui_map_list;
	if (!ui_map_tree):
		return;
	
	ui_map_tree.set_hide_root(true);
	ui_map_tree.create_item(); # create the root item
	ui_map_tree.set_columns(3);
	ui_map_tree.set_column_title(0, "Map");
	ui_map_tree.set_column_title(1, "Size");
	ui_map_tree.set_column_title(2, "Players");
	ui_map_tree.set_column_min_width(0, 8);
	ui_map_tree.set_column_min_width(1, 4);
	ui_map_tree.set_column_min_width(2, 3);
	ui_map_tree.set_column_titles_visible(true);
	ui_map_tree.set_select_mode(ui_map_tree.SELECT_ROW);
	
	var selectedStr = BDatabase.get_value("main_menu/selected_map", "");
	var first = true
	var map_descriptors = PluginMgr.get_map_descriptors();

	# a dictionary of name + descriptor
	var map_dict = {};
	for map_descriptor in map_descriptors:
		var map_name = map_descriptor.get_display_name();
		if map_dict.has(map_name):
			map_dict[map_name].append(map_descriptor);
		else:
			map_dict[map_name] = [map_descriptor];
	
	# apply dictionaty to the tree
	for map_dict_item in map_dict.keys():
		var map_array = map_dict[map_dict_item];
		var tree_item = ui_map_tree.create_item(ui_map_tree.get_root());
		tree_item.set_text(0, map_dict_item);
		tree_item.set_collapsed(true);
		
		# add children to the tree_item
		for map_descriptor in map_array:
			if (is_private() && !map_descriptor.available_in_singleplayer):
				continue;
				
			if (!is_private() && !map_descriptor.available_in_multiplayer):
				continue;
			
			# no children if 1 map
			var item = tree_item;
			if map_array.size() > 1:
				item = ui_map_tree.create_item(tree_item);
				
			item.set_text(0, map_descriptor.get_display_name());
			item.set_text(1, str(map_descriptor.map_size));
			item.set_text(2, str(map_descriptor.recommended_player_count));
			item.set_metadata(0, map_descriptor);
			
			# select first item
			if (first):
				first = false
				item.select(0)
				
			# now select what the user last selected last time the menu was displayed
			if (selectedStr && selectedStr == map_descriptor.get_display_name()):
				item.select(0)
	return;


func _on_MapList_item_selected():
	if is_selecting_map:
		return;
		
	is_selecting_map = true;
	var item = ui_map_list.get_selected();
	
	# hide all others
	var root_children = Util.getTreeItemChildren(ui_map_list.get_root());
	for child in root_children:
		if item != child:
			child.set_collapsed(true);
	
	# expand selection if possible
	item.set_collapsed(false);
	
	# check if we are a child object
	if (item.get_parent() != ui_map_list.get_root()):
		item.get_parent().set_collapsed(false);
		
	# check if has children
	# select first child
	var selected_children = Util.getTreeItemChildren(item);
	if selected_children != null and selected_children.size() > 0:
		item = selected_children[0];
		item.select(0); # select the item
		
	is_selecting_map = false;
	
	# or we are a map
	var map_descriptor = item.get_metadata(0);
	if map_descriptor == null:
		return;
	var map_description = map_descriptor.get_description();
	#var map_size = str(map_descriptor.map_size);
	ui_map_description_lbl.set_bbcode(map_description);
	ui_thumbnail.texture = map_descriptor.thumbnail;
	
	on_lobby_change();
	return;


func _on_Singleplayer_toggled(state):
	ui_players.set_visible(!state);
	update_hosting_status();
	return;

	
func set_visible(vis):	
	# game code is so dependant on the multiplayer systems of godot
	# we always needs to host, even if its single player
	if (Game.game_state != Game.GameState.INGAME):
		if (Game.network_state != Game.NetworkState.CLIENT):
			if (!Game.set_host_server(vis)):
				return; # failed to host
				
	.set_visible(vis);
		
	if (!vis):
		# disable master server if we leave the play menu
		if (controller.master_server):
			controller.master_server.update_hosting_status(false, false);
			
		# disconnect from any connected servers when we leave the Join/Lobby screen (if a client)
		Game.leave_game();
		
	# disable UI to be read-only for clients
	var is_client = !get_tree().is_network_server();
	
	ui_title_lbl.text = "Joined Game" if is_client else "Host Game";
	
	ui_private.disabled = is_client;
	ui_start_btn.disabled = is_client;
	
	var buttons = BUtil.find_children_by_class_name(self, "ButtonBase");
	for b in buttons:
		b.disabled = is_client;
		
	buttons = BUtil.find_children_by_class_name(self, "OptionButton");
	for b in buttons:
		b.disabled = is_client;
		
	var line_edits = BUtil.find_children_by_class_name(self, "LineEdit");
	for l in line_edits:
		l.editable = !is_client;
		
	set_map_selectable(!is_client);


func _on_Game_Password_text_changed(p_text):
	update_hosting_status();
