#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
extends Node

onready var ui_master_server_dlg = BUtil.find_child(self, "MasterServerDlg");
onready var ui_chat_dlg = BUtil.find_child(self, "ChatDlg");

onready var ui_message_edit = BUtil.find_child(ui_chat_dlg, "MessageEdit");
onready var ui_history = BUtil.find_child(ui_chat_dlg, "History");
onready var ui_message_list = BUtil.find_child(ui_chat_dlg, "MessageList");
onready var ui_users = BUtil.find_child(ui_chat_dlg, "Users");
onready var ui_user_list = BUtil.find_child(ui_chat_dlg, "UserList");
onready var ui_message_template = BUtil.find_child(ui_chat_dlg, "MessageTemplate");
#onready var ui_user_template = $Control/MatrixDlg/VBoxContainer/HSplitContainer/Users/UserList/UserTemplate;

const MAX_VISIBLE_USERS = 1000;

var scroll_to_bottom = true
var scrolled_to_bottom = true
var scrolled_to_top = false

#var member_ui_map = {};

var options;
var controller; # this is the main root menu
var scroll_timer;
#var user_popup_menu = PopupMenu.new();

const JOIN_ID = 1;

func apply_to_controller(p_controller):
	controller = p_controller;
	controller.master_server = self;
	
	Database.connect("settings_changed", self, "_options_changed");
	controller.connect("quiting", self, "_quiting");
	return;

func _ready():
	ui_chat_dlg.show();
	ui_message_template.set_visible(false);
	
	BMasterServer.connect("message_received", self, "on_message_received");
	BMasterServer.connect("channel_users_received", self, "on_channel_users_received");
	BMasterServer.connect("channel_connected", self, "on_channel_connected");
	BMasterServer.connect("channel_user_join", self, "on_channel_user_join");
	BMasterServer.connect("server_connected", self, "on_server_connected");
	BMasterServer.connect("channel_user_quit", self, "on_channel_user_quit");
	BMasterServer.connect("channel_user_nick_change", self, "on_channel_user_nick_change");
	BMasterServer.connect("error", self, "on_error");
	BMasterServer.connect_to_master_server();
	
	scroll_timer = Util.create_timer(self, "on_scroll_to_bottom", 0.2, true, true);
	add_message("", "[color=yellow]Connecting...[/color]");
	return;
	
	
func on_channel_user_nick_change(p_channel, p_from, p_to):
	if (p_channel == BMasterServer.get_chat_channel()):
		add_message("", "[color=yellow]" + p_from + " is now known as " + p_to + "[/color]");
		
	
func on_channel_user_quit(p_channel, p_nick):
	if (p_channel == BMasterServer.get_chat_channel()):
		add_message("", "[color=yellow]" + p_nick + " has quit[/color]");
		
		
func on_channel_user_join(p_channel, p_nick):
	if (p_channel == BMasterServer.get_chat_channel()):
		add_message("", "[color=yellow]" + p_nick + " has joined[/color]");
	
	
func on_server_connected(p_server_name):
	clear_messages();
	add_message("", "[color=yellow]Connected to " + p_server_name + "[/color]");
	

func on_channel_connected(p_channel, p_my_nick):
	if (p_channel == BMasterServer.get_chat_channel()):
		add_message("", "[color=yellow]Joined " + BMasterServer.get_chat_channel() + " as " + p_my_nick + "[/color]");
	
	
func on_channel_users_received(p_channel, p_user_count):
	if (p_channel == BMasterServer.get_chat_channel()):
		add_message("", "[color=yellow]" + BMasterServer.get_chat_channel() + " has " + str(p_user_count) + " users[/color]");
	
	
func on_error(text):
	add_message("", "[color=red]" + text + "[/color]");
	
	
func on_message_received(nick, text):
	add_message(nick, text);
	return;


func on_sort_message_children(message_ui):
	var message_label = BUtil.find_child(message_ui, "Message");
	if (message_label):
		#BLog.debug("start add_message");
		#message_label.get_parent().remove_child(message_label);
		#message_label.set_custom_minimum_size(Vector2(message_label.rect_min_size.x, 10));
		#message_label.set_size(Vector2(message_label.rect_min_size.x, 10));
		#BLog.debug("sz w: " + message_label.get_size());
		var sz = message_label.get_size();
		var height = BUtil.get_rich_text_label_height(message_label);
		#message_label.scroll_to_line(0); # force vscroll to be updated
		#var height = message_label.get_v_scroll().get_max();
		message_label.set_custom_minimum_size(Vector2(message_label.rect_min_size.x, height));
		message_ui.set_custom_minimum_size(Vector2(message_ui.rect_min_size.x, height));
		#BLog.debug("end add_message, height:" + String(height));
		
	return;


func clear_messages():
	while (ui_message_list.get_child_count()):
		ui_message_list.remove_child(ui_message_list.get_child(0));


func add_message(display_name, text):
	var message_ui = ui_message_template.duplicate();
	var container = message_ui.get_child(0);
	container.connect("sort_children", self, "on_sort_message_children", [message_ui]);
	
	add_event(message_ui, false);
	message_ui.set_visible(true);
	
	var sender_label = BUtil.find_child(message_ui, "Sender");
	if (sender_label):
		if (display_name.length() <= 0):
			sender_label.set_visible(false);
		else:
			sender_label.set_bbcode("[color=white]" + display_name + "[/color]");
	
	var message_label = BUtil.find_child(message_ui, "Message");
	if (message_label):
		message_label.set_bbcode(text);
	
	#call_deferred("scroll_to_bottom");
	scroll_timer.start();
	return message_ui;


func on_scroll_to_bottom():
	# scroll to bottom
	var max_val = ui_history.get_node("_v_scroll").get_max();
	ui_history.set_v_scroll(max_val);


# the user has started hosting a game, or has ended hosting a game
func update_hosting_status(is_hosting, is_passworded):
	var old_server_data = BMasterServer.get_server_data();
	if (is_hosting):
		var data = {
				v = BDatabase.get_value("application/version", ""),
				i = [Util.get_best_ip4_lan_address(), BInternet.get_internet_ip()],
				c = BInternet.get_country() # country
			};

		# inject p for passworded if server is passworded
		if (is_passworded):
			data["p"] = "1";
			
		# only send update if server data actually changed!
		if (old_server_data.hash() != data.hash()):
			BMasterServer.update_server(data);
	else:
		if (!old_server_data.empty()):
			BMasterServer.delete_server();

	return;


func _options_changed():
	var player_name = Database.get_value(Database.PLAYER_NAME, '');
	BMasterServer.set_nick(player_name, false);
	return;


func _quiting():
	update_hosting_status(false, false); # close the hosting status
	return;


func _on_MessageEdit_text_entered( text ):
	if text != "" and BMasterServer.get_connection_status() == BMasterServer.CS_CONNECTED:
		BLog.debug("_on_Message_text_entered: " + text);
		BMasterServer.send_text_message(text);
		add_message(BMasterServer.get_nick(), text);
		ui_message_edit.clear();
	return;


func add_event(event, old):
	if not old:
		ui_message_list.add_child(event);
		if (scrolled_to_bottom):
			scroll_to_bottom = true;
	else:
		ui_message_list.add_child(event);
		ui_message_list.move_child(event, 0);
		#yield(ui_history.get_node("_v_scroll"), "changed");


func scrollbar_changed(v):
	var scrollbar = ui_history.get_node("_v_scroll");
	if scrollbar.page >= ui_message_list.get_minimum_size().y:
		scrolled_to_bottom = true;
	if scroll_to_bottom:
		scrollbar.set_value(scrollbar.max_value-scrollbar.page);
		scroll_to_bottom = false;
	if scrolled_to_bottom:
		scrollbar.set_value(scrollbar.max_value-scrollbar.page);


func scrollbar_value_changed(v):
	var scrollbar = ui_history.get_node("_v_scroll");
	scrolled_to_bottom = (scrollbar.max_value == 0 or scrollbar.value == scrollbar.max_value-scrollbar.page);
	scrolled_to_top = scrollbar.value == 0;


func _on_InfoLabel_meta_clicked(meta):
	BLog.debug("Open url: " + meta);
	OS.shell_open(meta);
	return;

