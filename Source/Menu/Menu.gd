#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

signal quiting();

onready var ui_root_control = BUtil.find_child(self, "RootControl");
onready var errorDlg = BUtil.find_child(self, "ErrorDlg");
onready var ui_version_name_lbl = BUtil.find_child(self, "VersionNameLbl");
onready var ui_version_lbl = BUtil.find_child(self, "VersionLbl");
onready var ui_anim_player = BUtil.find_child(self, "AnimationPlayer");
onready var ui_button_template = BUtil.find_child(self, "MainExitButton");
onready var ui_game_button_template = BUtil.find_child(self, "GameExitButton");
onready var ui_button_container = BUtil.find_child(self, "ButtonContainer");
onready var ui_game_button_container = BUtil.find_child(self, "GameButtonContainer");
onready var ui_template_container = BUtil.find_child(self, "TemplateContainer");
onready var ui_template_scroll_container = BUtil.find_child(self, "TemplateScrollContainer");
onready var ui_template_panel = BUtil.find_child(self, "TemplatePanel");
onready var ui_window_container = BUtil.find_child(self, "WindowContainer");
onready var ui_loading_container = BUtil.find_child(self, "LoadingContainer");
onready var ui_splash_container = BUtil.find_child(self, "SplashContainer");
onready var ui_button_pnl = BUtil.find_child(self, "ButtonPnl");

# viewport
onready var ui_viewport_container = BUtil.find_child(self, "ViewportContainer");
onready var ui_background = BUtil.find_child(self, "BackgroundViewport");

var master_server;
var loading;

#var template_container_visible = false;
var active_panel;
var hud; # player hud
var play; # the play panel/controller set in play.gd

func request_completed ( result, response_code, headers, body ):
	BLog.debug("request received!");
	return;


func _ready():
	ui_loading_container.set_visible(false);
	ui_root_control.set_visible(false);
	ui_template_panel.set_visible(false);
	
	# setup multiplay connection signal
	Game.connect("connection_succeeded", self, "on_connection_success");
	Game.connect("game_loading", self, "on_game_loading");
	Game.connect("game_started", self, "on_game_started");
	call_deferred("_load");
	return;


func _load():
	BInternet.init();

	# window user quit
	get_tree().set_auto_accept_quit(false);
	
	# set version labels
	var version_name = BDatabase.get_value("application/version_name", "");
	ui_version_name_lbl.set_visible(version_name.length() > 0);
	ui_version_name_lbl.set_text(version_name);
	
	var version_major = BDatabase.get_value("application/version_major", "");
	var build_number = BDatabase.get_value("application/build_number", "");
	ui_version_lbl.set_text("v" + version_major + " b" + build_number);
	
	# add attachments for the menu controller
	_setup_from_database()
	
	ui_anim_player.play("show_button_panel");

	# buttons
	ui_button_template.connect("pressed", self, "quit");
	ui_game_button_template.connect("pressed", self, "quit_game");
	ui_game_button_container.set_visible(false);
	
	Game.connect("game_started", self, "_on_game_started")
	Game.connect("game_ended", self, "_on_game_ended")
	Game.connect("game_error", self, "_on_game_error")

	# go straight to the host lobby
	var join_ip = BDatabase.get_value("dev/join_ip", null);
	if (auto_host() != -1):
		play._on_HostBtn_pressed();
	# or go straight into joining a MP game
	elif (join_ip):
		Game.join_game(join_ip);
	return;


func _setup_from_database():
	BLog.debug("Loading Menu Prefabs");
	
	var db_menu = BDatabase.get_value("menu");
	for item in db_menu:
		var type = item['type'] if item.has('type') else null;
		var res_name = item['resource'];
		var res = load(res_name);
		Util.assert(res, "Failed to load: " + res_name);
		if (!res):
			continue;
		
		var instance = res.instance();
		
		# allow the instance to setup anything it needs
		if (instance.has_method("apply_to_controller")):
			instance.apply_to_controller(self);
		
		if (type == 'panel'):			
			ui_template_container.add_child(instance);
			
			instance.hide();
			instance.name = item['title'];
			
			# add button and connect
			if (item.has('button_text')):
				var button_parent = ui_button_template.get_parent();
				var new_button = ui_button_template.duplicate();
				button_parent.add_child(new_button);
				button_parent.move_child(new_button, new_button.get_index()-1);
				new_button.name = item['button_text'];
				new_button.text = item['button_text'];
				new_button.toggle_mode = true;
				new_button.connect("toggled", self, "_button_toggled", [instance]);

		if (type == 'window'):
			ui_window_container.add_child(instance);
			
		if (type == 'loading'):
			ui_loading_container.add_child(instance);
			
		if (item.has('visible_game') && item['visible_game']):
			var button_parent = ui_game_button_template.get_parent();
			var new_button = ui_game_button_template.duplicate();
			button_parent.add_child(new_button);
			button_parent.move_child(new_button, new_button.get_index()-1);
			new_button.name = item['button_text'];
			new_button.text = item['button_text'];
			new_button.toggle_mode = true;
			new_button.connect("toggled", self, "_button_toggled", [instance]);
		
	return;


# automatically host a game, and when so many clients join, start the game
func auto_host():
	return BDatabase.get_value("dev/auto_host", -1);


func on_connection_success():
	# the user has connected to a game
	# move them to the play screen
	switch_panel(play);
	return;


func _button_toggled(button_pressed, panel):
	var template_container_is_visible = ui_template_panel.is_visible();
	
	# when any menu is pressed
	if button_pressed and not template_container_is_visible:
		# if a button is pressed while the panel is not visible
		show_panel(panel);
	elif not button_pressed and template_container_is_visible:
		# if a button is released (closed) and container is visible
		close_panel(panel);
	else:
		# new button pressed while panel is open
		# swap, no animations
		switch_panel(panel);
		
	ui_template_panel.set_visible(button_pressed);
	return;


func switch_panel(panel):
	if (active_panel):
		active_panel.set_visible(false);
		
	ui_template_scroll_container.set_v_scroll(0);
	panel.set_visible(true);
	active_panel = panel;
	return;


func show_panel(panel):
	panel.set_visible(true);
	#ui_anim_player.play("show_template_panel");
	active_panel = panel;
	return;


func close_panel(panel):
	if (!panel):
		return;
		
	# disable animation for now
	#ui_anim_player.play("show_template_panel", -1, -4, true); # play_backwards fast
	#ui_anim_player.connect("animation_finished", self, "_panel_anim_complete", [panel], CONNECT_ONESHOT);
	_panel_anim_complete("", panel);
	return;


func _panel_anim_complete(anim_name, panel):
	ui_template_panel.set_visible(false);
	ui_template_scroll_container.set_v_scroll(0);
	panel.hide();
	return;


func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		quit();
	return;


func quit_game():
	# close any other open panels first if open
	button_container_clear_toggled();
		
	Game.end_game();
	return;


func quit():
	_quit("");
	return;


func _quit(anim_name):
	# all animation complete, quit
	#if anim_name == "show_button_panel":
	set_visible(false);
	emit_signal("quiting");
	get_tree().quit();
	
	#if anim_name == "show_template_panel":
	#	ui_template_panel.set_visible(false);
		
	#if template_container_visible:
	#	ui_anim_player.play("show_template_panel", -1, -4, true); # play_backwards but really fast
	#else:
	#	ui_anim_player.play("show_button_panel", -1, -4, true); # play_backwards but really fast
		
	#ui_anim_player.connect("animation_finished", self, "_quit");
	return;


func begin_game(p_game_state):
	# clear menu state
	button_container_clear_toggled();
	Game.begin_game(p_game_state);
	return;


func _on_game_error(errtxt):
	BLog.error(errtxt)
	BUtil.find_child(errorDlg, "ErrorMsg").set_text(errtxt);
		
	# raise doesnt seem to work, so incetead I've made a ErrorCenterContainer to ensure the error
	# message is above everything
	errorDlg.raise();	
	errorDlg.show(); #popup_centered_minsize()	
	return;


func _on_game_started():
	button_container_clear_toggled();
	set_visible(false);
	ui_button_container.set_visible(false);
	ui_game_button_container.set_visible(true);
	ui_viewport_container.set_visible(false);
	ui_template_panel.set_visible(false);
	return;


func _on_game_ended():
	set_visible(true);
	ui_game_button_container.set_visible(false);
	ui_button_container.set_visible(true);
	ui_viewport_container.set_visible(true);
	return;


func button_container_clear_toggled():
	ui_button_container.clear_toggled();
	ui_game_button_container.clear_toggled();
	close_panel(active_panel);
	return;



# set menu visibility
func set_visible(p_visible):
	ui_root_control.set_visible(p_visible);
	return;


func is_visible():
	return ui_root_control.visible;
	
	
# show loading screen
func on_game_loading():
	close_panel(active_panel);
	ui_button_pnl.set_visible(false);
	ui_window_container.set_visible(false);
	ui_loading_container.set_visible(true);
	
	# notify and loading panels to prepare themselves
	for l in ui_loading_container.get_children():
		if (l && l.has_method("start_loading")):
			l.start_loading();
	
	
# hide loading screen and restore menu
func on_game_started():
	ui_button_pnl.set_visible(true);
	ui_window_container.set_visible(true);
	ui_loading_container.set_visible(false);


func _on_ErrorOkBtn_pressed():
	errorDlg.hide();
	return;


func _on_GameResumeButton_pressed():
	button_container_clear_toggled();
	set_visible(false);
	hud.set_visible(true);

