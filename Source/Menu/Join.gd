#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

#var options;
var controller; # this is the main root menu

#onready var ui_join_dlg = BUtil.find_child(self, "JoinDlg");
onready var ui_map_widget = BUtil.find_child(self, "MapWidget");
onready var ui_join_ip_edit = BUtil.find_child(self, "JoinIpEdit");
onready var ui_status_label = BUtil.find_child(self, "StatusLabel");
onready var ui_server_list = BUtil.find_child(self, "ServerList");
onready var ui_password = BUtil.find_child(self, "PasswordEdit");
var nick_item_map = {};

func apply_to_controller(p_controller):
	controller = p_controller;
	return;


func _ready():
	#BLog.debug("_ready called on multiplayer");
	# load from db
	ui_join_ip_edit.set_text(Database.get_value(Database.JOIN_ADDRESS, "127.0.0.1"));
	
	nick_item_map = {};
	ui_server_list.set_hide_root(true)
	ui_server_list.create_item()
	ui_server_list.set_column_titles_visible(true)
	ui_server_list.set_columns(4)
	ui_server_list.set_column_title(0, "P")
	ui_server_list.set_column_title(1, "Host")
	ui_server_list.set_column_title(2, "Country")
	ui_server_list.set_column_title(3, "Version")
	#ui_server_list.set_column_title(3, "Ping")
	
	ui_server_list.set_column_min_width(0, 1);
	ui_server_list.set_column_min_width(1, 8);
	ui_server_list.set_column_min_width(2, 8);
	ui_server_list.set_column_min_width(3, 5);
	
	ui_server_list.set_select_mode(ui_server_list.SELECT_ROW);
	
	# this is for testing:
	#var sd = { "nick": "bob", "c": "adelaide" };
	#_on_server_updated("c", "bob", sd);

	BMasterServer.connect("server_updated", self, "_on_server_updated", [], Object.CONNECT_DEFERRED);
	BMasterServer.connect("channel_connected", self, "_on_channel_connected", [], Object.CONNECT_DEFERRED);
	BMasterServer.connect("channel_user_nick_change", self, "_on_channel_user_nick_change", [], Object.CONNECT_DEFERRED);
	ui_status_label.set_text("Connecting to Master Server...");		
	return;


func _on_channel_user_nick_change(p_channel, p_from, p_to):
	if (p_channel == BMasterServer.get_server_channel()):
		# if they are hosting a game we need to update things!
		# remove the old item, add a new item
		var item = nick_item_map[p_from] if nick_item_map.has(p_from) else null;
		if (item):
			var server_data = item.get_metadata(0);
			server_data["nick"] = p_to;
			_on_server_updated("d", p_from, {});
			_on_server_updated("c", p_to, server_data);
		
		
func _on_server_updated(p_action, p_nick, p_server_data):
	BLog.debug("on_server_updated, action:" + p_action + ", nick:" + p_nick);
	var item = nick_item_map[p_nick] if nick_item_map.has(p_nick) else null;
	
	# delete
	if p_action == "d":
		if (item):
			ui_server_list.get_root().remove_child(item);
			item.free();
		return;
		
	# create or update action	
	var version = p_server_data["v"] if p_server_data.has("v") else "?"; # version
	var ips = p_server_data["i"] if p_server_data.has("i") else []; # LAN IP's followed by internet IPs
	var nick = p_server_data["nick"] if p_server_data.has("nick") else "?";
	var country = p_server_data["c"] if p_server_data.has("c") else "?";
	var passworded: bool = true if p_server_data.has("p") else false;

	if item == null:
		#var items = Util.getTreeItemChildren(ui_server_list_root);
		var idx = -1;
		item = ui_server_list.create_item(ui_server_list.get_root(), idx); # second param = index, TODO: find where to insert so games are sorted by name. For now we just dump it on the end
		nick_item_map[p_nick] = item;
	
	item.set_text(0, "x" if passworded else "");
	item.set_text(1, nick);
	item.set_text(2, country);
	item.set_text(3, version);
	
	item.set_metadata(0, p_server_data);
	return;


func _on_channel_connected(p_channel, p_my_nick):
	if (p_channel == BMasterServer.get_server_channel()):
		ui_status_label.set_text("");
		_on_RefreshBtn_pressed();
	return;


func _on_MainMultiplayerBtn_toggled( pressed ):
	#ui_join_dlg.set_visible(pressed);
	
	# show server list
	if (pressed):
		_on_RefreshBtn_pressed();
	return;


func _on_RefreshBtn_pressed():
	BMasterServer.refresh_servers();
	return;
	
# this takes a list of ip's, it will try to connect to them one at a time
# when the list runs out, error is thrown to the user
func _try_connect_to(ip_list):
	BLog.debug("_try_connect_to: " + str(ip_list));

	if (Game.is_connected("connection_failed", self, "_try_connect_to")):
		Game.disconnect("connection_failed", self, "_try_connect_to");
	
	if (ip_list == null || ip_list.size() == 0):
		ui_status_label.set_text("Connection failed");
		Game.emit_signal("game_error", "Connection failed");
		return;
	
	var ip = ip_list[0];
	ip_list.remove(0);	
	
	# check ip, if bad, move to the next ip
	if (not ip.is_valid_ip_address()):
		Game.emit_signal("game_error", "Invalid IPv4 address: %s" % ip);
		_try_connect_to(ip_list);
		return;
	
	Game.connect("connection_failed", self, "_try_connect_to", [ip_list]);
	
	Game.set_password(ui_password.get_text());
	Game.join_game(ip);
	return;


func _on_JoinBtn_pressed():
	var ip_text = ui_join_ip_edit.get_text();
	# store in db
	Database.set_setting(Database.JOIN_ADDRESS, ip_text);
	Database.save();
	var ip_list = ip_text.split(";");
	_try_connect_to(ip_list);
	ui_status_label.set_text("Attempting to join:" + ip_text);
	return;


func _on_ServerList_item_activated():
	_on_ServerList_item_selected();
	_on_JoinBtn_pressed();
	return;


func _on_ServerList_item_selected():
	var item = ui_server_list.get_selected();
	var server_data = item.get_metadata(0);
	var ips = server_data["i"] if server_data.has("i") else []; # LAN IP's followed by internet IPs
	ui_join_ip_edit.set_text(PoolStringArray(ips).join(";"));
	return;


#func _on_LobbyStartBtn_pressed():
#	controller.begin_game(ui_map_widget.get_selected())
#	return;
#
#
#func _on_LobbyCancelButton_pressed():
#	# ui_join_dlg.show();
#	Game.end_game();
#	return;
	
	
func set_visible(vis):
	.set_visible(vis);
