#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var rich_text_label = BUtil.find_child(self, "RichTextLabel");

const MAX_NEWS_ENTRIES = 5;

var rss;
var controller; # this is the main root menu

func apply_to_controller(p_controller):
	controller = p_controller;
	return;
	
func _ready():
	rich_text_label.connect("meta_clicked", self, "_rich_text_label_meta_clicked");
	rss = BRSS.new();
	rss.connect("parse_url_complete", self, "_parse_url_complete");
	rss.parse_url("http://bit-shift.blogspot.com", "/feeds/posts/default/", 80, false);
	return;
	
func _parse_url_complete(entries):
	var text = "";
	var i = 0;
	
	for entry in entries:
		if (i >= MAX_NEWS_ENTRIES):
			break;
			
		i += 1;
		text += "[url=%s]%s[/url]\n" % [entry["link"], entry["title"]];
		
	rich_text_label.set_bbcode(text);
	return;
	
func _rich_text_label_meta_clicked(meta):
	BLog.debug("Open url: " + meta);
	OS.shell_open(meta);
	return;
	
func _on_BitshiftButton_pressed():
	OS.shell_open("http://www.bitshift.io/");

func _on_TwitterButton_pressed():
	OS.shell_open("https://twitter.com/bitshiftio");

func _on_BloggerButton_pressed():
	OS.shell_open("https://bit-shift.blogspot.com/");

func _on_RedditButton_pressed():
	OS.shell_open("https://www.reddit.com/r/bitshift");

