#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This class is the base class for a resource node which is a spatial place that generates or consumes resources
# eg. city, towns, farms, factories etc...
#

extends Spatial
class_name ResourceNode

signal on_mouse_over();
signal on_mouse_leave();

export(String) var display_name = "";

enum TYPE { PRIMARY_RESOURCE, INTERMEDIATE_RESOURCE, CITY, PROP, WAREHOUSE };
export(TYPE) var type = TYPE.PRIMARY_RESOURCE;

export(float) var xp_reduction_per_year = 0.0; # how xp per year for this city to grow? if we do not achieve this we shrink!

export(float) var build_inner_radius = 40; # cant build this close
export(float) var build_outer_radius = 50; # need to build within this radius

export(PackedScene) var level_up_effect;
export(PackedScene) var level_down_effect;

var terminals = [] # list of terminals linked to this node
var trainStations = []
var airports = []

#var ui_info_panel = preload("res://Prefab/UI/ResourceNodeui_info_panel.tscn").instance()
onready var ui_info_panel = BUtil.find_child(self,"Overlay");
onready var ui_icon_template = BUtil.find_child(ui_info_panel, "ResourceIconButtonTemplate");
onready var ui_details_container = BUtil.find_child(ui_info_panel, "DetailsContainer")
onready var ui_xp_progress_bar = BUtil.find_child(ui_info_panel, "XPProgressBar");
onready var ui_xp_label = BUtil.find_child(ui_info_panel, "XPLabel");
onready var ui_xp_container = BUtil.find_child(ui_info_panel, "XPContainer");
onready var ui_name = BUtil.find_child(self,"NameLbl");

var resource_data_list = [];

#onready var mouse_over_audio_pool = Util.getChildByName(self, "MouseOverAudioPool");

onready var xp = BUtil.find_child(self, "XP");

var xp_reduction_timer;

onready var world = WorldMgr.get_world(self);

func _setup_resource_icon(resource_data, container, bottom_display_type):
	if (!resource_data.get_market_resource()):
		return;
		
	var icon = ui_icon_template.duplicate();
	container.add_child(icon);
	icon.set_visible(true);
	icon.init_as_icon(resource_data, bottom_display_type);

# the resource node overlay UI
func _create_overlay_panel():
	#BLog.debug("_create_overlay_panel");
	#add_child(ui_info_panel);
	ui_name.set_text(get_display_name());
	var demandsContainer = ui_details_container.get_node("DemandPanel/DemandsCon");
	var suppliesContainer = ui_details_container.get_node("SupplyPanel/SuppliesCon");
	var resourceDataList = get_resource_data_list();
	
	# free old icons
	var first = true;
	for c in demandsContainer.get_children():
		if (first):
			first = false;
			continue;
			
		c.queue_free();
		
	first = true;
	for c in suppliesContainer.get_children():
		if (first):
			first = false;
			continue;
			
		c.queue_free();
		
	# setup new icons
	var suppliesCount = 0;
	var demandsCount = 0;
	for rd in resourceDataList:
		if (rd.isSupplied):
			_setup_resource_icon(rd, suppliesContainer, ui_icon_template.DisplayType.SUPPLY);
			suppliesCount += 1;
			
		if (rd.isDemanded):
			_setup_resource_icon(rd, demandsContainer, ui_icon_template.DisplayType.DEMAND);
			demandsCount += 1;
	
	ui_details_container.get_node("SupplyPanel").set_visible(suppliesCount > 0);
	ui_details_container.get_node("DemandPanel").set_visible(demandsCount > 0);
		
	# disable focus mode and mouse pass through
	for c in BUtil.get_branches_as_array([ui_info_panel]):
		if (c.is_class("Control")):
			c.focus_mode = Control.FOCUS_NONE;
			c.mouse_filter = Control.MOUSE_FILTER_IGNORE;
			
	return;
		
func _create_resource_data_from_dict(dict : Dictionary):
	var data = preload("res://Resource/ResourceData.tscn").instance();
	data.name = dict.name;
	data.isSupplied = dict.is_supplied;
	data.isDemanded = dict.is_demanded;
	data.consumeAllOnUnload = dict.consume_all;
	data.storageLimit = dict.storage_limit;
	data.supplyRate = dict.supply_rate;
	data.consumeRate = dict.consume_rate;
	return data;
	
func _create_resource_recipe_from_dict(dict : Dictionary):
	if (!dict.has("name") || !dict.has("recipe") || !dict.has("frequency")):
		return null;
		
	var recipe = preload("res://Resource/ResourceRecipe.tscn").instance();
	recipe.name = dict.name;
	recipe.recipe = dict.recipe;
	recipe.frequency = dict.frequency;
	return recipe;
					
func _setup_from_database():
	# 1) look for the node leaf name (lower case) in the database
	var nm = get_name();
	var snake_case = BUtil.camelcase_to_underscore(nm);
	var db_key = "resource_nodes/" + snake_case
	var val = BDatabase.get_value(db_key);
	
	# catch old naming scheme which is wrong!
	var lower_name = nm.to_lower();
	var db_key_temp = "resource_nodes/" + lower_name
	var val_temp = BDatabase.get_value(db_key_temp);
	if (val_temp && db_key_temp != db_key):
		BLog.error("Please rename database key from '" + db_key_temp + "''" + " to '" + db_key + "''");
		return;
	
	# 2) strip any numbers and try again
	if (!val):
		var regex = RegEx.new();
		regex.compile("(\\d+)");
		var number_stripped_name = regex.sub(snake_case, "", true);
		db_key = "resource_nodes/" + number_stripped_name;
		val = BDatabase.get_value(db_key);
		
	# 2) try using the props name converted to underscore notation (snake case)
	if (!val):
		var fname = get_filename();
		var fn = fname.get_file();
		var basename = fn.get_basename();
		var underscore = BUtil.camelcase_to_underscore(basename);
		db_key = "resource_nodes/" + underscore
		val = BDatabase.get_value(db_key);
		
	# out of things to try! bail
	if (!val):
		BLog.error("Couldn't find database key for: " + get_path());
		return;
	
	# create the level nodes
	var levels = [];
	for i in range(0, 3):
		var l = Node.new();
		l.name = "Level" + str(i + 1);
		levels.push_back(l);
		add_child(l);
	
	# now add resources (aka. ingredients)
	if (val.has('resources')):
		var resources = val.resources;
		for r in resources:			
			# check if resource has a 'levels' defined
			if (r.has('levels')):
				for level in r.levels.keys():
					var rlevel = r.levels[level];
					if rlevel == null:
						rlevel = {};
					var rmerged = BUtil.merge_dict(r, rlevel);
					var data = _create_resource_data_from_dict(rmerged);
					if (!data):
						continue;
						
					levels[level-1].add_child(data);
			else:
				# a resource with no levels
				var data = _create_resource_data_from_dict(r);
				if (!data):
					continue;
						
				add_child(data);
		
	# add recipies
	if (val.has('recipes')):
		var recipes = val.recipes;
		for r in recipes:
			# check if resource has a 'levels' defined
			if (r.has('levels')):
				for level in r.levels.keys():
					var rlevel = r.levels[level];
					if rlevel == null:
						rlevel = {};
					var rmerged = BUtil.merge_dict(r, rlevel);
					var data = _create_resource_recipe_from_dict(rmerged);
					if (!data):
						BLog.error("Bad recipie: " + db_key);
						continue;
						
					levels[level-1].add_child(data);
			else:
				# a resource with no levels
				var data = _create_resource_recipe_from_dict(r);
				add_child(data);
	
	return;
	
func _ready():
	if (!world):
		return;
		
	ui_icon_template.get_parent().remove_child(ui_icon_template);
	add_to_group("resource_nodes");
	
	_setup_from_database();
	
	# setup leveling system
	if (xp && type != TYPE.PROP):
		xp.connect("level_changed", self, "_level_changed", [true]);
		xp.connect("xp_changed", self, "_xp_changed");
		_xp_changed(xp);
		_level_changed(xp);
		if (get_tree().is_network_server()):
			if (xp_reduction_per_year > 0):
				var date_time = world.date_time;
				var freq = date_time.yearFrequency(xp_reduction_per_year);
				xp_reduction_timer = Util.create_timer(self, "_xp_reduction_timeout", freq, true);

	# setup the overlay UI
	ui_xp_container.set_visible(false);
	if (type == TYPE.CITY):
		ui_details_container.set_visible(false);
	elif (type == TYPE.PROP):
		ui_info_panel.queue_free();
		ui_info_panel = null;
	else:
		ui_info_panel.set_visible(false);
		
	# Register console commands
	Console.register_command(get_name().to_lower() + ".addxp", {
		method = funcref(self, "add_xp"),
		owner = self,
		description = "Add XP to resource node",
		args = ["int"]
	})
	
	#call_deferred("_post_ready");
	return;
	

func _post_ready():
	var pc = world.master_player;
	if (pc && pc.hud && ui_info_panel && !ui_info_panel.is_queued_for_deletion()):
		pc.hud.add_context_ui(ui_info_panel);
	return;
	
	
func _exit_tree():
	Console.deregister_commands(self)
	
func _xp_reduction_timeout():
	# work out if the resource node is achieving "growth"
	# a resource is growing if:
	# 1. imported products are above 50% - ie. being imported in a timely manner
	# 2. exported products are below 50% - ie. being exported in a timely manner
	
	# for now though, we will just subtract it linearly...
	# you must just overcome this fixed value per timeout
	xp.add_xp(-1.0);
	return;
	
#func get_terminals_belonging_to_company(company):
#	var results = []
#	for t in trainStations:
#		if (t.company == company):
#			results.append(t);
#
#	return results;
	
func get_terminals():
	return trainStations;
	
func get_resource_data(p_name):
	var resourceData = get_resource_data_list()
	for rd in resourceData:
		if (rd.get_name() == p_name):
			return rd
			
	return null;
	
func is_demanded(p_name):
	var resourceData = get_resource_data_list()
	for rd in resourceData:
		if (rd.get_name() == p_name):
			return rd.isDemanded;
			
	return false;
	
func is_supplied(p_name):
	var resourceData = get_resource_data_list();
	for rd in resourceData:
		if (rd.get_name() == p_name):
			return rd.isSupplied;
			
	return false;
	
func get_desc_strings():
	var supplyDesc = ""
	for rd in get_resource_data_list():
		if (rd.isSupplied):
			supplyDesc += " %s: %d" % [rd.get_name(), round(rd.available)]
			
	var demandDesc = ""
	for rd in get_resource_data_list():
		if (rd.isDemanded):
			demandDesc += " %s: %d" % [rd.get_name(), round(rd.available)]
		
	return [supplyDesc, demandDesc]
	
func get_resource_data_list():
	return resource_data_list

func add_train_station(station):
	trainStations.append(station);
	
func remove_train_station(station):
	trainStations.erase(station);
	
func add_airport(airport):
	airports.append(airport)
	
	
# does this respond to mouse over?
func is_interactive(controller):
	if (type == TYPE.PROP):
		return false;
		
	return true;
	

# can this prop be "used" or selected by the player
# ie. are we allowed to use this prop to build on or around
func is_usable(p_controller):
	if (type == TYPE.PROP):
		return false;
		
	return true;


func set_hud_visible(vis):
	if (type == TYPE.CITY):
		ui_info_panel.set_visible(vis);


func show_hud(controller, show):
	if (type == TYPE.PROP):
		return;
		
	ui_xp_container.set_visible(show);
	if (type == TYPE.CITY):
		ui_details_container.set_visible(show);
	else:
		ui_info_panel.set_visible(show);
	
	var supplyPanel = ui_details_container.get_node("SupplyPanel");
	var supplyContainer = ui_details_container.get_node("SupplyPanel");
	supplyPanel.set_custom_minimum_size(supplyContainer.get_size());

	var demandPanel = ui_details_container.get_node("DemandPanel");
	var demandContainer = ui_details_container.get_node("DemandPanel");
	demandPanel.set_custom_minimum_size(demandContainer.get_size());
	return;


func set_mouse_over(controller, mouse_over):
	show_hud(controller, mouse_over);
	
	if (mouse_over):
		emit_signal("on_mouse_over"); # can we connect it via the editor?
		#if (mouse_over_audio_pool):
		#	mouse_over_audio_pool.play();
	else:
		emit_signal("on_mouse_leave");
	return;


func set_selected(controller, selected):
	return false; # do not allow it to be selected


func add_xp(amount : int):
	if (xp):
		xp.add_xp(amount);
	
func _xp_changed(p_xp):
	ui_xp_progress_bar.set_value(p_xp.get_xp());
	ui_xp_label.set_text("L" + str(p_xp.get_level()) + " - " + str(p_xp.get_xp()));

func _level_changed(p_xp, p_spawn_effect = false):
	var level = p_xp.get_level();
	var prev_level = p_xp.get_previous_level();
	
	var level_xp_range = p_xp.get_level_xp_range(level);
	ui_xp_progress_bar.min_value = level_xp_range[0];
	ui_xp_progress_bar.max_value = level_xp_range[1];
	
	# did we level up or down? spawn appropriate effect
	if (p_spawn_effect):
		if (level > prev_level):
			var effect = level_up_effect.instance();
			EventMgr.trigger(EventMgr.Id.RESOURCE_NODE_LEVEL_UP, { "resource_node": self, "display_name": get_display_name() });
			add_child(effect);
		else:
			var effect = level_down_effect.instance();
			EventMgr.trigger(EventMgr.Id.RESOURCE_NODE_LEVEL_DOWN, { "resource_node": self, "display_name": get_display_name() });
			add_child(effect);
	
	_set_visibility_for_level();
	_set_resource_data_for_level();
	_set_resource_recipe_for_level();
	_create_overlay_panel();
	return;
	
func _set_resource_data_for_level():
	var old_resource_data_list = resource_data_list;
	resource_data_list.clear();
	
	var level = xp.get_level();
	var level_node = BUtil.find_child(self, "Level" + str(level));
	if (level_node):
		var level_resource_data_list = BUtil.find_children_by_class_name(level_node, "ResourceData");
		for rd in level_resource_data_list:
			if (!rd.is_queued_for_deletion()):
				resource_data_list.append(rd);
		
	# append global resource data (ie. that are not switched on or off per level)
	var global_resource_data_list = BUtil.find_children_by_class_name(self, "ResourceData", false);
	for rd in global_resource_data_list:
		if (!rd.is_queued_for_deletion()):
			resource_data_list.append(rd);

	#Util.assert(resource_data_list.size(), "Gotta be some resoruces for this level:" + str(level) + ", path:" + get_path());
	# TODO: transfer products from one resource data to the other!
	return;
	
# there may be levels of recipes
# level specific recipes are switched on/off depending on the level
# global recipes shold always be left on
func _set_resource_recipe_for_level():
	var level = xp.get_level();
	var recipes = BUtil.find_children_by_class_name(self, "ResourceRecipe", true);
	for recipe in recipes:
		var recipe_parent = recipe.get_parent();
		var enabled = true if (recipe_parent.get_name() == "Level" + str(level) || recipe_parent == self) else false;
		recipe.set_enabled(enabled);
		
	return;
	
func _set_visibility_for_level():
	# search for a node called "LevelX" and set its visibility
	var level = xp.get_level();
	for l in range(xp.get_min_level(), xp.get_max_level() + 1):
		# turn on new
		var level_node = BUtil.find_child(self, "SpatialLevel" + str(l));
		if (level_node):
			level_node.set_visible(l == level);
	return;
	
func get_display_name():
	if (display_name == "" || display_name == null || display_name == "null"):
		return self.name;
	return display_name;
	
	
