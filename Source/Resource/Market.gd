#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

# contains a list of MarketResource as children

export(float) var resource_value_multiplier = 1.0; # global value multiplier

func _ready():
	var path = get_path();
	add_to_group("market");
	_setup_from_database();
	return;
	
	
func _post_ready():
	return;


func _create_resource_from_dict(key, value):
	var node = Util.load_script_into_node("res://Resource/MarketResource.gd");
	node.name = key;
	node.display_name = value.display_name;
	node.carriage = load(value.carriage);
	node.icon = load(value.icon);
	node.colour = value.color;
	node.maxValueMovement = value.max_value_movement;
	node.maxValue = value.max_value;
	node.minValue = value.min_value;
	return node;


func _setup_from_database():
	var db_market = BDatabase.get_value("market");
	if (!db_market):
		return;
		
	var available_resources = db_market.get("available_resources");
		
	resource_value_multiplier = db_market.resource_value_multiplier;
	var db_resources = db_market.resources;
	for key in db_resources.keys():
		var value = db_resources[key];
		
		# setup the resource availability if "available_resources" is specified in the map, else assume all resources are available
		if (available_resources):
			var available_index = available_resources.find(key);
			var is_available = (available_index != -1);
			if (!is_available):
				continue;
		
		var node = _create_resource_from_dict(key, value);
		add_child(node);
	
	return;
	
# get list of all resource names available
# assumes all children are of MarketResource
func get_resource_names():
	var r = []
	for c in get_children():
		r.append(c.name);
		
	return r;
	
func get_resources():
	return get_children();
	
func get_resource_by_name(name):
	for c in get_children():
		if (c.name.to_lower() == name.to_lower()):
			return c;

	return null;
	
# given an array of resources, either add - if missing, or replace/override - if existing
func add_and_replace_resources(resources):
	for r in resources:
		for c in get_children():
			if (c.name == r.name):
				remove_child(c);
				c.queue_free();
				
	for r in resources:
		add_child(r);
		
	return;
