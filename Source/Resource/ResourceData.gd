#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# A ResourceNode has a list of ResourceData, which describe all the information about the supply/demand of a particular resource
# at the ResourceNode
# it also holds two containers of storage: that which has been imported, and that which is avilable for export
# this separation occurs so that what is generated is not used by recipes at this resource node itself
#
extends Node

signal amount_changed();

export(bool) var isSupplied = false
export(bool) var isDemanded = false
export(bool) var consumeAllOnUnload = false # check this for when you want a trains cargo dumped into oblivion, eg. passengers and mail 
export(float) var supplyRate = 0.0 # units per game year
export(float) var consumeRate = 0.0 # just consume the resource with no output.
export(int) var storageLimit = 10 # determine max vlaues of import_amount and export_amount

onready var world = WorldMgr.get_world(self);
onready var market = world.market if world else null;
onready var market_resource = null

var import_amount = 0.0 # how much has been supplied from another resource, we can use these in cooking
var export_amount = 0.0 # how much has been generated, these are available for export

var demandFrequencyTimer
var supplyFrequencyTimer


func _ready():
	if (!world):
		return;
		
	market_resource = market.get_resource_by_name(get_name())
	if (market_resource == null):
		BLog.error("Resource: %s cant find resource named: %s" % [get_path(), get_name()])
		queue_free();
		return
	
	if (!isDemanded && !isSupplied):
		BLog.error("Resource: %s must be either supplied or demanded!" % [get_path()]);
		
	# esnure there is some storage limit!
	if (isDemanded || isSupplied):
		if (storageLimit <= 0):
			BLog.error("Resource: %s must have some storage!" % [get_path()]);
		
	get_market_resource().register_amount_max(storageLimit)
	
	var resource_demand_supply_rate = BDatabase.get_value("game_settings/resource_demand_supply_rate", 1);
	
	var date_time = world.date_time;
	if (isDemanded && consumeRate > 0):
		var freq = date_time.MONTH_TIME / date_time.yearlyToMonthly(consumeRate * resource_demand_supply_rate)
		demandFrequencyTimer = Util.create_timer(self, "_demand_frequency_timeout", freq, true)
		
	if (isSupplied && supplyRate > 0):
		var freq = date_time.MONTH_TIME / date_time.yearlyToMonthly(supplyRate * resource_demand_supply_rate)
		demandFrequencyTimer = Util.create_timer(self, "_supply_frequency_timeout", freq, true)
	
func _demand_frequency_timeout():
	var reduction = -min(1.0, import_amount);
	add_import_amount(reduction);
	
func _supply_frequency_timeout():
	var increase = min(1.0, storageLimit - export_amount);
	add_export_amount(increase);
	
func add_export_amount(amount):
	if (!get_tree().is_network_server() || amount == 0):
		return
		
	var new_amount = clamp(export_amount + amount, 0, storageLimit);
	if (new_amount == export_amount):
		return;
		
	rpc("_set_export_amount_rpc", new_amount);
	
remotesync func _set_export_amount_rpc(amount):
	var delta = amount - export_amount;
	if (get_market_resource()):
		get_market_resource().add_global_amount(delta);
		
	export_amount = amount;
	emit_signal("amount_changed");
	
func get_export_amount():
	return export_amount;
	
func add_import_amount(amount):
	if (!get_tree().is_network_server() || amount == 0):
		return;
		
	var new_amount = clamp(import_amount + amount, 0, storageLimit);
	if (new_amount == import_amount):
		return;
	
	rpc("_set_import_amount_rpc", new_amount);
		
remotesync func _set_import_amount_rpc(amount):
	var delta = amount - import_amount;
	if (get_market_resource()):
		get_market_resource().add_global_amount(delta);
		
	import_amount = amount;
	emit_signal("amount_changed");
	
func get_import_amount():
	return import_amount;
	
func get_market_resource():
	return market_resource
