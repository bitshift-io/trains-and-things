#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var trif_exp = Util.compile_regex("trif(.*)then(.*)");
var if_exp = Util.compile_regex("if(.*)then(.*)");
var bool_exp = Util.compile_regex("(.*?)(or|and)");
var func_exp = Util.compile_regex("(.*)\\((.*)\\)");
var equal_exp;
var valid_variable_exp = Util.compile_regex("(^\\d)|(\\s)");

var variables = [];
var output: String = "";
var tabs = 0;
var obj;
var verbose = false; # set to true for debugging
var errors = 0;

# this Variable class allows us to pass by reference
# without it we would be pasing around by value which would complicate things
# immensely!
# it also provides is to do operator overloading if so required
# it also provides transaction support
class Variable:
	var import_amount;
	var export_amount;
	
	var name: String;
	var resource_data;
	
	func _init(p_name):
		name = p_name;
		reset();
		
	func reset():
		import_amount = [0.0];
		export_amount = [0.0];
	
	func operator_equals(p_value):
		BLog.error("Not yet implemented: operator_equals");
		return;
		
	func operator_multiply(p_value):
		BLog.error("Not yet implemented: operator_multiply");
		return;

	func operator_add(p_value):
		BLog.error("Not yet implemented: operator_add");
		return;
		
	func operator_subtract(p_value):
		BLog.error("Not yet implemented: operator_subtract");
		return;
		
	func transaction_begin():
		import_amount.push_back(import_amount[-1]);
		export_amount.push_back(export_amount[-1]);
		
	func transaction_revert():
		import_amount.pop_back();
		export_amount.pop_back();
		
	func transaction_commit():
		var ia = import_amount.pop_back();
		var ea = export_amount.pop_back();
		import_amount[-1] = ia;
		export_amount[-1] = ea;
		
	func get_import_amount():
		return import_amount.back();
		
	func get_export_amount():
		return export_amount.back();
		
	func set_import_amount(p_amount):
		import_amount[-1] = p_amount;
		
	func set_export_amount(p_amount):
		export_amount[-1] = p_amount;
		
		

var logic_operators = {
	"and": {
		"operator": "and",
		"regex": "and",
		"type": "logic"
	},
	"or": {
		"operator": "or",
		"regex": "or",
		"type": "logic"
	}
};

var operators = {
	"=": {
		"operator": "=",
		"regex": "=",
		"type": "assignment",
		"function": "operator_equals"
	},
	"*=": {
		"operator": "*=",
		"regex": "\\*=",
		"type": "assignment",
		"function": "operator_multiply"
	},
	"+=": {
		"operator": "+=",
		"regex": "\\+=",
		"type": "assignment",
		"function": "operator_add"
	},
	"-=": {
		"operator": "-=",
		"regex": "-=",
		"type": "assignment",
		"function": "operator_subtract"
	},
	"==": {
		"operator": "==",
		"regex": "==",
		"type": "boolean"
	},
	">=": {
		"operator": ">=",
		"regex": ">=",
		"type": "boolean"
	},
	"<=": {
		"operator": "<=",
		"regex": "<=",
		"type": "boolean"
	},
	"!=": {
		"operator": "!=",
		"regex": "!=",
		"type": "boolean"
	},
	">": {
		"operator": ">",
		"regex": ">",
		"type": "boolean"
	},
	"<": {
		"operator": ">",
		"regex": "<",
		"type": "boolean"
	}
};

func _init():
	# compile a regex from the equal_operators
	var operator_list = PoolStringArray();
	for operator_key in operators:
		operator_list.push_back(operators[operator_key].regex);
	
	var operator_str = operator_list.join("|");
	equal_exp = Util.compile_regex("(.*?)\\s*(" + operator_str + ")\\s*(.*)");
	return;
	
	
func evaluate(p_inputs: Dictionary, p_proxy: Node) -> void:
	obj.eval(p_inputs, p_proxy);
	return;


func compile(p_script: String):
	if (verbose):
		BLog.debug("Convert from:");
		BLog.debug(p_script);
	
	tabs = 0;
	
	# now wrap the transpiled code into a function we can call
	output += tab_line("func eval(p_inputs: Dictionary, p_proxy: Node) -> void:");
	tabs = 1; # because all our code is going into a single function
	
	var lines = p_script.split("\n");
	for line in lines:
		parse_line(line);
		
	tabs = 0;

	# patch in special variables
	var variables_string_pool_array = PoolStringArray();
	for variable in variables:
		variables_string_pool_array.push_back(variable);
		
	output = tab_line("extends Node\n") + tab_line("var variable_names = [\"" + variables_string_pool_array.join("\", \"") + "\"]\n") + output;
		
	if (verbose):
		BLog.debug("TO:");
		BLog.debug(output);
	
	if (errors > 0):
		BLog.error("Failed to compile recipe: " + p_script);
		return null;
		
	var script = GDScript.new();
	script.source_code = output;
	var err = script.reload();
	if (err != OK):
		BLog.error("Failed to compile recipe: " + p_script);
		return null;
	
	obj = Node.new();
	obj.set_script(script);
	return obj;

func parse_line(p_line: String):
	var line = p_line.strip_edges();
	if (line.length() <= 0):
		return;
		
	# lets classify the type of line ...
	
	# is it an if then statement?
	var result = trif_exp.search(line);
	if result:
		handle_trif_exp(result);
		return;
		
	# is it an if then statement?
	result = if_exp.search(line);
	if result:
		handle_if_exp(result);
		return;  

	# assume it is a normal line
	parse_comma_block(line);
	return;

func parse_comma_block(p_text: String):
	var splits = split_top_level_braces_aware(p_text, ",");
	for split in splits:
		var result = parse_basic_component(split);
		output += tab_line(result);
		
	return;
		
# parse something like:
# x = 1
# x *= 2
# or a function:
# fn(cheese, 2)
# 
# these are basic componenets which can be broken down any further
func parse_basic_component(p_text: String):
	# is it something with an = in it?
	var result = equal_exp.search(p_text);
	if result:
		return handle_equal_exp(result);
		
	# is it a function call?
	result = func_exp.search(p_text);
	if result:
		return handle_func_exp(result);
		
	return p_text.strip_edges();
		
		
func register_variable(p_name):
	if (variables.has(p_name)):
		return;
		
	# ensure valid variable name in use!
	var result = valid_variable_exp.search(p_name);
	if (result):
		BLog.error("Variable does not conform to a compilable variable name: " + p_name);
		errors += 1;
		return;
	
	variables.push_back(p_name);
	
	
func handle_equal_exp(p_match):
	var line = p_match.get_string(0);
	var pre_component = p_match.get_string(1).strip_edges();
	var operator_component = p_match.get_string(2);
	var post_component = p_match.get_string(3).strip_edges();

	var operator = operators[operator_component];

	var result = "";
	if (post_component.is_valid_float()): # post_component is a number...
		result = "p_inputs." + pre_component + "." + operator.function + "(" + post_component + ")";
	else: # post_component is a variable...
		result = "p_inputs." + pre_component + "." + operator.function + " p_inputs." + post_component + ".value";
		register_variable(post_component);

	register_variable(pre_component);
	return result;

func handle_func_exp(p_match):
	var line = p_match.get_string(0);
	var name_component = p_match.get_string(1);
	var params_component = p_match.get_string(2);
	var arguments = params_component.split(",");
	
	var arg_texts = PoolStringArray();
	for argument in arguments:
		# is the post component a number or some other variable?
		var num_value = float(argument);
		if (num_value == 0 && (argument != "0" || argument != "0.0")):
			num_value = null;
			
		if (num_value):
			arg_texts.push_back(argument);
		else:
			arg_texts.push_back("p_proxy.get_variable(p_inputs, \"" + argument + "\")");
			register_variable(argument);
	
	var result = "p_proxy." + name_component + "(" + arg_texts.join(", ") + ")";
	return result;
	
func tab():
	var result = "";
	for t in range(0, tabs):
		result += "\t";
		
	return result;
	
	
func tab_line(p_str: String):
	return tab() + p_str + "\n";
		
		
func parse_expression_tree(p_str: String):
	var actual_str = p_str.strip_edges();
	
	# remove outer most braces if they exist
	if (actual_str.begins_with("(") && actual_str.ends_with(")")):
		actual_str = actual_str.substr(1, actual_str.length() - 2);
		
	var sz = actual_str.length();
	var brace_idx = 0;
	var idx = 0;
	
	while idx < sz:
		var ch = actual_str[idx];
		
		if (brace_idx == 0):
			var right = actual_str.right(idx);
			var logic_keys = logic_operators.keys();
			for logic_key in logic_keys:
				if (right.begins_with(logic_key)):
					var left = actual_str.left(idx);
					right = actual_str.right(idx + logic_key.length());
					
					var left_node = parse_expression_tree(left);
					var right_node = parse_expression_tree(right);
					return {
						"left_node": left_node,
						"right_node": right_node,
						"operator": logic_key
					};
			
		if (ch == "("):
			brace_idx += 1;
			
		if (ch == ")"):
			brace_idx -= 1;
			
		idx += 1;
		
	return {
		"str": actual_str
	};
	
func append_to_output_from_expression_tree(p_node):
	if (p_node.has("left_node")):
		output += "(";
		append_to_output_from_expression_tree(p_node.left_node);
		
	if (p_node.has("operator")):
		output += " " + p_node.operator + " ";
		
	if (p_node.has("right_node")):
		append_to_output_from_expression_tree(p_node.right_node);
		output += ")";
		
	if (p_node.has("str")):
		output += parse_basic_component(p_node["str"]);
		
	return;
	

func handle_if_exp(p_match):
	var line = p_match.get_string(0);
	var if_component = p_match.get_string(1);
	var then_component = p_match.get_string(2);
	
	var node = parse_expression_tree(if_component);
		
	output += tab() + "if ";
	append_to_output_from_expression_tree(node);
	output += ":\n";
	
	tabs += 1;
	parse_comma_block(then_component);
	tabs -= 1;
	return;
	

# transactional if support
func handle_trif_exp(p_match):
	var line = p_match.get_string(0);
	var if_component = p_match.get_string(1);
	var then_component = p_match.get_string(2);
		
	var node = parse_expression_tree(if_component);
	
	output += tab_line("p_proxy.transaction_begin(p_inputs)");
	output += tab() + "if ";
	append_to_output_from_expression_tree(node);
	output += ":\n";
	
	tabs += 1;
	output += tab_line("p_proxy.transaction_commit(p_inputs)");
	parse_comma_block(then_component);
	tabs -= 1;
	
	output += tab_line("else:");
	tabs += 1;
	output += tab_line("p_proxy.transaction_revert(p_inputs)");
	tabs -= 1;
	
	return;


# given a string:
# qwe, bob (qwe, qwe, qwe)
# split only where p_split is outside of any braces
func split_top_level_braces_aware(p_str: String, p_split: String):
	var search_str = p_str;
	var sz = search_str.length();
	var brace_idx = 0;
	var idx = 0;
	
	var results = [];
	
	while idx < sz:
		var ch = search_str[idx];
		
		if (brace_idx == 0):
			var right = search_str.right(idx);
			if (right.begins_with(p_split)):
				var left = search_str.left(idx);
				search_str = search_str.right(idx + p_split.length());
				sz = search_str.length();
				idx = 0;
				results.push_back(left);
			
		if (ch == "("):
			brace_idx += 1;
			
		if (ch == ")"):
			brace_idx -= 1;
			
		idx += 1;
		
	results.push_back(search_str);
	return results;
