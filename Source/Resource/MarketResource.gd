#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

export(Texture) var icon = null
export(Color) var colour = Color(0, 0, 0) # colour, for plotting in the market chart
export(PackedScene) var carriage = null # so the carriage knows what to show when carrying this product

# resource name
export(String) var display_name = null

# current prices (or starting prices)
var value = null # 500.0	# price per carriage

# clamp the upper and lower limits of buying and selling
export(float) var maxValue = 1000
export(float) var minValue = -10

# the global supply and demand affect buy and sell values
# more supply pushes down the price - only if demand exists
var globalAmount = 0.0
var globalAmountMax = 0.0

export(float) var maxValueMovement = 1.0 # max change in price per minute

# periodic history of prices for plotting into a graph
var valueHistory = []
var isUsedInScene = false # is this resource used in this map?

const MaxHistoryLength = 100 # TODO: implement this

onready var world = WorldMgr.get_world(self); 

# anyone that can store this product should register the maximum amount they can hold
# so we can get an idea of the saturation point of the market when its full
func register_amount_max(units):
	globalAmountMax += units
	isUsedInScene = true
	
func get_value(units):
	return value * units * get_market().resource_value_multiplier;
	
func add_global_amount(delta):
	globalAmount += delta;
	
func get_market():
	return get_parent();
		
func _ready():
	if (!world):
		return;
		
	if (!display_name):
		display_name = get_name();
	
	Util.assert(display_name, "Resource has invalid name") # at least need a name set!
	if (colour == null):
		BLog.debug("MarketResource %s has no colour set, setting to black" % display_name)
	
	world.date_time.connect("month_changed", self, "_on_month_changed")
	
	# enable the RenderTexture node in the scene 
	var renderTextureNode = BUtil.find_child(self, "RenderTexture")
	if (renderTextureNode):
		renderTextureNode.show()
		
	# set value to be half way to start
	value = (maxValue - minValue) * 0.5 + minValue
	valueHistory.append(value)
	valueHistory.append(value) # need two points to make a graph
	
# assumes child 0 is a viewport with render texture setup
func get_icon():
	if (icon):
		return icon
#		
	var c0 = BUtil.find_child(self, "Viewport")
	if (!c0):
		return null
	
	return c0.get_render_target_texture()
	
func used_in_scene():
	return isUsedInScene

func _on_month_changed(monthIndex):
	# no max, no usage of this product at all on this map
	if (!used_in_scene()):
		return
		
	add_value()
	valueHistory.append(value)
	while (valueHistory.size() > MaxHistoryLength):
		valueHistory.pop_front()
	
func add_value():		
	if (!get_tree().is_network_server()):
		return

	Util.assert(globalAmountMax > 0, "Resource has no global max")
	var movement = rand_range(0, maxValueMovement)
	var globalAmountPct = clamp(globalAmount /  globalAmountMax, 0.0, 1.0)
	var minMaxValueDifference = maxValue - minValue
	var idealValue = ((1.0 - globalAmountPct) * minMaxValueDifference) + minValue
#	
#	# first time set the price to the ideal
#	if (value == null):
#		value = idealValue
#		movement = 0
#		
#	# we need to move the value upwards
#	if (idealValue > value):
#		value += movement
#	# move it downwards
#	else:
#		value -= movement

	value = idealValue
		
	rpc_unreliable("set_value_rpc", value)

remote func set_value_rpc(p_value):
	#print("setValueRPC: ", value)
	value = p_value
