#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# A ResourceNode has a list of ResourceRecipe, which is the foumrla for taking imported products and generating products
# for export
#
# a recipe would like like this in a typical programming language:
#	if can_consume(mail, 1) and can_supply(people, 1) then consume(mail, 1), supply(people, 1)
#
# which check first that it can do certain operations, then it does them
# but this is quite verbose.
# The same code can be written in a transactional way (think SQL transcations):
#	trif consume(mail, 1) and supply(people, 1) then return
#
# notice the 'tr' prefix means 'transactional if', this will allow the if expression to do the work
# and the result of the if statement determines if the transaction is commited or reverted.
#
extends Node

var RecipeCompiler = load("res://Resource/RecipeCompiler.gd");

export(String) var recipe = null

var enabled = true;

export(float) var frequency = 1.0  # times per game year

var frequencyTimer = null;
var compiled_recipe: Node;

onready var world = WorldMgr.get_world(self);

var resource_node;
	
func _ready():
	if (!world):
		return;
		
	compile();
	if (!compiled_recipe):
		return;
	
	resource_node = BUtil.find_parent_by_class_name(self, "ResourceNode");
	
	var resource_demand_supply_rate = BDatabase.get_value("game_settings/resource_demand_supply_rate", 1);
	
	var date_time = world.date_time;
	var freq = date_time.MONTH_TIME / date_time.yearlyToMonthly(frequency * resource_demand_supply_rate)
	frequencyTimer = Util.create_timer(self, "frequency_timeout", freq, true)
	

func compile():
	if (recipe == null):
		return null;
		
	var compiler = RecipeCompiler.new();
	compiled_recipe = compiler.compile(recipe);
	compiler.free();
	
	if (!compiled_recipe):
		BLog.error("Failed to compile recipe in " + get_path());
		
	return compiled_recipe;


# check the ingeredients to ensure they are matching what the script will use
func valid_ingredients(p_ingredients, p_display_errors = false) -> bool:
	var variable_names = compiled_recipe.variable_names;
	for variable_name in variable_names:
		if (!p_ingredients.has(variable_name)):
			if (p_display_errors):
				var p = get_path();
				BLog.error("Missing ingredient '" + variable_name + "' means recipe cannot run, check you have the correct ResourceData by this name. Path: " + p);
			return false;
			
	return true;
	
	
func eval(p_ingredients):
	compiled_recipe.eval(p_ingredients, self);
	

func apply_ingredient_changes(p_ingredients):
	# apply the changes
	for key in p_ingredients:
		var variable = p_ingredients[key];
		var resource_data = variable.resource_data;
		var import_amount = variable.get_import_amount();
		var export_amount = variable.get_export_amount();
		resource_data.add_import_amount(import_amount);
		resource_data.add_export_amount(export_amount);
		variable.reset();
	
	return;

		
func frequency_timeout():
	if (!enabled):
		return;
		
	# run the recipe!
	var ingredients = {};
	
	var resource_data = resource_node.get_resource_data_list();
	for rd in resource_data:
		var variable = RecipeCompiler.Variable.new(rd.get_name().to_lower());
		variable.resource_data = rd;
		ingredients[rd.get_name().to_lower()] = variable;
		#print(rd.get_name().to_lower());

	eval(ingredients);
	apply_ingredient_changes(ingredients);
	return;
		
func get_resource(name):
	var res_node = BUtil.find_parent_by_class_name(self, "ResourceNode");
	var resource = res_node.get_resource_data(name)
	# check we have access to this
	if (!resource):
		BLog.error("Looking for resource '%s' in '%s' not found! '%s' is not valid recipe. Disabling recipe." % [name, resource_node.get_path(), get_name()]);
		set_enabled(false);
		
	return resource
	
func set_enabled(p_enabled):
	enabled = p_enabled;
		
#
# proxy methods
#
func get_variable(p_ingredients, p_name):
	if (!p_ingredients.has(p_name)):
		return null;
		
	return p_ingredients[p_name];
	
	
func can_consume(p_variable, p_amount) -> bool:
	if (!p_variable):
		return false;
		
	# import_amount is the current amount, value the delta at this stage
	var amount_available = p_variable.resource_data.import_amount + p_variable.get_import_amount();
	if (amount_available < p_amount):
		return false;
		
	return true;
	
	
func consume(p_variable, p_amount) -> bool:
	if (!can_consume(p_variable, p_amount)):
		return false;
		
	p_variable.set_import_amount(p_variable.get_import_amount() - p_amount);
	return true;
	
	
func can_supply(p_variable, p_amount) -> bool:
	if (!p_variable):
		return false;
		
	var storage_limit = p_variable.resource_data.storageLimit; 
	var amount_available = p_variable.resource_data.export_amount + p_variable.get_export_amount();
	var storage_space_left = storage_limit - amount_available;
	if (storage_space_left < p_amount):
		return false;
		
	return true;
	
	
func supply(p_variable, p_amount) -> bool:
	if (!can_supply(p_variable, p_amount)):
		return false;
		
	p_variable.set_export_amount(p_variable.get_export_amount() + p_amount);
	return true;
	

func transaction_begin(p_inputs: Dictionary):
	for key in p_inputs:
		p_inputs[key].transaction_begin();
	return;

	
func transaction_commit(p_inputs: Dictionary):
	for key in p_inputs:
		p_inputs[key].transaction_commit();
	return;


func transaction_revert(p_inputs: Dictionary):
	for key in p_inputs:
		p_inputs[key].transaction_revert();
	return;
