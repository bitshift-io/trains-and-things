#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# https://wiki.unrealengine.com/C%2B%2B_Mutator_Tutorial
# https://wiki.factorio.com/User:Adil/Modding_tutorial

extends Node

export(String) var display_name = "";
export(String) var version = "";
export(String, MULTILINE) var description = "";
export(String) var game_version = "";
export(String) var website = "";
export(String, MULTILINE) var authors = "";
export(String, MULTILINE) var dependencies = "";
export(int) var mod_id = "";
export(bool) var enabled_by_default = false;

var enabled = false; # is the mod running?

func set_enabled(e):
	enabled = e;
	return;
	
func is_enabled():
	return enabled;
	
# description to show in the mod menu
func get_description():
	var desc = description;
	
	if (website.length()):
		desc += "\n\n[color=aqua]Website:[/color] [url=" + website + "]" + website + "[/url]";
	
	if (authors.length()):
		desc += "\n\n[color=aqua]Author(s):[/color]\n" + authors ;
		
	if (dependencies.length()):
		desc += "\n\n[color=aqua]Dependencies:\n[/color]" + dependencies;
		
	return desc;
	
# first method called on the mod, this will happen as the mod menu is loaded
func _ready():
	set_enabled(enabled_by_default);
	
	# TODO: check dependencies?
	return;
	
## the game has begin, instance what needs instancing?
#func instance_mod():
#	if (!is_enabled() || !mod_scene):
#		return;
#
#	mod_instance = mod_scene.instance();
#	mod_instance.mod_descriptor = self;
#	get_tree().get_root().add_child(mod_instance);
#	return mod_instance;
#
#func destroy_mod():
#	if (mod_instance):
#		mod_instance.queue_free();
#		mod_instance = null;
#
#	return;
	
func get_map_descriptor_list():
	return BUtil.find_children_by_class_name(self, "MapDescriptor");

func get_mod_descriptor_list():
	return BUtil.find_children_by_class_name(self, "ModDescriptor");
	
func get_descriptors():
	return BUtil.join_array(get_map_descriptor_list(), get_mod_descriptor_list());
