#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# https://wiki.unrealengine.com/C%2B%2B_Mutator_Tutorial
# https://wiki.factorio.com/User:Adil/Modding_tutorial

extends Node

export(String) var display_name = "";
export(int) var map_size;
export(String) var recommended_player_count = "";
export(String) var version = "";
export(String, MULTILINE) var description = "";
export(String) var game_version = "";
export(String) var website = "";
export(String, MULTILINE) var authors = "";
export(String, MULTILINE) var dependencies = "";
export(bool) var enabled_by_default = true;
export(Texture) var thumbnail;

export(bool) var available_in_singleplayer = true; # set to true for campaigns that don't support MP
export(bool) var available_in_multiplayer = true; #

# https://github.com/godotengine/godot/issues/15961
export(String, FILE, "*.tscn") var scene_path;
#export(PackedScene) var scene; # the scene to instance

var enabled = false; # is the mod running?

func set_enabled(e):
	enabled = e;
	return;
	
func is_enabled():
	return enabled;
	
# description to show in the mod menu
func get_description():
	var desc = description;
	
	if (website.length()):
		desc += "\n\n[color=aqua]Website:[/color] [url=" + website + "]" + website + "[/url]";
	
	if (authors.length()):
		desc += "\n\n[color=aqua]Author(s):[/color]\n" + authors ;
		
	if (dependencies.length()):
		desc += "\n\n[color=aqua]Dependencies:\n[/color]" + dependencies;
		
	return desc;
	
func get_display_name():
	return display_name;
	
# first method called on the mod, this will happen as the mod menu is loaded
func _ready():
	set_enabled(enabled_by_default);
	
	# TODO: check dependencies?
	return;


