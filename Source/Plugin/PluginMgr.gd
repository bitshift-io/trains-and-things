#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
#
# wrapper for BPluginManager to add game specific features
#
extends Node

func _init():
	# setup mod and map lists
	# this will handle zip files and pck files in these directories
	# plugin mgr expects a zip to have full paths
	# eg.
	# 	Maps/Hawaii/Plugin
	#
	BPluginMgr.scan_directory("res://Maps");
	BPluginMgr.scan_directory("res://Mods");
	return;

func get_map_descriptors():
	var descriptors = [];
	
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
		var descriptor_list = plugin_desc.get_map_descriptor_list();
		for descriptor in descriptor_list:
			descriptors.append(descriptor);
			
	return descriptors;
	
func get_mod_descriptors():
	var descriptors = [];
	
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
		var descriptor_list = plugin_desc.get_mod_descriptor_list();
		for descriptor in descriptor_list:
			descriptors.append(descriptor);
			
	return descriptors;
	
	
func find_plugin_by_mod_id(p_mod_id: int):
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
		var plugin_id = plugin_desc.mod_id;
		if (typeof(plugin_id) == TYPE_STRING):
			plugin_id = int(plugin_id);
			
		if (plugin_id == p_mod_id):
			return plugin_desc;
			
	return null;
	
# get mod list
func get_mods():
	var mods = [];
	var descriptors = get_mod_descriptors();
	for desc in descriptors:
		if (desc.instance):
			mods.append(desc.instance);
			
	return mods;
			
func instance_mods():
	var descriptors = get_mod_descriptors();
	for desc in descriptors:
		desc.instance_mod();
				
	return;
	
func destroy_mods():
	var descriptors = get_mod_descriptors();
	for desc in descriptors:
		desc.destroy_mod();
		
	return;
	
func get_map_resource_path(descriptor_name):
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
			
		var descriptor_list = plugin_desc.get_map_descriptor_list();
		for descriptor in descriptor_list:
			if (descriptor.display_name == descriptor_name):
				return descriptor.scene_path;
				
	return null;
	
# given a scene_path, get the map descriptor
func get_map_descriptor_from_scene_path(p_scene_path):
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
			
		var descriptor_list = plugin_desc.get_map_descriptor_list();
		for descriptor in descriptor_list:
			if (descriptor.scene_path == p_scene_path):
				return descriptor;
				
	return null;
	
	
func get_plugin_from_descriptor(p_descriptor):
	var plugins = BPluginMgr.get_plugins();
	for plugin_path in plugins:
		var plugin_desc = plugins[plugin_path];
		var descriptor_list = plugin_desc.get_descriptors();
		for descriptor in descriptor_list:
			if (descriptor == p_descriptor):
				return plugin_desc;

	return null;
			
				
func delete_plugin(p_plugin):
	return BPluginMgr.delete_plugin(p_plugin);
	
		
		
