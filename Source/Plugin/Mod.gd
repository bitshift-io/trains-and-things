#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# https://wiki.unrealengine.com/C%2B%2B_Mutator_Tutorial
# https://wiki.factorio.com/User:Adil/Modding_tutorial

extends Node

var mod_descriptor; # the mod descriptor associated with this mod

func _ready():
	return;
	
func modify_attachment_list(controller, attachment_resource_list):
	return attachment_resource_list;
	
func modify_load_resource_path(owner, resource_path):
	return resource_path;
	
func modify_load_resource(owner, resource):
	return resource;
