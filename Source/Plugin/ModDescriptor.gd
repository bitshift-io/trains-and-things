#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# https://wiki.unrealengine.com/C%2B%2B_Mutator_Tutorial
# https://wiki.factorio.com/User:Adil/Modding_tutorial

extends Node

export(String) var display_name = "";
export(String) var version = "";
export(String, MULTILINE) var description = "";
export(String) var game_version = "";
export(String) var website = "";
export(String, MULTILINE) var authors = "";
export(String, MULTILINE) var dependencies = "";
export(bool) var enabled_by_default = false;
export(Texture) var thumbnail;

export(String, FILE, "*.tscn") var scene_path;
#export(PackedScene) var scene; # the scene to instance when beggning a mod... do i even need this? is extending this enough?

var enabled = false; # is the mod running?
var menu_controller; # the menu system

var instance;

func set_enabled(e):
	enabled = e;
	return;
	
func is_enabled():
	return enabled;
	
# first method called on the mod, this will happen as the mod menu is loaded
func _ready():
	set_enabled(enabled_by_default);
	
	# TODO: check dependencies?
	return;
	
func get_display_name():
	return display_name;
	
# the game has begin, instance what needs instancing?
func instance_mod():
	if (!is_enabled() || !scene_path):
		return;
		
	instance = load(scene_path).instance();
	instance.mod_descriptor = self;
	Util.get_tree().get_root().add_child(instance);
	return instance;
	
func destroy_mod():
	if (instance):
		instance.queue_free();
		instance = null;
		
	return;
	
func set_menu_controller(c):
	menu_controller = c;
