#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "PlayerState.gd"

var resourceNode = preload("res://Resource/ResourceNode.gd").new();

enum Flags { FLAG_EDIT, FLAG_CLONE };

var leftMousePressed = false
var rightMousePressed = false
var mouseOverCollider = null

var vehicle_route = preload("res://Buildable/VehicleRoute.gd").new();

# these are invisible settings to allow us to determine in advance how much a train will cost to put together
var trainInst;
var trainCarriageInst;

var _edit_vehicle; # the vehicle we are editing

var ignore_max_carriage = false; # a flag to help us configure trains when we edit them
var auto_unload = true; # attempt to auto unload a product at the next station?

export(bool) var available_in_demo = false; # can we use this in the demo?

export(String) var globalsNodeName;
export(String) var networkNamePrefix;
export(String) var selectableClassName;

export(PackedScene) var vehicleTemplate;
export(PackedScene) var carriageTemplate;

export(NodePath) var button_path;
export(NodePath) var dialog_path;

onready var button = get_node(button_path);
onready var dialog = get_node(dialog_path);

onready var ui_route_list = BUtil.find_child(dialog, "RouteList");
onready var okButton = BUtil.find_child(dialog, "OkButton");
onready var cancelButton = BUtil.find_child(dialog, "CancelButton");
onready var ui_resource_node_template = BUtil.find_child(dialog, "TerminalTemplate");
onready var ui_helper_tip = BUtil.find_child(dialog, "HelperTip");
onready var ui_vehicle_name = BUtil.find_child(dialog, "VehicleNameEdit");
onready var ui_random_name_btn = BUtil.find_child(dialog, "RandomNameButton");
onready var ui_purchase_cost = BUtil.find_child(dialog, "PurchaseCostLbl");
onready var ui_maintenance_cost = BUtil.find_child(dialog, "MaintenanceCostLbl");
onready var ui_cost_panel = BUtil.find_child(dialog, "CostPanel");

var name_list = [];
var format_list = [];

onready var world = WorldMgr.get_world(self);
	
	
func apply_to_controller(controller):
	controller.add_state(self);
	return;
	
	
# convenience functions for mods: set a vehicle template
# and setup an instance of it ready for duplication
func set_vehicle_template(template):
	vehicleTemplate = template;
	if (trainInst):
		trainInst.queue_free();
		
	trainInst = vehicleTemplate.instance();
	trainInst.set_as_building_template();
	# add and then remove just so _ready has been called
	add_child(trainInst);
	remove_child(trainInst);
	Util.assert(trainInst.get_finances() != null, "Train has no finances");
	return;
	
	
func _ready():
	#set_network_mode(NETWORK_MODE_MASTER if get_tree().is_network_server() else NETWORK_MODE_SLAVE);
	set_network_master(1);
	
	controller.hud.add_state_button(button, "Train");
	controller.hud.add_right_panel(dialog);

	ui_resource_node_template.get_parent().remove_child(ui_resource_node_template); # remove from parent

	set_vehicle_template(vehicleTemplate);
	
	if (carriageTemplate != null):
		trainCarriageInst = carriageTemplate.instance();	
		trainCarriageInst.set_as_building_template();
		# add and then remove just so _ready has been called
		add_child(trainCarriageInst);
		remove_child(trainCarriageInst);
		Util.assert(trainCarriageInst.get_finances() != null, "Train has no finances");
		
	# parse names from database
	name_list = BDatabase.get_value("vehicle_names/name_list");
	format_list = BDatabase.get_value("vehicle_names/format_list");
	
	ui_random_name_btn.set_button_icon(ui_random_name_btn.get_icon("Reload", "EditorIcons"));
	Util.set_click_mask_from_normal_alpha(button);
	return;


func state_input(event):		
	if (event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed):
		leftMousePressed = true
		
	if (event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.pressed):
		rightMousePressed = true
		
	return;
	
	
func state_enter(prevState):
	_edit_vehicle = null;
	okButton.set_disabled(true);
	controller.clear_mouse_over();
	controller.mouse_over_enabled = false;
	controller.context_menu_enabled = false;
	show_dialog();
		
		
func state_exit(nextState):
	controller.mouse_over_enabled = true
	controller.context_menu_enabled = true;
	
	if (mouseOverCollider):
		mouseOverCollider.set_mouse_over(controller, false);
		
	dialog.set_visible(false);
	return;
	
	
func is_usable(resource_node):
	# no resource_node yet selected
	if (ui_helper_tip.get_parent()):
		return true;
		
	var first_resource_node = ui_route_list.get_child(0).resource_node;
	if (resource_node == first_resource_node):
		return true;
		
	if (!resource_node.is_usable(controller)):
		return false;
		
	# valid route exists for the train to get to the resource_node?
	var start_terminals = first_resource_node.get_terminals();
	var this_terminals = resource_node.get_terminals();
	for st in start_terminals:
		for tt in this_terminals:
			if (st.find_astar_paths(tt).size()):
				return true;
	
	return false;
	
	
# Update physics processing.
func state_physics_process(delta):
	# check if the user is in the train dialog
	update_ui();
		
	var mouseOverColliderThisFrame = null
	var collisions = controller.mouse_pick()
	for col in collisions:
		var collider = col["collider"]
		var resource_node = BUtil.find_parent_by_class_name(collider, selectableClassName)
		if (resource_node != null):
			# do not allow user to select a resource_node if we don't have a valid route to this resource_node
			if (is_usable(resource_node)):
				# dont allow the user to set the same resource_node twice
				if (vehicle_route.size() <= 0 || vehicle_route.get_last_resource_node() != resource_node):
					mouseOverColliderThisFrame = resource_node
					if (mouseOverColliderThisFrame):
							mouseOverColliderThisFrame.set_mouse_over(controller, true)
					
	if (mouseOverCollider != mouseOverColliderThisFrame):
		if (mouseOverCollider):
			mouseOverCollider.set_mouse_over(controller, false)
		mouseOverCollider = mouseOverColliderThisFrame;
			
	# user clicks, so lock thier mouse over collider
	if (leftMousePressed):
		leftMousePressed = false # handled the event
		
		if (mouseOverCollider):
			add_resource_node(mouseOverCollider);

	return;


func get_company():
	return controller.get_company();
	
	
func update_ui():
	var purchase_cost = get_purchase_cost(get_max_carriages());
	get_company().update_purchase_button(okButton, purchase_cost, ui_route_list.get_child_count() >= 2);
	
	# update costing
	ui_purchase_cost.set_text(Util.format_currency(purchase_cost));
	var maintenance_cost = get_maintenance_cost(get_max_carriages());
	ui_maintenance_cost.set_text(Util.format_currency(maintenance_cost));
	return;
	
	
# called when the user wants to edit or clone a vehicle
func show_vehicle_edit_ui(vehicle, p_flags):
	# enter into the vehicle edit state by emulating a button press
	if (!is_current_state()):
		button.set_pressed(true);
	else:
		show_dialog();
	
	# TODO: proper usage of flags
	# https://godotengine.org/qa/22370/bit-flags-in-gdscript
	if (p_flags != Flags.FLAG_CLONE):
		_edit_vehicle = vehicle;
		ui_vehicle_name.set_text(vehicle.get_display_name());
	
	# update the dialog from the vehicle
	var vehicle_route = vehicle.get_vehicle_route();
	for i in range(0, vehicle_route.size()):
		var resource_node = vehicle_route.get_resource_node(i);
		add_resource_node(resource_node);
		
	Util.assert(ui_route_list.get_child_count() == vehicle_route.size(), "Resource node to route size list mismatch");
	
	ignore_max_carriage = true;
	auto_unload = false;
	
	# load on the appropriate resources
	for i in range(0, vehicle_route.size()):
		var load_resources = vehicle_route.get_load_resources(i);
		var ui_resource_node = ui_route_list.get_child(i);
		for resource_name in load_resources:
			var resource_count = load_resources[resource_name];
			for j in range(0, resource_count):
				var supply_resource_button = ui_resource_node.find_last_resource_button_from_container(ui_resource_node.ui_supply_container, resource_name);				
				if (supply_resource_button):
					_on_resource_pressed(ui_resource_node, supply_resource_button); # emulate button press
					
	# unload on the appropriate resources
	for i in range(0, vehicle_route.size()):
		var unload_resources = vehicle_route.get_unload_resources(i);
		var ui_resource_node = ui_route_list.get_child(i);
		for resource_name in unload_resources:
			var resource_count = unload_resources[resource_name];
			for j in range(0, resource_count):
				var train_resource_button = ui_resource_node.find_last_resource_button_from_container(ui_resource_node.ui_train_inventory_container, resource_name);
				if (train_resource_button):
					_on_resource_pressed(ui_resource_node, train_resource_button); # emulate button press
		
	auto_unload = true;
	ignore_max_carriage = false;
	return;
	
	
func show_dialog():	
	controller.mouse_over_enabled = true
	
	# update costing
	var purchase_cost = get_purchase_cost(get_max_carriages());		
	ui_purchase_cost.set_text(Util.format_currency(purchase_cost));
	var maintenance_cost = get_maintenance_cost(get_max_carriages());
	ui_maintenance_cost.set_text(Util.format_currency(maintenance_cost));

	vehicle_route.clear();
	
	dialog.set_visible(true);
	ui_cost_panel.set_visible(true);
	
	_on_RandomNameButton_pressed();
	
	# claer the dialog, without deleting the tooltip, and re-add tooltip after cleared
	if (ui_helper_tip.get_parent()):
		ui_helper_tip.get_parent().remove_child(ui_helper_tip);
		
	BUtil.delete_children(ui_route_list);
	ui_route_list.add_child(ui_helper_tip);

	EventMgr.trigger(EventMgr.Id.VEHICLE_CONFIGURATION_DIALOG_DISPLAYED, { "sender": self, "dialog": dialog });
	return;
	
	
func generate_vehicle_name():
	# TODO: read these out of a file?
	var r = rand_range(0, format_list.size());
	var format_str = format_list[r];
	
	var r2 = rand_range(0, name_list.size());
	var name_str = name_list[r2];
	var name = format_str % name_str;
	return name;
	
	
func get_vehicle_route_from_ui():
	# generate the route from the UI	
	vehicle_route.clear();
	for ui_terminal in ui_route_list.get_children():
		var resource_node = ui_terminal.resource_node;
		#var vehicle_terminal = ui_terminal.vehicle_terminal;
		var unload_resources = ui_terminal.get_unload_resources();
		var load_resources = ui_terminal.get_load_resources();
		var manifest_resources = ui_terminal.get_manifest_resources();
		Util.assert(unload_resources.size() <= trainInst.max_carriages, "Unload resources bigger than can take off train!");
		Util.assert(load_resources.size() <= trainInst.max_carriages, "Load resources bigger than can fit on train!");
		vehicle_route.add(resource_node, manifest_resources, load_resources, unload_resources);
		
	return vehicle_route;
	
	
# make the purchase, returns false if we cant afford it
func make_purchase(p_vehicle_route_ser):
	# money handling
	var company = get_company()
	var route = create_route(p_vehicle_route_ser, company);
	var purchase_cost = get_purchase_cost(route.get_max_length());
	
	# this means we are selling carriages
	if (purchase_cost < 0):
		company.receive_cash(-purchase_cost, IncomeStatement.VEHICLES); # we want to receive +ve amount of money!
		return true;
	
	var loan_amount = company.loan_amount_required(purchase_cost)
	
	# cant actually afford it! abort!
	if (loan_amount == -1):
		return false;
	
	loan_amount = company.loan_amount_to_block(loan_amount);
	company.take_out_loan(loan_amount);
	company.pay_cash(purchase_cost, IncomeStatement.VEHICLES);
	return true;
	
	
func edit_vehicle():
	var vehicle_route = get_vehicle_route_from_ui();
	rpc("edit_vehicle_master_rpc", vehicle_route.serialize(), ui_vehicle_name.get_text(), _edit_vehicle.get_path());
	_on_CancelBtn_pressed() # close the dialog
	
	
master func edit_vehicle_master_rpc(p_vehicle_route_ser, p_vehicle_display_name, p_vehicle_path):
	_edit_vehicle = get_tree().get_root().get_node(p_vehicle_path);
	if (!make_purchase(p_vehicle_route_ser)):
		return;
	
	rpc("edit_vehicle_rpc", p_vehicle_route_ser, p_vehicle_display_name, p_vehicle_path)
	return;
	
	
remotesync func edit_vehicle_rpc(p_vehicle_route_ser, p_vehicle_display_name, p_vehicle_path):
	var vehicle = get_tree().get_root().get_node(p_vehicle_path);
	vehicle.set_vehicle_route(p_vehicle_route_ser, trainCarriageInst);
	vehicle.set_display_name_rpc(p_vehicle_display_name); # we don't need this to go through the network again
	EventMgr.trigger(EventMgr.Id.VEHICLE_EDITED, { "sender": self, "vehicle": vehicle });
	return;
	
	
func create_vehicle():	
	var vehicle_route_ser = get_vehicle_route_from_ui().serialize();
	rpc("create_vehicle_master_rpc", vehicle_route_ser, ui_vehicle_name.get_text());
	_on_CancelBtn_pressed() # close the dialog
	
	
master func create_vehicle_master_rpc(p_vehicle_route_ser, p_vehicle_display_name):
	_edit_vehicle = null;
	if (!make_purchase(p_vehicle_route_ser)):
		return;
	
	var vehicle_name = world.network.next_unique_name(controller.networkId, networkNamePrefix);
	# create the train on all clients - send them the name to guarnatee sync'ed name!
	rpc("create_vehicle_rpc", controller.get_company().get_path(), vehicle_name, p_vehicle_route_ser, p_vehicle_display_name)
	return;
	
	
remotesync func create_vehicle_rpc(companyPath, vehicle_name, vehicle_route_ser, vehicle_display_name):
	var company = get_tree().get_root().get_node(companyPath);
	var route = create_route(vehicle_route_ser, company);

	var globalTrainsNode = BUtil.find_child(world.world_container, globalsNodeName);

	# create train and drop it on the track
	var train = trainInst.duplicate(); #vehicleTemplate.instance() 
	train.set_visible(true);

	train.set_name(vehicle_name);
	globalTrainsNode.add_child(train) # tell train its ready # TODO: add to trains child node?
	train.spawn(company, route, trainCarriageInst, vehicle_display_name, get_name())

	EventMgr.trigger(EventMgr.Id.VEHICLE_CONSTRUCTED, { "sender": self, "vehicle": train });
	return;


func get_max_carriages():
	var max_carriages = 0;
	var route_count = ui_route_list.get_child_count();
	for i in range(0, route_count):
		var ui_resource_node = ui_route_list.get_child(i);
		# no carriages if we are showing the tooltip! 
		if (ui_resource_node == ui_helper_tip):
			return 0;
			
		var carriage_count = _get_carriage_count_at_station(ui_resource_node);
		max_carriages = max(max_carriages, carriage_count);
		
	return max_carriages;
	
	
func get_purchase_cost(max_carriages):
	var train_purchase_cost = trainInst.get_finances().get_purchase_cost();
	var carriage_purchase_cost = trainCarriageInst.get_finances().get_purchase_cost();
	
	# if we are editing a vehicle, we compute the difference in carriages
	if (_edit_vehicle):
		var carriages = _edit_vehicle.get_vehicle_carriages();
		var carriage_delta = max_carriages - carriages.size();
		
		# if we are refunding carriages, we only get the refund amount
		if (carriage_delta < 0):
			var carriage_refund = trainCarriageInst.get_finances().get_refund_amount();
			return (carriage_refund * carriage_delta); # delta is -ve so the result is -ve here
		else:
			return (carriage_purchase_cost * carriage_delta);
	
	var cost = train_purchase_cost + (carriage_purchase_cost * max_carriages);
	return cost;
	
	
func get_maintenance_cost(max_carriages):
	var maintenance_cost = trainInst.get_finances().get_maintenance_cost();
	var carriage_maintenance_cost = trainCarriageInst.get_finances().get_maintenance_cost();
	return maintenance_cost + (carriage_maintenance_cost * max_carriages);
	
	
func create_route(vehicle_route_ser, company):
	Util.assert(vehicle_route, "No vehicle route");
	var vr = vehicle_route.duplicate(); # duplicate is more mod firendly than new
	vr.deserialize(vehicle_route_ser, get_tree()); # passing in get_tree here sucks
	return vr;
	
	
func _on_OkButton_pressed():	
	if (_edit_vehicle):
		edit_vehicle();
	else:
		create_vehicle();
	
	# play any audio if it exists
	if ($OkPressed):
		$OkPressed.trigger();
		
	return;

func _on_CancelBtn_pressed():
	dialog.set_visible(false)
	controller.hud.clear_toggled_state_buttons();


func _on_RandomNameButton_pressed():
	var name = generate_vehicle_name();
	ui_vehicle_name.set_text(name);


func _on_TextureButton_toggled( pressed ):
	# disable in demo
	if (BDatabase.is_build_type("demo") and !available_in_demo):
		return;

	toggle_state(pressed);
	return;
	
	
# add station to the bottom of the route list
func add_resource_node(resource_node):
	# remove the helper tip when a station is added, if it is in the dialog
	if (ui_helper_tip.get_parent()):
		ui_helper_tip.get_parent().remove_child(ui_helper_tip);
	
	var ui_resource_node = ui_resource_node_template.duplicate();
	ui_route_list.add_child(ui_resource_node);
	ui_resource_node.controller = controller;
	ui_resource_node.set_resource_node(resource_node);
	ui_resource_node.connect("resource_pressed", self, "_on_resource_pressed");
	ui_resource_node.connect("delete_pressed", self, "_on_delete_pressed");
	
	update_ui();
	
	if (ui_route_list.get_child_count() <= 1):
		return;
		
	# copy the train inventory from the last reosurce node to this resource node
	var last_ui_resource_node = ui_route_list.get_child(ui_route_list.get_child_count() - 2);
	var childCount = last_ui_resource_node.ui_train_inventory_container.get_child_count();
	for i in range(1, childCount):
		var last_resource_button = last_ui_resource_node.ui_train_inventory_container.get_child(i);
		
		for supply_terminal in last_resource_button.supply_dict:
			var count = last_resource_button.supply_dict[supply_terminal];
			for i in range(0, count):
				_load_resource_into_train_at_terminal(ui_resource_node, last_resource_button.resource_data, last_resource_button, 0, supply_terminal);

	# now emulate pressing the buttons to remove them if they are demanded as this new station as to unload it
	for i in range(childCount - 1, 0, -1):
		var resource_button = ui_resource_node.ui_train_inventory_container.get_child(i);

		# check if the resource is demanded at this new station, if so
		# unload it
		if (ui_resource_node.is_demanded(resource_button.resource_data.get_name()) && auto_unload):
			# unload the resource here by emulating a press on the cloned button
			for j in range(0, resource_button.amount):
				_on_resource_pressed(ui_resource_node, resource_button);

	return;
	
	
# delete resource node from the queue
func _on_delete_pressed(ui_resource_node):
	# return any products supplied from this station
	for i in range(1, ui_resource_node.ui_supply_container.get_child_count()):
		var supply_resource_button = ui_resource_node.ui_supply_container.get_child(i);
		for r in range(0, -supply_resource_button.amount_delta):
			# unload from the train if the train has any left
			var last_resource_button = ui_resource_node.find_last_resource_button_from_container(ui_resource_node.ui_train_inventory_container, supply_resource_button.resource_data.get_name());
			Util.assert(last_resource_button, "Can't find resource on train");
			if (last_resource_button):
				_on_resource_pressed(ui_resource_node, last_resource_button); # emulate button press

	# return any products loaded off at station
	for i in range(1, ui_resource_node.ui_demand_container.get_child_count()):
		var demand_resource_button = ui_resource_node.ui_demand_container.get_child(i);
		for r in range(0, demand_resource_button.amount_delta):
			_on_resource_pressed(ui_resource_node, demand_resource_button); # emulate button press
	
	# return any products loaded off at station
	for i in range(1, ui_resource_node.ui_demand_container.get_child_count()):
		var demand_resource_button = ui_resource_node.ui_demand_container.get_child(i);
		for r in range(0, demand_resource_button.amount_delta):
			# go back in reverse and remove product from supply
			_go_backwards_and_remove_resource_from_supply(ui_resource_node, demand_resource_button.resource_data);
			
		Util.assert(demand_resource_button.amount_delta == 0, "Couldn't clear resources from:" + ui_resource_node.resource_node.get_display_name());
		
	# I think stuff on the train just passes through, so shouldn't need to be removed
#	# go back in reverse and remove product stuck on the train
#	for i in range(1, ui_resource_node.ui_train_inventory_container.get_child_count()):
#		var resource_button = ui_resource_node.ui_train_inventory_container.get_child(i);
#		_go_backwards_and_remove_resource_from_supply(ui_resource_node, resource_button.resource_data);
#
#	Util.assert(ui_resource_node.ui_train_inventory_container.get_child_count() == 1, "Couldn't clear train inventory from:" + ui_resource_node.resource_node.get_display_name());
			
	# now remove the button
	ui_resource_node.get_parent().remove_child(ui_resource_node);
	ui_resource_node.queue_free();
	
	if (ui_route_list.get_child_count() <= 0):
		ui_route_list.add_child(ui_helper_tip);
	
	return;
	
	
func _on_resource_pressed(resource_node_info, resource_button):
	var resource_data = resource_button.resource_data;
	var layout = resource_button.get_parent();
	
	# TODO: cool animation to slide from the resource_button to where it will be located?
	
	# use clicked on a supplied resource
	# clone the button and add to the train inventory
	if (layout == resource_node_info.ui_supply_container):
		_supply_resource_pressed(resource_node_info, resource_button);
		return;
		
	# user clicked on a demanded resource
	# move the item back on to the train, this has to be where it came from - might need to store a count
	if (layout == resource_node_info.ui_demand_container):
		_demand_resource_pressed(resource_node_info, resource_button);
		return;
		
	# user clicked a carriage from the train
	# go through, make sure the product can be removed
	# if so, add one to indicate the move to meet the demand
	if (layout == resource_node_info.ui_train_inventory_container):
		_train_inventory_resource_pressed(resource_node_info, resource_button);
		return;
	
	return;
	
	
func _load_resource_into_train_at_terminal(p_ui_terminal, p_resource_data, p_resource_button, p_delta_amount, p_origin):
	var carriage_resource_button = p_ui_terminal.find_last_resource_button_from_container(p_ui_terminal.ui_train_inventory_container, p_resource_data.get_name());
	# increment value on train if button exists, if it doesnt exist create a new button for the resource
	if (!carriage_resource_button):
		carriage_resource_button = p_ui_terminal.clone_resource_button(p_resource_button);
		carriage_resource_button.set_amount(0);
		carriage_resource_button.set_amount_delta(0);
		carriage_resource_button.disconnect_from_resource_data();
		p_ui_terminal.add_resource_button_to_vehicle_inventory(carriage_resource_button);
		
	carriage_resource_button.add_1_resource(1, p_delta_amount, p_origin);
	return carriage_resource_button;
			
	
func _supply_resource_pressed(resource_node_info, resource_button):
	var resource_data = resource_button.resource_data;
	
	# don't allow train to be overloaded
	if (_get_carriage_count_at_station(resource_node_info) >= trainInst.max_carriages):
		return;
		
	# when we add a resource to a train, go through and add it to each station
	var adjusted_route_list = Util.get_circular_array_starting_with(ui_route_list.get_children(), resource_node_info);
	var first = true;
	
	# don't allow this item to be loaded on the train if it will overfill it at some point
	for other_ui_terminal in adjusted_route_list:
		var carriage_count = _get_carriage_count_at_station(other_ui_terminal);
		if (!first && other_ui_terminal.is_demanded(resource_data.get_name()) && auto_unload):
			break;
			
		if (carriage_count >= trainInst.max_carriages): # -1, one for the train icon, +1 for the carriage we wish to add = 0
			return;
			
		first = false;
		
	# now we know that it can be added, go and add it to the carriages
	resource_button.set_amount_delta(resource_button.amount_delta - 1);
	
	first = true;
	for other_ui_terminal in adjusted_route_list:
		if (!first && other_ui_terminal.is_demanded(resource_data.get_name()) && auto_unload):
			# to make it easy for the user, assume they want to unload at the next stop
			# that demands this product
			# emulate user pressing the button on the train to unload
			var demand_resource_button = other_ui_terminal.find_last_resource_button_from_container(other_ui_terminal.ui_demand_container, resource_data.get_name());
			demand_resource_button.add_1_resource(0, 1, resource_node_info);
			return;
			
		_load_resource_into_train_at_terminal(other_ui_terminal, resource_data, resource_button, 1, resource_node_info);
		first = false;
			
	return;
	
	
func _demand_resource_pressed(resource_node_info, resource_button):
	var resource_data = resource_button.resource_data;
	
	if (_get_carriage_count_at_station(resource_node_info) >= trainInst.max_carriages):
		return;
		
	var delta = resource_button.amount_delta;
	if (delta <= 0):
		return;

	# when we add a resource to a train, go through and add it to each station
	# until we reach the station that supplied the product 
	# (which is the previous station that supplied the resource)
	var adjusted_route_list = Util.get_circular_array_starting_with(ui_route_list.get_children(), resource_node_info);
	var reversed_route_list = Util.reverse_array(adjusted_route_list);
	
	# now we know its safe to load up the train
	var supply_station = resource_button.remove_1_resource(0, 1);
	
	# don't allow this item to be loaded on the train if it will overfill it at some point
	for other_ui_terminal in adjusted_route_list:
		var carriage_count = _get_carriage_count_at_station(other_ui_terminal);
		if (other_ui_terminal == supply_station):
			break;
			
		if (carriage_count > trainInst.max_carriages): # -1, one for the train icon, +1 for the carriage we wish to add = 0
			resource_button.add_1_resource(0, 1, supply_station); # add what we took off back on
			return;
			
	# if when we press the demand button we create a new button on the train inventory
	# then we need to correctly set the amount delta
	var carriage_resource_button = _load_resource_into_train_at_terminal(resource_node_info, resource_data, resource_button, 0, supply_station);
	carriage_resource_button.set_amount_delta(-resource_button.amount_delta);
		
	# now go through all stations until we reach the origional supplying station
	# and add the product back on the train
	var first = true;
	for other_ui_terminal in adjusted_route_list:
		if (other_ui_terminal == supply_station):
			return;
			
		if (!first && other_ui_terminal.is_demanded(resource_data.get_name()) && auto_unload):
			# to make it easy for the user, assume they want to unload at the next stop
			# that demands this product
			# emulate user pressing the button on the train to unload
			var demand_resource_button = other_ui_terminal.find_last_resource_button_from_container(other_ui_terminal.ui_demand_container, resource_data.get_name());
			demand_resource_button.set_amount_delta(demand_resource_button.amount_delta + 1);
			return;

		if (!first):
			_load_resource_into_train_at_terminal(other_ui_terminal, resource_data, resource_button, 0, supply_station);
			
		first = false;
		
	return;
	
	
func _get_carriage_count_at_station(p_ui_terminal):
	var child_count = p_ui_terminal.ui_train_inventory_container.get_child_count();
	var count = 0;
	for i in range(1, child_count):
		var carriage = p_ui_terminal.ui_train_inventory_container.get_child(i);
		count += carriage.amount;
		
	return count;
	
	
func _train_inventory_resource_pressed(resource_node_info, resource_button):
	var resource_data = resource_button.resource_data;
	
	# is the user undoing a supply operation? ie. returning the goods to the station from which it came
	if (resource_button.has_resource_from_station(resource_node_info)):
		#resource_button.remove_1_resource(1, 1, resource_node_info);
	
		var supply_resource_button = resource_node_info.find_last_resource_button_from_container(resource_node_info.ui_supply_container, resource_data.get_name());
		supply_resource_button.set_amount_delta(supply_resource_button.amount_delta + 1);
		
		# remove the resource from the train at each stop until we find the stop where it is unloaded
		_unload_from_train_at_each_resource_node_till_found_where_unloaded(resource_node_info, resource_data, 0, resource_node_info, false);
		return;

	# does the resource node demand this product?
	var demand_resource_button = resource_node_info.get_demand_resource_button(resource_data.get_name());
	
	# check if the user is unloading at a warehouse and this is a new resource for the warehouse
	if (!demand_resource_button && resource_node_info.is_warehouse()):
		var supply_resource_button = resource_node_info.create_resource_icon(resource_data, resource_node_info.ui_icon_button_template.DisplayType.SUPPLY);
		resource_node_info.add_resource_button_to_supply(supply_resource_button);
		
		demand_resource_button = resource_node_info.create_resource_icon(resource_data, resource_node_info.ui_icon_button_template.DisplayType.DEMAND);
		resource_node_info.add_resource_button_to_demand(demand_resource_button);
		
	# check if the user is attempting to meet demand
	if (demand_resource_button):
		var supply_station = resource_button.get_first_supply_station();
		demand_resource_button.add_1_resource(0, 1, supply_station);

		# remove the resource from the train at each stop until we find the stop where it is unloaded
		_unload_from_train_at_each_resource_node_till_found_where_unloaded(resource_node_info, resource_data, 0, supply_station, true);
		return;
		
	return;
	
	
# remove 1x resource from the train at each stop until we find the stop where it is unloaded (or loaded)
# supply station is the origin of the resource to unload
func _unload_from_train_at_each_resource_node_till_found_where_unloaded(resource_node_info, resource_data, first_index, p_supply_station, p_stop_before_supply_station):
	var adjusted_route_list = Util.get_circular_array_starting_with(ui_route_list.get_children(), resource_node_info);
	for i in range(first_index, adjusted_route_list.size()):
		var other_ui_terminal = adjusted_route_list[i];

		# we found where it is supplied?
		if (i != first_index && other_ui_terminal == p_supply_station && p_stop_before_supply_station):
			return;
			
		var last_resource_button = other_ui_terminal.find_last_resource_button_from_container(other_ui_terminal.ui_train_inventory_container, resource_data.get_name());
		
		# we found where it is being unloaded?
		if (i != 0 && other_ui_terminal.is_demanded(resource_data.get_name())):
			var demand_resource_button = other_ui_terminal.find_last_resource_button_from_container(other_ui_terminal.ui_demand_container, resource_data.get_name());
			if (demand_resource_button.has_resource_from_station(p_supply_station)):
				demand_resource_button.remove_1_resource(0, 1, p_supply_station);
				
				# if the train leaves this station with some of this resource then its delta needs to be updated
				if (last_resource_button):
					last_resource_button.set_amount_delta(-demand_resource_button.amount_delta);
				return;
				
		# unload 1 from train
		if (last_resource_button):
			last_resource_button.remove_1_resource(1, 0, p_supply_station);
			var amount = last_resource_button.amount;
			if (amount <= 0):
				BUtil.remove_from_parent(last_resource_button);
				last_resource_button.queue_free();
				
		# we found where it is supplied?
		if (i != first_index && other_ui_terminal == p_supply_station):
			return;
				
	return;
	
	
# go backwards looking for where 'resource_data' is supplied from and remove it by emulating a click on the resource to return it to the supply
func _go_backwards_and_remove_resource_from_supply(resource_node_info, resource_data):
	var adjusted_route_list = Util.get_circular_array_reverse_starting_with(ui_route_list.get_children(), resource_node_info);
	for i in range(0, adjusted_route_list.size()):
		var other_ui_terminal = adjusted_route_list[i];
		if (i != 0 && other_ui_terminal.is_supplied(resource_data.get_name())):
			var last_resource_button = other_ui_terminal.find_last_resource_button_from_container(other_ui_terminal.ui_train_inventory_container, resource_data.get_name());
			_on_resource_pressed(other_ui_terminal, last_resource_button); # emulate button press
			return;
			
	return;
	
