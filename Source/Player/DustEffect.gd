extends Spatial

var controller;


func _process(delta):
	# TODO: Fab set notification on camera change
	# maybe put this in world?? or some effect manager/node
	#var cur_cam = controller.get_current_camera();
	var cur_cam = get_viewport().get_camera();
	BUtil.reparent(self, cur_cam);
	self.set_transform(Transform.IDENTITY);
	return;

func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_child(self);
	return;
