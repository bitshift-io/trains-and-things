#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Manager for outline select effect
# 
# TODO: future optimisation disable this when no effect visible
#
extends CanvasLayer

var controller;
var adding_node = false;

onready var vp = BUtil.find_child(self, "Viewport");
onready var cam = BUtil.find_child(self, "Viewport/Camera");


func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_child(self);
	return;
	
	
func _ready():
	get_tree().connect("screen_resized", self, "_screen_resized");
	_screen_resized();
	set_process(true);
	return;
	
	
func _screen_resized():
	var game_vp = get_viewport();
	var game_vp_width = game_vp.get_size().x;
	var game_vp_height = game_vp.get_size().y;
	
	vp.set_size(Vector2(game_vp_width, game_vp_height));
	return;
	
func _process(delta):
	# apply the game camera motion to our local viewport camera
	var game_vp = get_viewport();
	var game_camera = game_vp.get_camera();
	if (cam && game_camera):
		cam.transform = game_camera.transform;
		cam.projection = game_camera.projection;
		cam.fov = game_camera.fov;
		cam.near = game_camera.near;
		cam.far = game_camera.far;

	return;
	
	
func add(node):
	if (adding_node):
		return;
		
	adding_node = true;
	vp.add_child(node);
	adding_node = false;
	return;
	
	
func remove(node):
	if (node):
		vp.remove_child(node);

	return;
	
