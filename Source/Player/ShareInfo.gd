#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# this is the share info UI panel that sits inside dialogs
#
extends Panel

onready var ui_title = BUtil.find_child(self, "Title");
onready var ui_merge_button = BUtil.find_child(self, "MergeButton");
onready var ui_buy_button = BUtil.find_child(self, "BuyButton");
onready var ui_sell_button = BUtil.find_child(self, "SellButton");

onready var ui_tab_container = BUtil.find_child(self, "TabContainer");
onready var ui_holding_value_label = BUtil.find_child(self, "HoldingValueLabel");
onready var ui_cost_value_label = BUtil.find_child(self, "CostValueLabel");
onready var ui_value_value_label = BUtil.find_child(self, "ValueValueLabel");
onready var ui_invested_value_label = BUtil.find_child(self, "InvestedValueLabel");
onready var ui_gain_value_label = BUtil.find_child(self, "GainValueLabel");
onready var ui_remaining_available_holdings_value_label = BUtil.find_child(self, "RemainingAvailableHoldingsValueLabel");

onready var timer = Util.create_timer(self, "_update_ui", 1.0, true);

var controller; # this player controller and this players company
var share;		# this player companies share in company

func _ready():
	_update_ui();
	return;
	
func set_company(p_company):
	if (share):
		share.disconnect("changed", self, "_share_changed");
		share = null;
		
	share = controller.get_company().get_share_portfolio().get(p_company);
	share.connect("changed", self, "_share_changed");
	return;
	
func get_company():
	if (!share):
		return null;
		
	# TODO: another potentially betterway to do this is to have a "delete" signal on the company
	# we can listen too
	var company = share.get_company();
	if (!company):
		controller.company_destroyed(self);
		
	return company;
	
func _share_changed():
	_update_ui();
	return;
	
func _update_ui():
	var company = get_company();
	if (!company):
		return;
		
	ui_title.set_text(company.display_name);
	
	#if (company.is_available_on_share_market()):
	ui_tab_container.set_current_tab(0);
	#else:
	#	ui_tab_container.set_current_tab(1);
	
	# cant buy into our own company!
	var in_this_company = (company == controller.get_company());
	ui_buy_button.set_visible(!in_this_company);
	ui_sell_button.set_visible(!in_this_company);
	
	
	# https://www.investopedia.com/terms/c/controllinginterest.asp
	# if all shares are sold, whom ever has the greatest share has the controling interest
	# or if someone has more than 50% ownership of total shares available
	var has_controlling_share = (share.holding > (company.initial_shares_available * 0.5));
	if (company.shares_available <= 0):
		var companies = get_tree().get_nodes_in_group("companies");
		has_controlling_share = true;
		for i in range(0, companies.size()):
			var other_companies_share = companies[i].get_share_portfolio().get(company);
			if (other_companies_share.holding > share.holding):
				has_controlling_share = false;
			
	ui_merge_button.set_visible(has_controlling_share);
	
	# update labels		
	ui_holding_value_label.set_text(String(share.holding));
	ui_remaining_available_holdings_value_label.set_text(String(company.shares_available));
	ui_cost_value_label.set_text(Util.format_currency(company.get_share_value()));
	
	var value = share.holding * company.get_share_value();
	ui_value_value_label.set_text(Util.format_currency(value));
	ui_invested_value_label.set_text(Util.format_currency(share.invested));
	ui_gain_value_label.set_text(Util.format_currency(value - share.invested));
	return;
	
func search_match(text):
	var company = get_company();
	if (!company):
		return;
		
	var lower_text = text.to_lower();
	
	# match resource node name
	if (company.display_name.to_lower().match(lower_text)):
		return true;

	# match by who is in the company
	var players = company.get_players();
	for p in players:
		if (p.display_name.to_lower().match(lower_text)):
			return true;
		
	return false;

func _on_BuyButton_pressed():
	var company = get_company();
	if (!company):
		return;
		
	controller.get_company().buy_shares(company, 10);

func _on_SellButton_pressed():
	var company = get_company();
	if (!company):
		return;
		
	controller.get_company().sell_shares(company, 10);

func _on_MergeButton_pressed():
	var company = get_company();
	if (!company):
		return;
		
	controller.get_company().merge(company);
