#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var panel = find_node("Panel");
onready var chatDlg = find_node("ChatPanel");
onready var chatMessagesLbl = find_node("ChatMessagesLbl");
onready var chatInputEdt = find_node("ChatInputEdt");

var controller;

var timer  = Util.create_timer(self, "_hide_chat_timeout", 5.0, false);

func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_child(self);
	if (!controller.is_network_master()):
		return;
	
	controller.hud.add_dialog_panel(panel);

	
func _ready():
	chatMessagesLbl.set_scroll_follow(true);
	chatMessagesLbl.set_scroll_active(true);
	chatInputEdt.hide();
	chatMessagesLbl.clear();
	chatDlg.hide();

	
func activate():
	chatInputEdt.show();
	chatInputEdt.grab_focus();
	controller.get_current_camera().set_process_input(false);
	show_chat();	

	
func deactivate():
	controller.get_current_camera().set_process_input(true);
	chatMessagesLbl.set_scroll_active(false);
	chatInputEdt.hide();
	chatInputEdt.clear();
	hide_chat();

	
func _on_ChatInputEdt_text_entered( text ):
	send_chat(text);
	deactivate();

	
func send_chat(text):
	rpc("send_chat_rpc", get_tree().get_network_unique_id(), text);


master func send_chat_rpc(id, msg):
	msg = str(msg).strip_edges();
	if (msg.length() <= 0):
		return;

	msg = "[color=yellow]" + controller.display_name + "[/color] " + msg;
	rpc("add_message_rpc", msg);


# console command to broadcast a message
func broadcast_msg(text):
	if (!get_tree().is_network_server()):
		return;
		
	var cmd = text.split(" ", true)
	var cmdLen = cmd[0].length() + 1
	var msg = text.substr(cmdLen, text.length() - cmdLen)
	rpc("add_message_rpc", msg);


func send_msg_to(id, msg):
	if (!get_tree().is_network_server()):
		return;
	
	rpc_id(id, "add_message_rpc", msg);


# gets called on all clients, so here we find the local player controller
# and push the message to his UI
remotesync func add_message_rpc(msg):
	# send the message to the master player (the local network owner on this machine)
	var master_pc = controller.world.master_player;
	master_pc.get_chat_panel().add_message(msg);

		
func add_message(msg):
	#BLog.debug("add_message called with: " + msg + ", on :" + String(controller.networkId));
	
	# take the message from this player controller
	# and post it in the player controller who is local to this machine's
	# user interface	
	if (chatMessagesLbl.get_text() != ""):
		chatMessagesLbl.newline();
	
	chatMessagesLbl.append_bbcode(msg);
	show_chat();
	#fade_in();
#	anim_player.play("FadeIn");


func show_chat():
	if (timer):
		timer.stop();
		
	chatDlg.show();
	
	
func hide_chat():
	# no don't try to hide chat while the user is entering text
	if (chatInputEdt.visible):
		return;
		
	timer.start();
		
	
func _hide_chat_timeout():
	chatDlg.hide();
