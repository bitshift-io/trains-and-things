#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Interface between player controller and vehicle
# it hides the UI from the vehicle such that the vehicle needs to know
# nothing about the UI
#

extends Node

var VehicleEditorState = preload("res://Player/VehicleEditorState.gd");

var controller;

const EDIT = 0;
const FOLLOW = 1;
const DELETE = 2;
const CLONE = 3;

var node: WeakRef;


func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_handler(self);
	return;
	
	
func show_context_menu(p_node):
	node = weakref(p_node);
	
	var context_menu = controller.hud.get_context_menu();
	context_menu.clear();
	context_menu.add_item("Edit", EDIT);
	context_menu.add_item("Clone", CLONE);
	
	context_menu.add_check_item("Camera Follow", FOLLOW);
	var is_following = (controller.camera_get_follow_node() == node.get_ref());
	context_menu.set_item_checked(FOLLOW, is_following);
	
	context_menu.add_item("Delete", DELETE);
	controller.hud.popup_context_menu(self, "_context_menu_pressed");
	return;
	
	
func _context_menu_pressed(p_context_menu, p_id):
	if (!node.get_ref()):
		return;
		
	match p_id:
		EDIT: show_edit_ui();
		
		CLONE: show_clone_ui();
		
		FOLLOW: 
			var context_menu = controller.hud.get_context_menu();
			var follow = !context_menu.is_item_checked(p_id);
			if (follow):
				controller.camera_set_follow_node(node.get_ref());
			elif (controller.camera_get_follow_node() == node.get_ref()):
				controller.camera_set_follow_node(null);
				
		DELETE: delete(node.get_ref());
			
	return;


func show_edit_ui():
	controller.show_vehicle_edit_ui(node.get_ref(), "TrainEditorState", VehicleEditorState.Flags.FLAG_EDIT);
	return;
	
	
func show_clone_ui():
	controller.show_vehicle_edit_ui(node.get_ref(), "TrainEditorState", VehicleEditorState.Flags.FLAG_CLONE);
	return;
	
	
func handles(p_node):
	var is_vehicle = BUtil.is_class_name(p_node, "Vehicle");
	
	# only handle our vehicles!
	if (is_vehicle && p_node.get_company() != controller.get_company()):
		return false;
		
	return is_vehicle;
	

func delete(p_node):
	p_node.delete();
	return true;
