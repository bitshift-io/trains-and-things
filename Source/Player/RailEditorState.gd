#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "PlayerState.gd"

var curve_editor = preload("res://Game/CurveEditor.gd").new()
var TrackBuilder = preload("TrackBuilder.gd");

var snap_to_track_distance = 3.0; # make this an export

export(bool) var available_in_demo = false; # can we use this in the demo?

export(NodePath) var junction_path; # the prefab we place between mesh segments
export(NodePath) var track_path; # the prefab we are laying down and stretching and deforming

export(NodePath) var connection_cursor_path; # the cursor to indicate a connection

onready var track_inst = get_node(track_path);
onready var junction_inst = get_node(junction_path) if junction_path else null;
onready var connection_cursor_inst = get_node(connection_cursor_path);

var double_click = false;
var leftMousePressed = false;
var rightMousePressed = false;
var shiftDown = false;

var mouseOverCollider = null

var startConnection = null
var endConnection = null
var track_splits = []; # a list of dictionaries that describe what track and where to split it

var finishedLayingTrack = false

const CollisionHeightAdjust = 1.0 #Vector3(0.0, 1.0, 0.0)

export(NodePath) var button_path;
export(NodePath) var dialog_path;

export(float) var snap_to_junction_distance = 4.0; # if within two meters of start, snap to start junction same for the end

onready var button = get_node(button_path);
onready var dialog = get_node(dialog_path);

onready var okBtn = BUtil.find_child(dialog, "OkBtn");
onready var cancelBtn = BUtil.find_child(dialog, "CancelBtn");

onready var world = WorldMgr.get_world(self);

#var station_either_list = []; # train stations that are converted from either

func apply_to_controller(controller):
	track_inst = get_node(track_path);
	track_inst.set_as_building_template();
	track_inst.set_visible(false);
	
	junction_inst = get_node(junction_path) if junction_path else null;
	if (junction_inst):
		junction_inst.set_visible(false);
		junction_inst.set_as_building_template();
		
	controller.add_state(self);
	return;
	
func _ready():
	#set_network_mode(NETWORK_MODE_MASTER if get_tree().is_network_server() else NETWORK_MODE_SLAVE);
	set_network_master(1); # server is always 1
	
	controller.hud.add_state_button(button, "Rail");
	controller.hud.add_right_panel(dialog);
	
	#curve_editor.init(self)
	curve_editor.spawn(self, track_inst.instance_templates[0], controller.get_blueprint());
	curve_editor.set_name("CurveEditor");
	
	Util.set_click_mask_from_normal_alpha(button);
	return;

func state_enter(prevState):
	okBtn.connect("pressed", self, "_on_OkBtn_pressed")
	cancelBtn.connect("pressed", self, "_on_CancelBtn_pressed")
	
	okBtn.set_disabled(true)
	controller.clear_mouse_over()
	controller.mouse_over_enabled = false
	controller.context_menu_enabled = false;
	
func state_exit(nextState):	
	okBtn.disconnect("pressed", self, "_on_OkBtn_pressed")
	cancelBtn.disconnect("pressed", self, "_on_CancelBtn_pressed")
	
	dialog.set_visible(false)
	
	# reset the curves
	curve_editor.end()
	curve_editor.clear_all()
	
	startConnection = null
	endConnection = null
	
	finishedLayingTrack = false
	controller.mouse_over_enabled = true
	controller.context_menu_enabled = true;
	
	connection_cursor_inst.set_visible(false);
	
	#_on_CancelBtn_pressed()
	
func state_input(event):
	if (event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.pressed):
		rightMousePressed = true
		
	if (event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed):
		leftMousePressed = true
		double_click = event.doubleclick;
		
	if (event.is_action_pressed("shift")):
		shiftDown = true;
		
	if (event.is_action_released("shift")):
		shiftDown = false;

func find_snap_to_track(cursor_pos):
	# check for closest spline
	# is the cursor close enough to any track that we should be snapping to?
	#var globalTracksNode = get_node("/root/World/MapCore/Tracks");
	
	var trackList = get_tree().get_nodes_in_group("tracks");
	
	# append the nodes the user has already built in the curve editor
	#for curve in curve_editor.curve_mesh_node_list:
	#	trackList.append(curve);
	
	var closest_result = [];
	var closest_distance;
	
	for track in trackList: #globalTracksNode.get_children():
		if (!track.is_visible_in_tree()):
			continue;
		
		if (track.snap == track.SNAP_NONE):
			continue;
			
		var result = track.get_world_curve().closest_distance_to(cursor_pos, snap_to_track_distance);
		if (result.size() > 0):
			result["snap_to_junction"] = null;
			if (closest_result.size() <= 0 or result["distance_to_curve"] < closest_distance):
				var dist_along_curve = result["distance_along_curve"];
				var length = track.get_world_curve().get_baked_length();
				#if (dist_along_curve > 0.0 and dist_along_curve < length):
				#    continue;
				
				# move the "distance_along_curve" to be at the start or end
				if (track.snap == track.SNAP_TO_ENDS):
					if (dist_along_curve > (length / 2.0)):
						result["distance_along_curve"] = length;
						result["snap_to_junction"] = track.get_junction(track.END);
					else:
						result["distance_along_curve"] = 0.0;
						result["snap_to_junction"] = track.get_junction(track.START);
						
				# snap to any point along the curve, but if near a junction, snap to it to avoid fiddliness
				elif (track.snap == track.SNAP_ALONG):
					# snap to start
					if (dist_along_curve < snap_to_junction_distance):
						result["distance_along_curve"] = 0.0;
						result["snap_to_junction"] = track.get_junction(track.START);
						
					# snap to end junction
					elif (dist_along_curve > (length - snap_to_junction_distance)):
						result["distance_along_curve"] = length;
						result["snap_to_junction"] = track.get_junction(track.END);
				
				result["transform"] = track.get_transform_from_distance_along_curve(result["distance_along_curve"]); #.get_world_curve().get_transform_from_distance_along_curve(result["distance_along_curve"]);
				closest_distance = result["distance_to_curve"];
				closest_result = result;
				closest_result["node"] = track;
				
	# move test cube to the intersect point
	# TODO: we need the rotation
	if (closest_result.size() <= 0):
		return null;
	
	var track = closest_result["node"];
	#closest_result["curve"];
	var xform = track.get_transform_from_distance_along_curve(closest_result["distance_along_curve"]);
	#controller.get_node("TestCube2").set_global_transform(xform);
	controller.get_node("Cursor").set_global_transform(xform);
	closest_result["transform"] = xform;
	closest_result["node_path"] = closest_result["node"].get_path();
	return closest_result;


func find_best_connection(snap_to_track_result, cursor_pos, junction):
	var track = snap_to_track_result["node"];
	var start_junction = track.get_junction(track.START);
	var end_junction = track.get_junction(track.END);
	var can_reverse = track.can_reverse();		
	var usable_connection_list = [];
	
	# can reverse a track we are in the middle of using!
	if (startConnection and startConnection.get_junction().getTracks().has(track)):
		can_reverse = false;
	
	# if we have not started building a track, and the user has selected the incoming
	# reverse the track to make it the outgoing, then return the outgoing connection
	if (!curve_editor.is_active() and junction == start_junction and can_reverse):
		track.reverse();
		usable_connection_list = track.get_junction(track.END).getOutgoingConnections();
		
	# just starting a track, but the junctions face the correct way already
	elif (!curve_editor.is_active() and junction == end_junction):
		usable_connection_list = track.get_junction(track.END).getOutgoingConnections();
		
	# we are terminating a track
	elif (curve_editor.is_active() and junction == start_junction):
		usable_connection_list = track.get_junction(track.START).getIncomingConnections();
		
	# we are terminating a track (not on the track we started on)
	elif (curve_editor.is_active() and junction == end_junction and can_reverse):
		track.reverse(); # this might need to be moved to the track builder? hrmm
		usable_connection_list = track.get_junction(track.END).getIncomingConnections();
		
	else:
		usable_connection_list = [];
	
	# find the closest to the mouse cursor...
	var connection = null;
	var closest_dist = -1;
	for c in usable_connection_list:
		var dist_to_mouse = (c.get_global_transform().origin - cursor_pos).length_squared();
		if (closest_dist < 0 or dist_to_mouse < closest_dist):
			dist_to_mouse = closest_dist;
			connection = c;
		
	return connection;


func state_physics_process(delta):
	_update_ui()
	
	if (rightMousePressed):
		rightMousePressed = false # handled the event
		
		# close state if not active and user right clicks
		if (!curve_editor.is_active() && !finishedLayingTrack):
			_on_CancelBtn_pressed();
			
		finishedLayingTrack = false;
		undo_last_track();
		
	if (finishedLayingTrack):
		# same as double click, accept!
		if (leftMousePressed):
			leftMousePressed = false;
			double_click = false;
			_on_OkBtn_pressed();
			
		return
	
	var mouseOverColliderThisFrame = null;
	var collisions = controller.mouse_pick();
	var collision_point = Vector3(0,0,0);
	var collision_tangent = Vector3(0,0,0);
	
	#for collision in collisions:
	#	if (collision.collider && curve_editor.curve_mesh_node && curve_editor.curve_mesh_node.is_a_parent_of(collision.collider)):
	#		continue;
			
	#	collision_point = collision["position"] + collision["normal"] * CollisionHeightAdjust# + Vector3(0.0, bridgeHeight, 0.0)
		#BLog.debug("collision_point from collisions:" + String(collision_point) + " collision:" + collision.collider.get_path());
	#	break;
		
	#var cursor_pos = collision_point;
	
	var cursor_xform = controller.get_cursor().get_global_transform();
	var cursor_pos = cursor_xform.origin + cursor_xform.basis.y * CollisionHeightAdjust;
	collision_point = cursor_pos;
	
	# check for snapping cursor to the track
	var snap_junction = null;
	var terrain_collision_point = collision_point;
	var snap_to_track_result = find_snap_to_track(terrain_collision_point);
	var connection = null;
	
	connection_cursor_inst.set_visible(false);
	curve_editor.set_ignore_collisions_list([]);
	if (snap_to_track_result != null):
		var track = snap_to_track_result["node"];
		curve_editor.set_ignore_collisions_list([track]);
		var snap_to_track_transform = snap_to_track_result["transform"];
		
		connection_cursor_inst.set_visible(true);
		connection_cursor_inst.set_global_transform(snap_to_track_transform);
		
		snap_junction = snap_to_track_result["snap_to_junction"];
		# only allowed to connect the first connection to an outgoing connection (or any connection on a station)
		# to end a track, it must end on an incomming connection
		if (snap_junction != null):
			connection = find_best_connection(snap_to_track_result, cursor_pos, snap_junction);
			if (connection):
				connection_cursor_inst.set_global_transform(connection.get_global_transform());
				
				
				
			mouseOverColliderThisFrame = connection;
		else:
			var nothing = 0;
			nothing += 1;
		
		collision_point = snap_to_track_transform.origin;
		#BLog.debug("collision_point from snap_to_track_transform:" + collision_point);
		
		# the first time we connect from a track, it points in the same direction as the track
		# we snap too, else when we finish, it needs to angle into the same direction as the track
		collision_tangent = -snap_to_track_transform.basis[2];
		
		# tilt the tangent towards the cursor with a tiny amount, this will allow the user to move the mouse
		# and then change the shape of the curve potentially - this is visible when connecting
		# for example the front of a train station stright to the back
		collision_tangent += (collision_point - terrain_collision_point).normalized() * 0.001;

	# user clicks, so lock thier mouse over collider
	if (leftMousePressed):		
		# divide the track at the click point
		if (!curve_editor.is_active() and !connection and snap_to_track_result != null and shiftDown):
			rpc("_divide_track_master_rpc", snap_to_track_result);
			leftMousePressed = false # handled the event
			return;
		
		if (!curve_editor.is_active()):
			curve_editor.start(track_inst.radius, collision_point, collision_tangent, snap_to_track_result);
			startConnection = connection;
			dialog.set_visible(true);
		elif (curve_editor.is_good()):
			curve_editor.bake_end_point(collision_point, collision_tangent, snap_to_track_result);
			if (connection != null):
				endConnection = connection;
				finish_laying_track();
	
	curve_editor.update_end_point(collision_point, collision_tangent);
	
	if (leftMousePressed):
		if (double_click):
			_on_OkBtn_pressed();
			
		leftMousePressed = false # handled the event
		double_click = false;
		
	return;

func undo_last_track():
	finishedLayingTrack = false; # if the track was finished, it now is not finished!
	endConnection = null;
	if (!curve_editor.undo_last()):
		startConnection = null;
	return;
	
func finish_laying_track():
	curve_editor.end()
	finishedLayingTrack = true
	controller.mouse_over_enabled = true
		
	if (mouseOverCollider):
		mouseOverCollider.setMouseOver(false)
		mouseOverCollider = null
	
	
# use this to get a quote of the track cost before a track is built
func get_finances_for_length(track_inst, length):
	var f = track_inst.get_finances().duplicate();
	f.purchase_cost *= length;
	f.maintenance_cost *= length;
	return f;
	
	
func _update_ui():	
	var f = get_finances_for_length(track_inst, curve_editor.get_length())
	BUtil.find_child(dialog, "LengthLbl").set_text("%dM" % curve_editor.get_length())
	BUtil.find_child(dialog, "CostPerMetreLbl").set_text(Util.format_currency(track_inst.get_finances().get_purchase_cost()))
	
	var totalCost = f.get_purchase_cost();
	var totalCostStr = Util.format_currency(totalCost);
	BUtil.find_child(dialog, "TotalCostLbl").set_text(totalCostStr);
	
	BUtil.find_child(dialog, "MaintenanceCostPerMetreLbl").set_text(Util.format_currency(track_inst.get_finances().get_maintenance_cost(), true))
	BUtil.find_child(dialog, "MaintenanceTotalCostLbl").set_text(Util.format_currency(f.get_maintenance_cost()))
	
	get_company().update_purchase_button(okBtn, f.get_purchase_cost(), curve_editor.get_curve_list().size() > 0);

	
# the user can shift + left click to divide the tracks at no cost
master func _divide_track_master_rpc(snap_to_track_result):
	var builder_name = world.network.next_unique_name(controller.networkId, "TrackBuilder");
	rpc("_divide_track_rpc", builder_name, snap_to_track_result);
	var builder = get_node(builder_name);
	builder.divide_track(snap_to_track_result);
	builder.delete();
	return;
	
remotesync func _divide_track_rpc(builder_name, snap_to_track_result):
	BLog.debug("[createTrainTrackRPC] TrackBuilder created with name: " + builder_name);
	var builder = TrackBuilder.new();
	builder.set_name(builder_name);
	add_child(builder);
	BLog.debug("\tTrackBuilder created with path: " + builder.get_path());
	builder.init(controller, track_inst, junction_inst);
	return;
	
# this is basically the ok button being run on the server
master func _create_train_track_master_rpc(startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list):
	# yes, tracks cost money and lives!
	var length = TrackBuilder.get_curve_list_length(TrackBuilder.unpackCurveList(packedCurveList));
	var f = get_finances_for_length(track_inst, length);
	var purchase_cost = f.get_purchase_cost();
	var company = get_company()
	var loan_amount = company.loan_amount_required(purchase_cost)
	
	# cant actually afford it! abort!
	if (loan_amount == -1):
		return;
	
	loan_amount = company.loan_amount_to_block(loan_amount);
	company.take_out_loan(loan_amount);
	company.pay_cash(purchase_cost, IncomeStatement.TRACKS);
	
	var builder_name = world.network.next_unique_name(controller.networkId, "TrackBuilder");
	rpc("_create_train_track_rpc", builder_name, startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list);
	
	return;

remotesync func _create_train_track_rpc(builder_name, startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list):	
	BLog.debug("[createTrainTrackRPC] TrackBuilder created with name: " + builder_name);
	var builder = TrackBuilder.new();
	builder.set_name(builder_name);
	add_child(builder);
	BLog.debug("\tTrackBuilder created with path: " + builder.get_path());
	builder.init(controller, track_inst, junction_inst);
	builder.build_tracks_and_junctions(startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list);
	EventMgr.trigger(EventMgr.Id.TRACK_CONSTRUCTION_STARTED, { "sender": self, "start_connection": startConnectionPath, "end_connection": endConnectionPath });


func _on_OkBtn_pressed():
	var startConPath = startConnection.get_path() if (startConnection != null) else null;
	var endConPath = endConnection.get_path() if (endConnection != null) else null;
	rpc("_create_train_track_master_rpc", startConPath, endConPath, TrackBuilder.packCurveList(curve_editor.get_curve_list()), curve_editor.snap_to_track_list);
	_on_CancelBtn_pressed(); # close the dialog
	
	# play any audio if it exists
	if ($OkPressed):
		$OkPressed.trigger();
	
func _on_CancelBtn_pressed():	
	controller.hud.clear_toggled_state_buttons();
	return;
	
func _on_TextureButton_toggled( pressed ):
	# disable in demo
	if (BDatabase.is_build_type("demo") and !available_in_demo):
		return;
		
	toggle_state(pressed);
	return;
	
func get_company():
	return controller.get_company();
