#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

# expects to be at the origin for the spline editor to work!
# https://www.reddit.com/r/godot/comments/5mq7ix/interacting_with_other_objects/

var display_name = null
var networkId = 0 # the network id of the player this belongs too

var current_camera_stack = [];

onready var sm = $StateMachine;


const ResourceNode = preload("../Resource/ResourceNode.gd") # Relative path
const SaveGame = preload("res://Game/SaveGame.gd") # Relative path
onready var input_actions = BUtil.find_child(self, "InputActions");
onready var cursor = BUtil.find_child(self, "Cursor");
onready var handlers = BUtil.find_child(self, "Handlers");

var companyTemplate = preload("res://Company/Company.tscn"); # TODO: make this an export

var sphereRigidBody = preload("res://Player/SphereRigidBody.tscn").instance(); # TODO: make this an export

var context_menu_enabled = true; # determine if we can show context menu
var mouse_over_ui = false;
var mouse_over_enabled = true
var mouseOverNode = WeakRef.new()

var selectedNode = WeakRef.new();

var hud; # the player hud
onready var company = null;

var update_company_hud_timer;

var max_track_distance = 10.0; # should this be set by the track itself?
var vantage_points_visible = false;

onready var world = WorldMgr.get_world(self); 

# ready hasnt been called yet, but we are taking params 
# to configure this class
# and we know if we are network master so free any nodes
func _pre_ready(p_name, netId, is_master):
	add_to_group("players");
	display_name = p_name
	networkId = netId
	return;


func _init():
	return;


func _setup_from_database():
	BLog.debug("Loading Player Prefabs");
	
	var player = BDatabase.get_value("player");
	var attachments = [];
	for item in player.attachments:
		attachments.append(item['resource']);
	
	attachments = Util.mod_modify_attachment_list(self, attachments);
	Util.load_attachment_list(self, attachments);
	BLog.debug("Finished Player prefabs");
	return;


func _ready():
	if (!world):
		return;

	_setup_from_database();
	
	set_physics_process(is_network_master());
	set_process_input(is_network_master());
	set_process_unhandled_input(is_network_master());
	
	#call_deferred("_post_ready");
	
	if (!is_network_master()):
		return;
		
	update_company_hud_timer = Util.create_timer(self, "update_company_hud", 1.0, true);
	
	# terney test
	var ok = false if is_network_master() else true
	
	# Register console commands
	Console.register_command("givecash", {
		method = funcref(self, "give_cash"),
		owner = self,
		description = "Give cash",
		args = ["float"]
	})
	
	Console.register_command("broadcastmsg", {
		method = funcref(self, "broadcast_msg"),
		owner = self,
		description = "broadcast message",
		args = ["string"]
	})
	
	Console.register_command("addevent", {
		method = funcref(self, "add_event"),
		owner = self,
		description = "Show an event",
		args = ["string"]
	})
	
	world.date_time.connect("month_changed", self, "_month_changed")

	sm.connect("state_changed", self, "state_changed");
	
	# connect up signals to allow us to work out where the user has located the mouse (over the world or over some UI)
	var control = BUtil.find_child(hud, "MouseOverContainer");
	control.connect("mouse_entered", self, "_control_mouse_entered");
	control.connect("mouse_exited", self, "_control_mouse_exited");
	
	# setup inputs
	Console.connect("set_console_open", self, "_on_set_console_open");
	
	var action = input_actions.add_action("chat", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "activate_chat");

	action = input_actions.add_action("camera_next", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "camera_next");
	
	action = input_actions.add_action("show_primary_resource_nodes", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_nodes", [ResourceNode.TYPE.PRIMARY_RESOURCE]);
	action.connect("is_released", self, "hide_nodes");
	
	action = input_actions.add_action("show_intermediate_resource_nodes", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_nodes", [ResourceNode.TYPE.INTERMEDIATE_RESOURCE]);
	action.connect("is_released", self, "hide_nodes");
	
	action = input_actions.add_action("show_city_nodes", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_nodes", [ResourceNode.TYPE.CITY]);
	action.connect("is_released", self, "hide_nodes");
		
	action = input_actions.add_action("show_vehicle_overlays", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_vehicle_overlays");
	action.connect("is_released", self, "hide_vehicle_overlays");
	
	action = input_actions.add_action("show_warehouses", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_nodes", [ResourceNode.TYPE.WAREHOUSE]);
	action.connect("is_released", self, "hide_nodes");
	
	action = input_actions.add_action("toggle_vantage_points", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "toggle_vantage_points");
	
	action = input_actions.add_action("delete", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "delete_selected");
	
	action = input_actions.add_action("physics_test", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "_physics_test", [sphereRigidBody]);
	
	action = input_actions.add_action("left_click", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "handle_selection");
	
	action = input_actions.add_action("right_click", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "show_context_menu");
	return;
	
	
# just for fun!
func _physics_test(p_event, body_template):
	var mousePos = Util.get_viewport_mouse_position(get_viewport());
	var from = get_current_camera().project_ray_origin(mousePos);
	var impulse = get_current_camera().project_ray_normal(mousePos) * 100;
	
	var body = body_template.duplicate();
	body.transform.origin = from;
	body.apply_impulse(from, impulse);
	
	add_child(body);



func get_selection_effect_mgr():
	return $SelectionEffectMgr;


func state_changed(from_state, to_state):
	EventMgr.trigger(EventMgr.Id.PLAYER_STATE_CHANGED, { "sender": self, "from_state": from_state, "to_state": to_state });
	return;


# closest thing we have to a destructor
func _exit_tree():
	Console.deregister_commands(self)


func _month_changed(monthIndex):
	hud.ui_dateLbl.set_text(world.date_time.getDateString())


func give_cash(p_amount):
	get_company().receive_cash(p_amount)


# at this point we can do RPC calls
func _post_ready():
	set_player_name(display_name)
	create_new_company()		


func create_new_company():
	if (company != null):
		company.remove_player(self)
		
	company = companyTemplate.instance()
	company.set_name("Company_%d" % networkId)
	
	var companies = world.companies; #BUtil.find_child(WorldMgr.world, "Companies");
	companies.add_child(company)
	
	# name company based on first player to join it
	company.set_display_name("%s's Company" % display_name)
	company.add_player(self)
	return;


# TODO: network sync
func set_player_name(name):
	rpc("set_player_name_rpc", name)


remotesync func set_player_name_rpc(n):
	display_name = n


func get_company():
	return company


func update_company_hud():
	if (!get_company()):
		return
		
	hud.ui_moneyLbl.set_text(Util.format_currency(get_company().get_cash()))


func _update_input_enabled():
	var is_console_open = Console.is_console_open();
	var input_enabled = !is_console_open && !mouse_over_ui;
	set_process_input(input_enabled);
	set_process_unhandled_input(input_enabled);
	input_actions.set_enabled(input_enabled);
	
	
func _on_set_console_open(open):
	_update_input_enabled();
	
	set_process_input(!open);
	set_process_unhandled_input(!open);
	input_actions.set_enabled(!open);


func _set_mouse_over_ui(p_over):
	mouse_over_ui = false;
	return;
	
	
func _control_mouse_entered():
	_set_mouse_over_ui(false);
	mouse_over_ui = false;
	return;


func _control_mouse_exited():
	_set_mouse_over_ui(true);
	mouse_over_ui = true;
	clear_mouse_over();
	return;

func _unhandled_input(event):
	if (sm.get_current_state()):
		sm.get_current_state().state_unhandled_input(event);

	return;


func _input(event):
	if (sm.get_current_state()):
		sm.get_current_state().state_input(event)
		
	return;


func camera_follow_node_changed():
	# the camera is telling us it is now no longer following the node
	if (get_current_camera().get_follow_node() == null):
		set_selection(null);
	return;


func set_selection(node):
	mouseOverNode = weakref(node) if node else WeakRef.new();
	handle_selection(null);
	return;


func handle_selection(p_event):
	if (mouseOverNode.get_ref() == selectedNode.get_ref()):
		return;
		
	# deselect
	if (selectedNode.get_ref() != null):
		selectedNode.get_ref().set_selected(self, false);
		selectedNode = WeakRef.new();
	
	# select node, if it wants to be selected
	if (mouseOverNode.get_ref() && mouseOverNode.get_ref().set_selected(self, true)):
		selectedNode = mouseOverNode;
			
	return;


# right click context menu
func show_context_menu(p_event):
	if (!context_menu_enabled):
		return;
		
	var context_node = mouseOverNode.get_ref();
	if (!context_node):
		context_node = BUtil.get_terrain();
		
	if (context_node):
		# find a context menu handler that will handle the node we are mouseing over
		for handler in handlers.get_children():
			if (handler.handles(context_node) && handler.has_method("show_context_menu")):
				handler.show_context_menu(context_node);
				return;
				
	return;
		
	
func delete_selected(p_event):
	var context_node = selectedNode.get_ref();
	if (context_node):
		# stop following if we are following what we are deleting
		if (context_node == camera_get_follow_node()):
			camera_set_follow_node(null);
			
		clear_mouse_over() # safety first!
		
		# find a context menu handler that will handle the node we are deleting
		for handler in handlers.get_children():
			if (handler.handles(context_node) && handler.has_method("show_context_menu")):
				if (handler.delete(context_node)):
					selectedNode = WeakRef.new();
					return;
#
#
#		# are we allowed to delete it?
#		var selected_company = selected.get_company();
#		if (selected_company):
#			BLog.debug("Trying to delete selected node: " + selected_company.get_display_name() + " but we belongs to: " + get_company().get_display_name());
#
#		if (selected_company != get_company()):
#			return;
#
#		# the build states listen for their props being deleted and apply the appropriate refund
#		selected.delete();
#		selectedNode = WeakRef.new();
		
	return;



func camera_pan_to_node(node):
	get_current_camera().pan_to_node(node);
	return;


func camera_set_follow_node(node):
	get_current_camera().set_follow_node(node);
	return;


func camera_get_follow_node():
	return get_current_camera().get_follow_node();


# mouse picking by raycasting into the screen	
# this will cast through objects and so retusn an array, sorted by distance from camera
func mouse_pick():
	var mousePos = Util.get_viewport_mouse_position(get_viewport());
	var from = get_current_camera().project_ray_origin(mousePos)
	var to = from + get_current_camera().project_ray_normal(mousePos) * 10000
	
	#BLog.debug("vp: " + get_viewport().get_path());
	#BLog.debug("mousePos: " + str(mousePos));
	
	var colList = Util.raycast(from, to);
		
	if (colList.size()):
		pass;
	#if (colList.size()):
		#var xform = Transform();
		#xform.origin = colList[0]["position"];
		#get_node("Cursor").set_global_transform(xform);
		#BLog.debug("Hit phss object: " + String(xform.origin));
	
	
	if (BUtil.get_terrain()):
		var col = BUtil.get_terrain().raycast(from, to);
		if (!col.empty()):
			colList.append(col);
			var xform = Transform();
			xform.origin = col["position"];
			
			xform = BUtil.get_terrain().move_transform_to_ground(xform);
			get_cursor().set_global_transform(xform);
			#BLog.debug("Hit terrain: " + String(xform.origin));
	
	return colList;


func get_cursor():
	return cursor;


func _physics_process(delta):
	Util.assert(is_network_master(), "_physics_process only expected to run on the server"); # asserts on exit game!
	
	if (sm.get_current_state()):
		sm.get_current_state().state_physics_process(delta)
		
	if (mouse_over_enabled && !mouse_over_ui):
		check_mouse_over()    


func clear_mouse_over():
	if (mouseOverNode.get_ref()):
		mouseOverNode.get_ref().set_mouse_over(self, false);
		
	mouseOverNode = WeakRef.new()
	return


# is this quicker than collision detection?
func find_closest_track(cursor_pos):
	var trackList = get_tree().get_nodes_in_group("tracks");
	
	var closest_result = [];
	var closest_distance;
	
	for track in trackList:	
		if (!track.is_visible_in_tree()):
			continue;
			
		var result = track.get_world_curve().closest_distance_to(cursor_pos, max_track_distance);
		if (result.size() > 0):
			closest_distance = result["distance_to_curve"];
			closest_result = result;
			closest_result["node"] = track;
			
	return closest_result;


# check for mouse over cities (resource nodes)
func check_mouse_over():
	var mouseOverThisFrame = WeakRef.new();
	var colList = mouse_pick();

	# havent hit anything yet, so see if colList has hit anything useful
	if (!mouseOverThisFrame.get_ref()):
		for col in colList:
			var collider = col["collider"]
	
			# find a selectable object
			var node = collider;
			while (node.get_parent()):
				if (node.has_method("is_interactive") and !node.is_queued_for_deletion() && node.is_interactive(self)):
					# should the current state have a chance to modify this?
					# should the game mode have a chance to modify this?
					# what about the tutorial for example?
					# maybe i just need an event to say we selected something
					# which the tutorial might want to block
					mouseOverThisFrame = weakref(node);
					break;
					
				node = node.get_parent();
				
			# what we collide with first, we are done
			if (mouseOverThisFrame.get_ref()):
				break;
	
	# we have not hit anything of interest, what about the closest track from where we hit the terrain?
	if (!mouseOverThisFrame.get_ref() and colList.size()):
		var terrain_cursor_pos = colList.back()["position"];
		var closest_track = find_closest_track(terrain_cursor_pos);
		if (closest_track.size()):
			mouseOverThisFrame = weakref(closest_track["node"]);
			
	# no change in what we moused over
	if (mouseOverNode.get_ref() == mouseOverThisFrame.get_ref()):
		return;
		
	clear_mouse_over();
	mouseOverNode = mouseOverThisFrame;
					
	if (mouseOverNode.get_ref()):
		if (!hud.is_visible()):
			return;
		
		mouseOverNode.get_ref().set_mouse_over(self, true);
		
	return;


func end_game():
	set_physics_process(false);
	return;
	


func leave_game():
	Game.end_game();


func join_company(p_company):
	rpc("join_company_rpc", p_company.get_path() if (p_company != null) else null);


remotesync func join_company_rpc(companyPath):
	var c = get_tree().get_root().get_node(companyPath) if (companyPath != null) else null;
	var old_company = get_company();
	if (old_company):
		old_company.remove_player(self);
		
	if (c):
		c.add_player(self);
		
	self.company = c;
	EventMgr.trigger(EventMgr.Id.PLAYER_JOINED_COMPANY, { "sender": self, "player_display_name": get_display_name(), "company": c, "company_display_name": c.get_display_name(), "previous_company": old_company, "previous_company_display_name": old_company.get_display_name() });


func activate_chat(p_event):
	get_chat_panel().activate();
	return;


func show_end_game_panel(p_companies, p_winners):
	var company_paths = [];
	var winner_paths = [];
	
	for c in p_companies:
		company_paths.append(c.get_path());
		
	for w in p_winners:
		winner_paths.append(w.get_path());
		
	rpc("show_end_game_panel_rpc", company_paths, winner_paths)
	return;


master func show_end_game_panel_rpc(p_company_paths, p_winner_paths):
	var companies = [];
	var winners = [];
	
	for p in p_company_paths:
		var c = get_node(p);
		companies.append(c);
		
	for p in p_winner_paths:
		var w = get_node(p);
		winners.append(w);
		
	# TODO: This shouldn't be hard coded...
	get_node("EndGamePanel").show_end_game_panel(companies, winners);
	return;


func show_nodes(p_event, type):
	if (!hud.is_visible()):
		return;
		
	for c in BUtil.find_children_by_class_name(get_node("/root/World"), "ResourceNode", true):
		if c.type == type && c.is_visible():
			c.show_hud(self, true);
			
	return;


func hide_nodes(p_event):
	for c in BUtil.find_children_by_class_name(get_node("/root/World"), "ResourceNode", true):
		c.show_hud(self, false);
		
	return;


func toggle_vantage_points(p_event):
	if (!hud.is_visible()):
		return;
		
	vantage_points_visible = !vantage_points_visible;
	for c in get_tree().get_nodes_in_group("vantage_points"):
		c.set_mouse_over(self, vantage_points_visible);

	return;


func show_vehicle_overlays(p_event):
	if (!hud.is_visible()):
		return;
		
	var company_vehicles = get_company().get_trains();
	for i in range(0, company_vehicles.size()):
		company_vehicles[i].set_mouse_over(self, true);
		
	return;


func hide_vehicle_overlays(p_event):
	var company_vehicles = get_company().get_trains();
	for i in range(0, company_vehicles.size()):
		company_vehicles[i].set_mouse_over(self, false);
		
	return;


func camera_next(p_event):
	var cameras = $Camera.get_children();
	var nextCamIdx = 0;
	for i in range(cameras.size()):
		if (cameras[i].is_current()):
			nextCamIdx = i + 1;
			break;
			
	if (nextCamIdx >= cameras.size()):
		nextCamIdx = 0;
		
	cameras[nextCamIdx].make_current();
	return;


func set_debug_text_visible(vis):
	for p in BUtil.find_children_by_class_name(world, "DebugTextWidget", true):
		p.set_visible(vis);
	return;


func pause_toggle():
	var pause = !get_tree().is_paused();
	get_tree().set_pause(pause);
	return;


func add_state(state):
	sm.add_child(state);
	return;


# add a context menu handler which must have the API
# 	handles(node)
func add_handler(p_handler):
	handlers.add_child(p_handler);
	return;
		

func add_camera(cam):	
	if (!is_network_master()):
		cam.queue_free();
		return;
	
	var camera_container = BUtil.find_child(self, "Camera");
	BUtil.reparent(cam, camera_container);
	
	# set first camera as active
	if (current_camera_stack.empty()):
		current_camera_stack.push_back(cam);
		cam.connect("follow_node_changed", self, "camera_follow_node_changed");
		
	# keep the active camera current
	current_camera_stack.back().make_current();
	return;


func find_camera(name: String):
	for c in get_tree().get_nodes_in_group("cameras"): #$Camera:
		if c.name == name:
			return c;
			
	return null;


func push_camera(cam : Camera):
	current_camera_stack.push_back(cam);
	cam.make_current();


func pop_camera():
	var current = current_camera_stack.back();
	current_camera_stack.pop_back();
	current_camera_stack.back().make_current();


# get the current camera
func get_current_camera():
	return current_camera_stack.back();


func get_vehicles_panel():
	return $VehiclesPanel;


func get_chat_panel():
	return $ChatPanel;

	
func get_events_panel():
	return $EventsPanel;
	
	
# show event that will be sent to the master
master func show_event(p_params):
	if (is_network_master()):
		get_events_panel().show_event(p_params);
	else:
		rpc("show_event", p_params)
	

func _on_SaveBtn_pressed():
	var savegame = SaveGame.new();
	add_child(savegame);
	savegame.save_to_file("res://savegame.save");
	savegame.queue_free();
	hud.hide_game_menu();
	return;


func _on_LoadBtn_pressed():
	var savegame = SaveGame.new();
	add_child(savegame);
	savegame.load_from_file("res://savegame.save");
	savegame.queue_free();
	hud.hide_game_menu();
	return;


func show_vehicle_edit_ui(vehicle, p_editor_state_name, p_flags):
	var vehicle_editor_state = sm.get_state_by_name(p_editor_state_name);
	vehicle_editor_state.show_vehicle_edit_ui(vehicle, p_flags);
	return;


func get_blueprint():
	return BUtil.find_child(self, "Blueprint");


func get_display_name():
	return display_name;


