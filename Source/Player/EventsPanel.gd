#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var panel = find_node("Panel");
onready var event_list = find_node("EventList");

var controller;
var events = {};
var default_time = 5.0; # default display length, unless otherwise specified
var default_icon;

func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_child(self);
	if (!controller.is_network_master()):
		return;
	
	controller.hud.add_dialog_panel(panel);

	
func _ready():
	default_time = Database.get_value("hud/events_panel/default_time", default_time);
	default_icon = load(Database.get_value("hud/events_panel/default_icon", default_icon));
	
	# load data from the database and push into a dictionary for quick access
	var db_events = Database.get_value("hud/events_panel/events", {});
	var values = EventMgr.Id.values();
	var keys = EventMgr.Id.keys();
	for event in db_events:
		var event_id_str = event.event_id;
		var texture_str = event.icon;
		
		# if the event_id is in EventMgr.Id enum, then translate that to a number - it will be activated via listening for that event
		# it may also be triggered manually using show_event
		var event_id = event_id_str;
		var expanded_event = BUtil.merge_dict({ "texture": load(texture_str), "id": event_id }, event);
		
		if (EventMgr.Id.has(event_id_str)):
			var event_id_num = EventMgr.Id.get(event_id_str);
			EventMgr.listen([event_id_num], self, "_on_event");
			events[event_id_num] = expanded_event;
		
		events[event_id] = expanded_event;
		
	return;


func get_event_components(p_ui_item: Control):
	return {
		"ui_item": p_ui_item,
		"timer": p_ui_item.get_meta("timer"),
		"ui_icon": BUtil.find_child(p_ui_item, "Icon"), # a clickable button
		"ui_text": BUtil.find_child(p_ui_item, "Text"),
		"ui_container": BUtil.find_child(p_ui_item, "MarginContainer"),
	};
	
	
# internal function to display an event
func _add_event(p_params: Dictionary): # this needs to accept a click callback -- callable! TODO:
	var event = events[p_params.id];
	var msg = parse_variables(p_params.text if p_params.has("text") else event.text, p_params);
	var time = event.time if event.has("time") else default_time;
	
	var item = get_event_components(event_list.create_item());
	item.ui_icon.texture_normal = event.texture if event.has("texture") else default_icon;
	item.ui_text.set_bbcode(msg);
	
	item.ui_item.set_meta("timer", Util.create_timer(self, "_remove_event", time, true, true, [item.ui_item]));
	_update_height(item.ui_item);
	

# keep event to minimum size possible
func _update_height(p_ui_item):
	var item = get_event_components(p_ui_item);
	
	var height = BUtil.get_rich_text_label_height(item.ui_text);
	item.ui_text.set_custom_minimum_size(Vector2(item.ui_text.rect_min_size.x, height));

	var min_size = item.ui_container.get_minimum_size();
	var min_size_rect = Vector2(min_size.x, min_size.y);
	item.ui_item.rect_min_size = min_size_rect;
	item.ui_item.rect_size = min_size_rect;


func _remove_event(p_ui_item):
	p_ui_item.free(); # ideally fade out!
	
	
func _on_event(p_params: Dictionary):
	_add_event(p_params);
	

# a public method to show an event without using the event mgr
func show_event(p_dict: Dictionary):
	_add_event(p_dict);
	
	
func parse_variables(string: String, p_dict: Dictionary):
	for key in p_dict:
		var value = p_dict[key];
		
		# ignore objects, we can only handle strings, or numbers
		if (typeof(value) == TYPE_OBJECT):
			continue;
			
		string = string.replace("$(" + key + ")", value);

	return string;

