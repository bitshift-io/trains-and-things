#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool

extends Spatial

export(bool) var visible_in_editor = true;
export(Texture) var texture;
export(Color) var color = Color(1,1,1,0.8);

onready var editor_icon = BUtil.find_child(self, "EditorIcon");
onready var ui_icon = BUtil.find_child(self, "TextureIcon");

func _ready():
	if Engine.editor_hint:
		if visible_in_editor:
			# editor
			var material = editor_icon.material_override.duplicate();
			editor_icon.material_override = material;
			material.albedo_texture = texture;
			material.albedo_color = color;
	else:
		editor_icon.queue_free();
		ui_icon.texture = texture;
		ui_icon.queue_free(); # TODO: use in game at specific zoom level
	return;
