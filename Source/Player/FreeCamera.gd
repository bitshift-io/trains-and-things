#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Camera

var pan_keyboard_speed;
export(float) var sprint_speed_multiplier = 3.0; # multiplier
export(float) var mouse_sensitivity = 0.01;
export(float) var height_from_terrain = 1.0;

var yaw = 0;
var pitch = 0;

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	controller.add_camera(self);
	return;
	
func set_follow_node(node):
	return;
	
func get_follow_node():
	return null;
	
func _ready():
	set_pause_mode(PAUSE_MODE_PROCESS);
	set_process_input(true);
	set_process(true);
	set_name("FreeCamera");
	add_to_group("cameras");
	Database.connect('settings_changed', self, 'on_settings_changed');
	on_settings_changed();
	return;

	
func on_settings_changed():
	pan_keyboard_speed = Database.get_value(Database.CAMERA_MOVE_SPEED, 100.0);
	

func _notification(what):
	if (what == 50): #NOTIFICATION_BECAME_CURRENT):
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
		terrain_col_check();
		
	if (what == 51): #Camera.NOTIFICATION_LOST_CURRENT):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
		
	return;
	
func terrain_col_check():
	if (BDatabase.is_build_type("developer") or BDatabase.get_value("dev/developer", 0) == 1):
		return; # disable terrain collision for developers!
	
	if (BUtil.get_terrain()):
		var col = BUtil.get_terrain().raycast_down(global_transform.origin);
		if (!col.empty()):
			var pos = col["position"];
			pos.y += height_from_terrain;
			if (pos.y > global_transform.origin.y):
				global_transform.origin.y = pos.y;
	
func _input(event):
	if (!is_current()):
		return;
		
	if (Console.is_console_open()):
		return;
	
	if event is InputEventMouseMotion:
		yaw = fmod(yaw  - event.get_relative().x * mouse_sensitivity , 360);
		pitch = max(min(pitch - event.get_relative().y * mouse_sensitivity, 90), -90);
		
		var t = Transform();
		var t_yaw = t.rotated(Vector3(0, 1, 0), yaw);
		var t_pitch = t.rotated(Vector3(1, 0, 0), pitch);
		
		var t_final = t_yaw * t_pitch;
		transform.basis = t_final.basis;
		
	return;

func _process(delta):
	if (!is_current()):
		return;
		
	if (Console.is_console_open()):
		return;
		
	#mouse movement
	var speed = pan_keyboard_speed;
	if (Input.is_action_pressed("move_sprint")):
		speed *= sprint_speed_multiplier;
		
	if (Input.is_action_pressed("move_slow")):
		speed /= sprint_speed_multiplier;
	
	var dir = Vector3();
	if (Input.is_action_pressed("move_forward")):
		dir += Vector3(0,0,1);
	if (Input.is_action_pressed("move_backward")):
		dir -= Vector3(0,0,1);
	if (Input.is_action_pressed("move_left")):
		dir += Vector3(1,0,0);
	if (Input.is_action_pressed("move_right")):
		dir -= Vector3(1,0,0);
	if (Input.is_action_pressed("move_up")):
		dir -= Vector3(0,1,0);
	if (Input.is_action_pressed("move_down")):
		dir += Vector3(0,1,0);

	dir = dir.normalized();
	_translate(dir, delta * speed);
		
	terrain_col_check();
	return;
	
func _translate(axis : Vector3, delta: float):
	var t = get_transform();
	var translation = t.origin;
	translation = translation - t.basis * axis * delta;
	t.origin = translation;
	set_transform(t);
	
