#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# this is the vehicle UI panel that sits inside dialogs
#
extends Panel

var VehicleEditorState = preload("VehicleEditorState.gd");

onready var ui_route_tree = BUtil.find_child(self, "RouteTree");
onready var ui_revenues_lbl = BUtil.find_child(self, "RevenuesLbl");
onready var ui_expenses_lbl = BUtil.find_child(self, "ExpensesLbl");
onready var ui_profit_lbl = BUtil.find_child(self, "ProfitLbl");
onready var ui_vehicle_title = BUtil.find_child(self, "VehicleTitle");

onready var ui_edit_btn = BUtil.find_child(self, "EditButton");
onready var ui_clone_btn = BUtil.find_child(self, "CloneButton");
onready var ui_delete_btn = BUtil.find_child(self, "DeleteButton");

var vehicle;
var controller;
var updating = false;

func init(p_controller, p_vehicle):
	vehicle = p_vehicle;
	controller = p_controller;
		
	# connect signals to cause UI to update
	vehicle.connect("vehicle_state_changed", self, "_update_vehicle_state");
	vehicle.connect("vehicle_display_name_changed", self, "_update_vehicle_name");
	_update_vehicle_state(vehicle);
	_update_vehicle_name(vehicle);
	
	var finances = vehicle.get_finances();
	finances.connect("finances_changed", self, "_update_vehicle_finances");
	_update_vehicle_finances(finances);
	
	ui_edit_btn.set_button_icon(ui_edit_btn.get_icon("Edit", "EditorIcons"));
	ui_clone_btn.set_button_icon(ui_clone_btn.get_icon("Duplicate", "EditorIcons"));
	ui_delete_btn.set_button_icon(ui_delete_btn.get_icon("Remove", "EditorIcons"));
	return;

func _ready():
	Util.init_tree_with_titles(ui_route_tree, ["Route"]);
	return;

func _on_LocateButton_pressed():
	controller.set_selection(vehicle);
	return;
	
func _update_vehicle_name(p_vehicle):
	updating = true;
	ui_vehicle_title.set_text(vehicle.get_display_name());
	updating = false;

func _update_vehicle_state(p_vehicle):
	# below here chugs! you can see the micro stutter then this code is called
	# it needs a revamp to use icons instead of a tree anyway
	# and could be event based?
	# connect to signal on the vehicle?
	
	# update the route list - either select it or change bg colour
	var vehicle_route = vehicle.get_vehicle_route();
	Util.resize_tree_item_children(ui_route_tree, ui_route_tree.get_root(), vehicle_route.size());
	var route_tree_items = Util.getTreeItemChildren(ui_route_tree.get_root());
	var route_to_highlight = vehicle.get_current_or_destination_name_based_on_state();
	for i in range(0, vehicle_route.size()):
		var item  = route_tree_items[i];
		var name = vehicle_route.get_resource_node(i).name;
		item.set_text(0, name);
		if (item.get_text(0) == route_to_highlight):
			item.set_custom_bg_color(0, Color(0.5, 0.5, 0.5, 0.5));
		else:
			item.set_custom_bg_color(0, Color(0.5, 0.5, 0.5, 0.0));
		
func _update_vehicle_finances(p_finances):
	var finances = vehicle.get_finances();
	ui_revenues_lbl.set_text(Util.format_currency(finances.get_life_time_revenues()));
	ui_expenses_lbl.set_text(Util.format_currency(-finances.get_life_time_expenses())); # make negative so we know its an expense
	ui_profit_lbl.set_text(Util.format_currency(finances.get_life_time_profit()));
	return;
	
func search_match(text):
	var lower_text = text.to_lower();
	
	# match resource node name
	if (vehicle.get_display_name().to_lower().match(lower_text)):
		return true;
#
#	# match resource name
#	for resource_data in resource_node.get_resource_data_list():
#		var res_name = resource_data.get_market_resource().name;
#		if (res_name.to_lower().match(lower_text)):
#			return true;
#
#		if ("supply".match(lower_text) && resource_data.isSupplied):
#			return true;
#
#		if ("demand".match(lower_text) && resource_data.isDemanded):
#			return true;
			
	return false;

func _on_VehicleTitle_text_entered( text ):
	if (updating):
		return;
		
	vehicle.set_display_name(text);

func _on_EditButton_pressed():
	vehicle.show_vehicle_edit_ui(VehicleEditorState.Flags.FLAG_EDIT);
	return;

func _on_CloneButton_pressed():
	vehicle.show_vehicle_edit_ui(VehicleEditorState.Flags.FLAG_CLONE);
	return;

func _on_DeleteButton_pressed():
	vehicle.delete();
