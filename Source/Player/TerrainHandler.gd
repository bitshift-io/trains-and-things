#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Basically a null handler, means the player has right clicked over terrain
# here we show the build menu
#

extends Node

var controller;

const EDIT = 0;
const FOLLOW = 1;
const DELETE = 2;
const CLONE = 3;

var node: WeakRef;


func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_handler(self);
	return;
	
	
func show_context_menu(p_node):
	node = weakref(p_node);
	
	var context_menu = controller.hud.get_context_menu();
	context_menu.clear();
	
	var state_buttons_container = controller.hud.get_state_button_container();
	for state_button in state_buttons_container.get_children():
		var btn_name = state_button.get_meta("context_menu_name");
		context_menu.add_item(btn_name);
	
	controller.hud.popup_context_menu(self, "_context_menu_pressed");
	return;
	
	
func _context_menu_pressed(p_context_menu, p_id):
	if (!node.get_ref()):
		return;
		
	var state_buttons_container = controller.hud.get_state_button_container();
	var state_button = state_buttons_container.get_child(p_id);
	state_button.set_pressed(true);
	return;

	
	
func handles(p_node):
	var is_terrain = BUtil.is_class_name(p_node, "BTerrain");
	return is_terrain;
	

func delete(p_node):
	return false;
