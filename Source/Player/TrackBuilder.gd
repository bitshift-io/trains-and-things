#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# this class handles taking information from the RailEditorState
# and modifying the world to make it real
# it does this over time to make it look like the rail is being built over time as opposed to instantly
# this also helps distribute the load over many frames
#
extends Spatial

signal track_spawned(p_track);

enum SpawnType { 
	INSTANT, 
	ANIMATED,
}

export(NodePath) var junction_path; # the prefab we place between mesh segments
export(NodePath) var track_path; # the prefab we are laying down and stretching and deforming

var junction_inst; # the prefab we place between mesh segments
var track_inst; # the prefab we are laying down and stretching and deforming
var controller;

var snap_to_track_list;
var curve_list;
var curve_list_index;
var prev_junction;
var junctionList = [];
var trackList = [];
var start_connection;
var end_connection;

var junction_parent;
var track_parent;

var is_building_template = false;

onready var world = WorldMgr.get_world(self);

const TrackCurveRadius = 20;

func _init():
	return;
	
func _ready():
	if (!world):
		return;
		
	junction_parent = world.junctions;
	track_parent = world.tracks
	
	var curve_points = BUtil.find_children_by_class_name(self, "Position3D", true);
	if (curve_points.size() <= 0):
		return;
		
	#track_inst = get_node(track_path);
	#if (!track_inst): # need a track instance to determine the curve
	#	return;
		
	var curve = Util.newBArcLineCurve(TrackCurveRadius);#track_inst.radius);
	var snap_to_track_list = [];
	for c in curve_points:
		var curve_origin = c.get_transform().origin;
		curve.add_point(curve_origin);
		snap_to_track_list.append(null);

	# if we are building the track as attached to some prop,
	# just change the parent to be the parent
	junction_parent = get_parent();
	track_parent = get_parent();
	
	var parent_path = track_parent.get_path();
	var track_inst = get_node(track_path);
	var juntion_inst = get_node(junction_path);	
	init(controller, track_inst, juntion_inst);
	call_deferred("build_tracks_and_junctions", null, null, packCurveList([curve]), snap_to_track_list);
	return;

func set_as_building_template():
	is_building_template = true;
	return;
	
static func packCurveList(curveList):
	var array = []
	for curve in curveList:
		var cArray = Util.packCurve(curve)				
		array.append(cArray)
	return array
	
static func unpackCurveList(packedCurveList):
	var curveArray = []
	if (packedCurveList):
		for cArray in packedCurveList:
			var curve = Util.unpackCurve(cArray, TrackCurveRadius) #track_inst.radius)
			curveArray.append(curve)
				
	return curveArray;
	
# given a curve list, compute the length
static func get_curve_list_length(p_curve_list):
	var l = 0;
	for curve in p_curve_list:
		l += curve.get_baked_length();
		
	return l;
		
	
func init(controller, track_inst, junction_inst):
	#set_network_mode(NETWORK_MODE_MASTER if get_tree().is_network_server() else NETWORK_MODE_SLAVE);
	set_network_master(1); # server is always the master
	
	self.controller = controller;
	self.track_inst = track_inst;
	self.junction_inst = junction_inst;
	track_inst.set_visible(false);
	junction_inst.set_visible(false);
	track_inst.set_as_building_template();
	junction_inst.set_as_building_template();
	return;
	
#func build_tracks_and_junctions(startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list):
#	if (!get_tree().is_network_server()):
#		return;
#		
#	rpc("build_tracks_and_junctions_rpc", startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list);
#	return;
	
# this only runs on the server
master func build_tracks_and_junctions(startConnectionPath, endConnectionPath, packedCurveList, snap_to_track_list):
	#assert(get_tree().is_network_server());
	if (!get_tree() || !get_tree().is_network_server()):
		return;
	
	self.snap_to_track_list = snap_to_track_list;
	
	# resolve start and end connection and unpack the curves
	start_connection = get_tree().get_root().get_node(startConnectionPath) if (startConnectionPath != null) else null;
	end_connection = get_tree().get_root().get_node(endConnectionPath) if (endConnectionPath != null) else null;
	curve_list =  unpackCurveList(packedCurveList);
	curve_list_index = 0;
	
	# there should be a snap for each end of each curve
	var snap_size = snap_to_track_list.size();
	Util.assert((curve_list.size() * 2) == snap_size, "Size mismatch in TrackBuilder");
	
	# create tracks with junctions in between
	# snapping to an existing junction or connection..
	if (start_connection != null):
		prev_junction = start_connection;
	# snapping to a new junction...
	else:
		var s = snap_to_track_list[0]; # the snap point at the start of the first track
		if (s != null):
			prev_junction = divide_track(s);
		else:
			prev_junction = create_junction(curve_list[0].get_transform_from_distance_along_curve(0));
	
	build_next_track(); # now build the first track instantly
	return;
	
func build_next_track():
	assert(get_tree().is_network_server());
	
	if (curve_list_index >= curve_list.size()):
		# done!
		EventMgr.trigger(EventMgr.Id.TRACK_CONSTRUCTION_FINISHED, { "sender": self, "start_connection": start_connection.get_path() if start_connection else "", "end_connection": end_connection.get_path() if end_connection else "" });
		delete();
		return;
		
	# build the current track segment
	var c = curve_list[curve_list_index];
	var s = snap_to_track_list[(curve_list_index * 2) + 1]; # the snap point at the end of the track
	var junction = null;
	
	# snap to an existing terminating junction or connection	
	if (curve_list_index == (curve_list.size() - 1)):
		if (end_connection != null):
			junction = end_connection;
	
	if (junction == null):		
		# if there is some snapping, divide the track and use the returned junction
		if (s != null):	
			junction = divide_track(s);	
		# ending in thin air
		else:
			junction = create_junction(c.get_transform_from_distance_along_curve(c.get_baked_length()));				
	
	create_track(prev_junction, junction, c, SpawnType.ANIMATED);
	prev_junction = junction;


	# setup timer to build the next segment
	curve_list_index += 1;
	
#	else:
#		# TODO: this seems woffly out of whack with the track effect, does the track builder need to control the track fade in effect?
#		var build_time = curve_list[curve_list_index].get_baked_length() / track_inst.build_rate;
#		Util.create_timer(self, "build_next_track", build_time, true, true);
	
	return;
	
func get_company():
	if (!controller):
		return null;
	return controller.get_company();
	
master func create_track(from, to, curve, p_spawn_type):
	var from_path = from.get_path();
	var to_path = to.get_path();
	var packed_curve = Util.packCurve(curve);
	var name = world.network.next_unique_name(controller.networkId, "Track");
	#BLog.debug("master create_track: " + name);
	rpc("_create_track_rpc", name, from_path, to_path, packed_curve, p_spawn_type);
	return _create_track(name, from, to, curve, p_spawn_type);
	
puppet func _create_track_rpc(name, from, to, curve, p_spawn_type):
	#BLog.debug("slave _create_track_rpc: " + name);
	_create_track(name, get_node(from), get_node(to), Util.unpackCurve(curve, track_inst.radius), p_spawn_type);
	
func _create_track(name, from, to, curve, p_spawn_type):
	#BLog.debug("_create_track start: " + name);

	if (curve.get_baked_length() <= 0):
		#BLog.warning("Track has length zero, what good is it creating it?");
		pass;
	
	var fromConnection = from
	var toConnection = to
	
	if (BUtil.is_class_name(from, "Junction")):
		fromConnection = from.getConnections()[1];

	if (BUtil.is_class_name(to, "Junction")):
		toConnection = to.getConnections()[0];
			
	var track = track_inst.duplicate();
	
	if (is_building_template):
		track.set_as_building_template();
	
	track.spawn(track_parent, get_company(), fromConnection, toConnection, curve, name);
	
	if (p_spawn_type == SpawnType.ANIMATED):
		if (get_tree().is_network_server()):
			track.connect("track_build_complete", self, "_on_track_build_complete");

		track.play_spawn_effect();
		
	track.set_visible(true);
	trackList.append(track);
	emit_signal("track_spawned", track);
	
	Util.assert(fromConnection.getTracks().has(track), "Problem with fromConnection");
	Util.assert(toConnection.getTracks().has(track), "Problem with toConnection");
	#BLog.debug("_create_track with path: " + track.get_path());
	return track;
	
func _on_track_build_complete(track):
	self.build_next_track();
	return;

master func create_junction(xform):
	#BLog.debug("master func create_junction");
	var name = world.network.next_unique_name(controller.networkId, "Junction");
	rpc("_create_junction_rpc", name, xform);
	return _create_junction(name, xform);
	
puppet func _create_junction_rpc(name, xform):
	#BLog.debug("slave func _create_junction_rpc");
	_create_junction(name, xform);

func _create_junction(name, xform):
	#BLog.debug("_create_junction: " + name);
	var junction = junction_inst.duplicate();
	junction.spawn(junction_parent, get_company(), xform, name);
	junction.set_visible(true);
	junctionList.append(junction);
	#BLog.debug("_create_junction created with path: " + junction.get_path());
	return junction;
	
func subdivide_tracks():
	Util.assert(get_tree().is_network_server(), "subdivide_tracks is only to be run on the server");
	# go through snap_to_track_list and subdivide the tracks
	# delete the old tracks and generate new tracks
	var track_divide_map = {}; # map of tracks and division points
	var track_divide_map_index_map = {}; # map each division point to an index
	
	# only deal with the start point of each curve
	for i in range(0, snap_to_track_list.size(), 2):
		var s = snap_to_track_list[i];
		if (s == null):
			continue;
			
		var middle_junction = divide_track(s);
		
	return;
	
func divide_track(s):
	#BLog.debug("divide_track");
	
	Util.assert(get_tree().is_network_server(), "divide_track is only to be run on the server");
	var track_node = get_tree().get_root().get_node(s["node_path"]);
	var divide_dist = s["distance_along_curve"];
	var sub_curves = track_node.get_world_curve().subdivide([divide_dist]);
	
	# curve is divded right at the start or end, no point making new track,
	# just return the junction at the nearest end
	if (sub_curves.size() == 1):
		var junction_idx = track_node.START if divide_dist < (track_node.get_world_curve().get_baked_length() * 0.5) else track_node.END;
		return track_node.get_junction(junction_idx);
		
	if (sub_curves.size() != 2):
		Util.assert(false, "What is going on?"); # what is going on?
		return;	

	# the occupant might be on the track, yet it might be just the rear carriage
	# so we want to work out of the occupant's current track is this track before
	# computing an offset!
	var occupant = track_node.get_occupant();
	
	# clear track/junction occupancy
	track_node.clear_occupant_if(occupant);
	track_node.get_start_junction().clear_occupant_if(occupant);
	
	if (occupant):
		var occupant_track = occupant.get_current_track();
		if (occupant_track != track_node):
			occupant = null;
		
	var track_offset = occupant.pathOffset if (occupant) else 0.0;
	
	#var junction2 = create_junction(s["transform"]);	
	#return junction2; # TEST
	
	var junction = create_junction(s["transform"]);
	var tracks = [];
	
	if (sub_curves.size() > 0):
		var end_connection = junction.getIncomingConnections()[0];
		var track = create_track(track_node.getConnection(track_node.START), end_connection, sub_curves[0], SpawnType.INSTANT);
		tracks.append(track);
		
	if (sub_curves.size() > 1):
		var start_connection = junction.getOutgoingConnections()[0];
		var track = create_track(start_connection, track_node.getConnection(track_node.END), sub_curves[1], SpawnType.INSTANT);
		tracks.append(track);
		
	if (occupant != null):
		Util.assert(tracks.size() == 2, "Track size error!");
		var track_to_occupy = tracks[0];
		var new_track_offset = track_offset;
		if (track_offset >= divide_dist):
			track_to_occupy = tracks[1];
			new_track_offset -= tracks[0].get_world_curve().get_baked_length();
			
		occupant.move_to_track(track_to_occupy, new_track_offset);
	
	track_node.delete(); 
	return junction;

func delete():
	rpc("delete_master_rpc");
	
master func delete_master_rpc():
	if (!get_tree().is_network_server()):
		return;

	rpc("delete_rpc");
	
remotesync func delete_rpc():
	queue_free();
