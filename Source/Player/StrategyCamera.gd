#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# How the camera works
# A bounding volume is created
# the top of the bounding volume is point 0 and point 1 is as we approach the terrain
# so zoom level of 0 is maximum zoom out
# zoom level 1 is maximum zoom in

extends Camera

# pitch
export(float) var maximum_pitch_angle = -75;
export(float) var minimum_pitch_angle = -20;
export(Texture) var pitch_by_height;

# steps
export(int) var zoom_steps = 8; # the amount of mwheel clicks to get from min to max zoom level
export(Texture) var step_by_height;

# zoom/height/step settings
export(float) var height_from_terrain = 50.0; # dont allow terrain any closer to terrain than this
#var steps = []; # bake our zoom level presets into here for quick lookup

var zoom = 1; # current zoom value. # all the way zoomed out = 1, 0 = all the way zoomed in
var zoom_target = zoom; # all the way zoomed out = 1, 0 = all the way zoomed in

var last_zoom_target = zoom;
var last_zoom_time = 0;
export(float) var zoom_tween_time = 0.2; # how long to make the transition

var zoom_elapsed_max = 100;
var zoom_in_last = 0;
var zoom_out_last = 0;

var zoom_speed = 2.0; # TODO: expose this to the options


# start position tweaks
var start_scale = 0.8; # 80% of the sphere radius which fits the bounding box of the map
var start_offset = Vector3(1, 1, 1.01); # camera offset scale based on magic

var aabb; # bounding volume of camera
var aabb_pad = 0.2; # multiplier of aabb largest dimension - extra padding to add to each axis for camera

# keyboard pan settings
var pan_keyboard_speed;
export(float) var sprint_speed_multiplier = 3.0; # multiplier

export(Texture) var end_keyboard_pan;
onready var end_keyboard_pan_curve = end_keyboard_pan.get_curve(); # store the above curve
export(float) var end_keyboard_pan_ease_time = 0.1;
var last_end_keyboard_pan_time = 0;
var last_pan_dir = Vector3();
var pan_position = Vector3(); # TODO: what is this?

# mouse grab pan settings
var pan_mouse_grab_active = false;
var pan_mouse_grab_raycast_world_position = Vector3();

var follow_node = WeakRef.new();

onready var input_actions = get_node("InputActions");

var controller;

signal follow_node_changed();


func apply_to_controller(p_controller):
	controller = p_controller;
	
	if (!controller.is_network_master()):
		return;
		
	controller.add_camera(self);
	return;


func set_follow_node(node):
	if (node == null):
		follow_node = WeakRef.new();
		emit_signal("follow_node_changed");
		return;
		
	follow_node = weakref(node);
	emit_signal("follow_node_changed");
	return;


func get_follow_node():
	return follow_node.get_ref();


# Helper functions
func move_towards(p_current, target, maxdelta):
	if abs(target - p_current) <= maxdelta:
		return target
	return p_current + sign(target - p_current) * maxdelta


func _ready():
	# group
	set_name("StrategyCamera");
	add_to_group("cameras");
	
	# set position
	set_start_position();
		
	# input
	set_process_input(true); # this is required as it just tells us when some external class disables input on us
	set_process(true);
	
	# keybaord pan
	input_actions.add_actions(["move_forward", "move_backward", "move_left", "move_right", "move_sprint", "move_slow"], input_actions.HandledMode.HM_UNHANDLED);
	
	# mouse pan
	var action = input_actions.add_action("pan", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "start_mouse_grab_pan");
	action.connect("is_released", self, "end_mouse_grab_pan");
	
	# mouse wheel zoom
	action = input_actions.add_action("zoom_in", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "zoom_in");
	action = input_actions.add_action("zoom_out", input_actions.HandledMode.HM_HANDLED);
	action.connect("is_pressed", self, "zoom_out");
	
	Database.connect('settings_changed', self, 'on_settings_changed');
	on_settings_changed();
	return;

	
func on_settings_changed():
	pan_keyboard_speed = Database.get_value(Database.CAMERA_MOVE_SPEED, 100.0);
	zoom_speed = Database.get_value(Database.CAMERA_ZOOM_SPEED, 2.0);


func inside_aabb(point, axis=-1):
	if (axis == -1):
		return aabb.has_point(point);
	else:
		var larger_than_min = ( point[axis] >= aabb.position[axis] );
		var smaller_than_max = ( point[axis] <= aabb.position[axis] + aabb.size[axis] );
		return (larger_than_min and smaller_than_max);


func get_position_at_distance(distance):
	var transform = get_global_transform();
	var dir = transform.basis.z;
	var pos = transform.origin;
	var end_pos = dir * distance;
	return end_pos;


func set_start_position():
	var terrain = BUtil.get_terrain();
	if (!terrain):
		aabb = AABB();
		aabb.grow(10000);
		return;
	
	set_rotation_degrees(Vector3(maximum_pitch_angle,0,0));
	
	# sphere from aabb
	aabb = terrain.get_aabb();
	var end = aabb.end;
	var start = aabb.position;
	var aabb_origin = (end + start) * 0.5;
	var aabb_diameter = (end - start).length() * start_scale;
	
	# camera
	var div = sin(deg2rad(fov * 0.5));
	var distance = (aabb_diameter * 0.5) / div;
	
	# no further than far plane, kthxbai!
	# because the camera is on an angle at the most zoom, we need to use 1500 as our magic number fo the top part of the map that can still escape the far plane
	# the magic number is based on the angle, so if you change camera angle at max zoom, you will need to adjust the magic number
	var max_dist = (far - 1500.0) - aabb_origin.y;
	distance = min(distance, max_dist);
	
	var target_position = get_position_at_distance(distance);
	var offset = target_position * start_offset;
	set_translation(offset);
	
	# calculate bounding volume
	aabb = aabb.expand(offset);
	aabb_pad = max(aabb.size.x, aabb.size.y) * aabb_pad; # percentage of largest size of terrain
	aabb = aabb.expand(end + Vector3(aabb_pad, 0, aabb_pad));
	aabb = aabb.expand(start - Vector3(aabb_pad, 0, aabb_pad));
	return;


func zoom_in(p_event):
	#print("" + str(OS.get_ticks_msec()) + "," + str(p_event.time) + "," + str(Engine.get_frames_per_second()));
	
	var elapsed = 0;
	
	# TODO: if get_time exists, and time is non-zero, then time is supported - only on linux for now!
	if (p_event.has_method("get_time") && p_event.time):
		elapsed = min(p_event.time - zoom_in_last, zoom_elapsed_max);
		zoom_in_last = p_event.time;
	else:
		elapsed = min(OS.get_ticks_msec() - zoom_in_last, zoom_elapsed_max);
		zoom_in_last = OS.get_ticks_msec();
		
	if (elapsed == 0):
		return;
		
	var zoom_amt = (1.0 / elapsed) * zoom_speed;
	#print("zoom in elapsed: " + str(elapsed));
	#print("zoom in: " + str(zoom_amt));
	
	zoom_target = max(0.0, zoom_target - zoom_amt);



func zoom_out(p_event):
	#print("" + str(OS.get_ticks_msec()) + "," + str(p_event.time) + "," + str(Engine.get_frames_per_second()));
	
	var elapsed = 0
	
	# TODO: if get_time exists, and time is non-zero, then time is supported - only on linux for now!
	if (p_event.has_method("get_time") && p_event.time):
		elapsed = min(p_event.time - zoom_out_last, zoom_elapsed_max);
		zoom_out_last = p_event.time;
	else:
		elapsed = min(OS.get_ticks_msec() - zoom_out_last, zoom_elapsed_max);
		zoom_out_last = OS.get_ticks_msec();
		
	if (elapsed == 0):
		return;
	
	var zoom_amt = (1.0 / elapsed) * zoom_speed;
	#print("zoom out elapsed: " + str(elapsed));
	#print("zoom out: " + str(zoom_amt));
	
	zoom_target = min(1.0, zoom_target + zoom_amt);



func _process(delta):
	if (!is_current()):
		return;
		
	# should this just be a is_active ?
	if (Console.is_console_open()):
		return;
		
	process_mouse_zoom(delta);
	process_follow_node();
	process_keyboard_pan(delta);
	process_mouse_grab_pan(delta);
		
	terrain_col_check();
	#BLog.debug("origin: " + String(transform.origin));
	
	#input_actions.clear();


func clear_follow_node():
	if (follow_node.get_ref() != null):
		follow_node = WeakRef.new();
		emit_signal("follow_node_changed");


func start_mouse_grab_pan(p_event):
	if (controller.mouse_over_ui):
		return;
		
	if (!BUtil.get_terrain()):
		return;
	
	# get the grab point by raycasting the terrain
	# this then becomes the grab point we always want under the mouse
	var screen_mouse_pos = Util.get_viewport_mouse_position(get_viewport());
	var world_from = project_ray_origin(screen_mouse_pos)
	var world_to = world_from + project_ray_normal(screen_mouse_pos) * 100000
		
	var col = BUtil.get_terrain().raycast(world_from, world_to);
	if (!col.empty()):
		pan_mouse_grab_active = true;
		pan_mouse_grab_raycast_world_position = col["position"];
		clear_follow_node(); # I want to break free!


func end_mouse_grab_pan(p_event):
	pan_mouse_grab_active = false;


func process_mouse_grab_pan(delta):
	if (!pan_mouse_grab_active):
		return;
		
	# what this method does is grabs the world position the user clicked on
	# and then casts that down to a ground plane at 0 height
	# we also do the same with the mouse position on screen
	# then we have the two points projected onto the ground plane and and compute the movement
	# in the X/Z plane and just apply that movement to the camera
	
	#var screen_target_pos = unproject_position(pan_mouse_grab_raycast_world_position);
	var screen_mouse_pos = Util.get_viewport_mouse_position(get_viewport());
	var world_from = project_ray_origin(screen_mouse_pos)
	
	var ground_plane = Plane(Vector3(0, 1, 0), 0);
	var mouse_ground_intersect_point = ground_plane.intersects_ray(world_from, project_ray_normal(screen_mouse_pos));
	
	var grab_dir = pan_mouse_grab_raycast_world_position - world_from;
	var grab_position_ground_intersect_point = ground_plane.intersects_ray(world_from, grab_dir);
	
	var camera_movement = grab_position_ground_intersect_point - mouse_ground_intersect_point;
	global_translate(camera_movement);
	return;


# https://docs.godotengine.org/en/3.1/tutorials/physics/ray-casting.html
# Remember that during _input(), the space may be locked, so in practice this query should be run in _physics_process().
func process_mouse_zoom(delta):
	if (controller.mouse_over_ui):
		return;

	# no need to zoom
	if zoom_target == zoom:
		last_zoom_time = 0;
		return;
		
	var screen_mouse_pos = Util.get_viewport_mouse_position(get_viewport());
	
	# get the grab point by raycasting the terrain
	# this then becomes the grab point we always want under the mouse
	var world_from_position = project_position(screen_mouse_pos, 1); # project_ray_origin
	var world_from_direction = project_ray_normal(screen_mouse_pos);
	var world_to = world_from_position + world_from_direction * 100000;
		
	# if no terrain, use a zero plane instead
	var col = BUtil.get_terrain().raycast(world_from_position, world_to);
	var target_pos;
	if (col.empty()):
		target_pos = Plane.PLANE_XZ.intersects_ray(world_from_position, world_from_direction);
	else:
		target_pos = col.position;
		
	# camera underground, no zoom kthxbai
	if (!target_pos):
		last_zoom_time = 0;
		zoom = zoom_target;
		return;
		
	# TODO: Need to check if the target zoom level is valid before setting the next zoom level!
	# or we change the zoom levels to be an altitude above the ground?!
	
	# current zoom
	var src_height = self.transform.origin.y;
	var src_pitch = self.transform.basis.get_euler().x;
	
	# convert a value from 0 -> 1 into a height and angle
	var step_height_curve = step_by_height.get_curve();
	var pitch_height_curve = pitch_by_height.get_curve();
	var height = step_height_curve.interpolate_baked(zoom_target);
	var angle = pitch_height_curve.interpolate_baked(zoom_target);
	
	var aabb_y = aabb.end.y;
	var min_rad = deg2rad(minimum_pitch_angle); # convert to radians
	var max_rad = deg2rad(maximum_pitch_angle); # convert to radians
	
	var dst_height = lerp(0, aabb_y, height);
	var dst_pitch = lerp(min_rad, max_rad, angle);
	

	# zoom target changed
	# reset time for animation
	if last_zoom_target != zoom_target:
		last_zoom_time = 0;
	
	# calculate speed
	last_zoom_time += delta;
	var increment = 1.0 / zoom_tween_time;
	var t = last_zoom_time * increment;

	# calculate position
	var target_height = lerp(src_height, dst_height, t);
	var target_pitch = lerp(src_pitch, dst_pitch, t);

	# calc the change in rotation in radians
	var rot_rad = rotation.x;
	var angle_delta = target_pitch - rot_rad; # radians
	
	# create a plane at the target height
	var target_height_plane = Plane(Vector3 (0,1,0), target_height);
	
	# rotate around mouse world position
	# this will keep the mouse in the same location on the screen
	var cam_rotated = self.transform.basis.rotated(Vector3(1,0,0), angle_delta);
	var new_cam_position = target_height_plane.intersects_ray(target_pos, cam_rotated.z);
	
	# cant move any closer
	# end
	if (new_cam_position == null):
		last_zoom_time = 0;
		zoom = zoom_target;
		return;
	
	# apply transform
	var new_cam_transform = Transform(cam_rotated, new_cam_position);
	self.transform = new_cam_transform;
	
	# account for mouse position on screen offset in world space
	var new_mouse_world_pos = project_position(screen_mouse_pos, 1);
	var new_mouse_world_dir = project_ray_normal(screen_mouse_pos);
	var ground_plane_offset_pos = Plane(Vector3 (0,1,0), target_pos.y).intersects_ray(new_mouse_world_pos, new_mouse_world_dir);
	if (!ground_plane_offset_pos):
		last_zoom_time = 0;
		zoom = zoom_target;
		return;
		
	var mouse_world_offset = target_pos - ground_plane_offset_pos;
	global_translate(mouse_world_offset);
	
	last_zoom_target = zoom_target;
	
	# end
	# comment out this if we want to disable animation - not the contents
	if (last_zoom_time >= zoom_tween_time):
		last_zoom_time = 0;
		zoom = zoom_target;
		
	return;


# ease out animation
func end_keyboard_pan(delta):
	# calc time into the animation as scale 0 - 1;
	var increment = 1.0 / end_keyboard_pan_ease_time;
	var t = last_end_keyboard_pan_time * increment;

	# calculate position
	var dir_scale = end_keyboard_pan_curve.interpolate_baked(t);
	global_translate(last_pan_dir * dir_scale);
	last_end_keyboard_pan_time += delta;
	
	# end
	if (last_end_keyboard_pan_time >= end_keyboard_pan_ease_time):
		last_pan_dir = Vector3();
		last_end_keyboard_pan_time = 0;
	return;



# TODO: this should occur in the input handler
func process_keyboard_pan(delta):
	if (!is_processing_input()):
		return;
		
	# keyboard pan
	var dir = Vector3()
	if (input_actions.is_pressed("move_forward")):
		dir += -transform.basis[2];
	if (input_actions.is_pressed("move_backward")):
		dir += transform.basis[2];
	if (input_actions.is_pressed("move_left")):
		dir += -transform.basis[0];
	if (input_actions.is_pressed("move_right")):
		dir += transform.basis[0];
	
	# no directional key pressed
	if (dir.length_squared() <= 0.0):
		if (last_pan_dir.length_squared() > 0.0):
			end_keyboard_pan(delta);
		return;
	
	# I want to break free!
	clear_follow_node(); 
	
	# check if within bounds
	var position = self.transform.origin;
	
	# we are outside bounds
	if (not inside_aabb(position)):
		var min_x = aabb.position.x;
		var max_x = aabb.position.x + aabb.size.x;
		var min_z = aabb.position.z;
		var max_z = aabb.position.z + aabb.size.z;
		
		# is x vector moving away from the bounds
		if (position.x < min_x and dir.x < 0):
			dir.x = 0;
		if (position.x > max_x and dir.x > 0):
			dir.x = 0;
		# is z vector moving away from the bounds
		if (position.z < min_x and dir.z < 0):
			dir.z = 0;
		if (position.z > max_z and dir.z > 0):
			dir.z = 0;
		
	# calculate speed
	dir.y = 0;
	dir = dir.normalized();
	dir *= pan_keyboard_speed * delta;
	
	if (input_actions.is_pressed("move_sprint")):
		dir *= sprint_speed_multiplier;
		
	if (input_actions.is_pressed("move_slow")):
		dir /= sprint_speed_multiplier;
	
	# move
	global_translate(dir);
	last_pan_dir = dir;
	return;



# pan to node, or zoom to node
func pan_to_node(node):
	var position = node.get_global_transform().origin;
	if (node.has_method("get_follow_node_position")):
		position = node.get_follow_node_position();
		
	pan_to_position(position);


# pan to node, or zoom to position
func pan_to_position(position):
	var offset = compute_camera_movement_to_look_at(position);
	var target = transform.origin + offset;
	global_translate(offset);



func terrain_col_check():
	if (BUtil.get_terrain()):
		var col = BUtil.get_terrain().raycast_down(transform.origin);
		if (!col.empty()):
			var pos = col["position"];
			pos.y += height_from_terrain;
			if (pos.y > transform.origin.y):
				transform.origin.y = pos.y;
				
	return;


# return true if we are following a node
func process_follow_node():
	var node = follow_node.get_ref();
	if (node == null):
		return false;

	pan_to_node(node);
	return true;


# create a plane at the position the user wants us to look at
# then, make a ground plane out of this
# then project the camera's normal through this plane
# then we compute the x, z movement and apply it to the camera
#
# TODO: move to util class?
func compute_camera_movement_to_look_at(position):
	var centrePos = get_viewport().get_size() * 0.5 #.get_rect().size * 0.5
	var from = project_ray_origin(centrePos)
	
	var normal = Util.up;
	var plane = Plane(normal, normal.dot(position));
	var intersectPt = plane.intersects_ray(from, project_ray_normal(centrePos));
	
	# ops, we went through the plane we are raycasting into, so cast backwards
	if (intersectPt == null):
		intersectPt = plane.intersects_ray(from, -project_ray_normal(centrePos));
		
	var offset = position - intersectPt;
	return offset;


func is_orthogonal():
	return self.projection == Camera.PROJECTION_ORTHOGONAL;
	
	
# moves the camera by p_offset, but ensure it stays within the AABB Of the game world
func global_translate(p_offset):
	var position: Vector3 = transform.origin + p_offset;
	
	# keep inside AABB
	if (not inside_aabb(position)):
		var min_x = aabb.position.x;
		var max_x = aabb.position.x + aabb.size.x;
		
		var min_y = aabb.position.y;
		var max_y = aabb.position.y + aabb.size.y;
		
		var min_z = aabb.position.z;
		var max_z = aabb.position.z + aabb.size.z;
		position = Vector3(clamp(position.x, min_x, max_x), clamp(position.y, min_y, max_y), clamp(position.z, min_z, max_z));
		
	transform.origin = position;
	
