#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var timer = Util.create_timer(self, "_update_dialog", 1.0, true);
onready var dlg = get_node("Vehicle");

# info container items
onready var ui_search_edit = BUtil.find_child(dlg, "SearchEdit");

onready var ui_vehicle_info_template = BUtil.find_child(dlg, "VehicleInfoTemplate");
onready var ui_vehicle_list = BUtil.find_child(dlg, "VehicleList");

onready var ui_vehicles_button = get_node("VehiclesButton");

var controller;
var item_vehicle_path_map = {};

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	controller.add_child(self);
	controller.hud.add_left_panel(dlg);
	controller.hud.add_dialog_panel_button($VehiclesButton);
	self.controller = controller;
	return;
	
func _ready():
	ui_search_edit.set_right_icon(ui_search_edit.get_icon("Search", "EditorIcons"));
	ui_vehicle_info_template.get_parent().remove_child(ui_vehicle_info_template); # remove from parent
	ui_vehicles_button.self_modulate.a = 0.5;
	return;

func _update_dialog():
	if (!dlg.is_visible()):
		return;
	
	var company_vehicles = controller.get_company().get_trains();
	for i in range(0, company_vehicles.size()):
		var vehicle = company_vehicles[i];
		if (i > (ui_vehicle_list.get_child_count() - 1)):
			var vi = ui_vehicle_info_template.duplicate();
			ui_vehicle_list.add_child(vi);
			
		var vehicle_info = ui_vehicle_list.get_child(i);
		if (vehicle_info.vehicle != vehicle):
			vehicle_info.init(controller, vehicle);
			
	# free extras
	while (ui_vehicle_list.get_child_count() > company_vehicles.size()):
		var vehicle_info = ui_vehicle_list.get_child(ui_vehicle_list.get_child_count() - 1);
		ui_vehicle_list.remove_child(vehicle_info);
		vehicle_info.queue_free();
		
		
	return;
	
func _on_SearchEdit_text_changed( text ):
	for vehicle_info in ui_vehicle_list.get_children():
		vehicle_info.set_visible(vehicle_info.search_match("*" + text + "*"));

func _on_VehiclesButton_toggled(pressed):
	ui_vehicles_button.self_modulate.a = 1.0 if pressed else 0.5;
	dlg.set_visible(pressed);
	_update_dialog();
	return;

func _on_Title_icon_pressed():
	ui_vehicles_button.set_pressed(false);
	_on_VehiclesButton_toggled(false);
