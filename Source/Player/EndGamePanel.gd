#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var dialog = get_node("EndGamePanel");
onready var ui_company_template = BUtil.find_child(dialog, "CompanyTemplate");
onready var ui_company_list = dialog.find_node("CompanyList");

var controller;


func apply_to_controller(p_controller):
	controller = p_controller;
	if (!controller.is_network_master()):
		return;
		
	controller.add_child(self);
	controller.hud.add_dialog_panel(dialog);


func _ready():
	ui_company_template.get_parent().remove_child(ui_company_template); # remove from parent


func _on_ContinuePlayingBtn_pressed():
	dialog.set_visible(false);
	
	
func _on_ExitToMenuBtn_pressed():
	controller.leave_game();
	
	
# add company to the CompanyList container and populate the values
func add_company(p_company, p_winner):
	var template = ui_company_template.duplicate();
	ui_company_list.add_child(template);
	
	var ui_title = BUtil.find_child(template, "Title");
	ui_title.set_text(p_company.get_display_name());
	
	var ui_net_worth = BUtil.find_child(template, "NetWorth");
	ui_net_worth.set_text(Util.format_currency(p_company.get_net_worth()));
	
	var ui_loan = BUtil.find_child(template, "Loan");
	ui_loan.set_text(Util.format_currency(p_company.get_loan()));
	
	var warehouses = p_company.filter(get_tree().get_nodes_in_group("warehouses"));
	var ui_warehouses = BUtil.find_child(template, "Warehouses");
	ui_warehouses.set_text(str(warehouses.size()));
	
	var vehicles = p_company.filter(get_tree().get_nodes_in_group("vehicles"));
	var ui_vehicles = BUtil.find_child(template, "Vehicles");
	ui_vehicles.set_text(str(vehicles.size()));
	
	var terminals = p_company.filter(get_tree().get_nodes_in_group("vehicle_terminals"));
	var ui_terminals = BUtil.find_child(template, "Terminals");
	ui_terminals.set_text(str(terminals.size()));
	
	# whos on the team
	var ui_players = BUtil.find_child(template, "Players");
	var player_names = PoolStringArray();
	for p in p_company.players:
		player_names.append(p.get_display_name());
		
	ui_players.set_text(player_names.join(", "));
	
	
# p_companies is a SORTED list of ALL companies, while p_winners is a SORTED list of the winners from p_companies
func show_end_game_panel(p_companies, p_winners):
	for c in p_companies:
		add_company(c, p_winners.has(c));
		
	dialog.set_visible(true);
