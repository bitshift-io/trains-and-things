#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Interface between player controller interactive items
# for now just handles delete for:
# tracks and terminals
#

extends Node

var controller;

const DELETE = 0;

var node: WeakRef;


func apply_to_controller(p_controller):
	controller = p_controller;
	controller.add_handler(self);
	return;
	
	
func show_context_menu(p_node):
	node = weakref(p_node);
	
	var context_menu = controller.hud.get_context_menu();
	context_menu.clear();

	context_menu.add_item("Delete", DELETE);
	controller.hud.popup_context_menu(self, "_context_menu_pressed");
	return;
	

func _context_menu_pressed(p_context_menu, p_id):
	if (!node.get_ref()):
		return;
		
	match p_id:
		DELETE: delete(node.get_ref());
			
	return;

	
func handles(p_node):
	var is_terminal = BUtil.is_class_name(p_node, "VehicleTerminal");
	var is_track = BUtil.is_class_name(p_node, "Track");
	var is_warehouse = BUtil.is_class_name(p_node, "Warehouse");
	
	# only handle what belongs to us
	if ((is_terminal || is_track || is_warehouse) && p_node.get_company() != controller.get_company()):
		return false;
		
	return is_terminal || is_track || is_warehouse;
	
	
func delete(p_node):
	p_node.delete();
	return true;
