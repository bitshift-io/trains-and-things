#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "../StateMachine/State.gd"

var controller = null

func _ready():
	controller = BUtil.find_parent_by_class_name(get_parent(), "Player")
