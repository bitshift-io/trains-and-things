#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

const max_statements = 20;

var companyDlg;
onready var timer = Util.create_timer(self, "_update_dialog", 1.0, true);

onready var membersTree = BUtil.find_child(companyDlg, "BoardMembersTree");
onready var companiesTree = BUtil.find_child(companyDlg, "RivalCompaniesTree");
onready var companyNameEdt = BUtil.find_child(companyDlg, "CompanyNameEdt");

onready var ui_title = BUtil.find_child(companyDlg, "Title");

onready var ui_loan_increase_btn = BUtil.find_child(companyDlg, "LoanIncreaseBtn");
onready var ui_loan_repay_btn = BUtil.find_child(companyDlg, "LoanRepayBtn");

onready var ui_cash_lbl = BUtil.find_child(companyDlg, "CashLbl");
onready var ui_loan_lbl = BUtil.find_child(companyDlg, "LoanLbl");
onready var ui_assets_lbl = BUtil.find_child(companyDlg, "AssetsLbl");
onready var ui_net_worth_lbl = BUtil.find_child(companyDlg, "NetWorthLbl");

onready var ui_income_statement_list = BUtil.find_child(companyDlg, "IncomeStatementYearList");
onready var ui_interest_rate_lbl = BUtil.find_child(companyDlg, "InterestRateLbl");
onready var ui_max_loan_lbl = BUtil.find_child(companyDlg, "MaxLoanLbl");

onready var ui_income_statement_scroll_container = BUtil.find_child(companyDlg, "IncomeStatementScrollContainer");

onready var ui_join_btn = BUtil.find_child(companyDlg, "RivalsJoinBtn");

var ui_company_button;
var controller;
onready var world = WorldMgr.get_world(self);


func apply_to_controller(p_controller):
	controller = p_controller;
	if (!controller.is_network_master()):
		return;
	
	companyDlg = get_node("CompanyPanel");
	ui_company_button = get_node("CompanyButton");
	
	controller.hud.add_child(self);
	controller.hud.add_right_panel(companyDlg);
	controller.hud.add_dialog_panel_button_right(ui_company_button);
	
	# allow company panel to resize to the left!
	companyDlg.resize_flags = BPanel.DRAG_RESIZE_BOTTOM | BPanel.DRAG_RESIZE_LEFT;
	
	
func _ready():
	# setup the dialog for stuff we only want to do on first show
	membersTree.clear()
	membersTree.set_hide_root(true)
	membersTree.create_item() # create the root item
	membersTree.set_columns(1)
	membersTree.set_column_title(0, "Company Members")
	membersTree.set_column_titles_visible(true)
	membersTree.set_select_mode(membersTree.SELECT_ROW);
	membersTree.create_item();
	
	companiesTree.set_hide_root(true)
	companiesTree.set_columns(2)
	companiesTree.set_column_title(0, "Company Name")
	companiesTree.set_column_title(1, "Net Worth")
	companiesTree.set_column_titles_visible(true)
	companiesTree.set_select_mode(companiesTree.SELECT_ROW);
	companiesTree.create_item();
	
	ui_join_btn.set_disabled(true); # this forces the player to make a selection before this will become enabled
	
	Util.set_shortcut_key(ui_loan_increase_btn, KEY_EQUAL); #KEY_PLUS);
	Util.set_shortcut_key(ui_loan_repay_btn, KEY_MINUS);
	
	ui_company_button.self_modulate.a = 0.5;
	
	EventMgr.listen([EventMgr.Id.PLAYER_JOINED_COMPANY], self, "_on_player_joined_company");


func _on_player_joined_company(p_params):
	# the player changed company, so we need to reset some UI things
	ui_income_statement_list.clear();
	

func get_income_statement_components(p_ui_item):
	return {
		#"finances": p_ui_item.get_meta("finances"),
		"ui_year_lbl": BUtil.find_child(p_ui_item, "YearLbl"),
		"ui_total_lbl": BUtil.find_child(p_ui_item, "TotalLbl"),
		"income": {
			"ui_trains_lbl": BUtil.find_child(p_ui_item, "TrainsIncomeLbl"),
			"ui_total_lbl": BUtil.find_child(p_ui_item, "TotalIncomeLbl"),
		},
		"expenses": {
			"ui_trains_lbl": BUtil.find_child(p_ui_item, "TrainsExpensesLbl"),
			"ui_stations_lbl": BUtil.find_child(p_ui_item, "StationsExpensesLbl"),
			"ui_warehouses_lbl": BUtil.find_child(p_ui_item, "WarehousesExpensesLbl"),
			"ui_tracks_lbl": BUtil.find_child(p_ui_item, "TracksExpensesLbl"),
			"ui_loan_interest_lbl": BUtil.find_child(p_ui_item, "LoanInterestExpensesLbl"),
			"ui_total_lbl": BUtil.find_child(p_ui_item, "TotalExpensesLbl"),
		}
	};
	
	
func _update_dialog():
	if (!companyDlg.is_visible()):
		return;
		
	if (get_company()):
		ui_title.title = get_company().get_display_name();
		ui_title._update();
		
		var loan_size = get_company().get_loan_size();
		ui_loan_increase_btn.text = "Loan " + str(loan_size / 1000) + "k";
		ui_loan_repay_btn.text = "Repay " + str(loan_size / 1000) + "k";
		
		var interest_rate = get_company().get_interest_rate();
		ui_interest_rate_lbl.text = str(interest_rate) + "% p.a."
		
		ui_max_loan_lbl.text = str(get_company().get_loan_limit() / 1000) + "k";
	
	update_finances_tab();
	update_competition_tab();
	update_board_tab();

		
func update_finances_tab():
	var company = get_company();
	if (!company):
		return;

	ui_cash_lbl.text = Util.format_currency(company.get_cash());
	ui_loan_lbl.text = Util.format_currency(-company.get_loan());
	ui_assets_lbl.text = Util.format_currency(company.get_assets_total());
	ui_net_worth_lbl.text = Util.format_currency(company.get_net_worth());

	# update the changed income statements
	var statements = company.get_income_statements();
	var number_to_add = statements.size() - ui_income_statement_list.get_child_count(); # how many new UI items are needed to match the companies income statements?
	
	# player is changing to a new company, but _on_player_joined_company hasnt been called yet, so just abort, it will fix it self in a sec
	if (number_to_add < 0):
		return;
		
	if (number_to_add):
		# sticky scroll... if the user is scrolled away from the end, do not move the scroll bar
		# if they are at the end, then move it along
		var hscroll = ui_income_statement_scroll_container.get_node("_h_scroll");
		var scroll_max = hscroll.get_max();
		var scroll_actual = hscroll.get_value() + hscroll.get_page();
		var scroll_actual_2 = ui_income_statement_scroll_container.get_h_scroll();
		var scroll_is_at_end = scroll_max == scroll_actual;
		
		# update the last income statement incase it changed since last frame
		var last_ui_income_statement_item = ui_income_statement_list.get_last_child();
		if (last_ui_income_statement_item):
			var last_statement = statements[(statements.size() - 1) - number_to_add];
			update_income_statement_ui(last_ui_income_statement_item, last_statement);
		
		# now add any new ones that need to be added
		while (statements.size() > ui_income_statement_list.get_child_count()):
			var new_ui_income_statement_item = ui_income_statement_list.create_item();
			var statement = statements[ui_income_statement_list.get_child_count() - 1];
			update_income_statement_ui(new_ui_income_statement_item, statement);
		
		# wait a frame to allow control to update before we try to scroll to the end
		if (scroll_is_at_end):
			yield(get_tree(), "idle_frame")
			scroll_to_end();
	else:
		# update the last income statement
		var ui_income_statement_item = ui_income_statement_list.get_last_child();
		update_income_statement_ui(ui_income_statement_item, statements.back());

	
func scroll_to_end():
	var max_val = ui_income_statement_scroll_container.get_node("_h_scroll").get_max();
	ui_income_statement_scroll_container.set_h_scroll(max_val);
	
	
# apply the values in p_income_statement to the UI p_ui_income_statement_item
func update_income_statement_ui(var p_ui_income_statement_item, var p_income_statement):
	var income_statement_componenets = get_income_statement_components(p_ui_income_statement_item);
	income_statement_componenets.ui_year_lbl.text = world.date_time.months_to_quarter_date_str(p_income_statement.date.start.months);
	income_statement_componenets.income.ui_trains_lbl.text = Util.format_currency(p_income_statement.get_revenues(IncomeStatement.VEHICLES));
	income_statement_componenets.income.ui_total_lbl.text = Util.format_currency(p_income_statement.get_total_revenues());
	income_statement_componenets.expenses.ui_stations_lbl.text = Util.format_currency(-p_income_statement.get_expenses(IncomeStatement.TERMINALS));
	income_statement_componenets.expenses.ui_warehouses_lbl.text = Util.format_currency(-p_income_statement.get_expenses(IncomeStatement.WAREHOUSES));
	income_statement_componenets.expenses.ui_tracks_lbl.text = Util.format_currency(-p_income_statement.get_expenses(IncomeStatement.TRACKS));
	income_statement_componenets.expenses.ui_trains_lbl.text = Util.format_currency(-p_income_statement.get_expenses(IncomeStatement.VEHICLES));
	income_statement_componenets.expenses.ui_loan_interest_lbl.text = Util.format_currency(-p_income_statement.get_expenses(IncomeStatement.LOANS));
	income_statement_componenets.expenses.ui_total_lbl.text = Util.format_currency(-p_income_statement.get_total_expenses());
	income_statement_componenets.ui_total_lbl.text = Util.format_currency(p_income_statement.get_total_revenues() - p_income_statement.get_total_expenses());
	

func update_board_tab():
	if (!get_company()):
		return;
		
	var members = get_company().get_players();
	Util.resize_tree_item_children(membersTree, membersTree.get_root(), members.size());
	var membersTreeItems = Util.getTreeItemChildren(membersTree.get_root());
		
	for i in range(0, members.size()):
		var m = members[i];
		var item = membersTreeItems[i];
		item.set_text(0, m.get_display_name())
		
	# update the company name field if it has changed and the user isn't trying to change the company name
	if (!companyNameEdt.has_focus()):
		companyNameEdt.set_text(get_company().get_display_name())

	
func update_competition_tab():
	var root = companiesTree.get_root();
	var companyItem = root.get_children()
	var companiesNode = world.companies;
	var companies = companiesNode.get_children()
	
	for i in range(0, companies.size()):
		var company = companies[i]
		
		if (!companyItem):
			companyItem = companiesTree.create_item(root)
			
		companyItem.set_metadata(0, company);
		companyItem.set_text(0, company.get_display_name())
		companyItem.set_text(1, Util.format_currency(company.get_net_worth()))
		companyItem.set_text_align(1, TreeItem.ALIGN_RIGHT)
		
		var playerItem = companyItem.get_children()
		var players = company.get_players()
		for x in range(0, players.size()):
			var playerName = players[x].get_display_name()
			
			if (!playerItem):
				playerItem = companiesTree.create_item(companyItem)
				
			playerItem.set_metadata(0, players[x]); # store which player controller in metadata
			playerItem.set_text(0, playerName)
			playerItem = playerItem.get_next()
			
		# remove left overs... if a player left a team
		while (playerItem):
			companyItem.remove_child(playerItem)
			playerItem = playerItem.get_next() 
			
		companyItem = companyItem.get_next()
		
	# remove left overs... if a company disolved
	while (companyItem):
		root.remove_child(companyItem)
		companyItem = companyItem.get_next();

		
func _on_JoinCompanyBtn_pressed():	
	var item = companiesTree.get_selected()
	var selected_company = item.get_metadata(0);
	controller.join_company(selected_company);


func _on_CompaniesTre_item_selected():
	var item = companiesTree.get_selected();
	if (!item):
		ui_join_btn.set_disabled(true)
		return;
		
	# iterate up to the company item - if the user selects a user in a company
	var parent_item = item.get_parent();
	while (parent_item != companiesTree.get_root()):
		item = parent_item;
		parent_item = item.get_parent();
		
		
	var selected_company = item.get_metadata(0);
	if (selected_company != get_company()):
		ui_join_btn.set_disabled(false);
		return;

	ui_join_btn.set_disabled(true);

	
func _on_LoanBtn_pressed():
	var audio = BUtil.find_child(companyDlg, "LoanIncreaseBtnPress");
	audio.play();
	get_company().take_out_loan(get_company().get_loan_size());
	EventMgr.trigger(EventMgr.Id.BUTTON_PRESSED, { "sender": self, "button": ui_loan_increase_btn });


func _on_RepayBtn_pressed():
	var audio = BUtil.find_child(companyDlg, "LoanRepayBtnPress");
	audio.play();
	get_company().repay(get_company().get_loan_size());
	EventMgr.trigger(EventMgr.Id.BUTTON_PRESSED, { "sender": self, "button": ui_loan_repay_btn });

	
func get_company():
	return controller.get_company();
	
	
# this is disabled!
func _on_NewCompanyBtn_pressed():
	controller.create_new_company();


func _on_CompanyButton_toggled(pressed):
	companyDlg.set_visible(pressed);
	ui_company_button.self_modulate.a = 1.0 if pressed else 0.5;
	scroll_to_end();
	
	if (companyDlg.is_visible()):
		return

	if (!get_company()):
		return

	update_finances_tab();
	update_competition_tab();
	update_board_tab();
	_on_CompaniesTre_item_selected();


func _on_Title_icon_pressed():
	ui_company_button.set_pressed(false);
	_on_CompanyButton_toggled(false);
	

func _on_CompanyNameEdt_text_entered(new_text):
	get_company().set_display_name(new_text);
