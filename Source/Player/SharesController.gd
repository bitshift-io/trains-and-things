#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var dialog = get_node("SharesPanel");

onready var ui_share_info_template = BUtil.find_child(dialog, "ShareInfoTemplate");
onready var ui_share_list = dialog.find_node("ShareList");
onready var ui_search_edit = BUtil.find_child(dialog, "SearchEdit");
onready var ui_shares_button = get_node("SharesButton");
#onready var timer = Util.create_timer(self, "_update_dialog", 1.0, true);

var controller;

func apply_to_controller(p_controller):
	controller = p_controller;
	if (!controller.is_network_master()):
		return;
	
	controller.add_child(self);
	controller.hud.add_left_panel(dialog);
	controller.hud.add_dialog_panel_button(ui_shares_button);
	return;
	
func get_company():
	return controller.get_company();
	
func _ready():
	ui_share_info_template.get_parent().remove_child(ui_share_info_template); # remove from parent
	ui_shares_button.self_modulate.a = 0.5;
	return;
	

func _update_dialog():
	if (!dialog.is_visible()):
		return;
		
	ui_search_edit.set_right_icon(ui_search_edit.get_icon("Search", "EditorIcons"));
	
	# only add/remove changes to stop the UI being unresponsive	
	var companies = get_tree().get_nodes_in_group("companies");
	var existing_companies_share_infos = ui_share_list.get_children();
	
	for i in range(0, companies.size()):
		if (i >= existing_companies_share_infos.size()):
			var share_info = ui_share_info_template.duplicate();
			share_info.controller = self;
			share_info.set_company(companies[i]);
			ui_share_list.add_child(share_info);
		else:
			var share_info = existing_companies_share_infos[i];
			share_info.controller = self;
			share_info.set_company(companies[i]);
			share_info._ready(); # to refresh the state of this UI
			
	# remove left overs... if a company dissolved
	for i in range(companies.size(), existing_companies_share_infos.size()):
		ui_share_list.remove_child(existing_companies_share_infos[i]);
		existing_companies_share_infos[i].queue_delete();

	return;
	
func _on_SearchEdit_text_changed( text ):
	for share_info in ui_share_list.get_children():
		share_info.set_visible(share_info.search_match("*" + text + "*"));

# share info detected the company was destroyed (ie. company == null)
# so clear this ShareInfo from the UI
func company_destroyed(share_info):
	ui_share_list.remove_child(share_info);
	share_info.queue_free();

func _on_SharesButton_toggled(pressed):
	dialog.set_visible(pressed);
	ui_shares_button.self_modulate.a = 1.0 if pressed else 0.5;
	_update_dialog();
	return;

func _on_Title_icon_pressed():
	ui_shares_button.set_pressed(false);
	_on_SharesButton_toggled(false);
