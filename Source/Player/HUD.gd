#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends CanvasLayer

var ui_hud;

var ui_game_menu_button;
var ui_moneyLbl;
var ui_dateLbl;

var ui_button_container_left;
var ui_button_container_right;

var ui_hud_container;
var ui_window_container;
var ui_context_container;
var ui_mouse_over_container;
var ui_context_menu;

var controller;
var world;

func apply_to_controller(p_controller):
	controller = p_controller;
	world = controller.world;
	
	ui_hud = BUtil.find_child(self, "HUD");
	ui_game_menu_button = BUtil.find_child(self, "GameMenuBtn");
	ui_moneyLbl = BUtil.find_child(self, "MoneyLbl");
	ui_dateLbl = BUtil.find_child(self, "DateLbl");

	ui_button_container_left = BUtil.find_child(self, "DialogPanelButtonContainerLeft");
	ui_button_container_right = BUtil.find_child(self, "DialogPanelButtonContainerRight");

	ui_hud_container = BUtil.find_child(self, "HUDContainer");
	ui_window_container = BUtil.find_child(self, "WindowContainer");
	ui_context_container = BUtil.find_child(self, "ContextContainer");
	ui_mouse_over_container = BUtil.find_child(self, "MouseOverContainer");
	
	ui_context_menu = BUtil.find_child(self, "ContextMenu");
	ui_context_menu.set("hud", self); # poke a pointer to HUD in the context menu
	
	# non network master controllers have a hud
	# but it just is invisible 
	if (controller.is_network_master()):
		world.ui_viewport.add_child(self);
		
		# connect hud to menu
		# UGLY: TODO FIX ME
		Menu.hud = self;
	
	# assign self as hud
	if controller.hud == null:
		controller.hud = self;
	return;


func _ready():
	if (!world):
		return;
	
	ui_game_menu_button.self_modulate.a = 0.5;
	Util.set_shortcut_key(ui_game_menu_button, KEY_ESCAPE);
	
	Console.register_command("hudtogglevisible", {
		method = funcref(self, "toggle_visible"),
		owner = self,
		description = "Toggle HUD visible",
		args = []
	})
	
	return;


func _exit_tree():
	Console.deregister_commands(self)
	
	
func toggle_visible():
	set_visible(!is_visible());
	return;


func is_visible():
	return ui_hud.visible;


func set_visible(vis):
	ui_hud.visible = vis;
	
	var resource_nodes = get_tree().get_nodes_in_group("resource_nodes");
	for rn in resource_nodes:
		rn.set_hud_visible(vis);
	return;


func set_game_menu_visible(is_visible):
	if (Menu.is_visible() == is_visible):
		return;

	set_visible(!is_visible);
	Menu.set_visible(is_visible);
	return;


func _on_GameMenuBtn_pressed():
	set_game_menu_visible(!Menu.is_visible());
	return;


func add_state_button(button, p_name):
	if (!button):
		return;
		
	if (!controller.is_network_master()):
		button.queue_free();
		return;
		
	var container = BUtil.find_child(self, "StateButtonContainer");
	BUtil.reparent(button, container);
	
	button.set_meta("context_menu_name", p_name);
	button.enabled_focus_mode = Control.FOCUS_NONE; # disable focus
	Util.set_shortcut_key(button, KEY_1 + container.get_child_count() - 1); # take 1 for some reason?
	return;


func get_state_button_container():
	return BUtil.find_child(self, "StateButtonContainer");


# just a free floating panel
# such as chat and end game screens and tutorials
func add_dialog_panel(panel):
	if (!panel):
		return;
		
	if (!controller.is_network_master()):
		panel.queue_free();
		return;

	var container = BUtil.find_child(self, "WindowContainer");
	BUtil.reparent(panel, container);
	return;


# this type of panel is an animated slide out from the left panel
# these are more like helper dialogs and information type panels
func add_left_panel(panel):
	if (!panel):
		return;
		
	if (!controller.is_network_master()):
		panel.queue_free();
		return;
		
	var container = BUtil.find_child(self, "LeftPanelContainer");
	panel.rect_position = Vector2(0, 0);
	panel.size_flags_vertical = 0;
	#panel.rect_min_size = panel.rect_size;
	panel.resize_flags = BPanel.DRAG_RESIZE_BOTTOM; # | BPanel.DRAG_RESIZE_RIGHT;
	
	BUtil.reparent(panel, container);
	container.move_child(panel, 0);
	return;



# this type of panel is an animated slide out from the right panel
# typically state specific panels and company panels - these involve purchasing and setting up financial things
func add_right_panel(panel):
	if (!panel):
		return;
		
	if (!controller.is_network_master()):
		panel.queue_free();
		return;
		
	var container = BUtil.find_child(self, "RightPanelContainer");
	panel.rect_position = Vector2(0, 0);
	panel.size_flags_vertical = 0;
	#panel.rect_min_size = panel.rect_size;
	panel.resize_flags = BPanel.DRAG_RESIZE_BOTTOM; # | BPanel.DRAG_RESIZE_LEFT;
	
	BUtil.reparent(panel, container);
	return;


func add_dialog_panel_button(button):
	if (!button):
		return;
		
	if (!controller.is_network_master()):
		button.queue_free();
		return;
		
	button.enabled_focus_mode = Control.FOCUS_NONE; # disable focus
	
	var child_count = ui_button_container_left.get_child_count() + ui_button_container_right.get_child_count() - 1; # -1 for the menu button
	Util.set_shortcut_key(button, KEY_F1 + child_count);
	
	BUtil.reparent(button, ui_button_container_left);
	return;


# special for company panel
func add_dialog_panel_button_right(button):
	if (!button):
		return;
		
	if (!controller.is_network_master()):
		button.queue_free();
		return;
		
	button.enabled_focus_mode = Control.FOCUS_NONE; # disable focus
	
	var child_count = ui_button_container_left.get_child_count() + ui_button_container_right.get_child_count() - 1; # -1 for the menu button
	Util.set_shortcut_key(button, KEY_F1 + child_count);
	
	BUtil.reparent(button, ui_button_container_right);
	return;


func add_context_ui(control):
	if (!control):
		return;
		
	if (!controller.is_network_master()):
		control.queue_free();
		return;
	var container = BUtil.find_child(self, "ContextContainer");
	BUtil.reparent(control, container);
	return;


# untoggle all state buttons to restore back to default state
func clear_toggled_state_buttons():
	var container = BUtil.find_child(self, "StateButtonContainer");
	container.clear_toggled() # leave the train state
	return;


func get_context_menu():
	return ui_context_menu;
	
func popup_context_menu(p_target, p_method):
	ui_context_menu.target = p_target;
	ui_context_menu.method = p_method;
	ui_context_menu.popup();

