#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "PlayerState.gd"

signal building_constructed();

var left_mouse_pressed = false
var location_selected = false
var closest_resource_node = null;

export(bool) var available_in_demo = false; # can we use this in the demo?

export(NodePath) var button_path;
export(NodePath) var dialog_path;

export(PackedScene) var building_template;

var _building; # visual model displaying to the user
onready var button = get_node(button_path);
onready var dialog = get_node(dialog_path);

onready var okButton = BUtil.find_child(dialog, "OkButton");
onready var cancelButton = BUtil.find_child(dialog, "CancelButton");

var _resource_nodes; # cache of resource nodes for distance checking

onready var world = WorldMgr.get_world(self);

# the position determines how the building is positioned
# AT_CURSOR - build at the cursor location
# CLOSEST_RESOURCE_NODE - snap to the closest resource node
enum POSITION { AT_CURSOR, CLOSEST_RESOURCE_NODE };
export(POSITION) var position = POSITION.CLOSEST_RESOURCE_NODE;

export(String) var globals_node_name;


func apply_to_controller(controller):
	controller.add_state(self);
	return;


func _ready():
	#set_network_mode(NETWORK_MODE_MASTER if get_tree().is_network_server() else NETWORK_MODE_SLAVE);
	set_network_master(1); # the server is always 1
	
	controller.hud.add_state_button(button, "Terminal");
	controller.hud.add_right_panel(dialog);
	_building = building_template.instance();
	_building.set_as_building_template();

	# for now just set it way out of sight!
	# TODO: ideally set_visible should also disable the physics so we dont need this hack. or call it something else ?
	var xform = Transform();
	_building.spawn(controller, self, controller.company, "BuildStateTemplateBuilding", xform);
	_building.set_visible(false);
	
	Util.set_click_mask_from_normal_alpha(button);
	return;


func state_input(event):
	if (event is InputEventMouseButton):
		if (event.button_index == BUTTON_LEFT and event.pressed): 	#if (event.is_action_pressed("left_click")):
			left_mouse_pressed = true
			
		if (event.button_index == BUTTON_RIGHT and event.pressed): 	#if (event.is_action_pressed("right_click")):
			controller.hud.clear_toggled_state_buttons();
	
	return;


func state_enter(prevState):
	_resource_nodes = BUtil.find_children_by_class_name(get_node("/root/World"), "ResourceNode", true);
	left_mouse_pressed = false;
	location_selected = false;
	okButton.set_disabled(true);
	controller.clear_mouse_over();
	#controller.mouse_over_enabled = false;
	controller.context_menu_enabled = false;
	_building.set_visible(true);
	
	BUtil.find_child(dialog, "PurchaseCostLabel").set_text(Util.format_currency(_building.get_finances().get_purchase_cost()));
	BUtil.find_child(dialog, "MaintenanceCostLabel").set_text(Util.format_currency(_building.get_finances().get_maintenance_cost()));
	dialog.set_visible(true);
	return;


func state_exit(nextState):
	#controller.mouse_over_enabled = true
	controller.context_menu_enabled = true;

	_building.set_visible(false);
	
	dialog.set_visible(false);
	return;
		
		
func get_company():
	return controller.get_company();
	
	
# given the cursor position, get a transform for the building
# after this is returned it is raycast to the ground
# and applied to the building
func compute_building_transform_from_cursor(terrain_col):
	var building_xform = Transform();
	building_xform.origin = terrain_col["position"];
	
	if (position == POSITION.AT_CURSOR):
		return building_xform;

	# iterate over resources nodes, get the closest
	# and orient to that	
	var closest_dist_sqrd = -1;
	for c in _resource_nodes:
		var dist_sqrd = (building_xform.origin - c.transform.origin).length_squared();
		if (closest_dist_sqrd < 0 || dist_sqrd < closest_dist_sqrd):
			# make sure this resource node is usable by use (enemy warehouses are not)
			if (c.is_usable(controller)):
				closest_resource_node = c;
				closest_dist_sqrd = dist_sqrd;

	if (!closest_resource_node):
		_building.set_transform(building_xform);
		return;

	var dir_to_resource_node = (closest_resource_node.transform.origin - building_xform.origin);
	dir_to_resource_node.y = 0;
	var dist_to_resource_node = dir_to_resource_node.length();
	dir_to_resource_node = dir_to_resource_node.normalized();

	var forward = dir_to_resource_node.cross(Util.up);
	building_xform.basis = Basis(dir_to_resource_node, Util.up, forward);

	# now move it to within the build radius
	var dist_to_move = 0;
	if (dist_to_resource_node > closest_resource_node.build_outer_radius):
		dist_to_move = dist_to_resource_node - closest_resource_node.build_outer_radius;
	if (dist_to_resource_node < closest_resource_node.build_inner_radius):
		dist_to_move = -(closest_resource_node.build_inner_radius - dist_to_resource_node);

	building_xform.origin = building_xform.origin + (dir_to_resource_node * dist_to_move);
	return building_xform;
	
	
# Update physics processing.
func state_physics_process(delta):
	get_company().update_purchase_button(okButton, _building.get_finances().get_purchase_cost(), location_selected);

	if (location_selected):
		# left click again results in buying it
		if (left_mouse_pressed):
			_on_OkButton_pressed();
		return;
		
	var col_list = controller.mouse_pick();
	if (col_list.size() <= 0):
		return;

	var terrain_col = col_list.back();
	if (terrain_col == null):
		return;

	var building_xform = compute_building_transform_from_cursor(terrain_col);
	
	# raycast to ground
	if (BUtil.get_terrain()):
		var col = BUtil.get_terrain().raycast_down(building_xform.origin);
		if (!col.empty()):
			building_xform.origin = col["position"];
			building_xform = BUtil.get_terrain().move_transform_to_ground(building_xform);

	_building.set_transform(building_xform);
	
	# check if we can lock the current position in, if so, lock it in!
	var can_build = _building.can_build();
	var blueprint = controller.get_blueprint();
	blueprint.set_material_override(_building, blueprint.get_material_from_bool(can_build));
	if (left_mouse_pressed && _building.can_build()):
		location_selected = true;
		EventMgr.trigger(EventMgr.Id.BUILDING_LOCATION_SELECTED, { "sender": self, "resource_node": closest_resource_node });

	left_mouse_pressed = false;
	return;
	
	
func _on_TextureButton_toggled( pressed ):
	# disable in demo
	if (BDatabase.is_build_type("demo") and !available_in_demo):
		return;
		
	toggle_state(pressed);
	
	
master func create_building_master_rpc(company_path, resource_node_path, transform):
	# money handling
	var purchase_cost = _building.get_finances().get_purchase_cost();
	var company = get_company()
	var loan_amount = company.loan_amount_required(purchase_cost)
	
	# cant actually afford it! abort!
	if (loan_amount == -1):
		return;
	
	loan_amount = company.loan_amount_to_block(loan_amount);
	company.take_out_loan(loan_amount);
	company.pay_cash(purchase_cost, _building.get_income_statement_field());
	
	var name = world.network.next_unique_name(controller.networkId, "Building");
	rpc("create_building_rpc", company_path, resource_node_path, name, transform);
	return;
	
	
remotesync func create_building_rpc(company_path, resource_node_path, name, transform):
	var company = get_tree().get_root().get_node(company_path);
	var resource_node = get_tree().get_root().get_node(resource_node_path) if resource_node_path else null;
	var building = building_template.instance();
	
	var globals_node = BUtil.find_child(world.world_container, globals_node_name);
	building.spawn(controller, resource_node if resource_node else globals_node, company, name, transform);
	
	EventMgr.trigger(EventMgr.Id.BUILDING_CONSTRUCTED, { "sender": self, "building": building });
	return;
	
	
func _on_OkButton_pressed():	
	rpc("create_building_master_rpc", controller.get_company().get_path(), closest_resource_node.get_path() if closest_resource_node else null, _building.transform);
	controller.hud.clear_toggled_state_buttons();
	
	# play any audio if it exists
	if ($OkPressed):
		$OkPressed.trigger();

	return;


func _on_CancelButton_pressed():
	controller.hud.clear_toggled_state_buttons();
	return;
	
