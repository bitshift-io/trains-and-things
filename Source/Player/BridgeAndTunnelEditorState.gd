#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "RailEditorState.gd"

#var curve_editor = preload("../CurveEditor.gd").new()

#var bridgeTemplate = preload("res://Prefab/Transport/Bridge.tscn"); # TODO: make this an export
#onready var bridgeInst = bridgeTemplate.instance()

#var leftMousePressed = false
#var rightMousePressed = false
#
#var finishedLayingTrack = false
#
const CollisionHeightAdjust = -0.2;
#
#export(NodePath) var button_path;
#export(NodePath) var dialog_path;
#
#onready var button = get_node(button_path);
#onready var dialog = get_node(dialog_path);
#
#onready var okBtn = dialog.get_node("OkBtn")
#onready var cancelBtn = dialog.get_node("CancelBtn")

export(NodePath) var hide_path; # hide the node from the previous classes we dont use
export(NodePath) var hide_path2;

#func apply_to_controller(controller):
#	controller.add_state(self);
#	return;

func _ready():
	#._ready();
	
#	controller.add_state_button(button);
#	controller.add_side_panel(dialog);
#	
#	curve_editor.init(self)
	curve_editor.raycastToGround = false; 
	curve_editor.heightAboutGround = CollisionHeightAdjust;
	
	# hide some stuff from the RailEditorState such as track and junction
	get_node(hide_path).set_visible(false);
	get_node(hide_path2).set_visible(false);
	
	#add_child(bridgeInst)
	#bridgeInst.set_visible(false)

#func state_enter(prevState):
#	okBtn.connect("pressed", self, "_on_OkBtn_pressed")
#	cancelBtn.connect("pressed", self, "_on_CancelBtn_pressed")
	
#	okBtn.set_disabled(true)
#	controller.clear_mouse_over()
#	controller.mouse_over_enabled = false
	
#func state_exit(nextState):	
#	okBtn.disconnect("pressed", self, "_on_OkBtn_pressed")
#	cancelBtn.disconnect("pressed", self, "_on_CancelBtn_pressed")
#	
#	dialog.set_visible(false)
#	
#	# reset the curves
#	curve_editor.end()
#	curve_editor.clearAll()
#	
#	finishedLayingTrack = false
#	controller.mouse_over_enabled = true
	
#func state_unhandled_input(event):	
#	if (event.type == InputEvent.MOUSE_BUTTON and event.button_index == BUTTON_RIGHT and event.pressed):
#		rightMousePressed = true
#		
#	if (event.type == InputEvent.MOUSE_BUTTON and event.button_index == BUTTON_LEFT and event.pressed):
#		leftMousePressed = true

				
# Update physics processing.
func state_physics_process(delta):
	updateUI()
	
	if (rightMousePressed):
		rightMousePressed = false # handled the event
		undo_last_track();
		
	if (finishedLayingTrack):		
		return
		
	var collisions = controller.mouse_pick()
	var collisionPoint = Vector3(0,0,0)
	if (collisions.size()):
		collisionPoint = collisions[0]["position"] + collisions[0]["normal"] * CollisionHeightAdjust# + Vector3(0.0, bridgeHeight, 0.0)
		
	# user clicks
	if (leftMousePressed):
		leftMousePressed = false # handled the event
		
		# starting a spline
		if (!curve_editor.isActive()):
			curve_editor.start(track_inst.instanceTemplate, collisionPoint, Vector3(0, 0, 0), Vector3(0, 0, 0));	
			dialog.set_visible(true)
		else:	
			curve_editor.bakeEndPoint(collisionPoint)

	curve_editor.updateEndPoint(collisionPoint)

#func undo_last_track():
#	finishedLayingTrack = false; # if the track was finished, it now is not finished!
#	curve_editor.undo_last();		
#	return;

#func finishLayingTrack():
#	curve_editor.end()
#	finishedLayingTrack = true
#	controller.mouse_over_enabled = true

#	
#func updateUI():	
#	var f = track_inst.getFinancesForLength(curve_editor.getLength())
#	dialog.get_node("LengthLbl").set_text("%dM" % curve_editor.getLength())
#	dialog.get_node("CostPerMetreLbl").set_text(Util.formatCurrency(bridgeInst.finances.purchaseCost))
#	dialog.get_node("TotalCostLbl").set_text(Util.formatCurrency(f.purchaseCost))
#	
#	dialog.get_node("MaintenanceCostPerMetreLbl").set_text(Util.formatCurrency(bridgeInst.finances.maintenanceCost))
#	dialog.get_node("MaintenanceTotalCostLbl").set_text(Util.formatCurrency(f.maintenanceCost))
#	
#	# enable OK button if we can afford it and the track is complete
#	if (controller.get_company().can_afford_cash(f.purchaseCost) && curve_editor.getCurveList().size() > 0):
#		okBtn.set_disabled(false);
#	else:
#		okBtn.set_disabled(true)
#	
#	
#func packCurveList(curveList):
#	var array = []
#	for curve in curveList:
#		var cArray = Util.packCurve(curve)				
#		array.append(cArray)
#	return array
#	
#func unpackCurveList(packedCurveList):
#	var curveArray = []
#	for cArray in packedCurveList:
#		var curve = Util.unpackCurve(cArray)			
#		curveArray.append(curve)
#			
#	return curveArray
	
func createBridge():
	var globalBridgesNode = get_node("/root/World/MapCore/BridgesAndTunnels")
	rpc("createBridgeRPC", packCurveList(curve_editor.getCurveList()));
	
	# yes, tracks cost money and lives!
	var f = track_inst.getFinancesForLength(curve_editor.getLength())
	controller.get_company().pay_cash(f.purchaseCost, IncomeStatement.BRIDGES)

	_on_CancelBtn_pressed() # close the dialog

remotesync func createBridgeRPC(packedCurveList):
	var curveList =  unpackCurveList(packedCurveList)

	# now create the bridges to connect the nodes
	for i in range(0, curveList.size()):
		var curve = curveList[i]
		var bridge = track_inst.duplicate()
		
		bridge.isBridge = true; # should there be: initAsBridge or initAsTrack?
		bridge.init(controller.get_company(), null, null, curve)
		bridge.set_name(world.network.next_unique_name(controller.networkId, "Bridge"))
		
		var globalBridgesNode = get_node("/root/World/MapCore/BridgesAndTunnels")
		globalBridgesNode.add_child(bridge)
		

func _on_OkBtn_pressed():
	createBridge();
	return;
	
#func _on_CancelBtn_pressed():	
#	controller.hud.clear_toggled_state_buttons();
#	return;	
#
#func _on_TextureButton_toggled( pressed ):
#	toggle_state(pressed);
#	return;
