#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var world = WorldMgr.get_world(self);

var market;
onready var marketDlg = get_node("Market")
var timer;
onready var ui_market_button = get_node("MarketButton");

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	controller.add_child(self);
	controller.hud.add_left_panel(marketDlg);
	controller.hud.add_dialog_panel_button(ui_market_button);
	
	marketDlg.set_visible(false);
	return;
	
func _ready():
	if (!world):
		return;
		
	market = world.market;
	timer = Util.create_timer(self, "_update_dialog", 1.0, true);
		
	ui_market_button.self_modulate.a = 0.5;

func _update_dialog():
	if (!marketDlg):
		return;
		
	if (!marketDlg.is_visible()):
		return
		
	# find min and max values for all resources
	var idx = 0
	var legendTree = BUtil.find_child(marketDlg, "ValueHistoryLegendTre");
	var items = Util.getTreeItemChildren(legendTree.get_root())
	for r in market.get_resources():		
		if (!r.used_in_scene()):
			continue
			
		var item = items[idx]
		if (r.value != null):
			item.set_text(2, Util.format_currency(r.get_value(1)))
			item.set_text_align(2, TreeItem.ALIGN_RIGHT)
		idx += 1
		
	return;
		
func _on_MarketButton_toggled(pressed):
	marketDlg.set_visible(pressed);
	ui_market_button.self_modulate.a = 1.0 if pressed else 0.5;
	
	if (!marketDlg.is_visible()):
		return
		
	# setup the dialog for stuff we only want to do on first show
	var legendTree = BUtil.find_child(marketDlg, "ValueHistoryLegendTre");
	legendTree.clear()
	legendTree.set_hide_root(true)
	legendTree.create_item() # create the root item
	legendTree.set_columns(3)
	
	legendTree.set_column_title(1, "Resource")
	legendTree.set_column_title(2, "Value")
	legendTree.set_column_titles_visible(true)
	legendTree.set_select_mode(legendTree.SELECT_ROW)
	
	legendTree.set_column_expand(1, true)
	legendTree.set_column_min_width(0, 2)
	legendTree.set_column_min_width(1, 5)
	legendTree.set_column_min_width(2, 3)
	
	# find min and max values for all resources
	for r in market.get_resources():		
		if (!r.used_in_scene()):
			continue
			
		var item = legendTree.create_item(legendTree.get_root())
		#item.set_custom_bg_color(0, r.colour)
		item.set_text(1, r.display_name)
		item.set_icon_max_width(0, 20)
		item.set_icon(0, r.get_icon())
		
	_update_dialog()
	return;


func _on_Title_icon_pressed():
	ui_market_button.set_pressed(false);
	_on_MarketButton_toggled(false);
