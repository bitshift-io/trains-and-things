#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Panel

onready var market = BUtil.find_first_child_by_class_name(get_node("/root"), "Market") 

var updateTimer = Timer.new()

func _ready():
	updateTimer.set_one_shot(false)
	updateTimer.set_wait_time(1.0)
	updateTimer.connect("timeout", self, "updateTimerTimeout")
	add_child(updateTimer)
	updateTimer.start()
	pass

func updateTimerTimeout():
	if (is_visible()):
		update()
	
func _draw():
	var size = get_size()
	var maxValue = null
	var minValue = null
		
	# find min and max values for all resources
	for r in market.getResources():		
		if (!r.usedInScene()):
			continue
			
		if (maxValue == null || r.maxValue > maxValue):
			maxValue = r.maxValue
		if (minValue == null || r.minValue < minValue):
			minValue = r.minValue
		
	var minMaxRange = max(1, maxValue - minValue) # max saves dive by zero!
	
	# now go through and plot the graph
	for r in market.getResources():
		if (!r.usedInScene()):
			continue
			
		var prevPt = null
		var xPct = 0
		var xInc = size[0] / r.valueHistory.size()
		for h in r.valueHistory:
			var yPct = (h - minValue) / minMaxRange		
			var pt = Vector2(xPct, size[1] - (size[1] * yPct))
			xPct += xInc
			
			if (prevPt != null):
				draw_line(prevPt, pt, r.colour)
				
			prevPt = pt
	
	
