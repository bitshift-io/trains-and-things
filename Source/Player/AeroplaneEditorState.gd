#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "VehicleEditorState.gd"

export(float) var landLength = 1.0; # strength of spline take off and landing curves (higher give steeper curves)
export(float) var flightPathHeight = 100.0; # the height planes fly at above the airport
export(float) var reachHeightPathDist = 100.0; # the distance till we reach the flight path height

export(PackedScene) var trackTemplate;

func is_usable(resource_node):
	return true;
	
func create_route(stationsToVisit, company):
	# TODO: fixme! broken by TT-221
	# changed to take a list of paths, but this used to take a list of nodes
	for i in range(1, stationsToVisit.size()):
		# create a curve from A to B and then B to A
		
		if (stationsToVisit[0].findRoutes(stationsToVisit[1]).empty()):
			buildFlightPath(stationsToVisit[0], stationsToVisit[1], company);
			
		if (stationsToVisit[1].findRoutes(stationsToVisit[0]).empty()):
			buildFlightPath(stationsToVisit[1], stationsToVisit[0], company);
		
#	var vehicle_route = VehicleRoute.new();
#	vehicle_route.init(stationsToVisitPathList);
#	return vehicle_route;
	return;
	
func buildFlightPath(airportA, airportB, company):
	var runwayA = airportA.getRunwayNode();
	var runwayB = airportB.getRunwayNode();
	
	var xformA = runwayA.get_global_transform();
	var xformB = runwayB.get_global_transform();
	
	var curve = Curve3D.new();
	
	var halfRunwayLen = 20.0;
	
	
	# when we have reached flying altitude and now can steer to B
	var exitPtA = xformA.origin + (xformA.basis[2] * halfRunwayLen) + (xformA.basis[2] * reachHeightPathDist) + Vector3(0, flightPathHeight, 0);
	
	# the point we take to enter the landing approach at B
	var entrancePtB = xformB.origin - (xformB.basis[2] * halfRunwayLen) - (xformB.basis[2] * reachHeightPathDist) + Vector3(0, flightPathHeight, 0);
	
	# get the direction between exiting A and entering B
	var dirA = xformA.basis[2];
	var dirB = xformB.basis[2];
	var dot = dirA.dot(dirB);
	
	# add points to get us to to exitPtA
	curve.add_point(xformA.origin); # centre of runway
	curve.add_point(xformA.origin + (xformA.basis[2] * halfRunwayLen), Vector3(0, 0, 0), xformA.basis[2] * landLength); # end of runway
	curve.add_point(exitPtA, -xformA.basis[2] * landLength, xformA.basis[2] * landLength); # straight out front, reaching altitude
	
	# TODO: compute roll to make aeroplanes roll nicely into the curve
	
	if (dot > 0.0):
		var DistTweak = 0.3;
		var vecBetween = (entrancePtB - exitPtA);
		var distBetween = vecBetween.length();
		
		# vectors face the same direction, so find the midpoint and the direction in the mid point
		var midPt = exitPtA + (entrancePtB - exitPtA) * 0.5;
		var midPtDir = (-dirA - dirB) * 0.5;
		midPtDir = midPtDir * distBetween * DistTweak;
		curve.add_point(midPt, -midPtDir, midPtDir);
	else:
		# vectors face opposing directions, we can do a curve
		var vecBetween = (entrancePtB - exitPtA);
		var distBetween = vecBetween.length();
		var midPt = exitPtA + vecBetween * 0.5;
		var avgDir = (dirA - dirB) * 0.5;
		var DistTweak = 0.3;
		var DirTweak = 0.5;
		midPt = midPt + (avgDir * distBetween * DistTweak);
		
		var midPtDir = avgDir.cross(Vector3(0, 1, 0));
		midPtDir = midPtDir * distBetween * DirTweak;
		curve.add_point(midPt, -midPtDir, midPtDir);
	
	
	# add points to take us from entering B to landing
	curve.add_point(entrancePtB, -xformB.basis[2] * landLength, xformB.basis[2] * landLength); # straight behind, at altitude ready to land
	curve.add_point(xformB.origin - (xformB.basis[2] * halfRunwayLen), -xformB.basis[2] * landLength); # start landing runway
	curve.add_point(xformB.origin); # centre of runway
	
	var globalNode = get_node("/root/World/MapCore/" + globalsNodeName);
	var track = trackTemplate.instance();
	track.init(company, airportA.get_junctions()[0].getConnections()[1], airportB.get_junctions()[0].getConnections()[0], curve);
	track.set_name(world.network.next_unique_name(controller.networkId, networkNamePrefix + "Track"))
	track.setSplineMeshHidden(true); # set to false to debug flight paths
	globalNode.add_child(track);
	return;
	
func spawnCarriage(resourceName):
	var carriage = .spawnCarriage(resourceName);
	carriage.get_node("Vehicle-Carriage-A").set_hidden(true); # hide the carriages
	return carriage;
	
	
