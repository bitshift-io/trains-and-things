#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var dialog = get_node("Resources")

onready var ui_resource_node_info_template = BUtil.find_child(dialog, "ResourceNodeInfoTemplate");
onready var ui_resource_node_list = dialog.find_node("ResourceNodeList");
onready var ui_search_edit = BUtil.find_child(dialog, "SearchEdit");
onready var ui_resources_button = get_node("ResourcesButton");

var ui_populated = false;
var controller;

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	self.controller = controller;
	controller.add_child(self);
	controller.hud.add_left_panel(dialog);
	controller.hud.add_dialog_panel_button(ui_resources_button);


func _ready():
	BUtil.reparent(ui_resource_node_info_template, null); # remove from parent
	ui_search_edit.set_right_icon(ui_search_edit.get_icon("Search", "EditorIcons"));
	ui_resources_button.self_modulate.a = 0.5;
	EventMgr.listen([EventMgr.Id.BUILDING_CONSTRUCTED, EventMgr.Id.BUILDING_DELETED, EventMgr.Id.VEHICLE_CONSTRUCTED, EventMgr.Id.VEHICLE_EDITED, EventMgr.Id.VEHICLE_DELETED], self, "_on_warehouse_possibly_changed");
	
	
func _on_warehouse_possibly_changed(p_params):
	call_deferred("_populate_ui");
	
	
func _populate_ui():
	# only update dialog if visible
	if (!dialog.visible):
		return;
		
	var resource_nodes = get_tree().get_nodes_in_group("resource_nodes");
	BUtil.delete_children(ui_resource_node_list);
	for rn in resource_nodes:
		if (rn.type == ResourceNode.TYPE.PROP):
			continue;
			
		# only show OUR own companies warehouse
		if (rn.type == ResourceNode.TYPE.WAREHOUSE):
			if (rn.company != controller.get_company()):
				continue;
				
		var res_node_info = ui_resource_node_info_template.duplicate();
		ui_resource_node_list.add_child(res_node_info);
		res_node_info.set_resource_node(rn);
		res_node_info.controller = controller;
		res_node_info.set_delete_button_visible(false);
		res_node_info.set_train_inventory_visible(false);

	return;

	
func _on_SearchEdit_text_changed( text ):
	for res_node_info in ui_resource_node_list.get_children():
		res_node_info.set_visible(res_node_info.search_match("*" + text + "*"));


func _on_ResourcesButton_toggled(pressed):
	dialog.set_visible(pressed);
	_populate_ui();
	ui_resources_button.self_modulate.a = 1.0 if pressed else 0.5;


func _on_Title_icon_pressed():
	ui_resources_button.set_pressed(false);
	_on_ResourcesButton_toggled(false);
