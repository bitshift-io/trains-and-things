#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# this is the resource node UI panel that sits inside dialogs
# it wraps a resource node and provides a UI on top.
#
extends Panel

signal delete_pressed(sender);
signal locate_pressed(sender);
signal resource_pressed(resource_node_info, button);

onready var ui_icon_button_template = BUtil.find_child(self, "ResourceIconButtonTemplate");
onready var ui_train_inventory_container = BUtil.find_child(self, "TrainInventoryContainer");
onready var ui_supply_container = BUtil.find_child(self, "SupplyContainer");
onready var ui_demand_container = BUtil.find_child(self, "DemandContainer");
onready var ui_delete_button = BUtil.find_child(self, "DeleteButton");

var controller;
var resource_node;


func _ready():
	ui_icon_button_template.get_parent().remove_child(ui_icon_button_template); # remove from parent
	
	ui_delete_button.set_button_icon(ui_delete_button.get_icon("Close", "EditorIcons"));
	_update_height();
	return;
	
	
func set_delete_button_visible(vis):
	BUtil.find_child(self, "DeleteButton").set_visible(vis);
	
	
func set_train_inventory_visible(vis):
	BUtil.find_child(self, "TrainInventoryPanel").set_visible(vis);
	_update_height();


func create_resource_icon(resource_data, bottom_display_type):
	var res_name = resource_data.get_market_resource().name if resource_data.get_market_resource() else "Unknown resource";
	var resource_button = ui_icon_button_template.duplicate();
	resource_button.init_as_button(resource_data, bottom_display_type);
	resource_button.connect("pressed", self, "_on_resource_button_pressed", [resource_button]);
	resource_button.set_meta("resource_node_info", self);
	return resource_button;
	
	
func clone_resource_button(resource_button):
	var clone = resource_button.duplicate();
	clone.connect("pressed", self, "_on_resource_button_pressed", [clone]);
	clone.set_meta("resource_node_info", self);
	return clone;
		
		
func set_resource_node(rn):
	resource_node = rn;
	
	var res_node_name = resource_node.get_display_name();
	var title_lbl = BUtil.find_child(self, "Title");
	title_lbl.set_text(res_node_name);
	
	var supplies_anything = false;
	var demands_anything = false;
	
	# populate the supply/demand section with icons
	for resource_data in resource_node.get_resource_data_list():
		if (resource_data.isSupplied):
			supplies_anything = true;
			var icon_button = create_resource_icon(resource_data, ui_icon_button_template.DisplayType.SUPPLY);
			ui_supply_container.add_child(icon_button);
			
		if (resource_data.isDemanded):
			demands_anything = true;
			var icon_button = create_resource_icon(resource_data, ui_icon_button_template.DisplayType.DEMAND);
			ui_demand_container.add_child(icon_button);

	#ui_supply_container.set_visible(supplies_anything);
	#ui_demand_container.set_visible(demands_anything);
			
	_update_height();
	
	
# update the height of panel to minimize its size
func _update_height():
	var height = 0;
	var container = $"VBoxContainer";
	
	var min_sz = container.get_combined_minimum_size();
#
#	var sz = container.get_size();
#
#	var count = 0;
#	for child in container.get_children():
#		if (child.is_visible()):
#			height += child.rect_size.y;
#			count += 1;
#
#	var sep = container.get_constant("separation");
#	height += sep * (count - 1);
#
#	var margins = abs(container.margin_top) + abs(container.margin_bottom);
#	height += margins;
	rect_min_size = min_sz; #Vector2(rect_min_size.x, height);
	#rect_size = Vector2(rect_size.x, height);
	return;


func search_match(text):
	var lower_text = text.to_lower();
	
	# match resource node name
	if (resource_node.name.to_lower().match(lower_text)):
		return true;
		
	# match resource name
	for resource_data in resource_node.get_resource_data_list():
		var res_name = resource_data.get_market_resource().name;
		if (res_name.to_lower().match(lower_text)):
			return true;
			
		if ("supply".match(lower_text) && resource_data.isSupplied):
			return true;
			
		if ("demand".match(lower_text) && resource_data.isDemanded):
			return true;
			
	return false;
	
	
func is_demanded(name):
	return resource_node.is_demanded(name);


func is_supplied(name):
	return resource_node.is_supplied(name);
	
	
func _on_resource_button_pressed(resource_button):
	emit_signal("resource_pressed", self, resource_button);
	
	
func _on_LocateButton_pressed():
	controller.camera_pan_to_node(resource_node);
	emit_signal("locate_pressed", self);


func _on_DeleteButton_pressed():
	emit_signal("delete_pressed", self);
		
		
func find_last_resource_button_from_container(container, resource_name):
	var cont_child_count = container.get_child_count();
	for i in range(cont_child_count - 1, -1, -1):
		var child = container.get_child(i);
		if (child.resource_data.get_name() == resource_name):
			return child;
			
	return null;
	
	
# return an array of the cargo that is configured at this city/terminal as a list of resource names
func get_load_resources():
	var results = {};
	for i in range(0, ui_supply_container.get_child_count()):
		var resource_button  = ui_supply_container.get_child(i);
		if (resource_button.amount_delta < 0):
			Util.increment_count_in_dictionary(results, resource_button.resource_data.get_name(), -resource_button.amount_delta);
		
	return results;
	
	
# return an array of the cargo that is unloaded at this resource node
func get_unload_resources():
	var results = {};
	for i in range(0, ui_demand_container.get_child_count()):
		var resource_button = ui_demand_container.get_child(i);
		if (resource_button.amount_delta > 0):
			Util.increment_count_in_dictionary(results, resource_button.resource_data.get_name(), resource_button.amount_delta);
		
	return results;
	
	
# the configuration when the vehicle leaves that station
func get_manifest_resources():
	var results = {};
	for i in range(0, ui_train_inventory_container.get_child_count()):
		var resource_button  = ui_train_inventory_container.get_child(i);
		results[resource_button.resource_data.get_name()] = resource_button.amount;
		
	return results;
		
	
# this keeps the resources buttons sorted by resource name
func add_resource_button_to_container(p_container, p_resource_button):
	# reverse iterator
	for i in range(p_container.get_child_count() - 1, -1, -1):
		var rb = p_container.get_child(i);
		if (p_resource_button.resource_data.get_name() >= rb.resource_data.get_name()):
			p_container.add_child_below_node(rb, p_resource_button);
			return;
			
	p_container.add_child(p_resource_button);
	p_container.move_child(p_resource_button, 1);

	
# add a new resource to the vehicle inventory
func add_resource_button_to_vehicle_inventory(resource_button):
	add_resource_button_to_container(ui_train_inventory_container, resource_button);
	
	
# add a new resource to the demand container
func add_resource_button_to_demand(resource_button):
	add_resource_button_to_container(ui_demand_container, resource_button);
	
	
# add a new resource to the supply container
func add_resource_button_to_supply(resource_button):
	add_resource_button_to_container(ui_supply_container, resource_button);
	
	
# get the demand button given a resource name
func get_demand_resource_button(p_resource_name):
	# the user is attempting to meet demand
	for i in range(0, ui_demand_container.get_child_count()):
		var demand_resource_button = ui_demand_container.get_child(i);
		if (demand_resource_button.resource_data.get_name() == p_resource_name):
			return demand_resource_button;
			
	return null;
	
	
# is the resource node we reperesent a warehouse?
func is_warehouse():
	return resource_node.type == resource_node.TYPE.WAREHOUSE;
		
	

