#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var market = BUtil.find_first_child_by_class_name(get_node("/root"), "Market")
onready var panel = get_node("ResearchPanel")
#onready var timer = Util.create_timer(self, "_update_dialog", 1.0, true);

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	controller.add_child(self);
	controller.add_left_panel(panel);
	controller.add_dialog_panel_button($"ResearchButton");
	
	panel.set_visible(false);
	return;

func _update_dialog():
	if (!panel.is_visible()):
		return
		
	# find min and max values for all resources
	var idx = 0
	var legendTree = BUtil.find_child(panel, "ValueHistoryLegendTre");
	var items = Util.getTreeItemChildren(legendTree.get_root())
	for r in market.get_resources():		
		if (!r.used_in_scene()):
			continue
			
		var item = items[idx]
		if (r.value != null):
			item.set_text(2, Util.format_currency(r.value))
		idx += 1
		
	return;
		
func _on_MarketButton_pressed():
	if (!panel.is_visible()):
		panel.set_visible(true);
		#var animation_player = BUtil.find_child(panel, "AnimationPlayer");
		#animation_player.play("show");
	else:
		panel.set_visible(false);
	
#	panel.set_visible(true);
#	var animation_player = BUtil.find_child(self, "AnimationPlayer");
#	animation_player.play("show");
#
#	if (panel.is_visible()):
#		animation_player.play("show");
#	else:
#		animation_player.play_backwards("show");
#
#	if (!panel.is_visible()):
#		return
#
#	# setup the dialog for stuff we only want to do on first show
#	var legendTree = BUtil.find_child(panel, "ValueHistoryLegendTre");
#	legendTree.clear()
#	legendTree.set_hide_root(true)
#	legendTree.create_item() # create the root item
#	legendTree.set_columns(3)
#
#	legendTree.set_column_title(1, "Resource")
#	legendTree.set_column_title(2, "Value")
#	legendTree.set_column_titles_visible(true)
#	legendTree.set_select_mode(legendTree.SELECT_ROW)
#
#	legendTree.set_column_expand(1, true)
#	legendTree.set_column_min_width(0, 2)
#	legendTree.set_column_min_width(1, 5)
#	legendTree.set_column_min_width(2, 3)
#
#	# find min and max values for all resources
#	for r in market.get_resources():		
#		if (!r.used_in_scene()):
#			continue
#
#		var item = legendTree.create_item(legendTree.get_root())
#		#item.set_custom_bg_color(0, r.colour)
#		item.set_text(1, r.name)
#		item.set_icon_max_width(0, 20)
#		item.set_icon(0, r.get_icon())
#
#	_update_dialog()
	return;
