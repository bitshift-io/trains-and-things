#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var current_audio_stream_player = null;

func _ready():
	set_process(false);
	if (BDatabase.get_value("dev/mute_audio", null) != null):
		return;
		
	# disable music when running scripts
	if (Main.script_path):
		return;
		
	set_process(true);
	play_next();
	return;
	
func _process(delta):
	if (!current_audio_stream_player or !current_audio_stream_player.is_playing()):
		if (current_audio_stream_player):
			current_audio_stream_player.stop();
			
		play_next();
		
	return;
	
func play_next():
	var children = get_children();
	var child_count = children.size();
	var next_audio_stream_player = current_audio_stream_player;
	
	while (next_audio_stream_player == current_audio_stream_player && child_count > 1):
		var rand = rand_range(0, child_count - 1);
		next_audio_stream_player = children[rand];
	
	current_audio_stream_player = next_audio_stream_player;
	if (!current_audio_stream_player):
		return;
		
	current_audio_stream_player.get_stream().set_loop(false);
	current_audio_stream_player.play();
	return;
