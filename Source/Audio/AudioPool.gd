#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node
tool

# this hsouldl be an enum
const DONT_PLAY_IF_PLAYING = 0;
const STOP_AND_PLAY_NEW = 1;

export(int, "Don't play if playing", "Stop and play new") var play_mode = DONT_PLAY_IF_PLAYING;

export(float) var chance_to_play = 100;

export(float) var unit_db = 10;
export(int) var unit_size = 100;

export(bool) var trigger_on_ready = false;

var bus = "Effect";

var playing = null;

func _set(property, value):
	print("_set: ", property, " val: ", value)

	if (property == "bus"):
		bus = value

	#._set(property, value)

func _get(property):
	print("_get:", property)

	if (property == "bus"):
		return bus

	#return _get(property)
	
func _get_property_list():
	var listStr = ""
	for i in range(0, AudioServer.get_bus_count()):
		listStr += AudioServer.get_bus_name(i) + ",";
	
	var property_info = {
		"name": "bus",
		"type": TYPE_STRING,
		"hint": PROPERTY_HINT_ENUM,
		"hint_string": listStr
	}
	return [property_info]
	
func _ready():
	if (play_mode == null):
		play_mode = DONT_PLAY_IF_PLAYING;

	if (bus == null || bus == ""):
		bus = "Effect";
		
	call_deferred("_configure_children");
	
	if (trigger_on_ready):
		trigger();
		
	return;

func _configure_children():
	# apply values to children
	for child in get_children():
		if (child is AudioStreamPlayer3D):
			child.unit_db = unit_db;
			child.unit_size = unit_size;
			child.set_bus(bus);
		elif (child is AudioStreamPlayer):
			child.set_bus(bus);
			
	return;
	
# play a random sound
func play():
	if (BDatabase.get_value("dev/mute_audio", null) != null):
		return;
		
	var count = get_child_count();
	if (count == 0):
		return;
		
	var rand_id = rand_range(0, count);
	playing = get_child(rand_id);
	playing.play();
	return;
	
func trigger():
	if (playing and playing.is_playing()):
		if (play_mode == STOP_AND_PLAY_NEW):
			playing.stop();
			playing = null;
			
		if (play_mode == DONT_PLAY_IF_PLAYING):
			return;
			
	if (rand_range(0, 100) > chance_to_play):
		return;
		
	play();
	return;
	
func stop():
	if (playing):
		playing.stop();
		playing = null;
