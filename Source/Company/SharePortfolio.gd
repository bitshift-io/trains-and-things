#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# A portfolio of shares

extends Reference
class_name SharePortfolio

# this might be better for dealing with non-company shares
# https://www.investopedia.com/ask/answers/difference-between-shares-and-stocks/
class Share:
	signal changed;
	
	var _company; # a weak ref
	var holding; # how many shares do we hold?
	var invested; # how much money spent to buy the shares
	
	func _init():
		holding = 0;
		invested = 0;
		_company = WeakRef.new();
		return;
		
	func get_company():
		return _company.get_ref();
	
	
var shares = {}; # is a map or array best or a structure of sorts? - how to deal with non-company shares?


func get(p_company):
	if (shares.has(p_company)):
		return shares[p_company];
	
	var share = Share.new();
	share._company = weakref(p_company);
	shares[p_company] = share;
	return share;


func adjust(p_company, p_holding, p_invested):
	var share = get(p_company);
	share.holding = max(0, share.holding + p_holding);
	share.invested = max(0, share.invested + p_invested);
	share.emit_signal("changed");

