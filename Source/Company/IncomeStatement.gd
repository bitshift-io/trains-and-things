#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# class for tracking yearly/quarterly income statements
# and compiling them

extends Reference
class_name IncomeStatement

# date range for this statement
var date = {
	"start": null,
	"end": null
};

# fields:
# we could make this simpler in future if we want:
# vehicles, infrastructure, loans
# or
# vehicles, buildings, tracks, loans
# for now break it down in to precisely what the user can build: 1 for each state button
const VEHICLES = "vehicles"; # I like trains!
const WAREHOUSES = "warehouses";
const TRACKS = "tracks";
const TERMINALS = "terminals";
const LOANS = "loans";
const BRIDGES = "bridges";

var revenues = {
	VEHICLES: 0,
	#LOANS: 0 # is taking out a loan a revenue?
};

var expenses = {
	VEHICLES: 0,
	WAREHOUSES: 0,
	TRACKS: 0,
	TERMINALS: 0,
	LOANS: 0
};


# combine an array of income statements to this one
func join(var p_other_income_statements: Array):
	# TODO:
	return;
	
	
func add_expenses(var amount: float, var field):
	if (!field || !expenses.has(field)):
		return;
		
	expenses[field] += amount;


func add_revenues(var amount: float, var field):
	if (!field || !revenues.has(field)):
		return;
		
	revenues[field] += amount;
	
	
func get_total_revenues():
	var total = 0;
	for val in revenues.values():
		total += val;
		
	return total;
	

func get_total_expenses():
	var total = 0;
	for val in expenses.values():
		total += val;
		
	return total;
	
	
func get_revenues(var field) -> float:
	return revenues[field];
	

func get_expenses(var field) -> float:
	return expenses[field];
	
	
