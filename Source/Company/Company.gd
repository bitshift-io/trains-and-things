#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial
class_name Company

export(String) var display_name = null;
var players = []

# class handles the aspects of the company
# players become children of this company
var cash : float = 0
var loan = 0

export(int) var initial_shares_available = 100;
var shares_available = initial_shares_available;
var share_value = 10000;
var _is_available_on_share_market = false;
onready var world = WorldMgr.get_world(self);
var share_portfolio = SharePortfolio.new();
var income_statements = [];

func _ready():
	set_process(false);
	set_process_input(false);
	
	if (!world):
		return;
	
	var date_time = world.date_time;
	if (get_tree().is_network_server()):
		add_new_income_statement({ "months": world.date_time.months });
		date_time.connect("month_changed", self, "_month_changed")
		
	add_to_group("persist");
	add_to_group("companies");
	
	cash = BDatabase.get_value("game_settings/company_starting_cash", 0);
	return;
	
	
func get_income_statements():
	return income_statements;
	
	
func get_share_portfolio():
	return share_portfolio;
	
	
func is_available_on_share_market():
	return _is_available_on_share_market;
	
	
# should this be computed on the fly? that means if one player snaps up a bargin,
# prices will fluctuate faster....
# else we update it once every second or so
func get_share_value():
	# this is the hard part, we must not include cash on hand in this computation
	
	# designer tweakable
	var percent_min = 0.1;
	var percent_max = 0.5;
	var min_share_value = 10000;
	
	
	# as shares becomes rarer, their prices goes up
	var availability_multiplier = 1.0 - float(shares_available) / float(initial_shares_available);
	
	# the physical asset value
	var assests_value = get_assets_total();
	
	# remap percentage from 0->1 to percent_min->percent_max
	var percentage = availability_multiplier;
	percentage = percent_min + percentage * (percent_max - percent_min);
	
	# when we have time/asset value protection to stop companies being floated when they are small
	share_value = assests_value * percentage;
	
	if (share_value > min_share_value):
		_is_available_on_share_market = true;
	
	return share_value;
	
# get interest rate as number between 0 to 100
func get_interest_rate():
	return BDatabase.get_value("game_settings/interest_rate", 0);
	
	
# yearly interest repayments
func get_interest_amount():
	return loan * (get_interest_rate() / 100.0);
	
	
# monthly interest on loan
func _month_changed(p_month_index):
	assert(get_tree().is_network_server());
	
	var date_time = world.date_time;
	pay_cash(date_time.yearlyToMonthly(get_interest_amount()), IncomeStatement.LOANS);

	if (p_month_index % 3 == 0 && world.date_time.months > 0):
		add_new_income_statement({ "months": date_time.months });

	return;
	
	
func get_players():
	return players
	
	
func set_display_name(n):
	rpc("_set_display_name_rpc", n)
	
	
remotesync func _set_display_name_rpc(n):
	if (n == display_name):
		return;

	var previous_display_name = display_name
	display_name = n
	if (previous_display_name != null):
		EventMgr.trigger(EventMgr.Id.COMPANY_NAME_CHANGED, { "sender": self, "display_name": display_name, "previous_display_name": previous_display_name });
		
	return;
	
	
func add_player(pc):
	players.append(pc)
	
	
func remove_player(pc):
	players.erase(pc)
	# dissolve the company ?
	
	
# get the local (aka master) player controller assigned to this company (if there is one)
func get_master_player():
	for c in players:
		if (c.is_network_master()):
			return c;
			
	return null;
	
	
# how much cash we got?
func get_cash():
	return cash
	
	
func can_afford_cash(money):
	if (cash >= money):
		return true
		
	return false
	
	
# given a money value, determine how much is required to call loan
# to give us enough cash to pay the given p_amount
# return -1 if we can't do it!
func loan_amount_required(p_amount):
	var amount_can_loan = get_loan_limit() - loan;
	if (p_amount > amount_can_loan + cash):
		return -1;
		
	var amount_need_to_loan = p_amount - cash;
	if (amount_need_to_loan <= 0):
		return 0;
		
	return amount_need_to_loan;
	
	
# round p_amount up to the closest multiple of loan_size
func loan_amount_to_block(p_amount):
	var loan_block = get_loan_size();
	var times = ceil(p_amount / loan_block); # round up
	var block_amount = times * loan_block;
	return block_amount;
	
	
# when we take out a loan, the chunk size of the loan, i.e it must be a multiple of this when taking out a loan
func get_loan_size():
	return BDatabase.get_value("game_settings/loan_size", 0);
	
	
func get_loan_limit():
	return BDatabase.get_value("game_settings/loan_limit", 0);
	
	
func get_loan():
	return loan;
	
	
# given an 'amount', can I take a loan to cover it?
func can_loan(p_amount):
	var amount_can_loan = get_loan_limit() - loan;
	if (p_amount <= amount_can_loan):
		return true;
		
	return false;


# seal off the previous income statement and start a new one
master func add_new_income_statement(var p_start_date):
	rpc("add_new_income_statement_rpc", p_start_date);
	
	
remotesync func add_new_income_statement_rpc(var p_start_date):
	var previous = get_current_income_statement();
	if (previous):
		previous.date.end = p_start_date;
	
	var statement = IncomeStatement.new();
	statement.date.start = p_start_date;
	income_statements.append(statement);
	
	# fire a quarterly report if this is the local players company
	# and not the first month of the game
	if (world.master_player == get_master_player() && world.date_time.months > 0):
		var revenues = Util.format_currency(previous.get_total_revenues());
		var expenses = Util.format_currency(-previous.get_total_expenses()); #-ve to show its a loss
		world.master_player.show_event({ "id": "COMPANY_QUARTERLY_REPORT", "revenues": revenues, "expenses": expenses });


# get the current income statement that is open for adding transactions
func get_current_income_statement():
	if (income_statements.size() <= 0):
		return null;
		
	return income_statements.back();


func pay_cash(money, p_income_statement_field):
	rpc("_pay_cash_rpc", money, p_income_statement_field)
	
	
remotesync func _pay_cash_rpc(money, p_income_statement_field):
	cash -= money;
	get_current_income_statement().add_expenses(money, p_income_statement_field);
	
	
func receive_cash(money, p_income_statement_field):
	rpc("_receive_cash_rpc", money, p_income_statement_field)
	
	
remotesync func _receive_cash_rpc(money, p_income_statement_field):
	cash += money
	get_current_income_statement().add_revenues(money, p_income_statement_field);
	
	
func take_out_loan(amount):
	if (amount <= 0):
		return;
		
	var amt = min(amount, get_loan_limit() - loan)
	rpc("_increment_loan_amount_rpc", amt)
	receive_cash(amt, IncomeStatement.LOANS)
	
	
# repay some of the loan
func repay(amount):
	var amt = min(amount, cash)
	amt = min(amt, loan)
	rpc("_increment_loan_amount_rpc", -amt)
	pay_cash(amt, IncomeStatement.LOANS)
	
	
remotesync func _increment_loan_amount_rpc(amount):
	loan += amount
	
	
func get_net_worth():
	return get_assets_total() - get_loan() + get_cash();
	
	
# get a total of all physical assets (cash not included)
func get_assets_total():
	var train_assets = 0.0
	var vehicles = filter(get_tree().get_nodes_in_group("vehicles"));
	for t in vehicles:
		train_assets += t.get_finances().get_asset_value()
			
	var track_assets = 0.0;
	var tracks = filter(get_tree().get_nodes_in_group("tracks"));
	for t in tracks:
		track_assets += t.get_finances().get_asset_value()

	var terminal_assets = 0.0;
	var terminals = filter(get_tree().get_nodes_in_group("vehicle_terminals"));
	for t in terminals:
		terminal_assets += t.get_finances().get_asset_value();

	var warehouse_assets = 0.0;
	var warehouses = filter(get_tree().get_nodes_in_group("warehouses"));
	for t in warehouses:
		warehouse_assets += t.get_finances().get_asset_value();

	var total_assets = train_assets + track_assets + terminal_assets + warehouse_assets;
	return total_assets;
	
	
static func sort_net_worth(a, b):
	if a.get_net_worth() > b.get_net_worth():
		return true;
		
	return false;
	
	
# get list of trains that belong to this company
# this might be a bottle neck, just to it as an array when a train is constructed or destroyed?
# gee, this should be a list in the company?
# or a child of the company?
func get_trains():
	var vehicles = [];
	var vehicles_group = get_tree().get_nodes_in_group("vehicles");
	for t in vehicles_group:
		if (t.company == self):
			vehicles.append(t);
			
	return vehicles;
		
		
func save_to(savegame):
	var pc_names = [];
	for c in players:
		pc_names.append(c.name);
		
	savegame.append_dict({
		players = pc_names
	});
	
	savegame.append_builtins({
		cash = cash,
		loan = loan,
		display_name = display_name
	});

	return true;
	
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	savegame.load_builtins();
	savegame.resolve_parent().add_child(self); # setup parenting
	
	# make appropriate players join this company
	# TODO: best to use a player id, as a name can be duplicate
	var pc_names = c["players"];
	for pc_name in pc_names:
		for c in get_tree().get_nodes_in_group("players"):
			if (c.name == pc_name):
				c.join_company_rpc(get_path());
		
	return true;
	
	
func get_display_name():
	return display_name;
	
	
func buy_shares(p_company, p_amount):
	rpc("_buy_shares_master_rpc", p_company.get_path(), p_amount);
	
	
# verify and compute the sale of the shares on the master
master func _buy_shares_master_rpc(p_company_path, p_amount):
	var other_company = get_node(p_company_path);
	var other_company_share_value = other_company.get_share_value();
	var most_shares_can_afford = int(cash / other_company_share_value) if (other_company_share_value > 0) else other_company.shares_available;
	var shares_purchased = min(most_shares_can_afford, min(other_company.shares_available, p_amount));
	if (shares_purchased <= 0):
		return;
		
	var money = other_company.get_share_value() * shares_purchased;
	rpc("_adjust_shares_sync_rpc", p_company_path, shares_purchased, money);
	
	
func sell_shares(p_company, p_amount):
	rpc("_sell_shares_master_rpc", p_company.get_path(), p_amount);
	
	
# verify and compute the sale of the shares on the master
master func _sell_shares_master_rpc(p_company_path, p_amount):
	var other_company = get_node(p_company_path);
	var shares_sold = max(0, min(share_portfolio.get(other_company).holding, p_amount));
	if (shares_sold <= 0):
		return;
		
	var money = shares_sold * other_company.get_share_value();
	rpc("_adjust_shares_sync_rpc", p_company_path, -shares_sold, -money);
	
	
# adjust this companies share in another company, p_amount specifies how many shares
# p_money specifies how much was paid for the shares
remotesync func _adjust_shares_sync_rpc(p_company_path, p_amount, p_money):
	var other_company = get_node(p_company_path);
	other_company.shares_available -= p_amount;
	other_company.cash += p_money;
	cash -= p_money;
	share_portfolio.adjust(other_company, p_amount, p_money);
	
	
# merge the other company into this company
func merge(p_company):
	rpc("_merge_rpc", p_company.get_path());
	
	
remotesync func _merge_rpc(p_company_path):
	var other_company = get_node(p_company_path);
	
	# force other players to join this company
	var other_pcs = other_company.get_players();
	for pc in other_pcs:
		pc.join_company_rpc(get_path());
		
	# change ownership of assets
	for t in get_tree().get_nodes_in_group("vehicles"):
		if (t.company == other_company):
			t.set_company(self);
			
	for t in get_tree().get_nodes_in_group("tracks"):
		if (t.company == other_company):
			t.set_company(self);
			
	for t in get_tree().get_nodes_in_group("vehicle_terminals"):
		if (t.company == other_company):
			t.set_company(self);
			
	for t in get_tree().get_nodes_in_group("junctions"):
		if (t.company == other_company):
			t.set_company(self);

	# what happens below we only want to occur one one machine
	if (!is_network_master()):
		return;

	# release any shares the other company has
	var sp = other_company.get_share_portfolio();
	for company_key in sp.shares.keys():
		var share = sp.get(company_key);
		other_company.sell_shares(share.get_company(), share.holding);

	# what about other companies who have a share in the company being bought out?
	# lets pay them out (which results in other_company loosing money)
	var companies = get_tree().get_nodes_in_group("companies");
	for company in companies:
		if (company == self): # don't sell this companies shares!
			continue;

		var share = company.get_share_portfolio().get(other_company);
		company.sell_shares(other_company, share.holding);

	# now other_company should have some serious debt!
	# which this company must take over
	receive_cash(other_company.cash, IncomeStatement.LOANS); # is this really loans?
	rpc("_increment_loan_amount_rpc", other_company.loan);

	# finally, destroy the company - or should we leave it around in a disabled state
	# for the final end game screen to show those who lost vs the winner?
	other_company.delete();
	return;
	
	
func delete():
	rpc("_delete_rpc") 
	
	
# delete on local machine only, no RPC call
func delete_local():
	_delete_rpc();
	
	
remotesync func _delete_rpc():
	# TODO: another should have a "delete" signal entities can listen into
	# also a broadcast for the victory condition?
	queue_free()
	
	
# this will toggle the text on a button and enable it as appropriate
# for any dialog that allows the user to make a money transaction
func update_purchase_button(p_ok_btn, p_cost, p_enabled = true):
	var can_afford = can_afford_cash(p_cost);
	var can_loan = can_loan(p_cost);
	
	var was_disabled = p_ok_btn.is_disabled();
	
	if (can_afford):
		p_ok_btn.text = "Purchase";
	else:
		p_ok_btn.text = "Loan & Purchase";
		
	if (p_enabled && (can_afford || can_loan)):
		p_ok_btn.set_disabled(false);
		# the enabled state changed, so grab focus when enabled
		if (was_disabled):
			p_ok_btn.grab_focus();
	else:
		p_ok_btn.set_disabled(true);
	
	
# filter the given array and return only those that belong to this company
func filter(p_nodes):
	var filtered = [];
	for n in p_nodes:
		if (n.has_method("get_company") && n.get_company() == self):
			filtered.append(n);
			
	return filtered;

