#
# This file has been modified by bitshift (http://bitshift.io)
# Original copyright notice:
#
# Godot Console main script
# Copyright (c) 2016 Hugo Locurcio and contributors - MIT license
#

extends CanvasLayer

const MAX_LINES = 100 # controls how much the console displays

signal set_console_open(open);

onready var console_root = BUtil.find_child(self, "ConsoleRoot");
onready var console_text = BUtil.find_child(self, "ConsoleText");
onready var console_input = BUtil.find_child(self, "LineEdit");

onready var perf_root = BUtil.find_child(self, "PerformanceRoot");

var line_history = [] # a list of lengths between \n characters
var last_line_complete = true
var cmd_history = []
var cmd_history_count = 0
var cmd_history_up = 0
# All recognized commands
var commands = {}
# All recognized cvars
var cvars = {}

# GLobal variable bindings
var variables = {}

var mouse_mode = Input.MOUSE_MODE_VISIBLE;

func _ready():
	# Allow selecting console text
	console_text.set_selection_enabled(true)
	# Follow console output (for scrolling)
	console_text.set_scroll_follow(true)
	# Don't allow focusing on the console text itself
	console_text.set_focus_mode(console_text.FOCUS_NONE)

	set_process_input(true);
	console_root.set_visible(false);
	
	# By default we show help
	append_bbcode("[color=red]" + BDatabase.get_value("application/name", "") + "[/color] - [color=yellow]cmdlist[/color] to get a list of all commands\n")

	# Register built-in commands
	register_command("echo", {
		description = "Prints a string in console",
		args = ["string"]
	})
	
	register_command("history", {
		description = "Print all previous cmd used during the session",
		args = []
	})

	register_command("cmdlist", {
		description = "Lists all available commands",
		args = []
	})

	register_command("cvarlist", {
		description = "Lists all available cvars",
		args = []
	})

	register_command("help", {
		description = "Outputs usage instructions",
		args = []
	})

	register_command("quit", {
		description = "Exits the application",
		args = []
	})

	register_command("clear", {
		description = "clear the terminal",
		args = []
	})
	
	# Register built-in cvars
	register_cvar("client_max_fps", {
		description = "The maximal framerate at which the application can run",
		type = "int",
		default_value = 61,
		min_value = 10,
		max_value = 1000
	})
	
	var a_log = BLog;
	a_log.connect("print", self, "_print");

func _print(text, type):
	var colour;
	match type:
		BLog.DEBUG: colour = "pink";
		BLog.PRINT: colour = "aqua";
		BLog.WARN: colour = "yellow";
		BLog.ERROR: colour = "red";
	
	append_bbcode("[color=" + colour + "]" + text + "[/color]\n");
	return;
	
func _input(event):
	if (Engine.is_editor_hint()):
		return;
		
	if event.is_action_pressed("performance"):
		perf_root.toggle_visible();
		
	if event.is_action_pressed("console_toggle"):
		set_console_open(!is_console_open());

	if event.is_action_pressed("console_up"):
		if (cmd_history_up > 0 and cmd_history_up <= cmd_history.size()):
			cmd_history_up-=1
			console_input.set_text(cmd_history[cmd_history_up])

	if event.is_action_pressed("console_down"):
		if (cmd_history_up > -1 and cmd_history_up + 1 < cmd_history.size()):
			cmd_history_up +=1
			console_input.set_text(cmd_history[cmd_history_up])

	# autocomplete
	if is_console_open():
		if console_input.get_text() != "":
			if Input.is_key_pressed(KEY_TAB):
				autocomplete_input();
				
			if Input.is_key_pressed(KEY_ESCAPE):
				clear_input();
		else:
			if Input.is_key_pressed(KEY_ESCAPE):
				set_console_open(false);

	return;


func clear_input():
	console_input.clear();
	return;


func autocomplete_input():
	var text = console_input.get_text()
	var matches = 0
	# If there are no matches found yet, try to complete for a command or cvar
	if matches == 0:
		for command in commands:
			if command.begins_with(text):
				describe_command(command)
				console_input.set_text(command + " ")
				console_input.set_cursor_position(100)
				matches += 1
		for cvar in cvars:
			if cvar.begins_with(text):
				describe_cvar(cvar)
				console_input.set_text(cvar + " ")
				console_input.set_cursor_position(100)
				matches += 1

# This function is called from scripts/console_commands.gd to avoid the
# "Cannot access self without instance." error
func quit():
	get_tree().quit()


func set_console_open(open):
	# Close the console
	if open == false:
		console_root.set_visible(false);
		Input.set_mouse_mode(mouse_mode); # restore mouse mode
		
	# Open the console
	elif open == true:
		console_root.set_visible(true);
		console_input.grab_focus();
		console_input.call_deferred("clear");
		mouse_mode = Input.get_mouse_mode();
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
		
	emit_signal("set_console_open", open);

# use this to determine if the console is open and has focus
func is_console_open():
	return console_root.is_visible()

# Called when the user presses Enter in the console
func _on_LineEdit_text_entered(text):
	# used to manage cmd history
	if cmd_history.size() > 0:
		if (text != cmd_history[cmd_history_count - 1]):
			cmd_history.append(text)
			cmd_history_count+=1
	else:
		cmd_history.append(text)
		cmd_history_count+=1
	cmd_history_up = cmd_history_count
	var text_splitted = text.split(" ", true)
	# Don't do anything if the LineEdit contains only spaces
	if not text.empty() and text_splitted[0]:
		handle_command(text)
	else:
		# Clear the LineEdit but do nothing
		console_input.clear()

# deregister commands that are belonged to a "owner" object
func deregister_commands(owner):
	var removeList = []
	for c in commands:
		var cArgs = commands[c]
		if (cArgs.has("owner") && cArgs["owner"] == owner):
			removeList.append(c)
				
	for c in removeList:
		commands.erase(c)

# Registers a new command
func register_command(name, cmd):
	commands[name] = cmd;
	
func register_variable(name, params):
	variables[name] = params;
	
func deregister_variables(owner):
	var removeList = []
	for v in variables:
		var cArgs = variables[v]
		if (cArgs.has("owner") && cArgs["owner"] == owner):
			removeList.append(v)
				
	for v in removeList:
		variables.erase(v)

# Registers a new cvar (control variable)
func register_cvar(name, cmd):
	cvars[name] = cmd
	cvars[name].value = cvars[name].default_value

func append_bbcode(bbcode):
	if (bbcode == null || bbcode.length() <= 0 || !console_text):
		return
		
	var lines = bbcode.split("\n")
	var linesIdx = 0
	
	if (!last_line_complete):
		line_history[line_history.size() - 1] += lines[0].length() # dont need to account for \n here as its done below
		linesIdx += 1
	
	for l in range(linesIdx, lines.size()):
		var line = lines[l]
		if (line.length()):
			line_history.append(line.length() + 1)	# + 1 to account for \n	
		
	last_line_complete = bbcode.ends_with("\n")
	
	# chop off old lines
	var text = console_text.get_bbcode() + bbcode
	var charsToChop = 0
	while (line_history.size() > MAX_LINES):
		charsToChop += line_history.pop_front()
	
	if (charsToChop > 0):
		text = text.substr(charsToChop, text.length())
	
	console_text.set_bbcode(text)

func get_history_str():
	var strOut = ""
	var count = 0
	for i in cmd_history:
		strOut += "[color=#ffff66]" + str(count) + ".[/color] " + i + "\n"
		count+=1
	
	return strOut

func clear():
	console_text.set_bbcode("")
	line_history = []
	last_line_complete = true

# Describes a command, user by the "cmdlist" command and when the user enters a command name without any arguments (if it requires at least 1 argument)
func describe_command(cmd):
	var command = commands[cmd]
	var description = command.description
	var args = command.args

	if args.size() >= 1:
		append_bbcode("[color=#ffff66]" + cmd + ":[/color] " + description + " [color=#88ffff](usage: " + cmd + " " + PoolStringArray(args).join(" ") + ")[/color]\n")
	else:
		append_bbcode("[color=#ffff66]" + cmd + ":[/color] " + description + " [color=#88ffff](usage: " + cmd + ")[/color]\n")

# Describes a cvar, used by the "cvarlist" command and when the user enters a cvar name without any arguments
func describe_cvar(cvar):
	var cvariable = cvars[cvar]
	var description = cvariable.description
	var type = cvariable.type
	var default_value = cvariable.default_value
	var value = cvariable.value
	if type == "str":
		append_bbcode("[color=#88ff88]" + str(cvar) + ":[/color] [color=#9999ff]\"" + str(value) + "\"[/color]  " + str(description) + " [color=#ff88ff](default: \"" + str(default_value) + "\")[/color]\n")
	else:
		var min_value = cvariable.min_value
		var max_value = cvariable.max_value
		append_bbcode("[color=#88ff88]" + str(cvar) + ":[/color] [color=#9999ff]" + str(value) + "[/color]  " + str(description) + " [color=#ff88ff](" + str(min_value) + ".." + str(max_value) + ", default: " + str(default_value) + ")[/color]\n")

func handle_command(p_text):
	# special case to execute gdscript using /c
	if (p_text.begins_with("/c")):
		var source = p_text.substr(2, p_text.length()); 
		evaluate(source);
		return;
	
	# split incase multiple commands are supplied
	var cmds = p_text.split(";", false);
	for cmd in cmds:
		_handle_command(cmd.strip_edges());
	
	
func _convert_value(p_text, type):
	match type:
		"string": return str(p_text);
		"str": return str(p_text);
		"int": return int(p_text);
		"float": return float(p_text);
		_: continue;
		
	return p_text;
		
# convert args into the proper type
func _process_args(args, argTypesArr):
	var results = [];
	for i in range(0, min(argTypesArr.size(), args.size())):
		var newValue = _convert_value(args[i], argTypesArr[i]);
		results.push_back(newValue);
		
	return results;
	
func _call_func(method, args):
	match args.size():
		0: return method.call_func();
		1: return method.call_func(args[0]);
		2: return method.call_func(args[0], args[1]);
		3: return method.call_func(args[0], args[1], args[2]);
		_: continue;
			
	Util.assert(false, "Console needs to support more args");
	
func _handle_command(text):
	# The current console text, splitted by spaces (for arguments)
	var split = text.split(" ", true);
	var cmdName = split[0];
	split.remove(0);
	var args = split;
	
	# Check if the first word is a valid command
	if commands.has(cmdName):
		var command = commands[cmdName];
		print("> " + text)
		append_bbcode("[b]> " + text + "[/b]\n")
		# If no argument is supplied, then show command description and usage, but only if command has at least 1 argument required
		if args.size() == 0 and not command.args.size() == 0:
			describe_command(cmdName)
		else:
			# Run the command! If there are no arguments, don't pass any to the other script.
			if command.args.size() == 0:
				if (command.has("method")):
					command.method.call_func()
				else:
					call(cmdName.replace(".",""))
			else:
				args = _process_args(args, command.args);
				if (command.has("method")):
					_call_func(command.method, args);
					#command.method.call_func(args)
				else:
					call(cmdName.replace(".",""), args)
					
	# Check if the first word is a valid cvar
	elif cvars.has(cmdName):
		var cvar = cvars[cmdName]
		print("> " + text)
		append_bbcode("[b]> " + text + "[/b]\n")
		# If no argument is supplied, then show cvar description and usage
		if args.size() == 0:
			describe_cvar(cmdName)
		else:
			# Let the cvar change values!
			if cvar.type == "str":
				for word in range(0, args.size()):
					if word == 1:
						cvar.value = str(args[word])
					else:
						cvar.value += str(" " + args[word])
			elif cvar.type == "int":
				cvar.value = int(args[0])
			elif cvar.type == "float":
				cvar.value = float(args[0])

			# Call setter code
			call(cmdName, cvar.value)
	else:
		# Treat unknown commands as unknown
		append_bbcode("[b]> " + text + "[/b]\n")
		append_bbcode("[i][color=#ff8888]Unknown command or cvar: " + cmdName + "[/color][/i]\n")
	console_input.clear()
	
	
# https://godotengine.org/qa/339/does-gdscript-have-method-to-execute-string-code-exec-python
func evaluate(text):
	var script = GDScript.new();
	script.source_code += "extends Node\n"
	
	for key in variables:
		script.source_code += "var " + key + "\n"
		
	script.source_code += "func _eval():\n\t" + text;
	script.reload();

	var obj = Node.new();
	obj.set_script(script);
		
	for key in variables:
		var variable = variables[key];
		obj.set(key, variable.variable);
	
	var ret = obj._eval();
	obj.queue_free();
	return ret;
	
	

# Prints a string in console
static func echo(text):
	# Erase "echo" from the output
	text.erase(0, 5)
	Console.append_bbcode(text + "\n")

# Lists all available commands
static func cmdlist():
	var cmds = Console.commands
	for command in cmds:
		Console.describe_command(command)

static func history():
	Console.append_bbcode(Console.get_history_str())

# Lists all available cvars
static func cvarlist():
	var v_cvars = Console.cvars
	for cvar in v_cvars:
		Console.describe_cvar(cvar)

# Prints some help
static func help():
	var help_text = """Type [color=#ffff66]cmdlist[/color] to get a list of commands.
Type [color=#ffff66]quit[/color] to exit the application."""
	Console.append_bbcode(help_text + "\n")
	
# The maximal framerate at which the application can run
static func client_max_fps(value):
	OS.set_target_fps(int(value))


func _on_LineEdit_text_changed( text ):
	pass # replace with function body

