#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
extends ViewportContainer
enum enum_viewport_type {UI, CAMERA}
export (enum_viewport_type) var viewport_type;


func _ready():
	Database.connect("settings_changed", self, "settings_changed");
	update();
	return;


func settings_changed():
	update();
	return;


func update():
	# change own settings
	if (viewport_type == enum_viewport_type.CAMERA):
		var shrink_value = Database.get_value(Database.STRETCH_SHRINK, 0);
		if (shrink_value == 0):
			stretch_shrink = 5;
		if (shrink_value == 1):
			stretch_shrink = 4;
		if (shrink_value == 2):
			stretch_shrink = 3;
		if (shrink_value == 3):
			stretch_shrink = 2;
		if (shrink_value == 4):
			stretch_shrink = 1;
	
	# loop each child object, and changes its settings
	var children = get_children();
	var shader_quality = Database.get_value(Database.SHADER_QUALITY, 0);
	var shadow_quality = Database.get_value(Database.SHADOW_QUALITY, 0);
	
	var shadow_atlas_size_setting = 0;
	if (shadow_quality == 0):
		shadow_atlas_size_setting = 0;
	if (shadow_quality == 1):
		shadow_atlas_size_setting = 4;
	if (shadow_quality == 2):
		shadow_atlas_size_setting = 8;
	if (shadow_quality == 3):
		shadow_atlas_size_setting = 16;
	if (shadow_quality == 4):
		shadow_atlas_size_setting = 32;

	var msaa_setting = 0;
	if (shader_quality == 0):
		msaa_setting = 0;
	if (shader_quality == 1):
		msaa_setting = 0;
	if (shader_quality == 2):
		msaa_setting = 0;
	if (shader_quality == 3):
		msaa_setting = 2;
	if (shader_quality == 4):
		msaa_setting = 4;
		
	for child in children:
		child.msaa = msaa_setting;
		child.shadow_atlas_size = shadow_atlas_size_setting;
		# debug_draw
		# shadow_atlas_quad_0 > 3
	return;
