extends VBoxContainer

signal changed(p_state);

onready var ui_advanced = BUtil.find_child(self, "Advanced");
onready var ui_advanced_container = BUtil.find_child(self, "AdvancedContainer");
onready var ui_preconfigured = BUtil.find_child(self, "Preconfigured");
onready var ui_loan_limit = BUtil.find_child(self, "LoanLimit");
onready var ui_loan_size = BUtil.find_child(self, "LoanSize");
onready var ui_interest_rate = BUtil.find_child(self, "InterestRate");
onready var ui_company_starting_cash = BUtil.find_child(self, "CompanyStartingCash");
onready var ui_company_shares_enabled = BUtil.find_child(self, "CompanySharesEnabled");
onready var ui_company_merger = BUtil.find_child(self, "CompanyMerger");
onready var ui_time_limit = BUtil.find_child(self, "TimeLimit");
onready var ui_net_worth = BUtil.find_child(self, "NetWorth");
onready var ui_loan_amount = BUtil.find_child(self, "LoanAmount");
onready var ui_resource_demand_supply_rate = BUtil.find_child(self, "ResourceDemandSupplyRate");

var merger_items = [
	{
		"title": "Disabled",
		"value": "disabled"
	},
	{
		"title": "First",
		"value": "first"
	},
	{
		"title": "All",
		"value": "all"
	}
];

var previous_selected_index = 0;
var custom_idx;

func _ready():
	ui_advanced_container.set_visible(false);
	
	for i in range(0, merger_items.size()):
		var item = merger_items[i];
		ui_company_merger.add_item(item.title);
		ui_company_merger.set_item_metadata(i, item);
	
	var db_settings = BDatabase.get_value("game_settings_control");
	for i in range(0, db_settings.size()):
		var item = db_settings[i];
		item.index = i;
		ui_preconfigured.add_item(item.title);
		ui_preconfigured.set_item_metadata(i, item);
		
	custom_idx = db_settings.size();
	ui_preconfigured.add_item("Custom");
	ui_preconfigured.set_item_metadata(custom_idx, { index = custom_idx });
	
	_on_Preconfigured_item_selected(0);


func _on_Preconfigured_item_selected(id):
	var item = ui_preconfigured.get_item_metadata(id);
	
	# if we change to custom, then copy the item we were previously on to pre-populate the custom fields
	if (item.index == custom_idx):
		item = ui_preconfigured.get_item_metadata(previous_selected_index).duplicate();
		item.index = custom_idx;
		ui_preconfigured.set_item_metadata(custom_idx, item);
		
	var advanced_checked: bool = ui_advanced.pressed;
	ui_advanced_container.set_visible(advanced_checked || (item.index == custom_idx));
	update_ui(item);
	emit_signal("changed", item);
	previous_selected_index = id;
	
	
func update_ui(item):
	if (!item):
		return;
		
	ui_preconfigured.select(item.index);
	
	ui_loan_limit.text = str(item.loan_limit);
	ui_loan_size.text = str(item.loan_size);
	ui_interest_rate.text = str(item.interest_rate);
	ui_company_starting_cash.text = str(item.company_starting_cash);
	
	ui_resource_demand_supply_rate.text = str(item.resource_demand_supply_rate);
	
	# block signals else we get forced into custom!
	ui_company_shares_enabled.set_block_signals(true);
	ui_company_shares_enabled.pressed = item.company_shares_enabled;
	ui_company_shares_enabled.set_block_signals(false);
	
	ui_time_limit.text = str(item.win_time_limit);
	ui_net_worth.text = str(item.win_company_net_worth);
	ui_loan_amount.text = str(item.win_company_loan_amount);
	
	for i in range(0, merger_items.size()):
		var mi = merger_items[i];
		if (mi.value == item.win_company_merger):
			ui_company_merger.select(i);
			
	#apply(item);
	
		
		
func get_state():
	return get_current_item();
	
	
func set_state(p_dict):
	update_ui(p_dict);
	
		
#func apply(item):
#	# just insert the whole dictionary into the database
#	#Database.insert(item);
	
	
func _on_Advanced_toggled(button_pressed):
	ui_advanced_container.set_visible(button_pressed);


func get_current_item():
	var item = ui_preconfigured.get_selected_metadata().duplicate();
	return item;
	
	
# set custom item + set the selected to custom + apply
func apply_custom_item(item):
	item.index = custom_idx;
	ui_preconfigured.set_item_metadata(custom_idx, item);
	ui_preconfigured.select(custom_idx);
	#apply(item);
	emit_signal("changed", item);
	
	
func _on_TimeLimit_text_changed(new_text):
	var item = get_current_item();
	item.win_time_limit = int(new_text); # TODO: why isn't this line working?
	apply_custom_item(item);


func _on_LoanLimit_text_changed(new_text):
	var item = get_current_item();
	item.loan_limit = int(new_text);
	apply_custom_item(item);


func _on_LoanSize_text_changed(new_text):
	var item = get_current_item();
	item.loan_size = int(new_text);
	apply_custom_item(item);


func _on_InterestRate_text_changed(new_text):
	var item = get_current_item();
	item.interest_rate = int(new_text);
	apply_custom_item(item);


func _on_CompanyStartingCash_text_changed(new_text):
	var item = get_current_item();
	item.company_starting_cash = int(new_text);
	apply_custom_item(item);


func _on_CompanySharesEnabled_toggled(button_pressed):
	var item = get_current_item();
	item.company_shares_enabled = button_pressed;
	apply_custom_item(item);


func _on_NetWorth_text_changed(new_text):
	var item = get_current_item();
	item.win_company_net_worth = int(new_text);
	apply_custom_item(item);


func _on_LoanAmount_text_changed(new_text):
	var item = get_current_item();
	item.win_company_loan_amount = int(new_text);
	apply_custom_item(item);


func _on_CompanyMerger_item_selected(id):
	var item = get_current_item();
	var metadata = ui_company_merger.get_item_metadata(id);
	item.win_company_merger = metadata.value;
	apply_custom_item(item);


func _on_ResourceDemandSupplyRate_text_changed(new_text):
	var item = get_current_item();
	item.resource_demand_supply_rate = float(new_text);
	apply_custom_item(item);
