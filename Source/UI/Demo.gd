#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var controller; # this is the main root menu

onready var ui_dlg = $Panel;

var timer;
var quit_timer;

func apply_to_controller(controller):
	self.controller = controller;
	controller.connect("quiting", self, "_quiting");
	controller.get_tree().get_root().add_child(self);
	#controller.add_center_panel(ui_dlg);
	ui_dlg.hide();
	return;

func _ready():
	if (BDatabase.is_build_type("demo")):
		timer = Util.create_timer(self, "_expired", 30 * 60, true);
	
	return;
	
func _quiting():
	return;
	
func _expired():
	# help the user quit
	quit_timer = Util.create_timer(self, "_on_QuitButton_pressed", 30, true);
	
	# push to front
	controller.get_tree().get_root().remove_child(self);
	controller.get_tree().get_root().add_child(self);
	ui_dlg.show();

func _on_QuitButton_pressed():
	OS.shell_open("http://bit-shift.io/trains-and-things/");
	controller.quit();
