#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

signal set_visible(vis);

onready var rich_text_label = BUtil.find_first_child_by_class_name(self, "RichTextLabel");

func _init():
	return;
	
func _ready():
	set_visible(false);
	return;
	
# DISABLED FOR NOW, causing debug text on the template buildings
#func _notification(what):
#	if (what == NOTIFICATION_VISIBILITY_CHANGED):
#		var is_vis = get_parent().is_visible_in_tree();
#		set_visible(is_vis);
#
#	if (what ==  NOTIFICATION_ENTER_TREE):
#		var is_vis = get_parent().is_visible_in_tree();
#		set_visible(is_vis);
#
#	return;
		
func get_rich_text_label():
	if (rich_text_label):
		return rich_text_label;
		
	rich_text_label = BUtil.find_first_child_by_class_name(self, "RichTextLabel");
	return rich_text_label;

func set_visible(vis):
	.set_visible(vis);
	get_rich_text_label().set_visible(vis);
	emit_signal("set_visible", vis);
	
func clear():
	get_rich_text_label().clear();
	
func add_text(text):
	get_rich_text_label().append_bbcode(text);
	
func add_line(text):
	add_text(text + "\n");
