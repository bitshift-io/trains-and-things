#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

#const DRAG_MOVE = 0;
#const DRAG_COPY = 1;

#export(int, "Move", "Copy") var drag_type = DRAG_MOVE; # drag copy or self?
#export(bool) var destroy_when_released_on_none_dropable_control = false;

#export(int, "Move", "Copy") var clone_drag_type = DRAG_COPY; # when this is cloned, how should the clones act?
#export(bool) var clone_destroy_when_released_on_none_dropable_control = false; # clone setting

var dragable = true; # can use this to stop the item being dragged

var metadata; # field for user data

signal _drop_finished(); # internal signal

var begin_drag_func;
var can_drop_on_control_func;
var drop_on_control_func;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

# set data being dragged
func get_drag_data(pos):
	if (!dragable):
		return;
		
	# assign a duplicate of ourself to the drag preview
	# when this duplicate is destroyed, we know the drop is finished
	# which helps us detect a failed drop
	
	var drag_data = {};
	drag_data["dragable"] = self;
	drag_data["drop_control"] = null; # control we were dropped on
	drag_data["drag_preview"] = duplicate();
	
#	if (drag_type == DRAG_MOVE):
#		set_visible(false);

	if (begin_drag_func):
		drag_data = begin_drag_func.call_func(drag_data);
	
	if (drag_data["drag_preview"]):
		var drag_preview = drag_data["drag_preview"];
		drag_preview.connect("_drop_finished", self, "_drop_finished");
		set_drag_preview(drag_preview);
		
	return drag_data;
	
func _notification(what):		
	if (what == NOTIFICATION_PREDELETE):
		emit_signal("_drop_finished");
		
func drop_on_control(control):
	if (get_viewport() && get_viewport().gui_get_drag_data()):
		var drag_data = get_viewport().gui_get_drag_data();
		drag_data["drop_control"] = control;

	if (drop_on_control_func):
		drop_on_control_func.call_func(self, control);
		return;
	else:
		add_to_control(control);
#
#	if (get_viewport() && get_viewport().gui_get_drag_data()):
#		var drag_data = get_viewport().gui_get_drag_data();
#		drag_data["drop_status"] = true;
#
#	var inst = self;
#	if (drag_type == DRAG_COPY):
#		inst = duplicate();
#		inst.drag_type = clone_drag_type;
#		inst.destroy_when_released_on_none_dropable_control = clone_destroy_when_released_on_none_dropable_control;
#
#	if (inst.get_parent()):
#		inst.get_parent().remove_child(inst);
#
#	control.add_child(inst);
#	control.move_child(inst, 0);
#	inst.set_visible(true);
#
#	emit_signal("drop_on_control", self, control);
		
func add_to_control(control):
	if (!control):
		return;
		
	if (get_parent()):
		get_parent().remove_child(self);

	control.add_child(self);
	#control.move_child(self, 0);
		
func _drop_finished():
	# dropped this item on a non dropable control!
	if (get_viewport() && get_viewport().gui_get_drag_data()):
		var drag_data = get_viewport().gui_get_drag_data();
		if (drag_data["drop_control"] == null):
			drop_on_control(null);
		
#
#
#	if (destroy_when_released_on_none_dropable_control && drag_data["drop_status"] == false):
#		queue_free();
#		return;
#
#	if (drag_type == DRAG_MOVE):
#		set_visible(true);
#
#	emit_signal("drop_finished", self);
		
func can_drop_on_control(control):
	var can_drop = true;
	if (can_drop_on_control_func):
		can_drop = can_drop_on_control_func.call_func(self, control);

	return can_drop;
	
func duplicate():
	var dup = .duplicate();
	dup.metadata = metadata;
	dup.can_drop_on_control_func = can_drop_on_control_func;
	dup.drop_on_control_func = drop_on_control_func;
	return dup;
		
		
		
