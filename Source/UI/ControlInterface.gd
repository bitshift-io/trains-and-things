#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

#export(bool) var movable = true
export(bool) var visibleOnLoad = false

#var mouseOffset = Vector2(0,0)
#var pressed = false
var mouseOver = false

func _ready():
	set_process_input(true)
	
	set_visible(visibleOnLoad)
	screenResized()
	get_tree().connect("screen_resized", self, "screenResized")

func getKey():
	return "control_interface/%s/%s/window_pos" % [get_name(), windowSizeStr()]

func windowSizeStr():
	return "%dx%d" % [get_viewport().get_size().x, get_viewport().get_size().y]

func bringToFront():
	# remove all children and put our self as the first child
	# then add all the other children back
	var p = get_parent()
	if (p.get_children().back() == self):
		return

	p.remove_child(self)
	p.add_child(self)
	
	
# input function
func _input(event):
	# mouse button && left mouse button && within dialog rectangle
	if (mouseOver):# && event.is_action_pressed("left_click")): # and get_rect().has_point(event.pos)):
		if (event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
			bringToFront()
			#pressed = event.pressed
			#mouseOffset = event.get_position() - get_position()
	
	# if pressed
	#if event is InputEventMouseMotion:
	#	if pressed:
	#		set_position(event.get_position() - mouseOffset)
			
# closest thing we have to a destructor
func _exit_tree():
	#BDatabase.set_setting(getKey(), get_position())
	return;
	
# next best thing to a destructor
func _notification(what):
	if (what == NOTIFICATION_MOUSE_ENTER):
		mouseOver = true;
	if (what == NOTIFICATION_MOUSE_EXIT):
		mouseOver = false;

#	if (what == NOTIFICATION_PREDELETE):
#		mainMenu.config.set_value("Player", "%s/WindowPos" % get_name(), get_pos())
#	pass

func screenResized():
	#var pos = BDatabase.get_value(getKey(), null);
	#if (pos != null):
	#	set_position(pos);
	return;
	
