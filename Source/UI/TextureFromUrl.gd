extends TextureRect


func load_from_url(p_url, p_filename):
	var cache_path = "res://.cache/" + p_filename;
	var f = File.new();
	var file_exists = f.file_exists(cache_path);
	if (file_exists):
		load_texture(cache_path);
		return;
	
	var request = HTTPRequest.new();
	request.download_file = cache_path;
	
	add_child(request);
	
	request.connect("request_completed", self, "on_request_completed", [cache_path]);
	request.request(p_url);
	
	
func on_request_completed(result, response_code, headers, body, cache_path):
	load_texture(cache_path);
	
func load_texture(path):
	#return;
	var image = Image.new();
	var ok = image.load(path);
	
	var img_texture = ImageTexture.new();
	var ok2 = img_texture.create_from_image(image);
	
	apply_texture(img_texture); #call_deferred("apply_texture", img_texture);
	
	
func apply_texture(img_texture):
	set_texture(img_texture);
