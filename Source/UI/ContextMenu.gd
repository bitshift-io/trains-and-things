#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends PopupMenu

# TODO: make this a radial/circular popup menu

var target;
var method;

#var id_func_map = {};

func _ready():
	connect("id_pressed", self, "_id_pressed");
	return;
	
func _id_pressed(id):
#	var con = id_func_map[id];
#	var target = con["target"];
#	var method = con["method"];		
	if (target):
		target.call(method, self, id);
		
	return;
	
func clear():
#	id_func_map = {};
	target = null;
	method = null;
	.clear();
	
	
#func add_check_item_connect(p_label: String, p_target: Node, p_target_function_name: String):
#	.add_check_item(p_label);
#	var idx = get_item_id(get_item_count());
#	id_func_map[idx] = {
#		"target": p_target,
#		"method": p_target_function_name
#	};
#
#func add_item_connect(p_label: String, p_target: Node, p_target_function_name: String):
#	.add_item(p_label);
#	var idx = get_item_id(get_item_count());
#	id_func_map[idx] = {
#		"target": p_target,
#		"method": p_target_function_name
#	};
	
	
func popup(p_rect = Rect2()):
	var position = get_viewport().get_mouse_position();
	set_position(position);
	set_as_minsize();
	.popup(p_rect);
