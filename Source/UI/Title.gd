#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

tool

extends Node

export(String) var title;
export(Texture) var texture;

signal icon_pressed();

#func _set(property, value):
#	_update();
	
func _ready():
	_update();
	
func _update():
	BUtil.find_child(self, "Label").text = title;
	BUtil.find_child(self, "TextureButton").texture_normal = texture;
	pass

func _on_TextureButton_pressed():
	emit_signal("icon_pressed");
	pass # Replace with function body.
