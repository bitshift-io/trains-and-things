#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

onready var debug_root = self;

onready var option_debug_draw = BUtil.find_child(self,"DebugDraw")
onready var lod_check = BUtil.find_child(self,"LODCheck")
onready var pause_check = BUtil.find_child(self,"PauseCheck")
onready var hud_visible_check = BUtil.find_child(self,"HUDVisibleCheck")
onready var camera_option = BUtil.find_child(self,"CameraOption")
onready var near_dof = BUtil.find_child(self,"NearDof")
onready var far_dof = BUtil.find_child(self,"FarDof")
onready var dof_amount = BUtil.find_child(self,"DofAmount")

var capture = null;
var capture_3d_frame = 0;

const lod_key = "bitshift/lod/enabled";

func _ready():
	set_process(true);
	Util.populate_option_button(option_debug_draw,["Disabled","Unshaded","Overdraw","Wireframe"]);
	
	# disable focus mode
	for c in BUtil.get_branches_as_array([debug_root]):
		if (c.is_class("Control")):
			c.focus_mode = FOCUS_NONE;
	return;

func _process(d):
	if (capture):
		var world = get_node("/root/World");
		var globals = get_node("/root/World/MapCore");
		
		# first frame with LOD disabled, grab the static capture
		if (capture_3d_frame == 0):
			capture.add_static([world], [globals]);
		
		# just export the dynamic stuff
		capture.add_frame([globals], [], d);
		
		capture_3d_frame += 1;
		
	return;


func _get_master_players():
	var worlds = BUtil.find_children_by_class_name(get_tree().get_root(), "World");
	var mcs = [];
	for world in worlds:
		mcs.append(world.master_player);
		
	return mcs;

func _set_visible(value):
	debug_root.set_visible(value)
	
	var lod_enabled = BDatabase.get_value(lod_key, false);
	lod_check.pressed = lod_enabled;
	pause_check.pressed = get_tree().is_paused();
	
	# incase we are in free camera mode!
	if (value):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
	
	var controller = _get_master_players()[0];
	if (controller):
		hud_visible_check.pressed = controller.is_hud_visible();
		
	camera_option.clear();
	var cameras = get_tree().get_nodes_in_group("cameras");
	var idx = 0;
	for camera in cameras:
		camera_option.add_item(camera.get_name());
		if (camera.is_current()):
			camera_option.select(idx);
			
		idx += 1;
	
	# get_viewport().set_debug_draw(Viewport.DEBUG_DRAW_DISABLED);


func _on_COLLISION_toggled( pressed ):
	get_tree().set_debug_collisions_hint(pressed)

func _on_DebugDraw_item_selected( ID ):
	get_viewport().set_debug_draw(ID);


func _on_LODCheck_toggled(button_pressed):
	BDatabase.set_setting(lod_key, button_pressed);

func _on_PauseCheck_toggled(button_pressed):
	var pause = !get_tree().is_paused();
	get_tree().set_pause(pause);

func _on_HUDVisibleCheck_toggled(button_pressed):
	var controller = _get_master_players()[0];
	if (controller):
		controller.hud.set_visible(!button_pressed);
	
	return

func _on_CameraOption_item_selected(ID):
	var camera_name = camera_option.get_item_text(ID);
	var cameras = get_tree().get_nodes_in_group("cameras");
	for camera in cameras:
		if (camera.get_name() == camera_name):
			camera.make_current();
			_set_visible(false); # close as freecamera hides the mouse
			return;
			
	return


func _on_DebugText_toggled(button_pressed):
	_get_master_players()[0].set_debug_text_visible(button_pressed);
	pass # Replace with function body.


func _on_dof_value_changed(value):
	_dof_changed();
	
func _on_dof_toggled(button_pressed):
	_dof_changed();
	
func _dof_changed():
	var world_environments = BUtil.find_children_by_class_name(get_tree().get_root(), "WorldEnvironment");
	for we in world_environments:
		var env = we.camera_effects;
		if (!env):
			we.camera_effects = CameraEffects.new();
			env = we.camera_effects;
			
		var near = get_dof_values(near_dof);
		var far = get_dof_values(far_dof);
		
		env.dof_blur_near_enabled = near.enabled;
		env.dof_blur_near_distance = near.distance;
		env.dof_blur_near_transition = near.transition;
		
		env.dof_blur_far_enabled = far.enabled;
		env.dof_blur_far_distance = far.distance;
		env.dof_blur_far_transition = far.transition;
		
		env.dof_blur_amount = dof_amount.value;
		# env.dof_blur_quality = CameraEffects.DOF_BLUR_QUALITY_HIGH; # moved to godot project settings
		
	return;
	
func get_dof_values(container):
	return {
		"enabled": BUtil.find_child(container, "Enabled").is_pressed(),
		"transition": BUtil.find_child(container, "Transition").value,
		"distance": BUtil.find_child(container, "Distance").value,
	};
	


