#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

func _ready():
	return;

# checks if we can recive the dropped data
func can_drop_data(pos, data): 
	var dragable = data["dragable"];
	return dragable.can_drop_on_control(self);

# triggers on target drop
func drop_data(pos, data):
	var dragable = data["dragable"];
	dragable.drop_on_control(self);


