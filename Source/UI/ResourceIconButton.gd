#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
extends Control

enum DisplayType {
	SUPPLY,
	DEMAND
}

var resource_data;
var amount = 0;
var amount_delta = 0; # this is the change in the avilable that will occur due to resources being loaded/unloaded
var bottom_display_type = DisplayType.SUPPLY; # what does the bottom value display? the demand met or supply generated?

var supply_dict = {}; # map where amounts are coming from

signal pressed();

func _ready():
	set_amount_delta(amount_delta);
	return;


func _set_resource_data(rd):
	resource_data = rd;
	
	# this is used as a button in various dialogs
	if (!$"Button".is_connected("pressed", self, "_on_button_pressed")):
		$"Button".connect("pressed", self, "_on_button_pressed");
	
	if (resource_data && resource_data.get_market_resource()):
		$"Button".set_normal_texture(resource_data.get_market_resource().get_icon());
		resource_data.connect("amount_changed", self, "_on_amount_changed");
		_on_amount_changed();
		
	return;

# disconnect this button so the amount field does not update with the resource_data
# used for train carriage representations
func disconnect_from_resource_data():
	resource_data.disconnect("amount_changed", self, "_on_amount_changed");
	
# setup the UI to act as an icon
func init_as_icon(rd, p_bottom_display_type):
	bottom_display_type = p_bottom_display_type;
	_set_resource_data(rd);
	$"Button".mouse_filter = Control.MOUSE_FILTER_IGNORE;
	return;


func init_as_button(rd, p_bottom_display_type):
	bottom_display_type = p_bottom_display_type;
	_set_resource_data(rd);
	if (resource_data && resource_data.get_market_resource()):
		$"Button".set_tooltip(resource_data.get_market_resource().name);
	return;


func _on_button_pressed():
	emit_signal("pressed");


func set_amount_visible(vis):
	$"BottomLabel".set_visible(vis);


func set_amount(p_amount):
	amount = p_amount;
	$"BottomLabel".set_text("%d" % p_amount);
	
	
func _on_amount_changed():
	var amt = resource_data.get_export_amount() if (bottom_display_type == DisplayType.SUPPLY) else resource_data.get_import_amount();
	set_amount(amt);


func set_amount_delta(delta):
	amount_delta = delta;
	if (delta > 0):
		$"TopLabel".set_text("+%d" % delta);
	if (delta < 0):
		$"TopLabel".set_text("%d" % delta);
		
	$"TopLabel".set_visible(amount_delta != 0);
	return;


func duplicate(p_flags = DUPLICATE_GROUPS | DUPLICATE_SIGNALS | DUPLICATE_SCRIPTS):
	var clone = .duplicate(p_flags);
	clone._set_resource_data(resource_data);
	clone.get_node("Button").mouse_filter = $"Button".mouse_filter;
	clone.get_node("Button").set_tooltip($"Button".get_tooltip());
	clone.set_amount_delta(amount_delta);
	clone.bottom_display_type = bottom_display_type;
	return clone;

# remove 1 product which comes from p_origin (which is a resource_node_info)
func add_1_resource(p_amount, p_amount_delta, p_origin):
	Util.assert(p_amount == 0 || p_amount == 1, "Out of bounds");
	Util.assert(p_amount_delta == 0 || p_amount_delta == 1, "Out of bounds");
	set_amount(amount + p_amount);
	set_amount_delta(amount_delta + p_amount_delta);
	Util.increment_count_in_dictionary(supply_dict, p_origin, 1, true);
	
# remove 1 product and return where the product came from
# if supply station is supplied, ensure we remove that product - if there is no product for the given supply station return null
func remove_1_resource(p_amount, p_amount_delta, p_supply_station = null):
	Util.assert(p_amount == 0 || p_amount == 1, "Out of bounds");
	Util.assert(p_amount_delta == 0 || p_amount_delta == 1, "Out of bounds");
	if (p_supply_station):
		if (!has_resource_from_station(p_supply_station)):
			return null;
		
	set_amount(amount - p_amount);
	set_amount_delta(amount_delta - p_amount_delta);
	
	var origin = get_first_supply_station();
	Util.increment_count_in_dictionary(supply_dict, origin, -1, true);
	return origin;
	
func get_first_supply_station():
	if (supply_dict.keys().size() <= 0):
		return null;
		
	return supply_dict.keys()[0];
	
func has_resource_from_station(p_origin):
	return supply_dict.has(p_origin);
	
