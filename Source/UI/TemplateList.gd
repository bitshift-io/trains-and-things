#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# We so often have a list which a template child, so here is a class to help deal with it!

extends BoxContainer

onready var ui_template = get_child(0);


func _ready():
	BUtil.reparent(ui_template, null); # remove from parent


func create_item():
	var ui_item = ui_template.duplicate();
	add_child(ui_item);
	return ui_item;
	

func clear():
	BUtil.queue_delete_children(self);
	
	
func get_last_child():
	if (get_child_count() == 0):
		return null;
		
	return get_child(get_child_count() - 1);
