#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends CanvasItem

# get children and only allow one to be active at once

var childConnectionMap = {}

var disabled = false;

func _ready():
	var children = get_children()
	for c in children:
		configure_child(c);
		
	return;
		
		
func add_child(child, p_legible_unique_name = false):
	if (disabled):
		child.set_disabled(true);
		
	configure_child(child);
	.add_child(child, p_legible_unique_name);
	return;
	
	
func configure_child(c):
	# don't call configure twice on the same child!
	if (childConnectionMap.has(c)):
		return;
		
	var connections = c.get_signal_connection_list("toggled");
	childConnectionMap[c] = connections
	
	# disconnect the existing signals
	for con in connections:
		var target = con["target"]
		var method = con["method"]
		c.disconnect("toggled", target, method)
		
	c.connect("toggled", self, "_child_toggled", [c]);
	return;

	
func _child_toggled(pressed, button):
	# disable other buttons
	var button_path = button.get_path() if button else null;
	var children = get_children();
	for c in children:
		if (c != button && c.is_pressed()):
			var c_path = c.get_path();
			#BLog.debug("Unpressing button and executing signals for: " + c_path);
			
			c.set_block_signals(true);
			c.set_pressed(false);
			c.set_block_signals(false);

			# call connections for this button first
			for con in childConnectionMap[c]:
				var target = con["target"];
				var method = con["method"];
				target.call(method, false);
			
	if (!button):
		return;
			
	#BLog.debug("Executing signals for pressed button: " + button_path);
	
	# call connections for the pressed button
	# after
	for con in childConnectionMap[button]:
		var target = con["target"];
		var method = con["method"];
		target.call(method, pressed);
		
		
# set_pressed false on all buttons
func clear_toggled():
	_child_toggled(false, null);

				
func set_disabled(p_disabled):
	disabled = p_disabled;
	var children = get_children()
	for c in children:
		c.set_disabled(disabled);
	
