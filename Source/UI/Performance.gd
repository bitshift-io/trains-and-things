#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

# http://docs.godotengine.org/en/stable/classes/class_performance.html
onready var perf_root = self;
onready var curFPS = BUtil.find_child(self,"CUR_FPS");
onready var drawCalls = BUtil.find_child(self,"DRAW_CALLS");
onready var objectsFrame = BUtil.find_child(self,"OBJ_FRAME");
onready var vertsFrame = BUtil.find_child(self,"VERT_FRAME");

func _ready():
	set_visible(false);
	set_process(false);
	return;


func _process(d):
	curFPS.set_text(str(Performance.get_monitor(Performance.TIME_FPS)))
	drawCalls.set_text(str(Performance.get_monitor(Performance.RENDER_DRAW_CALLS_IN_FRAME)))
	objectsFrame.set_text(str(Performance.get_monitor(Performance.RENDER_OBJECTS_IN_FRAME)))
	vertsFrame.set_text(str(Performance.get_monitor(Performance.RENDER_VERTICES_IN_FRAME)))
	return;


func toggle_visible():
	set_process(!is_visible());
	perf_root.set_visible(!is_visible());
	return;
