#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Control

export(Vector3) var offset = Vector3(0, 0, 0);

onready var spatial = BUtil.find_parent_by_class_name(self, 'Spatial'); # the node to follow

func _ready():
	set_process(visible && !!spatial);

func _process(delta):
	var result = Util.unproject_camera_position(spatial.get_viewport(), spatial.get_global_transform().origin + offset);
	if (!result):
		.set_visible(false);
		return;

	.set_visible(result.visible);
	set_position(result.position);
	
func set_visible(p_visible):
	.set_visible(p_visible);
	set_process(p_visible);
