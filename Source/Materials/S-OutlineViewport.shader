shader_type canvas_item;

uniform float width: hint_range(0.0, 10.0);
//uniform vec4 color: hint_color;

void fragment() {
	vec4 c = texture(TEXTURE, SCREEN_UV, 0.0);
	COLOR = vec4(0, 0, 0, 0);
	
	// for now just check the red channel, we were checking alpha, but this doesn't look to be working in the
	// Vulkan branch
	if (c.r == 0.0) {
		vec2 side_width = width*SCREEN_PIXEL_SIZE;
		bool is_done = false;
	
		for (float x = -side_width.x; x <= side_width.x && !is_done; x += SCREEN_PIXEL_SIZE.x) {
			for (float y = -side_width.y; y <= side_width.y && !is_done; y += SCREEN_PIXEL_SIZE.y) {
				vec4 nc = texture(TEXTURE, SCREEN_UV + vec2(x, -y), 0.0);
				if ( nc.r != 0.0) {
					COLOR = nc;
					is_done = true;
				}
			}
		}
	}
	
	//COLOR = vec4(1, 0, 0, 0.5);
}