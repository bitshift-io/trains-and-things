#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

var timer;

export(NodePath) var window_path;
onready var window = get_node(window_path);
var hud;

onready var world = WorldMgr.get_world(self); 

func _ready():
	return;


func _post_ready():
	timer = Util.create_timer(self, "start_scenario", 2, true, true);
	return;


func start_scenario():
	# move self into hud
	hud = world.master_player.hud;
	hud.add_dialog_panel(self);
		
	window.show();
	var first_container = BUtil.find_first_child_by_class_name(self, "ScenarioContainer");
	first_container.activate(null);
	return;


func end_scenario():
	window.hide();
	return;


func parse_variables(string):
	string = string.replace("$(name)", Database.get_value(Database.PLAYER_NAME, "Recruit")); #Game.get_player_name());
	
	# iterate over all possible inputs to parse them
	#var var_exp = RegEx.new();
	#var_exp.compile("\\$\\(.*?\\)");
	#var res = var_exp.search(string)
	#while res != null:
	#    print(res.get_string(0))
	#    res = var_exp.search(text, res.get_end(0))
	
	var actions = InputMap.get_actions();
	for action_name in actions:
		var action_list = InputMap.get_action_list(action_name);
		if (action_list.size() <= 0):
			continue;
			
		var action = action_list[0];
		if (action is InputEventKey):
			var scancode = action["scancode"];
			var key_name = OS.get_scancode_string(scancode);
			var variable_name = "$(" + action_name + ")";
			var parsed_value = "[color=red]" + key_name + "[/color]";
			string = string.replace(variable_name, parsed_value);

	return string;
