#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "Scenario.gd"

export(NodePath) var resource_node_1_path;
export(NodePath) var resource_node_2_path;
export(NodePath) var city_node_path;

export(String) var resource_name_1;
export(String) var resource_name_2;

var resource_node_1;
var resource_node_2;
var city_node;


func _ready():
	$"Panel/TakeOutLoan".connect("notify_event", self, "_TakeOutLoan_NotifyEvent");
	$"Panel/StartBuildTrainStation".connect("notify_event", self, "_StartBuildTrainStation_NotifyEvent");
	$"Panel/BuildTrainStation".connect("notify_event", self, "_BuildTrainStation_NotifyEvent");
	$"Panel/BuildTrainStation_Fail".connect("notify_event", self, "_BuildTrainStation_NotifyEvent");
	$"Panel/FinishBuildTrainStation".connect("notify_event", self, "_FinishBuildTrainStation_NotifyEvent");
	
	$"Panel/BuildTrainStation2".connect("notify_event", self, "_BuildTrainStation2_NotifyEvent");
	$"Panel/BuildTrainStation2_Fail".connect("notify_event", self, "_BuildTrainStation2_NotifyEvent");
	
	$"Panel/StartBuildTrack".connect("notify_event", self, "_StartBuildTrack_NotifyEvent");
	$"Panel/BuildTrack".connect("notify_event", self, "_BuildTrack_NotifyEvent");
	
	$"Panel/StartBuildTrain".connect("notify_event", self, "_StartBuildTrain_NotifyEvent");
	$"Panel/FinishBuildTrain".connect("notify_event", self, "_FinishBuildTrain_NotifyEvent");
	
	$"Panel/FinalObjective".connect("notify_event", self, "_FinalObjective_NotifyEvent");
	return;
	
	
func parse_variables(string):
	# this is called before onready as children onready is called prior to this 
	if (!resource_node_1):
		resource_node_1 = get_node(resource_node_1_path);
		resource_node_2 = get_node(resource_node_2_path);
		city_node = get_node(city_node_path);

	string = .parse_variables(string);
	string = string.replace("$(resource_node_1.name)", resource_node_1.get_display_name());
	string = string.replace("$(resource_node_2.name)", resource_node_2.get_display_name());
	string = string.replace("$(city_node.name)", city_node.get_display_name());
	
	string = string.replace("$(resource_name_1)", resource_name_1);
	string = string.replace("$(resource_name_2)", resource_name_2);
	return string;
	
	
func _TakeOutLoan_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.BUTTON_PRESSED and p_params.button.get_name() == "LoanIncreaseBtn"):
		p_params.scenario_container.activate_next_container();
		
		
func _StartBuildTrainStation_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.PLAYER_STATE_CHANGED and p_params.to_state.get_name() == "BuildTrainStationState"):
		p_params.scenario_container.activate_next_container();
		
		
func _BuildTrainStation_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.BUILDING_LOCATION_SELECTED):
		var closest_resource_node = p_params.resource_node;
		if (closest_resource_node.name != resource_node_1.name): #"Hilo Forestry Company"):
			p_params.sender.location_selected = false;
			p_params.scenario_container.activate_container($"Panel/BuildTrainStation_Fail");
		else:
			p_params.sender.cancelButton.set_disabled(true); # stop the user cancelling this dialog!
			p_params.scenario_container.activate_next_container();
			
			
func _FinishBuildTrainStation_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.BUILDING_CONSTRUCTED):
		p_params.scenario_container.activate_next_container();
		
		
func _BuildTrainStation2_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.BUILDING_LOCATION_SELECTED):
		var closest_resource_node = p_params.resource_node;
		if (closest_resource_node.name != resource_node_2.name): #"Kau Mills"):
			p_params.sender.location_selected = false;
			p_params.scenario_container.activate_container($"Panel/BuildTrainStation2_Fail");
		else:
			p_params.sender.cancelButton.set_disabled(true); # stop the user cancelling this dialog!
			
	if (event_id == EventMgr.Id.BUILDING_CONSTRUCTED):
		p_params.scenario_container.activate_next_container();
			
			
func _StartBuildTrack_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.PLAYER_STATE_CHANGED and p_params.to_state.get_name() == "RailEditorState"):
		p_params.scenario_container.activate_next_container();
		
		
func _BuildTrack_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.TRACK_CONSTRUCTION_FINISHED):
		var startConnectionPath = p_params.start_connection; #args[0];
		var endConnectionPath = p_params.end_connection; #args[1];
		
		# a two way route exists
		var station_1 = resource_node_1.trainStations[0] if (resource_node_1.trainStations.size()) else null;
		var station_2 = resource_node_2.trainStations[0] if (resource_node_2.trainStations.size()) else null;
		if (station_1 and station_2 and station_1.find_astar_paths(station_2).size() and station_2.find_astar_paths(station_1).size()):
			p_params.scenario_container.activate_next_container();
	return;
	
	
func _StartBuildTrain_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.PLAYER_STATE_CHANGED and p_params.to_state.get_name() == "TrainEditorState"):
		p_params.scenario_container.activate_next_container();
			
				
func _FinishBuildTrain_NotifyEvent(p_params): #event_id, sender, args, container):
	var event_id = p_params.id;
	if (event_id == EventMgr.Id.VEHICLE_CONSTRUCTED): # TODO: verify the train cargo, src and dest
		p_params.scenario_container.activate_next_container();
		
		
func _FinalObjective_NotifyEvent(p_params): #event_id, sender, args, container):
	# a two way route exists between resource_node_2 and city_node_1
	var event_id = p_params.id;
	var station_1 = city_node.trainStations[0] if (city_node.trainStations.size()) else null;
	var station_2 = resource_node_2.trainStations[0] if (resource_node_2.trainStations.size()) else null;
	if (station_1 and station_2 and station_1.find_astar_paths(station_2).size() and station_2.find_astar_paths(station_1).size()):
		if (event_id == EventMgr.Id.VEHICLE_CONSTRUCTED): # TODO: verify the train cargo, src and dest
			p_params.scenario_container.activate_next_container();
	
