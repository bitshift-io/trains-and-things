#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Container

export(bool) var skip_btn_visible = false;
export(bool) var next_btn_visible = false;
export(NodePath) var next_container;
export(Texture) var texture;
export(String, MULTILINE) var text = "";

var scenario;

signal notify_event(p_params); #event_id, sender, args, container);
signal on_activate();
signal on_deactivate();

func _ready():
	set_visible(false);
	
	# test for debugging
	#if self is get_parent():
	#	return;
		
	scenario = BUtil.find_parent_by_class_name(self, "Scenario");
		
	if (!skip_btn_visible):
		find_node("SkipButton", true).set_visible(false);
		
	if (!next_btn_visible):
		find_node("NextButton", true).set_visible(false);
		
	if (texture):
		BUtil.find_first_child_by_class_name(self, "TextureRect", true).set_texture(texture);
		
	var rich_text_label = BUtil.find_first_child_by_class_name(self, "RichTextLabel", true);
	rich_text_label.set_bbcode(scenario.parse_variables(text));
	
func activate(prev):
	EventMgr.connect("notify_event", self, "notify_event");
	set_visible(true);
	emit_signal("on_activate");
	return;
	
func deactivate(next):
	EventMgr.disconnect("notify_event", self, "notify_event");
	set_visible(false);
	emit_signal("on_deactivate");
	return;
	
func notify_event(p_params): #event_id, sender, args):
	var params = p_params.duplicate();
	params.scenario_container = self;
	emit_signal("notify_event", params); #event_id, sender, args, self);
	return;
	
func get_active_container():
	var next_idx = 0;
	for i in range(0, get_parent().get_children().size()):
		var c = get_parent().get_child(i);
		if (c.is_visible()):
			return c;
		
# activate a container by name - assumes this is the active container
func activate_container(container):
	self.deactivate(container);
	container.activate(self);
		
# just the next in the sequence
func activate_next_container():
	var next = get_node(next_container);
	if (next and next != self):
		activate_container(next);
		return;
		
	var next_idx = get_index() + 1;
	if (next_idx >= get_parent().get_children().size()):
		scenario.end_scenario();
		return;
		
	var c = get_parent().get_child(next_idx);
	activate_container(c);
	return;

func _on_SkipButton_pressed():
	scenario.end_scenario();
	return;
	
func _on_NextButton_pressed():
	activate_next_container();
	return;
