#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends "TrainStation.gd"

func _ready():
	resourceNode = BUtil.find_parent_by_class_name(get_parent(), "ResourceNode")
	if (resourceNode):
		resourceNode.addAirport(self)

func getRunwayNode():
	return get_node("Spatial/Structure-AirportRunway-A");
