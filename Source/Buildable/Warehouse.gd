#
# Copyright (C) 2017 bitshift
# http://bit-shift.io 
# 
# View LICENSE at:
# https://github.com/bit-shift-io/trains-and-things/blob/master/LICENSE.md
#

#
# A warehouse is a player buildable resource node that allows a company to stockpile goods for later distribution
# 

extends "../Resource/ResourceNode.gd"

onready var finances = $Finances;

var company;
var is_building_template = false;

func _ready():
	if (!world):
		return;
		
	if (is_building_template):
		Util.call_deferred("remove_from_group_and_all_children", self, "persist");
		remove_from_group("resource_nodes");
	else:
		add_to_group("warehouses");
		add_to_group("persist");
		
		if ($SelectEffect && !is_building_template):
			$SelectEffect.prepare(find_node("Spatial"), global_transform);
			
		if (get_tree().is_network_server()):
			world.date_time.connect("month_changed", self, "_on_month_changed");
			
	return;


# called when first constructed to setup any data required
# we need to pass in information on who constructing the object
# so we can generate a network id
func spawn(p_controller, p_parent, p_company, p_name, p_transform):
	company = p_company;
	set_transform(p_transform);
	set_name(p_name);
	
	display_name = p_company.get_display_name() + " " if p_company && p_company.get_display_name().length() else "Company ";
	display_name += "Warehouse";
	
	p_parent.add_child(self); # this calls _ready

	finances = $Finances;
	if (!is_building_template):
		finances.add_expense(finances.get_purchase_cost(), true);
		
	if (!is_building_template):
		EventMgr.listen([EventMgr.Id.VEHICLE_CONSTRUCTED, EventMgr.Id.VEHICLE_DELETED, EventMgr.Id.VEHICLE_EDITED], self, "_on_vehicle_constructed_deleted_edited");
		
	return;


func get_finances():
	return finances;
	
	
func _on_month_changed(monthIndex):
	if (company):
		var expense = world.date_time.yearlyToMonthly(finances.get_maintenance_cost());
		company.pay_cash(expense, IncomeStatement.WAREHOUSES);
		finances.add_expense(expense, true);


# this is called if we want to configure the prop as a template
# to allow us to deferentiate what is being built vs a built structure
func set_as_building_template():
	is_building_template = true;
	return;
	
	
# determine if this building can be built at its current location
func can_build():
	var world = get_tree().get_root().get_world();
	Util.assert(world, "world invalid");
	var space_state = world.get_direct_space_state();
	Util.assert(space_state, "space_state invalid");
	
	var params = PhysicsShapeQueryParameters.new();
	params.set_shape(get_clearance_collision_shape().get_shape());
	params.set_transform(transform);
	params.exclude = BUtil.get_terrain().get_collision_nodes() + get_collision_nodes();  # ignore terrain + any child collision nodes
	var results = space_state.intersect_shape(params, 1);
	return false if (results.size() > 0) else true;
	
	
func get_collision_nodes():
	var col_children = BUtil.find_children_by_class_name(self, "CollisionObject");
	return col_children;
	
	
# this shape is used to check the space around the building is free to be able to build
func get_clearance_collision_shape():
	return $"ClearanceCollisionShape";
	
	
func is_usable(p_controller):
	# can only pick our own warehouses
	if (p_controller.get_company() != company):
		return false;
		
	return .is_usable(p_controller);
		
	
func _on_vehicle_constructed_deleted_edited(p_params):
	update_resources(true);
	
	
# update the resources that this warehouse needs to display
func update_resources(p_generate_ui):
	var resources = {};
	
	# go through all vehicles and see if we are used by any of them
	# build a list of resources we need to support
	var vehicles = get_tree().get_nodes_in_group("vehicles");
	for v in vehicles:
		if (v.is_visible() && !v.is_queued_for_deletion() && v.company == company):
			var route = v.get_vehicle_route();
			for i in range(0, route.size()):
				var rn = route.get_resource_node(i);
				if (rn == self):
					var load_resources = route.get_load_resources(i);
					var unload_resources = route.get_unload_resources(i);
					
					resources = BUtil.merge_dict(resources, load_resources);
					resources = BUtil.merge_dict(resources, unload_resources);
					
	# now re-create the UI...
	var existing_resource_datas = {};
	resource_data_list.clear();
	
	# free old ResourceData and old ResourceRecipe's
	var old = BUtil.find_children_by_class_name(self, "ResourceData");
	for rd in old:
		# if this station is holding on to some stock of something, it needs to keep the supply and demand of these products
		if (rd.import_amount > 0 || rd.export_amount > 0):
			existing_resource_datas[rd.get_name()] = rd;
			continue;

		rd.name += "_QUEUE_FREE"; # avoid name collision below when we create new version
		rd.queue_free();
		
	old = BUtil.find_children_by_class_name(self, "ResourceRecipe");
	for rr in old:
		rr.name += "_QUEUE_FREE"; # avoid name collision below when we create new version
		rr.queue_free();
		
	
	var level = xp.get_level();
	
	# new/edit ResourceData and ResourceRecipe's
	var resource_names = resources.keys();
	for resource_name in resource_names:
		if (existing_resource_datas.has(resource_name)):
			var rd = existing_resource_datas[resource_name];
			var n = rd.name;
			rd.storageLimit = 10 * level; # modfy data based on level
		else:
			var rd = _create_resource_data_from_dict({
				"name": resource_name,
				"is_supplied": true,
				"is_demanded": true,
				"consume_all": false,
				"storage_limit": 10 * level, # mutltiply by level, how much we can store
				"supply_rate": 0,
				"consume_rate": 0
			});
			var n = rd.name;
			add_child(rd);
		
		# now we use a recipie to move products from the demanded, to the supplied area
		var recipe = "trif consume({0}, 1) and supply({0}, 1) then return".format([resource_name]);
		var rr = _create_resource_recipe_from_dict({
			"name": resource_name + "_to_" + resource_name,
			"frequency": 24 * level, # it represents the unload speed of this resource node - 2 per month * level
			"recipe": recipe,
		});
		add_child(rr);
	
	_set_resource_data_for_level();
	_set_resource_recipe_for_level();
	if (p_generate_ui):
		_create_overlay_panel();
		
	return;
	

func _level_changed(p_xp, p_spawn_effect = false):
	if (!is_building_template):
		update_resources(false); # regenerate storage limits and recipies on level change
		
		# TODO: update selection effect to handle multiple spatial's.... 
		# maybe make it more automated by having it search for enabled spatials
		# then we can make it self cleaning also
#		var level = xp.get_level();
#		var level_node = BUtil.find_child(self, "SpatialLevel" + str(level));
#		if ($SelectEffect && !is_building_template):
#			$SelectEffect.prepare(level_node, global_transform);
		
		
	._level_changed(p_xp, p_spawn_effect);
	
	
func get_company():
	return company;
	
	
func set_mouse_over(p_controller, p_mouse_over):
	# disabled warehouse select effect for now, as it makes the UI hard to read
	#if ($SelectEffect):
	#	$SelectEffect.apply_to(self, p_mouse_over);
	
	.set_mouse_over(p_controller, p_mouse_over);
	
	
func delete():
	rpc("delete_master_rpc");
	
	
master func delete_master_rpc():
	if (!get_tree().is_network_server()):
		return;
		
	# apply refund
	var refund = get_finances().get_refund_amount();
	company.receive_cash(refund, IncomeStatement.WAREHOUSES);
		
	rpc("delete_rpc");
	
	
remotesync func delete_rpc():
	if ($SelectEffect):
		$SelectEffect.release();
		
	queue_free();
	
	# event is after queue_free so we can detect deleted via is_queued_for_deletion
	EventMgr.trigger(EventMgr.Id.BUILDING_DELETED, { "sender": self, "building": self });


func set_visible(p_visible):
	.set_visible(p_visible);
	$StaticBody.set_enabled(p_visible);
