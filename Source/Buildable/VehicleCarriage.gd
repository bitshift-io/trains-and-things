#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

const ResourceNode = preload("../Resource/ResourceNode.gd")

var resource_name; # what product is being moved?
var amount = 0 # how much resource have we aboard?

var capacity = 1 # how much can we carry per load? really this is resource specific!

export(float) var length = 8.0 # how long is this carriage?

export(float) var load_time = 1.0 # how long the carriage takes to load
export(float) var unload_time = 1.0 # how long the carriage takes to load

onready var load_node = null
onready var market = BUtil.find_first_child_by_class_name(get_node("/root"), "Market");

var unload_timer = Util.create_timer(self, "_start_unload", 1, false, true);
var load_timer = Util.create_timer(self, "_start_load", 1, false, true);

onready var finances = get_node("Finances")

var vehicle; # who we belong too

var is_building_template = false;

func _ready():
	if (!is_building_template):
		add_to_group("persist");
		
	if (has_node("SelectEffect")):
		$"SelectEffect".prepare(find_node("train"), global_transform);
	return;
	
# this is called if we want to configure the prop as a template
# to allow us to deferentiate what is being built vs a built structure
func set_as_building_template():
	is_building_template = true;
	return;
	
func _free_load_node():
	if (load_node):
		load_node.queue_free();
		load_node = null;
		
	return;
	
# null for name means that carriage is not being used by the train
func _set_resource_name(name):
	if (name == resource_name):
		return;
		
	resource_name = name;
	
	# free the old carriage
	_free_load_node();
	
	Util.assert(market, "Couldnt find market");
	if (resource_name == null):
		return;
		
	#get_market_resource().registerAmountMax(capacity)

	var res = market.get_resource_by_name(resource_name);
	Util.assert(res, "Couldnt find market for this resource: " + resource_name)
	if (!res):
		return;
	
	var carriage_template = res.carriage;
	load_node = carriage_template.instance();
	#load_node.set_visible(true);
	add_child(load_node);
	
	return;
	
func init(p_vehicle):
	vehicle = p_vehicle;
	
func delete():
	# remove our modification to the market
	#get_market_resource().registerAmountMax(-capacity)
	#get_market_resource().consume(amount)
	
	if ($SelectEffect):
		$SelectEffect.release();
		
	queue_free();
	return;
	
func unload_resource():
	Util.assert(get_tree().is_network_server(), "_start_unload only to be called on the server");
	rpc("_start_unload_rpc");
	return unload_time;
	
remotesync func _start_unload_rpc():
	if (get_tree().is_network_server()):	
		var resource_node = vehicle.vehicle_route.get_current_resource_node();
		var resourceData = resource_node.get_resource_data(resource_name);
		if (resourceData && resourceData.isDemanded):			
			var resource = market.get_resource_by_name(resource_name)
			var value = resource.get_value(amount)
			
			if (resourceData.consumeAllOnUnload):
				get_company().receive_cash(value * amount, IncomeStatement.VEHICLES);
				vehicle.finances.add_revenue(value * amount, true);
				resource_node.add_xp(amount);
			else:
				var amount_to_unload = min(amount, resourceData.storageLimit - resourceData.get_import_amount())
				if (amount_to_unload > 0.0):
					resourceData.add_import_amount(amount_to_unload);
					get_company().receive_cash(value * amount_to_unload, IncomeStatement.VEHICLES);
					vehicle.finances.add_revenue(value * amount_to_unload, true);
					resource_node.add_xp(amount_to_unload);
					
			set_amount(0); # what ever is left is thrown out - wasted product!
		
	_set_resource_name(null);
	vehicle.position_on_current_track_at_offset();
	return;

func can_load_resource(p_resource_name):
	var resource_node = vehicle.vehicle_route.get_current_resource_node();
	var resource_data = resource_node.get_resource_data(p_resource_name)
	if (resource_data && resource_data.isSupplied):
		var amount_to_load = min(capacity, resource_data.get_export_amount())
		# only take a full train load to make things simple
		if (amount_to_load >= capacity):
			return true;
			
	return false;
	
func load_resource(p_resource_name, p_index):
	Util.assert(get_tree().is_network_server(), "_start_load only to be called on the server");
	rpc("_start_load_rpc", p_resource_name, p_index);
	
	# if no resource being loaded, return 0 here
	if (!p_resource_name):
		return 0;
		
	return load_time;
	
remotesync func _start_load_rpc(p_resource_name, index):
	_set_resource_name(p_resource_name);
	
	if (get_tree().is_network_server()):
		var resource_node = vehicle.vehicle_route.get_current_resource_node();
		var resourceData = resource_node.get_resource_data(resource_name)
		if (resourceData && resourceData.isSupplied):			
			var amount_to_load = min(capacity, resourceData.get_export_amount())
	
			# only take a full train load to make things simple
			if (amount_to_load < capacity):
				return
	
			resource_node.add_xp(amount_to_load);
			resourceData.add_export_amount(-amount_to_load);
			set_amount(amount + amount_to_load)
			
	#vehicle.obtain_vehicle_carriage_from_pool(self);
	vehicle.move_child(self, index);
	vehicle.position_on_current_track_at_offset();
	return;

func set_amount(a):
	if (get_tree().is_network_server()):
		rpc("set_amount_rpc", a)
	
remotesync func set_amount_rpc(a):
	amount = a
	if (load_node):
		if (amount == 0.0):
			load_node.set_visible(false)
		else:
			load_node.set_visible(true)
		
func get_company():
	return get_parent().company;

func get_market_resource():
	return market.get_resource_by_name(resource_name);
	
func save_to(savegame):
	savegame.append_builtins({
		resource_name = resource_name,
		amount = amount
	});

	return true;
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	savegame.load_builtins();
	savegame.resolve_parent().add_child(self); # setup parenting	
	_set_resource_name(resource_name);
	set_amount(amount);
	return true;

func get_finances():
	return finances;

