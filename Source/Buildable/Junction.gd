#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# A collection of connection points
# many connection points form a junction (or intersection)
# this class controls which connection points allows flow into any other
# connection point.
# Generally, any connection point (which itself can have many connections)
# only allows flow in from itself out to any other connection
# or from any other connection out to any of its connections
# this stops for examples, trains trying to do a 145 degree turn!
#
extends Spatial

var facingScale = 0.0; # not used, but we might need it later
var company = null;
var astarId = null;
var is_building_template = false;

export(float) var astar_weight = 0.0; # the weight for a train path through this junction
#export (NodePath) var keepVertical; # what node we want to keep upright

onready var debug_panel = BUtil.find_first_child_by_class_name(self, "DebugTextWidget");

var occupant = WeakRef.new(); # is there a train on this track?

onready var structure_big = BUtil.find_child(self, "Structure-Big")
onready var structure_small = BUtil.find_child(self, "Structure-Small")

enum LightColour { LIGHT_RED, LIGHT_GREEN };

onready var world = WorldMgr.get_world(self); 

func _ready():
	if (!world):
		return;
		
	#BLog.debug("Junction _ready(): " + get_path());
	world.astar.get(world.astar.RAIL).add_point_object(self);
	
	debug_panel.connect("set_visible", self, "_on_debug_panel_set_visible");
	return;
	
func set_as_building_template():
	is_building_template = true;
	
func spawn(p_parent, p_company, atransform, p_name):
	set_name(p_name);
	
	if (is_building_template):
		pass;
	else:
		add_to_group("persist");
		add_to_group("junctions");
		company = p_company
	
	set_global_transform(atransform);
	
	p_parent.add_child(self); # this calls _ready

	# transform should be applied by now

	# code to keep the keep vertical node straight up and down
	#if (keepVertical == null):
#		return;
		
	update_debug_panel();
	
	_keep_vertical(structure_small);
	_keep_vertical(structure_big);
	_update_visibility();
		
func _keep_vertical(keepVerticalNode):
	var oldXform = keepVerticalNode.get_global_transform();
	var oldFwd = oldXform.basis[2];
	var right = Vector3(0, 1, 0).cross(oldFwd);
	var fwd = right.cross(Vector3(0, 1, 0));
	var xform = oldXform;
	xform.basis = Basis(right, Vector3(0, 1, 0), fwd);
	keepVerticalNode.set_global_transform(xform);
	
func _on_debug_panel_set_visible(vis):
	update_debug_panel();
	
func update_debug_panel():
	if (!debug_panel or !debug_panel.is_visible()):
		return;
		
	debug_panel.clear();
	debug_panel.add_line(get_name());
	debug_panel.add_line("astart_id: " + str(astarId));
	debug_panel.add_line("astar weight: " + str(get_astar_weight()));
	
	if (occupant.get_ref()):
		debug_panel.add_line("occupant: " + occupant.get_ref().get_display_name());
		
	var connection_list = getConnections();
	for connection in connection_list:
		connection.update_debug_panel(debug_panel);
	
	return;
		
func get_astar_weight():
	if (occupant.get_ref()):
		return occupant.get_ref().get_astar_weight() + astar_weight;
		
	return astar_weight;
	
func is_occupied():
	return occupant.get_ref() != null;
	
func get_occupant():
	return occupant.get_ref();

func set_occupant(by):
	if (is_occupied() && by != null):
		Util.assert(get_occupant() == by, "Changing occupant when it doesnt belong to us or null");
		
	occupant = weakref(by) if by else WeakRef.new(); 
	update_debug_panel();
	
func clear_occupant_if(p_occupant):		
	if (p_occupant == get_occupant()):
		set_occupant(null);
	
func getConnections():
	return BUtil.find_children_by_class_name(self, "Connection", true);

# find a route as a list of junctions
func findRouteTo(otherJunction):
	return findRoute(self, otherJunction)
	
# AStar route finding
func findRoute(fromJunction, toJunction):
	var path = world.astar.get(world.astar.RAIL).getPath(fromJunction, toJunction)
	var junctionPath = []
	for p in path:
		if (BUtil.is_class_name(p, "Junction")):
			junctionPath.append(p)
			
	return junctionPath
	
func setMouseOver(mouseOver):
	if (mouseOver):
		set_scale(Vector3(1.1, 1.1, 1.1))		
	else:
		set_scale(Vector3(1.0, 1.0, 1.0))
		
func onSelected():
	#samplePlayer.play("Selected")
	pass
	
func get_station():
	# need a more generic way to do this....
	var ts = BUtil.find_parent_by_class_name(self, "TrainStation");
	if (ts):
		return ts;
		
	return BUtil.find_parent_by_class_name(self, "Airport");
	
## helper to avoid some complexity, find and return the track connecting two junctions
## this is a weak ref to the track and curve
#func getTrackAndCurveTo(otherJunction):
#	var outgoingConnections = getConnections()
#	for ocn in outgoingConnections:
#		for t in ocn.getTracks():
#			#var otherConnectionNode = t.getOtherConnection(ocn)
#			var otherConnectionNode = t.getConnection(t.END)
#			if (otherConnectionNode.get_junction() == otherJunction):
#				var curve = t.getCurve(ocn)
#				return [weakref(t), curve]
#
#	return [null, null]
	
	
## helper to avoid some complexity, find and return the track connecting two junctions
#func getTrackTo(otherJunction):
#	var outgoingConnections = getConnections()
#	for ocn in outgoingConnections:
#		for t in ocn.getTracks():
#			#var otherConnectionNode = t.getOtherConnection(ocn)
#			var otherConnectionNode = t.getConnection(t.END)
#			if (otherConnectionNode.get_junction() == otherJunction):
#				return t
#
#	return null
	
# find a path from this junction to another junction
func find_astar_path(otherJunction):
	return world.astar.get(world.astar.RAIL).get_astar_path(self, otherJunction);

func notifyAddTrack(track):
	update_debug_panel();
	_update_visibility();
	return;
	
# a track was removed
func notifyRemoveTrack(track):
	# if we are a junction, ensure we clear occupancy for when a new track is built!
	if (track.get_occupant() == get_occupant()):
		set_occupant(null);
	
	var trackCount = 0
	for c in getConnections():
		trackCount += c.getTracks().size()
		
	if (trackCount == 0 && !get_station()):
		delete();
		
	update_debug_panel();
	_update_visibility();
	return;
	
# delete on local machine only, no RPC call
func delete_local():
	delete();
	
func delete():
	world.astar.get(world.astar.RAIL).remove_point_object(self)
	queue_free()
	
# get a list of outgoing connections
func getOutgoingConnections():
	var results = []
	var connections = getConnections();
	for c in connections:
		if (c.isOutgoing()):
				results.append(c);
			
	return results;
	
# get a list of outgoing tracks
func getOutgoingTracks():
	var results = []
	var connections = getConnections();
	for c in connections:
		if (c.isOutgoing()):
			for t in c.getTracks():
				results.append(t);
			
	return results;
	
# get a list of incoming connections
func getIncomingConnections():
	var results = []
	var connections = getConnections();
	for c in connections:
		if (c.isIncoming()):
			results.append(c);
			
	return results;
	
# get a list of incoming tracks
func getIncomingTracks():
	var results = []
	var connections = getConnections();
	for c in connections:
		if (c.isIncoming()):
			for t in c.getTracks():
				results.append(t);
			
	return results;
			
# get a list of any tracks that connect to this junction
func getTracks():
	var results = [];
	var connections = getConnections();
	for c in connections:
		for t in c.getTracks():
			results.append(t);
			
	return results;
	
func reverse():
	# rotate the mesh
	#transform = transform.rotated(Vector3(0, 1, 0), 180);
	var xform = get_global_transform();
	var origin = xform.origin;
	xform.origin = Vector3();
	xform = xform.looking_at(xform.basis[2], Vector3(0, 1, 0));
	xform.origin = origin;
	set_global_transform(xform);
	
	# reverse the connections
	var connections = getConnections();
	for c in connections:
		c.reverse();
	
	return;
		
func save_to(savegame):
	if (!company):
		return false;
		
	savegame.append_dict({
		company = company.get_path(),
		global_transform = global_transform
	});

	return true;
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	company = savegame.resolve_node(c["company"]);
	var xform = c["global_transform"];
	spawn(savegame.resolve_parent(), company, xform, get_name());
	return true;
	
func _update_visibility():
	var big_visible = (getOutgoingTracks().size() > 1 || getIncomingTracks().size() > 1);
	structure_big.set_visible(big_visible);
	structure_small.set_visible(!big_visible);
	
func get_visible_structure():
	if (structure_big.is_visible()):
		return structure_big;
		
	return structure_small;
	
func set_light_colour(light_colour):
	var structure = get_visible_structure();
	var green = (light_colour == LightColour.LIGHT_GREEN);
	structure.get_node("red").set_visible(!green);
	structure.get_node("green").set_visible(green);
	
func set_company(p_company):
	company = p_company;
