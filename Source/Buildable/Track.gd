#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This is the track between two connections of a junction.
# this is represented by two points connected via a biezier curve
# and is responsible for laying out the mesh along this path
#
extends Spatial

const START = 0
const END = 1

#var im = ImmediateGeometry.new()
#var m = SpatialMaterial.new()

onready var instance_template = get_node("InstanceTemplate")
onready var finances = get_node("Finances")

#onready var outline_material = load("res://Materials/M-DefaultAlphaOnUV2.tres");

onready var curve_mesh_nodes = [BUtil.find_child(self, "LOD0/BCurveMeshNode"), 
								BUtil.find_child(self, "LOD1/BCurveMeshNode")]
onready var instance_templates = [BUtil.find_child(self, "Structure-RailRoad-A/LOD0"), 
									BUtil.find_child(self, "Structure-RailRoad-A/LOD1")]
									
onready var lod_group = BUtil.find_child(self, "BLODGroup")

var build_rate = 100.0 # metres per second
var length_built = 0;

var radius = 10.0

var length = 0.0 # the weight for a train path through this track

var occupant = WeakRef.new(); # is there a train on this track?

# train track has a start and end connection so we can find the connecting PathNode
var connections = [null, null]

var company = null
var astarId = null;
var isBridge = false;

const SNAP_ALONG = 0;
const SNAP_TO_ENDS = 1;
const SNAP_NONE = 2;

export(int, "Along", "ToEnds", "None") var snap = 0; # how should snapping work for this piece of track

onready var debug_panel = BUtil.find_first_child_by_class_name(self, "DebugTextWidget");

var is_building_template = false;
var terminal; 

onready var world = WorldMgr.get_world(self); 

signal track_build_complete(track);


func _init():
	set_notify_local_transform(true);
	set_notify_transform(true);
	
	#set_generate_mesh_on_transform_changed(false);  # this breaks things
	return;


func _ready():
	if (!world):
		return;
		
	curve_mesh_nodes[0].connect("mesh_generated", self, "_mesh_generated0");
	curve_mesh_nodes[1].connect("mesh_generated", self, "_mesh_generated1");
	return;


# this is called if we want to configure the prop as a template
# to allow us to deferentiate what is being built vs a built structure
#
# the only time this is called for a Track is by the train station building template
# so it needs to deform the track to the terrain and draw it cheaply
func set_as_building_template():
	is_building_template = true;
#
#	set_mesh_generation_type(MGT_BOUNDS);
#	set_generate_mesh_on_transform_changed(true);
#
#
#	if (world.date_time.is_connected("month_changed", self, "_on_month_changed")):
#		world.date_time.disconnect("month_changed", self, "_on_month_changed");
#
#	remove_from_group("tracks");
#	remove_from_group("persist");
#	company = null;
	return;
	
	
# called to indicate we belong to a terminal
func set_terminal(p_terminal):
	terminal = p_terminal;

	finances.purchase_cost = 0;
	finances.maintenance_cost = 0;
	finances.life_time_expenses = 0;
	
	
func get_terminal():
	return terminal;
	
	
func get_astar_weight():
	var weight = length;
	
	if (terminal):
		weight += terminal.astar_weight;
		
	if (occupant.get_ref()):
		weight += occupant.get_ref().get_astar_weight();
		
	return length;
	
	
func _on_month_changed(monthIndex):
	if (company):
		var expense = world.date_time.yearlyToMonthly(finances.get_maintenance_cost());
		company.pay_cash(expense, IncomeStatement.TRACKS);
		finances.add_expense(expense, true);
	
	
func is_occupied():
	return occupant.get_ref() != null;
		
		
func get_occupant():
	return occupant.get_ref();


func set_occupant(by):
	if (is_occupied() && by != null):
		Util.assert(get_occupant() == by, "Changing occupant when it doesnt belong to us or null");
		
	occupant = weakref(by) if by else WeakRef.new(); 
	update_debug_panel(true);
	get_start_junction().set_light_colour(get_start_junction().LightColour.LIGHT_GREEN if (by == null) else get_start_junction().LightColour.LIGHT_RED);


func clear_occupant_if(p_occupant):
	if (is_occupied()):
		Util.assert(p_occupant == get_occupant(), "Clearing occupant when it doesnt belong to us or null");
		
	if (p_occupant == get_occupant()):
		set_occupant(null);


func _notification(what):
	if (what == Spatial.NOTIFICATION_TRANSFORM_CHANGED): # NOTIFICATION_TRANSFORM_CHANGED #NOTIFICATION_LOCAL_TRANSFORM_CHANGED):
		_transform_changed();
	return;


func _transform_changed():
	# template tracks 
	if (!is_building_template):
		return;
	
	generate_mesh_and_collision();
	return;


func generate_mesh_and_collision():
	# move the LOD groups LOD origin to the middle of the track segment
	# need to inverse out the global transform to compute a local space position
	var mid_point = curve_mesh_nodes[0].get_transform_from_distance_along_curve(length / 2);
	var gxform = global_transform;
	gxform = gxform.inverse();
	var mid_point_local = gxform * mid_point.origin;
	var aabb = AABB();
	aabb.position = mid_point_local;
	# TODO: set AABB to contain the track 
	lod_group.aabb = aabb;
	
	for i in range(0, curve_mesh_nodes.size()):
		curve_mesh_nodes[i].generate_mesh();
		
		# building template dont need physics
		if (!is_building_template):
			curve_mesh_nodes[i].generate_collision();
	
	var start_junction = get_junction(0);
	if (start_junction):	
		start_junction.set_global_transform(curve_mesh_nodes[0].get_transform_from_distance_along_curve(0));
		
	var end_junction = get_junction(1);
	if (end_junction):	
		end_junction.set_global_transform(curve_mesh_nodes[0].get_transform_from_distance_along_curve(curve_mesh_nodes[0].get_world_curve().get_baked_length()));

	# move debug panel
	if (debug_panel):
		debug_panel.global_transform.origin = mid_point.origin;
		
		
# called when the mesh on LOD0 has been generated
func _mesh_generated0(mesh_inst):
	if (!is_building_template && $SelectEffect):
		$SelectEffect.prepare(mesh_inst, global_transform);

	# instance materials
	_mesh_generated1(mesh_inst);
	return;
	
	
func _mesh_generated1(mesh_inst):
	# instance & setup materials so we can do alpha fade effects
	var instances = Util.instance_materials(mesh_inst);
	for mat_inst in instances:
		if (mat_inst.is_class("BStandardMaterial3D")):
			mat_inst.set_transparency(StandardMaterial3D.TRANSPARENCY_ALPHA_SCISSOR);
			mat_inst.set_flag(StandardMaterial3D.FLAG_ALPHA_ON_UV2, true);
			mat_inst.set_alpha_scissor_threshold(0.0);
	
	
func _local_transform_changed():
	return;
	
	
func _update_length():
	length = curve_mesh_nodes[0].get_world_curve().get_baked_length();
#	var f = finances.duplicate();
#	f.purchase_cost *= length;
#	f.maintenance_cost *= length;
#	f.add_expense(f.get_purchase_cost(), true);
	
	
func updateCurve():	
	_update_length();
	
	#Util.assert(instanceTemplate, "instanceTemplate is null");
	#curve_mesh.set_instance_template(instanceTemplate);
	
	for i in range(0, curve_mesh_nodes.size()):
		curve_mesh_nodes[i].set_instance_template(instance_templates[i]);
	
	generate_mesh_and_collision();
	return;


func setSplineMeshHidden(hidden):
	#curve_mesh.set_visible(!hidden);
	#hide/show the mesh instance
	return;


func get_world_curve():
	return curve_mesh_nodes[0].get_world_curve();


# return the curve mesh node, the highest LOD
#func get_curve_mesh_node():
#	return curve_mesh_nodes[0];
		
		
func get_transform_from_distance_along_curve(p_offset):
	return curve_mesh_nodes[0].get_transform_from_distance_along_curve(p_offset);
	
	
func play_spawn_effect():
	# setup & play fade in effect
	length_built = 0.0;
	set_process(true); 
	
	
func spawn(p_parent, company, startConnection, endConnection, p_curve, p_name):
	#BLog.debug("Spawning track: " + p_name);
		
	set_name(p_name);
	p_parent.add_child(self); # this calls _ready()
	
	world.astar.get(world.astar.RAIL).add_point_object(self);
	self.company = company;
	
	#assert(startConnection.isOutgoing());
	#assert(endConnection.isIncoming());
	
	connections[START] = startConnection;
	connections[END] = endConnection;
	
	if (is_building_template):
		curve_mesh_nodes = [curve_mesh_nodes[0]]; # remove all except the first curve mesh node
		curve_mesh_nodes[0].set_mesh_generation_type(curve_mesh_nodes[0].MGT_BOUNDS);
		lod_group.set_process(false); # disable it changing to other lods!
	
	for i in range(0, curve_mesh_nodes.size()):
		Util.assert(curve_mesh_nodes[i].local_curve != null, "Invalid local_curve");
		curve_mesh_nodes[i].local_curve.set_radius(radius);
		curve_mesh_nodes[i].get_world_curve().set_radius(radius);
		curve_mesh_nodes[i].local_curve = p_curve;
	
	#add_child(im)
	#add_child(splineMesh)
	Util.assert(instance_template != null, "instance_template is null");
	updateCurve()
	#call_deferred("updateCurve");
	instance_template.set_visible(false)
	
	# let spline nodes what tracks come from them
	for c in connections:
		if (c != null):
			c.addTrack(self);
	
	if (!is_building_template):
		add_to_group("tracks");
		add_to_group("persist");
		
		if (get_tree().is_network_server()):
			world.date_time.connect("month_changed", self, "_on_month_changed");
		
	update_debug_panel(true);
	
	#updateDebugDrawing()
	
	#Util.assert(p_curve.radius == radius, "Track radius mismatch!");
	#Util.assert(local_curve.radius == radius, "Track radius mismatch!");
	#Util.assert(get_world_curve().radius == radius, "Track radius mismatch!");
		
	finances = get_node("Finances");
	finances.purchase_cost *= length;
	finances.maintenance_cost *= length;
	
	if (!is_building_template):
		finances.add_expense(finances.get_purchase_cost(), true);
		
	set_occupant(null); # to set light colour
	
	# by default we instant spawn!
	length_built = length;
	set_process(false); 
	
	#BLog.debug("Spawning track complete: " + get_path());
	return;
	
	
func update_debug_panel(force = false):
	if (!debug_panel or (!debug_panel.is_visible() and !force)):
		return;
		
	debug_panel.clear();
	debug_panel.add_line(get_name());
	debug_panel.add_line("astart_id: " + str(astarId));
	
	if (connections[START]):
		debug_panel.add_line("start conn: " + str(connections[START].get_junction().get_name()));
	if (connections[END]):
		debug_panel.add_line("end conn: " + str(connections[END].get_junction().get_name()));
		
	debug_panel.add_line("astar weight: " + str(get_astar_weight()));
	
	if (get_occupant()):
		debug_panel.add_line("occupant: " + get_occupant().get_display_name());
	
	var mid_point = curve_mesh_nodes[0].get_transform_from_distance_along_curve(length / 2);
	debug_panel.global_transform.origin = mid_point.origin;
		
	#var mid_point = get_global_transform_from_distance_along_curve(length / 2);
	#debug_panel.transform.origin = mid_point.origin;
	
	
# get the finances for this actual track - using the curve of this track
func get_finances():
	return finances; #get_finances_for_length(get_world_curve().get_baked_length())


#func getTrainStations():
#	var stations = []
#	for con in connections:
#		stations.append(Util.find_parent_by_class_name(con, "TrainStation"))
#	
#	return stations


func getConnection(index):
	return connections[index]
	
	
func get_junction(index):
	if (connections[index] == null):
		return null;
		
	return connections[index].get_junction()
	
	
func get_start_junction():
	return get_junction(START);
		
		
func get_end_junction():
	return get_junction(END);


# check if a junction is part of a train station	
#func getStation(index):
#	return get_junction(index).getStation()

# check if this track belongs to a train station
func get_station():
	return BUtil.find_parent_by_class_name(self, "TrainStation");
	
	
# a train track has a connection at both ends
# if we have one end and pass that into this function,
# this returns the other end
func getOtherConnection(thisConnection):
	Util.assert(connections.size() == 2, "Tracks should only have 2 connections")
	if (connections[0] == thisConnection):
		return connections[1]
	
	return connections[0]
	
	
#func updateDebugDrawing():
#	# draw the path
#	var pointList = local_curve.tesselate(5, 4);
#	im.set_material_override(m)
#	im.clear()
#	im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
#	for x in pointList:
#		im.add_vertex(x)
#	im.end()


func is_interactive(controller):
	return true;


func set_mouse_over(controller, mouseOver):
	if ($SelectEffect):
		$SelectEffect.apply_to(self, mouseOver);
	return;
	
	
func set_selected(controller, selected):
	# should really just do some material fx to make it pastel
#	var mesh_inst = get_mesh_instance();
#	if (selected):
#		mesh_inst.translate(Vector3(0, 1.0, 0));
#	else:
#		mesh_inst.translate(Vector3(0, -1.0, 0));
#		
	return true;

	
func delete():
	# redirect deletion to our owner
	if (terminal):
		terminal.delete();
		return;
		
	rpc("delete_master_rpc");
	
master func delete_master_rpc():
	if (!get_tree().is_network_server()):
		return;
	
	# apply refund for the deletion
	var refund = get_finances().get_refund_amount();
	company.receive_cash(refund, IncomeStatement.TRACKS);
		
	rpc("delete_rpc");
	
	
# delete on local machine only, no RPC call, and no events fired - called from VehicleTerminal.gd to clean up private tracks
func delete_local():
	for con in connections:
		if (con != null):
			con.removeTrack(self)
	
	if ($SelectEffect):
		$SelectEffect.release();
			
	world.astar.get(world.astar.RAIL).remove_point_object(self)
	queue_free()
	
	
remotesync func delete_rpc():
	delete_local();
	
	# event is after queue_free so we can detect deleted via is_queued_for_deletion
	EventMgr.trigger(EventMgr.Id.TRACK_DELETED, { "sender": self, "track": self });
	
	
# returns an array of valid astar paths to the given a list of terminals
func find_astar_paths_to_terminals(terminal_list):
	var astar_paths = []
	for terminal in terminal_list:
		var terminal_end_junctions = terminal.get_end_junctions();
		for o in terminal_end_junctions:
			var astar_path = get_junction(END).find_astar_path(o);
			if (astar_path):
				astar_paths.append(astar_path);
					
	return astar_paths;
	
	
# get the set of next potential tracks to take
func getOutgoingTracks():
	return get_junction(END).getOutgoingTracks();
	
	
# is this track able to be reversed?
func can_reverse():
	var start_tracks = get_junction(START).getTracks();
	var end_tracks = get_junction(END).getTracks();
	
	for t in start_tracks:
		if (t != self):
			return false;
			
	for t in end_tracks:
		if (t != self):
			return false;
		
	return true;
	
	
# turn the track around
func reverse():
	rpc("reverse_master_rpc");
	
	
master func reverse_master_rpc():
	rpc("reverse_rpc");
	
	
remotesync func reverse_rpc():
	for i in range(0, curve_mesh_nodes.size()):
		curve_mesh_nodes[i].local_curve = curve_mesh_nodes[i].local_curve.get_reversed();
		
	generate_mesh_and_collision();
	return;
	
	
func save_to(savegame):
	if (is_building_template or !company):
		return false;

	var connection_paths = [connections[0].get_path(), connections[1].get_path()];
	savegame.append_dict({
		company = company.get_path(),
		local_curve = Util.packCurve(curve_mesh_nodes[0].local_curve),
		connections = connection_paths
	});
	return true;
	
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	company = savegame.resolve_node(c["company"]);
	var curve = Util.unpackCurve(c["local_curve"], radius);
	var le = curve.get_baked_length();
	var con0 = savegame.resolve_node(c["connections"][0]);
	var con1 = savegame.resolve_node(c["connections"][1]);
	spawn(savegame.resolve_parent(), company, con0, con1, curve, get_name());
	return true;
	
	
func set_company(p_company):
	company = p_company;
	
	
func get_company():
	return company;
	
	
func _process(delta):
	# update the build_percent
	length_built += build_rate * delta;
	var built_percent = clamp(length_built / length, 0.0, 1.0) if length > 0 else 1.0;
	if (built_percent >= 1.0):
		emit_signal("track_build_complete", self);
		set_process(false);
		
	# override the material so we can use an alpha effect
	for i in range(0, curve_mesh_nodes.size()):
		var mesh_instances = BUtil.find_children_by_class_name(curve_mesh_nodes[i], "MeshInstance");
		for mesh_inst in mesh_instances:
			var mesh = mesh_inst.get_mesh();
			if (!mesh):
				continue;
				
			for si in range(0, mesh.get_surface_count()):
				var material = mesh_inst.get_surface_material(si);
				if (material):
					material.set_alpha_scissor_threshold(1.0 - built_percent);
				#var material = mesh.surface_get_material(si);
				#mesh_inst.set_surface_material(si, override_material);
				
