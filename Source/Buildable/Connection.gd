#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Connection is a Junction node
# it is a connection point from a track to a junction
# this is needed as many tracks might come to a single connection
# but a train can only go out through a different connection
# this also represents the physical node
# and a junction can span physical space with any number of connections into the junction
#
extends Spatial

var tracks = []; # weak refs

const EITHER = 0;
const INCOMING = 1;
const OUTGOING = 2;

var direction_str = ["Either", "Incoming", "Outgoing"];

export(int, "Either", "Incoming", "Outgoing") var direction = 0; # outgoing or incoming?

#onready var mouseOverIndicator = $MouseOverIndicator; #get_node("MouseOverIndicator")

onready var world = WorldMgr.get_world(self); 

func _ready():
	if (!world):
		return;
		
	#set_physics_process(true)
#	if (mouseOverIndicator):
#		mouseOverIndicator.hide();
#	else:
#		BLog.debug("'" + get_path() + "' which has no node 'MouseOverIndicator'");
#		
	return;

func _physics_process(delta):
	return;
	
func setMouseOver(mouseOver):
#	if (mouseOver):
#		mouseOverIndicator.show();
#	else:
#		mouseOverIndicator.hide();
	return;
		
func update_debug_panel(debug_panel):
	debug_panel.add_line(get_name() + "(" + direction_str[direction] + ")");
	
	for track in tracks:
		if (direction == OUTGOING):
			debug_panel.add_line("\t" + get_junction().get_name() + " -> " + track.get_name());
		elif (direction == INCOMING):
			debug_panel.add_line("\t" + track.get_name() + " -> " + get_junction().get_name());
	
	return;
		
func getTracks():
	return tracks;
	
func addTrack(track):
	tracks.append(track);
	_connect_track(track);
	get_junction().notifyAddTrack(track); # TODO: make signal
	return;
	
# connect track in astar
func _connect_track(track):
	if (direction == OUTGOING):
		world.astar.get(world.astar.RAIL).connect_point_objects(get_junction(), track);
		#BLog.debug("Connecting " + get_junction().get_name() + " -> " + track.get_name());
	elif (direction == INCOMING):
		world.astar.get(world.astar.RAIL).connect_point_objects(track, get_junction());
		#BLog.debug("Connecting" + track.get_name() + " -> " + get_junction().get_name());
		
	return;
	
func removeTrack(track):
	tracks.remove(tracks.find(track));
	_disconnect_track(track);
	get_junction().notifyRemoveTrack(track); # TODO: make signal
	return;
	
# disconnect track in astar
func _disconnect_track(track):
	if (direction == OUTGOING):
		world.astar.get(world.astar.RAIL).disconnect_point_objects(get_junction(), track);
		#BLog.debug("Disconnecting" + get_junction().get_name() + " -> " + track.get_name());
	elif (direction == INCOMING):
		world.astar.get(world.astar.RAIL).disconnect_point_objects(track, get_junction());
		#BLog.debug("Disconnecting" + track.get_name() + " -> " + get_junction().get_name());

func get_junction():
	return BUtil.find_parent_by_class_name(get_parent(), "Junction");
	
func isOutgoing():			
	return direction == EITHER or direction == OUTGOING;
	
func isIncoming():
	return direction == EITHER or direction == INCOMING;
	
func isEitherDirection():
	return direction == EITHER;
	
func reverse():
	for track in tracks:
		_disconnect_track(track);
		
	if (direction == OUTGOING):
		direction = INCOMING;
	elif (direction == INCOMING):
		direction = OUTGOING;
		
	for track in tracks:
		_connect_track(track);
	return;
	
		
