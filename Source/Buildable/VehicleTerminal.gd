#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This is a VehicleTerminal - a location where a certain type(s) of vehicles can bring and exchange goods or people
#

extends Spatial


# http://dfaction.net/using-reflection-in-gdscript-to-resolve-nodepaths/
# http://docs.godotengine.org/en/latest/reference/gdscript.html#exports
# https://github.com/godotengine/godot/issues/3586

onready var finances = get_node("Finances")

var resource_node;
var company;
var is_building_template = false;

var astar_weight = 100.0; # the weight to add to the tracks connected to this station

onready var world = WorldMgr.get_world(self);


func get_finances():
	return finances;
	
	
# called when first constructed to setup any data required
# we need to pass in information on who constructing the object
# so we can generate a network id
func spawn(p_controller, p_parent, p_company, p_name, p_transform):
	Util.assert(p_parent, "Spawning building has no parent");
	if (!p_parent):
		return;
		
	company = p_company;
	
	# compute transform that is relative to the parent
	if (p_parent.has_method("get_global_transform")):
		var xform = p_parent.get_global_transform().affine_inverse() * p_transform;
		set_transform(xform);
	
	set_name(p_name);
	var track_builder_list = BUtil.find_children_by_class_name(self, "TrackBuilder");
	for i in range(0, track_builder_list.size()):
		var track_builder = track_builder_list[i];
		track_builder.connect("track_spawned", self, "_on_track_spawned");
		if (is_building_template):
			track_builder.set_as_building_template();
			#track_builder.free();
			#continue;
			
		track_builder.controller = p_controller;
		track_builder.set_name(p_name + "_TrackBuilder_" + str(i));
		
	p_parent.add_child(self); # this calls _ready
	
	# child tracks should be spawned so fudge their astar weight
#	if (!is_building_template):
#		for i in range(0, track_builder_list.size()):
#			var track_builder = track_builder_list[i];
#			for track in track_builder.trackList:
#				track.astar_weight += astar_weight;
#				track.set_terminal(self);
	
	finances = get_node("Finances");
	if (!is_building_template):
		finances.add_expense(finances.get_purchase_cost(), true);
	return;
	

func _on_track_spawned(p_track):
	p_track.set_terminal(self); # tell the track it belongs to us
		
	
func _ready():
	if (!world):
		return;
		
	#BLog.debug("TrainStation _ready(): " + get_path());
	resource_node = BUtil.find_parent_by_class_name(get_parent(), "ResourceNode")
	if (resource_node):
		resource_node.add_train_station(self);
	
	if (is_building_template):
		Util.call_deferred("remove_from_group_and_all_children", self, "persist");
	else:
		add_to_group("vehicle_terminals");
		add_to_group("persist");
		
		if ($SelectEffect && !is_building_template):
			$SelectEffect.prepare(find_node("station"), global_transform);
			
		if (get_tree().is_network_server()):
			world.date_time.connect("month_changed", self, "_on_month_changed")

	return;
	
	
func _on_month_changed(monthIndex):
	if (company):
		var expense = world.date_time.yearlyToMonthly(finances.get_maintenance_cost());
		company.pay_cash(expense, IncomeStatement.TERMINALS);
		finances.add_expense(expense, true);


# this is called if we want to configure the prop as a template
# to allow us to deferentiate what is being built vs a built structure
func set_as_building_template():
	is_building_template = true;
	return;
	
	
func is_interactive(controller):
	return !is_building_template;
	
	
func set_mouse_over(controller, mouseOver):
	if ($SelectEffect):
		$SelectEffect.apply_to(self, mouseOver);
		
	var tracks = BUtil.find_children_by_class_name(self, "Track");
	for track in tracks:
		track.set_mouse_over(controller, mouseOver);
	
	#if (mouseOver):
		#set_scale(Vector3(1.1, 1.1, 1.1));	
	#	apply_outline(self, true);
	#else:
		#set_scale(Vector3(1.0, 1.0, 1.0));
	#	apply_outline(self, false);
		
	if (resource_node):
		resource_node.set_mouse_over(controller, mouseOver);


func set_selected(controller, selected):
	return true;
	
	
# get the terminating junctions (i.e. where a train should stop)	
func get_end_junctions():
	var track_list = BUtil.find_children_by_class_name(self, "Track", true);
	var outgoing_junction_list = [];
	for track in track_list:
		var junction = track.get_junction(track.END);
		if (junction != null):
			outgoing_junction_list.append(junction);
		
	return outgoing_junction_list;
	
	
func get_resource_node():
	return resource_node;
	
	
# returns an array of valid paths from this station to the other station
func find_astar_paths(otherStation):
	var selfJuncs = get_end_junctions();
	var otherJuncs = otherStation.get_end_junctions();
	
	var routes = []
	for s in selfJuncs:
		for o in otherJuncs:
			var route = s.find_astar_path(o);
			if (route && route.size()):
				routes.append(route);
				
	return routes;
	
	
func save_to(savegame):
	if (!company):
		return false;
	
	savegame.append_dict({
		company = company.get_path(),
		transform = transform
	});
	
	return true;
	
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	company = savegame.resolve_node(c["company"]);
	transform = c["transform"];
	
	# track builders don't need to do anything as the tracks are saved
	var track_builder_list = BUtil.find_children_by_class_name(self, "TrackBuilder");
	for track_builder in track_builder_list:
		track_builder.free();
	
	savegame.resolve_parent().add_child(self); # setup parenting
	return true;
	
	
func delete():
	rpc("delete_master_rpc") 
	
	
master func delete_master_rpc():
	if (!get_tree().is_network_server()):
		return;
		
	# apply refund
	var refund = get_finances().get_refund_amount();
	company.receive_cash(refund, IncomeStatement.TERMINALS);
		
	rpc("delete_rpc");
	
	
# delete on local machine only, no RPC call
func delete_local():
	delete_rpc();
	
	
remotesync func delete_rpc():
	if ($SelectEffect):
		$SelectEffect.release();
			
	# move from the train station parenting to the global parenting
	var junction_parent = world.junctions;
	var junction_list = BUtil.find_children_by_class_name(self, "Junction", true);
	for junction in junction_list:
		var xform = junction.get_global_transform();
		junction.get_parent().remove_child(junction);
		junction_parent.add_child(junction);
		junction.set_global_transform(xform);
		
	var track_list = BUtil.find_children_by_class_name(self, "Track", true);
	for track in track_list:
		track.delete_local(); # this means no signals are emitted about this track being deleted
		
	if (resource_node):
		resource_node.remove_train_station(self);
		
	queue_free()
	
	# event is after queue_free so we can detect deleted via is_queued_for_deletion
	EventMgr.trigger(EventMgr.Id.BUILDING_DELETED, { "sender": self, "building": self });
	
	
# determine if this building can be built at its current location
func can_build():
	var world = get_tree().get_root().get_world();
	Util.assert(world, "world invalid");
	var space_state = world.get_direct_space_state();
	Util.assert(space_state, "space_state invalid");
	
	var params = PhysicsShapeQueryParameters.new();
	params.set_shape(get_clearance_collision_shape().get_shape());
	params.set_transform(transform);
	params.exclude = BUtil.get_terrain().get_collision_nodes() + get_collision_nodes();  # ignore terrain + any child collision nodes
	var results = space_state.intersect_shape(params, 1);
	return false if (results.size() > 0) else true;
	
	
func get_collision_nodes():
	var col_children = BUtil.find_children_by_class_name(self, "CollisionObject");
	return col_children;
	
	
# this shape is used to check the space around the building is free to be able to build
func get_clearance_collision_shape():
	return $"ClearanceCollisionShape";


func set_company(p_company):
	company = p_company;
	
	var junction_list = BUtil.find_children_by_class_name(self, "Junction", true);
	for junction in junction_list:
		junction.set_company(p_company);
		
	var track_list = BUtil.find_children_by_class_name(self, "Track", true);
	for track in track_list:
		track.set_company(p_company);
		

func get_company():
	return company;
	
	
func set_visible(p_visible):
	.set_visible(p_visible);
	$StaticBody.set_enabled(p_visible);
	
	
func get_income_statement_field():
	return IncomeStatement.TERMINALS;
