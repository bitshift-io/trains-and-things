#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# This class lists the vehicles route from ResourceNode to the next ResourceNode
# each route point includes a cargo list.
# This also tracks the vehicle current terminal, and its destination
#
extends Node

var resource_node_list = []; # a list of resource nodes to visit
var load_resources_list = []; # this is a list of a dictionaries of resource to load at the stop
var unload_resources_list = []; # this is a list of dictionary resources to unload at the stop
var manifest_resources_list = []; # when leaving each station, how the train carriages should be configured

var destination_index = 0;
var current_index = 0;


func copy(other):
	deserialize(other.serialize(), get_tree());
	return;
	
	
func size():
	return resource_node_list.size();
	
	
func clear():
	resource_node_list = [];
	load_resources_list = [];
	unload_resources_list = [];
	manifest_resources_list = [];
	destination_index = 0;
	current_index = 0;


func _ready():
	add_to_group("persist");
	return;
	
	
# set destination 
func reset_for_vehicle_spawn():
	if (!get_tree().is_network_server()):
		return;

	if (current_index != (resource_node_list.size() - 1)):
		rpc("set_current_index_rpc", resource_node_list.size() - 1);

	if (destination_index != 0):
		rpc("set_destination_index_rpc", 0);
		
	return;
	
	
func progress_to_next_destination():
	if (!get_tree().is_network_server()):
		return;
		
	if (destination_index + 1 < resource_node_list.size()):
		rpc("set_destination_index_rpc", destination_index + 1);
	else:
		rpc("set_destination_index_rpc", 0);
		
	return;
	
		
# set current_index to destination_index
func arrive_at_destination():
	if (!get_tree().is_network_server()):
		return;
		
	rpc("set_current_index_rpc", destination_index);
		
		
remotesync func set_destination_index_rpc(index):
	destination_index = index;


remotesync func set_current_index_rpc(index):
	current_index = index;
	
	
func get_current_manifest_resources():
	return manifest_resources_list[current_index];
	
	
func get_current_load_resources():
	return load_resources_list[current_index];
	
	
func get_current_unload_resources():
	return unload_resources_list[current_index];
	
	
# set the current resource node if possible
func set_current_resource_node(resource_node):
	if (!get_tree().is_network_server()):
		return false;
		
	for i in range(0, resource_node_list.size()):
		if (resource_node_list[i] == resource_node):
			rpc("set_current_index_rpc", i);
			return true;
	
	return false;
		
		
func get_current_resource_node():
	return resource_node_list[current_index];
	
# set the destination resource node if possible
func set_destination_resource_node(resource_node):
	if (!get_tree().is_network_server()):
		return false;
		
	for i in range(0, resource_node_list.size()):
		if (resource_node_list[i] == resource_node):
			rpc("set_destination_index_rpc", i);
			return true;
			
	return false;
			
			
func get_destination_resource_node():
	return resource_node_list[destination_index];
	
	
func get_resource_node(index):
	if (resource_node_list.size() > 0):
		return resource_node_list[index];
		
	return null;
	
	
func get_load_resources(index):
	if (load_resources_list.size() > 0):
		return load_resources_list[index];
		
	return null;
	
	
func get_unload_resources(index):
	if (unload_resources_list.size() > 0):
		return unload_resources_list[index];
		
	return null;
	
	
func get_first_resource_node():
	if (resource_node_list.size() > 0):
		return resource_node_list[0];
	
	return null;
	
	
func get_last_resource_node():
	return resource_node_list.back();
	
	
func save_to(savegame):
	savegame.append_dict({
		json = serialize(),
	});
	return;
	
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	deserialize(c["json"], get_tree());
	#savegame.add_child_to_parent(); # setup parenting
	return;
	
	
func is_valid():
	return resource_node_list.size() >= 2;
	
	
#func reset():
#	if (!get_tree().is_network_server()):
#		return;
#
#	if (current_index != 0):
#		rpc("set_current_index_rpc", 0);
#
#	if (destination_index != 0):
#		rpc("set_destination_index_rpc", 0);
		
		
func serialize():
	var vt_path_list = [];
	for vehicle_terminal in resource_node_list:
		vt_path_list.append(vehicle_terminal.get_path());
	
	var dict = {
		vehicle_terminal_path_list = vt_path_list,
		load_resources_list = load_resources_list,
		unload_resources_list = unload_resources_list,
		manifest_resources_list = manifest_resources_list,
		current_index = current_index
	}
	return to_json(dict);
	
	
func deserialize(p_str, tree):
	var dict = parse_json(p_str);
	
	resource_node_list = [];
	var vt_path_list = dict["vehicle_terminal_path_list"];
	for vt_path in vt_path_list:
		var vehicle_terminal = tree.get_root().get_node(vt_path);
		Util.assert(vehicle_terminal, "deserialize failed to get path: " + vt_path);
		resource_node_list.append(vehicle_terminal);
		
	load_resources_list = dict["load_resources_list"];
	unload_resources_list = dict["unload_resources_list"];
	manifest_resources_list = dict["manifest_resources_list"];
	current_index = dict["current_index"];
	return;
	
	
func add(vehicle_terminal, manifest_resources, load_resources, unload_resources):
	resource_node_list.append(vehicle_terminal);
	load_resources_list.append(load_resources);
	unload_resources_list.append(unload_resources);
	manifest_resources_list.append(manifest_resources);
	
	
# ie. max carriages this route will use
func get_max_length():
	var max_load = 0;
	for i in range(0, manifest_resources_list.size()):
		var dict = manifest_resources_list[i];
		var dict_max_load = 0;
		for value in dict.values():
			dict_max_load += value;
			
		max_load = max(max_load, dict_max_load);
		
	return max_load;
