#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Spatial

signal vehicle_state_changed(vehicle);
signal vehicle_display_name_changed(vehicle);

var astar_path = null; # low level path through the astar system (tracks, junctions, stations)
onready var vehicle_route = get_node("VehicleRoute"); # route is the high level list of resource nodes to visit

var trackList = [] # history so carraiges can follow with 0 being the start
var currentTrack = null
var nextTrack = null

var chooseNextTrackDist = 20.0 # how far from end of track before we select the next track

export(int) var maxSpeed = 15 # meters per second - with one carriage
export(int) var minSpeed = 10  # meters per second - when all(max_carriages) cariages aboard
export(float) var full_weight = 1.0;
export(float) var empty_weight = 0.25;
export(int) var max_carriages = 20 # max carriages we can haul

onready var ui_info_panel = BUtil.find_child(self,"Overlay");
onready var ui_name = BUtil.find_child(ui_info_panel,"NameLbl");


# so y/height component represents minSpeed at the bottom, top speed at teh top
# then on the x/width component, on the right we have 1 carriage, on the left we have max_carriages
# currently we have a curve of about 0.5 so as the train gets longer the speed drops with 2-3 carriages
# being the sweet spot
export(CurveTexture) var speed_carriage_curve_value = null; # for storing the texture
onready var speed_carriage_curve = speed_carriage_curve_value.get_curve();

var speed = 0;

var pathOffset = 0
var display_name = "!Express to error land"; # name for display in UI
var engine = null

onready var animation_player = BUtil.find_child(self, "AnimationPlayer")
onready var particles_moving = BUtil.find_child(self, "Particles_Moving")
onready var particles_idle = BUtil.find_child(self, "Particles_Idle")
onready var particles_attention = BUtil.find_child(self, "Particles_Attention")
onready var exit_vehicle_terminal_audio_pool = BUtil.find_child(self, "Engine/ExitVehicleTerminal_AudioPool");
onready var enter_vehicle_terminal_audio_pool = BUtil.find_child(self, "Engine/EnterVehicleTerminal_AudioPool");
onready var moving_audio_pool = BUtil.find_child(self, "Engine/Moving_AudioPool");
onready var waiting_audio_pool = BUtil.find_child(self, "Engine/Waiting_AudioPool");

export(bool) var occupiesTrack = true; # used for aeroplanes which do not book the track

# when checking for the next track to select, assume they all have a train on them
# and so are blocked, at some stage we wil be able to move forwards
# but a longer track might allow us to move up while a shorter track cannot
# we this is a weight difference from the shorted weighted route that we will allow
# for checking if we can move up
export(float) var move_ahead_weight_max = 200.0

export(float) var astar_waiting_weight = 200.0 # the weight added to path finding for a train blocking/sitting on the track
export(float) var astar_moving_weight = 100.0 # the weight added to path finding for a train moving on the track

export(float) var length = 8.0 # how long is it to the first carraige

var unload_timer = Util.create_timer(self, "_unload_carriage", 1.0, false, true);
var load_timer = Util.create_timer(self, "_load_carriage", 1.0, false, true);

onready var finances = get_node("Finances")

var isPaused = false

var company = null

const WAITING_TO_SPAWN_ON_TRACK = 0
const MOVING = 1
const WAITING = 2
const UNLOADING = 3
const LOADING = 4

var state_names = ["WAITING_TO_SPAWN_ON_TRACK", "MOVING", "WAITING", "UNLOADING", "LOADING"];
var state = WAITING_TO_SPAWN_ON_TRACK
var waitTimer = 0
var timeWaiting = 0

const TRACK_AHEAD_BLOCKED_WAIT_TIME = 2.0

onready var debug_panel = BUtil.find_first_child_by_class_name(self, "DebugTextWidget");

export(float) var pre_unload_time = 1.0 # how long the train takes before it starts loading carriages
export(float) var post_load_time = 1.0 # how long the train takes before departing

onready var start_unload_vehicle_carriage_timer = Util.create_timer(self, "_start_unload_vehicle_carriage", 1.0, false, true);
onready var start_load_vehicle_carriage_timer = Util.create_timer(self, "_start_load_vehicle_carriage", 1.0, false, true);
onready var exit_vehicle_terminal_timer = Util.create_timer(self, "_exit_vehicle_terminal", 1.0, false, true);

var is_building_template = false;

var editor_state_name = "TrainEditorState";

onready var world = WorldMgr.get_world(self); 


func _ready():
	if (!world):
		return;
		
	engine = BUtil.find_child(self, "Engine")
	Util.assert(engine, "Couldnt find child 'Engine'");

	if (is_building_template):
		Util.call_deferred("remove_from_group_and_all_children", self, "persist");
		set_process(false);
	else:
		set_process(true);
		add_to_group("persist");
	
		if ($SelectEffect):
			$SelectEffect.prepare(find_node("train-LOD0"), global_transform); # TODO: move this to an editable field
	return;
	
#func _post_ready():
#	if (is_building_template):
#		remove_from_group("persist");
#
#		# ensure children do not save themselves
#		var node_list = BUtil.find_children_by_class_name(self, "Node");
#		for node in node_list:
#			if (node.is_in_group("persist")):
#				node.remove_from_group("persist");
#
#	return;

# this is called if we want to configure the prop as a template
# to allow us to deferentiate what is being built vs a built structure
func set_as_building_template():
	is_building_template = true;
	return;


# speed is determined by the number of visible (ie. active) carriages
func update_speed():
	var carriage_count = 0;
	for c in get_vehicle_carriages():
		if (c.amount == 0):
			carriage_count += empty_weight;
		else:
			carriage_count += full_weight;

	var carriage_percent = float(carriage_count) / float(max_carriages);
	var speed_scale = speed_carriage_curve.interpolate_baked(carriage_percent);
	speed = minSpeed + (maxSpeed - minSpeed) * speed_scale;
	return;


func get_vehicle_route():
	return vehicle_route;
	
func get_astar_weight():
	if (state == MOVING):
		return astar_moving_weight;
	else:
		return astar_waiting_weight;
	
func _month_changed(monthIndex):
	var expense = world.date_time.yearlyToMonthly(finances.get_maintenance_cost());
	company.pay_cash(expense, IncomeStatement.VEHICLES);
	finances.add_expense(expense, true);
	
# get the current manifest of this vehicle
func get_current_resource_list():
	var vehicle_carriages = get_vehicle_carriages();
	var results = [];
	for vehicle_carriage in vehicle_carriages:
		results.append(vehicle_carriage.resource_name);
		
	return results;
	
remotesync func _enter_vehicle_terminal_rpc():
	vehicle_route.arrive_at_destination();
	
	# chance to play
	enter_vehicle_terminal_audio_pool.trigger();
	
	# clear the route so train will not move
	isPaused = true;
	set_state(UNLOADING);
	
	if (!get_tree().is_network_server()):
		return;
	
	start_unload_vehicle_carriage_timer.disconnect("timeout", self, "_start_unload_vehicle_carriage");
	start_unload_vehicle_carriage_timer.connect("timeout", self, "_start_unload_vehicle_carriage", [get_vehicle_carriages().duplicate(), vehicle_route.get_current_unload_resources().duplicate()]);
	start_unload_vehicle_carriage_timer.set_wait_time(pre_unload_time);
	start_unload_vehicle_carriage_timer.start();
	return;
	
func _start_unload_vehicle_carriage(remaining_carriages, remaining_unload_resources):
	assert(get_tree().is_network_server());

	# if out of carriages to load OR out of resources to load into carriages
	# we are done!
	if (remaining_carriages.size() <= 0 || remaining_unload_resources.size() <= 0):
		#Util.assert(remaining_unload_resources.size() == 0, "Something hasn't been unloaded?");
		rpc("_unload_complete_rpc");
		return;
		
	# get next carriage
	var vehicle_carriage = remaining_carriages.pop_back(); # go from back to front when unloading
	if (!vehicle_carriage):
		BLog.debug("No carriage, what happened?");
	
	# carriage already loaded with some product we are meant to unload, so unload it!
	if (vehicle_carriage && remaining_unload_resources.has(vehicle_carriage.resource_name)):
		Util.increment_count_in_dictionary(remaining_unload_resources, vehicle_carriage.resource_name, -1, true);
		#_start_load_vehicle_carriage(remaining_carriages, remaining_unload_resources, index + 1);
		
		var unload_time = vehicle_carriage.unload_resource();
		start_unload_vehicle_carriage_timer.disconnect("timeout", self, "_start_unload_vehicle_carriage");
		start_unload_vehicle_carriage_timer.connect("timeout", self, "_start_unload_vehicle_carriage", [remaining_carriages, remaining_unload_resources]);
		start_unload_vehicle_carriage_timer.set_wait_time(unload_time);
		start_unload_vehicle_carriage_timer.start();
		return;
		
	# not meant to unload this carriage, ignore it
	_start_unload_vehicle_carriage(remaining_carriages, remaining_unload_resources);
	return;
	
remotesync func _unload_complete_rpc(): 
	set_state(LOADING);
		
	if (!get_tree().is_network_server()):
		return;
		
	_start_load_vehicle_carriage(get_vehicle_carriages().duplicate(), vehicle_route.get_current_manifest_resources().duplicate(), vehicle_route.get_current_load_resources().duplicate(), 0);
	return;

func get_expected_resource_for_carriage_index(p_manifest, p_index):
	var idx = 0;
	for resource in p_manifest:
		var resource_count = p_manifest[resource];
		idx += resource_count;
		if (idx > p_index):
			return resource;
			
	return null;
	
# index here is used to position the carriage in alphabetical order
func _start_load_vehicle_carriage(remaining_carriages, manifest_resources, remaining_load_resources, index):
	assert(get_tree().is_network_server());
	
	var expected_resource_name = get_expected_resource_for_carriage_index(manifest_resources, index);
		
	# if out of carriages to load OR out of resources to load into carriages
	# we are done!
	if (!expected_resource_name || remaining_carriages.size() <= 0 || remaining_load_resources.size() <= 0):
		#Util.assert(remaining_load_resources.size() == 0, "Something hasn't been loaded?");
		rpc("_load_complete_rpc");
		return;
	
	# if any carriages exist with the expected_resource_name
	# bring them forward to where the manifest wants them
	var last_empty_carriage = null;
	var last_empty_carriage_index = -1;
	for ci in range(0, remaining_carriages.size()):
		var carriage = remaining_carriages[ci];
		if (carriage.resource_name == null):
			last_empty_carriage = carriage;
			last_empty_carriage_index = ci;
			
		if (carriage.resource_name == expected_resource_name):
			remaining_carriages.remove(ci);
			carriage.load_resource(expected_resource_name, index);
			_start_load_vehicle_carriage(remaining_carriages, manifest_resources, remaining_load_resources, index + 1);
			return;
		
	var vehicle_carriage = null;
	
	# next carriage is not empty...
	var next_carriage = remaining_carriages.front();
	if (next_carriage.resource_name != null):
		# ...and its not expected to be in this slot according to the manifest
		# we need to bring an empty carriage forwards
		if (next_carriage.resource_name != expected_resource_name):
			remaining_carriages.remove(last_empty_carriage_index);
			vehicle_carriage = last_empty_carriage;
			
		# ...and carriage already loaded with expected product
		# (will happen when we picked up some of logs from multiple stations for example)
		# skip to the next index
		elif (vehicle_carriage.resource_name == expected_resource_name):
			vehicle_carriage = remaining_carriages.pop_front();
			_start_load_vehicle_carriage(remaining_carriages, manifest_resources, remaining_load_resources, index + 1);
			return;
			
	# next carriage is empty
	else:
		vehicle_carriage = remaining_carriages.pop_front();
		
	#
	# here we have an empty carriage we can load something
	#
	var can_load = vehicle_carriage.can_load_resource(expected_resource_name);
	var more_to_load = remaining_load_resources.has(expected_resource_name);
	if (can_load && more_to_load):
		# load it!
		Util.increment_count_in_dictionary(remaining_load_resources, expected_resource_name, -1, true);
		
		var load_time = vehicle_carriage.load_resource(expected_resource_name, index);
		start_load_vehicle_carriage_timer.disconnect("timeout", self, "_start_load_vehicle_carriage");
		start_load_vehicle_carriage_timer.connect("timeout", self, "_start_load_vehicle_carriage", [remaining_carriages, manifest_resources, remaining_load_resources, index + 1]);
		start_load_vehicle_carriage_timer.set_wait_time(load_time);
		start_load_vehicle_carriage_timer.start();
		
	# we can't load it, for example the resource node doesn't have enough for us to take
	# here we leave an empty carriage
	else:
		vehicle_carriage.load_resource(null, index); # just to move the carriage
		_start_load_vehicle_carriage(remaining_carriages, manifest_resources, remaining_load_resources, index + 1);
		
	return;
	
remotesync func _load_complete_rpc():
	if (!get_tree().is_network_server()):
		return;
		
	# else, all done, tget ready to leave the station!
	exit_vehicle_terminal_timer.set_wait_time(post_load_time);
	exit_vehicle_terminal_timer.start();
	return;

func _exit_vehicle_terminal():
	Util.assert(get_tree().is_network_server(), "_exit_vehicle_terminal only to be run on the server");
	rpc("_exit_vehicle_terminal_rpc");
	
remotesync func _exit_vehicle_terminal_rpc():
	isPaused = false;
	set_state(WAITING);
	update_speed();
	vehicle_route.progress_to_next_destination();
	
	# chance to play when leaving station
	exit_vehicle_terminal_audio_pool.trigger();
	return;


func update_debug_panel(force = false):
	if (!debug_panel or (!debug_panel.is_visible() and !force)):
		return;
		
	debug_panel.clear();
	debug_panel.add_line("[color=yellow]" + get_name());
	debug_panel.add_line("state: " + state_names[state]);
	
	if (waitTimer > 0):
		debug_panel.add_line("waitTimer: " + str(waitTimer));
		
	if (currentTrack and currentTrack.get_ref()):
		debug_panel.add_line("currentTrack: " + currentTrack.get_ref().get_name());
		
	debug_panel.add_line("pathOffset: " + str(pathOffset));
	
	if (nextTrack and nextTrack.get_ref()):
		debug_panel.add_line("nextTrack: " + nextTrack.get_ref().get_name());
		
	#debug_panel.add_line("[\\color]");
	return;
	
func _process_ui():
	if (!ui_info_panel.is_visible()):
		return
		
	var camera = get_viewport().get_camera()
	if (!camera):
		return;
	
	var screenPos = camera.unproject_position(get_follow_node_position());
	ui_info_panel.set_position(screenPos);
	return;
	
func _trySpawnOnTrack():
	var spawned = false;
	
	# set route to head off to the first stop, this first bit will only work if there is a loop
	# from start to end terminal, so it ensures we spawn on the correct loop
	vehicle_route.reset_for_vehicle_spawn();
	var start_terminals = vehicle_route.get_current_resource_node().get_terminals();
	var end_terminals = vehicle_route.get_destination_resource_node().get_terminals();
	var astar_paths = find_astar_paths(start_terminals, end_terminals);
	for astar_path in astar_paths:
		var nextTrack = astar_path.find_last_track();
		Util.assert(nextTrack, "nextTrack null");
		Util.assert(nextTrack != currentTrack, "nextTrack equals currentTrack");
		if (nextTrack && nextTrack.get_occupant() == null):
			var path_length = nextTrack.get_world_curve().get_baked_length();
			move_to_track(nextTrack, path_length);
			rpc("_enter_vehicle_terminal_rpc");
			spawned = true;
			break;
			
	if (spawned):
		set_visible(true);
		return true;
		
	# if we get here, there is only a segment from start terminal to end terminal - no loop
	# so just spawn where we can
	vehicle_route.arrive_at_destination();
	vehicle_route.progress_to_next_destination();
	
	start_terminals = vehicle_route.get_current_resource_node().get_terminals();
	end_terminals = vehicle_route.get_destination_resource_node().get_terminals();
	
	astar_paths = find_astar_paths(start_terminals, end_terminals);
	for astar_path in astar_paths:
		# TODO: ideally here, we should back up to the previous track - the track that belongs to the station
		var nextTrack = astar_path.find_first_track();
		if (nextTrack && nextTrack.get_occupant() == null):
			move_to_track(nextTrack, 0);
			rpc("_enter_vehicle_terminal_rpc");
			spawned = true;
			break;
		
	if (spawned):
		set_visible(true);
		return true;
		
	# failed to spawn
	return false;
	
	
func _process(delta):
	if (!vehicle_route || !vehicle_route.is_valid()):
		return
		
	timeWaiting += delta
	
	update_debug_panel();
	_process_ui();
	
	# train paused, do nothing
	if (isPaused):
		return
		
	# waiting for a period of time
	if (waitTimer > 0):
		waitTimer = max(0, waitTimer - delta)
		return
		
	# wait till there is a free moment to spawn on the track
	if (get_tree().is_network_server() && state == WAITING_TO_SPAWN_ON_TRACK):
		if (!_trySpawnOnTrack()):
			return;
		
	
	# ah we just spawned, waiting for server to let us know where to go next
	# or waiting to spawn
	if (currentTrack == null):
		if (!nextTrack):
			if (get_tree().is_network_server() && state == WAITING_TO_SPAWN_ON_TRACK):
				waitTimer = TRACK_AHEAD_BLOCKED_WAIT_TIME
			return
		currentTrack = nextTrack
		nextTrack = null
	
	var track = currentTrack.get_ref() 
	# ops, its been deleted! are dead in the water!
	# go back to spawn state
	if (!track):
		set_state(WAITING);
		set_visible(false);
		if (get_tree().is_network_server()):
			set_state(WAITING_TO_SPAWN_ON_TRACK);
		return
		
	var pathLength = track.get_world_curve().get_baked_length()	
	var destinationAtEndOfCurrentTrack = vehicle_route.get_destination_resource_node().get_terminals().has(track.get_station());
		
	# check if we are moving into the next track, look for which track to go too
	if (pathOffset > (pathLength - chooseNextTrackDist)):
		if (!destinationAtEndOfCurrentTrack && nextTrack == null):
			# look for a route from the junction ahead to the destination station
			if (get_tree().is_network_server()):
				find_next_track();

			# pause here until a nextTrack is set
			if (nextTrack == null || nextTrack.get_ref() == null):
				if (get_tree().is_network_server()):
					waitTimer = TRACK_AHEAD_BLOCKED_WAIT_TIME;
					
				set_state(WAITING);
				return
			
	# move the train forwards if not loading or unloading
	if (state != LOADING && state != UNLOADING):
		pathOffset += delta * speed
		set_state(MOVING);
		timeWaiting = 0.0
	
	# move on to the next track
	if (pathOffset > pathLength):
		# we reached our destination
		if (destinationAtEndOfCurrentTrack):
			if (get_tree().is_network_server()):
				# get ready for the next station we are working towards
				rpc("_enter_vehicle_terminal_rpc");
				
			return
			
		trackList.push_front(currentTrack)
		currentTrack = nextTrack
		nextTrack = null
		pathOffset = 0
			
	position_on_current_track_at_offset();
	return;
	
func find_astar_paths(start_terminals, end_terminals):
	var results = [];
	for st in start_terminals:
		for et in end_terminals:
			var route = st.find_astar_paths(et);
			for r in route:
				results.append(r);
			
	return results;
		
func astar_dict_array_sort(a, b):
	return a["weight"] < b["weight"];

func can_move_to_track(p_track):
	return !p_track.is_occupied() && !p_track.get_start_junction().is_occupied();
	
# this should only be run on the server
func find_next_track():
	var track = currentTrack.get_ref() 
	if (!track):
		return
		
	# as an optimisation, we don't need to do any path finding if
	# this is a single track ahead of us!
	var outgoingTracks = track.getOutgoingTracks();
	if (outgoingTracks.size() == 1):
		var nextTrack = outgoingTracks[0];
		Util.assert(nextTrack != currentTrack, "Trying to change to current track!?");
		if (can_move_to_track(nextTrack)):
			rpc("set_next_track_rpc", nextTrack.get_path());
			
		# done here!
		return;
	
	
#	# now check the astar path to see if we can just continue on (unless it has a loop as it might get stuck!)
#	if (astar_path != null && !astar_path.has_loop()):
#		var nextTrack = astar_path.find_next_track(track);
#		if (nextTrack):
#			Util.assert(nextTrack != currentTrack, "Plz don't get stuck in loops!");
#			if (can_move_to_track(nextTrack)):
#				rpc("set_next_track_rpc", nextTrack.get_path());
#				return;
		
	astar_path = null;
	
	# the above optimisations have failed, we need to look for a new route
	# if this is still an issue, we may need to thread it?
	var astar_paths = track.find_astar_paths_to_terminals(vehicle_route.get_destination_resource_node().get_terminals());
	
	# no possible routes
	if (astar_paths.size() <= 0):
		return;
		
	var astar_dict_array = [];
	for apath in astar_paths:
		var weight = apath.get_weight();
		
		# loops are bad, but the user might use a loop as a method to squash trains up
		if (apath.has_loop()):
			weight += 1000;
			
		astar_dict_array.append({"weight" : weight, "astar_path" : apath});
		
		
		
	astar_dict_array.sort_custom(self, "astar_dict_array_sort");
	
	# choosing the shortest weighted route may not always be ideal
	# if there is a track slightly longer that will allow us to move forwards
	# this is a better choice, as it will allow trains behind us to pack
	# tightly together
	var best_weight = astar_dict_array[0]["weight"];
	var best_astar_path = astar_dict_array[0]["astar_path"];
	var max_weight = best_weight + move_ahead_weight_max;
	for adict in astar_dict_array:
		if (adict["weight"] > max_weight):
			break;
				
		if (!adict["astar_path"].find_first_track().is_occupied()):
			best_weight = adict["weight"];
			best_astar_path = adict["astar_path"];
			break;
			
		
		
	# this could be a hefty cost just to 
#	var best_astar_path = null;
#	var lowest_weight = -1;
#	astar_paths.sort_custom(self, "astar_sort");
#
#	for apath in astar_paths:
#		var weight = apath.get_weight();
#		#var weight = apath.get_weight_to_first_blockage();
#		if (weight_to_fist_blockage < lowest_weight or lowest_weight == -1):
#			best_astar_path = apath;
#			lowest_weight = weight;
#
	# found no valid path
	if (!best_astar_path):
		return;
		
	# just ensure that the track we are switching over too is connected correctly
	# to the junction, this is just an assertion test
	var nextTrack = best_astar_path.find_first_track();
	Util.assert(nextTrack != currentTrack, "Next Track and Current track are the same!");
	var next_track_found = false;
	for outgoing_track in track.get_junction(track.END).getOutgoingTracks():
		if (outgoing_track == nextTrack):
			next_track_found = true;
			break;
			
	if (!next_track_found):
		Util.assert(next_track_found, "Train is going to warp!?");
		
	Util.assert(track != nextTrack, "NO more loopin!");
	if (can_move_to_track(nextTrack)):
		astar_path = best_astar_path;
		rpc("set_next_track_rpc", nextTrack.get_path());
		return;
		
	# failure!
	return;
		
# this looks to be causing micro stuttering every so often
# might need to move Util getTransformFromCurve and other util code to c++
func position_on_current_track_at_offset():
	var trackIdx = 0

	var track = currentTrack.get_ref()
	if (!track):
		return
		
	var scale2 = engine.get_scale()
	var transform2 = track.get_transform_from_distance_along_curve(pathOffset);
	engine.set_transform(transform2)
	engine.set_scale(scale2)
	
	if ($SelectEffect):
		$SelectEffect.set_global_transform(engine.global_transform);
	
	# position the carriages
	var carriageOffset = pathOffset - length
	var vehicle_carriages = get_vehicle_carriages();
	for i in range(0, vehicle_carriages.size()):
		var c = vehicle_carriages[i];
		
		# ops, need to go the previous curve and place carriages along there!
		while (carriageOffset < 0.0):
			# no history left, just squash the carriages up!
			if (trackIdx >= trackList.size()):
				carriageOffset = 0
				break
				
			var t = trackList[trackIdx].get_ref()
			if (t):
				track = t
				carriageOffset = t.get_world_curve().get_baked_length() + carriageOffset # carriageOffset is negative so this is really correct
				
			trackIdx += 1
			
		var carriageScale = c.get_scale()
		var carriageTransform = track.get_transform_from_distance_along_curve(carriageOffset); #Util.getTransformFromCurve(track.getCurve(), carriageOffset, pathLookahead)
		c.set_transform(carriageTransform)
		c.set_scale(carriageScale)
		carriageOffset -= c.length
		
		# last carriage frees up junctions we have left behind
		if (track && (i == (vehicle_carriages.size() - 1)) && carriageOffset > chooseNextTrackDist && track.get_start_junction().get_occupant() == self):
			track.get_start_junction().clear_occupant_if(self);
		
	# special case for no carriages
	if (track && vehicle_carriages.size() == 0 && pathOffset > chooseNextTrackDist && track.get_start_junction().get_occupant() == self):
		track.get_start_junction().clear_occupant_if(self);
		
	#
	# free any unused curves
	#
	while (trackList.size() > trackIdx):
		# clear our occupancy
		# but we need to check we are not backing out of a station for example
		# were we are reversing over ourself, this would free an occupancy that should not be free'd!
		var t = trackList[trackList.size() - 1].get_ref()
#		var trackInUseEarlier = false
#		for i in range(0, trackIdx + 1):
#			if (t == trackList[i].get_ref()):
#				trackInUseEarlier = true
#				break
				
#		if (!trackInUseEarlier && t && t.getOccupiedBy() == self):
		if (t != null):
			t.clear_occupant_if(self);
			t.get_start_junction().clear_occupant_if(self);
			
		trackList.pop_back()
		
func get_finances():
	return finances;
	
func set_display_name(vehicle_display_name):
	rpc("set_display_name_rpc", vehicle_display_name);
	
remotesync func set_display_name_rpc(vehicle_display_name):
	display_name = vehicle_display_name;
	ui_name.set_text(display_name);
	emit_signal("vehicle_display_name_changed", self);
	
# set the vehicles route, this needs to be called on all clients
# by the UI that calls this
func set_vehicle_route(p_vehicle_route_ser, p_vehicle_carriage_instance):
	var vehicle_carriage_instance = p_vehicle_carriage_instance
	# set the new route, but try to resume from where we are
	var cur = vehicle_route.get_current_resource_node();
	var dest = vehicle_route.get_destination_resource_node();
	vehicle_route.deserialize(p_vehicle_route_ser, get_tree());
	vehicle_route.set_current_resource_node(cur);
	vehicle_route.set_destination_resource_node(dest);
	
	# allocate or free carriages as appropriate
	var vehicle_carriages = get_vehicle_carriages();
	var current_max_carriages = vehicle_carriages.size();
	var max_carriages = vehicle_route.get_max_length();
	
	if (max_carriages > current_max_carriages):
		for i in range(current_max_carriages, max_carriages):
			spawn_vehicle_carriage(vehicle_carriage_instance, i);
	elif (max_carriages < current_max_carriages):
		# TODO: this deletes carriages from the end of the train
		# which is a problem when the user deletes carriages from the start or 
		# end leaving a product on the train that will never be unloaded!
		for i in range(max_carriages, current_max_carriages):
			var free_carriage = vehicle_carriages.back();
			vehicle_carriages.pop_back();
			free_carriage.delete();
			
	update_speed();
	return;

	
func spawn(p_company, p_vehicle_route, vehicle_carriage_instance, vehicle_display_name, p_editor_state_name):
	
	if (!is_building_template):
		add_to_group("vehicles");
	
	display_name = vehicle_display_name;
	editor_state_name = p_editor_state_name;
	
	ui_name.set_text(display_name);
	ui_info_panel.set_visible(false);
	
	if (p_vehicle_route):
		vehicle_route.copy(p_vehicle_route);
		Util.assert(vehicle_route.is_valid(), "Invalid vehicle route");
	
	# spawn the carriages
	var max_carriages = vehicle_route.get_max_length();
	for i in range(0, max_carriages):
		spawn_vehicle_carriage(vehicle_carriage_instance, i);
	
	# add the purchase cost to the expenses
	finances.add_expense(finances.get_purchase_cost(), true);
	#for c in get_vehicle_carriages():
	#	c.finances.add_expense(c.finances.get_purchase_cost(), true);
	
	set_state(WAITING);
	if (get_tree().is_network_server()):
		set_state(WAITING_TO_SPAWN_ON_TRACK);
		world.date_time.connect("month_changed", self, "_month_changed");
	
	update_speed();
	company = p_company;

	# init particles
	particles_moving.set_enabled(false);
	particles_idle.set_enabled(true);
	particles_attention.set_enabled(false);
	return;
	
# called when a track a train is on is subdivided by TrackBuilder
func move_to_track(track, offset):
	Util.assert(get_tree().is_network_server(), "move_to_track only to run on the server");
	if (!get_tree().is_network_server()):
		return;

	Util.assert(!track.is_occupied(), "Track trying to move to is occupied");
	Util.assert(!track.get_start_junction().is_occupied(), "Trying to move to track when junction is occupied");
	
	Util.assert(can_move_to_track(track), "Trying to move to a track that we can't move to");
	rpc("set_next_track_rpc", track.get_path(), offset, true);
	return;
	
# set the current track and occupy
remotesync func set_next_track_rpc(trackPath, offset = null, make_current = false):
	if (offset):
		pathOffset = offset;
		
	var track = get_tree().get_root().get_node(trackPath);

	# clear our next track occupancy
	if (nextTrack != null && nextTrack.get_ref()):
		nextTrack.get_ref().clear_occupant_if(self);
		nextTrack.get_ref().get_start_junction().clear_occupant_if(self);
		
	Util.assert(track, "Bad track");
	if (occupiesTrack):
		track.set_occupant(self);
		track.get_start_junction().set_occupant(self);
		
	nextTrack = weakref(track);
	Util.assert(nextTrack != currentTrack, "Oh man!, more loops!");
			
	# this happens when we just spawn normally
	if (currentTrack == null or make_current):
		if (currentTrack and currentTrack.get_ref()):
			currentTrack.get_ref().clear_occupant_if(self);
			currentTrack.get_ref().get_start_junction().clear_occupant_if(self);
			currentTrack = null;
			
		currentTrack = nextTrack;
		nextTrack = null;
		
	return;
	
func spawn_vehicle_carriage(vehicle_carriage_instance, index):
	var vehicle_carriage = vehicle_carriage_instance.duplicate();
	vehicle_carriage.set_visible(true);
	vehicle_carriage.init(self);
	vehicle_carriage.set_name(get_name() + "_vehicle_carriage_" + str(index));
	add_child(vehicle_carriage);
	return vehicle_carriage;


func get_vehicle_carriages():
	return BUtil.find_children_by_class_name(self, "TrainCarriage", true);
	
	
# TODO: move this to a handler VehicleHandler.gd?
func is_interactive(controller):
	return true;
	
# TODO: move this to a handler VehicleHandler.gd?
func set_selected(controller, selected):
	if (selected):
		controller.camera_set_follow_node(self);
	else:
		if (controller.camera_get_follow_node() == self):
			controller.camera_set_follow_node(null);
						
	return true;

	
# get the position of this object for the camera to focus on
func get_follow_node_position():
	return $Engine.transform.origin;
	
# TODO: move this to a handler VehicleHandler.gd?
func set_mouse_over(controller, mouseOver):
	if ($SelectEffect):
		$SelectEffect.apply_to(self, mouseOver);
		
	ui_info_panel.set_visible(mouseOver)
		
#	if (mouseOver):
#		get_node("Engine").set_scale(Vector3(1.1, 1.1, 1.1))		
#		for c in get_vehicle_carriages():
#			c.set_scale(Vector3(1.1, 1.1, 1.1))
#	else:
#		get_node("Engine").set_scale(Vector3(1.0, 1.0, 1.0))
#		for c in get_vehicle_carriages():
#			c.set_scale(Vector3(1.0, 1.0, 1.0))
	return;
	
func delete():
	rpc("delete_master_rpc");
	
master func delete_master_rpc():
	if (!get_tree().is_network_server()):
		return;
	
	# apply refund for the deletion
	var refund = get_finances().get_refund_amount();
	
	# compute refund for carriages sold
	for c in get_vehicle_carriages():
		refund += c.get_finances().get_refund_amount();
		
	company.receive_cash(refund, IncomeStatement.VEHICLES);
		
	rpc("delete_rpc");
	
remotesync func delete_rpc():
	for c in get_vehicle_carriages():
		c.delete()
		
	# clear our track occupancy
	if (nextTrack != null && nextTrack.get_ref()):
		nextTrack.get_ref().clear_occupant_if(self);
		nextTrack.get_ref().get_start_junction().clear_occupant_if(self);
		
	if (currentTrack != null && currentTrack.get_ref()):
		currentTrack.get_ref().clear_occupant_if(self);
		currentTrack.get_ref().get_start_junction().clear_occupant_if(self);
		
	for t in trackList:
		if (t != null && t.get_ref()):
			t.get_ref().clear_occupant_if(self);
			t.get_ref().get_start_junction().clear_occupant_if(self);
			
	if ($SelectEffect):
		$SelectEffect.release();
	
	queue_free();
	
	# event is after queue_free so we can detect deleted train via is_queued_for_deletion
	EventMgr.trigger(EventMgr.Id.VEHICLE_DELETED, { "sender": self, "vehicle": self });
	
	
func get_display_name():
	return display_name;
	
# display where the train is, once its on the move, display where its going
func get_current_or_destination_name_based_on_state():
	if (state == WAITING || state == MOVING):
		var destResourceNode = vehicle_route.get_destination_resource_node();
		return destResourceNode.name
		
	var currentName = vehicle_route.get_current_resource_node().name
	return currentName;
	
func save_to(savegame):
	if (!company):
		return false;
		
	# TODO:save route to file
	#vehicle_route.save_to(savegame);
#
#	var stations_to_visit = [];
#	for station in stationsToVisit:
#		stations_to_visit.append(station.get_path());
		
	
	var carriages = get_vehicle_carriages();
	
	savegame.append_dict({
		company = company.get_path(),
		currentTrack = currentTrack.get_ref().get_path() if (currentTrack and currentTrack.get_ref()) else NodePath(),
		#vehicle_route = stations_to_visit, # TODO: fix me, broken in TT-221
		vehicle_carriage_instance_name = carriages[0].get_filename(),
		state = state_names[state], # convert to human readable state
		#current_vehicle_terminal = current_vehicle_terminal.get_path() if (current_vehicle_terminal) else NodePath() # TODO: fix me, broken in TT-221
	});
	
	savegame.append_builtins({
		pathOffset = pathOffset,
		display_name = display_name,
		editor_state_name = editor_state_name,
		#destinationIndex = destinationIndex, # TODO: fix me, broken in TT-221
	});
	
	set_state(state);
	return true;
	
func load_from(savegame):
	var c = savegame.get_current_node_dict();
	company = savegame.resolve_node(c["company"]);
	var vehicle_carriage_instance_name = c["vehicle_carriage_instance_name"];
	var vehicle_carriage_instance = load(vehicle_carriage_instance_name).instance();
	
	var track = savegame.resolve_node(c["currentTrack"]);
	
	# TODO: load route from savegame?
#	var stations_to_visit = c["stations_to_visit"];
#	for station_name in stations_to_visit:
#		stationsToVisit.append(savegame.resolve_node(station_name));
	
	savegame.load_builtins();
	savegame.resolve_parent().add_child(self); # setup parenting
		
	#spawn(company, null); #stationsToVisit);
	spawn(company, null, vehicle_carriage_instance, display_name, editor_state_name);
	move_to_track(track, pathOffset);
	
	
	# TODO: broken by TT-221
	#if (c["current_vehicle_terminal"]):
	#	current_vehicle_terminal = savegame.resolve_node(c["current_vehicle_terminal"]);
		
	# convert human readable state to an index
	var state_name = c["state"];
	state = 0;
	for sn in state_names:
		if sn == state_name:
			break;
			
		state += 1;
		
	set_state(state);		
	return true;
	
# display the screen to edit this vehicle's route - TODO: move this to a handler VehicleHandler.gd?
func show_vehicle_edit_ui(p_flags):
	var master_pc = company.get_master_player();
	if (master_pc):
		master_pc.show_vehicle_edit_ui(self, editor_state_name, p_flags);
		
func set_state(p_state):
	if (p_state == state):
		return;
		
	state = p_state;
	_process_state_change();
	emit_signal("vehicle_state_changed", self);
	
	
# process state change data here
func _process_state_change():
	if (state == MOVING):
		moving_audio_pool.trigger();
		waiting_audio_pool.stop();
		particles_attention.set_enabled(false);
		particles_moving.set_enabled(true);
		particles_idle.set_enabled(false);
		animation_player.play("Moving");
	elif (state == WAITING):
		moving_audio_pool.stop();
		waiting_audio_pool.trigger();
		particles_moving.set_enabled(false);
		particles_idle.set_enabled(false);
		particles_attention.set_enabled(true);
		animation_player.play("Attention")
	elif (state == LOADING):
		particles_moving.set_enabled(false);
		particles_attention.set_enabled(false);
		particles_idle.set_enabled(true);
		animation_player.play("Idle");
	elif (state == UNLOADING):
		particles_attention.set_enabled(false);
		particles_moving.set_enabled(false);
		particles_idle.set_enabled(true);
		animation_player.play("Idle");
	return;
	
	
func get_current_track():
	return currentTrack.get_ref();


func set_company(p_company):
	company = p_company;
	
	
func get_company():
	return company;
	
	
func set_visible(p_visible):
	.set_visible(p_visible);
	#$Engine/KinematicBody.set_enabled(p_visible); # this isn't working with set_mouse_over, but we don't need it for now
