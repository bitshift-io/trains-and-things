#
# Dump stuff stuff about the map in testing AI
#
extends Node

onready var world = WorldMgr.get_world(self); 

func _ready():
	# only dump map in developer mode
	if (BDatabase.is_build_type("developer") or BDatabase.get_value("dev/developer", 0) == 1):
		call_deferred("dump_map")
	
func get_type_str(rn):
	var type_str;
	match(rn.type):
		rn.TYPE.PRIMARY_RESOURCE: type_str = "PrimaryResource"; 
		rn.TYPE.INTERMEDIATE_RESOURCE: type_str = "IntermediateResource"; 
		rn.TYPE.CITY: type_str = "City"; 
		
	return type_str;
			
func dump_map():
	if (!world):
		return;
		
	# things to dump:
	# for each ResourceNode
	var rn_data = []
	var resource_nodes = get_tree().get_nodes_in_group("resource_nodes")
	for rn in resource_nodes:
		var name = rn.name;
		var pos = rn.get_translation();
		var resource_data_list = rn.get_resource_data_list();
		var type = rn.type;
		var type_str = get_type_str(rn);
		
		match(type):
			rn.PRIMARY_RESOURCE: type_str = "PrimaryResource"; 
			rn.INTERMEDIATE_RESOURCE: type_str = "IntermediateResource"; 
			rn.CITY: type_str = "City"; 
		
		var rd_data = []
		for rd in resource_data_list:
			var r = rd;
			var market_resource = rd.market_resource;
			if (!market_resource):
				continue;
				
			var value = market_resource.value;
			var res_name = market_resource.name;
			var display_name = market_resource.display_name;
			var is_demanded = rd.isDemanded;
			var is_supplied = rd.isSupplied;
			var supply_rate = rd.supplyRate;
			rd_data.append({
				"resource_name": res_name,
				"is_demanded": is_demanded,
				"is_supplied": is_supplied,
				"supply_rate": supply_rate,
				"value": value
			})
			
		rn_data.append({
			"name": name,
			"pos": pos,
			"resources": rd_data,
			"type": type_str
		})
	
	var data = {
		"resource_nodes": rn_data,
		"routes": compute_routes()
	}
	
	var file = File.new()
	file.open("res://User/map_dump.json", File.WRITE)
	file.store_line(to_json(data))
	file.close()
	return;
	
	
# compute all possible routes
func compute_routes():
	var routes = [];
	var resource_nodes = get_tree().get_nodes_in_group("resource_nodes")
	for rn1 in resource_nodes:
		var resource_data_list_1 = rn1.get_resource_data_list();
		
		for rn2 in resource_nodes:
			if (rn1 == rn2):
				continue;
				
			var resource_data_list_2 = rn2.get_resource_data_list();
			
			for rd1 in resource_data_list_1:
				for rd2 in resource_data_list_2:
					if (rd1.name == rd2.name):
						if (rd1.isSupplied && rd2.isDemanded):
							routes.append({
								"from": {
									"name": rn1.name,
									"type": get_type_str(rn1)
								},
								
								"to": {
									"name": rn2.name,
									"type": get_type_str(rn2)
								},
								
								"distance": rn1.get_translation().distance_to(rn2.get_translation()),
								"resource_name": rd1.name,
							});
							
	return routes;
						
			
				
			
				
			
