#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
# 
# Package mods and maps into separate pck files
#
# run by going:
# godot.exe -s Editor/Package.gd -outdir=/home/fabian/Projects/TrainsAndThings/Build/Full
#

extends SceneTree

# what extensions to include in the pck
var extensions = [
	"tscn",
	"tres",
	"yml",
	"json",
	"png",
	"svg",
	"glb",
	"crt",
	"godot",
	"gd",
	"wav",
	"ogg",
	"po",
	"ttf",
	"shader"
];

var content_ignore_names = [
	"Maps",
	"Mods"
];

func _init():	
	# read command line params
	var outdir = "res://";
	var cmdline = OS.get_cmdline_args();
	print("\nRunning Package.gd with cmd: " + cmdline.join(" ") + "\n");
	
	for cmd in cmdline:
		var splits = cmd.split("=");
		if (splits.size() >= 2):
			if (splits[0] == '-outdir'):
				outdir = splits[1];
	
	# ensure output dirs are created ready for us
	print("making dirs...");
	var dir = Directory.new()
	var err = dir.make_dir_recursive(outdir + "/Maps");
	err = dir.make_dir_recursive(outdir + "/Mods");
	
	print("packaging Content...");
	create_package_from_directory("res://", outdir, "Content", content_ignore_names);
	
	print("packaging maps...");
	package_directories_as_separate_packages("res://Maps", outdir + "/Maps");
	
	print("packaging mods...");
	package_directories_as_separate_packages("res://Mods", outdir + "/Mods");
	
	quit()
	
	
func package_directories_as_separate_packages(path, output_dir):
	var dir = Directory.new()
	var err = dir.open(path);
	if err == OK:
		dir.list_dir_begin();
		var file_name = dir.get_next()
		while (file_name != ""):
			if (file_name.begins_with(".")):
				file_name = dir.get_next();
				continue;
				
			if dir.current_is_dir():
				create_package_from_directory(path, output_dir, file_name);
			else:
				pass;
				
			file_name = dir.get_next()
	else:
		print("An error occurred (" + str(err) + ") when trying to access the path: " + path)
		
func create_package_from_directory(path, output_dir, p_pck_name, p_ignore_names = []):
	var pck = PCKPacker.new();
	var pck_name = output_dir + "/" + p_pck_name + ".pck";
	print("Creating PCK: " + pck_name);
	pck.pck_start(pck_name, 0);
	
	package_directory_recusive(pck, path, p_ignore_names);

	pck.flush(true);
	print("OK\n");
	return;
	
func package_directory_recusive(p_pck, p_dir_path, p_ignore_names):
	var dir = Directory.new();
	if dir.open(p_dir_path) == OK:
		dir.list_dir_begin();
		var file_name = dir.get_next();
		while (file_name != ""):
			if (file_name.begins_with(".")):
				file_name = dir.get_next();
				continue;
				
			var full_path = p_dir_path + "/" + file_name if !p_dir_path.ends_with("/") else p_dir_path + file_name;
			if (p_ignore_names.has(file_name) || p_ignore_names.has(full_path)):
				file_name = dir.get_next();
				continue;
					
			if dir.current_is_dir():
				package_directory_recusive(p_pck, full_path, p_ignore_names);
			else:
				package_file(p_pck, full_path);
				
			file_name = dir.get_next();
			
	return;
	
	
func package_file(pck, file_path):
	# package up tscn with its dependencies
	# if dependencies are inside the Mods or Maps folder
	var extension = file_path.get_extension();
	if (extensions.has(extension)):
		var dependencies = ResourceLoader.get_dependencies(file_path);
		for dep in dependencies:
			if (dep.begins_with("res://Maps") || dep.begins_with("res://Mods")):
				# check the dependency, if it itself has other dependencies!
				var extension2 = dep.get_extension();
				if (extensions.has(extension2)):
					package_file(pck, dep);
				else:
					add_file(pck, dep);
				
		add_file(pck, file_path);
		
	return;
	
func add_file(pck, file_path):
	var import_path = BUtil.import_remap(file_path);
	if (import_path != file_path):
		var dot_import = file_path + ".import";
		add_raw_file(pck, dot_import);
		
	add_raw_file(pck, import_path);
		
func add_raw_file(pck, file_path):
	print("\tAdding file: " + file_path);
	pck.add_file(file_path, file_path);
