#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# In this sample mod we want to do a few things:
#	1) Change the default train model and speed
#	2) Add a new button to the players UI
#	3) Remove some stuff from the players UI
#
extends "res://Plugin/Mod.gd"

export(PackedScene) var train_template;

func _ready():
	# connect this mod so when a player controller is created, we are notified about it
	Game.connect("player_created", self, "_player_created");
	BLog.warning("WARNING Example Mod is enabled!");
	return;

func _player_created(player):
	var train_editor_state = player.sm.get_state_by_name("TrainEditorState");
	
	# I have two options here:
	# 1) trainInst is an instance of the train_editor_state.vehicleTemplate
	# I could change vehicleTemplate and create a new trainInst if I want to load
	# a whole new train from a scene file
	# OR
	# 2) modify the train instance in code to modify it precedurally
	# 
	# I will go with the first option:
	train_editor_state.set_vehicle_template(train_template);
	BLog.debug("Example Mod: modifying train template");
	return;
	
func modify_load_resource_path(owner, resource_path):
	#BLog.debug("Example Mod: About to load resource: " + resource_path);
	return resource_path;
	
func modify_load_resource(owner, resource):
	#BLog.debug("Example Mod: Loading resource: " + resource.get_name());
	return resource;
	
func modify_attachment_list(controller, attachment_resource_list):
	# I want to remove the ResourcePanel from the top bar,
	# modify_attachment_list is called just before the attachments are loaded and attached to the player controller
	# so here is our chance to remove it
	attachment_resource_list.erase("res://Player/ResourcesPanel.tscn");
	BLog.debug("Example Mod: removing res://Player/ResourcesPanel.tscn");
	
	attachment_resource_list.append("res://Mod/Example/Player/ExamplePanel.tscn");
	BLog.debug("Example Mod: adding res://Mod/Example/Player/ExamplePanel.tscn");
	return attachment_resource_list;
