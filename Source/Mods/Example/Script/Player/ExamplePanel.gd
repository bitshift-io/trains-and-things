#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

extends Node

onready var ui_example_dlg = $"ExampleDlg";

func apply_to_controller(controller):
	if (!controller.is_network_master()):
		return;
		
	controller.add_child(self);
	controller.add_left_panel(ui_example_dlg);
	controller.add_dialog_panel_button($"ExampleButton");
	return;
	
func _on_ExampleButton_pressed():
	ui_example_dlg.set_visible(!ui_example_dlg.is_visible());
	return;


func _on_Title_icon_pressed():
	ui_example_dlg.set_visible(!ui_example_dlg.is_visible());
	pass # Replace with function body.
