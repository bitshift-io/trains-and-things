#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

#
# Unit test API
#
extends Node

signal run_completed();

var run_on_ready = true;
var report;


func _ready():
	if (run_on_ready):
		run();
		get_tree().quit();
		

func run():
	return { "result": "ok" };
	
	
func ok():
	report = {};
	report["name"] = get_script().get_path();
	report["result"] = "ok";
	emit_signal("run_completed");
	
	

func fail(message):
	report = {};
	report["name"] = get_script().get_path();
	report["result"] = "fail";
	report["message"] = message;
	emit_signal("run_completed");


