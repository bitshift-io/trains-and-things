#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# run by going:
# godot.exe -s UnitTest/Cpp.gd
extends "UnitTest.gd"


func run():
	# TODO: make the unit tests return an error message
	
	var curve = BArcLineCurve.new();
	curve.unit_test();
	BDatabase.unit_test();
	return ok();
