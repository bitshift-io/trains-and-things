#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#
extends Node

var tests = [];

func _init():
	return;
	
	
func _ready():
	load_tests();
	run_tests();
	return;
	
	
	
func load_tests():
	var dir = Directory.new();
	var path = "res://UnitTest";
	var err = dir.open(path);
	if err == OK:
		dir.list_dir_begin();
		var file_name = dir.get_next()
		while (file_name != ""):
			if dir.current_is_dir():
				if (file_name != "." && file_name != ".."):
					#print("Found directory: " + file_name)
					pass;
			else:
				print("File found: " + file_name);
				
				# packaed scripts are called XXX.gd.remap
				# so change the file name to XXX.gd so we can use it
				if (file_name.ends_with(".remap")):
					file_name = file_name.replace(".remap", "");
						
				# skip ourself non test files!
				# ensure files end with .gd
				if (file_name != "UnitTestMgr.gd" && file_name != "UnitTest.gd" && file_name.ends_with(".gd")):
					print("Unit test found: " + file_name);
					
					# add the unit test incase it needs acesss to the scene tree
					var script_path = path + "/" + file_name;
					var obj = Util.load_script_into_node(script_path);
					obj.run_on_ready = false;
					add_child(obj);
				
			file_name = dir.get_next()
	else:
		print("An error occurred (" + str(err) + ") when trying to access the path: " + path);
	
		
func run_tests():
	var children = get_children();
	var reports = [];
	var error = false;

	for child in children:
		var obj = child.run();
		if (obj):
			yield(obj, "completed"); #run_completed");
		var report = child.report;
		reports.append(report);

		# abort on first error
		if (report.result != "ok"):
			error = true;
			break;


	for r in reports:
		var msg = r.name + " => " + r.result;
		if (r.result != "ok"):
			msg = msg + " | " + r.message;

		print(msg);
		BLog.debug(msg);

	# exit with errors + print to console as the build system looks for these messages (as godot always quits with errors!)
	if (error):
		print("QUIT WITH ERRORS");
		OS.set_exit_code(1);
	else:
		print("QUIT WITH SUCCESS");

	get_tree().quit();
