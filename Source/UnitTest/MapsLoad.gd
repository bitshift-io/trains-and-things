#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# Test to ensure all maps load without errors
# run by going:
# godot.exe -s=UnitTest/MapsLoad.gd
extends "UnitTest.gd"

#var map_descriptors;
#var map_descriptor;

var error;
	
func run():
	WorldMgr.connect("world_loaded", self, "_on_world_loaded");
	WorldMgr.connect("error", self, "_on_error");
	print("Running MapsLoad.gd Unit test");
	
	var map_descriptors = PluginMgr.get_map_descriptors();
	for map_descriptor in map_descriptors:
		var map_name = map_descriptor.get_display_name();
		var scene_path = map_descriptor.scene_path;
		BLog.debug("Loading map: " + map_name + " (" + scene_path + ")");
		print("Loading map: " + map_name + " (" + scene_path + ")");
		
		error = null;
		Game.set_host_server(true); # required to load some maps
		Game.begin_game({ "map": scene_path });
		
		yield(WorldMgr, "world_loaded");
		
		if (error):
			return fail(scene_path + " failed to load: " + error);
			
		# skip some frames - ideally we shouldn't need to do this
		for i in range(0, 11):
			yield(get_tree(), "idle_frame");
			
		# save a screen grab 
		# TODO: generate a name from the scene_name no display name!
		# TODO: Save outside of the res:// folder OR in the User folder
		var screenshot_path = scene_path.replace("res://Maps/", "res:");
		screenshot_path = screenshot_path.replace(".tscn", ".png");
		screenshot_path = screenshot_path.replace("/", "_");
		screenshot_path = screenshot_path.replace("res:", "res://User/");
		Main.screenshot(screenshot_path);
		
		Game.end_game();
		
	#Menu.set_visible(false); # so we can see the maps
	#load_next();
	#return { "result": "ok" };
	
	BLog.debug("Finished running MapsLoad unit test");
	print("Finished running MapsLoad unit test");
	return ok();

#
#func load_next():
#	if (!map_descriptors || map_descriptors.size() <= 0):
#		BLog.debug("UNIT TEST OK");
#		yield(get_tree(), "idle_frame")
#		get_tree().quit();
#		return;
#
#	map_descriptor = map_descriptors[0];
#	map_descriptors.pop_front();
#
#	var map_name = map_descriptor.get_display_name();
#	var scene_path = map_descriptor.scene_path;
#	BLog.debug("Loading map: " + map_name + " (" + scene_path + ")");
#
#	Game.set_host_server(true); # required to load some maps
#	Game.begin_game({ "map": scene_path });
#	#WorldMgr.load_world(scene_path);
#	return;
	
#
#func _on_world_loaded(world):
#	# so we are guaranteed to have the player controller setup
##	call_deferred("_on_world_loaded_2", world);
#
#func _on_world_loaded_2(world):
#
#	var scene_path = map_descriptor.scene_path;
#	BLog.debug("Map loaded OK: " + scene_path);
#
#	# to ensure the game has loaded let a few frames go by
#	yield(get_tree(), "idle_frame")
#	yield(get_tree(), "idle_frame")
#
#	# save a screen grab 
#	# TODO: generate a name from the scene_name no display name!
#	var screenshot_path = scene_path.replace("res://Maps/", "res:");
#	screenshot_path = screenshot_path.replace(".tscn", ".png");
#	screenshot_path = screenshot_path.replace("/", "_");
#	screenshot_path = screenshot_path.replace("res:", "res://");
#	Main.screenshot(screenshot_path);
#
#	# to ensure screen grab occured
#	yield(get_tree(), "idle_frame")
#	yield(get_tree(), "idle_frame")
#
#	Game.end_game();
#	#WorldMgr.unload_worlds();
#	load_next();
#	return;
	
	
func _on_error(p_error):
	error = p_error;
	WorldMgr.emit_signal("world_loaded"); # so the yield resumes
	
#	BLog.debug("UNIT TEST FAILED: " + error + " with map: " + map_descriptor.scene_path);
#	yield(get_tree(), "idle_frame")
#	get_tree().quit();
#	return;
