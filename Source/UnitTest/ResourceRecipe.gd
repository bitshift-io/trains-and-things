#
# Copyright (C) 2019 bitshift
# http://bitshift.io 
# Refer to LICENSE.md
#

# run by going:
# godot.exe -s=UnitTest/ResourceRecipe.gd
extends "UnitTest.gd"

var RecipeCompiler = load("res://Resource/RecipeCompiler.gd");
var ResourceRecipe = load("res://Resource/ResourceRecipe.tscn");
var ResourceData = load("res://Resource/ResourceData.tscn");

func run():
	Game.set_host_server(true);
	_test_missing_ingredients();
	_test_multiline_recipe();
	_test_transactional_if();
	return ok();
	
	
func _test_missing_ingredients():
	var recipe = ResourceRecipe.instance();
	recipe.recipe = """
		trif consume(people, 1) and supply(meat, 1) then return
	""";
	add_child(recipe);
	var obj = recipe.compile();
	if (!obj):
		return fail("Failed to compile a valid recipe");
	
	var ingredients = {};
	if (recipe.valid_ingredients(ingredients)):
		return fail("Valid ingedients returned true but should have returned false");
		
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	# =, + or * operators are not supported for now, its all done via consume or supply
	# if it needs to be added, it should just call p_proxy.operator_add(p_proxy.get_variable("people"), 4) for example
#	# testing the = operator
#	var recipe2 = ResourceRecipe.instance();
#	recipe2.recipe = """
#		people = 4
#	""";
#	add_child(recipe2);
#	var obj2 = recipe2.compile();
#	if (!obj2):
#		return fail("Failed to compile a valid recipe");
#
#	var ingredients2 = {};
#	if (recipe.valid_ingredients(ingredients2)):
#		return fail("Valid ingedients returned true but should have returned false");
#
#	recipe2.eval(ingredients2);
#	recipe2.apply_ingredient_changes(ingredients2);
	return;
	
	
func _test_transactional_if():
	var recipe = ResourceRecipe.instance();
	recipe.recipe = """
		trif consume(people, 1) and supply(meat, 1) then return
	""";
	add_child(recipe);
	recipe.compile();
	
	var ingredients = {};
	
	var meat = ResourceData.instance();
	add_child(meat);
	var variable = RecipeCompiler.Variable.new("meat");
	variable.resource_data = meat;
	ingredients["meat"] = variable;
	
	var people = ResourceData.instance();
	add_child(people);
	var variable2 = RecipeCompiler.Variable.new("people");
	variable2.resource_data = people;
	ingredients["people"] = variable2;
	
	assert(recipe.valid_ingredients(ingredients) == true);
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	# first time through there is no meat to consume, so the transaction fails
	
	assert(meat.import_amount == 0);
	assert(meat.export_amount == 0);
	assert(people.import_amount == 0);
	assert(people.export_amount == 0);
	
	# now we set it up so that there is a person that can be consumed, 
	# hence the transaction will pass
	people.import_amount = 2;
	
	assert(recipe.valid_ingredients(ingredients) == true);
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	assert(meat.import_amount == 0);
	assert(meat.export_amount == 1);
	assert(people.import_amount == 1);
	assert(people.export_amount == 0);
	
	return;

func _test_multiline_recipe():
	var recipe = ResourceRecipe.instance();
	recipe.recipe = """
		trif consume(people, 1) and consume(timber, 2) and supply(furniture, 4) then return
		trif consume(timber, 4) and supply(furniture, 1) then return
	""";
	add_child(recipe);
	recipe.compile();
	
	var ingredients = {};
	
	var timber = ResourceData.instance();
	add_child(timber);
	var variable = RecipeCompiler.Variable.new("timber");
	variable.resource_data = timber;
	ingredients["timber"] = variable;
	
	var people = ResourceData.instance();
	add_child(people);
	var variable2 = RecipeCompiler.Variable.new("people");
	variable2.resource_data = people;
	ingredients["people"] = variable2;
	
	var furniture = ResourceData.instance();
	add_child(furniture);
	var variable3 = RecipeCompiler.Variable.new("furniture");
	variable3.resource_data = furniture;
	ingredients["furniture"] = variable3;
	
	assert(recipe.valid_ingredients(ingredients) == true);
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	# first time through there is no timber to consume, so all transactions fails
	assert(timber.import_amount == 0);
	assert(timber.export_amount == 0);
	assert(people.import_amount == 0);
	assert(people.export_amount == 0);
	assert(furniture.import_amount == 0);
	assert(furniture.export_amount == 0);
	
	# now we set it up so that there is a person and timber that can be consumed, 
	# hence the first transaction will pass
	people.import_amount = 5;
	timber.import_amount = 5;
	
	assert(recipe.valid_ingredients(ingredients) == true);
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	assert(timber.import_amount == 3);
	assert(timber.export_amount == 0);
	assert(people.import_amount == 4);
	assert(people.export_amount == 0);
	assert(furniture.import_amount == 0);
	assert(furniture.export_amount == 4);
	
	# now lets set it up to test the second transaction
	people.import_amount = 0;
	timber.import_amount = 5;
	furniture.export_amount = 0;
	
	assert(recipe.valid_ingredients(ingredients) == true);
	recipe.eval(ingredients);
	recipe.apply_ingredient_changes(ingredients);
	
	assert(timber.import_amount == 1);
	assert(timber.export_amount == 0);
	assert(people.import_amount == 0);
	assert(people.export_amount == 0);
	assert(furniture.import_amount == 0);
	assert(furniture.export_amount == 1);
		
	return;
