#!/usr/bin/env python3
# -*- coding: utf_8 -*-
#
# Copyright (C) 2017 bitshift
# http://bit-shift.io 
# 
# View LICENSE at:
# https://github.com/bit-shift-io/trains-and-things/blob/master/LICENSE.md
#
#
# sudo pip install GDAL
# sudo pip install openexr
# sudo pip install numpy
# sudo pip install scipy
#



from image import *



parser = argparse.ArgumentParser(description='Convert Geotiff to height map.\n\nExample Usages:\n\t./geotiff_to_height.py -i input.tif -o output.tif -b 3 -s 1024', formatter_class=RawTextHelpFormatter)
parser.add_argument('-o', '--output', nargs='?', help='Output EXR file')
args = parser.parse_args()

if (len(sys.argv) <= 1):
    parser.print_help(sys.stderr)
    sys.exit(1);

    
#
# ENTRY POINT
#

print("");
outTifImg = TifImg();

print("Creating gradient...");
outTifImg.create_gradient(128, 128);

print("");
print("Saving as " + args.output);
outTifImg.save_float32(args.output);
#print("Saving as " + args.o + ".exr");
#outExrImg.save(args.o + ".exr");
print("Done");
print("");




