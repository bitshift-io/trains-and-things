#!/usr/bin/env python3
# -*- coding: utf_8 -*-
#
# Copyright (C) 2017 bitshift
# http://bit-shift.io 
# 
# View LICENSE at:
# https://github.com/bit-shift-io/trains-and-things/blob/master/LICENSE.md
#

import numpy as np
from scipy import exp, mgrid, signal, row_stack, column_stack, tile, misc, ndimage
from scipy.ndimage.filters import gaussian_filter
import array
#import Imath
import numpy
from osgeo import osr, gdal, ogr
import argparse
from argparse import RawTextHelpFormatter
from pathlib import Path
from copy import deepcopy
#import OpenEXR
import sys
import subprocess
import os
import shutil
import pathlib
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

# By default, GDAL does not raise exceptions - enable them
# See http://trac.osgeo.org/gdal/wiki/PythonGotchas
gdal.UseExceptions()
from warnings import warn


config = {}
log_to_file = False


def log(str=''):
    print(str)
    if not log_to_file:
        return

    with open("log.txt", "a") as f:
        f.write(str + '\n')
        f.close()
    return


def pause():
	programPause = input('Press <Enter> to continue...')
	return


def is_power2(num):
    # states if a number == a power of two
    return num != 0 and ((num & (num - 1)) == 0)

# https://stackoverflow.com/questions/14267555/find-the-smallest-power-of-2-greater-than-n-in-python
def next_pow2(x):
    return 1<<(x-1).bit_length()


def process_image(args):
    log("Arguments: {}".format(args))

    # get input file
    input_file = Path(args['input'])
    input_file_path = str(input_file.parent).replace(".","")
    input_file_name = str(input_file.stem)
    input_file_suffix = str(input_file.suffix)

    outTifImg = TifImg()
    log("\nOpen: {}".format(args['input']))
    outTifImg.open(args['input'])
    log("")

    if (args['size'] != None):
        log("\nSize: {}".format(args['size']))
        outTifImg.resize(args['size'])

    if ('clamp' in args and args['clamp'] != None):
        log("\nClamp: {}".format(args['clamp']))
        outTifImg.clamp(args['clamp'])

    if (args['blur'] != '0'):
        log("\nBlur: {}".format(args['blur']))
        outTifImg.gaussian_blur(args['blur'])

    if ('mask' in args and args['mask'] != None):
        log("\nMask: {}".format(args['mask']))
        mask = outTifImg.create_mask(args['mask'])
        mask_output = input_file_path + input_file_name +  "_mask" + input_file_suffix
        mask.save_float32(mask_output)

    if (args['normalize'] == '1' or args['normalize'] == True):
        log("\nnormalize: True")
        outTifImg.normalize()

    if ('angle' in args and args['angle'] != 0):
        log("\Slope: {}".format(args['angle']))
        slope = outTifImg.create_slope(args['angle'])

        # if 1 make a name, else name == passed in
        if args['angle'] == '1':
            slope_output = input_file_path + input_file_name +  "_slope" + input_file_suffix
        else:
            slope_output = args['angle']
        slope.save_float32(slope_output)

    if (args['output'] == None):
        args['output'] = input_file_path + input_file_name +  "_output" + input_file_suffix
    log("\nOutput: {}".format(args['output']))
    outTifImg.save_float32(args['output'])

    log("\nDone")
    return


class qt_app(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'Image Tool'
        self.width = 250
        self.height = 160
        self.left = 0
        self.top = 0
        self.init_ui()
        return


    def init_ui(self):
        # window
        self.setWindowFlags(Qt.Window | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.center()

        # layout
        layout = QVBoxLayout()

        # assign widgets
        # assign self any widgets we need to be global

        # input
        hlayout = QHBoxLayout()
        ui_input_label = QLineEdit()
        self.ui_input_label = ui_input_label
        ui_input_label.setReadOnly(True)
        hlayout.addWidget(ui_input_label)
        ui_input_image = QPushButton('Input')
        ui_input_image.setMaximumWidth(60)
        ui_input_image.clicked.connect(self.input_image)
        hlayout.addWidget(ui_input_image)
        layout.addLayout(hlayout)

        # output
        hlayout = QHBoxLayout()
        ui_output_label = QLineEdit()
        self.ui_output_label = ui_output_label
        ui_output_label.setReadOnly(True)
        hlayout.addWidget(ui_output_label)
        ui_output_image = QPushButton('Output')
        ui_output_image.setMaximumWidth(60)
        ui_output_image.clicked.connect(self.output_image)
        hlayout.addWidget(ui_output_image)
        layout.addLayout(hlayout)

        # size
        hlayout = QHBoxLayout()
        ui_size_label = QLabel('Size')
        hlayout.addWidget(ui_size_label)
        ui_size = QSpinBox()
        self.ui_size = ui_size
        ui_size.setRange(256, 16384)
        ui_size.setValue(8192)
        #ui_size.setMaximumWidth(60)
        hlayout.addWidget(ui_size)
        layout.addLayout(hlayout)

        # normalize
        hlayout = QHBoxLayout()
        ui_blur_label = QLabel('normalize')
        hlayout.addWidget(ui_blur_label)
        ui_normalize = QCheckBox()
        self.ui_normalize = ui_normalize
        #ui_normalize.setMaximumWidth(60)
        ui_normalize.setChecked(True)
        hlayout.addWidget(ui_normalize)
        layout.addLayout(hlayout)

        # blur
        hlayout = QHBoxLayout()
        ui_blur_label = QLabel('Blur')
        hlayout.addWidget(ui_blur_label)
        ui_blur = QSpinBox()
        self.ui_blur = ui_blur
        #ui_blur.setMaximumWidth(60)
        hlayout.addWidget(ui_blur)
        layout.addLayout(hlayout)

        # apply
        ui_apply = QPushButton('Apply')
        ui_apply.clicked.connect(self.apply)
        layout.addWidget(ui_apply)

        # assign layout to window
        self.setLayout(layout)
        self.show()
        return


    def apply(self):
        # check values are set
        if (not os.path.exists(self.input_image)):
            self.alert("Please specify an input")
            return

        if (self.output_image == ''):
            self.alert("Please specify an output")
            return

        # process here!
        args = {}
        args['blur'] = self.ui_blur.value()
        args['size'] = self.ui_size.value()
        args['normalize'] = self.ui_normalize.isChecked()
        args['input'] = self.input_image
        args['output'] = self.output_image
        process_image(args)
        return


    def alert(self, message):
        alert = QMessageBox()
        alert.setWindowTitle('Oops!')
        alert.setText(message)
        alert.exec_()
        return


    def center(self):
        frame = self.frameGeometry()
        screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
        centerPoint = QApplication.desktop().screenGeometry(screen).center()
        frame.moveCenter(centerPoint)
        self.move(frame.topLeft())
        return


    def input_image(self):
        self.input_image = self.open_file_dialog()
        self.ui_input_label.setText(self.input_image)
        return


    def output_image(self):
        output_image = self.save_file_dialog()
        if (not output_image.endswith('.tif')):
            output_image = output_image + '.tif'
        self.output_image = output_image
        self.ui_output_label.setText(self.output_image)
        return


    def open_file_dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self,"Open", "","Image Files (*.tif);;All Files (*)", options=options)
        return file_name


    def save_file_dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getSaveFileName(self,"Save","","Image Files (*.tif);;All Files (*)", options=options)
        return file_name




class TifImg:
    #
    # imgData == an array, where [0] = r, [1] = g, [2] = b etc...
    #
    
    def __init__(self):
        self.driver = gdal.GetDriverByName('GTIFF')
        return
    

    def open(self, path):
        ds = gdal.Open(path,0)
        assert(ds.RasterCount == 1) # == it single band? it better be!
        self.create_from(ds)
        

    def create_from(self, gdalTiff):
        self.source_datasource = gdalTiff
    
        stats = gdalTiff.GetRasterBand(1).GetStatistics(True, True)
        log("min: {}".format(stats[0]))
        
        noDataValue = gdalTiff.GetRasterBand(1).GetNoDataValue()
        log("No data value: {}".format(noDataValue))
        
        #gdalTiff.GetRasterBand(1).SetNoDataValue(stats[0])
        #noDataValue = gdalTiff.GetRasterBand(1).GetNoDataValue()
        #log("No data value 2:", noDataValue)
        
        array = gdalTiff.GetRasterBand(1).ReadAsArray(0,0,gdalTiff.RasterXSize,gdalTiff.RasterYSize)
        
        # where ever there == a "no data value" change it to be the minimum value
        array[array == noDataValue] = stats[0]
        
        minimum = float(numpy.amin(array))
        maximum = float(numpy.amax(array))
        height = maximum - minimum
        log('min: {} max: {} height: {} meters'.format(minimum, maximum, height))
        
        # convert from 16bit int to float32
        #if gdalTiff.GetRasterBand(1).DataType != gdal.GDT_Float32:
        self.destination_datasource = self.driver.Create("~float32.tif.tmp", gdalTiff.RasterXSize,gdalTiff.RasterYSize, 1, gdal.GDT_Float32)
        self.destination_datasource.GetRasterBand(1).WriteArray(array)
        #else:
        #    self.destination_datasource = gdalTiff
            
        self.imgData = [self.destination_datasource.GetRasterBand(1).ReadAsArray(0,0,self.destination_datasource.RasterXSize,self.destination_datasource.RasterYSize)]
        
        self.width = gdalTiff.RasterXSize
        self.height = gdalTiff.RasterYSize
        
        self.dataType = gdalTiff.GetRasterBand(1).DataType # convert back to to 16int on save
        
        self.gt = gdalTiff.GetGeoTransform()
        self.proj = gdalTiff.GetProjection()
        

    # 8 bit rgb
    def create_rgb(self, width, height):        
        self.width = width
        self.height = height
        
        self.dataType = gdal.GDT_Byte
        
        self.imgData = []
        for b in range(0, 3):
            arr = np.zeros((height, width), dtype=np.uint8)
            self.imgData.append(arr)
        return
       

    # create a mask texture
    def create_mask(self, value):
        mask = deepcopy(self)

        return mask


    # create a slope texture
    def create_slope(self):
        mask = deepcopy(self)

        return mask        


    # create a gradient texture - for testing
    def create_gradient(self, width, height):
        self.width = width
        self.height = height
        
        self.dataType = gdal.GDT_Float32
        
        self.imgData = []
        arr = np.zeros((height, width), dtype=np.float32)
        
        self.imgData.append(arr)
        
        step = 1.0 / width
        for y in range(0, height):
            for x in range(0, width):
                val = float(x) * step
                self.imgData[0][y][x] = val
                log("v:" + str(val))
        return              
        

    # clamp texture from min to max
    def clamp(self, range):
        minimum = float(range[0])
        maximum = float(range[1])


        return
        
    # Normalize the whole image from 0 to 1
    def normalize(self):
        minimum = float(numpy.amin(self.imgData[0]))
        self.imgData[0] = self.imgData[0] - minimum
        maximum = float(numpy.amax(self.imgData[0]))
        scalar = 1.0 / maximum
        log('min: {} max: {} scale: {}'.format(minimum, maximum, scalar))
        self.imgData[0] = self.imgData[0] * scalar
        # check new values
        minimum = float(numpy.amin(self.imgData[0]))
        maximum = float(numpy.amax(self.imgData[0]))
        log('min: {} max: {}'.format(minimum, maximum))
        return

        
    # Normalize the above water component from 0 to INT16_MAX
    def normalize_int16(self):
        minimum = float(numpy.amin(self.imgData[0]))
        maximum = float(numpy.amax(self.imgData[0]))
        log("min:" + str(minimum))
        log("max:" + str(maximum))
        
        delta = maximum - minimum
        #log("delta:" + str(delta))
        
        # move the lowest point to zero
        #self.imgData[0] = self.imgData[0] - minimum
        
        # because we are exporting as int16, we normalize from -INT16_MAX to INT16_MAX
        INT16_MAX = 0xFFFF
        scalar = INT16_MAX / maximum
        log("scalar:" + str(scalar))
        
        centre = minimum + ((maximum - minimum) / 2)
        log("data centre:" + str(centre))
        
        # want to move the centre of the data to zero
        offset = -centre
        log("offset:" + str(offset))
        
        # 
        #scalar = 1.0 / maximum
        #if (self.dataType == gdal.GDT_Int16):
        #    scalar = INT16_MAX / maximum
        #elif (self.dataType == gdal.GDT_Float32):
        #    pass
        #else:
        #    log("Unhandled DataType:", self.dataType)
                  
        self.imgData[0] = self.imgData[0] + offset # centre the data around zero
        
        minimum = float(numpy.amin(self.imgData[0]))
        maximum = float(numpy.amax(self.imgData[0]))
        log("min:" + str(minimum))
        log("max:" + str(maximum))
        
        self.imgData[0] = self.imgData[0] * scalar # map the above water component from (-data type range maximum)->(data type range maximum) range
        
        #lowest = -10
        #self.imgData[0][self.imgData[0] > 0] *= scalar 
        
        log("type" + str(type(self.imgData[0])))
        log("shape" +  str(np.shape(self.imgData[0])))
        
        
        # this pushes down what == at or below what level to avoid z-fighting with the water shader
        #self.imgData[0][self.imgData[0] <= 0] -= 10
        
        #self.imgData[0][self.imgData[0] < -32767] = -32767
        
        #log("min:" + str(float(numpy.amin(self.imgData[0]))))
        #log("max:" + str(float(numpy.amax(self.imgData[0]))))
        
        minimum = float(numpy.amin(self.imgData[0]))
        maximum = float(numpy.amax(self.imgData[0]))
        log("min:" + str(minimum))
        log("max:" + str(maximum))
        return

        
    # resize the image and centre it
    def resize(self, size):
        oldWidth = self.width
        oldHeight = self.height

        # check == a percentage or fixed size
        if (isinstance(size, str) and "%%" in size): 
            size = size.replace("%%","")
            self.width = self.width * size * 0.01
            self.height = self.height * size * 0.01
        else:
            size = int(size)
            self.width = size #next_pow2(oldWidth)
            self.height = size #next_pow2(oldHeight)

        if (is_power2(self.width) == False or is_power2(self.height) == False ):
            log("New size {} x {} != a power of 2".format(self.width, self.height))
        
        newImgData = np.zeros(shape=(self.height,self.width))
        
        #log("Image offset:", [x_offset, y_offset])
        #log("Old Size:", [oldWidth, oldHeight])
        #log("New Size:", [self.width, self.height])
        #log("newImgData dimensions:", newImgData.shape)
        #log("ImgData dimensions:", self.imgData[0].shape)
        
        # lets see if we can this texture to better fit to minimize "ocean"
        # scale the texture to best fit the pow2 texture
        x_scale = self.width / oldWidth
        y_scale = self.height / oldHeight
        final_scale = min(x_scale, y_scale)
        
        resampledHeight = int(oldHeight * final_scale)
        resampledWidth = int(oldWidth * final_scale)
        
        # write the imgData back to the raster band so that we are using the modified imgData not the original!
        self.destination_datasource.GetRasterBand(1).WriteArray(self.imgData[0], 0, 0)
        
        resampled_datasource = self.driver.Create("~resampled.tif.tmp", resampledWidth, resampledHeight, 1, gdal.GDT_Float32)
        gdal.RegenerateOverviews(self.destination_datasource.GetRasterBand(1), [ resampled_datasource.GetRasterBand(1) ], 'mode')
        resampled_imgData = resampled_datasource.GetRasterBand(1).ReadAsArray(0,0,resampledWidth,resampledHeight)
        
        # allow us to centre the map
        x_offset = int((self.width - resampledWidth) / 2)
        y_offset = int((self.height - resampledHeight) / 2)
        
        newImgData[y_offset:(y_offset+resampledHeight), x_offset:(x_offset+resampledWidth)] = resampled_imgData
        self.imgData[0] = newImgData
        return


    def get_normal(self, a):
        #log(a)
        return
        

    def generate_normal_map(self):
        vecfunc = np.vectorize(self.get_normal)
        self.normalImgData = vecfunc(self.imgData[0])
        
        #np.apply_along_axis(self.get_normal, 0, self.imgData[0])
        return

            
    def save_float32(self, path): 
        self.save(path, gdal.GDT_Float32)
        return


    def save_int16(self, path):   
        self.save(path, gdal.GDT_Int16)
        return


    def save(self, path, dataType):        
        channels = len(self.imgData)
            
        log("channel count: {}".format(channels))
        # Create output image of the same type as we were opened with
        output = self.driver.Create(path, self.width, self.height, channels, dataType, options = [ 'COMPRESS=LZW' ]) #GDT_Float32)#, self.dataType)
        
        if (hasattr(self, "gt")):
            output.SetGeoTransform(self.gt)
            
        if (hasattr(self, "proj")):
            output.SetProjection(self.proj)

        for i in range(0, channels):
            outBand = output.GetRasterBand(i + 1).WriteArray(self.imgData[i])
            
        output.FlushCache()
        return


    def gaussian_blur(self, size):
        if (float(size) <= 0):
            return
        
        channels = len(self.imgData)
        for i in range(0, channels):
            self.imgData[i] = gaussian_filter(self.imgData[i], sigma=float(size))
        return


    def get_r(self, x, y):
        return self.imgData[0][y][x]
    

    def set_rgb(self, x, y, rgb):
        self.imgData[0][y, x] = rgb[0] * 255
        self.imgData[1][y, x] = rgb[1] * 255
        self.imgData[2][y, x] = rgb[2] * 255
        return
    
"""
class ExrImg:
    
    def __init__(self):
        return
        
    def create(self, width, height, fillValue):
        self.width = width
        self.height = height
        self.R = np.full((width * height), fillValue) #numpy.zeros(width * height)
        self.G = np.full((width * height), fillValue) #numpy.zeros(width * height)
        self.B = np.full((width * height), fillValue) #numpy.zeros(width * height)
        
    def open(self, path):
        self.file = OpenEXR.InputFile("untitled2.exr")
        #log(self.file.header())
        
        dw = self.file.header()['dataWindow']
        self.width = dw.max.x - dw.min.x + 1
        self.height = dw.max.y - dw.min.y + 1

        FLOAT = Imath.PixelType(Imath.PixelType.FLOAT)
        (self.R,self.G,self.B) = [array.array('f', self.file.channel(Chan, FLOAT)).tolist() for Chan in ("R", "G", "B") ]
        #(self.R) = [array.array('f', self.file.channel(Chan, FLOAT)).tolist() for Chan in ("R") ]
        
        log("Size: ", len(self.R))        
        
    def get_r(self, x, y):
        x = max(0, min(x, self.width - 1))
        y = max(0, min(y, self.height - 1))
        idx = (y * self.width) + x
        #log([x, y])        
        #log(idx)
        return self.R[(y * self.width) + x]
    
    def set_rgb(self, x, y, rgb):
        idx = (y * self.width) + x
        self.R[idx] = rgb[0]
        self.G[idx] = rgb[1]
        self.B[idx] = rgb[2]
        
    def save(self, path):
        # convert to strings
        (Rs, Gs, Bs) = [ array.array('f', Chan).tostring() for Chan in (self.R, self.G, self.B) ]

        # write channels
        out = OpenEXR.OutputFile(path, OpenEXR.Header(self.width, self.height))
        out.writePixels({'R' : Rs, 'G' : Gs, 'B' : Bs })

"""

 
#
# ENTRY POINT
#
# main
# command line args go here
if __name__== "__main__":
    parser = argparse.ArgumentParser(description='Convert Geotiff to height map.\n\nExample Usages:\n ./geotiff_to_height.py -i input.tif -o output.tif -b 3 -s 1024', formatter_class=RawTextHelpFormatter)
    parser.add_argument('-i', '--input', nargs='?', help='Input Geotiff file (file)')
    parser.add_argument('-o', '--output', nargs='?', help='Output (file) (optional)')
    parser.add_argument('-b', '--blur', nargs='?', help='Blur size (optional)', default=0)
    parser.add_argument('-s', '--size', nargs='?', help='Size pixels (8192) or scale (50%%) (optional)')
    parser.add_argument('-c', '--clamp', nargs='+', help='Clamp value (min max) (optional)')
    parser.add_argument('-n', '--normalize', nargs='?', help='Normalize image (1) (optional)', default=0)
    parser.add_argument('-m', '--mask', nargs='?', help='Mask image bellow value (float) (optional)')
    parser.add_argument('-a', '--angle', nargs='?', help='Slope/Angle map (1, or output name) (optional)', default=0)
    args = parser.parse_args()

    # no args
    # launch the gui app
    if len(sys.argv) <= 1 or args.input == None:
        app = QApplication(sys.argv)
        ex = qt_app()
        sys.exit(app.exec_())

    # process command args
    process_image(args)
    sys.exit()
