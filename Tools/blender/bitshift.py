bl_info = {
    "name": "Bitshift",
    "author": "Bronson & Fabian Mathews",
    "version": (0, 1, 5),
    "blender": (2, 80, 0),
    "location": "Workspace > Properties Panel > Bitshift",
    "description": "Bitshift tools",
    "warning": "",
    "wiki_url": "http://bitshift.io",
    "category": "User",
}

# <pep8 compliant>
import bpy
from bpy.types import Operator, Panel, PropertyGroup
from bpy.props import (
    BoolProperty,
    IntProperty,
    FloatProperty,
    FloatVectorProperty,
    EnumProperty
)
from mathutils import Vector, Color
import os
import sys
import json
import bmesh
from timeit import default_timer as timer
from pathlib import *
import math

# for storing states between classes
temp_dictionary = {}


#------------------- panels ------------------------------  
class WorkSpaceButtonsPanel:
    #bl_space_type = 'PROPERTIES'
    #bl_region_type = 'WINDOW'
    #bl_context = ".workspace"

    # Developer note: this is displayed in tool settings as well as the 3D view.
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

class WORKSPACE_PT_BitshiftExportPanel(WorkSpaceButtonsPanel, Panel):
    bl_label = "bitshift"

    def draw(self, context):
        layout = self.layout


        col = layout.column(align=True)
        
        col.label(text="Tool")
        col.operator("bitshift.save_modified_images", text="Save Modified Textures")

        col.operator("bitshift.vertex_color_to_uv", text="Vertex Color to UV")

        show_vertex_colours = col.operator("bitshift.toggle_vertex_colours", text="Show Vertex Colours")
        show_vertex_colours.mode='show'

        show_vertex_colours = col.operator("bitshift.toggle_vertex_colours", text="Hide Vertex Colours")
        show_vertex_colours.mode='hide'

        col.label(text="Export")
        col.operator("bitshift.export_scene", text="Export Scene")
        col.operator("bitshift.export_collections", text="Export Collections")
        col.operator("bitshift.export_selected", text="Export Selected")
        col.operator("export_scene.gltf", text="Export Dialog")
        col.operator("bitshift.export_curves", text="Export Curves")
        
        col.label(text="Terrain")
        quick_render = col.operator("bitshift.render_map", text="Quick Render Map")
        quick_render.mode='quick'

        render = col.operator("bitshift.render_map", text="Render Map")
        render.mode='default'

        col.operator("bitshift.import_map", text="Import Map")
        
        col.label(text="Import")
        col.operator("bitshift.import_capture_3d_video", text="Capture 3D Video")


#------------------- export path ------------------------------   
def get_path(nameOverride = None):
    # get scene render path, if user has changed from /tmp/
    directory = bpy.path.abspath(bpy.context.scene.render.filepath)

    # find the root directory
    # only if scene path is not specified
    if directory == "/tmp/":
        directory = os.path.dirname(bpy.data.filepath)
        print("searching for path...")
        print(directory)
        while True:
            gitdir = os.path.join(directory, '.git')
            if os.path.exists(gitdir):
                directory = os.path.join(directory, 'Source/Mesh')
                break
            elif directory == "/":
                directory = bpy.path.abspath(bpy.context.scene.render.filepath)
                break
            directory = os.path.dirname(os.path.dirname(directory))

    # get filename
    name = bpy.path.display_name_from_filepath(bpy.data.filepath) if nameOverride is None else nameOverride

    linux_filepath = str(directory + "/" + name + '.glb')
    windows_filepath = str(directory + "\\" + name + '.glb')

    if sys.platform.startswith('win'):
        path = windows_filepath
    
    elif sys.platform.startswith('linux'):
        path = linux_filepath 
    
    print(path)
    return path


def render_finish(scene):
    print("render finish")
    
    # pull these variables in a dictionary
    global temp_dictionary
    selected = bpy.data.objects[temp_dictionary['selected']]
    camera = bpy.data.objects[temp_dictionary['camera']]
    copy = bpy.data.objects[temp_dictionary['copy']]
    temp_dictionary = {}

    # cleanup
    selected.hide_render = False
    selected.hide_viewport = False

    # delete
    bpy.data.cameras.remove(camera.data)
    bpy.data.meshes.remove(copy.data)

    # select
    selected.select_set(True)
    
    print("rename files")
    directory = bpy.path.abspath(bpy.context.scene.render.filepath)
    print(directory)
    for f in os.listdir(directory):
        f = PurePath(f)
        f_name = f.name

        # rename textures with T-
        if ((f_name.endswith('.png') or f_name.endswith('.exr')) and not f_name.startswith('T-')):
            f_new_name = 'T-' + f_name
            os.replace(os.path.join(directory, f_name), os.path.join(directory, f_new_name))
            f_name = f_new_name

        # remove numbering and overwrite file
        if (f_name.endswith("0001.png") or f_name.endswith("0000.png") or f_name.endswith("0001.exr") or f_name.endswith("0000.exr")):
            f_new_name = f_name.replace("0001","").replace("0000","")
            print('{} -> {}'.format(f_name, f_new_name))
            os.replace(os.path.join(directory, f_name), os.path.join(directory, f_new_name))

#------------------- export curves ------------------------------  
class export_curves(bpy.types.Operator):
    """Export Curves"""
    bl_label = "Export Curves"
    bl_idname = "bitshift.export_curves"
    
    def execute(self, context):
        print("Export Curves")
        path = get_path()

        for ob in bpy.data.objects.values(): 
            if ob.type == 'CURVE':
                currPath = path.replace(".glb",( "-" + ob.name + ".json"))
                file = open(currPath, "w")
                print(currPath)

                file.write( '{\n"name":"%s",\n' % ob.name)
                # swap y - z & invert y
                file.write( '"transform":\n')
                file.write("    [[%.3f, %.3f, %.3f, %.3f],\n" % (ob.matrix_world[0][0], ob.matrix_world[0][1], ob.matrix_world[0][2], ob.matrix_world[0][3]))
                file.write("    [%.3f, %.3f, %.3f, %.3f],\n" % (ob.matrix_world[1][0], ob.matrix_world[1][1], ob.matrix_world[1][2], ob.matrix_world[1][3]))
                file.write("    [%.3f, %.3f, %.3f, %.3f],\n" % (ob.matrix_world[2][0], ob.matrix_world[2][1], ob.matrix_world[2][2], ob.matrix_world[2][3]))
                file.write("    [%.3f, %.3f, %.3f, %.3f]],\n" % (ob.matrix_world[3][0], ob.matrix_world[3][1], ob.matrix_world[3][2], ob.matrix_world[3][3]))

                # spline
                for spline in ob.data.splines :
                    file.write( '"cyclic":"%s",\n' % spline.use_cyclic_u)
                    num_points = len(spline.bezier_points)
                    if num_points > 0:
                        file.write('"curve":[\n')
                        for p in range(0, num_points):
                            bezier_point = spline.bezier_points.values()[p]
                            handle_left  = bezier_point.handle_left - bezier_point.co
                            co           = bezier_point.co
                            handle_right = bezier_point.handle_right - bezier_point.co

                            file.write("    [[%.3f, %.3f, %.3f], " % (handle_left.x, handle_left.y, handle_left.z ))
                            file.write("    [%.3f, %.3f, %.3f], " % (co.x, co.y, co.z ))
                            file.write("    [%.3f, %.3f, %.3f]]" % (handle_right.x, handle_right.y, handle_right.z ))
                            
                            if p < (num_points-1):
                                file.write(",\n")
                            else:
                                file.write("\n")

                file.write(']\n}\n')

        file.close()
        return {"FINISHED"}


#------------------- export selected ------------------------------  
class export_selected(bpy.types.Operator):
    """Export Selected"""
    bl_label = "Export Selected"
    bl_idname = "bitshift.export_selected"
    
    def execute(self, context):
        print("Export Selected")
        path = get_path()
        bpy.ops.export_scene.gltf(export_apply=True, filepath=path, filter_glob="*.glb", export_selected=True)
        return {"FINISHED"}


#------------------- export scene ------------------------------  
class export_scene(bpy.types.Operator):
    """Export All"""
    bl_label = "Export All"
    bl_idname = "bitshift.export_scene"
    
    def execute(self, context):
        print("Export All")
        path = get_path()
        bpy.ops.export_scene.gltf(export_apply=True, filepath=path, filter_glob="*.glb")
        return {"FINISHED"}


#------------------- export collections ------------------------------  
class export_collections(bpy.types.Operator):
    """Export Collections"""
    bl_label = "Export Collections"
    bl_idname = "bitshift.export_collections"
    
    def execute(self, context):
        print("Export Collections")

        bpy.ops.object.select_all(action='DESELECT')
        
        for collection in bpy.data.collections:
            collection_objects = collection.objects
            for co in collection_objects:
                co.select_set(state=True)
                splits = co.name.split('.') # remove any .001 or what ever blender appends as it dont like duplicate names
                co.name = splits[0]

            path = get_path(collection.name_full)
            bpy.ops.export_scene.gltf(export_apply=True, filepath=path, filter_glob="*.glb", export_selected=True)

            bpy.ops.object.select_all(action='DESELECT')

        return {"FINISHED"}


#------------------- render terrain ------------------------------   
class terrain_render_map(bpy.types.Operator):
    bl_label = "Render Map"
    bl_idname = "bitshift.render_map"
    
    node_types = {
        'RL' : 'CompositorNodeRLayers',
        'OF' : 'CompositorNodeOutputFile',
        'OC' : 'CompositorNodeComposite'
    }

    mode: bpy.props.StringProperty()
    
    def execute(self, context):
        print("Rendering map with mode: ", self.mode)
        self.render_image(context)
        return {"FINISHED"}
    

    def get_output( self, passout ):
        """ Find the renderlayer node's output that matches the current render
            pass """

        # Renderlayer pass names and renderlayer node output names do not match
        # which is why we're using this dictionary (and regular expressions)
        # to match the two
        # file name extension : blender extension
        output_dict = {
            'ambient_occlusion' : 'AO',
            'material_index'    : 'IndexMA',
            'object_index'      : 'IndexOB',
            'reflection'        : 'Reflect',
            'refraction'        : 'Refract',
            'combined'          : 'Image',
            'uv'                : 'UV',
            'z'                 : 'Depth'
        }

        output = ''
        if passout in output_dict.keys():
            output = output_dict[ passout ]
        elif "_" in passout:
            wl = passout.split("_") # Split to list of words
            # Capitalize first char in each word and rejoin with spaces
            output = " ".join([ s[0].capitalize() + s[1:] for s in wl ])
        else: # If one word, just capitlaize first letter
            output = passout[0].capitalize() + passout[1:]

        return output


    def save_output_passes(self,context):
        # compositing
        bpy.context.scene.use_nodes = True
        tree = bpy.context.scene.node_tree
        links = tree.links
        
        for node in tree.nodes:
            tree.nodes.remove(node)
            
        # get name of current file
        filename = bpy.path.basename(bpy.context.blend_data.filepath) 
        basename = 'T-' + os.path.splitext(filename)[0].replace(' ','') # T for texture, remove spaces
        pass_attr_str = 'use_pass_'

        rl_nodes_y   = 0
        node = ''  # Initialize node so that it would exist outside the loop

        # for each render layer create an entry in the output node
        for rl in bpy.context.scene.view_layers:
            # Create a new render layer node
            node = ''
            node = tree.nodes.new( type = self.node_types['RL'] )

            # Set node location, label and name
            node.location = 0, rl_nodes_y
            node.label    = rl.name
            node.name     = rl.name

            # Select the relevant render layer
            node.layer = rl.name

            # create nodes
            output_node = tree.nodes.new( type = self.node_types['OF'] )
            output_node.file_slots.remove(output_node.inputs[0]) # remove first entry

            # Set base path, location, label and name
            #filepath = bpy.data.filepath
            #directory = os.path.dirname(filepath)
            #output_node.base_path = directory # should be default render path!
            output_node.location  = 500, 0
            output_node.label     = 'file output'
            output_node.name      = 'file output'
            output_node.format.file_format = 'PNG'
            output_node.format.color_mode = 'RGB'
            output_node.format.compression = 100

            # for each pass in layer
            passes = [ p for p in dir(rl) if pass_attr_str in p ]
            for p in passes:
                # If render pass is active (True) - create output
                if getattr( rl, p ):
                    pass_name = p[len(pass_attr_str):]
                    output = self.get_output( pass_name )
                    file_path = basename + "-" + output
        
                    #if output == 'Image' and not output_node.inputs[ output ].links:
                    #    links.new( node.outputs[ output ], output_node.inputs[ output ])   
                        
                    if output:
                        # Add file output socket
                        output_node.file_slots.new( name = output )
                        socket = output_node.file_slots[-1]
                        socket.path = file_path
                        socket.use_node_format = False
                        
                        if output == 'Depth':
                            file_path = basename + "-Height"
                            socket.path = file_path
                            normalize_node = tree.nodes.new(type="CompositorNodeNormalize")
                            links.new(node.outputs[ output ],normalize_node.inputs[0])
                            
                            invert_node = tree.nodes.new(type="CompositorNodeInvert")
                            links.new(normalize_node.outputs[0],invert_node.inputs[1])
                            links.new(invert_node.outputs[0],output_node.inputs[-1])  
                            
                            # configure texture
                            socket.format.file_format = 'PNG' # OPEN_EXR
                            #socket.format.exr_codec = 'ZIP'
                            socket.format.color_mode = 'RGB'
                            socket.format.color_depth = '16' # 16 - half float, 32 - full float
                            socket.format.use_zbuffer = True

                            
                        elif output == 'Normal':
                            seperate_node = tree.nodes.new(type="CompositorNodeSepRGBA")
                            links.new(node.outputs[ output ], seperate_node.inputs[0])
                            
                            r_normalize_node = tree.nodes.new(type="CompositorNodeNormalize")
                            links.new(seperate_node.outputs[0], r_normalize_node.inputs[0])
                            
                            g_normalize_node = tree.nodes.new(type="CompositorNodeNormalize")
                            links.new(seperate_node.outputs[1], g_normalize_node.inputs[0])
                            
                            combine_node = tree.nodes.new(type="CompositorNodeCombRGBA")
                            links.new(r_normalize_node.outputs[0], combine_node.inputs[0])  
                            links.new(g_normalize_node.outputs[0], combine_node.inputs[1]) 
                            links.new(seperate_node.outputs[2], combine_node.inputs[2]) 
                            links.new(combine_node.outputs[0], output_node.inputs[-1])   

                            # configure texture
                            socket.format.color_mode = 'RGB'
                            socket.format.color_depth = '16'
                            socket.format.compression = 100

                            # add an extra output - slope texture
                            # Add file output socket
                            output_node.file_slots.new( name = output )
                            socket = output_node.file_slots[-1]
                            socket.path = file_path
                            socket.use_node_format = False

                            # configure texture
                            socket.format.color_mode = 'BW'
                            socket.format.color_depth = '16'
                            socket.format.compression = 100                            

                            ramp_node = tree.nodes.new(type="CompositorNodeValToRGB")
                            
                            links.new(ramp_node.outputs[0], output_node.inputs[-1])
                                                      

                        elif output == 'AO':
                            ramp_node = tree.nodes.new(type="CompositorNodeValToRGB")
                            ramp_node.color_ramp.elements[0].position = 0.5
                            links.new(node.outputs[ output ], ramp_node.inputs[0])
                            
                            links.new(ramp_node.outputs[0], output_node.inputs[-1])                               
                            
                            # configure texture
                            socket.format.color_mode = 'BW'
                            socket.format.compression = 100
                            
                        else:
                            # Set up links
                            links.new( node.outputs[ output ], output_node.inputs[-1] )

            rl_nodes_y -= 300
        '''
        # Create composite node, just to enable rendering
        cnode = ''
        cnode = tree.nodes.new( type = self.node_types['OC'] )

        # Link composite node with the last render layer created
        links.new( node.outputs[ 'Image' ], cnode.inputs[0] )
        '''

    def render_image(self,context):
        #print("current context:")
        #print(context.area.type)

        # ALWAYS SAVE MODIFIED IMAGES TO AVOID DATA LOSS!!!! Poor Brons!
        bpy.ops.bitshift.save_modified_images()

        selected = bpy.context.active_object
        if selected == "" or selected is None:
            self.report({'ERROR'},"please select a mesh")
            return {"FINISHED"}

        is_quick_mode = (self.mode == 'quick')
        
        # setup cameras before we go fudging with the mesh
        bbox_corners = [selected.matrix_world @ Vector(corner) for corner in selected.bound_box]
        pos = max(z for (x,y,z) in bbox_corners) + 1  # python wizardry?
        bpy.ops.object.camera_add(align='VIEW', enter_editmode=False, location=(0.0, 0.0, pos), rotation=(0.0, 0.0, 0.0),)
        camera = bpy.context.active_object
        camera.name = "camera_delete"
        camera.data.type = 'ORTHO'
        camera.data.clip_start = 0.5 # needs to be less than 1 we add above
        camera.data.clip_end = 1 + selected.dimensions.z
        camera.data.ortho_scale = max(selected.dimensions)
        bpy.context.scene.camera = camera

        # deselect camera and select initial selection
        camera.select_set(state=False)
        selected.select_set(state=True)
        bpy.context.view_layer.objects.active = selected

        # copy mesh for render
        bpy.ops.object.duplicate()
        copy = bpy.context.active_object
        copy.name = "terrain_delete"

        # modify for edge cleanup
        bpy.ops.object.mode_set(mode='EDIT')
        #bpy.ops.object.editmode_toggle()
        bpy.context.tool_settings.mesh_select_mode = (False, True, True) # edge mode
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.mesh.select_non_manifold()
        bpy.ops.mesh.extrude_region()
        bpy.ops.mesh.select_non_manifold()
        bpy.ops.transform.resize(value=(2, 2, 2), constraint_axis=(True, True, False), orient_type='GLOBAL')
        bpy.ops.object.mode_set(mode='OBJECT')
        #bpy.ops.object.editmode_toggle()

        # hide selected mesh
        selected.hide_viewport = True
        selected.hide_render = True

        # setup render
        cycles = bpy.context.scene.cycles
        bpy.context.scene.render.engine = 'CYCLES'
        bpy.context.scene.sequencer_colorspace_settings.name = 'Raw'    
        bpy.context.scene.view_layers[0].use_pass_normal = True
        bpy.context.scene.view_layers[0].use_pass_z = True
        bpy.context.scene.view_layers[0].use_pass_combined = False
        bpy.context.scene.view_layers[0].use_pass_ambient_occlusion = True
        #bpy.context.scene.view_settings.view_transform = 'Raw'

        cycles.max_bounces = 1
        cycles.min_bounces = 1
        cycles.caustics_reflective = False
        cycles.caustics_refractive = False
        cycles.diffuse_bounces = 1
        cycles.glossy_bounces = 1
        cycles.transmission_bounces = 1
        cycles.volume_bounces = 1
        cycles.transparent_min_bounces = 1
        cycles.transparent_max_bounces = 1
        cycles.use_denoising = True # enable denoise
        cycles.samples = 128 # default

        bpy.data.worlds["World"].light_settings.distance = 1000 # ao distance

        # quick mode settings
        if (is_quick_mode):
            bpy.context.scene.view_layers[0].use_pass_ambient_occlusion = False
            #bpy.context.scene.render.resolution_percentage = 100 # user should define this!
            cycles.samples = 4

        # https://www.blenderguru.com/articles/4-easy-ways-to-speed-up-cycles
        # optimise for CPU rendering
        bpy.context.scene.render.tile_x = 16 
        bpy.context.scene.render.tile_y = 16

        # store these variables in a dictionary
        global temp_dictionary
        temp_dictionary = {}
        temp_dictionary['selected'] = selected.name
        temp_dictionary['camera'] = camera.name
        temp_dictionary['copy'] = copy.name

        # add render handler
        bpy.app.handlers.render_complete.append(render_finish)

        # render
        self.save_output_passes(context)

        print("render begin")
        # crash with gui
        bpy.ops.render.render('INVOKE_DEFAULT')
        
        # crash free without gui
        #bpy.ops.render.render()
        return
        

#------------------- terrain import ------------------------------      
class terrain_import_map(bpy.types.Operator):
    bl_label = "Import Map"
    bl_idname = "bitshift.import_map"
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")
    
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    
    def execute(self, context):
        print("Import map")
        
        # fix colour space to fix render issues
        bpy.context.scene.display_settings.display_device = 'None'

        # setup render
        bpy.context.scene.render.engine = 'CYCLES'
        bpy.context.scene.sequencer_colorspace_settings.name = 'Raw'         
        
        # open image
        image = bpy.data.images.load(filepath=self.filepath, check_existing=False)
        image.colorspace_settings.name = 'Raw'
        texture = bpy.data.textures.new('Terrain', type = 'IMAGE')
        texture.image = image
        texture.extension = 'EXTEND'

        # create mesh
        # todo: support for non-square maps!
        width = image.size[0]
        height = image.size[1]
        print('image size: {} {}'.format(width, height))
        min_size = min(width, height)

        # divide into 4 levels
        # 1 vertex per pixel
        # formula for divisions: (n + 1) / 2)
        def calc_div(n):
            return (n + 1) / 2

        def inv_calc_div(n):
            return 2 * n  - 1
        
        #x_divisions = int(width / 8)
        #y_divisions = int(height / 8)

        x_divisions = calc_div(width)
        x_divisions = calc_div(x_divisions)
        x_divisions = calc_div(x_divisions)
        x_divisions = calc_div(x_divisions)
        x_divisions = math.ceil(x_divisions)

        y_divisions = calc_div(width)
        y_divisions = calc_div(y_divisions)
        y_divisions = calc_div(y_divisions)
        y_divisions = calc_div(y_divisions)
        y_divisions = math.ceil(y_divisions)

        debug_x_div = x_divisions
        debug_y_div = y_divisions
        for i in range(0, 5):
            print('level: {} divisions: {} {}'.format(i, debug_x_div, debug_y_div))
            debug_x_div = inv_calc_div(debug_x_div)
            debug_y_div = inv_calc_div(debug_y_div)

        # create grid
        bpy.ops.mesh.primitive_grid_add(x_subdivisions=x_divisions, y_subdivisions=y_divisions, size=min_size, calc_uvs=True, enter_editmode=False, align='WORLD', location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))
        bpy.ops.object.shade_smooth()
        mesh = bpy.context.active_object
        mesh.name = "terrain"
        mesh.dimensions = [width, height, 0]

        # subdiv 
        # apply 4 levels
        multires = mesh.modifiers.new("Multires", type='MULTIRES')
        multires.subdivision_type = 'SIMPLE'
        bpy.ops.object.multires_subdivide(modifier="Multires")
        bpy.ops.object.multires_subdivide(modifier="Multires")
        bpy.ops.object.multires_subdivide(modifier="Multires")
        bpy.ops.object.multires_subdivide(modifier="Multires")


        # displace
        displace = mesh.modifiers.new("Displace", type='DISPLACE')
        displace.strength = 100
        displace.mid_level = 0
        displace.texture_coords = 'UV'
        displace.uv_layer = "UVMap"
        displace.texture = texture
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Displace")
        
        # add palette for painting terrain
        pal_array = [
            (1, 0, 0),
            (0, 1, 0),
            (0, 0, 1),
            (0, 0, 0)
        ]

        bpy.ops.palette.new()
        pal = bpy.data.palettes[-1]
        pal.name='terrain'
        for col in pal_array:
            bpy.ops.palette.color_add()
            pal.colors[-1].color = Color(col)
            
        # apply material
        material = self.create_terrain_material(context, mesh)
        material_2 = self.create_height_material(context, mesh)
        
        # try and load terrain map, and boundary map if they exist
        #directory = os.path.dirname(image.filepath)
        terrain_path = image.filepath.replace("Height","Terrain")
        color_path = image.filepath.replace("Height","Color")
        boundary_path = image.filepath.replace("Height","Boundary")

        nodes = material.node_tree.nodes
        links = material.node_tree.links

        # todo: color space was moved in blender 2.80
        if os.path.isfile(terrain_path):
            terrain_img = bpy.data.images.load(filepath=terrain_path, check_existing=True)
            terrain_node = nodes["terrain"]
            terrain_node.image = terrain_img
            #terrain_node.image.color_space = 'NONE'
            terrain_node.extension = 'EXTEND'    

        if os.path.isfile(color_path):
            color_img = bpy.data.images.load(filepath=color_path, check_existing=True)
            color_node = nodes["color"]
            color_node.image = color_img
            #color_node.image.color_space = 'NONE'
            color_node.extension = 'EXTEND'              
            
        if os.path.isfile(boundary_path):
            boundary_img = bpy.data.images.load(filepath=boundary_path, check_existing=True)
            boundary_node = nodes["boundary"]
            boundary_node.image = boundary_img       
            #boundary_node.image.color_space = 'NONE'
            boundary_node.extension = 'EXTEND'

        return {"FINISHED"}    
    

    def dump(self, obj):
        for attr in dir(obj):
            if hasattr( obj, attr ):
                print( "obj.%s = %s" % (attr, getattr(obj, attr)))
                
    def create_height_material(self, context, mesh):
        material = bpy.data.materials.get("height")
        if material != None:
            bpy.data.materials.remove(material)
        material = bpy.data.materials.new("height")
        material.use_nodes = True
        mesh.data.materials.append(material)

        nodes = material.node_tree.nodes
        links = material.node_tree.links

        # geomery node
        geometry = nodes.new('ShaderNodeNewGeometry')
        geometry.location = (-600,0)

        # mapping node
        mapping = nodes.new('ShaderNodeMapping')
        mapping.location = (-600,0)
        mapping.vector_type = 'POINT'

        # world pos value node
        world_position = nodes.new('ShaderNodeValue')
        world_position.label = 'World Position'
        world_position.location = (-600,0)
        self.dump(world_position)

        # world scale value node
        world_scale = nodes.new('ShaderNodeValue')
        world_scale.label = 'World Scale'
        world_scale.location = (-600,0)

        # seperate node
        separate = nodes.new('ShaderNodeSeparateXYZ')

        # color ramp node
        color_ramp = nodes.new('ShaderNodeValToRGB')

        # material output node
        mat = nodes['Material Output']
        mat.location = (500,0) 

        # link nodes
        links.new(geometry.outputs['Position'], mapping.inputs['Vector'])
        links.new(world_position.outputs['Value'], mapping.inputs['Location'])
        links.new(world_scale.outputs['Value'], mapping.inputs['Scale'])
        links.new(mapping.outputs['Vector'], separate.inputs['Vector'])
        links.new(separate.outputs['Z'], color_ramp.inputs['Fac'])
        links.new(color_ramp.outputs['Color'], mat.inputs['Surface'])
        return material
    

    def create_terrain_material(self, context, mesh):
        material = bpy.data.materials.get("terrain")
        if material != None:
            bpy.data.materials.remove(material)
        material = bpy.data.materials.new("terrain")
        material.use_nodes = True
        mesh.data.materials.append(material)
        
        nodes = material.node_tree.nodes
        links = material.node_tree.links
        
        # delete old nodes
        #for n in nodes:
        #    n.delete()
        
        # create the mix/blender/layer group node
        layer_group = self.create_layer_group()
        group = nodes.new("ShaderNodeGroup")
        group.node_tree = layer_group
        group.label = "terrain_shader"
        #bpy.data.node_groups['layer_group'] # or call by name
        group.location = (0,0)

        # mapping
        tex_coord = nodes.new('ShaderNodeTexCoord')
        tex_coord.location = (-800,0) 

        mapping = nodes.new('ShaderNodeMapping')
        mapping.location = (-600,0)
        #mapping.scale = [10,10,10]
        mapping.vector_type = 'VECTOR'     
        
        # input nodes
        alpha = nodes.new('ShaderNodeTexImage')
        alpha.name = "alpha"
        alpha.location = (-400,900)
        #alpha.color_space = 'NONE'
        
        red = nodes.new('ShaderNodeTexImage')
        red.name = "red"
        red.location = (-400,600)
        #red.color_space = 'NONE'  
        
        green = nodes.new('ShaderNodeTexImage')
        green.name = "green"
        green.location = (-400,300)
        #green.color_space = 'NONE'
        
        blue = nodes.new('ShaderNodeTexImage')
        blue.name = "blue"
        blue.location = (-400,000)
        #blue.color_space = 'NONE'   
        
        base = nodes.new('ShaderNodeTexImage')
        base.name = "base"
        base.location = (-400,-300)
        #base.color_space = 'NONE'

        terrain = nodes.new('ShaderNodeTexImage')
        terrain.name = "terrain"
        terrain.location = (-400,-600)         
        #terrain.color_space = 'NONE'
        terrain.extension = 'EXTEND'        

        color = nodes.new('ShaderNodeTexImage')
        color.name = "color"
        color.location = (-400,-900)  
        #color.color_space = 'NONE'
        color.extension = 'EXTEND'

        boundary = nodes.new('ShaderNodeTexImage')
        boundary.name = "boundary"
        boundary.location = (-400,-1200)
        #boundary.color_space = 'NONE'  
        boundary.extension = 'EXTEND'
        
        
        # output nodes
        dif = nodes['Principled BSDF'] # get existing nodes
        dif.location = (300,0) 
        
        mat = nodes['Material Output']
        mat.location = (500,0) 

        # link mapping nodes
        links.new(tex_coord.outputs['UV'], mapping.inputs['Vector'])
        links.new(mapping.outputs['Vector'], alpha.inputs['Vector'])
        links.new(mapping.outputs['Vector'], red.inputs['Vector'])
        links.new(mapping.outputs['Vector'], green.inputs['Vector'])
        links.new(mapping.outputs['Vector'], blue.inputs['Vector'])
        links.new(mapping.outputs['Vector'], base.inputs['Vector'])
        
        # link image nodes
        links.new(alpha.outputs['Color'], group.inputs['alpha layer'])
        links.new(blue.outputs['Color'], group.inputs['blue layer'])
        links.new(green.outputs['Color'], group.inputs['green layer'])
        links.new(red.outputs['Color'], group.inputs['red layer'])
        links.new(base.outputs['Color'], group.inputs['base layer'])
        links.new(terrain.outputs['Color'], group.inputs['terrain_rgb'])
        links.new(terrain.outputs['Alpha'], group.inputs['terrain_a'])
        links.new(color.outputs['Color'], group.inputs['color'])
        links.new(boundary.outputs['Color'], group.inputs['boundary'])
        
        # output nodes
        links.new(group.outputs['color'], dif.inputs['Base Color']) 
        links.new(dif.outputs['BSDF'], mat.inputs['Surface']) 
        
        return material
        
    def create_layer_group(self):
        # function adds a group to the group menu, the user still has to place it afterwards
        
        # check for old, and remove it
        if "layer_group" in bpy.data.node_groups:
            group = bpy.data.node_groups["layer_group"]
            bpy.data.node_groups.remove(group)
            
        group = bpy.data.node_groups.new("layer_group", 'ShaderNodeTree')
        
        # create group inputs
        group_inputs = group.nodes.new('NodeGroupInput')
        group_inputs.location = (-500,0)
        group.inputs.new('NodeSocketColor','alpha layer')
        group.inputs.new('NodeSocketColor','red layer')
        group.inputs.new('NodeSocketColor','green layer')
        group.inputs.new('NodeSocketColor','blue layer')
        group.inputs.new('NodeSocketColor','base layer')
        group.inputs.new('NodeSocketColor','terrain_rgb')
        group.inputs.new('NodeSocketColor','terrain_a')
        group.inputs.new('NodeSocketColor','color')
        group.inputs.new('NodeSocketColor','boundary')

        uv_tile = group.inputs.new('NodeSocketFloat','uv tile')

        # default values
        uv_tile.default_value = 10
        
        # create group outputs
        group_outputs = group.nodes.new('NodeGroupOutput')
        group_outputs.location = (500,0)
        group.outputs.new('NodeSocketColor','color')   

        # create mix nodes
        mix_0 = group.nodes.new('ShaderNodeMixRGB')
        mix_0.location = (0,0)

        mix_1 = group.nodes.new('ShaderNodeMixRGB')
        mix_1.location = (100,200)

        mix_2 = group.nodes.new('ShaderNodeMixRGB')
        mix_2.location = (200,400)
        
        mix_3 = group.nodes.new('ShaderNodeMixRGB')
        mix_3.location = (300,600) 
        
        # create seperate nodes
        rgb = group.nodes.new('ShaderNodeSeparateRGB')
        rgb.location = (-300,500)
        
        # create seperate nodes
        a = group.nodes.new('ShaderNodeInvert')
        a.location = (-300,300)      

        # create multiply for color
        mix_color = group.nodes.new('ShaderNodeMixRGB')  
        mix_color.location = (400,0)
        mix_color.blend_type = 'MULTIPLY'

        # link group inputs -> nodes
        group.links.new(group_inputs.outputs['base layer'], mix_0.inputs['Color1'])
        
        group.links.new(group_inputs.outputs['blue layer'], mix_0.inputs['Color2'])
        group.links.new(group_inputs.outputs['green layer'], mix_1.inputs['Color2'])
        group.links.new(group_inputs.outputs['red layer'], mix_2.inputs['Color2'])
        group.links.new(group_inputs.outputs['alpha layer'], mix_3.inputs['Color2'])

        group.links.new(group_inputs.outputs['color'], mix_color.inputs['Color2'])
        
        group.links.new(group_inputs.outputs['terrain_rgb'], rgb.inputs['Image'])
        group.links.new(a.inputs['Color'], group_inputs.outputs['terrain_a'])
        
        # link nodes -> nodes
        group.links.new(mix_1.inputs['Color1'], mix_0.outputs['Color'])
        group.links.new(mix_2.inputs['Color1'], mix_1.outputs['Color'])
        group.links.new(mix_3.inputs['Color1'], mix_2.outputs['Color'])
        
        group.links.new(mix_0.inputs['Fac'], rgb.outputs['B'])
        group.links.new(mix_1.inputs['Fac'], rgb.outputs['G'])
        group.links.new(mix_2.inputs['Fac'], rgb.outputs['R'])
        group.links.new(mix_3.inputs['Fac'], a.outputs['Color'])

        group.links.new(mix_3.outputs['Color'], mix_color.inputs['Color1'],)
        
        # link nodes -> group output
        group.links.new(mix_color.outputs['Color'], group_outputs.inputs['color'])     

        return group

#------------------- register/unregister ------------------------------ 
class import_capture_3d_video(bpy.types.Operator):
    bl_label = "Import Capture 3D Video"
    bl_idname = "bitshift.import_capture_3d_video"
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")
    
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    
    def execute(self, context):
        print("Import capture 3D video: " + self.filepath)

        total_start = timer()
        
        with open(self.filepath, "r") as read_file:
            capture_data = json.load(read_file)
            
        dir = os.path.dirname(os.path.abspath(self.filepath));
        
        speed_multiplier = capture_data["speed_multiplier"]
        print("speed_multiplier: " + str(speed_multiplier));
        

        default_collection = bpy.data.collections["Collection"]

        # import static scenery into its own collection 
        static_collection = bpy.data.collections.new("Static")
        for si, s in enumerate(capture_data["static"]):
            path = os.path.join(dir, s["f"]);
            print("IMPORTING STATIC: " + path);

            old_objs = set(default_collection.objects)
            bpy.ops.import_scene.gltf(filepath=path)
            imported_objs = set(default_collection.objects) - old_objs

            bpy.context.scene.collection.children.link(static_collection)
            for obj in imported_objs:
            	# add to collection
                static_collection.objects.link(obj)

                # remove from the default collection
                default_collection.objects.unlink(obj)

            
        current_frame_time = 0;
        total = len(capture_data["frames"])

        wm = context.window_manager
        wm.progress_begin(0, total);
        
        # import and add to animation timeline
        for fi, f in enumerate(capture_data["frames"]):

            frame_start = timer()

            frame_time = f["t"]
            path = os.path.join(dir, f["f"]);
            print("IMPORTING FRAME: " + str(fi) + " of " + str(total) + " - " + path);

            wm.progress_update(fi)
            
            # https://blender.stackexchange.com/questions/108110/getting-an-imported-objects-name
            old_objs = set(default_collection.objects) #context.scene.objects)

            #print("num objs in default collection: " + str(len(default_collection.objects)));

            bpy.ops.import_scene.gltf(filepath=path)
            imported_objs = set(default_collection.objects) - old_objs #context.scene.objects) - old_objs

            # create a collection
            collection = bpy.data.collections.new("Frame_" + str(fi))
            bpy.context.scene.collection.children.link(collection)

            # create an empty as the root of the collection, as we currently cant
            # animate collection visibility
            # https://blender.stackexchange.com/questions/51290/how-to-add-empty-object-not-using-bpy-ops
            #emptyObj = bpy.data.objects.new("empty", None)
            #collection.objects.link(emptyObj)
            
            
            current_frame = fi * 10 # TODO: compute proper frame
            next_frame = current_frame + 10 # TODO:as above

            # link imported objs to the collection
            for obj in imported_objs:
                #print("     imported: " + obj.name)

                # parent to empty
                #obj.parent = emptyObj

                # add to collection
                collection.objects.link(obj)

                # remove from the default collection
                default_collection.objects.unlink(obj)

                self.animate_visibility(context, obj, current_frame, next_frame)
                
                # https://blenderscripting.blogspot.com/2011/07/scripted-keyframing-of-hide-hide-render.html
                
            # now lets animate the collection
            #self.animate_visibility(context, emptyObj, current_frame, next_frame)

                
            current_frame_time += frame_time

            frame_end = timer()
            print("    IMPORTED IN " + str(frame_end - frame_start) + "s");

                        
        wm.progress_end()
        total_end = timer()
        print("\nCAPTURE IMPORTED IN A TOTAL OF " + str(total_end - total_start) + "s");

        return {"FINISHED"} 

    def animate_visibility(self, context, obj, current_frame, next_frame):
        # 
        if (current_frame > 0):
            obj.hide_render = True
            obj.hide_viewport = True
            obj.keyframe_insert(  data_path="hide_render", 
                                        index=-1, 
                                        frame=0)  
            obj.keyframe_insert(  data_path="hide_viewport", 
                                        index=-1, 
                                        frame=0)  

        obj.hide_render = False
        obj.hide_viewport = False
        obj.keyframe_insert(  data_path="hide_viewport", 
                                index=-1, 
                                frame=current_frame)  
        obj.keyframe_insert(  data_path="hide_render", 
                                index=-1, 
                                frame=current_frame)
        
        obj.hide_render = True
        obj.hide_viewport = True
        obj.keyframe_insert(  data_path="hide_render", 
                                index=-1, 
                                frame=next_frame)  
        obj.keyframe_insert(  data_path="hide_viewport", 
                                index=-1, 
                                frame=next_frame)  

#------------------- vertex colours ------------------------------
class toggle_vertex_colours(Operator):
    """Toggle Vertex Colours"""
    bl_label = "Toggle Vertex Colours"
    bl_idname = "bitshift.toggle_vertex_colours"
    mode: bpy.props.StringProperty()
    
    def execute(self, context):
        print("toggle_vertex_colours")

        if self.mode == 'show':
            objects = bpy.context.selected_objects
            if len(objects) <= 0:
                objects = bpy.data.objects

            for obj in objects:
                material = self.create_material(context)
                mat_idx = self.add_material(context, obj, material)
                self.activate_material_index(context, obj, mat_idx)

        if self.mode == 'hide':
            material = bpy.data.materials.get("vertex_colour")
            if material != None:
                bpy.data.materials.remove(material)
                bpy.ops.object.material_slot_remove()

            self.clean_mat_slots()
                
        
        # change to material view so user can see it is applied
        for area in bpy.context.screen.areas: 
            if area.type == 'VIEW_3D':
                space = area.spaces.active
                if space.type == 'VIEW_3D':
                    space.shading.type = 'MATERIAL'

        return {"FINISHED"}

    def clean_mat_slots(self):
        for ob in bpy.data.objects:
            ob.active_material_index = 0
            for i in range(len(ob.material_slots)):
                bpy.ops.object.material_slot_remove({'object': ob})

    def find_material(self, context, obj, name):
        for m in mesh.data.materials:
            if m.name == "vertex_colour":
                return m

        return None

    def add_material(self, context, obj, material):
        vertex_colour_mat_index = -1
        #slots = list(obj.material_slots)
        for i, s in enumerate(obj.data.materials):
            if s == material:
                print("material found!")
                return i

        obj.data.materials.append(material)
        return len(obj.data.materials)

    def create_material(self, context):
        material = bpy.data.materials.get("vertex_colour")
        if material != None:
            return material

        #material = self.find_material(context, obj, "vertex_colour")
        #if material != None:
        #    return material
            
        material = bpy.data.materials.new("vertex_colour")
        material.use_nodes = True
        
        nodes = material.node_tree.nodes
        links = material.node_tree.links
        
        # delete old nodes
        for n in nodes:
            if n.bl_idname != 'ShaderNodeOutputMaterial':
                nodes.remove(n)
        
        # input nodes
        attrib = nodes.new('ShaderNodeAttribute')
        attrib.name = "vertex_colour"
        attrib.attribute_name = "Col"
        attrib.location = (-300,0)
        
        mat = nodes['Material Output']
        mat.location = (0,0) 
        
        # output nodes
        links.new(attrib.outputs['Color'], mat.inputs['Surface']) 
        
        return material

    def activate_material_index(self, context, obj, mat_index):
        for poly in obj.data.polygons:
            poly.material_index = mat_index
        

#------------------- modified images ------------------------------
class save_modified_images(Operator):
    """Save Modified Images"""
    bl_label = "Save Modified Images"
    bl_idname = "bitshift.save_modified_images"
    
    def execute(self, context):
        # from save dirty which was retired
        # https://developer.blender.org/rBaac95aa1e9727f145e258561816baac80b915f20#change-Yh6e97jSVSQm
        print("save_modified_images")
        unique_paths = set()
        for image in bpy.data.images:
            if image.is_dirty:
                if image.packed_file:
                    if image.library:
                        self.report({'WARNING'},
                            "Packed library image: %r from library %r"
                            " can't be re-packed" %
                            (image.name, image.library.filepath))
                    else:
                        image.pack()
                else:
                    filepath = bpy.path.abspath(image.filepath, library=image.library)
                    if "\\" not in filepath and "/" not in filepath:
                        self.report({'WARNING'}, "Invalid path: " + filepath)
                    elif filepath in unique_paths:
                        self.report({'WARNING'},
                        "Path used by more than one image: %r" %
                        filepath)
                    else:
                        unique_paths.add(filepath)
                        image.save()
        return {'FINISHED'}

#------------------- panels ------------------------------          
class vertex_color_to_uv(Operator):
    """Vertex Color to UV"""
    bl_label = "Vertex Color to UV"
    bl_idname = "bitshift.vertex_color_to_uv"
    
    def execute(self, context):
        print(self.bl_idname)

        selected = bpy.context.active_object
        if selected == "" or selected is None:
            self.report({'ERROR'},"please select a mesh")
            return {"FINISHED"}

        mesh = selected.data

        # check for second uv channel
        if len(mesh.uv_layers) < 2:
            mesh.uv_layers.new()

        # copy verts[1][0] to uv[1][0]
        # copy verts[2][0] to uv[1][1]
        
        uv_layer = mesh.uv_layers[1].data #.active.data
        vcol_layer = mesh.vertex_colors[1].data #.active.data

        for poly in mesh.polygons:
            #print("Polygon index: %d, length: %d" % (poly.index, poly.loop_total))
            for loop_index in poly.loop_indices:
                #print("    Vertex: %d" % mesh.loops[loop_index].vertex_index)
                #print("    UV: %r" % uv_layer[loop_index].uv)
                #print("    Color: %s" % vcol_layer[loop_index].color)
                uv_layer[loop_index].uv[0] = vcol_layer[loop_index].color[0]

        return {"FINISHED"}

#------------------- register/unregister ------------------------------  
classes = (
    WORKSPACE_PT_BitshiftExportPanel,
    terrain_render_map,
    terrain_import_map,
    export_selected,
    export_scene,
    export_collections,
    export_curves,
    import_capture_3d_video,
    toggle_vertex_colours,
    save_modified_images,
    vertex_color_to_uv
)

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in reversed(classes):
        bpy.utils.unregister_class(c)

if __name__ == "__main__":  # only for live edit.
    register()
