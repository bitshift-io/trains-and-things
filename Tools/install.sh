#!/bin/bash

if hash yay 2>/dev/null; then
	yay -S --noconfirm python-pip
	yay -S --noconfirm gdb
	yay -S --noconfirm patchelf # so we can set rpath on libmodio.so
	yay -S --noconfirm cmake # for modio

	# for windows cross-compiling
	yay -S --noconfirm mingw-w64-binutils
	yay -S --noconfirm mingw-w64-crt
	yay -S --noconfirm mingw-w64-headers
	yay -S --noconfirm mingw-w64-winpthreads
	yay -S --noconfirm mingw-w64-gcc

	# for mac cross-compiling
	#yay -S --noconfirm clang
	#yay -S --noconfirm osxcross-git

	# install docker, then generate the docker build environment - for linux SteamOS friendly builds
	cd docker
	./install.sh
	./build.sh # this will likely need to be be done manually after a reboot!
	cd ..

	# one for android
	#yay -S --noconfirm ncurses5-compat-libs
else
	echo "This linux distro is not support, please use Manjaro/Arch"
	# LEGACY!!! this was for the ubuntu build server, but now occurs via docker!

	# incase we run this script multiple times, we need older version of python
	sudo update-alternatives --set python /usr/bin/python2.7

	sudo apt-get -y install wget # for schroot of Ubuntu

	sudo apt-get -y --force-yes install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev \
	    libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libfreetype6-dev libudev-dev libxi-dev \
	    libxrandr-dev yasm

	# sudo apt-get -y install patchelf
	sudo apt-get -y install autoconf
	wget https://github.com/NixOS/patchelf/archive/0.10.tar.gz
	tar -xvzf 0.10.tar.gz
	cd patchelf-0.10/
	./bootstrap.sh
	./configure
	make
	sudo make install

	sudo apt-get -y install mingw-w64
	sudo apt-get -y install python3.5
	sudo apt-get -y install libcurl4-openssl-dev # for modio

	# get cmake >= 3.5 (needed by godot)
	# https://www.claudiokuenzler.com/blog/755/install-upgrade-cmake-3.10.1-ubuntu-14.04-trusty-alternatives
	r=`cmake --version | grep 3.10`
	if [ -n "$r" ]; then
		echo "CMake 3.10.x already installed"
	else
		wget http://www.cmake.org/files/v3.10/cmake-3.10.1.tar.gz
		tar -xvzf cmake-3.10.1.tar.gz
		cd cmake-3.10.1/
		./configure
		make 
		sudo make install
		sudo update-alternatives --install /usr/bin/cmake cmake /usr/local/bin/cmake 1 --force
	fi

	# gcc >= 4.8.5 (needed by nlohmann json)
	sudo add-apt-repository ppa:ubuntu-toolchain-r/test
	sudo apt-get update
	sudo apt-get install gcc-4.9 g++-4.9
	sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-4.9

	# make python 3.5 the default
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.5 1
	sudo update-alternatives --set python /usr/bin/python3.5

	# this ensures we are installing modules for python3.5:
	sudo curl https://bootstrap.pypa.io/ez_setup.py -o - | sudo python3.5
	sudo easy_install pip
fi

# python
pip install scons
pip install matrix_client
#pip install sphinx
#pip install recommonmark 
#pip install sphinx_rtd_theme
#pip install sphinx-autobuild
#pip install recommonmark
#pip install sphinx_rtd_theme

echo "All done! Run build.py for build systems"

echo "Please reboot, then run Tools/docker/build.sh to build docker image for doing builds"
