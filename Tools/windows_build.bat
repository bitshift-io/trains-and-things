
ECHO "Setting up environment..."
SET PATH=%PATH%;C:\Python27;C:\Python27\Scripts;C:\Program Files\Git
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64


REM ECHO "Checking out Godot..."
REM rmdir /s /q ..\Build\godot\modules\bitshift
REM git-bash godot_build.sh %1

ECHO "Building..."
cd ..\Build\godot
scons -j2 platform=windows bits=64 use_static_cpp=yes builtin_openssl=yes verbose=yes modio=no vsproj=yes
cd ..\..
