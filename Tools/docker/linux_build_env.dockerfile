# This docker makes a docker that can build our custom version of Godot
# such that it can run on SteamOS.
# This involves building against GLIBC 2.19 which ubuntu trust uses (14 LTS)

FROM ubuntu:trusty

ARG A_THREADS=9

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
#RUN groupadd -r -g 999 fabian && useradd -r -g fabian -u 999 fabian

# permission handling - https://denibertovic.com/posts/handling-permissions-with-docker-volumes/
# setup a startup script to create as the user who is running the docker image
#RUN apt-get update && apt-get -y --no-install-recommends install \
#    ca-certificates \
#    curl

# grab gosu for easy step-down from root
# https://github.com/tianon/gosu/releases
ENV GOSU_VERSION 1.12
RUN \
    #set -eux; \
	#savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends ca-certificates dirmngr gnupg wget; \
	#rm -rf /var/lib/apt/lists/*; \
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	#wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	#export GNUPGHOME="$(mktemp -d)"; \
	#gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	#gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	#gpgconf --kill all; \
	#rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	#apt-mark auto '.*' > /dev/null; \
	#[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark > /dev/null; \
	#apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	chmod +x /usr/local/bin/gosu; \
	gosu --version; \
	gosu nobody true

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
# end permission handling

# some basics to get ubuntu up to date
RUN apt-get update; \
    apt-get -y install software-properties-common wget

# install godot required packages + some bitshift specifics
# libcurl is needed for modio
# python3.5 for my build scripts
RUN apt-get -y --force-yes install build-essential \
        scons \
        pkg-config \
        libx11-dev \
        libxcursor-dev \
        libxinerama-dev \
        libgl1-mesa-dev \
        libglu-dev \
        libasound2-dev \
        libpulse-dev \
        libfreetype6-dev \
        libudev-dev \
        libxi-dev \
        libxrandr-dev \
        yasm \
        libcurl4-openssl-dev

# install gcc >= 4.8.5 and set as default (needed by nlohmann jsona and for c11 features)
RUN sudo add-apt-repository ppa:ubuntu-toolchain-r/test \
	&& sudo apt-get update \
	&& sudo apt-get -y install gcc-9 g++-9 \
	&& sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 100 --slave /usr/bin/g++ g++ /usr/bin/g++-9 \
    && sudo update-alternatives --set gcc /usr/bin/gcc-9 \
    && echo "DEFAULT VERSION OF GCC:" \
    && gcc -v

# download and build patchelf
# dont need this anymore - modio is precompiled
RUN apt-get -y install autoconf \
    && wget "https://github.com/NixOS/patchelf/archive/0.10.tar.gz" \
    && tar -xvzf 0.10.tar.gz \
    && cd patchelf-0.10 \
    && ./bootstrap.sh \
    && ./configure \
    && make -j$A_THREADS \
    && sudo make install \
    && cd .. \
    && rm -rf patchelf-0.10 \
    && rm -rf 0.10.tar.gz

# make and install CMake for modio
# dont need this anymore - modio is precompiled
RUN wget "http://www.cmake.org/files/v3.16/cmake-3.16.1.tar.gz" \
    && tar -xvzf cmake-3.16.1.tar.gz \
    && cd cmake-3.16.1 \
    && ./configure \
    && make -j$A_THREADS \
    && sudo make install \
    && sudo update-alternatives --install /usr/bin/cmake cmake /usr/local/bin/cmake 1 --force \
    && cd .. \
    && rm -rf cmake-3.16.1 \
    && rm -rf cmake-3.16.1.tar.gz

# install python 3.5 and pip3 and make the default python + python dependencies
RUN apt-get -y install python3.5 \
        python3-pip \
        python-pip \
	&& sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.5 1 \
	&& sudo update-alternatives --set python /usr/bin/python3.5 \
    && sudo pip install scons
#    && sudo pip3 install sphinx \
#    && sudo pip3 install recommonmark \ 
#    && sudo pip3 install sphinx_rtd_theme \
#    && sudo pip3 install matrix_client \
#    && sudo pip3 install sphinx-autobuild \
#    && sudo pip3 install recommonmark \
#    && sudo pip3 install sphinx_rtd_theme

# project root = working dir (ie. what ever is mount to /working_volume is also the working dir)
VOLUME ["/working_volume"]
WORKDIR "/working_volume"
