#!/bin/bash

yay -S --noconfirm docker

echo "adding user to docker. type exit when # shell is displayed..."

# https://docs.docker.com/install/linux/linux-postinstall/
sudo groupadd docker
sudo usermod -aG docker $USER

# force apply the user group change so we dont need to restart to use docker
sudo newgrp docker

echo "starting docker"

sudo systemctl enable docker
sudo systemctl start docker

echo "Docker installed, now run build.sh to build docker images"
