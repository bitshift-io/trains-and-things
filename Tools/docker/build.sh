#!/bin/bash

docker build . -f linux_build_env.dockerfile --tag linux_build_env

# Sample on how to run a command via the docker:
#   docker run -v /home/fabian/Projects/TrainsAndThings/:/working_volume linux_build_env -e LOCAL_USER_ID=`id -u $USER` echo "Hello world!"
#   docker run -v /home/fabian/Projects/TrainsAndThings/Build/godot:/working_volume -e LOCAL_USER_ID=`id -u $USER` linux_build_env scons -j8 platform=x11

# How to get into a image with a shell:
#   docker image ls

# login as me:
#   docker run -v /home/fabian/Projects/TrainsAndThings:/working_volume -e LOCAL_USER_ID=`id -u $USER` -it linux_build_env /bin/bash

# or as root:
#   docker run -v /home/fabian/Projects/TrainsAndThings:/working_volume -u 0 -it linux_build_env /bin/bash



echo "Docker images built"
