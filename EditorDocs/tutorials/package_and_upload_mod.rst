.. _doc_gui_tutorial:

Package and Upload Maps and Mods
=================================

Once you are ready to upload your mod for the first time go to `mod.io <https://mod.io>`_ and register/login.

Then you need to create a new mod by going to the `Trains & Things Add Mod page <https://trainsandthings.mod.io/add>`_.

Upload a suitable thumbnail and pick any relevant tags. Give your mod a name and fill out the summary.

This summary is what will display to uses in game when they are looking at your mod.

Note we do not use the Description, Dependencies or Metadata fields.

Down the bottom of this page you will see "Metadata key value pairs".

Here we need to setup a field:

    * install_dir which is the directory to install the map or mod into. This should be "Mods" for a mod, or "Maps" for a map. Mods that include maps should just go into "Mods"

Once filled out hit the "Save mod and next step" button.

You can skip the Media page by just pressing "Skip step".

Now you should be on the files page. Up the top of this page you should have a title for your mod, something like "My Mod for Trains & Things". Under this is the release date then who created your mod.
What you need to get is the ID of the mod which will be after the "Created by Joe Blogs (ID: XXXXX)". Copy this number and assign this id to the "mod_id" field as shown:

.. image:: /img/plugin_mod_id.jpg

Make sure you save the changed Plugin.tscn

The field allows us to identify if you mod is installed and match it to the mod in mod.io.

Now you have the mod id assigned in your mod, you can package it up and upload it to mod.io

In the root of the Editor directory you can run the following command to package up your maps and mods,
For Linux::

    ./TrainsAndThings_Linux -s Editor/Package.gd -outdir=tmp

For Windows::

    TrainsAndThings_Windows.exe -s Editor/Package.gd -outdir=tmp

This will create a directory called tmp and populate it with the Packaged up Maps and Mods

Find your mod then add it to a zip file.

On the mod.io files page you can then upload this zip file.

Ensure you have the same Version field as on your Plugin.tscn file and enter that into mod.io Version field.

If there is a difference in these fields, Trains & Things will attempt to indicate to the user that a newer version of the mod is available and can be updated.

Your now free to finish the Files step.

Your mod should now be published and show up in game.


