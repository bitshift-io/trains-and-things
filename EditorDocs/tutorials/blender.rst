.. _doc_gui_tutorial:

Blender
======================================

Introduction
~~~~~~~~~~~~
Some Blender notes and tips.

#. Create a new plane, radius 500.

#. Press "u" select unwrap.

#. Press ctrl+e on selected face, subdivide with 9 cuts

#. repeat the above now with 99 cuts to have a total of 1000x1000 divisions

#. add displace modifier with your height map and configure to desired height

#. use the plugin from https://github.com/domlysz/BlenderGIS here if using geotiff raw (16 or 32bit)


