.. _doc_gui_tutorial:

Make a Plugin
======================

Introduction
~~~~~~~~~~~~
Trains & Things uses the concept of a "Plugin" to load both maps and mods.
A Plugin may have a number of Map Descriptors which describe a map the game may load.
A plugin may also have a number of Mod Descriptors which describe a mod the game can load.
Plugins should be contained with in their own folder in either the Map or Mod directory for the game to detect them.

The quick and lazy way
~~~~~~~~~~~~~~~~~~~~~~~
The quick way to create a plugin is to open the directory of an existing map and copy the Plugin.tscn file into the directory where your map is located.

If you are making a mod, do the same from an existing mod.

You can then edit the file in a text editor or open the file in the Godot editor and edit the fields.

The full way
~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Open the Godot editor.

2) In the menu select Scene > New Scene

.. image:: /img/editor_new_scene.jpg

3) Press the Add Child Node icon

.. image:: /img/editor_add_child_node.jpg

4) Select Node, then Create

.. image:: /img/editor_create_new_node.jpg

5) Rename the node by double clicking it and call it Plugin

.. image:: /img/editor_plugin_node.jpg

6) Select the node, then in the Inspector view, click the script down arrow and click the Load button

.. image:: /img/editor_inspector_script_load.jpg

7) In the Open a File dilog the pops up, navigate to Script/Plugin and select Plugin.gd before pressing Open

.. image:: /img/editor_open_script_plugin.jpg

8) Press the Add Child Node icon (as we did in step 3)

9) Select Node, then Create (as we did in step 4)

10) Rename the new node by double clicking it and call it MapDescriptor (similar to step 5)

Your Scene view should now look like this:
.. image:: /img/editor_scene_map_descriptor.jpg

11) Select the Mapdescriptor node, then in the Inspector view, click the Script down arrow and click the Load button (as we did in step 6)

12) In the Open a File dilog the pops up, navigate to Script/Plugin and select MapDescriptor.gd before pressing Open (similar to what we did in step 7)

13) Now select the Plugin node and in the Inspector view we can enter appropriate values

14) Select the MapDescriptor node and enter appropriate values in the Inspector view.
Ensure Scene Path is pointing to the map tscn file you want to load.

15) In the menu select Scene > Save Scene and save the file as Plugin.tscn with in your map directory

Notes
~~~~~~~~~~~~~~~~~~~~~~~~~~
The above process gives instructions on setting up a single Map Descriptor, the same process is done except the ModDescriptor.gd is selected instead of MapDescriptor.gd for adding in a Mod Descriptor.

As mentioned in the Introduction, a Plugin can have as many Map Descriptors and or Mod Descriptors as its children as you want. This allows for example, for you to have a Single player campaign version of your map and a multiplayer specific version or some forms of variations on of your maps and mods.



