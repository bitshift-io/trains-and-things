.. _doc_gui_tutorial:

Create a Height Map and a Normal Map
======================================

Introduction
~~~~~~~~~~~~

Generating a height map is the most important part of the map making process. This is the current method we are using for getting realistic terrain for the game.

Get satellite data
~~~~~~~~~~~~~~~~~~~~~~~~~~~
#. Get the data from your source of choice. We currently use:

| `http://opentopo.sdsc.edu/raster?opentopoID=OTSRTM.042013.4326.1 <http://opentopo.sdsc.edu/raster?opentopoID=OTSRTM.042013.4326.1>`_
| `https://terrain.party/ <https://terrain.party/>`_
| 

#. Use the select region button and draw a square box around the area you wish to use.

#. Select output format GeoTiff.

#. Deselect all visualizations.

#. Enter a job title, description and your email.

#. The resulting data will be emailed to you. Continue to download the files.

From GeoTiff to Game
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We now have a Geotif image which needs to be adjusted for support within Blender or Trains and Things.

#. First, we need to process the image to something that is editable within your typical image editing software

    #. Run the 'image.py' script, which is located in the tools/image_tools directory, over your texture. Or using the command line: ::
    
        ./image.py -i hawaii.tif -n 1
        
    This will convert the Geotiff which has height elevation data that can be negative or positive values to values between the range of 0 to 1 using a floating point texture to maintain precision.
    
    #. Edit the generated texture to your hearts content with your texture editing software. We use Krita as it supports 32-bit float textures.
    
    #. Ensure that you crop the image to be a square texture. If using in the game, it will need to be a power of 2. If it will be used in blender, then it does not matter.

    #. This saved texture may be used directly in Godot as the heightmap for terrain allowing you to have a quick turn around between editing and viewing the results

Bring it into Blender
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We like to take the heightmaps in to Blender to paint terrain maps, and sculpt further details.

#. Open the Blender (at minimum version 2.80).
    
#. Import the bitshift tools. Edit > Preferences > Add-ons > Install > Locate Tools/blender/bitshift.py

#. Under the Active Tool panel on the right, there should now be a bitshift group.

#. From the bitshift panel select Import Map. And point to your height map. Note: you can also use this to import existing game heightmaps too!

#. After it has imported feel free to sculpt and paint your layers.

#. When you are done, use the Render Map button. If you want to quickly check a low quality version in game, you can also use Quick Render Map.

Final important tips
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A few final tips to help you build great maps.

#. The highest quality terrain Trains & Things supports is 1 quad per metre. This means that your texture size should be the same size as your terrain (or as close as possible). For example, we have our Hawaii map set to be 1200 in size, and so our height map texture is 1024. This helps avoid jagged terrain being generated.

#. It is recommended to work at double resolution with your textures (incase you need to scale the map up), then half the height map and leave the normal map at double the size of the height map. So in Hawaii we have a 2048 sized normal map, and work on a 2048 sized height map.

#. If your planning to make multiple map sizes of the same map, create the largest one first.  
