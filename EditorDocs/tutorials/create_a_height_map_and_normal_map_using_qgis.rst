.. _doc_gui_tutorial:

Create a Height Map and a Normal Map (old method)
======================================

Introduction
~~~~~~~~~~~~

Generating a height map is the most important part of the map making process. Using QGIS we will show you how to create real world height maps and import them into Godot for use with making maps.

Install QGIS and GIS data
~~~~~~~~~~~~~~~~~~~~~~~~~~~
#. Install QGIS 3.x from qgis.org
    
    For Arch users this is on the AUR and can be installed with the command::

        yay -S qgis
        sudo pip install OWSLib
        sudo pip install psycopg2

#. Download the region of the world you wish to generate a map from `http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/ <http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/>`_
        
    Once downloaded extract the zip files. If you are using linux you can do this real easy::

        unzip \*.zip
        rm *.zip

    For this tutorial I will be using North America which is part of the West data set.
    
    Note that QGIS 3.x has a plugin called "SRTM Downloader" which I haven't tried but might do something similar.

One time QGIS setup
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#. Load QGIS

#. Load some sort of global data to help us see state borders and nations on the map

    #. Right click XYZ Tiles from the Browser tree and select New Connection...
    #. For google maps enter the URL::
        
        http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga
        
    #. For google satellite enter the URL:
    
        https://mt1.google.com/vt/lyrs=s&x=%7Bx%7D&y=%7By%7D&z=%7Bz%7D
        
        This is useful for putting together the colour map for the terrain
        
    #. For openstreetmaps, repeat step 1 and 2 but using the URL::
        
        http://tile.openstreetmap.org/{z}/{x}/{y}.png
        
    #. Double click the new connections to add them as visible Layers
    
    You can find more XYZ files over at `https://github.com/nextgis/quickmapservices_contrib/tree/master/data_sources <https://github.com/nextgis/quickmapservices_contrib/tree/master/data_sources>`_.
    
    Now each time you load QGIS, these connections will be available and you can just double click them to get them as visible layers.
    
Using QGIS to make a height map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#. Load QGIS

#. Click Project > Project Properties, in filter type "4326", then select "WGS 84" under "Coordinate Reference System"

#. Double click and XYZ tile connections we prepared earlier to help view and navigate the map

#. Now we want to load the data we downloaded. We do this with the following steps:

    #. From the menu select Layer > Add Layer > Add Raster Layer...
    
    #. Press the ... and select the appropriate files, for me this is tif file /home/fabian/Projects/srtm3/SRTM_W_250m
        
    #. Press Add and then hit Close
        
    Your map should look something like this:

    .. image:: /img/qgis_north_america.jpg
    
    Note: If you want to make a map that straddles multiple images you will need to merge them into a single layer, a guide can be found `here <http://www.qgistutorials.com/en/docs/raster_mosaicing_and_clipping.html>`_.

#. Next we need to generate a selection around the region we want to export

    #. Drag Google Maps to the top layer to see the state boundaries
    
    #. Select Raster > Extraction > Clip Raster by Extent
    
    #. Make sure the Input layer is the layer you want to export

    #. Under Clipping extent select the '...' button > Select Extent on Canvas
    
    #. Press Run then Close when complete.
    
    #. You should have a new layer called Clipped (extent)
    
    Your map should look something like this:
    
    .. image:: /img/qgis_north_america_clipped.jpg
    
#. Export the clipped map

    #. Right click the Clipped layer and press Save As
    
    #. Choose an output location and name before pressing OK to save.
    

#. Optional step is to backup the Extent

    This is important if you want to minimise how much data is required to backup the height map incase you want to create it from scratch in future

    #. Right clicking the Extent layer and selecting Save As
    
    #. Change the format to ESRI Shapefile
    
    #. Enter an appropriate name, I've called mine Texas-Extent and OK the dialog
    
    #. Commit Texas-Extent.* files to your version control system
    
From GeoTiff to Game
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We now have a Geotif image which needs to be adjusted for support within Blender or Trains and Things.

#. First, we need to process the image to something that is editable within your typical image editing software

    #. Run the 'image.py' script, which is located in the tools/image_tools directory, over your texture. Or using the command line: ::
    
        ./image.py -i hawaii.tif -n 1
        
    This will convert the Geotiff which has height elevation data that can be negative or positive values to values between the range of 0 to 1 using a floating point texture to maintain precision.
    
    #. Edit the generated texture to your hearts content with your texture editing software. We use Krita as it supports 32-bit float textures.
    
    #. Ensure that you crop the image to be a square texture. If using in the game, it will need to be a power of 2. If it will be used in blender, then it does not matter.

    #. This saved texture may be used directly in Godot as the heightmap for terrain allowing you to have a quick turn around between editing and viewing the results

Bring it into Blender
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We like to take the heightmaps in to Blender to paint terrain maps, and sculpt further details.

#. Open the Blender (at minimum version 2.80).
    
#. Import the bitshift tools. Edit > Preferences > Add-ons > Install > Locate Tools/blender/bitshift.py

#. Under the Active Tool panel on the right, there should now be a bitshift group.

#. From the bitshift panel select Import Map. And point to your height map. Note: you can also use this to import existing game heightmaps too!

#. After it has imported feel free to sculpt and paint your layers.

#. When you are done, use the Render Map button. If you want to quickly check a low quality version in game, you can also use Quick Render Map.

Final important tips
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A few final tips to help you build great maps.

#. The highest quality terrain Trains & Things supports is 1 quad per metre. This means that your texture size should be the same size as your terrain (or as close as possible). For example, we have our Hawaii map set to be 1200 in size, and so our height map texture is 1024. This helps avoid jagged terrain being generated.

#. It is recommended to work at double resolution with your textures (incase you need to scale the map up), then half the height map and leave the normal map at double the size of the height map. So in Hawaii we have a 2048 sized normal map, and work on a 2048 sized height map.

#. If your planning to make multiple map sizes of the same map, create the largest one first.  

    
Other GIS data repositories
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| `https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/ <https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/>`_
| `http://www.viewfinderpanoramas.org/dem3.html <http://www.viewfinderpanoramas.org/dem3.html>`_
| `http://srtm.csi.cgiar.org/ <http://srtm.csi.cgiar.org/>`_
| `https://visibleearth.nasa.gov/view.php?id=73934 <https://visibleearth.nasa.gov/view.php?id=73934>`_
| 



