.. _doc_gui_tutorial:

Make a Map
===================

Introduction
~~~~~~~~~~~~

Lets see how to make a custom map which you will be able to distribute once complete.

Running the editor
~~~~~~~~~~~~~~~~~~

The first thing you need to do is get into the editor.
Open a terminal and run the command (for linux)::

    ./Editor -e
    
You should be presented with the godot editor

.. image:: /img/editor.jpg

For more information on using the editor please head over to the `Godot website <https://godotengine.org>`_

Investigate an existing map
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Navigate to Map/Test and open Test.tscn
Use the wheel to zoom and middle mouse to pan around to get a nice view of the map.

.. image:: /img/editor_test_map.jpg

On the left is the Scene view which shows the contents of the map.
The important thing to note here is the root node "World" which is a Spatial node.
Then down the bottom of the scene list is "Globals" which has been imported into the map out of the Prefab directory which can be seen on the left, this comes from importing "SceneGlobals.tscn" into the map.
These are the most critical elements of a map.
After this are the resources that are placed on the map as well as obviously some form of terrain and lighting!

Make a new map
~~~~~~~~~~~~~~~~~~~~~~~~~~

To begin...
