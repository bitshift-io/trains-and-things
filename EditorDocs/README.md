# Trains &amp; Things Documentation
Documentation for [Trains &amp; Things](http://bit-shift.io/trains-and-things.php)
Follow the link for more information!


The docs can be view online [at read the docs website](http://trains-and-things-docs.readthedocs.io)


For information on how to get this up and running refer to [read the docs website getting started guide](https://docs.readthedocs.io/en/latest/getting_started.html)

