
Documentation for Trains & Things
=============================================

This is documentation for those who wish to mod, map or help contribute to Trains & Things.
The official website can be found at `www.bitshift.io/trains-and-things <https://www.bitshift.io/trains-and-things>`_.


The main documentation for the site is organized into a few sections:

.. * :ref:`sec-tutorials`
.. * :ref:`sec-reference`
.. * :ref:`sec-community`

.. toctree::
   :maxdepth: 1
   :caption: Tutorials
   :name: sec-tutorials

   tutorials/_tutorials

.. toctree::
   :maxdepth: 1
   :caption: Reference
   :name: sec-reference
   
   classes/_classes


.. toctree::
   :maxdepth: 1
   :caption: Community
   :name: sec-community

   contribute/_contribute


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
