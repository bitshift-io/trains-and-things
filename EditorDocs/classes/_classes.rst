Class Reference
===============

The best way to learn how to mod for Trains & Things is to just look through the source code.
Classes that have a B prefix are C++ classes that we (bitshift) have added to extend Godot.

.. toctree::
   :maxdepth: 1
   :name: toc-class-ref
   :glob:

   class_*
 
